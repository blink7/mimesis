// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


PROPHUNT_API DECLARE_LOG_CATEGORY_EXTERN(LogWeapon, Log, All);
PROPHUNT_API DECLARE_LOG_CATEGORY_EXTERN(LogPropHunt, Log, All);

#define COLLISION_SPAWN					ECC_GameTraceChannel1
#define COLLISION_WEAPON				ECC_GameTraceChannel2
#define COLLISION_PROJECTILE			ECC_GameTraceChannel3
#define COLLISION_INVISIBLEWORLDSTATIC	ECC_GameTraceChannel4
#define COLLISION_INTERACT				ECC_GameTraceChannel5

#define MAX_PLAYER_NAME_LENGTH 16

UENUM(BlueprintType)
enum class AbilityInput : uint8
{
	None				UMETA(DisplayName = "None"),
	PrimaryAction		UMETA(DisplayName = "Primary Action"),
	SecondaryAction		UMETA(DisplayName = "Secondary Action"),
	SkillAction			UMETA(DisplayName = "Skill Action"),
	Sprint				UMETA(DisplayName = "Sprint"),
	Jump				UMETA(DisplayName = "Jump"),
	PrimaryFire			UMETA(DisplayName = "Primary Fire"),
	SecondaryFire		UMETA(DisplayName = "Secondary Fire"),
	AlternateFire		UMETA(DisplayName = "Alternate Fire"),
	Targeting			UMETA(DisplayName = "Targeting"),
	Reload				UMETA(DisplayName = "Reload"),
	NextWeapon			UMETA(DisplayName = "Next Weapon"),
	PrevWeapon			UMETA(DisplayName = "Previous Weapon"),
	Spray				UMETA(DisplayName = "Spray"),
	PointOfInterest		UMETA(DisplayName = "PointOfInterest")
};
