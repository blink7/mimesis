// Copyright Art-Sun Games. All Rights Reserved.

#include "PropHunt.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PropHunt, "PropHunt" );

DEFINE_LOG_CATEGORY(LogWeapon)
DEFINE_LOG_CATEGORY(LogPropHunt)
