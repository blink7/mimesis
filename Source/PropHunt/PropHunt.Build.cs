// Copyright Art-Sun Games. All Rights Reserved.

using UnrealBuildTool;

public class PropHunt : ModuleRules
{
    public PropHunt(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "CoreUObject",
                "Engine",
                "LevelSequence",
                "MovieScene",
                "AkAudio"
            }
        );

        PrivateDependencyModuleNames.AddRange(
            new string[] {
                "PropHuntLoadingScreen",
                "InputCore",
                "Slate",
                "SlateCore",
                "UMG",
                "PerfCounters",
                "MoviePlayer",
                "Niagara",
                "Http",
                "Json",
                "JsonUtilities",
                "GameplayAbilities",
                "GameplayTags",
                "GameplayTasks",
                "Paper2D",
                "PhysicsCore",
                "ChunkDownloader"
            }
        );

        PrivateDependencyModuleNames.Add("OnlineSubsystem");

        if (Target.Platform == UnrealTargetPlatform.Win32 || Target.Platform == UnrealTargetPlatform.Win64)
        {
            DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");
            PrivateDependencyModuleNames.Add("Steamworks");
        }
    }
}
