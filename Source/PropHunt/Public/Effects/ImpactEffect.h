// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Core/PHTypes.h"
#include "ImpactEffect.generated.h"


class USoundCue;

UCLASS()
class PROPHUNT_API AImpactEffect : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	/** Surface data for spawning */
	UPROPERTY(BlueprintReadOnly, Category = "Surface", meta = (ExposeOnSpawn = true))
	FHitResult SurfaceHit;

	UPROPERTY(BlueprintReadOnly, Category = "Surface", meta = (ExposeOnSpawn = true))
	EProjectileSize FXSize;

	bool bRandomizeDecalRoll;

	/** Spawn effect */
	void PostInitializeComponents() override;

protected:
	UPROPERTY()
	class UDataTable* FXMaterialDataTable;

	UPROPERTY()
	class UParticleSystem* SizzleImpactFX;

	UPROPERTY()
	class USoundAttenuation* ParticleSoundAttenuation;

	UPROPERTY()
	USoundCue* SizzlesDrySound;

	UPROPERTY()
	USoundCue* WetSplashSound;

	UPROPERTY()
	USoundCue* DrySplashSound;

	FFXMaterialRow* MaterialValues;

	UFUNCTION()
	void OnImpactParticleCollide(FName EventName, float EmitterTime, int32 ParticleTime, FVector Location, FVector Velocity, FVector Direction, FVector Normal, FName BoneName, class UPhysicalMaterial* PhysMat);
	
	void SpawnDebrisFX(FVector& Location);
};
