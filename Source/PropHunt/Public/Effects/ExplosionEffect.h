// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplosionEffect.generated.h"


UCLASS()
class PROPHUNT_API AExplosionEffect : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	class UNiagaraSystem* ExplosionFX;

	UPROPERTY(BlueprintReadOnly, Category = "Effect")
	float ExplosionRadius;

protected:
	UPROPERTY(VisibleDefaultsOnly, Category = "Effect")
	class UPointLightComponent* ExplosionLight;
	
public:
	/** How long keep explosion light on? */
	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	float ExplosionLightFadeOut;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	class USoundBase* ExplosionSound;

	/** Surface data for spawning */
	UPROPERTY(BlueprintReadOnly, Category = "Surface")
	FHitResult SurfaceHit;

protected:
	virtual void BeginPlay() override;

	/** Point light component name */
	FName ExplosionLightComponentName;

public:
	virtual void Tick(float DeltaTime) override;

	/** Returns ExplosionLight subobject **/
	FORCEINLINE class UPointLightComponent* GetExplosionLight() const { return ExplosionLight; }
};
