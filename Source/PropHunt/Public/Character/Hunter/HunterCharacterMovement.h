// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HunterCharacterMovement.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UHunterCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	virtual float GetMaxSpeed() const override;

	/** Mass limit of the impact object, to which downward force scales exponentially from its mass. */
	UPROPERTY(Category = "Character Movement: Physics Interaction", EditAnywhere, BlueprintReadWrite, meta = (editcondition = "bEnablePhysicsInteraction"))
	float PhysicsMassDependanceLimit = 30.f;

	/** Smoothing exponent of mass dependence. */
	UPROPERTY(Category = "Character Movement: Physics Interaction", EditAnywhere, BlueprintReadWrite, meta = (editcondition = "bEnablePhysicsInteraction"))
	float PhysicsMassDependanceSmoothness = 10.f;

	virtual void ApplyDownwardForce(float DeltaSeconds) override;
};
