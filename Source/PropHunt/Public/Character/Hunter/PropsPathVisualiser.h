// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PropsPathVisualiser.generated.h"


UCLASS()
class PROPHUNT_API APropsPathVisualiser : public AActor
{
	GENERATED_UCLASS_BODY()

protected:
	UPROPERTY(VisibleDefaultsOnly, Category = "Effects")
	class USplineComponent* SplinePath;

public:	
	virtual void Tick(float DeltaTime) override;

	void AddPoint(FVector Location);

	void SetPoints(TArray<FVector>& Points);

	void RemoveFirstPoint();
};
