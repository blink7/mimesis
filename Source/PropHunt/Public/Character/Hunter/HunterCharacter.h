// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Character/PHCharacterBase.h"
#include "HunterCharacter.generated.h"

class AWeapon;
class USoundBase;

DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnHunterCharacterWeaponChange, class AHunterCharacter*, AWeapon* /* new */, AWeapon* /* old */);

UCLASS(Abstract)
class PROPHUNT_API AHunterCharacter : public APHCharacterBase
{
	GENERATED_BODY()

public:
	AHunterCharacter(const FObjectInitializer& ObjectInitializer);

	//~ Begin ACharacter Interface
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void OnRep_PlayerState() override;
	virtual void PostInitializeComponents() override;
	virtual void PawnClientRestart() override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void RecalculateBaseEyeHeight() override;
	//~ End ACharacter Interface

	//~ Begin AActor Interface
	virtual void Tick(float DeltaSeconds) override;
	virtual void Destroyed() override;
	//~ End AActor Interface

	//~ Begin APHCharacterBase Interface
	virtual void GetPlayerViewPoint(FVector& OutLocation, FRotator& OutRotation) const override;
	//~ End APHCharacterBase Interface

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UPHSpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCamera;

	// Pawn mesh: 1st person view (arms; seen only by self)
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* Legs1P;

	// Pawn mesh: 1st person view (arms; seen only by self)
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* Mesh1P;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Effects", meta = (AllowPrivateAccess = "true"))
	class UPostProcessComponent* PropsPathPostProcess;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Light", meta = (AllowPrivateAccess = "true"))
	class USpotLightComponent* Flashlight1P;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Light", meta = (AllowPrivateAccess = "true"))
	class USpotLightComponent* Flashlight3P;

	/** AkComponent to handle playback */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AkAmbientSound", meta = (ShowOnlyInnerProperties))
	class UAkComponent* AkComponent;

public:
	void SimulateFlashlightRotation(float DeltaSeconds);

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	bool IsWeaponInInventory(AWeapon* InWeapon);

	/**
	* [server] Add weapon to inventory
	*
	* @param NewWeapon to add.
	*/
	void AddWeaponToInventory(AWeapon* NewWeapon);

	/**
	* [server] Remove weapon from inventory
	*
	* @param Weapon	Weapon to remove.
	*/
	void RemoveWeaponFromInventory(AWeapon* Weapon);

	/**
	* Find in inventory
	*
	* @param WeaponClass	Class of weapon to find.
	*/
	AWeapon* FindWeapon(TSubclassOf<AWeapon> WeaponClass);

	/**
	* [server + local] Equips weapon from inventory
	*
	* @param NewWeapon to equip
	*/
	void EquipWeapon(AWeapon* NewWeapon);

	// Unequips the specified weapon. Used when OnRep_CurrentWeapon fires.
	void UnEquipWeapon(AWeapon* WeaponToUnEquip);

	// Unequips the current weapon. Used if for example we drop the current weapon.
	void UnEquipCurrentWeapon();

	//////////////////////////////////////////////////////////////////////////
	// Input handlers

	/**
	* Move forward/back
	*
	* @param Value Movement input to apply
	*/
	void MoveForward(float Value);

	/**
	* Strafe right/left
	*
	* @param Value Movement input to apply
	*/
	void MoveRight(float Value);

	/** Player pressed next or prev weapon action */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void OnChangeWeapon(bool bNext);

	void OnStartCrouching();

	void OnStopCrouching();

	//////////////////////////////////////////////////////////////////////////
	// Reading data

	USkeletalMeshComponent* GetPawnMesh() const;

	/** Get currently equipped weapon */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	AWeapon* GetWeapon() const { return CurrentWeapon; };

	FName GetWeaponAttachPoint() const { return WeaponAttachPoint; }

	/** Get weapon target modifier speed	*/
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	float GetTargetingSpeedModifier() const;

	/** Get targeting state */
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsTargeting() const;

	/** Get the modifier value for running speed */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	float GetRifleAmmo() const;

	/** Get the modifier value for running speed */
	UFUNCTION(BlueprintCallable, Category = "HunterCharacter")
	float GetRunningSpeedModifier() const;

	/** Get camera view type */
	UFUNCTION(BlueprintCallable, Category = "Mesh")
	virtual bool IsFirstPerson() const;

	UFUNCTION(BlueprintCallable, Category = "HunterCharacter|Attributes")
	float GetViolations() const;

	UFUNCTION(BlueprintCallable, Category = "HunterCharacter|Attributes")
	float GetMaxViolations() const;

	UFUNCTION(BlueprintCallable, Category = "HunterCharacter")
	bool IsAdmitted() const;

	/*
	* Get either first or third person mesh.
	*
	* @param	WantFirstPerson		If true returns the first person mesh, else returns the third
	*/
	USkeletalMeshComponent* GetSpecificPawnMesh(bool WantFirstPerson) const;

	void HandleViolation();

	void Eject(float DamageAmount, class UGameplayEffectUIData* UIData);

private:
	bool bMesh3PInitialized;

protected:
	//~ Begin APHCharacterBase Interface
	virtual void OnDeath(float DamageAmount, class APawn* InstigatingPawn, class AActor* DamageCauser) override;
	//~ End APHCharacterBase Interface

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh")
	bool bShowHair;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh")
	bool bShowAccessories;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh")
	USkeletalMesh* Body;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh")
	USkeletalMesh* Hair;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh")
	USkeletalMesh* Accessories;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh")
	USkeletalMesh* Arms;

	// Skeleton that will be used for the merged mesh.
    // Leave empty if the generated skeleton is OK.
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Mesh")
    class USkeleton* Skeleton;

	/** Socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	FName WeaponAttachPoint;

	/** Default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	TArray<TSubclassOf<AWeapon>> DefaultInventoryClasses;

	/** Weapons in inventory */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_Inventory)
	TArray<AWeapon*> Inventory;

	/** Currently equipped weapon */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
	AWeapon* CurrentWeapon;

	// Cache tags
	FGameplayTag NoWeaponTag;
	FGameplayTag WeaponChangingDelayReplicationTag;
	FGameplayTag WeaponAmmoTypeNoneTag;
	FGameplayTag WeaponAbilityTag;
	FGameplayTag CurrentWeaponTag;

	// Attribute changed delegate handles
	FDelegateHandle PrimaryAmmoChangedDelegateHandle;
	FDelegateHandle SecondaryAmmoChangedDelegateHandle;

	// Tag changed delegate handles
	FDelegateHandle WeaponChangingDelayReplicationTagChangedDelegateHandle;

	// Set to true when we change the weapon predictively and flip it to false when the Server replicates to confirm.
	// We use this if the Server refused a weapon change ability's activation to ask the Server to sync the client back up
	// with the correct CurrentWeapon.
	bool bChangedWeaponLocally;

	/** Modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	float TargetingSpeedModifier;

	/** Modifier for max movement speed */
	UPROPERTY(EditDefaultsOnly, Category = "HunterCharacter")
	float RunningSpeedModifier;

	/** When low health effects should start */
	float LowHealthPercentage;

	/** The value of the original BaseEyeHeight */
	float DefaultBaseEyeHeight;

	/* Switch to ragdoll */
	void SetRagdollPhysics();

	/** Handle mesh visibility and updates */
	void UpdatePawnMeshes();

	UPROPERTY(EditDefaultsOnly, Category = "Light")
	FName FlashlightAttachPoint;

	UPROPERTY(ReplicatedUsing = OnRep_FlashlightEnable)
	bool bFlashlightEnable;

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void OnToggleFlashlight();

	void ToggleFlashlight();

	UFUNCTION()
	void OnRep_FlashlightEnable();

	virtual void KilledBy(class APawn* EventInstigator) override;

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** updates current weapon */
	void SetCurrentWeapon(AWeapon* NewWeapon, AWeapon* LastWeapon = nullptr);

	UFUNCTION()
	void OnRep_CurrentWeapon(AWeapon* LastWeapon);

	UFUNCTION()
	void OnRep_Inventory();

	/** [server] Spawns default inventory */
	void SpawnDefaultInventory();

	AWeapon* SpawnWeaponAndAddToInventory(TSubclassOf<AWeapon> WeaponClass);

	/** [server] Remove all weapons from inventory and destroy them */
	void DestroyInventory();

	/** Equip weapon */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerEquipWeapon(AWeapon* NewWeapon);

	// Tag changed callbacks
	virtual void WeaponChangingDelayReplicationTagChanged(const FGameplayTag CallbackTag, int32 NewCount);

	// The CurrentWeapon is only automatically replicated to simulated clients.
	// The autonomous client can use this to request the proper CurrentWeapon from the server when it knows it may be
	// out of sync with it from predictive client-side changes.
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSyncCurrentWeapon();

	// The CurrentWeapon is only automatically replicated to simulated clients.
	// Use this function to manually sync the autonomous client's CurrentWeapon when we're ready to.
	// This allows us to predict weapon changes (changing weapons fast multiple times in a row so that the server doesn't
	// replicate and clobber our CurrentWeapon).
	UFUNCTION(Client, Reliable)
	void ClientSyncCurrentWeapon(AWeapon* InWeapon);

	//////////////////////////////////////////////////////////////////////////
	// Abilities

	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Props Path Visualization")
	TSubclassOf<class APropsPathVisualiser> PropsPathVisualiserTemplate;

	/** Handle for efficient management of DefaultTimer timer */
	FTimerHandle TimerHandle_PathTrack;

	TMap<APawn*, TArray<FVector>> PropsLocationsMap;

	UPROPERTY()
	TMap<APawn*, class APropsPathVisualiser*> PropsPathMap;

	bool bVisualizePropsPath;

	/** Number of stored Props locations. */
	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Props Path Visualization")
	int32 PathAmount;

	/** How many steps to retreat from Props when rendering path. */
	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Props Path Visualization")
	float PathOffset;

	UFUNCTION(BlueprintCallable, Category = "Abilities|Props Path Visualization")
	void TrackPropsPath();

	UFUNCTION(BlueprintCallable, Category = "Abilities|Props Path Visualization")
	void ShowPropsPath(bool bEnable);

	void EmptyPropsPathMap();

	UFUNCTION(BlueprintCallable, Category = "HunterCharacter")
	bool IsStandingOnFloor();

	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Cloak")
	class UCurveFloat* CloakCurve ;

	UPROPERTY()
	class UTimelineComponent* CloakTimeline ;

	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Cloak")
	class UMaterialInterface* CloakMaterial ;

	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Cloak")
	FName CloakMaterialParameter;

	UPROPERTY()
	TArray<USkeletalMeshComponent*> CloakMeshes;

	UPROPERTY(ReplicatedUsing = OnRep_Invisible)
	bool bInvisible;

	UFUNCTION(BlueprintCallable, Category = "Abilities|Cloak")
	void ActivateCloakMode(bool bEnable);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerActivateCloakMode(bool bEnable);

	void SetInvisible(bool bInInvisible);

	UFUNCTION()
	void OnRep_Invisible();

	USkeletalMeshComponent* CreateCloakMesh(USkeletalMeshComponent* Template);

	UFUNCTION(BlueprintCallable, Category = "Abilities|Cloak")
	void OnUpdateCloakAnimation(float Output);

	UFUNCTION(BlueprintCallable, Category = "Abilities|Cloak")
	void OnFinishedCloakAnimation();

	void Debug();

	virtual void InitAbilitySystemComponent() override;

	void OnAbilityActivationFailed(const class UGameplayAbility* FailedAbility, const FGameplayTagContainer& FailTags);

	void InitCustomWeapon();

public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Transportation")
	void ShowTransportationEffect(bool bStart);
};
