// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PropCharacterMovement.generated.h"


DECLARE_STATS_GROUP(TEXT("PropCharacter"), STATGROUP_PropCharacter, STATCAT_Advanced);

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPropCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UPropCharacterMovement();

protected:
	/** Character movement component belongs to */
	UPROPERTY(Transient, DuplicateTransient)
	class APropCharacter* PropCharacterOwner;

public:
	/**
	 * Don't allow the character to perch on the edge of a surface if the contact is this close to the edge of the capsule.
	 * Note that characters will not fall off if they are within MaxStepHeight of a walkable surface below.
	 */
	UPROPERTY(Category = "PropPropCharacter Movement: Walking", EditAnywhere, BlueprintReadWrite, AdvancedDisplay, meta = (ClampMin = "0", UIMin = "0"))
	FVector2D PerchDepthWidthThreshold;

	/** Uses when Use Controller Rotation is turned on. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PropCharacter Movement (Rotation Settings)")
	float FaceRotationRate;

private:
	// Tracks whether or not we need to update the bSweepWhileNavWalking flag do to an upgrade.
	bool bNeedsSweepWhileWalkingUpdate;

public:
	/**
	 * Shrink simulated proxy collision depth (radius for capsule) by this amount, to account for network rounding that may cause encroachment. Changing during gameplay is not supported.
	 * @see AdjustProxyCollisionSize()
	 */
	UPROPERTY(Category = "PropCharacter Movement (Networking)", EditDefaultsOnly, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float NetProxyShrinkDepth;

	/**
	 * Shrink simulated proxy collision width by this amount, to account for network rounding that may cause encroachment. Changing during gameplay is not supported.
	 * @see AdjustProxyCollisionSize()
	 */
	UPROPERTY(Category = "PropCharacter Movement (Networking)", EditDefaultsOnly, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float NetProxyShrinkWidth;

public:
	// Begin UObject Interface
	void Serialize(FArchive& Archive) override;
	// End UObject Interface

	//Begin UActorComponent Interface
	void PostLoad() override;
	//End UActorComponent Interface

	FORCEINLINE class APropCharacter* GetPropCharacterOwner() const { return PropCharacterOwner; }

public:
	void UpdateBasedMovement(float DeltaSeconds) override;

	FVector GetImpartedMovementBaseVelocity() const override;

	bool StepUp(const FVector& GravDir, const FVector& Delta, const FHitResult& Hit, struct UCharacterMovementComponent::FStepDownResult* OutStepDownResult = NULL) override;

	void ApplyRepulsionForce(float DeltaSeconds) override;

	void PhysFalling(float deltaTime, int32 Iterations) override;

public:
	/**
	 * Return true if the 2D distance to the impact point is inside the edge tolerance (CapsuleRadius minus a small rejection threshold).
	 * Useful for rejecting adjacent hits when finding a floor or landing spot.
	 */
	bool IsWithinEdgeTolerance_PH(const FVector& CollisionLocation, const FVector& TestImpactPoint, const struct FShapeExtent& CollisionSize) const;

	virtual bool MorphInto(const struct FMorphingData& MorphingData, bool bClientSimulation = false);

	bool CheckLedgeDirection(const FVector& OldLocation, const FVector& SideStep, const FVector& GravDir) const override;

	void PhysicsRotation(float DeltaTime) override;

	/** Perform rotation from input NewRotation. */
	virtual void SetCharacterRotation(FRotator DesiredRotation, float DeltaTime);

	virtual void RotateUpdatedComponent(FQuat DesiredRotation);

	FVector LineTraceForPenetrationAdjustment(const FVector& StartPoint, const FVector& EndPoint, const FQuat& Rot, ECollisionChannel TraceChannel, const FCollisionQueryParams& Params, const FCollisionResponseParams& ResponseParam);

	void SetUpdatedComponent(USceneComponent* NewUpdatedComponent) override;

	/** Returns The distance from the edge of the capsule within which we don't allow the character to perch on the edge of a surface. */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PropCharacterMovement")
	FVector2D GetPerchDepthWidthThreshold() const;

	/**
	 * Returns the radius within which we can stand on the edge of a surface without falling (if this is a walkable surface).
	 * Simply computed as the capsule radius minus the result of GetPerchRadiusThreshold().
	 */
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PropCharacterMovement")
	FVector2D GetValidPerchDepthWidth() const;

protected:
	void SetNavWalkingPhysics(bool bEnable) override;

public:
	void FindFloor(const FVector& CapsuleLocation, FFindFloorResult& OutFloorResult, bool bCanUseCachedLocation, const FHitResult* DownwardSweepResult = NULL) const override;

	/**
	 * Compute distance to the floor from bottom sphere of capsule and store the result in OutFloorResult.
	 * This distance is the swept distance of the capsule to the first point impacted by the lower hemisphere, or distance from the bottom of the capsule in the case of a line trace.
	 * This function does not care if collision is disabled on the capsule (unlike FindFloor).
	 * @see FindFloor
	 *
	 * @param CollisionLocation:	Location of the shape used for the query
	 * @param LineDistance:		If non-zero, max distance to test for a simple line check from the capsule base. Used only if the sweep test fails to find a walkable floor, and only returns a valid result if the impact normal is a walkable normal.
	 * @param SweepDistance:	If non-zero, max distance to use when sweeping a capsule downwards for the test. MUST be greater than or equal to the line distance.
	 * @param OutFloorResult:	Result of the floor check. The HitResult will contain the valid sweep or line test upon success, or the result of the sweep upon failure.
	 * @param SweepRadius:		The radius to use for sweep tests. Should be <= capsule radius.
	 * @param DownwardSweepResult:	If non-null and it contains valid blocking hit info, this will be used as the result of a downward sweep test instead of doing it as part of the update.
	 */
	virtual void ComputeFloorDist_PH(const FVector& CollisionLocation, float LineDistance, float SweepDistance, FFindFloorResult& OutFloorResult, const FVector2D& SweepRadius, const FHitResult* DownwardSweepResult = NULL) const;

	/**
	* Compute distance to the floor from bottom sphere of capsule and store the result in FloorResult.
	* This distance is the swept distance of the capsule to the first point impacted by the lower hemisphere, or distance from the bottom of the capsule in the case of a line trace.
	* This function does not care if collision is disabled on the capsule (unlike FindFloor).
	*
	* @param CapsuleLocation		Location where the capsule sweep should originate
	* @param LineDistance			If non-zero, max distance to test for a simple line check from the capsule base. Used only if the sweep test fails to find a walkable floor, and only returns a valid result if the impact normal is a walkable normal.
	* @param SweepDistance			If non-zero, max distance to use when sweeping a capsule downwards for the test. MUST be greater than or equal to the line distance.
	* @param SweepRadius			The radius to use for sweep tests. Should be <= capsule radius.
	* @param FloorResult			Result of the floor check
	*/
	UFUNCTION(BlueprintCallable, Category = "Pawn|Components|PropCharacterMovement", meta = (DisplayName = "ComputeFloorDistance_PH", ScriptName = "ComputeFloorDistance_PH"))
	virtual void K2_ComputeFloorDist_PH(FVector CapsuleLocation, float LineDistance, float SweepDistance, FVector2D SweepRadius, FFindFloorResult& FloorResult) const;

	/**
	 * Sweep against the world and return the first blocking hit.
	 * Intended for tests against the floor, because it may change the result of impacts on the lower area of the test (especially if bUseFlatBaseForFloorChecks is true).
	 *
	 * @param OutHit			First blocking hit found.
	 * @param Start				Start location of the capsule.
	 * @param End				End location of the capsule.
	 * @param Rot				Rotation of the box (it's used only if we have box collision).
	 * @param TraceChannel		The 'channel' that this trace is in, used to determine which components to hit.
	 * @param CollisionShape	Capsule collision shape.
	 * @param Params			Additional parameters used for the trace.
	 * @param ResponseParam		ResponseContainer to be used for this trace.
	 * @return True if OutHit contains a blocking hit entry.
	 */
	virtual bool FloorSweepTestWithRotation(
		struct FHitResult& OutHit,
		const FVector& Start,
		const FVector& End,
		const FQuat& Rot,
		ECollisionChannel TraceChannel,
		const struct FCollisionShape& CollisionShape,
		const struct FCollisionQueryParams& Params,
		const struct FCollisionResponseParams& ResponseParam
	) const;

	bool IsValidLandingSpot(const FVector& CapsuleLocation, const FHitResult& Hit) const override;

	bool ShouldCheckForValidLandingSpot(float DeltaTime, const FVector& Delta, const FHitResult& Hit) const override;

	bool ShouldComputePerchResult(const FHitResult& InHit, bool bCheckRadius = true) const override;

	/**
	 * Compute the sweep result of the smaller capsule with radius specified by GetValidPerchRadius(),
	 * and return true if the sweep contacts a valid walkable normal within InMaxFloorDist of InHit.ImpactPoint.
	 * This may be used to determine if the capsule can or cannot stay at the current location if perched on the edge of a small ledge or unwalkable surface.
	 * Note: Only returns a valid result if ShouldComputePerchResult returned true for the supplied hit value.
	 *
	 * @param TestRadius:			Radius to use for the sweep, usually GetValidPerchRadius().
	 * @param InHit:				Result of the last sweep test before the query.
	 * @param InMaxFloorDist:		Max distance to floor allowed by perching, from the supplied contact point (InHit.ImpactPoint).
	 * @param OutPerchFloorResult:	Contains the result of the perch floor test.
	 * @return True if the current location is a valid spot at which to perch.
	 */
	virtual bool ComputePerchResult_PH(const FVector2D& TestRadius, const FHitResult& InHit, const float InMaxFloorDist, FFindFloorResult& OutPerchFloorResult) const;

	class FNetworkPredictionData_Client* GetPredictionData_Client() const override;

protected:
	/** Get the capsule extent for the Pawn owner, possibly reduced in size depending on ShrinkMode.
	 * @param ShrinkMode			Controls the way the capsule is resized.
	 * @param CustomShrinkAmount	The amount to shrink the capsule, used only for ShrinkModes that specify custom.
	 * @return The capsule extent of the Pawn owner, possibly reduced in size depending on ShrinkMode.
	 */
	FVector GetPawnCollosionExtent(const EShrinkCapsuleExtent ShrinkMode, const float CustomShrinkAmount = 0.f) const;

	/** Get the collision shape for the Pawn owner, possibly reduced in size depending on ShrinkMode.
	 * @param ShrinkMode			Controls the way the capsule is resized.
	 * @param CustomShrinkAmount	The amount to shrink the capsule, used only for ShrinkModes that specify custom.
	 * @return The capsule extent of the Pawn owner, possibly reduced in size depending on ShrinkMode.
	 */
	FCollisionShape GetPawnCollisionShape(const EShrinkCapsuleExtent ShrinkMode, const float CustomShrinkAmount = 0.f) const;

	void AdjustProxyCapsuleSize() override;

protected:
	void ReplicateMoveToServer(float DeltaTime, const FVector& NewAcceleration) override;

	bool ClientUpdatePositionAfterServerUpdate() override;

public:
	void CalcAvoidanceVelocity(float DeltaTime) override;

	/** BEGIN IRVOAvoidanceInterface */
	float GetRVOAvoidanceRadius() override;
	float GetRVOAvoidanceHeight() override;
	/** END IRVOAvoidanceInterface */
};


class PROPHUNT_API FSavedMove_PropCharacter : public FSavedMove_Character
{
public:
	void SetInitialPosition(ACharacter* C) override;
};


class PROPHUNT_API FNetworkPredictionData_Client_PropCharacter : public FNetworkPredictionData_Client_Character
{
public:
	FNetworkPredictionData_Client_PropCharacter(const UPropCharacterMovement& ClientMovement);

	FSavedMovePtr AllocateNewMove() override;
};
