﻿#pragma once

#include "CoreMinimal.h"
#include "ShapeExtent.generated.h"

USTRUCT()
struct PROPHUNT_API FShapeExtent
{
	GENERATED_USTRUCT_BODY()

	FShapeExtent() = default;
	FShapeExtent(float Depth, float Width, float Height);
	FShapeExtent(float Radius, float HalfHeight);
	explicit FShapeExtent(const FVector& Extent);

	float GetRadius() const { return GetHalfDepth(); }
	float GetHalfDepth() const { return Size.X; }
	float GetHalfWidth() const { return Size.Y; }
	float GetHalfHeight() const { return Size.Z; }
	FVector2D GetHalf2DSize() const { return FVector2D(Size); }

	bool operator==(const FShapeExtent& Other) const { return Size.Equals(Other.Size); }
	bool operator!=(const FShapeExtent& Other) const { return !(*this == Other); }

	static const FShapeExtent ZeroSize;

	FVector Size;
};
