// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Character/PHCharacterBase.h"
#include "Core/Online/PHCustomizationHttpClient.h"
#include "PropCharacter.generated.h"

class UGameplayEffectUIData;

USTRUCT()
struct FMorphingData
{
	GENERATED_BODY()

	UPROPERTY()
	UStaticMesh* NewMesh;

	UPROPERTY()
	TArray<UMaterialInterface*> NewMaterials;

	UPROPERTY()
	uint8 NewShapeType;

	UPROPERTY()
	FVector NewExtent;

	UPROPERTY()
	float NewScale;

	UPROPERTY()
	float CurrentHeightOffset;

	UPROPERTY()
	float CurrentHalfHeight;

	FMorphingData() :
		NewMesh(nullptr),
		NewShapeType(3),
		NewExtent(1.f),
		NewScale(1.f),
		CurrentHeightOffset(0),
		CurrentHalfHeight(1.f)
	{}

	FORCEINLINE ECollisionShape::Type GetNewShapeType() const { return static_cast<ECollisionShape::Type>(NewShapeType); }

	FORCEINLINE void SetNewShapeType(ECollisionShape::Type NewType) { NewShapeType = static_cast<uint8>(NewType); }
};


/**
 * 
 */
UCLASS()
class PROPHUNT_API APropCharacter : public APHCharacterBase
{
	GENERATED_BODY()

public:
	APropCharacter(const FObjectInitializer& ObjectInitializer);

	//~ Begin APawn Interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void UpdateNavigationRelevance() override;
	virtual void FaceRotation(FRotator NewControlRotation, float DeltaTime = 0.f) override;
	//~ End APawn Interface

	//~ Begin AActor Interface
	virtual void Tick(float DeltaSeconds) override;
	//~ End AActor Interface

	void ApplyDamageMomentumWithHit(float DamageImpulse, const FHitResult& HitInfo, bool bScaleMomentumByMass = false);

	//~ Begin ICharacterInterface Interface
	virtual void GetPlayerViewPoint(FVector& OutLocation, FRotator& OutRotation) const override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMeshComponent;

	/** The BoxComponent being used for movement collision (by CharacterMovement). Always treated as being vertically aligned in simple collision check functions. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PropCharacter", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* BoxComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PropCharacter", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PropCharacter", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PropCharacter", meta = (AllowPrivateAccess = "true"))
	class UNiagaraComponent* BodyNiagara;

	/** AkComponent to handle playback */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AkAmbientSound", meta = (ShowOnlyInnerProperties))
	class UAkComponent* AkComponent;

	UPROPERTY()
	UStaticMeshComponent* MorphMeshComponentHelper;

public:
	class UPropCharacterMovement* GetPropCharacterMovement() const;

	FORCEINLINE UStaticMeshComponent* GetStaticMeshComp() const { return StaticMeshComponent; }

	/** Returns CapsuleComponent subobject. **/
	FORCEINLINE class UBoxComponent* GetBoxComponent() const { return BoxComponent; }

	/** Returns current collision subject (in root). */
	UPrimitiveComponent* GetCollisionComponent() const;

	ECollisionShape::Type CollisionType;

	/** Returns collision component type **/
	ECollisionShape::Type GetCollisionType() const { return CollisionType; }

	/** Compute size of capsule or box components of the CharacterOwner */
	struct FShapeExtent GetCollisionSize() const;

	void SetCollisionSize(FVector InExtent, bool bUpdateOverlaps = true);

	/** Get the scale used by the current collision. */
	float GetShapeScale() const;

	/** Change root collision to Box, Capsule etc. */
	void SwitchCollisionShape(ECollisionShape::Type NewCollisionType);

protected:
	UPROPERTY(ReplicatedUsing = OnRep_MorphingData)
	FMorphingData MorphingData;

	UFUNCTION()
	void OnRep_MorphingData();

	/** Perform changing of the mesh to another model as a part of the morphing process. */
	virtual void SetStaticMesh(UStaticMesh* NewMesh, const FVector& Scale, const TArray<UMaterialInterface*>& Materials);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//////////////////////////////////////////////////////////////////////////
	// Input handlers

	void MoveForward(float Value);

	void MoveRight(float Value);

	/** Player pressed next or prev weapon action */
	void OnChangeWeapon(bool bNext);

	void OnStartTargeting();

	void OnStopTargeting();

public:
	//////////////////////////////////////////////////////////////////////////
	// Reading data

	UFUNCTION(BlueprintCallable, Category = "PropCharacter|Attributes")
	float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "PropCharacter|Attributes")
	void SetHealth(float Health);

	UFUNCTION(BlueprintCallable, Category = "PropCharacter|Attributes")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "PropCharacter|Attributes")
	void SetMaxHealth(float MaxHealth);

	/** Check if pawn is still alive */
	UFUNCTION(BlueprintCallable, Category = "PropCharacter")
	bool IsAlive() const;

	/** Returns percentage of health when low health effects should start */
	float GetLowHealthPercentage() const;

	/** Respond to the damage */
	virtual void HandleDamage(AController* EventInstigator, AActor* DamageCauser);

	/**
	* Kills pawn.  Server/authority only.
	* @param DamageAmount - Damage amount of the killing blow
	* @param UIData - Damage UI data of the killing blow
	* @param Killer - Who killed this pawn
	* @param DamageCauser - the Actor that directly caused the damage (i.e. the Projectile that exploded, the Weapon that fired, etc)
	* @returns true if allowed
	*/
	virtual void Die(float DamageAmount, UGameplayEffectUIData* UIData, AController* Killer, class AActor* DamageCauser);

protected:
	/** Notification when killed, for both the server and client. */
	virtual void OnDeath(float DamageAmount, class APawn* InstigatingPawn, class AActor* DamageCauser) override;

	/** When low health effects should start */
	float LowHealthPercentage;

	virtual void KilledBy(class APawn* EventInstigator) override;

	void FixBodyRotation(bool bEnable);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFixBodyRotation(bool bEnable);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Morphing")
	class USoundBase* MorphingSound;

	UPROPERTY(EditDefaultsOnly, Category = "Abilities|Morphing")
	class UNiagaraSystem* MorphingFX;

	UFUNCTION(BlueprintImplementableEvent, Category = "Abilities|Morphing")
	void UpdateCameraBoomPosition(float TargetHalfHeight);

	float DischargeAngle = 45.f;

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool CheckHunterToDischarge(AActor* InHunterToDischarge);

	//////////////////////////////////////////////////////////////////////////
	// Color customization
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
	bool bUseCustomBodyColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
	class UCurveLinearColor* BodyColorCurve;

	float BodyColorLerpFactor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance", meta = (ClampMin = 0.1f, ClampMax = 1.f))
	float BodyColorLerpSpeed;

	UPROPERTY(ReplicatedUsing = OnRep_ColorScheme)
	FPropColorScheme ColorScheme;

	UFUNCTION()
	void OnRep_ColorScheme();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSetColorScheme(FPropColorScheme InColorScheme);

	void ApplyColorScheme();

	//////////////////////////////////////////////////////////////////////////
	// Morphing
	
public:
	UFUNCTION(BlueprintCallable, Category = "Ability|Morphing")
	bool MorphInto(UPrimitiveComponent* SampleComponent);

	UFUNCTION(BlueprintCallable, Category = "Ability|Morphing")
	bool CanMorph(UPrimitiveComponent* SampleComponent);

	void OnEndMorphing(float NewMeshHeightOffset);

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void SetJumpMaxCount(int32 NewValue);

protected:
	float DefaultCameraArmLength;
	float TargetCameraArmLength;

	bool bUpdateCameraArmLength;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	float MinCameraArmLength;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	float CameraArmLengthChangeStep;

	void ChangeCameraArmLength(bool bDecrease);
};
