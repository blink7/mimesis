// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "PHPlayerCameraManager.generated.h"


namespace ECameraMode
{
	extern const FName NAME_ThirdPerson;
	extern const FName NAME_FirstPerson;
}

/**
 * 
 */
UCLASS()
class PROPHUNT_API APHPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_UCLASS_BODY()
	
public:
	/** Minimum view pitch for third person camera style, in degrees. */
	float FirstPersonViewPitchMin;

	/** Maximum view pitch for third person camera style, in degrees. */
	float FirstPersonViewPitchMax;

	/** Minimum view pitch for third person camera style, in degrees. */
	float ThirtPersonViewPitchMin;

	/** Maximum view pitch for third person camera style, in degrees. */
	float ThirtPersonViewPitchMax;

	void PostInitializeComponents() override;

	UFUNCTION(BlueprintCallable, Category = "PlayerCameraManager")
	void SetFirstPersonFOV(float InFOV);

	float GetFirstPersonFOV() const { return DefaultFirstPersonFOV; }

	UFUNCTION(BlueprintCallable, Category = "PlayerCameraManager")
	void SetThirdPersonFOV(float InFOV);

	float GetThirdPersonFOV() const { return DefaultThirdPersonFOV; }

	void UpdateCameraSettings(FName InCameraMode);

protected:
	UPROPERTY(BlueprintReadOnly, Category = "PlayerCameraManager")
	float DefaultFirstPersonFOV;

	UPROPERTY(BlueprintReadOnly, Category = "PlayerCameraManager")
	float DefaultThirdPersonFOV;

	float NormalFOV;

	FName CameraMode;

	void OnChangeFirstPersonFOV(IConsoleVariable* Var);

	void OnChangeThirdPersonFOV(IConsoleVariable* Var);
};
