// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Core/PHTypes.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "PHCharacterBase.generated.h"


UCLASS(Abstract)
class PROPHUNT_API APHCharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	APHCharacterBase(const FObjectInitializer& ObjectInitializer);
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void PreReplication(IRepChangedPropertyTracker& ChangedPropertyTracker) override;
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void OnRep_PlayerState() override;
	void PossessedBy(AController* NewController) override;

	// Implement IAbilitySystemInterface
	class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	/* Pawn suicide */
	virtual void Suicide();

	/** Kill this pawn */
	virtual void KilledBy(class APawn* EventInstigator) {};

	/** Returns True if the pawn can die in the current state */
	virtual bool CanDie() const;

	UFUNCTION(BlueprintCallable, Category = "Character")
	virtual void GetPlayerViewPoint(FVector& OutLocation, FRotator& OutRotation) const;

	UFUNCTION(BlueprintCallable, Category = "CharacterMovement")
	class UPrimitiveComponent* GetStandOnComponent() const;

	void SetIndicatorVisibility(bool bNewVisibility);

	//////////////////////////////////////////////////////////////////////////
	// Movement

	/** Player pressed run action */
	UFUNCTION(BlueprintCallable, Category = "CharacterMovement")
	void OnStartRunning(bool bEnable);

	/** Player pressed toggled run action */
	UFUNCTION(BlueprintCallable, Category = "CharacterMovement")
	void OnStartRunningToggle();

	UFUNCTION(BlueprintCallable, Category = "CharacterMovement")
	bool IsRunning() const;

	UFUNCTION(BlueprintCallable, Category = "CharacterMovement")
	void SetMaxValkSpeed(float NewMaxSpeed);

protected:
	/** Identifies if pawn is in its dying state */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	bool bDying;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	float BaseTurnRate;

	/** Base lookup rate, in deg/sec. Other scaling may affect final lookup rate. */
	float BaseLookUpRate;

	/** Current running state */
	UPROPERTY(Transient, Replicated)
	bool bWantsToRun;

	/** From gamepad running is toggled */
	bool bWantsToRunToggled;

	/** [server + local] Change running state */
	void SetRunning(bool bNewRunning, bool bToggle = false);

	/** Update running state */
	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetRunning(bool bNewRunning, bool bToggle);
	void ServerSetRunning_Implementation(bool bNewRunning, bool bToggle);
	bool ServerSetRunning_Validate(bool bNewRunning, bool bToggle);

	void OnRotateItem(bool bEnable);

	void OnZoomMinimap();

	//////////////////////////////////////////////////////////////////////////
	// Ability System

	TWeakObjectPtr<class UPHAbilitySystemComponent> AbilitySystemComponent;
	TWeakObjectPtr<class UPHAttributeSet> AttributeSetBase;

	// Default effects for a character for initializing on spawn.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<class UGameplayEffect>> PassiveGameplayEffects;

	/** Abilities to grant to this character on creation. These will be activated by tag or event and are not bound to specific inputs */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<class UPHGameplayAbility>> GameplayAbilities;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "PropCharacter")
	TSubclassOf<UGameplayEffect> DeathEffect;

	FGameplayTag DeadTag;

	bool bAbilitiesInputBound;

	virtual void InitAbilitySystemComponent();

	void BindAbilityInput(class UInputComponent* PlayerInputComponent);

public:
	void RemoveAbilities();

	UFUNCTION(BlueprintCallable, Category = "Character|Attributes")
	int32 GetCharacterLevel() const;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PickUp")
	class UGrabComponent* Grabber;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HUD")
	class UPlayerIndicatorComponent* PlayerIndicator;

	//////////////////////////////////////////////////////////////////////////
	// Input handlers

	void AddControllerPitchInput(float Val) override;

	void AddControllerYawInput(float Val) override;

	/* Frame rate independent turn */
	void TurnAtRate(float Value);

	/* Frame rate independent lookup */
	void LookUpAtRate(float Value);

	/** Notification when killed, for both the server and client. */
	virtual void OnDeath(float DamageAmount, class APawn* InstigatingPawn, class AActor* DamageCauser) {};

	/** sets up the replication for taking a hit */
	void ReplicateHit(float DamageAmount, APawn* InstigatingPawn, AActor* DamageCauser, bool bKilled);

	/** Play hit or death on client */
	UFUNCTION()
	void OnRep_LastTakeHitInfo();

	/** Replicate where this pawn was last hit and damaged */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_LastTakeHitInfo)
	struct FTakeHitInfo LastTakeHitInfo;

	/** Time at which point the last take hit info for the actor times out and won't be replicated; Used to stop join-in-progress effects all over the screen */
	float LastTakeHitTimeTimeout;
};
