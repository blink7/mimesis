// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "PlayerIndicatorComponent.generated.h"


/**
 * 
 */
UCLASS(Blueprintable, meta = (BlueprintSpawnableComponent))
class PROPHUNT_API UPlayerIndicatorComponent : public UWidgetComponent
{
	GENERATED_UCLASS_BODY()
	
public:
	void SetIndicatorVisibility(bool bNewVisibility);
};
