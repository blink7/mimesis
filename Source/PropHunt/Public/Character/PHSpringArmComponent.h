// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "PHSpringArmComponent.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHSpringArmComponent : public USpringArmComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	FRotator AngleOffset;

	UFUNCTION(BlueprintCallable, Category = "SpringArm")
	FRotator GetTargetRotationWithOffset() const;

protected:
	/** Updates the desired arm location, calling BlendLocations to do the actual blending if a trace is done */
	void UpdateDesiredArmLocation(bool bDoTrace, bool bDoLocationLag, bool bDoRotationLag, float DeltaTime) override;
};
