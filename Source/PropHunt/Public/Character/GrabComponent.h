// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GrabComponent.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogGrabComponent, Log, All);

USTRUCT()
struct FGrabbingMove
{
	GENERATED_BODY()

	UPROPERTY()
	FVector_NetQuantize TargetLocation;

	UPROPERTY()
	FRotator TargetRotation;

	bool IsEmpty() const { return TargetLocation == FVector_NetQuantize::ZeroVector; }

	FGrabbingMove() : 
		TargetLocation(FVector_NetQuantize::ZeroVector),
		TargetRotation(FRotator::ZeroRotator)
	{}
};

USTRUCT()
struct FGrabbingData
{
	GENERATED_BODY()

	UPROPERTY()
	UPrimitiveComponent* PickedUpComponent;

	UPROPERTY()
	FVector_NetQuantize Impulse;

	FGrabbingData() : 
		PickedUpComponent(nullptr),
		Impulse(FVector_NetQuantize::ZeroVector)
	{}
};


/**
 * GrabComponent is a functional component adding possibility to a character 
 * to pick up movable items (marked with actor tag "Pickup").
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROPHUNT_API UGrabComponent : public UPhysicsHandleComponent
{
	GENERATED_BODY()

public:
	UGrabComponent();

	//Begin UActorComponent Interface
	void BeginPlay() override;
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	//End UActorComponent Interface

	void SimulatedTick(float DeltaTime);

private:
	// Pawn that owns this component
	UPROPERTY()
	class APHCharacterBase* CharacterOwner;

	float DistanceToItem;

public:
	// Maximum mass of a pickable item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float MaxMass;

	// Magnitude of impulse applied to the throwed item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float ThrowImpulse;

	// Distance from the player on which the picked up item holds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float HoldDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float MinHoldDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float MaxHoldDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float MaxDropVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float CustomYawRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Adjust")
	float DistanceStep;

private:
	UPROPERTY(Replicated)
	FGrabbingData GrabbingData;

	UPROPERTY(Replicated)
	FGrabbingMove Move;

	// Whether the player holds an item
	bool bHoldItem;

	float CustomYawRotation;

	// Creates new move to position the picked up item to target location. Returns false if can't create one.
	void CreateMove(float DeltaTime);

	void SetMove(const FGrabbingMove& NewMove);

public:
	UFUNCTION(BlueprintCallable, Category = "Ability")
	bool Pickup(UPrimitiveComponent* ComponentToGrab);

	UFUNCTION(BlueprintCallable, Category = "Ability")
	void Drop(bool bThrow = false);

	// Rotate picked up item about its Z-axis.
	void AddYawRotation(float Value);

	UFUNCTION(BlueprintCallable, Category = "Ability")
	bool IsHoldingItem() { return bHoldItem; }

	void AdjustHoldDistance(bool bIncrease);

	bool bRotateItem;

private:
	void PerformGrab(UPrimitiveComponent* ComponentToGrab);

	void PerformDrop();

private:
	UFUNCTION(Server, Unreliable, WithValidation)
	void ServerSendMove(FGrabbingMove NewMove);
	void ServerSendMove_Implementation(FGrabbingMove NewMove);
	bool ServerSendMove_Validate(FGrabbingMove NewMove);
};
