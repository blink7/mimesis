// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "PHPersistentUser.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHPersistentUser : public USaveGame
{
	GENERATED_UCLASS_BODY()
	
public:
	/** Loads user persistence data if it exists, creates an empty record otherwise. */
	static UPHPersistentUser* LoadPersistentUser(FString SlotName, const int32 UserIndex);

	/** Saves data if anything has changed. */
	void SaveIfDirty();

	/** needed because we can recreate the subsystem that stores it */
	void TellInputAboutKeybindings();

	int32 GetUserIndex() const { return UserIndex; };

	/** Is controller vibration turned on? */
	bool GetVibration() const { return bVibration; }

	/** Is the y axis inverted? */
	bool GetInvertedYAxis() const { return bInvertedYAxis; }

	/** Setter for controller vibration option */
	void SetVibration(bool bInVibration);

	/** Setter for inverted y axis */
	void SetInvertedYAxis(bool bInvert);

	/** Getter for the aim sensitivity */
	float GetAimSensitivity() const { return AimSensitivity; }

	void SetAimSensitivity(float InSensitivity);

	bool IsRecordingDemos() const { return bRecordingDemos; }

	void SetIsRecordingDemos(const bool InbIsRecordingDemos);

	FString GetName() const { return SlotName; }

	void SetToDefaults();

protected:
	/** Checks if the Mouse Sensitivity user setting is different from current */
	bool IsAimSensitivityDirty() const;

	/** Checks if the Inverted Mouse user setting is different from current */
	bool IsInvertedYAxisDirty() const;

	/** Triggers a save of this data. */
	void SavePersistentUser();

	UPROPERTY()
	bool bRecordingDemos;

	/** Holds the mouse sensitivity */
	UPROPERTY()
	float AimSensitivity;

	/** Is the y axis inverted or not? */
	UPROPERTY()
	bool bInvertedYAxis;

	UPROPERTY()
	bool bVibration;

private:
	/** Internal. True if data is changed but hasn't been saved. */
	bool bDirty;

	/** The string identifier used to save/load this persistent user. */
	FString SlotName;

	int32 UserIndex;
};
