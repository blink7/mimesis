// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameplayAbilitySpec.h"
#include "Abilities/PHGameplayEffectTypes.h"
#include "../../Wwise/Source/AkAudio/Classes/AkMediaAsset.h"
#include "PHBlueprintFunctionLibrary.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	/**
	* FGameplayAbilitySpecHandle
	*/
	UFUNCTION(BlueprintPure, Category = "Ability")
	static bool IsAbilitySpecHandleValid(const FGameplayAbilitySpecHandle& Handle);

	/** Gets the source object of the GameplayCue */
	UFUNCTION(BlueprintPure, Category = "Ability|GameplayCue")
	static UObject* GetSourceObject(const FGameplayCueParameters& Parameters);

	/** Adds targets to a copy of the passed in effect container spec and returns it */
	UFUNCTION(BlueprintCallable, Category = "Ability", meta = (AutoCreateRefTerm = "HitResults,TargetActors"))
	static FPHGameplayEffectContainerSpec AddTargetsToEffectContainerSpec(const FPHGameplayEffectContainerSpec& ContainerSpec, const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors);

	/** Applies container spec that was made from an ability */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	static TArray<FActiveGameplayEffectHandle> ApplyExternalEffectContainerSpec(const FPHGameplayEffectContainerSpec& ContainerSpec);

	UFUNCTION(BlueprintPure, Category = "Ability|TargetData")
	static void GetHitActorFromTargetData(const FGameplayAbilityTargetDataHandle& TargetData, int32 Index, AActor*& HitActor, UPrimitiveComponent*& HitComponent);

	UFUNCTION(BlueprintPure, Category = "Game")
	static FString GetGameVersion();
	
	UFUNCTION(BlueprintPure, Category = "Input")
	static FText GetKeyName(FName InActionName);

	UFUNCTION(BlueprintPure, Category = "AssetManager")
	static FString GetAssetName(TSoftObjectPtr<UObject> SoftObjectReference);

	UFUNCTION(BlueprintPure, Category = "Ability|EffectContext", Meta = (DisplayName = "Get External Media Asset"))
	static TSoftObjectPtr<UAkExternalMediaAsset> EffectContextGetExternalMediaAsset(FGameplayEffectContextHandle EffectContextHandle);

	UFUNCTION(BlueprintPure, Category = "Ability|TargetData")
	static FGameplayAbilityTargetDataHandle AbilityTargetDataFromExternalMediaAsset(const TSoftObjectPtr<UAkExternalMediaAsset>& ExternalMediaAsset);

	UFUNCTION(BlueprintPure, Category = "Ability|TargetData")
	static TSoftObjectPtr<UAkExternalMediaAsset> GetExternalMediaAssetFromTargetData(const FGameplayAbilityTargetDataHandle& TargetData, int32 Index);
};
