﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManagerTypes.h"
#include "PHTauntDataAsset.generated.h"


/**
 * 
 */
UCLASS(BlueprintType)
class PROPHUNT_API UPHTauntDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()
	
public:
	UPHTauntDataAsset();
	
	/** Management rules for this specific asset, if set it will override the type rules */
	UPROPERTY(EditAnywhere, Category = "Rules", meta = (ShowOnlyInnerProperties))
	FPrimaryAssetRules Rules;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Taunt", AssetRegistrySearchable)
	FName Language;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Taunt", meta = (AssetBundles = "TauntBundle"))
	TArray<TSoftObjectPtr<class UAkExternalMediaAsset>> TauntAssets;
	
#if WITH_EDITORONLY_DATA
	/** This scans the class for AssetBundles metadata on asset properties and initializes the AssetBundleData with InitializeAssetBundlesFromMetadata */
	virtual void UpdateAssetBundleData() override;
#endif
};
