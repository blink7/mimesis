// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Items/PHItem.h"
#include "PHSkillItem.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHSkillItem : public UPHItem
{
	GENERATED_BODY()
	
public:
	UPHSkillItem();
};
