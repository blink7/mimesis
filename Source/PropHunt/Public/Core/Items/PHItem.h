// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "PHItem.generated.h"


UENUM()
enum class EItemSuitableTeam : uint8
{
	Both,
	Hunter,
	Prop
};

UENUM(BlueprintType)
enum class EItemUsageType : uint8
{
	Energy,
	Amount,
	Passive
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHItem : public UPrimaryDataAsset
{
	GENERATED_BODY()
	
public:
	/** Type of this item, set in native parent class */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item")
	FPrimaryAssetType ItemType;

	/** User-visible short name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	FText ItemName;

	/** User-visible long description */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	FText ItemDescription;

	/** Which team can use this item */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	EItemSuitableTeam SuitableTeam;

	/** What type of cost does this item use */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	EItemUsageType UsageType;

	/** Abilities to grant if this item is slotted */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<class UPHGameplayAbility>> GrantedAbilities;

	/** Ability level this item grants. <= 0 means the character level */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	int32 AbilityLevel;

	/** Required character level to use this item */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	int32 RequiredLevel;

	/** Returns the logical name, equivalent to the primary asset id */
	UFUNCTION(BlueprintCallable, Category = "Item")
	FString GetIdentifierString() const;

	/** Overridden to use saved type */
	FPrimaryAssetId GetPrimaryAssetId() const override;
};
