// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Items/PHItem.h"
#include "PHWeaponItem.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHWeaponItem : public UPHItem
{
	GENERATED_BODY()

public:
	UPHWeaponItem();

	/** Weapon actor to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<class AWeapon> WeaponActorClass;
};
