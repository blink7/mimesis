// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameEngine.h"
#include "PHGameEngine.generated.h"

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHGameEngine : public UGameEngine
{
	GENERATED_BODY()
	
public:
	/**
	 * 	All regular engine handling, plus update King state appropriately.
	 */
	virtual void HandleNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString) override;
};
