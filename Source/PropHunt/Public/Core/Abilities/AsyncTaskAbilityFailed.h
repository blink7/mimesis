// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "AbilitySystemComponent.h"
#include "AsyncTaskAbilityFailed.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAbilityFailed, const UGameplayAbility*, Ability, const FGameplayTagContainer&, FailureReason);

/**
 * Blueprint node to automatically register a listener for any ability activation failures in an AbilitySystemComponent.
 * Useful to use in UI.
 */
UCLASS(BlueprintType, meta = (ExposedAsyncProxy = AsyncTask))
class PROPHUNT_API UAsyncTaskAbilityFailed : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable)
	FOnAbilityFailed OnAbilityFailed;

	// Listens for an ability activation fail event.
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true"))
	static UAsyncTaskAbilityFailed* ListenForAbilityFailed(UAbilitySystemComponent* AbilitySystemComponent);

	// You must call this function manually when you want the AsyncTask to end.
	// For UMG Widgets, you would call it in the Widget's Destruct event.
	UFUNCTION(BlueprintCallable)
	void EndTask();

protected:
	UPROPERTY()
	UAbilitySystemComponent* ASC;

	void AbilityFailed(const UGameplayAbility* Ability, const FGameplayTagContainer& FailureReason);
};
