// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "PHAbilityTask_WaitChangeFOV.generated.h"


/** Delegate type used, EventTag and Payload may be empty */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FChangeFOVDelegate);

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHAbilityTask_WaitChangeFOV : public UAbilityTask
{
	GENERATED_BODY()
	
public:
	UPHAbilityTask_WaitChangeFOV();

	void TickTask(float DeltaTime) override;

	UPROPERTY(BlueprintAssignable)
	FChangeFOVDelegate OnTargetFOVReached;

	// Change the FOV to the specified value, using the float curve (range 0 - 1) or fallback to linear interpolation
	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UPHAbilityTask_WaitChangeFOV* WaitChangeFOV(UGameplayAbility* OwningAbility, FName TaskInstanceName, class APlayerCameraManager* CameraManager, float TargetFOV, float Duration = 0.1f, UCurveFloat* OptionalInterpolationCurve = nullptr);

private:
	bool bIsFinished;

	float StartFOV;

	float TargetFOV;

	float Duration;

	float TimeChangeStarted;

	float TimeChangeWillEnd;

	class APlayerCameraManager* CameraManager;

	UCurveFloat* LerpCurve;
};
