// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Abilities/Tasks/PHAbilityTask_WaitInteractableTarget.h"
#include "PHAbilityTask_WaitTransformationTarget.generated.h"


/**
 * Performs a line trace on a timer, looking for an Actor that implements APhysicsProp that is available for transformation to.
 */
UCLASS()
class PROPHUNT_API UPHAbilityTask_WaitTransformationTarget : public UPHAbilityTask_WaitInteractableTarget
{
	GENERATED_BODY()

public:
	/**
	* Traces a line on a timer looking for PhysicsProp.
	* @param MaxRange How far to trace.
	* @param TimerPeriod Period of trace timer.
	* @param bShowDebug Draws debug lines for traces.
	*/
	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true", HideSpawnParms = "Instigator"), Category = "Ability|Tasks")
	static UPHAbilityTask_WaitTransformationTarget* WaitForTransformationTarget(UGameplayAbility* OwningAbility, FName TaskInstanceName, ECollisionChannel TraceChannel, float MaxRange = 200.f, float TimerPeriod = 0.1f, bool bShowDebug = false);

protected:
	/** Traces as normal, but will manually filter all hit actors */
	void LineTrace(FHitResult& OutHitResult, const UWorld* World, const FVector& Start, const FVector& End, ECollisionChannel ChannelName, const FCollisionQueryParams Params, bool bLookForInteractableActor) const override;
};
