// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "GameplayCueNotify_Static.h"
#include "PHGameplayCueNotify_HitImpact.generated.h"


/**
 *	Non instanced GameplayCueNotify for spawning Niagara and sound FX.
 */
UCLASS(Blueprintable, meta = (DisplayName = "GCN Hit Impact", Category = "GameplayCue"))
class PROPHUNT_API UPHGameplayCueNotify_HitImpact : public UGameplayCueNotify_Static
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameplayCue")
	class USoundBase* Sound;

	/** Effects to play for weapon attacks against specific surfaces */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameplayCue")
	class UNiagaraSystem* ParticleSystem;
};
