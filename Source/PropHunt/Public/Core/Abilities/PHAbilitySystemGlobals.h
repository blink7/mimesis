// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemGlobals.h"
#include "PHGameplayEffectTypes.h"
#include "PHAbilitySystemGlobals.generated.h"


/**
 *
 */
UCLASS(Config = "Game")
class PROPHUNT_API UPHAbilitySystemGlobals : public UAbilitySystemGlobals
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FGameplayTag DeadTag;
	UPROPERTY(Config)
	FName DeadName;

	UPROPERTY()
	FGameplayTag InteractingTag;
	UPROPERTY(Config)
	FName InteractingName;

	UPROPERTY()
	FGameplayTag InteractingRemovalTag;
	UPROPERTY(Config)
	FName InteractingRemovalName;

	static UPHAbilitySystemGlobals& PHGet()
	{
		return dynamic_cast<UPHAbilitySystemGlobals&>(Get());
	}

	virtual void InitGlobalTags() override;

	virtual FGameplayEffectContext* AllocGameplayEffectContext() const override
	{
		return new FPHGameplayEffectContext();
	}
};
