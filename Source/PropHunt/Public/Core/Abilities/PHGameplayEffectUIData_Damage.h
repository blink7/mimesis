// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "GameplayEffectUIData.h"
#include "PHGameplayEffectUIData_Damage.generated.h"


/**
 * UI data that contains only text. This is mostly used as an example of a subclass of UGameplayEffectUIData.
 * If your game needs only text, this is a reasonable class to use. To include more data, make a custom subclass of UGameplayEffectUIData.
 */
UCLASS()
class PROPHUNT_API UPHGameplayEffectUIData_Damage : public UGameplayEffectUIData
{
	GENERATED_BODY()

public:
	/** Icon displayed in death messages log when killed with this weapon */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data")
	class UTexture2D* KillIcon;
 
	/** Force feedback effect to play on a player hit by this damage type */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data")
	class UForceFeedbackEffect *HitForceFeedback;
 
	/** Force feedback effect to play on a player killed by this damage type */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data")
	class UForceFeedbackEffect *KilledForceFeedback;
};
