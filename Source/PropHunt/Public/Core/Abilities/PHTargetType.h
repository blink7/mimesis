// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Abilities/GameplayAbilityTypes.h"
#include "PHTargetType.generated.h"


class AActor;
class APHPlayerState;

/**
 * Class that is used to determine targeting for abilities
 * It is meant to be blueprinted to run target logic
 * This does not subclass GameplayAbilityTargetActor because this class is never instanced into the world
 * This can be used as a basis for a game-specific targeting blueprint
 * If your targeting is more complicated you may need to instance into the world once or as a pooled actor
 */
UCLASS(Blueprintable, meta = (ShowWorldContextPin))
class PROPHUNT_API UPHTargetType : public UObject
{
	GENERATED_BODY()
	
public:
    /** Called to determine targets to apply gameplay effects to */
    UFUNCTION(BlueprintNativeEvent)
    void GetTargets(APHPlayerState* TargetingPlayerState, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const;
};

/** Trivial target type that uses the owner */
UCLASS(NotBlueprintable)
class PROPHUNT_API UPHTargetType_UseOwner : public UPHTargetType
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	UPHTargetType_UseOwner() {}

	/** Uses the passed in event data */
	void GetTargets_Implementation(APHPlayerState* TargetingPlayerState, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const override;
};

/** Trivial target type that pulls the target out of the event data */
UCLASS(NotBlueprintable)
class PROPHUNT_API UPHTargetType_UseEventData : public UPHTargetType
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	UPHTargetType_UseEventData() {}

	/** Uses the passed in event data */
	void GetTargets_Implementation(APHPlayerState* TargetingPlayerState, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const override;
};
