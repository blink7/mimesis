﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "GameplayTagContainer.h"
#include "PHAsyncTaskCooldownChange.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnCooldownChanged, FGameplayTag, CooldownTag, float, TimeRemaining, float, Duration);

class UAbilitySystemComponent;

/**
* Blueprint node to automatically register a listener for Cooldown begin and end.
* Useful to use in Blueprint/UMG.
*/
UCLASS(BlueprintType, meta = (ExposedAsyncProxy = AsyncTask))
class PROPHUNT_API UAsyncTaskCooldownChange : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

	// Listens for Cooldown begin and end.
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true"))
	static UAsyncTaskCooldownChange* ListenForCooldownChange(UAbilitySystemComponent* AbilitySystemComponent, FGameplayTagContainer CooldownTags);

	// You must call this function manually when you want the AsyncTask to end.
	// For UMG Widgets, you would call it in the Widget's Destruct event.
	UFUNCTION(BlueprintCallable)
	void EndTask();
	
	public:
	UPROPERTY(BlueprintAssignable)
	FOnCooldownChanged OnBegin;

	UPROPERTY(BlueprintAssignable)
	FOnCooldownChanged OnEnd;

private:
	UPROPERTY()
	UAbilitySystemComponent* ASC;

	FGameplayTagContainer Tags;

	virtual void TagChanged(const FGameplayTag CooldownTag, int32 NewCount);
};
