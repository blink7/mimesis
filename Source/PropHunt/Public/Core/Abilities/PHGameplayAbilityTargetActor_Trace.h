// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PHGameplayAbilityTargetActor_Trace.generated.h"


using AGameplayAbilityWorldReticlePtr = TWeakObjectPtr<AGameplayAbilityWorldReticle>;

class UGameplayAbility;

/**
 * Reusable, configurable trace TargetActor. Subclass with your own trace shapes.
 * Meant to be used with custom PHAbilityTask_WaitTargetDataUsingActor instead of the default WaitTargetData AbilityTask as the default
 * one will destroy the TargetActor.
 */
UCLASS(Abstract)
class PROPHUNT_API APHGameplayAbilityTargetActor_Trace : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()

public:
	APHGameplayAbilityTargetActor_Trace();

	void BeginPlay() override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Traces as normal, but will manually filter all hit actors */
	void LineTraceMultiWithFilter(TArray<FHitResult>& OutHitResults, const UWorld* World, const FGameplayTargetDataFilterHandle FilterHandle, const FVector& Start, const FVector& End, ECollisionChannel ChannelName, const FCollisionQueryParams Params);

	void AimWithPlayerController(const AActor* InSourceActor, FCollisionQueryParams Params, const FVector& TraceStart, FVector& OutTraceEnd, bool bIgnorePitch = false);

	static bool ClipCameraRayToAbilityRange(FVector CameraLocation, FVector CameraDirection, FVector AbilityCenter, float AbilityRange, FVector& ClippedPosition);

    void StartTargeting(UGameplayAbility* Ability) override;

	void CancelTargeting() override;

	void ConfirmTargetingAndContinue() override;

	void Tick(float DeltaSeconds) override;

	virtual void TickTrace();

    virtual void StopTargeting();

	UFUNCTION(BlueprintCallable)
	virtual void ResetSpread();

	virtual float GetCurrentSpread() const;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	float MaxRange;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, config, meta = (ExposeOnSpawn = true), Category = "Trace")
	TEnumAsByte<ECollisionChannel> TraceChannel;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	bool bTraceComplex;

	// Does the trace affect the aiming pitch
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	bool bTraceAffectsAimPitch;

	// Should we modify spread based on if we're aiming? If true, must set InAimingTag and AimingRemovalTag.
	UPROPERTY(BlueprintReadWrite, Category = "Accuracy")
	bool bUseAimingSpreadMod;

	// Base targeting spread (degrees)
	UPROPERTY(BlueprintReadWrite, Category = "Accuracy")
	float BaseSpread;

	// Multiplicative modifier to spread if aiming.
	UPROPERTY(BlueprintReadWrite, Category = "Accuracy")
	float AimingSpreadMod;

	// Amount spread increments from continuous targeting in degrees.
	UPROPERTY(BlueprintReadWrite, Category = "Accuracy")
	float TargetingSpreadIncrement;

	// Maximum amount of spread for continuous targeting in degrees.
	UPROPERTY(BlueprintReadWrite, Category = "Accuracy")
	float TargetingSpreadMax;

	// Current spread from continuous targeting
	float CurrentTargetingSpread;

	// Predicted GameplayTag for aiming. Only used if we modify spread while aiming. If used, must set AimingRemovalTag also.
	UPROPERTY(BlueprintReadWrite, Category = "Accuracy")
	FGameplayTag AimingTag;

	// Predicted GameplayTag for aiming removal. Only used if we modify spread while aiming. If used, must set AimingTag also.
	UPROPERTY(BlueprintReadWrite, Category = "Accuracy")
	FGameplayTag AimingRemovalTag;

	// Maximum hit results to return per trace. 0 just returns the trace end point.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	int32 MaxHitResultsPerTrace;

    // Number of traces to perform at one time. Single bullet weapons like rifles will only do one trace.
	// Multi-bullet weapons like shotguns can do multiple traces. Not intended to be used with PersistentHits.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	int32 NumberOfTraces;

	// Ignore blocking collision hits in the trace. Useful if you want to target through walls.
    UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	bool bIgnoreBlockingHits;

	/**
	* Should we trace from the player ViewPoint instead of the StartLocation? The
	* TargetData HitResults will still have the StartLocation for the TraceStart. This is useful for FPS where we want
	* to trace from the player ViewPoint but draw the bullet tracer from the weapon muzzle.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	bool bTraceFromPlayerViewPoint;

	// HitResults will persist until Confirmation/Cancellation or until a new HitResult takes its place
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	bool bUsePersistentHitResults;

protected:
	// Trace End point, useful for debug drawing
	FVector CurrentTraceEnd;

	TArray<AGameplayAbilityWorldReticlePtr> ReticleActors;

	TArray<FHitResult> PersistentHitResults;

	UFUNCTION(BlueprintCallable, Category = "Trace")
	virtual TArray<FHitResult> PerformTraceMulti(AActor* InSourceActor);

	virtual FGameplayAbilityTargetDataHandle MakeTargetData(const TArray<FHitResult>& HitResults) const;

	virtual void DoTrace(TArray<FHitResult>& HitResults, const UWorld* World, const FGameplayTargetDataFilterHandle FilterHandle, const FVector& Start, const FVector& End, ECollisionChannel Channel, const FCollisionQueryParams Params) PURE_VIRTUAL(APHGameplayAbilityTargetActor_Trace, return;);
	
	virtual void ShowDebugTrace(TArray<FHitResult>& HitResults, EDrawDebugTrace::Type DrawDebugType, float Duration = 2.0f) PURE_VIRTUAL(APHGameplayAbilityTargetActor_Trace, return;);

	virtual void DestroyReticleActors();
};