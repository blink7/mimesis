// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "PHAbilityActorAttributeSet.generated.h"


// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * UAttributeSet used for APHAbilityActorBase
 */
UCLASS()
class PROPHUNT_API UPHAbilityActorAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:
	UPHAbilityActorAttributeSet();

	void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(BlueprintReadOnly, Category = "PowerPanel", ReplicatedUsing = OnRep_PowerPanelEnergy)
	FGameplayAttributeData PowerPanelEnergy;
	ATTRIBUTE_ACCESSORS(UPHAbilityActorAttributeSet, PowerPanelEnergy)

	UPROPERTY(BlueprintReadOnly, Category = "PowerPanel", ReplicatedUsing = OnRep_MaxPowerPanelEnergy)
	FGameplayAttributeData MaxPowerPanelEnergy;
	ATTRIBUTE_ACCESSORS(UPHAbilityActorAttributeSet, MaxPowerPanelEnergy)

	UPROPERTY(BlueprintReadWrite, Category = "Portal", ReplicatedUsing = OnRep_PortalEnergy)
	FGameplayAttributeData PortalEnergy;
	ATTRIBUTE_ACCESSORS(UPHAbilityActorAttributeSet, PortalEnergy)

	UPROPERTY(BlueprintReadWrite, Category = "Portal", ReplicatedUsing = OnRep_MaxPortalEnergy)
	FGameplayAttributeData MaxPortalEnergy;
	ATTRIBUTE_ACCESSORS(UPHAbilityActorAttributeSet, MaxPortalEnergy)

protected:
	/** Helper function to proportionally adjust the value of an attribute when it's associated max attribute changes. (i.e. When MaxHealth increases, Health increases by an amount that maintains the same percentage as before) */
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty);

	UFUNCTION()
	virtual void OnRep_PowerPanelEnergy(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxPowerPanelEnergy(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_PortalEnergy(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxPortalEnergy(const FGameplayAttributeData& OldValue);
};
