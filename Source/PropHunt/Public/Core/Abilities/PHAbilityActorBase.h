// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbilitySystemInterface.h"
#include "GameplayAbilitySpec.h"
#include "PHAbilityActorBase.generated.h"


/**
* Base AActor class that has an AbilitySystemComponent.
*/
UCLASS(Abstract)
class PROPHUNT_API APHAbilityActorBase : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:	
	APHAbilityActorBase();

	void PostInitializeComponents() override;

	virtual void HandlePowerPanelEnergyChanged(AController* InInstigator, float DeltaValue) {};

	virtual void HandlePortalEnergyChanged(AController* InInstigator, float DeltaValue) {};

protected:
	void BeginPlay() override;

public:
	// Implement IAbilitySystemInterface
	class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	template<class T>
	T* GetAbilitySystemComponent() const { return Cast<T>(GetAbilitySystemComponent()); }

	class UPHAbilityActorAttributeSet* GetAttributeSet() const { return AttributeSet; }

protected:	
	/** The component used to handle ability system interactions */
	UPROPERTY()
	class UPHAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY()
	class UPHAbilityActorAttributeSet* AttributeSet;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TArray<TSubclassOf<class UPHGameplayAbility>> Abilities;

	UPROPERTY(BlueprintReadOnly, Category = "Abilities")
	TArray<FGameplayAbilitySpecHandle> AbilitySpecHandles;

	/** Passive gameplay effects applied on creation */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<class UGameplayEffect>> PassiveGameplayEffects;
};
