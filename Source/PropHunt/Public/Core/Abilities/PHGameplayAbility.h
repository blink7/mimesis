// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "../PropHunt.h"
#include "PHGameplayEffectTypes.h"
#include "PHGameplayAbility.generated.h"


class UPHAbilitySystemComponent;

/**
 *
 */
UCLASS()
class PROPHUNT_API UPHGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	UPHGameplayAbility();

	// Abilities with this set will automatically activate when the input is pressed
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	AbilityInput AbilityInputID = AbilityInput::None;

	// Tells an ability to activate immediately when its granted. Used for passive abilities and abilities forced on others.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
	bool bActivateAbilityOnGranted = false;

	// If true, this ability will activate when its bound input is pressed. Disable if you want to bind an ability to an
	// input but not have it activate when pressed.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
	bool bActivateOnInput;

	/** Map of gameplay tags to gameplay effect containers */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameplayEffects")
	TMap<FGameplayTag, FPHGameplayEffectContainer> EffectContainerMap;

	// If an ability is marked as 'bActivateAbilityOnGranted', activate them immediately when given here
	// Epic's comment: Projects may want to initiate passives or do other "BeginPlay" type of logic here.
	virtual void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

	/** Returns the controller associated with this ability. May be null */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	class APHPlayerState* GetPlayerStateFromActorInfo() const;

	/** Returns the controller associated with this ability. May be null */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	class APlayerController* GetPlayerControllerFromActorInfo() const;

	/** Returns the base character associated with this ability. May be null */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	class APHCharacterBase* GetCharacterFromActorInfo() const;

	UFUNCTION(BlueprintCallable, Category = "Ability")
	UPHAbilitySystemComponent* GetPHAbilitySystemComponentFromActorInfo() const;
	UPHAbilitySystemComponent* GetPHAbilitySystemComponentFromActorInfo_Checked() const;
	UPHAbilitySystemComponent* GetPHAbilitySystemComponentFromActorInfo_Ensured() const;

	// Expose GetSourceObject to Blueprint. Retrieves the SourceObject associated with this ability. Callable on non instanced abilities.
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Ability", meta = (DisplayName = "Get Source Object"))
	UObject* K2_GetSourceObject(FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo) const;

	virtual bool CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;

	// Allows C++ and Blueprint abilities to override how cost is checked in case they don't use a GE like weapon ammo
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Ability")
	bool CheckCostCustom(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo) const;

	/** Is input currently pressed. */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	bool IsInputPressed() const;

	/** Make gameplay effect container spec to be applied later, using the passed in container */
	UFUNCTION(BlueprintCallable, Category = "Ability", meta = (AutoCreateRefTerm = "EventData"))
	virtual FPHGameplayEffectContainerSpec MakeEffectContainerSpecFromContainer(const FPHGameplayEffectContainer& Container, const FGameplayEventData& EventData, int32 OverrideGameplayLevel = -1);

	/** Search for and make a gameplay effect container spec to be applied later, from the EffectContainerMap */
	UFUNCTION(BlueprintCallable, Category = "Ability", meta = (AutoCreateRefTerm = "EventData"))
	virtual FPHGameplayEffectContainerSpec MakeEffectContainerSpec(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel = -1);

	/** Applies a gameplay effect container spec that was previously created */
	UFUNCTION(BlueprintCallable, Category = "Ability")
	virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainerSpec(const FPHGameplayEffectContainerSpec& ContainerSpec);

	/** Applies a gameplay effect container, by creating and then applying the spec */
	UFUNCTION(BlueprintCallable, Category = "Ability", meta = (AutoCreateRefTerm = "EventData"))
	virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainer(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel = -1);

	// Attempts to activate the given ability handle and batch all RPCs into one. This will only batch all RPCs that happen
	// in one frame. Best case scenario we batch ActivateAbility, SendTargetData, and EndAbility into one RPC instead of three.
	UFUNCTION(BlueprintCallable, Category = "Ability")
	virtual bool BatchRPCTryActivateAbility(FGameplayAbilitySpecHandle InAbilityHandle, bool EndAbilityImmediately);

	// Same as calling K2_EndAbility. Meant for use with batching system to end the ability externally.
	virtual void ExternalEndAbility();

	/** Invoke a gameplay cue on the ability owner, with extra parameters */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (GameplayTagFilter = "GameplayCue"), DisplayName = "Execute GameplayCueWithParams On Owner Local", meta=(ScriptName = "ExecuteGameplayCueWithParamsLocal"))
	virtual void K2_ExecuteGameplayCueWithParamsLocal(FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	/** Adds a persistent gameplay cue to the ability owner. Optionally will remove if ability ends */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (GameplayTagFilter = "GameplayCue"), DisplayName = "Add GameplayCueWithParams To Owner Local", meta=(ScriptName = "AddGameplayCueWithParamsLocal"))
	virtual void K2_AddGameplayCueWithParamsLocal(FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameter);

	/** Removes a persistent gameplay cue from the ability owner */
	UFUNCTION(BlueprintCallable, Category = Ability, meta=(GameplayTagFilter="GameplayCue"), DisplayName="Remove GameplayCue From Owner Local", meta=(ScriptName="RemoveGameplayCueLocal"))
	virtual void K2_RemoveGameplayCueLocal(FGameplayTag GameplayCueTag);
};
