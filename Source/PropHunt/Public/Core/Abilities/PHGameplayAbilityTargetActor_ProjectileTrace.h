// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor_Trace.h"
#include "PHGameplayAbilityTargetActor_ProjectileTrace.generated.h"


/**
 * 
 */
UCLASS(Blueprintable)
class PROPHUNT_API APHGameplayAbilityTargetActor_ProjectileTrace : public AGameplayAbilityTargetActor_Trace
{
    GENERATED_BODY()

public:
	void Tick(float DeltaSeconds) override;

    /**
	* Should we trace from the player ViewPoint instead of the StartLocation? The
	* TargetData HitResult will still have the StartLocation for the TraceStart. This is useful for FPS where we want
	* to trace from the player ViewPoint but draw the bullet tracer from the weapon muzzle.
	*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	bool bTraceFromPlayerViewPoint;

	// Initial launch speed at the start of the trace.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	float ProjectileSpeed;

	// Projectile radius, used when tracing for collision. If <= 0, a line trace is used instead.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	float ProjectileRadius;

	// Trace channel to use, if tracing with collision.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Trace")
	TEnumAsByte<ECollisionChannel> TraceChannel;

protected:
    FHitResult PerformTrace(AActor* InSourceActor) override;

	virtual void VisualizeTrace(const UObject* WorldContextObject, FVector Start, FVector End, float TossSpeed, float CollisionRadius = 0.f);

	UFUNCTION(BlueprintImplementableEvent, Category = "Trace", meta = (DisplayName = "VisualizeTrace"))
    void K2_VisualizeTrace(const TArray<FVector>& ProjectilePathPositions);
};