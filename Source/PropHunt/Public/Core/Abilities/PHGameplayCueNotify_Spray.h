// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayCueNotify_Actor.h"
#include "PHGameplayCueNotify_Spray.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API APHGameplayCueNotify_Spray : public AGameplayCueNotify_Actor
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spray", meta = (AllowPrivateAccess = "true"))
	UDecalComponent* Decal;

public:
	APHGameplayCueNotify_Spray();

	void HandleGameplayCue(AActor* MyTarget, EGameplayCueEvent::Type EventType, const FGameplayCueParameters& Parameters) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Spray")
	class USoundBase* SpraySound;

	UPROPERTY()
	class UMaterialInstanceDynamic* DynamicSprayMaterial;

	void SetSprayTexture(class UTexture2D* NewTexture);
};
