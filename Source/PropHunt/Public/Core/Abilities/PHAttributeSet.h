// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "PHAttributeSet.generated.h"


// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:
	UPHAttributeSet();

	void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const override;

	/** Current Health, when 0 we expect owner to die. Capped by MaxHealth */
	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_Health)
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, Health)

	/** MaxHealth is its own attribute, since GameplayEffects may modify it */
	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_MaxHealth)
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MaxHealth)

	UPROPERTY(BlueprintReadOnly, Category = "Violation", ReplicatedUsing = OnRep_Violations)
	FGameplayAttributeData Violations;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, Violations)

	/** MaxHealth is its own attribute, since GameplayEffects may modify it */
	UPROPERTY(BlueprintReadOnly, Category = "Violation", ReplicatedUsing = OnRep_MaxViolations)
	FGameplayAttributeData MaxViolations;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MaxViolations)

	/** Current Mana, used to execute special abilities. Capped by MaxEnergy */
	UPROPERTY(BlueprintReadOnly, Category = "Energy", ReplicatedUsing = OnRep_Energy)
	FGameplayAttributeData Energy;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, Energy)

	UPROPERTY(BlueprintReadOnly, Category = "Energy", ReplicatedUsing = OnRep_MaxEnergy)
	FGameplayAttributeData MaxEnergy;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MaxEnergy)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo", ReplicatedUsing = OnRep_RifleAmmo)
	FGameplayAttributeData RifleAmmo;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, RifleAmmo)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo", ReplicatedUsing = OnRep_MaxRifleAmmo)
	FGameplayAttributeData MaxRifleAmmo;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MaxRifleAmmo)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo", ReplicatedUsing = OnRep_RifleClipAmmo)
	FGameplayAttributeData RifleClipAmmo;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, RifleClipAmmo)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo", ReplicatedUsing = OnRep_MaxRifleClipAmmo)
	FGameplayAttributeData MaxRifleClipAmmo;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MaxRifleClipAmmo)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo", ReplicatedUsing = OnRep_GrenadeAmmo)
	FGameplayAttributeData GrenadeAmmo;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, GrenadeAmmo)

	UPROPERTY(BlueprintReadOnly, Category = "Ammo", ReplicatedUsing = OnRep_MaxGrenadeAmmo)
	FGameplayAttributeData MaxGrenadeAmmo;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MaxGrenadeAmmo)

	UPROPERTY(BlueprintReadOnly, Category = "Skill", ReplicatedUsing = OnRep_SkillUseAmount)
	FGameplayAttributeData SkillUseAmount;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, SkillUseAmount)

	UPROPERTY(BlueprintReadOnly, Category = "Skill", ReplicatedUsing = OnRep_MaxSkillUseAmount)
	FGameplayAttributeData MaxSkillUseAmount;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MaxSkillUseAmount)

	/** AttackPower of the attacker is multiplied by the base Damage to reduce health, so 1.0 means no bonus */
	UPROPERTY(BlueprintReadOnly, Category = "Damage", ReplicatedUsing = OnRep_AttackPower)
	FGameplayAttributeData AttackPower;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, AttackPower)

	/** Base Damage is divided by DefensePower to get actual damage done, so 1.0 means no bonus */
	UPROPERTY(BlueprintReadOnly, Category = "Damage", ReplicatedUsing = OnRep_DefensePower)
	FGameplayAttributeData DefensePower;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, DefensePower)

	/** Damage is a 'temporary' attribute used by the DamageExecution to calculate final damage, which then turns into -Health */
	UPROPERTY(BlueprintReadOnly, Category = "Damage", meta = (HideFromLevelInfos))
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, Damage)

	/** Damage Impulse is a 'temporary' attribute used to apply damage momentum to the target character */
	UPROPERTY(BlueprintReadOnly, Category = "Damage", meta = (HideFromLevelInfos))
	FGameplayAttributeData DamageImpulse;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, DamageImpulse)

	/** Damage is a 'temporary' attribute used by the DamageExecution to calculate final damage, which then turns into -Health */
	UPROPERTY(BlueprintReadOnly, Category = "Damage", meta = (HideFromLevelInfos))
	FGameplayAttributeData PropertyDamage;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, PropertyDamage)

	/** MoveSpeed affects how fast characters can move */
	UPROPERTY(BlueprintReadOnly, Category = "MoveSpeed", ReplicatedUsing = OnRep_MoveSpeed)
	FGameplayAttributeData MoveSpeed;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, MoveSpeed)

	/** Experience points gained from killing enemies. Used to level up. */
	UPROPERTY(BlueprintReadOnly, Category = "XP", ReplicatedUsing = OnRep_XP)
	FGameplayAttributeData XP;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, XP)

	// Experience points awarded to the character's killers. Used to level up (not implemented in this project).
	UPROPERTY(BlueprintReadOnly, Category = "XP", ReplicatedUsing = OnRep_XPBounty)
	FGameplayAttributeData XPBounty;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, XPBounty)

	UPROPERTY(BlueprintReadOnly, Category = "Taunt", meta = (HideFromLevelInfos))
	FGameplayAttributeData TauntDuration;
	ATTRIBUTE_ACCESSORS(UPHAttributeSet, TauntDuration)

	static FGameplayAttribute GetAmmoAttributeFromTag(FGameplayTag& PrimaryAmmoTag);
	static FGameplayAttribute GetMaxAmmoAttributeFromTag(FGameplayTag& PrimaryAmmoTag);

protected:
	FGameplayTag EnemyHitTag;

	/** Helper function to proportionally adjust the value of an attribute when it's associated max attribute changes. (i.e. When MaxHealth increases, Health increases by an amount that maintains the same percentage as before) */
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty);

	UFUNCTION()
	virtual void OnRep_Health(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxHealth(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_Violations(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxViolations(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_Energy(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxEnergy(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_RifleAmmo(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxRifleAmmo(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_RifleClipAmmo(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxRifleClipAmmo(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_GrenadeAmmo(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxGrenadeAmmo(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_SkillUseAmount(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxSkillUseAmount(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_AttackPower(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_DefensePower(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MoveSpeed(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_XP(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_XPBounty(const FGameplayAttributeData& OldValue);
};
