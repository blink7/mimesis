﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetTypes.h"
#include "PHGameplayEffectTypes.generated.h"


/**
* Struct defining a list of gameplay effects, a tag, and targeting info
* These containers are defined statically in blueprints or assets and then turn into Specs at runtime
*/
USTRUCT(BlueprintType)
struct PROPHUNT_API FPHGameplayEffectContainer
{
	GENERATED_BODY()

	FPHGameplayEffectContainer() {}

	/** Sets the way that targeting happens */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GameplayEffectContainer")
	TSubclassOf<class UPHTargetType> TargetType;

	/** List of gameplay effects to apply to the targets */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GameplayEffectContainer")
	TArray<TSubclassOf<class UGameplayEffect>> TargetGameplayEffectClasses;
};

/** A "processed" version of PHGameplayEffectContainer that can be passed around and eventually applied */
USTRUCT(BlueprintType)
struct PROPHUNT_API FPHGameplayEffectContainerSpec
{
	GENERATED_BODY()

	FPHGameplayEffectContainerSpec() {}

	/** Computed target data */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GameplayEffectContainer")
	FGameplayAbilityTargetDataHandle TargetData;

	/** List of gameplay effects to apply to the targets */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GameplayEffectContainer")
	TArray<FGameplayEffectSpecHandle> TargetGameplayEffectSpecs;

	/** Returns true if this has any valid effect specs */
	bool HasValidEffects() const;

	/** Returns true if this has any valid targets */
	bool HasValidTargets() const;

	/** Adds new targets to target data */
	void AddTargets(const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors);

	/** Applies container spec that was made from an ability */
	TArray<FActiveGameplayEffectHandle> ApplyExternally();
};

USTRUCT(BlueprintType)
struct PROPHUNT_API FPHGameplayAbilityTargetData_SoftObject : public FGameplayAbilityTargetData
{
	GENERATED_BODY()

	template<typename T>
	TSoftObjectPtr<T> GetOptionalSoftObject() const
	{
		return TSoftObjectPtr<T>(OptionalSoftObject);
	}

	template<typename T>
	void SetOptionalSoftObject(const TSoftObjectPtr<T>& InExternalMediaAsset)
	{
		OptionalSoftObject = InExternalMediaAsset.ToSoftObjectPath();
	}

	/** Modifies the context and adds this target data to the target data handle stored within */
	virtual void AddTargetDataToContext(FGameplayEffectContextHandle& Context, bool bIncludeActorArray) const override;

	virtual UScriptStruct* GetScriptStruct() const override
	{
		return FPHGameplayAbilityTargetData_SoftObject::StaticStruct();
	}

	virtual FString ToString() const override
	{
		return TEXT("FPHGameplayAbilityTargetData_TauntInfo");
	}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess);

protected:
	UPROPERTY()
	FSoftObjectPath OptionalSoftObject;
};

template<>
struct TStructOpsTypeTraits<FPHGameplayAbilityTargetData_SoftObject> : public TStructOpsTypeTraitsBase2<FPHGameplayAbilityTargetData_SoftObject>
{
	enum
	{
		WithNetSerializer = true	// For now this is REQUIRED for FGameplayAbilityTargetDataHandle net serialization to work
	};
};

USTRUCT()
struct PROPHUNT_API FPHGameplayEffectContext : public FGameplayEffectContext
{
	GENERATED_BODY()

	template<typename T>
	TSoftObjectPtr<T> GetOptionalSoftObject() const
	{
		return TSoftObjectPtr<T>(OptionalSoftObject);
	}

	template<typename T>
	void SetOptionalSoftObject(const TSoftObjectPtr<T>& InExternalMediaAsset)
	{
		OptionalSoftObject = InExternalMediaAsset.ToSoftObjectPath();
	}

	/** Returns the actual struct used for serialization, subclasses must override this! */
	virtual UScriptStruct* GetScriptStruct() const override
	{
		return FPHGameplayEffectContext::StaticStruct();
	}

	/** Creates a copy of this context, used to duplicate for later modifications */
	virtual FGameplayEffectContext* Duplicate() const override
	{
		FPHGameplayEffectContext* NewContext = new FPHGameplayEffectContext();
		*NewContext = *this;
		NewContext->AddActors(Actors);
		if (GetHitResult())
		{
			// Does a deep copy of the hit result
			NewContext->AddHitResult(*GetHitResult(), true);
		}
		if (OptionalSoftObject.IsValid())
		{
			NewContext->OptionalSoftObject = OptionalSoftObject;
		}
		return NewContext;
	}

	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;

protected:
	UPROPERTY()
	FSoftObjectPath OptionalSoftObject;

	friend FPHGameplayAbilityTargetData_SoftObject;
};

template<>
struct TStructOpsTypeTraits<FPHGameplayEffectContext> : public TStructOpsTypeTraitsBase2<FPHGameplayEffectContext>
{
	enum
	{
		WithNetSerializer = true,
		WithCopy = true		// Necessary so that TSharedPtr<FHitResult> Data is copied around
	};
};
