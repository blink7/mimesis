// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "PHGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMatchStateChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemainingTimeChangedDelegate, int32, NewRemainingTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnergyChangedDelegate, float, NewEnergy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerStateChangedDelegate, APlayerState*, PlayerState);

class APHPlayerState;

/** Timed PlayerState map, created from the GameState */
using TimedPlayerMap = TMap<int32, TWeakObjectPtr<APHPlayerState>>;

/** Ranked PlayerState map, created from the GameState */
using RankedPlayerMap = TMap<int32, TWeakObjectPtr<APHPlayerState>>;

USTRUCT()
struct FMatchData
{
	GENERATED_USTRUCT_BODY()

	FMatchData() 
		: TeamWins({ 0, 0 }), 
		RoundCount(1) 
	{}
	
	UPROPERTY()
	TArray<int32> TeamWins;

	UPROPERTY()
	uint8 RoundCount;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API APHGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	//~ Begin AGameState Interface
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void OnRep_MatchState() override;
	//~ End AGameState Interface

	//~ Begin AGameStateBase Interface
	virtual void AddPlayerState(APlayerState* PlayerState) override;
	virtual void RemovePlayerState(APlayerState* PlayerState) override;
	//~ End AGameStateBase Interface

	virtual void OnMatchStateSet();
	
	/** Number of teams in current game (doesn't deprecate when no players are left in a team) */
	UPROPERTY(Transient, Replicated)
	int32 NumTeams;

	/** Is timer paused? */
	UPROPERTY(Transient, Replicated)
	bool bTimerPaused;

	UPROPERTY(Transient, Replicated)
	FString ServerName;

	UPROPERTY(Transient, Replicated)
	FString MapName;

	UPROPERTY(Transient, Replicated)
	int32 MaxPlayers;

	UPROPERTY(Transient, Replicated)
	FString ServerLang;

	UPROPERTY(BlueprintAssignable, Category = "PHGameState")
	FOnMatchStateChanged OnMatchIsWaitingToStart;

	UPROPERTY(BlueprintAssignable, Category = "PHGameState")
	FOnMatchStateChanged OnMatchHasStarted;

	UPROPERTY(BlueprintAssignable, Category = "PHGameState")
	FOnMatchStateChanged OnMatchHasStartedHunting;

	UPROPERTY(BlueprintAssignable, Category = "PHGameState")
	FOnMatchStateChanged OnMatchHasEnded;

	UPROPERTY(BlueprintAssignable, Category = "PHGameState")
	FOnMatchStateChanged OnLeavingMap;

	UPROPERTY(BlueprintAssignable, Category = "PHGameState")
	FOnRemainingTimeChangedDelegate OnRemainingTimeChanged;

	/**
	 * Called when PlayerState has been added, removed or changed.
	 * Reference to changed PlayerState can be nullptr if has been removed.
	 */
	UPROPERTY(BlueprintAssignable)
	FOnPlayerStateChangedDelegate OnPlayerStateChanged;

	void SetRemainingTime(int32 NewValue);
	int32 GetRemainingTime() const { return RemainingTime; }

	/** Decrement the value of RemainingTime by 1 and return the result. */
	int32 DecrementRemainingTime();

	void SetRoundCount(const uint8& RoundCount);
	uint8 GetRoundCount() const;

	void SetTeamWins(const TArray<int32>& TeamWins);
	const TArray<int32>& GetTeamWins() const;

	/** Gets timed PlayerState map for specific team */
	void GetTimedPlayerMap(int32 TeamIndex, TimedPlayerMap& OutTimedMap) const;

	/** Gets ranked PlayerState map for specific team */
	void GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const;

	uint8 GetLivePlayerCount(int32 TeamIndex) const;

	uint8 GetActivePlayersNumber(int32 TeamNumber) const;

	uint8 GetVotesForMap(int32 MapIndex) const;

	uint8 GetTeamScore(int32 TeamNumber) const;

	uint32 GetReadyPlayers() const;

	void RequestFinishAndExitToMainMenu();

protected:
	UPROPERTY(Transient, Replicated)
	FMatchData MatchData;

	/** Time left for warmup / match */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_RemainingTime)
	int32 RemainingTime;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_TotalEnergy)
	float TotalEnergy;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_PortalEnergy)
	float PortalEnergy;

	UFUNCTION()
	void OnRep_RemainingTime();

	UFUNCTION()
	void OnRep_TotalEnergy();

	UFUNCTION()
	void OnRep_PortalEnergy();

public:
	UPROPERTY(Transient, Replicated, BlueprintReadOnly, Category = "Portal")
	float NeedPortalEnergy;

	UPROPERTY(BlueprintAssignable)
	FOnEnergyChangedDelegate OnTotalEnergyChanged;

	UPROPERTY(BlueprintAssignable)
	FOnEnergyChangedDelegate OnPortalEnergyChanged;

	void SetTotalEnergy(float NewValue);

	float GetTotalEnergy() const { return TotalEnergy; }

	void SetPortalEnergy(float NewValue);

	float GetPortalEnergy() const { return PortalEnergy; }
};
