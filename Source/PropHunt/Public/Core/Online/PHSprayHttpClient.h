// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HttpClient.h"
#include "Blueprint/AsyncTaskDownloadImage.h"
#include "PHSprayHttpClient.generated.h"

using SprayTextureCallback = TFunction<void(class UTexture2D* SprayTexture)>;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHSprayHttpClient : public UHttpClient
{
	GENERATED_BODY()
	
public:
	UPHSprayHttpClient();

	void CheckSprayExists(const FUniqueNetId& UserId, const FString& SprayHash, ResponseStatusCallback OnSprayExistsCallback);

	void UploadSpray(const FUniqueNetId& UserId, class UTexture2D* SprayTexture, const FString& Name, ResponseStatusCallback OnUploadedCallback);

	void DownloadSpray(const FUniqueNetId& UserId, SprayTextureCallback OnSprayDownloadCallback);

protected:
	void OnSprayExistsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnSprayExistsCallback);

	void OnUploadPlayerSprayResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUploadedCallback);

	void OnDownloadSprayResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, SprayTextureCallback OnSprayLoadedCallback);

private:
	FString SprayEndpoint;
};
