// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Online/HttpClient.h"
#include "PHCustomizationHttpClient.generated.h"

USTRUCT()
struct FPropColorScheme
{
	GENERATED_BODY()

	FPropColorScheme()
	: useCustomColor(false),
	bodyColor(FLinearColor::Black),
	tailColor(FLinearColor::Black)
	{}

	UPROPERTY()
	bool useCustomColor;

	UPROPERTY()
	FLinearColor bodyColor;

	UPROPERTY()
	FLinearColor tailColor;
};

using ColorSchemeCallback = TFunction<void(const FPropColorScheme& ColorSchema)>;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHCustomizationHttpClient : public UHttpClient
{
	GENERATED_BODY()
	
public:
	UPHCustomizationHttpClient();

	void GetPropColorScheme(const FUniqueNetId& UserId, ColorSchemeCallback OnGetColorSchemaCallback);

	void UpdatePropColorScheme(const FUniqueNetId& UserId, const FPropColorScheme& ColorSchema, ResponseStatusCallback OnUpdateColorSchemaCallback);

protected:
	void OnGetPropColorSchemeResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ColorSchemeCallback OnGetColorSchemeCallback);

	void OnUpdatePropColorSchemeResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateColorSchemeCallback);

private:
	FString CustomizationEndpoint;
};
