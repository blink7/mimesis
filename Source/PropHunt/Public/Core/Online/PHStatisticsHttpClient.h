// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Online/HttpClient.h"
#include "PHStatisticsHttpClient.generated.h"

USTRUCT()
struct FResponse_TodayRatingItem
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FString playerName;

	UPROPERTY()
	uint32 score;

	UPROPERTY()
	bool owner;
};

USTRUCT()
struct FRequest_PlayerStats
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	int32 kills;

	UPROPERTY()
	int32 deaths;

	UPROPERTY()
	int32 taunts;

	UPROPERTY()
	int32 transforms;

	UPROPERTY()
	uint32 survivalTime;

	UPROPERTY()
	int32 wins;

	UPROPERTY()
	int32 losses;

	UPROPERTY()
	int32 propXP;

	UPROPERTY()
	int32 hunterXP;

	bool bValid;
};

using TodayRatingCallback = TFunction<void(TArray<FResponse_TodayRatingItem> InTodayRating)>;

using StatsCallback = TFunction<void(const FRequest_PlayerStats & PlayerStats)>;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHStatisticsHttpClient : public UHttpClient
{
	GENERATED_BODY()
	
public:
	UPHStatisticsHttpClient();

	void GetTodayRating(const FUniqueNetId& UserId, const FName& RatingType, TodayRatingCallback OnGetTodayRatingCallback);

	void GetPlayerStats(const FUniqueNetId& UserId, StatsCallback OnGetStatsCallback);

	void UpdatePlayerStats(const FUniqueNetId& UserId, const FRequest_PlayerStats& PlayerStats, ResponseStatusCallback OnUpdateStatsCallback);

protected:
	void OnGetTodayRatingResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, TodayRatingCallback OnGetTodayRatingCallback);

	void OnGetPlayerStatsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, StatsCallback OnGetStatsCallback);

	void OnUpdatePlayerStatsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateStatsCallback);

private:
	FString StatisticsEndpoint;
};
