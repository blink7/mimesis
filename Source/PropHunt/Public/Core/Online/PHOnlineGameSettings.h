// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "OnlineSessionSettings.h"


#define SETTING_SERVERNAME FName(TEXT("SERVERNAME"))

/**
 * General session settings for a Shooter game
 */
class FPHOnlineSessionSettings : public FOnlineSessionSettings
{
public:

	FPHOnlineSessionSettings(bool bIsLAN = false, bool bIsPresence = false, int32 MaxNumPlayers = 4);
	virtual ~FPHOnlineSessionSettings() {}
};

/**
 * General search setting for a Shooter game
 */
class FPHOnlineSearchSettings : public FOnlineSessionSearch
{
public:
	FPHOnlineSearchSettings(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FPHOnlineSearchSettings() {}
};

/**
 * Search settings for an empty dedicated server to host a match
 */
class FPHInlineSearchSettingsEmptyDedicated : public FPHOnlineSearchSettings
{
public:
	FPHInlineSearchSettingsEmptyDedicated(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FPHInlineSearchSettingsEmptyDedicated() {}
};
