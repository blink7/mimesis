// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AbilitySystemInterface.h"
#include "GameplayAbilitySpec.h"
#include "GameplayTagContainer.h"
#include "PHPlayerState.generated.h"

DECLARE_DELEGATE(FOnLoadPlayerCompleteDelegate);
DECLARE_DELEGATE(FOnTeamChangedDelegate);
DECLARE_DELEGATE(FOnCharacterSkillSetDelegate);
DECLARE_DELEGATE_OneParam(FOnSprayLoaded, UTexture2D*);

class UGameplayEffect;
class UPHGameplayAbility;

/**
 * 
 */
UCLASS()
class PROPHUNT_API APHPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	APHPlayerState();
	
	//~ Begin APlayerState Interface
	virtual void PostInitializeComponents() override;
	virtual void UnregisterPlayerWithSession() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker) override;
	virtual void OverrideWith(APlayerState* PlayerState) override;
	virtual void CopyProperties(APlayerState* PlayerState) override;
	virtual void ClientInitialize(class AController* C) override;
	//~ End APlayerState Interface

	UPROPERTY(Transient, Replicated)
	bool bReady;

	UPROPERTY(Transient, Replicated)
	int32 NextMapVote;

	bool bHearTransformations;

	int32 GetHunterScore() const { return HunterScore; }

	int32 GetPropScore() const { return PropScore; }

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	int32 GetKills() const { return Kills; }

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	int32 GetDeaths() const { return Deaths; }

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	int32 GetTaunts() const { return Taunts; }

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	int32 GetTransforms() const { return Morphs; }

	int32 GetSurvivalTime() const { return SurvivalTime; }

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	int32 GetWins() const { return Wins; }

	int32 GetLosses() const { return Losses; }

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	int32 GetPlayerLevel() const { return PlayerLevel; }

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	bool IsHunter() const;

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
	bool IsProp() const;

	float GetTimeSinceTeamChanged() const { return TimeSinceTeamChanged; }

	/** Get whether the player quit the match */
	bool IsQuitter() const { return bQuitter; }

	bool IsInteracting() const;

	int32 GetCurrentXP() const { return CurrentXP; }

	int32 GetNeededXP() const { return NeededXP; }

	FPrimaryAssetId GetSkillId(int32 ForTeam) const;
	
	FPrimaryAssetId GetCustomWeaponId() const { return CustomWeaponId; }

	void SetSkillId(FPrimaryAssetId Id, int32 ForTeam);

	void SaveAbilities();

	/**
	 * Set new team and update pawn.
	 *
	 * @param	NewTeamNumber	Team we want to be on.
	 */
	void SetTeamNumber(int32 NewTeamNumber);

	UFUNCTION(BlueprintCallable, Category = "PlayerState")
    int32 GetTeamNumber() const { return TeamNumber; }

	UFUNCTION()
	void OnRep_TeamNumber();

	void SetDesiredTeamNumber(int32 NewTeamNumber);

	void SwapTeamNumber();

	FOnTeamChangedDelegate OnTeamChanged;

	/** Gets truncated player name to fit in death log and scoreboards */
	FString GetShortPlayerName() const;

	/** Sends kill (excluding self) to clients */
	UFUNCTION(Client, Reliable)
	void NotifyKill(APHPlayerState* KillerPlayerState, APHPlayerState* KilledPlayerState, class UGameplayEffectUIData* UIData);

	/** Broadcast death to local clients */
	UFUNCTION(NetMulticast, Reliable)
	void BroadcastDeath(APHPlayerState* KillerPlayerState, APHPlayerState* KilledPlayerState, class UGameplayEffectUIData* UIData);

	//We don't need stats about amount of ammo fired to be server authenticated, so just increment these with local functions
	void AddBulletsFired(int32 NumBullets);
	void AddRocketsFired(int32 NumRockets);

	/** Set whether the player is a quitter */
	void SetQuitter(bool bInQuitter);

	bool CanHearTransformations() const;

protected:
	UPROPERTY(Replicated)
	int32 HunterScore;

	UPROPERTY(Replicated)
	int32 PropScore;

	UPROPERTY(Replicated)
	int32 Kills;

	UPROPERTY(Replicated)
	int32 Deaths;

	UPROPERTY(Replicated)
	int32 Taunts;

	UPROPERTY(Replicated)
	int32 Morphs;

	UPROPERTY(Replicated)
	int32 SurvivalTime;

	UPROPERTY(Replicated)
	int32 Wins;

	UPROPERTY(Replicated)
	int32 Losses;

	/** Last time since player changes team */
	UPROPERTY(Replicated)
	float TimeSinceTeamChanged;
	
	/** Value of the current the team number */
	UPROPERTY(ReplicatedUsing = OnRep_TeamNumber)
	int32 TeamNumber;

	/** Desired value of the team number which will be applied in the next round after current one */
	int32 DesiredTeamNumber;

	/** Number of bullets fired this match */
	UPROPERTY()
	int32 NumBulletsFired;

	/** Number of rockets fired this match */
	UPROPERTY()
	int32 NumRocketsFired;

	/** Whether the user quit the match */
	UPROPERTY()
	bool bQuitter;

	/** Player progress level */
	UPROPERTY(Replicated)
	int32 PlayerLevel;

	UPROPERTY(Replicated)
	int32 CurrentXP;

	UPROPERTY(Replicated)
	int32 NeededXP;

	FPrimaryAssetId HunterSkillId;

	FPrimaryAssetId PropSkillId;

	FPrimaryAssetId CustomWeaponId;

	bool bUserDataLoaded;

public:
	int32 SurvivalCount;

	int32 IncrementKills() { return ++Kills; }

	int32 IncrementDeath() { return ++Deaths; }

	void ScoreWin(bool bWinner);

	UFUNCTION(BlueprintCallable, Category = "Stats")
	int32 IncrementTauntAmount() { return ++Taunts; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	int32 IncrementMorphingAmount() { return ++Morphs; }

	UFUNCTION(BlueprintCallable, Category = "Stats")
	void AddSurvivalTime(float Time) { SurvivalTime += Time; }

	void AddXP();

	void ResetStats();

	void CreatePlayer();

	FOnLoadPlayerCompleteDelegate OnLoadPlayerCompleteDelegate;

	void LoadPlayer();

	bool IsPlayerLoaded() const { return bUserDataLoaded; }

	void SaveStats();

	//TODO Move call to Menu
	void LoadStats();

	//////////////////////////////////////////////////////////////////////////
	// Ability System
	
	// Implement IAbilitySystemInterface
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	template<class T>
	T* GetAbilitySystemComponent() const { return Cast<T>(GetAbilitySystemComponent()); }

	class UPHAttributeSet* GetAttributeSet() const { return AttributeSet; }

	/** Returns the character level that is passed to the ability system */
	UFUNCTION(BlueprintCallable, Category = "PlayerState|Attributes")
	int32 GetCharacterLevel() const;

	/** Modifies the character level, this may change abilities. Returns true on success */
	UFUNCTION(BlueprintCallable)
	virtual bool SetCharacterLevel(int32 NewLevel);

	/** Returns current movement speed */
	UFUNCTION(BlueprintCallable)
	virtual float GetMoveSpeed() const;

	/** Check if pawn is still alive */
	bool IsAlive() const;

protected:
	/** The component used to handle ability system interactions */
	UPROPERTY()
	class UPHAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(EditAnywhere, Replicated, Category = "Abilities")
	int32 CharacterLevel;

	/** Abilities to grant to this character on creation. These will be activated by tag or event and are not bound to specific inputs */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<UPHGameplayAbility>> GameplayAbilities;

	/** Passive gameplay effects applied on creation */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<UGameplayEffect>> PassiveGameplayEffects;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Character")
	TSubclassOf<UGameplayEffect> HuntingEffect;

	UPROPERTY()
	class UPHAttributeSet* AttributeSet;

	bool bAbilitiesInitialized;

	FGameplayTag InteractingTag;
	FGameplayTag InteractingRemovalTag;
	FGameplayTag EffectRemoveOnDeathTag;

	/** Apply the startup gameplay abilities and effects */
	void AddCommonStartupGameplayAbilities();

	/** Attempts to remove any startup gameplay abilities */
	void RemoveCommonStartupGameplayAbilities();

	// Called from PHAttributeSet
	virtual void HandleMoveSpeedChanged(float DeltaValue, const struct FGameplayTagContainer& EventTags);

	/** Helper for scoring points */
	virtual void HandleXPChanged(float DeltaValue, const struct FGameplayTagContainer& EventTags);

	// Friended to allow access to handle functions above
	friend class UPHAttributeSet;

private:
	int32 CurrentTauntIndex;

	TArray<int32> TauntPlaylist;

public:
	UFUNCTION(BlueprintCallable, Category = "Abilities|Taunt")
	bool GetRandTauntMediaAsset(TSoftObjectPtr<class UAkExternalMediaAsset>& TauntMediaAsset);

	void ApplyHuntingEffect();

	void AddGameplayAbilitiesAndEffects(AActor* AbilityOwner, const TArray<TSubclassOf<UPHGameplayAbility>>& GameplayAbilitiesToAdd, const TArray<TSubclassOf<UGameplayEffect>>& GameplayEffectsToAdd);

	// Removes all provided abilities. Can only be called by the Server. Removing on the Server will remove from Client too.
	void RemoveGameplayAbilitiesAndEffects(AActor* AbilityOwner, const TArray<TSubclassOf<UPHGameplayAbility>>& GameplayAbilitiesToRemove);

	void RemoveOnDeathEffects();

protected:
	FGameplayAbilitySpecHandle AddGameplayAbility(AActor* AbilityOwner, TSubclassOf<UPHGameplayAbility> GameplayAbilityToAdd, int32 Level) const;

	void ClearGameplayAbility(const FGameplayAbilitySpecHandle& Handle) const;

	FActiveGameplayEffectHandle AddGameplayEffect(AActor* EffectOwner, TSubclassOf<UGameplayEffect> GameplayEffectToAdd, int32 Level) const;

	UPROPERTY(ReplicatedUsing = OnRep_CharacterAbilityId)
	FPrimaryAssetId CharacterSkillId;

	TArray<FGameplayAbilitySpecHandle> CustomAbilitySpecHandles;

	UFUNCTION()
    void OnRep_CharacterAbilityId();

	void SetCharacterSkill(const FPrimaryAssetId& NewCharacterSkillId);

public:
	FOnCharacterSkillSetDelegate OnCharacterSkillSet;

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	const FPrimaryAssetId& GetCharacterSkill() const { return CharacterSkillId; }

	UFUNCTION()
	void InitCustomSkill(APawn* AbilityOwner);

	UFUNCTION(Server, Reliable, WithValidation)
	void OnSprayUpdated();

	void LoadPlayerSpray(TFunction<void(UTexture2D*)> LoadSprayCallback);

protected:
	UPROPERTY(Replicated)
	bool bSprayDirty;

	UPROPERTY()
	UTexture2D* CurrentSprayTexture;
};
