// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HttpClient.h"
#include "PHPlayerHttpClient.generated.h"

USTRUCT()
struct FRequest_Player
{
	GENERATED_BODY()

	UPROPERTY()
	FString uniqueNetId;

	UPROPERTY()
	FString playerName;

	UPROPERTY()
	FString onlineSubsystem;

	UPROPERTY()
	uint32 created;

	UPROPERTY()
	uint32 lastLogin;

	UPROPERTY()
	int32 characterLevel;

	UPROPERTY()
	int32 currentXP;

	UPROPERTY()
	int32 neededXP;

	UPROPERTY()
	FString hunterSkillId;

	UPROPERTY()
	FString propSkillId;

	UPROPERTY()
	FString customWeaponId;
};

USTRUCT()
struct FRequest_PlayerExp
{
	GENERATED_BODY()

	UPROPERTY()
	int32 lvl;

	UPROPERTY()
	int32 currentXP;

	UPROPERTY()
	int32 neededXP;
};

using PlayerCreatedCallback = TFunction<void(bool bSuccess)>;
using PlayerCallback = TFunction<void(FRequest_Player InPlayer)>;

UCLASS()
class PROPHUNT_API UPHPlayerHttpClient : public UHttpClient
{
	GENERATED_BODY()

public:
	UPHPlayerHttpClient();

	void CreatePlayer(const FRequest_Player& CreatePlayerRequest, PlayerCreatedCallback OnCreatePlayerCallback);

	void GetPlayer(const FUniqueNetId& UserId, PlayerCallback OnGetPlayerCallback);

	void UpdatePlayerExp(const FUniqueNetId& UserId, const FRequest_PlayerExp& PlayerExp, ResponseStatusCallback OnUpdateExpCallback);

	void UpdateAbilities(const FUniqueNetId& UserId, FString HunterSkillId, FString PropSkillId, FString CustomWeaponId, ResponseStatusCallback OnUpdateAbilitiesCallback);

protected:
	void OnCreatePlayerResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, PlayerCreatedCallback OnCreatePlayerCallback);

	void OnGetPlayerResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, PlayerCallback OnGetPlayerCallback);

	void OnUpdatePlayerExpResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateExpCallback);

	void OnUpdateAbilitiesResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateAbilitiesCallback);

private:
	FString PlayerEndpoint;
};
