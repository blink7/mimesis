// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"

#include "JsonObjectConverter.h"
#include "Runtime/Online/HTTP/Public/Http.h"

#include "HttpClient.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogHttpClient, Log, All);

using ResponseStatusCallback = TFunction<void(bool bSuccess)>;
using HttpRequestRef = TSharedRef<IHttpRequest, ESPMode::ThreadSafe>;

UCLASS(Abstract, Config = "Game")
class PROPHUNT_API UHttpClient : public UObject
{
	GENERATED_BODY()

protected:
	UPROPERTY(Config)
	FString ApiBaseUrl;

	FString AuthorizationHeader = TEXT("Authorization");

	void SetAuthorizationHash(FString Hash, HttpRequestRef& Request);

	HttpRequestRef RequestWithRoute(FString Subroute);

	void SetRequestHeaders(HttpRequestRef& Request);

	HttpRequestRef GetRequest(FString Subroute);

	HttpRequestRef PostRequest(FString Subroute, FString ContentJsonString);

	HttpRequestRef PutRequest(FString Subroute, FString ContentJsonString);

	HttpRequestRef PutRequest(FString Subroute, const TArray<uint8>& ContentPayload);

	void Send(const HttpRequestRef& Request);

	bool ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);

	template <typename StructType>
	void GetJsonStringFromStruct(const StructType& FilledStruct, FString& StringOutput);

	template <typename StructType>
	void GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput);

	template <typename StructType>
	void GetStructArrayFromJsonString(FHttpResponsePtr Response, TArray<StructType>& StructArrayOutput);
};

template <typename StructType>
void UHttpClient::GetJsonStringFromStruct(const StructType& FilledStruct, FString& StringOutput)
{
	FJsonObjectConverter::UStructToJsonObjectString(FilledStruct, StringOutput, 0, 0);
}

template <typename StructType>
void UHttpClient::GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput)
{
	FString JsonString = Response->GetContentAsString();
	FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(JsonString, &StructOutput, 0, 0);
}

template <typename StructType>
void UHttpClient::GetStructArrayFromJsonString(FHttpResponsePtr Response, TArray<StructType>& StructArrayOutput)
{
	FString JsonString = Response->GetContentAsString();
	FJsonObjectConverter::JsonArrayStringToUStruct<StructType>(JsonString, &StructArrayOutput, 0, 0);
}
