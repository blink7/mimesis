// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

THIRD_PARTY_INCLUDES_START

#include "steam/steam_api.h"
#include "steam/steam_gameserver.h"

THIRD_PARTY_INCLUDES_END

#include "OnlineAsyncTaskManager.h"


DECLARE_LOG_CATEGORY_EXTERN(LogSteamOnline, Verbose, All);

/**
 *	Steam version of the async task manager to register the various Steam callbacks with the engine
 */
class FPHOnlineAsyncTaskManagerSteam : public FOnlineAsyncTaskManager
{
private:

	/** Delegate registered with Steam to trigger when player avatar image is loaded */
	STEAM_CALLBACK(FPHOnlineAsyncTaskManagerSteam, OnAvatarImageLoaded, AvatarImageLoaded_t, OnAvatarImageLoadedCallback);

protected:

	/** Cached reference to the game instance */
	class UPHGameInstance* GameInstance;

public:

	FPHOnlineAsyncTaskManagerSteam(class UPHGameInstance* InGameInstance) :
		OnAvatarImageLoadedCallback(this, &FPHOnlineAsyncTaskManagerSteam::OnAvatarImageLoaded),
		GameInstance(InGameInstance)
	{
	}

	~FPHOnlineAsyncTaskManagerSteam()
	{
	}

	// FOnlineAsyncTaskManager
	virtual void OnlineTick() override;
};