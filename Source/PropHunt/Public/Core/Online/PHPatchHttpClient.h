﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HttpClient.h"
#include "PHPatchHttpClient.generated.h"


USTRUCT()
struct FResponse_ContentBuild
{
	GENERATED_BODY()

	UPROPERTY()
	FString ContentBuildID;

	UPROPERTY()
	TArray<int32> ChunkDownloadList;
};

using ContentBuildCallback = TFunction<void(FResponse_ContentBuild ContentBuild)>;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHPatchHttpClient : public UHttpClient
{
	GENERATED_BODY()

public:
	void GetContentBuild(ContentBuildCallback OnGetContentBuildCallback);

protected:
	void OnGetContentBuildResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ContentBuildCallback OnGetContentBuildCallback);
};
