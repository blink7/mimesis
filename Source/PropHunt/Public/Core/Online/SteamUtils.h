// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"

THIRD_PARTY_INCLUDES_START

#include "steam/steam_api.h"

THIRD_PARTY_INCLUDES_END


enum class EAvatarSize : uint8
{
	Small,	// 32x32
	Medium,	// 64x64
	Large	// 184x184, needs callback
};

/**
 * 
 */
class PROPHUNT_API FSteamUtils
{
public:
	static int GetSteamAvatar(const FString& PlayerId, EAvatarSize SizeType);

	static class UTexture2D* GetSteamImageAsTexture(int Image);

	static CSteamID StringToCSteamID(const FString& SteamId);

	static FString CSteamIDToString(const CSteamID& SteamId);
};
