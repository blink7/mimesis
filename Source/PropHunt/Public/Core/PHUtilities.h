// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"


namespace PropHunt
{
	namespace Utilities
	{
		class USkeletalMeshComponent* CopyMeshComponent(class USkeletalMeshComponent* Template, FString NameSuffix);

		void OverrideMeshMaterials(class USkeletalMeshComponent* TargetMesh, class UMaterialInterface* NewMaterial);

		FName CollisionShapeToName(ECollisionShape::Type ShapeType);

		template<typename TEnum>
		static FORCEINLINE FString GetEnumValueAsString(const FString& Name, TEnum Value)
		{
			const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
			if (!EnumPtr)
			{
				return FString("Invalid");
			}
			return EnumPtr->GetNameByValue((int64)Value).ToString();
		}

		FString TimeToString(float TimeSeconds);
	}
}