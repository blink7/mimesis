// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameViewportClient.h"
#include "PHGameViewportClient.generated.h"


class UDialog;
class UPHUserWidget;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHGameViewportClient : public UGameViewportClient
{
	GENERATED_UCLASS_BODY()

	void ShowInGameMenu();
	void HideInGameMenu();

	void ShowScoreboard();
	void HideScoreboard();

	void ShowTeamChoice();
	void HideTeamChoice();
	void ToggleTeamChoice();

	void ShowMatchResult();
	void HideMatchResult();

	UDialog* ShowDialog(const FText& InHeader, const FText& Message, const FText& OKButtonText, const FText& CancelButtonText = FText::GetEmpty());
	void HideDialog();

	void ShowDataLoadingIndicator();
	void HideDataLoadingIndicator();

	bool IsShowingInGameWidget();

	void HideExistingWidgets();

	void SetMainMenu(const TSoftObjectPtr<class UMainMenu>& InMainMenu);
	
protected:
	TSubclassOf<UDialog> DialogClass;
	TSubclassOf<UPHUserWidget> TeamChoiceClass;
	TSubclassOf<UPHUserWidget> ScoreboardClass;
	TSubclassOf<UPHUserWidget> MatchResultClass;
	TSubclassOf<class UInGameMenu> InGameMenuClass;
	TSubclassOf<UPHUserWidget> DataLoadingClass;

	UPROPERTY()
	TArray<UDialog*> DialogWidgetStack;

	UPROPERTY()
	class UInGameMenu* InGameMenu;

	UPROPERTY()
	UPHUserWidget* InGameWidget;

	UPROPERTY()
	TSoftObjectPtr<class UMainMenu> MainMenu;

	UPROPERTY()
	UPHUserWidget* HiddenInGameWidget;

	UPROPERTY()
	UPHUserWidget* DataLoadingWidget;

	void ShowWidget(TSubclassOf<UPHUserWidget> WidgetClass);

	void HideWidget(TSubclassOf<UPHUserWidget> WidgetClass);
};
