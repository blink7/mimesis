// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LocalPlayer.h"
#include "PHLocalPlayer.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHLocalPlayer : public ULocalPlayer
{
	GENERATED_BODY()
	
public:
	virtual void SetControllerId(int32 NewControllerId) override;

	virtual FString GetNickname() const;

	class UPHPersistentUser* GetPersistentUser() const;
	
	/** Initializes the PersistentUser */
	void LoadPersistentUser();

private:
	/** Persistent user data stored between sessions (i.e. the user's savegame) */
	UPROPERTY()
	class UPHPersistentUser* PersistentUser;
};
