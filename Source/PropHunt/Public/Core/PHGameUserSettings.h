// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "PHGameUserSettings.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHGameUserSettings : public UGameUserSettings
{
	GENERATED_UCLASS_BODY()

public:
	/** Applies all current user settings to the game and saves to permanent storage (e.g. file), optionally checking for command line overrides. */
	virtual void ApplySettings(bool bCheckForCommandLineOverrides) override;

	virtual void ApplyNonResolutionSettings() override;

	void ApplySoundSettings();

	virtual void SetToDefaults() override;

	void SetGraphicsToDefaults();

	void SetAudioToDefaults();

	void SetGameToDefault();

	bool IsLanMatch() const { return bLanMatch; }

	void SetIsLanMatch(bool bInLanMatch) { bLanMatch = bInLanMatch; }
	
	bool IsDedicatedServer() const { return bDedicatedServer; }

	void SetIsDedicatedServer(bool bInDedicatedServer) { bDedicatedServer = bInDedicatedServer; }

	//Sound parameters
	float GetMasterSoundVolume() const { return MasterSoundVolume; }

	void SetMasterSoundVolume(float InVolume) { MasterSoundVolume = InVolume; }
	
	float GetAmbientSoundVolume() const { return AmbientSoundVolume; }

	void SetAmbientSoundVolume(float InVolume) { AmbientSoundVolume = InVolume; }

	float GetEffectsSoundVolume() const { return EffectsSoundVolume; }

	void SetEffectsSoundVolume(float InVolume) { EffectsSoundVolume = InVolume; }

	float GetMusicSoundVolume() const { return MusicSoundVolume; }

	void SetMusicSoundVolume(float InVolume) { MusicSoundVolume = InVolume; }

	float GetMenuSoundVolume() const { return MenuSoundVolume; }

	void SetMenuSoundVolume(float InVolume) { MenuSoundVolume = InVolume; }

	float GetVoiceSoundVolume() const { return VoiceSoundVolume; }

	void SetVoiceSoundVolume(float InVolume) { VoiceSoundVolume = InVolume; }

	float GetGamma() const { return Gamma; }

	void SetGamma(float InGamma) { Gamma = InGamma; }

	bool IsGammaDirty() const;

	void SetFirstPersonFOV(float InFOV) { FirstPersonFOV = InFOV; }

	float GetFirstPersonFOV() const { return FirstPersonFOV; }

	bool IsFirstPersonFOVDirty() const;

	void SetThirdPersonFOV(float InFOV) { ThirdPersonFOV = InFOV; }

	float GetThirdPersonFOV() const { return ThirdPersonFOV; }

	bool IsThirdPersonFOVDirty() const;

	void SetSpray(const FString& NewSpray) { Spray = NewSpray; }

	UFUNCTION(BlueprintCallable, Category = "Spray")
	FString GetSpray() const { return Spray; }

	void SetTauntLang(const FString& InTauntLang) { TauntLang = InTauntLang; }
	
	FString GetTauntLang() const { return TauntLang; }

	virtual bool IsDirty() const override;

	UFUNCTION(BlueprintCallable, Category = "Settings")
	static UPHGameUserSettings* GetPHGameUserSettings();
	
private:
	UPROPERTY(Config)
	bool bLanMatch;

	UPROPERTY(Config)
	bool bDedicatedServer;

	UPROPERTY(Config)
	float MasterSoundVolume;

	UPROPERTY(Config)
	float AmbientSoundVolume;

	UPROPERTY(Config)
	float EffectsSoundVolume;

	UPROPERTY(Config)
	float MusicSoundVolume;

	UPROPERTY(Config)
	float MenuSoundVolume;

	UPROPERTY(Config)
	float VoiceSoundVolume;

	/** Holds the gamma correction setting */
	UPROPERTY(Config)
	float Gamma;

	UPROPERTY(Config)
	float FirstPersonFOV;

	UPROPERTY(Config)
	float ThirdPersonFOV;

	UPROPERTY(Config)
	FString Spray;

	UPROPERTY(Config)
	FString TauntLang;
};
