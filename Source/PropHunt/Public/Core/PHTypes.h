// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "Engine/DataTable.h"
#include "PHTypes.generated.h"

class USoundCue;
class UParticleSystem;

static const FName InteractableCollisionProfileName("Interactable");

enum EMenuType : uint8
{
	Main,
	CreateServer,
	FindServer,
	Profile,
	Rating,
	Settings,
	CommonSettings,
	GraphicsSettings,
	AudioSettings,
	ControlsSettings,
	MultiplayerSettings
};

enum ETeamType : int32
{
	None = -1,
	Hunter = 0,
	Prop = 1,
	Spectator = 2
};

UENUM()
enum class EProjectileSize : uint8
{
	Small,
	Medium,
	Large
};

UENUM(BlueprintType)
enum class EHintType : uint8
{
	Interaction,
	Morphing
};

UENUM(BlueprintType)
enum class EPointOfInterest : uint8
{
	POI_Minimap	UMETA(DisplayName = "Show on Minimap"),
	POI_Screen	UMETA(DisplayName = "Show on Screen"),
	POI_Both	UMETA(DisplayName = "Show on Minimap and Screen")
};

namespace TeamList
{
	static const TArray<FText> Teams = 
	{
		NSLOCTEXT("Team", "Hunters", "Hunters"), 
		NSLOCTEXT("Team", "Props", "Props"), 
		NSLOCTEXT("Team", "Spectators", "Spectators") 
	};
}

namespace MapList
{
	static const TArray<FString> Names = { TEXT("Office_01"), TEXT("ResearchFacility_01") };
	static const TArray<FString> DisplayedNames = { TEXT("Office"), TEXT("Research Facility") };
}

USTRUCT()
struct PROPHUNT_API FMapRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Map")
	FText Label;
	
	UPROPERTY(EditAnywhere, Category = "Map")
	FString FileName;

	UPROPERTY(EditAnywhere, Category = "Map")
	class UTexture2D* Icon;

	FMapRow() :
		Icon(nullptr)
	{}
};

/** replicated information on a hit we've taken */
USTRUCT()
struct PROPHUNT_API FTakeHitInfo
{
	GENERATED_BODY()

	/** The amount of damage actually applied */
	UPROPERTY()
	float ActualDamage;

	/** Who hit us */
	UPROPERTY()
	TWeakObjectPtr<class APawn> PawnInstigator;

	/** Who actually caused the damage */
	UPROPERTY()
	TWeakObjectPtr<class AActor> DamageCauser;

	/** Rather this was a kill */
	UPROPERTY()
	bool bKilled;

private:

	/** A rolling counter used to ensure the struct is dirty and will replicate. */
	UPROPERTY()
	uint8 EnsureReplicationByte;

public:
	FTakeHitInfo();

	void EnsureReplication();
};

USTRUCT()
struct PROPHUNT_API FDecalData
{
	GENERATED_BODY()

	/** Material */
	UPROPERTY(EditDefaultsOnly, Category = "Decal")
	class UMaterialInterface* DecalMaterial;

	/** Quad size (width & height) */
	UPROPERTY(EditDefaultsOnly, Category = "Decal")
	float DecalSize;

	/** Lifespan */
	UPROPERTY(EditDefaultsOnly, Category = "Decal")
	float LifeSpan;

	/** defaults */
	FDecalData()
		: DecalMaterial(nullptr)
		, DecalSize(256.f)
		, LifeSpan(10.f)
	{
	}
};

USTRUCT()
struct PROPHUNT_API FFXMaterialRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	TEnumAsByte<EPhysicalSurface> Surface;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	UParticleSystem* ImpactParticleFX;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	UParticleSystem* RicochetParticleTrailFX;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	UParticleSystem* DebrisParticleFX;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	USoundCue* ImpactSoundFX;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	FDecalData ImpactDecal;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	FDecalData RicochetDecal;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	FDecalData ParticleDecal;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	FDecalData RearSpatterDecal;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	USoundCue* ParticleCollisionSounds;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	bool bLiquidSurface;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	bool bHotSurface;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	bool bColdSurface;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	bool bFlammable;

	UPROPERTY(EditDefaultsOnly, Category = "FXMaterial")
	bool bCaustic;
	
	FFXMaterialRow() :
		Surface(EPhysicalSurface::SurfaceType1),
		ImpactParticleFX(nullptr),
		RicochetParticleTrailFX(nullptr),
		DebrisParticleFX(nullptr),
		ImpactSoundFX(nullptr),
		ParticleCollisionSounds(nullptr),
		bLiquidSurface(false),
		bHotSurface(false),
		bColdSurface(false),
		bFlammable(false),
		bCaustic(false)
	{}
};

USTRUCT(BlueprintType)
struct PROPHUNT_API FHint
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Hint")
	FText Message;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Hint")
	EHintType Type;
};

USTRUCT(BlueprintType)
struct PROPHUNT_API FSprayRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spray")
	class UTexture2D* Image;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spray")
	FString Label;

	FSprayRow() :
		Image(nullptr)
	{}
};
