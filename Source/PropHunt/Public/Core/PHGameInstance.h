// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Online.h"
#include "Containers/Ticker.h"
#include "Online/PHPatchHttpClient.h"
#include "UObject/PrimaryAssetId.h"
#include "PHGameInstance.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogPatch, Log, All);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAssetListLoaded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPatchCompleteDelegate, bool, bSucceeded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FChunkMountedDelegate, int32, ChunkID, bool, bSucceeded);

class USoundClass;
class FVariantData;
class ULocalPlayer;
class UPHTauntDataAsset;

namespace PHGameInstanceState
{
	extern const FName None;
	extern const FName PendingInvite;
	extern const FName WelcomeScreen;
	extern const FName MainMenu;
	extern const FName MessageMenu;
	extern const FName Playing;
}

/** This class holds the value of what message to display when we are in the "MessageMenu" state */
struct FPendingMessage
{
	FText	DisplayHeader;				// This is the header of the message window
	FText	DisplayString;				// This is the display message in the main message body
	FText	OKButtonString;				// This is the ok button text
	FText	CancelButtonString;			// If this is not empty, it will be the cancel button text
	FName	NextState;					// Final destination state once message is discarded
};

UENUM()
enum class EOnlineMode : uint8
{
	Offline,
	LAN,
	Online
};

USTRUCT(BlueprintType)
struct FPatchStats
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int32 FilesDownloaded;

	UPROPERTY(BlueprintReadOnly)
	int32 TotalFilesToDownload;

	UPROPERTY(BlueprintReadOnly)
	float DownloadPercent;

	UPROPERTY(BlueprintReadOnly)
	int32 MBDownloaded;

	UPROPERTY(BlueprintReadOnly)
	int32 TotalMBToDownload;

	UPROPERTY(BlueprintReadOnly)
	FText LastError;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPHGameInstance();

	bool Tick(float DeltaSeconds);

	class APHGameSession* GetGameSession() const;

	//~ Begin UGameInstance Interface
	virtual void Init() override;
	virtual void Shutdown() override;
	virtual void StartGameInstance() override;
	//~ End UGameInstance Interface

	bool HostGame(ULocalPlayer* InLocalPlayer, const FString& InMapName, const FString& InServerName, int32 InNumPlayers, const FString& InGameType, bool bInRecordDemo, bool bInAllowInternationalTaunts);
	virtual bool JoinSession(ULocalPlayer* LocalPlayer, int32 SessionIndexInSearchResults) override;

	bool UpdateGame(const FString& InMapName);

	/** Initiates the session searching */
	bool FindSessions(ULocalPlayer* PlayerOwner, bool bIsDedicatedServer, bool bIsLanMatch);

	/** Sends the game to the specified state. */
	void GotoState(FName NewState);

	/** Obtains the initial welcome state, which can be different based on platform */
	FName GetInitialState();

	/* Sends the game to the initial startup/frontend state  */
	void GotoInitialState();

	/**
	* Creates the message menu, clears other menus and sets the KingState to Message.
	*
	* @param	Message				Main message body
	* @param	OKButtonString		String to use for 'OK' button
	* @param	CancelButtonString	String to use for 'Cancel' button
	* @param	NewState			Final state to go to when message is discarded
	*/
	void ShowMessageThenGotoState(const FText& Header, const FText& Message, const FText& OKButtonString, const FText& CancelButtonString, const FName& NewState, const bool OverrideExisting = true, TWeakObjectPtr<ULocalPlayer> PlayerOwner = nullptr);

	class UDialog* ShowMessage(const FText& Header, const FText& Message, const FText& OKButtonString, const FText& CancelButtonString = FText::GetEmpty());

	void ShowDataLoading(bool bEnable);

	/* Returns true if the game is in online mode */
	EOnlineMode GetOnlineMode() const { return OnlineMode; }

	/** Sets the online mode of the game */
	void SetOnlineMode(EOnlineMode InOnlineMode);

	/** Updates the status of using multiplayer features */
	void UpdateUsingMultiplayerFeatures(bool bIsUsingMultiplayerFeatures);

	/** Shuts down the session, and frees any net driver */
	void CleanupSessionOnReturnToMenu();

	/** Flag the local player when they quit the game */
	void LabelPlayerAsQuitter(ULocalPlayer* LocalPlayer) const;

	/** Start task to get user privileges. */
	virtual void StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& Delegate, EUserPrivileges::Type Privilege, const FUniqueNetId& UserId);

	/** Common cleanup code for any Privilege task delegate */
	void CleanupOnlinePrivilegeTask();

	/** Show approved dialogs for various privileges failures */
	void DisplayOnlinePrivilegeFailureDialogs(const FUniqueNetId& UserId, EUserPrivileges::Type Privilege, uint32 PrivilegeResults);

	void SetMainMenu(class UMainMenu* MenuWidget);

protected:
	UPROPERTY()
	class USoundMix* SoundMix;

	UPROPERTY()
	USoundClass* MasterSound;

	UPROPERTY()
	USoundClass* CharactersSound;

	UPROPERTY()
	USoundClass* WeaponsSound;

	UPROPERTY()
	USoundClass* AmbientSound;

	TSoftObjectPtr<class UMainMenu> MainMenu;

	/** Online async task runnable */
	class FPHOnlineAsyncTaskManagerSteam* OnlineAsyncTaskThreadRunnable;

public:
	DECLARE_MULTICAST_DELEGATE_TwoParams(FOnAvatarImageLoaded, FString /*SteamId*/, int /*Image*/)
	FOnAvatarImageLoaded AvatarImageLoadedEvent;
	
protected:
	UPROPERTY(EditDefaultsOnly, Category = "Level")
	TSoftObjectPtr<UWorld> WelcomeScreenLevel;
	
	UPROPERTY(EditDefaultsOnly, Category = "Level")
	TSoftObjectPtr<UWorld> MainMenuLevel;

	FName CurrentState;
	FName PendingState;

	FPendingMessage PendingMessage;

	/** URL to travel to after pending network operations */
	FString TravelURL;

	FString ServerName;

	FString MapName;
	FString HostMapName;

	FString GameType;

	FString ServerLang;

	int32 MaxPlayers;
	int32 CurrentPlayers;

	int32 RoundCount;

	TArray<int32> TeamWins = { 0, 0 };

	bool bControllerConnected;

	/** Current online mode of the game (offline, LAN, or online) */
	EOnlineMode OnlineMode;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<class UDialog> MessageMenuClass;

	UPROPERTY()
	class UDialog* MessageMenu = nullptr;

	/** Last connection status that was passed into the HandleNetworkConnectionStatusChanged handler */
	EOnlineServerConnectionStatus::Type	CurrentConnectionStatus;

	/** Delegate for callbacks to Tick */
	FTickerDelegate TickDelegate;

	/** Handle to various registered delegates */
	FDelegateHandle TickDelegateHandle;
	FDelegateHandle TravelLocalSessionFailureDelegateHandle;
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;
	FDelegateHandle OnSearchSessionsCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	FDelegateHandle OnEndSessionCompleteDelegateHandle;
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;
	FDelegateHandle OnCreatePresenceSessionCompleteDelegateHandle;
	FDelegateHandle OnUpdateSessionCompleteDelegateHandle;

	void OnPreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel);

	void OnPostLoadMap(UWorld* LoadedWorld);

	void OnUserControllerConnectionChange(bool bConnected, FPlatformUserId UserId, int32 ControllerId);

	/** Delegate for ending a session */
	FOnEndSessionCompleteDelegate OnEndSessionCompleteDelegate;

	void OnEndSessionComplete(FName SessionName, bool bWasSuccessful);

	void MaybeChangeState();
	void EndCurrentState(FName NextState);
	void BeginNewState(FName NewState, FName PrevState);

// 	void BeginPendingInviteState();
	void BeginWelcomeScreenState();
	void BeginMainMenuState();
	void BeginMessageMenuState();
	void BeginPlayingState();

// 	void EndPendingInviteState();
// 	void EndWelcomeScreenState();
	void EndMainMenuState();
	void EndMessageMenuState();
	void EndPlayingState();

	void ShowLoadingScreen(bool bSeamlessTravel = false);
	void HideLoadingScreen();
	void AddNetworkFailureHandlers();
	void RemoveNetworkFailureHandlers();

	/** Called when there is an error trying to travel to a local session */
	void TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString& ErrorString);

	/** Callback which is intended to be called upon joining session */
	void OnJoinSessionComplete(EOnJoinSessionCompleteResult::Type Result);

	/** Callback which is intended to be called upon session creation */
	void OnCreatePresenceSessionComplete(FName SessionName, bool bWasSuccessful);

	void OnUpdateSessionComplete(FName SessionName, bool bWasSuccessful);

	/** Called after all the local players are registered */
	void FinishSessionCreation(EOnJoinSessionCompleteResult::Type Result);

	/** Called after all the local players are registered in a session we're joining */
	void FinishJoinSession(EOnJoinSessionCompleteResult::Type Result);

	/**
	* Creates the message menu, clears other menus and sets the KingState to Message.
	*
	* @param	Message				Main message body
	* @param	OKButtonString		String to use for 'OK' button
	* @param	CancelButtonString	String to use for 'Cancel' button
	*/
	void ShowMessageThenGoMain(const FText& Header, const FText& Message, const FText& OKButtonString, const FText& CancelButtonString = FText::GetEmpty());

	/** Callback which is intended to be called upon finding sessions */
	void OnSearchSessionsComplete(bool bWasSuccessful);

	bool LoadFrontEndMap(const TSoftObjectPtr<UWorld>& LevelToLoad);

	/** Sets a rich presence string for all local players. */
	void SetPresenceForLocalPlayers(const FString& StatusStr, const FVariantData& PresenceData);

	/** Travel directly to the named session */
	void InternalTravelToSession(const FName& SessionName);

	public:
	class UPHGameViewportClient* GetPHGameViewportClient();

	FString GetServerName() const { return ServerName; }

	FString GetMapName() const { return MapName; }

	FString GetHostMapName() const { return HostMapName; }

	int32 GetMaxPlayers() const { return MaxPlayers; }

	int32 GetRoundCount() const { return RoundCount; }

	TArray<int32> GetTeamWins() const { return TeamWins; };

	const FString& GetServerLang() const { return ServerLang; }

	void IncreaseRoundCount();

	void ResetRoundCount();

	void ScoreTeamWins(int32 WinnerTeam);

	void ResetTeamWins();

	void SwapTeamWins();

	UFUNCTION(BlueprintCallable, Category = "Input")
	bool IsControllerConnected() const { return bControllerConnected; }

	bool IsInMainMenu() const { return CurrentState == PHGameInstanceState::MainMenu; }

private:
	UPROPERTY(Transient)
	class UPHPatchHttpClient* PatchHttpClient;
	
	UPROPERTY(Transient)
	class UPHPlayerHttpClient* PlayerHttpClient;

	UPROPERTY(Transient)
	class UPHStatisticsHttpClient* StatisticsHttpClient;

	UPROPERTY(Transient)
	class UPHCustomizationHttpClient* CustomizationHttpClient;

	UPROPERTY(Transient)
	class UPHSprayHttpClient* SprayHttpClient;

public:
	class UPHPlayerHttpClient& GetPlayerHttpClient() const { return *PlayerHttpClient; }

	class UPHStatisticsHttpClient& GetStatisticsHttpClient() const { return *StatisticsHttpClient; }

	class UPHCustomizationHttpClient& GetCustomizationHttpClient() const { return *CustomizationHttpClient; }

	class UPHSprayHttpClient& GetSprayHttpClient() const { return *SprayHttpClient; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Data")
	class UDataTable* MapListTable;

	TArray<struct FMapRow*> MapList;
	
	TArray<class UPHSkillItem*> SkillItems;
	
	TArray<class UPHWeaponItem*> WeaponItems;
	
	TArray<UPHTauntDataAsset*> TauntDataAssets;

	void LoadAssets();

	void OnAssetsLoadCompleted();
	
private:
	TSharedPtr<struct FStreamableHandle> SkillItemsLoadHandle;
	TSharedPtr<struct FStreamableHandle> WeaponItemsLoadHandle;
	TSharedPtr<struct FStreamableHandle> TauntAssetsLoadHandle;

public:
	const TArray<struct FMapRow*>& GetMapList() const { return MapList; }
	
	const TArray<class UPHSkillItem*>& GetSkillItems() const { return SkillItems; }

	const TArray<class UPHWeaponItem*>& GetWeaponItems() const { return WeaponItems; }

	static class UPHItem* GetAbilityItem(FPrimaryAssetId ItemId);

	template<class T>
	T* GetAbilityItem(FPrimaryAssetId ItemId) { return static_cast<T*>(GetAbilityItem(ItemId)); }
	
	UPHTauntDataAsset* GetTauntDataAssetByCountry(FString CountryCode) const;

	UPROPERTY(BlueprintAssignable, Category = "Abilities")
	FAssetListLoaded OnSkillItemsLoaded;
	
	UPROPERTY(BlueprintAssignable, Category = "Abilities")
	FAssetListLoaded OnWeaponItemsLoaded;
	
	UPROPERTY(BlueprintAssignable, Category = "Taunt")
	FAssetListLoaded OnTauntAssetsLoaded;

	//Chunk Downloader

	UPROPERTY(BlueprintAssignable, Category = "Patching")
	FPatchCompleteDelegate OnPatchReady;

	UPROPERTY(BlueprintAssignable, Category = "Patching")
	FPatchCompleteDelegate OnPatchComplete;

	UFUNCTION(BlueprintCallable, Category = "Patching")
	void InitPatching(const FString& VariantName);

protected:
	void OnPatchVersionResponse(FResponse_ContentBuild ContentBuild);

public:
	UFUNCTION(BlueprintCallable, Category = "Patching")
	bool PatchGame();
	
	UFUNCTION(BlueprintPure, Category = "Patching")
	static FPatchStats GetPatchStatus();
	
protected:
	FString PatchPlatform;

	/** List of Chunk IDs to try and download */
	TArray<int32> ChunkDownloadList;

	bool bDownloadManifestUpToDate;

	bool bPatchingGame;

	bool bChunkDownloaderInitialized;

public:
	UFUNCTION(BlueprintPure, Category = "Patching")
	bool IsChunkLoaded(int32 ChunkID) const;
	
	UPROPERTY(BlueprintAssignable, Category = "Patching")
	FPatchCompleteDelegate OnSingleChunkPatchComplete;

protected:
	/** List of single chunks to download separate of the main process */
	TArray<int32> SingleChunkDownloadList;
	
	bool bDownloadingSingleChunks;

public:
	UFUNCTION(BlueprintPure, Category = "Patching")
	bool IsDownloadingSingleChunks() const { return bDownloadingSingleChunks; }

	/** Add a single chunk to the download list and start the load and mount process */
	UFUNCTION(BlueprintCallable, Category = "Patching")
	bool DownloadSingleChunk(int32 ChunkID);
};
