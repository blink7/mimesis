// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PHUserWidget.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPHUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	/** Indicates whether to allow the player to move when widget is displayed */
	bool bAllowControll;

	/** Show mouse cursor on displayed widget */
	bool bShowMouseCursor;

	/** Is this widget the most important and should be shown in UI only mode */
	bool bRespondUIOnly;

public:
	virtual void ShowWidget();

	virtual void HideWidget();

	void SetFocus();
};
