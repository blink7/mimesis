﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SPHDosBorder.h"
#include "Components/ContentWidget.h"
#include "PHDosBorder.generated.h"


/**
 * 
 */
UCLASS(meta = (DisplayName = "Dos Border"))
class PROPHUNT_API UPHDosBorder : public UContentWidget
{
	GENERATED_BODY()

public:
	/** The alignment of the content horizontally. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content")
	TEnumAsByte<EHorizontalAlignment> HorizontalAlignment;

	/** The alignment of the content vertically. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content")
	TEnumAsByte<EVerticalAlignment> VerticalAlignment;

	/** Whether or not to show the disabled effect when this border is disabled */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", AdvancedDisplay)
	bool bShowEffectWhenDisabled;

	/** Color and opacity multiplier of content in the border */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content", meta = (sRGB="true"))
	FLinearColor ContentColorAndOpacity;

	/** A bindable delegate for the ContentColorAndOpacity. */
	UPROPERTY()
	FGetLinearColor ContentColorAndOpacityDelegate;
	
	/** The padding area between the slot and the content it contains. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content")
	FMargin Padding;

	/**
	* Scales the computed desired size of this border and its contents. Useful
	* for making things that slide open without having to hard-code their size.
	* Note: if the parent widget is set up to ignore this widget's desired size,
	* then changing this value will have no effect.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	FVector2D DesiredSizeScale;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance|Header")
	bool bShowHeader;

	/** The text to display */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance|Header", meta = (EditCondition = "bShowHeader"))
	FText HeaderText;

	/** The font to render the text with */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance|Header", meta = (EditCondition = "bShowHeader"))
	FSlateFontInfo Font;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	float Thickness;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance|Header", meta = (EditCondition = "bShowHeader && BlankPosition != EBlankPosition::Center"))
	float BlankOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (EditCondition = "bDoubleBorder"))
	float SpaceWidth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance|Header", meta = (EditCondition = "bShowHeader"))
	EHeaderPosition BlankPosition;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	bool bDoubleBorder;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	FLinearColor BorderColorAndOpacity;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	bool bAnimate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (EditCondition = "bAnimate"))
	float AnimationDuration;

	UPHDosBorder(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetContentColorAndOpacity(FLinearColor InContentColorAndOpacity);

	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetPadding(FMargin InPadding);

	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetHorizontalAlignment(EHorizontalAlignment InHorizontalAlignment);

	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetVerticalAlignment(EVerticalAlignment InVerticalAlignment);

	/**
	* Sets the DesireSizeScale of this border.
	*
	* @param InScale    The X and Y multipliers for the desired size
	*/
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetDesiredSizeScale(FVector2D InScale);

	/**
	* Directly sets the header text.
	* Warning: This will wipe any binding created for the Text property!
	* @param InHeaderText The text to assign to the header
	*/
	UFUNCTION(BlueprintCallable, Category = "Appearance", meta = (DisplayName="SetHeaderText (Text)"))
	virtual void SetHeaderText(FText InHeaderText);
	
	/**
	* Dynamically set the font info for this text block
	* 
	* @param InFontInfo The new font info
	*/
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetFont(FSlateFontInfo InFontInfo);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetThickness(float InThickness);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetShowHeader(bool bInShowHeader);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetBlankOffset(float InBlankOffset);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetSpaceWidth(float InSpaceWidth);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetBlankPosition(EHeaderPosition InBlankPosition);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetDoubleBorder(bool bInDoubleBorder);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetBorderColorAndOpacity(FLinearColor InBorderColorAndOpacity);

	//~ Begin UWidget Interface
	virtual void SynchronizeProperties() override;
	//~ End UWidget Interface

	//~ Begin UVisual Interface
	virtual void ReleaseSlateResources(bool bReleaseChildren) override;
	//~ End UVisual Interface

	//~ Begin UObject Interface
	virtual void PostLoad() override;
	//~ End UObject Interface

#if WITH_EDITOR
	//~ Begin UObject Interface
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
	//~ End UObject Interface

	virtual const FText GetPaletteCategory() override;
#endif

protected:
	//~ Begin UWidget Interface
	virtual TSharedRef<SWidget> RebuildWidget() override;
	//~ End UWidget Interface

	// UPanelWidget
	virtual UClass* GetSlotClass() const override;
	virtual void OnSlotAdded(UPanelSlot* Slot) override;
	virtual void OnSlotRemoved(UPanelSlot* Slot) override;
	// End UPanelWidget
	
	TSharedPtr<SPHDosBorder> MyBorder;

	PROPERTY_BINDING_IMPLEMENTATION(FLinearColor, ContentColorAndOpacity)
};
