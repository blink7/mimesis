// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TickerWidget.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UTickerWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UScrollBox* ScrollContent;

	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ticker")
	float Speed = 40.f;

private:
	bool ScrollForward = true;

	float SleepTime;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ticker")
	float Delay = 3.f;
};
