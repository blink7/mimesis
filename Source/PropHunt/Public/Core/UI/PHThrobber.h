// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/Widget.h"
#include "PHThrobber.generated.h"


/**
 * A Throbber widget that shows several squares running along a row.
 */
UCLASS()
class PROPHUNT_API UPHThrobber : public UWidget
{
	GENERATED_UCLASS_BODY()

public:
	/** Total length */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (ClampMin = "2", ClampMax = "100", UIMin = "2", UIMax = "100"))
	int32 Length;

	/** How many pieces there are */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (ClampMin = "1", ClampMax = "100", UIMin = "1", UIMax = "100"))
	int32 NumberOfPieces;

	/** Frequency of running pieces */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (ClampMin = "0.0001", ClampMax = "1.0", UIMin = "0.0001", UIMax = "1.0"))
	float AnimationFrequency;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	FSlateBrush Image;

public:
	/** Sets total lenth */
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetLength(int32 InLength);

	/** Sets how many pieces there are */
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetNumberOfPieces(int32 InNumberOfPieces);

	/** Sets how quickly pieces are running (1 is slowest) */
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetAnimationFrequency(float InFrequency);

	//~ Begin UWidget Interface
	virtual void SynchronizeProperties() override;
	//~ End UWidget Interface

	//~ Begin UVisual Interface
	virtual void ReleaseSlateResources(bool bReleaseChildren) override;
	//~ End UVisual Interface

#if WITH_EDITOR
	virtual const FText GetPaletteCategory() override;
#endif

protected:
	//~ Begin UWidget Interface
	virtual TSharedRef<SWidget> RebuildWidget() override;
	//~ End UWidget Interface

private:
	/** The Throbber widget managed by this object. */
	TSharedPtr<class SPHThrobber> MyThrobber;
};
