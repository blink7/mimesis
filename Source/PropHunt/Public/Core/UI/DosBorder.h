// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DosBorder.generated.h"


UENUM()
enum class EBlankPosition : uint8
{
	Left,
	Center,
	Right
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UDosBorder : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* Header;

	UPROPERTY(meta = (BindWidget))
	class UNamedSlot* Content;

public:
	void SynchronizeProperties() override;

protected:
	int32 NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border")
	FLinearColor Color;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border")
	float Thickness;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border")
	FMargin ContentPadding;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border")
	bool bDoubleBorder;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border", meta = (EditCondition = "bDoubleBorder"))
	float SpaceWidth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Border")
	bool bEnableBlank;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border", meta = (EditCondition = "bEnablBlank && BlankPosition != EBlankPosition::Center"))
	float BlankOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border", meta = (EditCondition = "bEnablBlank && !bShowHeader"))
	float BlankWidth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border", meta = (EditCondition = "bEnablBlank"))
	EBlankPosition BlankPosition;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border", meta = (EditCondition = "bEnablBlank"))
	bool bShowHeader;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Border", meta = (EditCondition = "bShowHeader"))
	FText HeaderText;

	void DrawRectangle(FPaintContext& InContext, float InTop, float InBottom, float InLeft, float InRight, const FVector2D& OverallSize) const;

	UFUNCTION(BlueprintCallable, Category = "Border")
	void SetHeaderText(FText InText);

public:
	UFUNCTION(BlueprintCallable, Category = "Border")
	void SetColor(const FLinearColor& InColor);
};
