// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "Engine/DataTable.h"
#include "Styling/SlateTypes.h"
#include "StyleTypes.generated.h"


USTRUCT(BlueprintType)
struct FPHButtonStyle : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FButtonStyle Button;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateFontInfo Font;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor TextColor_Normal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor TextColor_Hover;
};

USTRUCT(BlueprintType)
struct FPHMenuStyle : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateFontInfo Font;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor Primary;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor Secondary;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor Background;
};

USTRUCT(BlueprintType)
struct FPHHUDStyle : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSlateFontInfo Font;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor PrimaryColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor SecondaryColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor TertiaryColor;
};
