// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Slider.h"
#include "PHSlider.generated.h"


/**
 * Implementation for a Slider with addition snappable Slider Handle
 */
UCLASS()
class PROPHUNT_API UPHSlider : public USlider
{
	GENERATED_BODY()

	virtual TSharedRef<SWidget> RebuildWidget() override;

	UFUNCTION()
	void HandleOnCaptureBegin();

	UFUNCTION()
	void HandleOnCaptureEnd();

public:
	/** Needs to call it every tick to animate snap behavior. */
	UFUNCTION(BlueprintCallable, Category = "Behavior")
	void Tick(float DeltaTime);

	/** Switch between min and max values with smooth animation. */
	UFUNCTION(BlueprintCallable, Category = "Behavior")
	void Switch();

	/* If true snap the value to grid with animation scaled by distance from current to target value. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behavior")
	bool bAnimationScaledByDistance = true;

protected:
	bool bThumbCaptured;

	bool bAdjusting;

	bool bSwitchAdjusting;

	float SwitchState;

	bool SetValueCustom(float InValue, float DeltaTime);
};
