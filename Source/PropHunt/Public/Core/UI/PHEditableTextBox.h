// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/EditableTextBox.h"
#include "PHEditableTextBox.generated.h"


/**
 * Implementation for a Text Box that optionally can accepts only a numeric value with range
 */
UCLASS(meta = (DisplayName = "PH Text Box"))
class PROPHUNT_API UPHEditableTextBox : public UEditableTextBox
{
	GENERATED_BODY()
	
public:
	UPHEditableTextBox();

	void SynchronizeProperties() override;

	/** Sets whether this text box is for storing only numbers */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	bool bIsNumber;

	/* Apply format to committed number */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (editcondition = "bIsNumber"))
	int32 MinimumIntegralDigits;

	/* Apply format to committed number */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (editcondition = "bIsNumber"))
	int32 MaximumIntegralDigits;

	/* Apply format to committed number */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (editcondition = "bIsNumber"))
	int32 MinimumFractionalDigits;

	/* Apply format to committed number */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (editcondition = "bIsNumber"))
	int32 MaximumFractionalDigits;

	/** Maximum amount of characters that can be entered, 0 for no limit */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	int32 MaxWidth;

	/** Minimum possibly value */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (editcondition = "bIsNumber"))
	float MinValue;

	/** Maximum possibly value */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (editcondition = "bIsNumber"))
	float MaxValue;

	/** Get numeric representation of the current text */
	UFUNCTION(BlueprintCallable, Category = "Widget", meta = (DisplayName = "GetValue (Text Box)"))
	float GetValue() const;

	/** Set numeric value as text */
	UFUNCTION(BlueprintCallable, Category = "Widget", meta = (DisplayName = "SetValue (Text Box)"))
	void SetValue(float InValue);

protected:
	virtual void HandleOnTextChanged(const FText& InText) override;
	virtual void HandleOnTextCommitted(const FText& InText, ETextCommit::Type CommitMethod) override;

	FNumberFormattingOptions NumberFormattingOptions;
};
