// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


/**
 * 
 */
class PROPHUNT_API SScreenIndicatorEntry : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SScreenIndicatorEntry)
		: _Color(FLinearColor::White),
		_Size(48.f),
		_Animate(true)
		{}

		SLATE_ARGUMENT(TWeakObjectPtr<class APlayerController>, PlayerController)
		SLATE_ARGUMENT(TWeakObjectPtr<AActor>, TargetActor)
		SLATE_ARGUMENT(class UTexture2D*, OnScreenTexture)
		SLATE_ARGUMENT(class UTexture2D*, OffScreenTexture)
		SLATE_ARGUMENT(FSlateColor, Color)
		SLATE_ARGUMENT(float, Size)
		SLATE_ARGUMENT(bool, Animate)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	void SetTexture(FSlateBrush& BrushToApply, UTexture2D* Texture);

private:
	TWeakObjectPtr<class APlayerController> PlayerController;

	TWeakObjectPtr<AActor> TargetActor;

	FSlateBrush OnScreenImageBrush;
	FSlateBrush OffScreenImageBrush;

	float PointSize;

	bool bAnimate;

	bool bOnScreen;

	FSlateRenderTransform Transform;

	FCurveSequence AnimCurve;
	FCurveHandle AnimCurveHandle;

	const FSlateBrush* GetImageBrush() const;

	TOptional<FSlateRenderTransform> GetPointTransform() const;

	FVector2D GetAnimScale() const;
	FLinearColor GetAnimColor() const;
};
