// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScreenIndicator.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UScreenIndicator : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UNativeWidgetHost* ContainerHost;

	UScreenIndicator(const FObjectInitializer& ObjectInitializer);

	void NativePreConstruct() override;

public:
	void AddTarget(AActor* TargetActor);

	void AddTarget(AActor* TargetActor, FLinearColor InColor, float InSize, UTexture2D* InOnScreenIcon = nullptr, UTexture2D* InOffScreenIcon = nullptr);

	bool RemoveTarget(AActor* TargetActor);

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	UTexture2D* OnScreenIcon;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	UTexture2D* OffScreenIcon;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	FLinearColor Color;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	float Size;

private:
	TSharedPtr<class SOverlay> Container;

	UPROPERTY()
	TMap<AActor*, int32> ActorMap;
};
