// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Core/UI/Widgets/Scoreboard_PlayerEntry.h"
#include "Scoreboard_TeamEntry.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UScoreboard_TeamEntry : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	class UListView* PlayersList;

protected:
	virtual void NativeConstruct() override;

	UPROPERTY(EditDefaultsOnly, Category = "Scoreboard")
	TSubclassOf<class UScoreboard_PlayerEntry> ScoreboardPlayerCellClass;

public:
	void AddItemAt(int32 Index, class APHPlayerState* PlayerState);

	void RemoveItemAt(int32 Index);

	void SetupWidget(class UScoreboard* InOwner, bool bInForHunter, int32 InTeamScore);

protected:
	UFUNCTION()
	void OnItemHovered(UObject* InHoveredItem, bool bHovered);

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "SetupWidget"))
	void K2_SetupWidget(bool bForHunter, int32 TeamScore);

private:
	UPROPERTY()
	class UScoreboard* Owner;
};
