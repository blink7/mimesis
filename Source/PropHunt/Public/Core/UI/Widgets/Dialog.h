// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/PHUserWidget.h"
#include "Dialog.generated.h"


DECLARE_EVENT(UDialog, FOnDialogButtonClickedEvent);

UENUM(BlueprintType)
enum class EDialogType : uint8
{
	Message,
	Filter,
	ServerPassword,
	OpenFile
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UDialog : public UPHUserWidget
{
	GENERATED_BODY()

	friend class UOpenFileWidget;
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UButtonWrapper* ConfirmButton;
	
	UPROPERTY(meta = (BindWidget))
	class UButtonWrapper* CancelButton;

	UPROPERTY(Transient, BlueprintReadOnly, meta = (BindWidgetAnim))
	class UWidgetAnimation* ShowAnimation;

public:
	void SynchronizeProperties() override;

protected:
	void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;

	void NativeConstruct() override;

	void NativeOnFocusLost(const FFocusEvent& InFocusEvent) override;
	FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

	void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

	UFUNCTION(BlueprintCallable, Category = "Button")
	void OnClickedConfirm();

	UFUNCTION(BlueprintCallable, Category = "Button")
	void OnClickedCancel();

private:
	bool bConfirming;

	bool bCanceling;

public:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Dialog")
	EDialogType Type;

	FOnDialogButtonClickedEvent OnConfirmClicked;

	FOnDialogButtonClickedEvent OnCancelClicked;

	UPROPERTY(EditInstanceOnly, Category = "Dialog")
	FText ConfirmButtonText;

	UPROPERTY(EditInstanceOnly, Category = "Dialog")
	FText CancelButtonText;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Dialog")
	FText Header;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Dialog")
	FText Message;

	void EnableConfirmButon(bool bEnable);

	UUserWidget* GetWidgetCell() const { return WidgetCell; }

	template <class T>
	T* GetWidgetCell() const { return Cast<T>(WidgetCell); }

	void ConstructMessageDialog(const FText& InHeader, const FText& InMessage, const FText& InConfirmButtonText, const FText& InCancelButtonText = FText::GetEmpty());

	void ConstructOpenFileDialog(const FText& InHeader, const FText& InConfirmButtonText, const FText& InCancelButtonText = FText::GetEmpty());

protected:
	UPROPERTY(BlueprintReadWrite, Category = "Dialog")
	UUserWidget* WidgetCell;
};
