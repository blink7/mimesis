// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "KillLogEntry.generated.h"


class UTextBlock;

struct FKillLogItemData
{
	/** Name of player scoring kill. */
	FString KillerDesc;

	/** Name of killed player. */
	FString VictimDesc;

	/** Killer is local player. */
	bool bKillerIsOwner;

	/** Victim is local player. */
	bool bVictimIsOwner;

	/** Team number of the killer. */
	int32 KillerTeamNum;

	/** Team number of the victim. */
	int32 VictimTeamNum;

	/** What killed the player. */
	TWeakObjectPtr<class UTexture2D> KillIcon;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UKillLogEntry : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* KillerName;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* VictimName;
	
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UImage* KillIcon;

public:
	void SetMessage(const FKillLogItemData& Message);

protected:
	UPROPERTY(BlueprintReadOnly, Category = "KillLog")
	bool bKillerIsHunter;
};
