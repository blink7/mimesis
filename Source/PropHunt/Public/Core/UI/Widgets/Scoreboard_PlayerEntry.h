// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "Blueprint/UserWidget.h"
#include "Scoreboard_PlayerEntry.generated.h"


class UTextBlock;
class UScoreboard;

UCLASS()
class UScoreboardItemData : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY()
	class APHPlayerState* PlayerState;

	DECLARE_DELEGATE_OneParam(FOnServerHover, bool)
	FOnServerHover OnItemHoverEvent;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UScoreboard_PlayerEntry : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UImage* PlayerAvatar;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* PlayerName;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* PlayerScore;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* PlayerPing;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	virtual void NativeOnListItemObjectSet(UObject* ListItemObject) override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnItemHoverChanged(bool bNewHover);

public:
	void LoadAvatar(const FString& OnlinePlayerId);

private:
	UPROPERTY()
	UScoreboardItemData* ItemData;
};
