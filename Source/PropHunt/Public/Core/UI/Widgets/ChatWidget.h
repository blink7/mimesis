// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ChatWidget.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UChatWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UListView* ChatHistoryList;
	
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UEditableTextBox* ChatEditBox;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	class UWidgetAnimation* ExpandAnimation;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UUserWidget* ChatHistoryOverlay;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class URichTextBlock* ChatStatusText;

public:
	UChatWidget(const FObjectInitializer& ObjectInitializer);

protected:
	void NativeConstruct() override;

	UFUNCTION()
	void OnChatTextCommitted(const FText& Text, ETextCommit::Type CommitMethod);

public:
	void AddChatLine(const FString& SenderName, const FString& Message, int32 Team, bool bInTeammate, bool bSystem = false);

	void OpenChat(bool bInTeammate);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Appearance")
	FName StyleTableRowName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Appearance")
	class UDataTable* StyleTable;

	bool bTeammate;

	void ShowEditBox(bool bEnable);
};
