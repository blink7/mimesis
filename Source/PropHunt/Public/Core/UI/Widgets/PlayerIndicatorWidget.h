// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerIndicatorWidget.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UPlayerIndicatorWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* PlayerName;
	
	UPROPERTY(meta = (BindWidget))
	class UImage* PlayerAvatar;
	
	UPROPERTY(meta = (BindWidget))
	class UBorder* AvatarBorder;

protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public:
	void SetUp(class APHPlayerState* ForPlayerState);

protected:
	class APawn* OwnerPawn;

	float LastRenderTime = 0.f;

	/** Maximum distance between players, on which the widget has an opacity of 1. */
	float FullOpacityDistanceThreshold = 10.f;

	void LoadAvatar(const FString& OnlinePlayerId);
};
