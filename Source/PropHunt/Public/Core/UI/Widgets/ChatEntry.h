// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "ChatEntry.generated.h"


UCLASS()
class UChatItemData : public UObject
{
	GENERATED_BODY()

public:
	FString Message;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UChatEntry : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()
	
	UPROPERTY(meta = (BindWidget))
	class URichTextBlock* Message;

protected:
	void NativeOnListItemObjectSet(UObject* ListItemObject) override;
};
