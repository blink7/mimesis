// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LiveStatus.generated.h"


/**
 *
 */
UCLASS()
class PROPHUNT_API ULiveStatus : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* List;

public:
	ULiveStatus(const FObjectInitializer& ObjectInitializer);

	void Setup(int32 TeamNumber);

	void UpdatePlayers(uint8 LivePlayers, uint8 TotalPlayers);

	bool IsEmpty() const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Appearance")
	FName StyleTableRowName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Appearance")
	class UDataTable* StyleTable;

	UPROPERTY(EditDefaultsOnly, Category = "Appearance")
	TSubclassOf<UUserWidget> EntryTemplate;

	FLinearColor DeadColor;

	FLinearColor LiveColor;
};
