﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Scoreboard_PlayerDetails.generated.h"


class UTextBlock;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UScoreboard_PlayerDetails : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void OnSetup(class APHPlayerState* PlayerState);

	UFUNCTION(BlueprintImplementableEvent)
	void OnClear();
};
