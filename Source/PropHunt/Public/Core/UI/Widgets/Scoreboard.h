// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/PHUserWidget.h"
#include "Scoreboard.generated.h"


class UPanelWidget;
class APHPlayerState;
class UScoreboard_TeamEntry;

/** Ranked PlayerState map, created from the GameState */
using RankedPlayerMap = TMap<int32, TWeakObjectPtr<APHPlayerState>>;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UScoreboard : public UPHUserWidget
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* ServerName;
	
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* MapName;
	
	UPROPERTY(meta = (BindWidget))
	UScoreboard_TeamEntry* TeamCellA;
	
	UPROPERTY(meta = (BindWidget))
	UScoreboard_TeamEntry* TeamCellB;
	
	UPROPERTY(meta = (BindWidget))
	UPanelWidget* DetailsContainerTop;
	
	UPROPERTY(meta = (BindWidget))
	UPanelWidget* DetailsContainerBottom;
	
	//~ Begin UPHUserWidget Interface
	virtual void ShowWidget() override;
	virtual void HideWidget() override;
	//~ End UPHUserWidget Interface

private:
	void BindEvents();

	UFUNCTION()
	void OnPlayerStateChanged(APlayerState* Owner);

	uint8 MainTeam;

	void SetupDetails(APHPlayerState* PlayerState, UPanelWidget* ShowDetailsContainer, UPanelWidget* HideDetailsContainer);

public:
	void DisplayDetails(APHPlayerState* PlayerState);

protected:
	/** Updates widgets when players leave or join */
	void UpdateScoreboardGrid();

	void FillTeamWidget(UScoreboard_TeamEntry* TeamCell, int32 TeamNumber);

	/** Updates PlayerState maps to display accurate scores */
	void UpdatePlayerStateMaps();

	/** the Ranked PlayerState map...cleared every frame */
	TArray<RankedPlayerMap> PlayerStateMaps;

	/** player count in each team in the last tick */
	TArray<int32> LastTeamPlayerCount;

	UPROPERTY(EditDefaultsOnly, Category = "Scoreboard")
	TSubclassOf<class UScoreboard_PlayerDetails> ScoreboardPlayerDetailsClass;
};
