// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Core/PHTypes.h"
#include "HUDWidget.generated.h"

class UTextBlock;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UHUDWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* KillLog;

	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* InfoMessageList;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* MatchTimer;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* TeamA_Label;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* TeamB_Label;

	UPROPERTY(meta = (BindWidget))
	class ULiveStatus* TeamA_LiveStatus;

	UPROPERTY(meta = (BindWidget))
	class ULiveStatus* TeamB_LiveStatus;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UChatWidget* ChatWidget;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	class UWidgetAnimation* HitAnimation;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UMinimap* Minimap;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UScreenIndicator* ScreenIndicator;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	TSubclassOf<class UKillLogEntry> KillLogEntryClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	TSubclassOf<class UInfoMessageEntry> InfoMessageEntryClass;

	UFUNCTION()
	void SetTimer(int32 Time);

	void AddMessage(const FText& Message, float LifeTime = 0.f, bool bPersistent = false);

	void AddDeathMessage(const struct FKillLogItemData& Message);

	void OpenChat(bool bTeammate) const;

	void AddChatLine(const FString& SenderName, const FString& Message, int32 TeamNumber, bool bTeammate, bool bSystem);

	void AddPlayerToMinimap(class APawn* PlayerPawn);

	void ClearPlayersFromMinimap();

	void ZoomMinimap() const;

	void AddTargetOnMinimap(AActor* TargetActor, UTexture2D* Icon, FLinearColor Color, bool bStatic);

	void AddTargetOnScreen(AActor* TargetActor, UTexture2D* Icon, FLinearColor Color, float Size);

	void RemovePointOfInterest(AActor* TargetActor);

	void UpdateMatchStats(uint8 AlivePlayersTeamA, uint8 TotalPlayersTeamA, uint8 AlivePlayersTeamB, uint8 TotalPlayersTeamB);

	void SetupMatchStats(const FText& TitleTeamA, const FText& TitleTeamB, int32 PrimaryTeamNumber);

	void NotifyEnemyHit();

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnUpdateColorScheme(int32 TeamNumber);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnShowPortalStatus();

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void StartInteractionTimer(float Duration);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void StopInteractionTimer();

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnShowHint(const FHint& Hint);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnSetCrosshairVisibility(bool bVisible);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnSetHealthBoxVisibility(bool bVisible);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnSetupStatusBox(bool bForHunter);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnSetupSkillBox(class UPHItem* AbilityItem);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void OnShowDeadMessage(bool bForProp);

	friend class APHHUD;

private:
	UPROPERTY()
	class UInfoMessageEntry* PersistentMessageEntry;
};
