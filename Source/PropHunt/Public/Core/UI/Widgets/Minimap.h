// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Minimap.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UMinimap : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UNativeWidgetHost* POIHost;

	void NativePreConstruct() override;

public:
	void AddTargetActor(AActor* TargetActor, class UTexture2D* Texture, FLinearColor Color = FLinearColor::White, bool bStatic = false);

	void AddTargetPawn(class APawn* PlayerPawn);

	void RemoveActor(AActor* ActorToRemove);

	void ClearPawns();

	void Clear();

	void ZoomMinimap();

protected:
	UPROPERTY(BlueprintReadOnly)
	float Zoom;

	UPROPERTY(BlueprintReadOnly)
	float DefaultZoom;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
	float ZoomDelta;

	UPROPERTY(BlueprintReadWrite)
	float Dimensions;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "PlayerIcon")
	class UTexture2D* PlayerTexture;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "PlayerIcon")
	FLinearColor PlayerColor;

	UFUNCTION(BlueprintCallable)
	void SetDefaultZoom(float Value);
								
	float GetFactor() const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void OnZoomChanged(float NewValue);

private:
	TSharedPtr<class SOverlay> Container;

	UPROPERTY()
	TArray<class APawn*> PawnList;

	UPROPERTY()
	TMap<AActor*, int32> ActorMap;

	uint8 CurrentZoomIndex;
};
