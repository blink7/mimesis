// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MapVoteCell.generated.h"


class UTextBlock;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UMapVoteCell : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
	class UButton* SelectButton;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* VoteCount;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* MapName;

public:
	virtual bool Initialize() override;

protected:
	DECLARE_EVENT(UMapVoteCell, FOnMapSelected);
	FOnMapSelected MapSelectedEvent;

	UFUNCTION()
	void OnMapClicked();

public:
	void SetMapName(FString InMapName);

	void SetVoteCount(int32 InVoteCount);

	void SetActive(bool bEnable);

	FOnMapSelected& OnMapSelected() { return MapSelectedEvent; }
};
