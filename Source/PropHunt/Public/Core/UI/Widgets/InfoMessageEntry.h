// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InfoMessageEntry.generated.h"


/**
 *
 */
UCLASS()
class PROPHUNT_API UInfoMessageEntry : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, Category = "InfoMessage")
	void Setup(const FText& InMessage, float InLifeTime);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "InfoMessage")
	void SetMessage(const FText& InMessage);
};
