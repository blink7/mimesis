// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OpenFileWidget.generated.h"

/**
 * 
 */
UCLASS()
class PROPHUNT_API UOpenFileWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UOpenFileWidget(const FObjectInitializer& ObjectInitializer);
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class USettingsItemCell_ComboBox* PathSelector;

	UPROPERTY(meta = (BindWidget))
	class UListView* ItemList;

	virtual void NativeConstruct() override;

	void SetupDiskSelector(const FString& CurrentDir);

	void RebuildList(const FString& RelativeToDirectory);

	UFUNCTION()
	void OnItemClicked(UObject* MyListView);

	UFUNCTION()
	void OnItemDoubleClicked(UObject* MyListView);

	UFUNCTION()
	void OnItemHovered(UObject* InHoveredItem, bool bHovered);

	UFUNCTION()
	void OnDiskSelectorChanged();

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "OpenFile")
	TArray<FString> ExtensionFilter;

	UPROPERTY(BlueprintReadWrite, Category = "OpenFile")
	class UDialog* DialogOwner;

	FString GetCurrentPath();
};
