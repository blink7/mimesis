// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "OpenFileEntry.generated.h"


UENUM(BlueprintType)
enum class EOpenFileItemType : uint8
{
	Directory,
	File
};

UCLASS(BlueprintType)
class UOpenFileItemData : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "AbilityItemData")
	FString DirectoryPath;

	UPROPERTY(BlueprintReadOnly, Category = "AbilityItemData")
	FString DisplayName;

	UPROPERTY(BlueprintReadOnly, Category = "AbilityItemData")
	EOpenFileItemType Type;

	DECLARE_DELEGATE(FOnUpdateState)
	FOnUpdateState OnUpdateStateDelegate;

	DECLARE_DELEGATE_OneParam(FOnOpenFileItemHover, bool);
	FOnOpenFileItemHover OnOpenFileItemHover;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UOpenFileEntry : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* Label;

	void NativeOnListItemObjectSet(UObject* ListItemObject) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "OpenFileEntry")
	void OnItemHovered(bool bHovered);
};
