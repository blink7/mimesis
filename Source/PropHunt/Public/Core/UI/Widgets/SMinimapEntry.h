// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


/**
 * 
 */
class PROPHUNT_API SMinimapEntry : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SMinimapEntry)
		: _Color(FLinearColor::White),
		_Factor(1),
		_Size(16.f)
		{}

		SLATE_ARGUMENT(TWeakObjectPtr<AActor>, OwnerActor)
		SLATE_ARGUMENT(TWeakObjectPtr<AActor>, TargetActor)
		SLATE_ARGUMENT(class UTexture2D*, Texture)
		SLATE_ARGUMENT(FSlateColor, Color)
		SLATE_ATTRIBUTE(float, Factor)
		SLATE_ARGUMENT(bool, Static)
		SLATE_ARGUMENT(float, Size)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	void SetTexture(UTexture2D* Texture);

private:
	TWeakObjectPtr<AActor> OwnerActor;

	TWeakObjectPtr<AActor> TargetActor;

	FSlateBrush ImageBrush;

	TAttribute<float> Factor;

	bool bStatic;

	float PointSize;

	bool bVisible;

	FSlateRenderTransform Transform;

	const FSlateBrush* GetImageBrush() const;

	EVisibility GetPointVisibility() const;

	TOptional<FSlateRenderTransform> GetPointTransform() const;
};
