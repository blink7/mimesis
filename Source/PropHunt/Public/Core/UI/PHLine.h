﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ContentWidget.h"
#include "PHLine.generated.h"


/**
 * 
 */
UCLASS(meta = (DisplayName = "Line"))
class PROPHUNT_API UPHLine : public UContentWidget
{
	GENERATED_BODY()

public:
	/** Whether or not to show the disabled effect when this border is disabled */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", AdvancedDisplay)
	bool bShowEffectWhenDisabled;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	float Thickness;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	FLinearColor LineColorAndOpacity;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	bool bHorizontal;

	UPHLine(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetThickness(float InThickness);
	
	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetLineColorAndOpacity(FLinearColor InLineColorAndOpacity);

	UFUNCTION(BlueprintCallable, Category = "Appearance")
	void SetHorizontal(bool bInHorizontal);

	//~ Begin UWidget Interface
	virtual void SynchronizeProperties() override;
	//~ End UWidget Interface

	//~ Begin UVisual Interface
	virtual void ReleaseSlateResources(bool bReleaseChildren) override;
	//~ End UVisual Interface

#if WITH_EDITOR
	virtual const FText GetPaletteCategory() override;
#endif
	
protected:
	//~ Begin UWidget Interface
	virtual TSharedRef<SWidget> RebuildWidget() override;
	//~ End UWidget Interface

	TSharedPtr<class SPHLine> MyLine;
};
