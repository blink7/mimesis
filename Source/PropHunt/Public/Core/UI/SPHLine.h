﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"


/**
 * 
 */
class PROPHUNT_API SPHLine : public SCompoundWidget
{
public:
SLATE_BEGIN_ARGS(SPHLine)
		: _LineColorAndOpacity(FLinearColor::White),
		_Thickness(3.f),
		_bHorizontal(true),
		_ShowEffectWhenDisabled(true)
		{}

		SLATE_ATTRIBUTE(FSlateColor, LineColorAndOpacity)
		SLATE_ARGUMENT(float, Thickness)
		SLATE_ARGUMENT(bool, bHorizontal)
		/** Whether or not to show the disabled effect when this border is disabled */
		SLATE_ATTRIBUTE(bool, ShowEffectWhenDisabled)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	// Begin of SWidget interface
	virtual int32 OnPaint( const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled ) const override;
	// End of SWidget interface

	void SetThickness(float InThickness);

	void SetLineColorAndOpacity(const TAttribute<FSlateColor>& InColorAndOpacity);

	void SetHorizontal(bool bInHorizontal);
	
	/** See ShowEffectWhenDisabled attribute */
	void SetShowEffectWhenDisabled(const TAttribute<bool>& InShowEffectWhenDisabled);
	
private:
	TAttribute<FSlateColor> LineColorAndOpacity;
	
	float Thickness;
	
	bool bHorizontal;
	
	/** Whether or not to show the disabled effect when this border is disabled */
	TAttribute<bool> ShowDisabledEffect;
};
