// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SubSettings.h"
#include "GraphicsSubSettings.generated.h"


class USettingsItem;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UGraphicsSubSettings : public USubSettings
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	USettingsItem* WindowMode;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Resolution;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* FrameRateLimit;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* VerticalSync;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Gamma;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* FirstPersonFOV;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* ThirdPersonFOV;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* GraphicsQuality;
	
	UPROPERTY(meta = (BindWidget))
	USettingsItem* ResolutionScale;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* ViewDistance;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* AntiAliasing;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* PostProcessing;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Shadows;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Textures;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Effects;

	virtual void NativeOnInitialized() override;
	virtual void NativeConstruct() override;

	void LoadOptions();

	/** Get current options values for display */
	void UpdateOptions();

	void LoadQualityOptions();

	void UpdateQualityOptions();

public:
	virtual void ApplySettings() override;
	virtual void SetToDefaults() override;
	virtual void RevertChanges() override;

	void AutoDetect();

protected:
	float ResolutionScaleOpt;

	int32 WindowModeOpt;

	FIntPoint ResolutionOpt;

	float FrameRateLimitOpt;

	bool bUseVSyncOpt;

	float GammaOpt;

	float FirstPersonFOVOpt;

	float ThirdPersonFOVOpt;

	int32 GraphicsQualityOpt;

	int32 ViewDistanceQualityOpt;

	int32 AntiAliasingQualityOpt;

	int32 PostProcessingQualityOpt;

	int32 ShadowQualityOpt;

	int32 TextureQualityOpt;

	int32 EffectsQualityOpt;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Graphics")
	float MinGamma = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Graphics")
	float MaxGamma = 5.f;

	void UpdateGraphicsQualityOption();

	void SaveGraphicsQualityOption();

	void CheckGraphicsQuality();

	void OnWindowModeChanged();

	void OnResolutionChanged();

	void OnFrameRateLimitChanged();

	void OnVerticalSyncChanged();

	void OnGammaChanged();

	void OnFirstPersonFOVChanged();

	void OnThirdPersonFOVChanged();

	void OnGraphicsQualityChanged();

	void OnResolutionScaleChanged();

	void OnViewDistanceChanged();

	void OnAntiAliasingChanged();

	void OnPostProcessingChanged();

	void OnShadowsChanged();

	void OnTexturesChanged();

	void OnEffectsChanged();
};
