// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MatchResultCell.generated.h"


class UTextBlock;

enum class ERewardType : uint8
{
	None,
	BestHunter,
	BestProp,
	MostTaunts,
	MostTransforms,
	MostInconspicuous
};

struct FPlayerInfo
{
	FPlayerInfo() = default;

	FPlayerInfo(
		ERewardType InRewardType, 
		FString InOnlinePlayerId, 
		FString InPlayerName, 
		int32 InScore
	) : RewardType(InRewardType), 
		OnlinePlayerId(InOnlinePlayerId), 
		PlayerName(InPlayerName), 
		Score(InScore) 
	{}

	ERewardType RewardType;
	FString OnlinePlayerId;
	FString PlayerName;
	int32 Score;

	bool operator==(const FPlayerInfo& Other)
	{
		return OnlinePlayerId == Other.OnlinePlayerId 
			&& RewardType == Other.RewardType;
	}

	bool operator!=(const FPlayerInfo& Other)
	{
		return !(*this == Other);
	}
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UMatchResultCell : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
	UTextBlock* RewardTitle;

	UPROPERTY(meta = (BindWidget))
	class UImage* PlayerAvatar;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* PlayerName;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Score;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* Description;

public:
	void SetUp(FPlayerInfo InPlayerInfo);

	void Refresh();

protected:
	FPlayerInfo PlayerInfo;

	TPair<FText, FText> GetTextReward(ERewardType RewardType);

private:
	void LoadAvatar();

	void ApplySteamAvatar(FString SteamId, int Image);
};
