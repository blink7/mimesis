// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "AbilityEntry.generated.h"


enum class EApplyAnimation : uint8
{
	Stop,
	PlayForward,
	PlayReverse
};

UENUM(BlueprintType)
enum class EAbilityStatus : uint8
{
	Available,
	NotAvailable,
	Applied
};

UCLASS(BlueprintType)
class UAbilityItemData : public UObject
{
	GENERATED_BODY()

public:
	bool bRoot;

	int32 Team;

	UPROPERTY(BlueprintReadOnly, Category = "AbilityItemData")
	TArray<UAbilityItemData*> Children;

	UPROPERTY(BlueprintReadOnly, Category = "AbilityItemData")
	class UPHItem* AbilityItem;

	UPROPERTY(BlueprintReadOnly, Category = "AbilityItemData")
	EAbilityStatus Status;

	DECLARE_DELEGATE(FOnUpdateState)
	FOnUpdateState OnUpdateStateDelegate;

	DECLARE_DELEGATE_OneParam(FOnAbilityHover, bool)
	FOnAbilityHover OnAbilityHoverEvent;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UAbilityEntry : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* StatusText;

	UPROPERTY(meta = (BindWidget))
	class USizeBox* StatusSize;

	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	//~ Begin IUserObjectListEntry Interface
	void NativeOnListItemObjectSet(UObject* ListItemObject) override;
	//~ End IUserObjectListEntry Interface

protected:
	UPROPERTY()
	UAbilityItemData* ItemData;

	UPROPERTY(EditDefaultsOnly, Category = "AbilityEntry")
	float ApplyAnimationDuration = 0.2f;

	UFUNCTION(BlueprintCallable, Category = "AbilityEntry")
	EAbilityStatus GetStatus() const;

	UFUNCTION(BlueprintCallable, Category = "AbilityEntry")
	bool IsRoot() const;

	UFUNCTION(BlueprintImplementableEvent, Category = "AbilityEntry|Event")
	void Setup(const FText& Label, bool bRoot);

	UFUNCTION(BlueprintImplementableEvent, Category = "AbilityEntry|Event")
	void OnChangeHover(bool bNewHover);

	void OnUpdateStatus();

private:
	EApplyAnimation ApplyAnimation;
};
