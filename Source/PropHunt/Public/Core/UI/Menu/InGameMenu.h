// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/PHUserWidget.h"
#include "InGameMenu.generated.h"


class UButtonWrapper;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UInGameMenu : public UPHUserWidget
{
	GENERATED_UCLASS_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ContinueButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* SettingsButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* MainMenuButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* QuitButton;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;

// 	UPROPERTY(meta = (BindWidget))
	class USettingsMenu* SettingsMenu;

	UPROPERTY(meta = (BindWidget))
	class USecondaryMenu* SecondaryMenu;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	UWidgetAnimation* NavigationFadeInAnim;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	UWidgetAnimation* BackgroundFadeInAnim;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
	UWidgetAnimation* MenuFadeOutAnim;
	
protected:
	void NativeConstruct() override;

public:
	FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

protected:
	bool bPendingToHide;

	void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	
	void ReturnToMe();
	
	UFUNCTION()
	void OnHideMenu();

	UFUNCTION()
	void OnOptionsSelected();

	UFUNCTION()
	void OnExitToMain();

	/** Delegate called when user confirms confirmation dialog to exit to main menu */
	UFUNCTION()
	void OnConfirmExitToMain();

	/** Quits the game */
	UFUNCTION()
	void OnQuit();

	UFUNCTION()
	void OnConfirmQuit();
};
