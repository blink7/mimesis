// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SubMenu.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "FindServerMenu.generated.h"


class UButtonWrapper;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UFindServerMenu : public USubMenu
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* ServerCount;
	
	UPROPERTY(meta = (BindWidget))
	class UListView* ServerList;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* EventLog;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* RefreshButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ConnectButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ReturnButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* FilerButton;

public:
	virtual void NativeConstruct() override;

protected:
	virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

	virtual void ReturnToMainMenu() override;

	virtual void Setup() override;

	enum class EMatchType
	{
		Custom,
		Quick
	};

	/** Custom match or quick match */
	EMatchType MatchType;

	/** Map filter name to use during server searches */
	FString MapFilterName;

	/** Currently selected list item */
	UPROPERTY()
	class UServerItemData* SelectedServer;

	/**
	 * Get the current game session
	 *
	 * @return The current game session
	 */
	class APHGameSession* GetGameSession() const;

	/** Start the check for whether the owner of the menu has online privileges */
	void StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& Delegate);

	/** Delegate function executed after checking privileges for joining an online game */
	void OnUserCanPlayOnlineJoin(const FUniqueNetId& UserId, EUserPrivileges::Type Privilege, uint32 PrivilegeResults);

	/** Common cleanup code for any Privilege task delegate */
	void CleanupOnlinePrivilegeTask();

	/** Starts searching for servers */
	void BeginServerSearch();

	void OnFindSessionsComplete(bool bWasSuccessful);

	/** Called when server search is finished */
	void OnServerSearchFinished();

	UFUNCTION()
	void OnServerHovered(UObject* InHoveredServer, bool bHovered);

	/** Selection changed handler */
	UFUNCTION()
	void OnServerSelectionChanged(UObject* InSelectedServer);

	/** UListView item double clicked */
	UFUNCTION()
	void OnServerDoubleClicked(UObject* InDoubleClickedServer);

	UFUNCTION()
	void OnFindServerSelected();

	void OnJoinServer();

	/** Connect to chosen server */
	UFUNCTION()
	void ConnectToServer();

	/** Selects item at current + MoveBy index */
	void MoveSelection(int32 MoveBy);

	bool bIsLanMatch;

	bool bIsRecordingDemo;

	bool bIsDedicatedServer;

	/** Time the last search began */
	double LastSearchTime;

	/** Minimum time between searches (platform dependent) */
	double MinTimeBetweenSearches;

	/** Whether we're searching for servers */
	bool bSearchingForServers;

	FText GetMapLabel(const FString& MapName);

private:
	bool bJoiningServer;
	
	void TurnJoiningStatus();
};
