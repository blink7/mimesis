// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SettingsItemCell.h"
#include "SettingsItemCell_Slider.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API USettingsItemCell_Slider : public USettingsItemCell
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UButton* DecreaseButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* IncreaseButton;

	UPROPERTY(meta = (BindWidget))
	class USlider* SliderValue;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* ProgressValue;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UPHEditableTextBox* ValueText;

public:
	virtual void SynchronizeProperties() override;

protected:
	UFUNCTION()
	void ValueTextCommitted(const FText& Text, ETextCommit::Type CommitMethod);

	UFUNCTION()
	void Decrease();

	UFUNCTION()
	void Increase();

	UFUNCTION()
	void OnSliderChanged(float Value);

	UFUNCTION()
	void OnSliderCaptureEnd();

	void CheckForMinValue();

	void CheckForMaxValue();

	float CurrentValue;

	float MinValue;

	float MaxValue;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Content")
	float Division = 10.f;

	void UpdateView();

	virtual float GetFloatValue() override { return CurrentValue; };

public:
	void SetCurrentValue(float InValue);

	void SetRange(float InMinValue, float InMaxValue);

	UFUNCTION(BlueprintCallable, Category = "Settings")
	void Setup(float InValue, const FVector2D& InRange);
};
