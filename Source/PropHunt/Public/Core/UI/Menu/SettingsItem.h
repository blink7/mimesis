// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SettingsItemCell.h"
#include "SettingsItem.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnSettingsItemHoverEvent, bool);

UENUM(BlueprintType)
enum class ESettingsItemType : uint8
{
	ComboBox,
	Text,
	Slider
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API USettingsItem : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UBorder* Container;

	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;
	virtual void SynchronizeProperties() override;
	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;

public:
	FOnSettingsItemHoverEvent OnHover;

	FOnValueChangedEvent OnValueChangedEvent;
	FOnValueCommittedEvent OnValueCommittedEvent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance")
	FText Label;

	UFUNCTION(BlueprintImplementableEvent, Category = "AbilityEntry|Event")
	void OnChangeHover(bool bNewHover);

	bool SetCurrentValue(const FText& InValue);

	bool SetCurrentValue(const FString& InValue);

	bool SetCurrentValue(float InValue);

	void SetSelectedIndex(int32 Index);

	void SetItems(const TArray<FString>& InItems) { Items = InItems; }

	void AddItem(const FString& InItem);

	void ClearItems();

	int32 Num() const;

	template <class Type >
	Type GetValue();

	template <>
	int32 GetValue()
	{
		if (WidgetCell)
		{
			return WidgetCell->GetIntegerValue();
		}

		return 0;
	}

	template <>
	float GetValue()
	{
		if (WidgetCell)
		{
			return WidgetCell->GetFloatValue();
		}

		return 0.f;
	}

	template <>
	bool GetValue()
	{
		if (WidgetCell)
		{
			return WidgetCell->GetBooleanValue();
		}

		return false;
	}

	template <>
	FString GetValue()
	{
		if (WidgetCell)
		{
			return WidgetCell->GetStringValue();
		}

		return FString();
	}

	UFUNCTION(BlueprintCallable, Category = "SettingsItem")
	void SetActive(bool bInActive);

protected:
	UPROPERTY()
	USettingsItemCell* WidgetCell;

	UPROPERTY(EditDefaultsOnly, Category = "Appearance")
	TSubclassOf<class USettingsItemCell_ComboBox> ComboBoxClass;

	UPROPERTY(EditDefaultsOnly, Category = "Appearance")
	TSubclassOf<class USettingsItemCell_Text> TextClass;

	UPROPERTY(EditDefaultsOnly, Category = "Appearance")
	TSubclassOf<class USettingsItemCell_Slider> SliderClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "SettingsItemType == ESettingsItemType::ComboBox"), Category = "Content")
	TArray<FString> Items;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Content")
	FText CurrentValue;

	FString CurrentValueString;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "SettingsItemType == ESettingsItemType::Slider"), Category = "Content")
	float MinValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "SettingsItemType == ESettingsItemType::Slider"), Category = "Content")
	float MaxValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance")
	ESettingsItemType SettingsItemType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "SettingsItemType == ESettingsItemType::Text"), Category = "Appearance")
	FText HintText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "SettingsItemType == ESettingsItemType::Text"), Category = "Appearance")
	bool bIsPassword;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Appearance")
	bool bActive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Appearance")
	float InactiveOpacity = 0.4f;

	void OnValueChanged() const;
	void OnValueCommitted() const;
};
