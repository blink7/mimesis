// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SubSettings.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class PROPHUNT_API USubSettings : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidgetOptional))
	class UPanelWidget* ItemList;

	virtual void NativeOnInitialized() override;
	virtual void NativeConstruct() override;

public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDirty, bool);
	FOnDirty OnDirtyEvent;

	DECLARE_MULTICAST_DELEGATE_TwoParams(FOnSettingHovered, FText, FText);
	FOnSettingHovered OnSettingHoveredEvent;

	/** Called when this sub menu has been displayed by pressing the corresponding menu button */
	virtual void OnOpened() {}

	virtual void ApplySettings();
	virtual void SetToDefaults();
	virtual void RevertChanges();

	UFUNCTION(BlueprintCallable, Category = "SubSettings")
	const FText& GetDescription(int32 Index) const;

	bool IsDirty() const { return bDirty > 0; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SubSettings")
	TArray<FText> ItemDescriptions;

	/** User settings pointer */
	UPROPERTY()
	class UPHGameUserSettings* UserSettings;

	uint64 bDirty;

	void OnSettingChanged(const UWidget* InItemWidget, bool bInDirty);

	void OnSettingChanged(int32 InIndex, bool bInDirty);

	void ResetDirtyState();

	int32 GetItemIndex(const UWidget* InItem);

	/** Get the persistence user associated with PlayerOwner*/
	class UPHPersistentUser* GetPersistentUser() const;

	void OnSettingHovered(int32 Index, FText Label);
};
