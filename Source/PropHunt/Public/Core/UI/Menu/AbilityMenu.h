// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SubProfile.h"
#include "AbilityMenu.generated.h"


class UTextBlock;
class UAbilityItemData;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UAbilityMenu : public USubProfile
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTreeView* AbilityList;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* RequiredLvl;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* UsesEnergy;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* Description;

	UPROPERTY(meta = (BindWidget))
	class UButtonWrapper* ApplyButton;

	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;

public:
	UFUNCTION(BlueprintCallable, Category = "AbilityMenu")
	void LoadAbilities();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "AbilityMenu")
	class USoundBase* ApplySound;

	UPROPERTY()
	UAbilityItemData* CurrentHunterAbility;

	UPROPERTY()
	UAbilityItemData* CurrentPropAbility;

	UPROPERTY()
	UAbilityItemData* SelectedAbility;

	int32 CurrentHuntersAbilityIndex;
	int32 CurrentPropsAbilityIndex;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AbilityMenu")
	FSlateColor ValidLvlColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AbilityMenu")
	FSlateColor NotValidLvlColor;

	void FillAbilities(int32 ForTeam);

	void OnAbilityHovered(UObject* InAbilityItem, bool bIsHovered);

	void OnAbilitySelected(UObject* InAbilityItem);

	void OnAbilityExpansionChanged(UObject* InAbilityItem, bool bExpanded);

	void UpdateDetails(UAbilityItemData* ItemData);

	UFUNCTION()
	void OnClickApply();

	void ApplyAbility(UAbilityItemData** CurrentAbility);
};
