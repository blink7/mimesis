// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SettingsItemCell.h"
#include "SettingsItemCell_ComboBox.generated.h"

/**
 * 
 */
UCLASS()
class PROPHUNT_API USettingsItemCell_ComboBox : public USettingsItemCell
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UComboBoxString* ComboBox;

public:
	virtual void SynchronizeProperties() override;

	virtual int32 GetIntegerValue() override;
	virtual bool GetBooleanValue() override;
	virtual FString GetStringValue() override;

	UFUNCTION(BlueprintCallable, Category = "Settings")
	void Setup(const TArray<FString>& InDataSet, const FString& InCurrentValue);

	void SetSelectedIndex(int32 Index);

	void SetSelectedOption(const FString& Option);

	void AddOption(const FString& Option);

	bool RemoveOption(const FString& Option);

	void ClearOptions();

protected:
	UFUNCTION()
	void OnSelectionChanged(FString SelectedItem, ESelectInfo::Type SelectionType);
};
