// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SubSettings.h"
#include "MultiplayerSubSettings.generated.h"

USTRUCT()
struct FSprayItem
{
	GENERATED_BODY()

	UPROPERTY()
	UTexture2D* Image;

	UPROPERTY()
	FString Label;

	UPROPERTY()
	bool bCustom;

	FSprayItem(UTexture2D* InImage, FString InLabel, bool bInCustom = false) : 
		Image(InImage), 
		Label(InLabel), 
		bCustom(bInCustom)
	{}

	FSprayItem() : FSprayItem(nullptr, FString())
	{}

	bool operator==(const FString& SearchLabel) const { return Label.Equals(SearchLabel); }
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UMultiplayerSubSettings : public USubSettings
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UButtonWrapper* ImportSprayButton;

	UPROPERTY(meta = (BindWidget))
	class USettingsItem* SprayImage;

	UPROPERTY(meta = (BindWidget))
	class UImage* SprayPreview;

	virtual void NativeOnInitialized() override;
	virtual void NativeConstruct() override;

	void LoadOptions();

	/** Get current options values for display */
	void UpdateOptions();

public:
	virtual void OnOpened() override;
	virtual void ApplySettings() override;
	virtual void SetToDefaults() override;
	virtual void RevertChanges() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Spray")
	class UDataTable* DefaultSprayListTable;

	TArray<FSprayItem> SprayList;

	UPROPERTY(EditDefaultsOnly, Category = "OpenFileDialog")
	TSubclassOf<class UDialog> DialogClass;

	UPROPERTY()
	class UDialog* ImportSprayDialog;

	FString SprayDirectory;

	void OnSprayImageChanged();

	UFUNCTION()
	void OnImportSpray();

	UFUNCTION()
	void ConfirmImportSpray();

	UFUNCTION()
	void HideOpenSprayDialog();

	void HandleNewSpray(const FString& SprayPath);

	UFUNCTION()
	void OnUserSprayLoaded(UTexture2D* SprayTexture, const FString& Path);

	UFUNCTION()
	void OnGameFolderSprayLoaded(UTexture2D* SprayTexture, const FString& Path);

	FString OnSprayLoaded(UTexture2D* SprayTexture, const FString& Path);

	void SaveSprayToDisk(UTexture2D* Texture, const FString& Name);

	void LoadSpraysFolder();

private:
	FString SprayOpt;

	int32 SprayDirectoryLoadCounter;
};
