// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "ServerEntry.generated.h"


UCLASS()
class UServerItemData : public UObject
{
	GENERATED_BODY()

public:
	FString ServerName;
	FString CurrentPlayers;
	FString MaxPlayers;
	FText MapLabel;
	FString Ping;
	int32 SearchResultsIndex;

	DECLARE_DELEGATE_OneParam(FOnServerHover, bool)
	FOnServerHover OnItemHoverEvent;
};


class UTextBlock;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UServerEntry : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* ServerName;
	
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* Map;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* Players;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* Ping;

	//~ Begin IUserObjectListEntry Interface
	void NativeOnListItemObjectSet(UObject* ListItemObject) override;
	void NativeOnItemSelectionChanged(bool bIsSelected) override;
	//~ End IUserObjectListEntry Interface

	UFUNCTION(BlueprintImplementableEvent, Category = "Server Row")
	void OnChangeSelection(bool bNewSelection);

	UFUNCTION(BlueprintImplementableEvent, Category = "Server Row")
	void OnChangeHover(bool bNewHover);
};
