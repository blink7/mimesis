// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SubMenu.h"
#include "CreateServerMenu.generated.h"


class USettingsItem;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UCreateServerMenu : public USubMenu
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	USettingsItem* ServerName;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* PlayersNumber;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* GameMode;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Password;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* LanMatch;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* InternationalTaunts;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* RecordingDemo;

	UPROPERTY(meta = (BindWidget))
	class UImage* MapIcon;

	UPROPERTY(meta = (BindWidget))
	class UListView* MapList;

	UPROPERTY(meta = (BindWidget))
	class UButtonWrapper* CreateButton;

	UPROPERTY(meta = (BindWidget))
	class UButtonWrapper* ReturnButton;

public:
	virtual void SynchronizeProperties() override;

protected:
	virtual void NativeConstruct() override;

	virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

	enum class EMap
	{
		EOffice_01,
		ResearchFacility_01,
		EMax
	};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings")
	int32 ServerNameMinLength = 3;

	UFUNCTION()
	void OnCreateServerSelected();

	bool IsServerNameValid() const;

	/** Hosts a game */
	void HostGame();

	UFUNCTION()
	void OnServerNameChanged();

	UFUNCTION()
	void OnServerNameCommitted();

	struct FMapRow* GetSelectedMapRow(class USelectionEntry* InSelectedMap) const;

	UFUNCTION()
	void OnMapHovered(UObject* InHoveredMap, bool bHovered);

	UFUNCTION()
	void OnMapSelected(UObject* InSelectedMap);

	/** Checks the ChunkInstaller to see if the selected map is ready for play */
	bool IsMapReady() const;

	EMap GetSelectedMap() const;

	/** Returns the string name of the currently selected map */
	FString GetMapName() const;
};
