// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "RatingEntry.h"

#include "RatingMenu.generated.h"


class UButton;

/**
 * 
 */
UCLASS()
class PROPHUNT_API URatingMenu : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UPanelWidget* ItemList;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* Title;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UButton* RefreshButton;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UButton* ReturnButton;

	UPROPERTY(meta = (BindWidget))
	UButton* ShowMeButton;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* Switcher;

	void NativeConstruct() override;

	void NativeOnFocusLost(const FFocusEvent& InFocusEvent) override;
	FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

public:
	DECLARE_MULTICAST_DELEGATE(FOnNavigate)
	FOnNavigate OnReturnToMainMenuEvent;

	FOnNavigate OnShowMeEvent;

	void Setup();

	void DisplayRating();

protected:
	UPROPERTY(EditInstanceOnly, Category = "Menu")
	TSubclassOf<class URatingEntry> RatingItemClass;

	void LoadRating();

	UFUNCTION()
	void RefreshRating();

	UFUNCTION()
	void ReturnToMainMenu();

	UFUNCTION()
	void OnShowMe();

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Menu")
	TMap<FName, FText> RatingLabels;

	TArray<FRatingItemData> Ratings;

	int32 PrevRating;
};
