// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/PHUserWidget.h"
#include "MainMenu.generated.h"


class UButtonWrapper;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UMainMenu : public UPHUserWidget
{
	GENERATED_BODY()

public:
	UMainMenu(const FObjectInitializer& ObjectInitializer);

protected:
	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* CreateServerButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* FindServerButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ProfileButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* RatingButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* SettingsButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* QuitButton;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UBorder* DialogContainer;

	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;

	virtual void NativeOnFocusLost(const FFocusEvent& InFocusEvent) override;
	virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

public:
	UPROPERTY(BlueprintReadWrite, Category = "Sequence")
	class ALevelSequenceActor* SequenceActor;

	UPROPERTY(EditInstanceOnly, Category = "Sequence")
	class ULevelSequence* SecondaryMenuSequence;

	UPROPERTY(EditInstanceOnly, Category = "Sequence")
	class ULevelSequence* RatingMenuSequence;

	UPROPERTY(BlueprintReadWrite, Category = "Menu")
	class USecondaryMenu* SecondaryMenu;

	UPROPERTY(BlueprintReadWrite, Category = "Menu")
	class URatingMenu* RatingMenu;

	UFUNCTION(BlueprintCallable, Category = "Menu")
	void Setup(class USecondaryMenu* InSecondaryMenu, class URatingMenu* InRatingMenu);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "MessageDialog")
	TSubclassOf<class UDialog> DialogClass;

	UPROPERTY()
	class UDialog* Dialog;

	/** Quits the game */
	UFUNCTION()
	void OnQuit();

	UFUNCTION()
	void ConfirmQuit();

	void OnPlaySequenceAndOpenMenu(uint8 InMenuType);

	UFUNCTION()
	void OnOpenMenu();

	void OnPlayerLoaded();

private:
	uint8 CurrentMenuType;

	bool bPlayingSequence;

public:
	void ReturnToMe();

	class UDialog* ShowMessage(const FText& InHeader, const FText& InMessage, const FText& OkButtonString, const FText& CancelButtonString = FText::GetEmpty());

	UFUNCTION()
	void HideMessage();

protected:
	UFUNCTION()
	void OnPatchReady(bool bSuccess);

	UFUNCTION()
	void OnPatchComplete(bool bSuccess);

	UFUNCTION(BlueprintImplementableEvent, Category = "Patching")
	void OnBeginPatch();

	UFUNCTION(BlueprintImplementableEvent, Category = "Patching")
	void OnBeginMenu();

	UPROPERTY(BlueprintReadOnly, Category = "Patching")
	bool bPatchingGame;
};
