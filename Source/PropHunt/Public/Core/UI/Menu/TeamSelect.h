// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/PHUserWidget.h"
#include "TeamSelect.generated.h"


/** Timed PlayerState map, created from the GameState */
using TimedPlayerMap = TMap<int32, TWeakObjectPtr<class APHPlayerState>>;

class UPanelWidget;
class UButtonWrapper;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UTeamSelect : public UPHUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* TimerText;

	UPROPERTY(meta = (BindWidget))
	UPanelWidget* HunterList;

	UPROPERTY(meta = (BindWidget))
	UPanelWidget* PropList;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* JoinHuntersButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* JoinPropsButton;

	UPROPERTY(EditDefaultsOnly, Category = "List Entries")
	TSubclassOf<class UTeamSelectEntry> TeamSelectEntryClass;

public:
	UTeamSelect(const FObjectInitializer& ObjectInitializer);

	//~ Begin UUserWidget Interface
	virtual void NativeConstruct() override;
	//~ End UUserWidget Interface

	virtual void ShowWidget() override;

	virtual void HideWidget() override;

private:
	void BindEvents();

	UFUNCTION()
	void UpdateMatchStartTimer(int32 Time);

	UFUNCTION()
    void OnPlayerStateChanged(APlayerState* Owner);

	bool bInitialized;

	int32 InTeam = -1;

	/** Handle for efficient management of BindMatchTimerChanges timer */
	FTimerHandle TimerHandle_BindMatchTimerChanges;

protected:
	void OnJoinTeamSelected(int32 TeamNumber);

	/** Updates PlayerState maps to display accurate scores */
	void UpdatePlayerStateMaps();

	/** Updates widgets when players change team, leave or join */
	void UpdateTeamTable();

	void SetTableItem(const struct FPlayerData& PlayerData, int32 NumTeams, int32 TeamNumber, int32 Index);

	/** PlayerState map...cleared every frame */
	TArray<TimedPlayerMap> PlayerStateMaps;

	bool AllowedToJoinByBalance(int32 TeamNumber, int32 MaxPlayers);

private:
	UPanelWidget* GetTeamWidget(int32 TeamNumber) const;

	UButtonWrapper* GetTeamButton(int32 TeamNumber) const;
};
