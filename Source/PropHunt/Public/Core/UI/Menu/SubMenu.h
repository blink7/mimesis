// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SubMenu.generated.h"


/**
 * 
 */
UCLASS(Abstract)
class PROPHUNT_API USubMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	USubMenu(const FObjectInitializer& ObjectInitializer);
	
protected:
	FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

	UFUNCTION()
	virtual void ReturnToMainMenu();

public:
	UPROPERTY()
	class USecondaryMenu* SecondaryMenu = nullptr;

	DECLARE_EVENT_OneParam(USubMenu, FOnShowDialog, class UDialog*);
	FOnShowDialog OnShowDialogEvent;

	virtual void Setup() {}

	virtual void OnPlayerLoaded() {}

protected:
	bool bControlsLocked;

	void LockControls(bool bEnable) { bControlsLocked = bEnable; }
};
