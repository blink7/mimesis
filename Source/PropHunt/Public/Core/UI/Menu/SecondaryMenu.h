// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SecondaryMenu.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API USecondaryMenu : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* Path;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;

	UPROPERTY(meta = (BindWidget))
	class UBorder* DialogContainer;

public:
	DECLARE_MULTICAST_DELEGATE(FOnNavigate)
	FOnNavigate OnReturnToMainMenuEvent;

	void OnSwitchMenu(uint8 InMenuState);

	void ReturnToMainMenu();

	void ShowDialog(class UDialog* DialogToShow);

	void ReturnFocusToSubMenu();

	void OnPlayerLoaded();
};
