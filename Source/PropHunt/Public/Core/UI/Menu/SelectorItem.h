// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "SelectorItem.generated.h"


UCLASS()
class USelectionEntry : public UObject
{
	GENERATED_BODY()

	DECLARE_DELEGATE_OneParam(FOnEntryHover, bool)

public:
	FText Label;
	bool bEnabled = true;
	FOnEntryHover OnItemHoverEvent;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API USelectorItem : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* Label;

	//~ Begin IUserObjectListEntry Interface
	void NativeOnListItemObjectSet(UObject* ListItemObject) override;
	void NativeOnItemSelectionChanged(bool bIsSelected) override;
	//~ End IUserObjectListEntry Interface

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "State")
	bool bActive = true;

	UFUNCTION(BlueprintImplementableEvent, Category = "Map Selection")
	void OnChangeSelection(bool bNewSelection);

	UFUNCTION(BlueprintImplementableEvent, Category = "Map Selection")
	void OnChangeHover(bool bNewHover);
};
