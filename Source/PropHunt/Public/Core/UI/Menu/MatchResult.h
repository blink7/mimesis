// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/PHUserWidget.h"
#include "MatchResult.generated.h"


class UTextBlock;
enum class ERewardType : uint8;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UMatchResult : public UPHUserWidget
{
	GENERATED_BODY()
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* RemainingTimeText;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* RemainingTimeBar;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* BestTeam;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* BestTeamScore;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* BestTeamWins;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* CenterWidgetSwitcher;

	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* RewardList;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* CurrentLevel;
	
	UPROPERTY(meta = (BindWidget))
	UTextBlock* NextLevel;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* ProgressXP;

	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* NextMapVoteList;

public:
	UMatchResult(const FObjectInitializer& ObjectInitializer);

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

protected:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void UpdateTimer(int32 Time);

	void BindMatchTimerChanges();

	void UpdateBestTeam();

	void SetBestTeam(int32 TeamNumber, uint8 Score, uint8 Wins);

	void FillRewards();

	void CreateRewardCard(class APHPlayerState* PlayerState, int32 Score, ERewardType RewardType);

	void FillVoteList();

	void UpdateMapVotes();

	void ShowLevelUp();

	void UpdateLevelUp(float InDeltaTime);

	void RunLevelUpUpdate();

	void OnNextMapSelected(int32 MapIndex);

protected:
	TSubclassOf<class UMapVoteCell> MapVoteCellClass;
	TSubclassOf<class UMatchResultCell> MatchResultCellClass;

	TArray<uint8> MapVotes;

	int32 NextMapVote;

	int32 Level;

	int32 CurrentXP;

	int32 NeededXP;

	bool bRequiresLevelUpUpdate;
};
