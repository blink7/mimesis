// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TeamSelectEntry.generated.h"


USTRUCT()
struct FPlayerData
{
	GENERATED_USTRUCT_BODY()

	FString OnlinePlayerId;

	bool bIsOwnerPlayer;

	FString PlayerName;

	FPlayerData() : bIsOwnerPlayer(false)
	{}

	FORCEINLINE bool IsEmpty() { return OnlinePlayerId.IsEmpty(); }

	bool operator==(const FPlayerData& Other) { return OnlinePlayerId.Equals(Other.OnlinePlayerId); }

	bool operator!=(const FPlayerData& Other) { return !(*this == Other); }
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API UTeamSelectEntry : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UImage* PlayerAvatar;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* PlayerName;

public:
	void Refresh();

	void SetPlayerData(FPlayerData NewPlayerData);

private:
	FPlayerData PlayerData;

	void LoadAvatar();
};
