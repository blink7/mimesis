// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SettingsItemCell.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnValueChangedEvent);
DECLARE_MULTICAST_DELEGATE(FOnValueCommittedEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCellHoveredEvent, bool, bHovered);

/**
 * 
 */
UCLASS(Abstract)
class PROPHUNT_API USettingsItemCell : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
	
public:
	virtual int32 GetIntegerValue() { return 0; }
	
	virtual float GetFloatValue() { return 0.f; }
	
	virtual bool GetBooleanValue() { return false; }
	
	virtual FString GetStringValue() { return FString(); }

	FOnValueChangedEvent OnValueChangedEvent;
	FOnValueCommittedEvent OnValueCommittedEvent;

	UPROPERTY(BlueprintAssignable, Category = "SettingsItemCell|Event")
	FOnCellHoveredEvent OnHovered;
};
