// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SettingsItemCell.h"
#include "SettingsItemCell_Text.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API USettingsItemCell_Text : public USettingsItemCell
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UEditableTextBox* ValueText;

public:
	virtual void SynchronizeProperties() override;

	UFUNCTION(BlueprintCallable, Category = "Settings")
	void Setup(const FText& Text, const FText& HintText, bool bIsPassword);

protected:
	virtual FString GetStringValue() override;

	UFUNCTION()
	void OnTextChanged(const FText& Text);

	UFUNCTION()
	void OnTextCommitted(const FText& Text, ETextCommit::Type CommitMethod);
};
