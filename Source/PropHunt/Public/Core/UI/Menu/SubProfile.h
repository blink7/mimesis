// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SubProfile.generated.h"

/**
 * 
 */
UCLASS()
class PROPHUNT_API USubProfile : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UButtonWrapper* ReturnButton;

	void NativeConstruct() override;

public:
	DECLARE_EVENT(USubProfile, FOnReturn)
	FOnReturn OnReturnEvent;
};
