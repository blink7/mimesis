// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SubSettings.h"
#include "AudioSubSettings.generated.h"


class USettingsItem;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UAudioSubSettings : public USubSettings
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	USettingsItem* Master;
	
	UPROPERTY(meta = (BindWidget))
	USettingsItem* Ambient;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Effects;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Music;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Menu;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* Voice;

	virtual void NativeOnInitialized() override;
	virtual void NativeConstruct() override;

	void LoadOptions();

	/** Get current options values for display */
	void UpdateOptions();

public:
	virtual void ApplySettings() override;
	virtual void SetToDefaults() override;
	virtual void RevertChanges() override;

protected:
	float MasterSoundVolumeOpt;
	
	float AmbientSoundVolumeOpt;

	float EffectsSoundVolumeOpt;

	float MusicSoundVolumeOpt;

	float MenuSoundVolumeOpt;

	float VoiceSoundVolumeOpt;

	void OnMasterVolumeChanged();
	
	void OnAmbientVolumeChanged();

	void OnEffectsVolumeChanged();

	void OnMusicVolumeChanged();

	void OnMenuVolumeChanged();

	void OnVoiceVolumeChanged();
};
