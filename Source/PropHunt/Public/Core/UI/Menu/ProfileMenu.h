// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SubMenu.h"
#include "ProfileMenu.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API UProfileMenu : public USubMenu
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* SubProfileSwitcher;

	UPROPERTY(meta = (BindWidget))
	class UAbilityMenu* AbilityMenu;

public:
	void Setup() override;

	void OnPlayerLoaded() override;
};
