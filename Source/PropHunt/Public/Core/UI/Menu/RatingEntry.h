// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RatingEntry.generated.h"


USTRUCT(BlueprintType)
struct FRatingItemData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "Rating")
	FText PlayerName;

	UPROPERTY(BlueprintReadOnly, Category = "Rating")
	FText Score;

	UPROPERTY(BlueprintReadOnly, Category = "Rating")
	bool bOwner;
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API URatingEntry : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetData(const FRatingItemData& InData) { Data = InData; }

	UPROPERTY(BlueprintReadOnly, Category = "Rating")
	FRatingItemData Data;
};
