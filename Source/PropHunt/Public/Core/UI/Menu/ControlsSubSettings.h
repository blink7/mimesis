// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/UI/Menu/SubSettings.h"
#include "ControlsSubSettings.generated.h"


class USettingsItem;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UControlsSubSettings : public USubSettings
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	USettingsItem* InvertYAxis;

	UPROPERTY(meta = (BindWidget))
	USettingsItem* MouseSensitivity;

	virtual void NativeOnInitialized() override;
	virtual void NativeConstruct() override;

	void LoadOptions();

	/** Get current options values for display */
	void UpdateOptions();

public:
	virtual void ApplySettings() override;
	virtual void SetToDefaults() override;
	virtual void RevertChanges() override;

protected:
	bool bInvertYAxisOpt;

	/** Current sensitivity set in options */
	float SensitivityOpt;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Controls")
	float MinSensitivity = 0.01f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Controls")
	float MaxSensitivity = 2.f;

	void OnInvertYAxisChanged();

	void OnMouseSensitivityChanged();
};
