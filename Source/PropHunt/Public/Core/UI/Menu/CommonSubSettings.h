﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SubSettings.h"
#include "CommonSubSettings.generated.h"


class USettingsItem;

/**
 * 
 */
UCLASS()
class PROPHUNT_API UCommonSubSettings : public USubSettings
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	USettingsItem* TauntLang;

	virtual void NativeOnInitialized() override;
	virtual void NativeConstruct() override;

	void LoadOptions();

	/** Get current options values for display */
	void UpdateOptions();

public:
	virtual void ApplySettings() override;
	virtual void SetToDefaults() override;
	virtual void RevertChanges() override;

protected:
	FString TauntLangOpt;

	void OnTauntLangChanged();

private:
	TMap<FString, FString> LocalizationMap;
};
