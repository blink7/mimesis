// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SubMenu.h"
#include "SettingsMenu.generated.h"


class USubSettings;
class UButtonWrapper;

/**
 * 
 */
UCLASS()
class PROPHUNT_API USettingsMenu : public USubMenu
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* CommonButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* GraphicsButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* AudioButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ControlsButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* MultiplayerButton;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* SettingLabel;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* SettingDescription;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* SubSettingsSwitcher;

	UPROPERTY(meta = (BindWidget))
	USubSettings* CommonSettings;

	UPROPERTY(meta = (BindWidget))
	USubSettings* GraphicsSettings;

	UPROPERTY(meta = (BindWidget))
	USubSettings* AudioSettings;

	UPROPERTY(meta = (BindWidget))
	USubSettings* ControlsSettings;

	UPROPERTY(meta = (BindWidget))
	USubSettings* MultiplayerSettings;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* AutoGraphicsButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ResetButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ApplyButton;

	UPROPERTY(meta = (BindWidget))
	UButtonWrapper* ReturnButton;

	void NativeConstruct() override;
	
	void ChangeNavigationState(bool bActive);

	void ReturnToMainMenu() override;

public:
	void Setup() override;

	void OnMenuSelected(uint8 InType);

	void SelectMenu(uint8 InType);

	/** UI callback for applying settings */
	void OnApplySettings();

	void OnAutoDetectGraphics();

	void OnResetSettings();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "MessageDialog")
	TSubclassOf<class UDialog> DialogClass;

	UPROPERTY()
	class UDialog* Dialog = nullptr;

	UPROPERTY()
	USubSettings* CurrentSubSettings;

	uint8 CurrentMenuType;

	void SwitchSettings(uint8 SettingsToOpen, bool bSaveCurrent);

	void Return(bool bSaveCurrent);

	void HandleAfterDialog(bool bSaveCurrent);
};
