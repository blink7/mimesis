﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/PHTypes.h"
#include "GameFramework/HUD.h"
#include "PHHUD.generated.h"

enum EInfoMessageType : uint8
{
	INFO_None,
	INFO_Hiding,
	INFO_RoundEnd,
	INFO_UpdateStatus,
	INFO_Portal
};

UENUM(BlueprintType)
enum class EBonusType : uint8
{
	BONUS_Taunt			UMETA(DisplayName = "Taunt"),
	BONUS_Morphing		UMETA(DisplayName = "Morphing"),
	BONUS_Survival		UMETA(DisplayName = "Survival")
};

UCLASS()
class PROPHUNT_API APHHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;
	virtual void ShowHUD() override;

protected:
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<class UUserWidget> HUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
	float CrossThickness;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
	float CrossLength;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
	FVector2D CrossSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Appearance")
	FLinearColor CrossColor;

public:
	void Setup();
	void RemoveHUDWidget();
	
	void ShowHUD(bool bShow);

	/**
	* Add death message.
	*
	* @param	KillerPlayerState	Player that did the killings state.
	* @param	VictimPlayerState	Played that was killed state.
	* @param	UIData				Data with icon and other related things.
	*/
	void ShowDeathMessage(class APHPlayerState* KillerPlayerState, class APHPlayerState* VictimPlayerState, class UGameplayEffectUIData* UIData);

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void ShowBonus(EBonusType BonusType, int32 Value);

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void ShowMorphingSizeError();

	void ShowInfoMessage(EInfoMessageType MessageType, float Duration);

	void ShowWinner(int32 TeamNumber);

	void OpenChat(bool bTeammate) const;

	void AddChatLine(APlayerState* SenderPlayerState, const FString& Message, FName Type, float MsgLifeTime);

	void AddPlayersToMinimap(const TArray<APawn*>& PlayerPawns);
	
	void ClearPlayersFromMinimap();

	void ZoomMinimap() const;

	void AddPointOfInterest(AActor* TargetActor, EPointOfInterest DisplayType, UTexture2D* MinimapIcon, UTexture2D* ScreenIcon, float Size, FLinearColor Color, bool bStatic) const;

	void RemovePointOfInterest(AActor* TargetActor) const;

	void NotifyEnemyHit() const;

	void UpdateReadyStatus(int32 ReadyPlayers, int32 TotalPlayers);

	void ShowPortalStatus() const;
	
	void StartInteractionTimer(float Duration);

	void StopInteractionTimer() const;
	
	void ShowHint(const struct FHint& Hint);

private:
	UPROPERTY()
	class UHUDWidget* HUDWidget;

	/** Player count in each team in the last tick */
	TArray<uint8> LastTeamLivePlayerCount;

	void BindEvents();

	UFUNCTION()
	void OnRemainingTimeChanged(int32 Time);

	void SetupSkillBox();

	/** Updates PlayerState maps to display accurate scores */
	void UpdatePlayerStateMaps();

	void UpdateMatchStats(int32 OwnerTeamNumber);

	FText GetKeyName(const FName& InActionName) const;
	
	void DrawCrosshair();
};
