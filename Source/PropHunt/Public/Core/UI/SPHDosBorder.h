﻿// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


UENUM(BlueprintType)
enum class EHeaderPosition : uint8
{
	Left,
	Center,
	Right
};

struct FBorderGeometry
{
	float Top;
	float Bottom;
	float Left;
	float Right;
	FVector2D OverallSize;

	FBorderGeometry(float Top, float Bottom, float Left, float Right, const FVector2D& OverallSize) :
		Top(Top),
		Bottom(Bottom),
		Left(Left),
		Right(Right),
		OverallSize(OverallSize)
	{}

	FBorderGeometry() : FBorderGeometry(0.f, 0.f, 0.f, 0.f, FVector2D::ZeroVector)
	{}
};

class PROPHUNT_API SPHDosBorder : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPHDosBorder)
		: _HAlign(HAlign_Fill),
		_VAlign(VAlign_Fill),
		_Padding(FMargin(20.f, 30.f)),
		_ContentScale(FVector2D(1.f)),
		_ColorAndOpacity(FLinearColor(1.f, 1.f, 1.f, 1.f)),
		_TextStyle(&FCoreStyle::Get().GetWidgetStyle<FTextBlockStyle>("NormalText")),
		_BorderColor(FLinearColor::White),
		_Thickness(4.f),
		_BlankOffset(80.f),
		_SpaceWidth(6.f),
		_BlankPosition(EHeaderPosition::Left),
		_AnimDuration(.5f),
		_DesiredSizeScale(FVector2D(1.f)),
		_ShowEffectWhenDisabled(true)
		{}

		SLATE_DEFAULT_SLOT(FArguments, Content)
		SLATE_ARGUMENT(EHorizontalAlignment, HAlign)
		SLATE_ARGUMENT(EVerticalAlignment, VAlign)
		SLATE_ARGUMENT(FMargin, Padding)

		SLATE_ATTRIBUTE(FVector2D, ContentScale)
		SLATE_ATTRIBUTE(FLinearColor, ColorAndOpacity)
	
		SLATE_ARGUMENT(FText, HeaderText)
		SLATE_STYLE_ARGUMENT(FTextBlockStyle, TextStyle)
		SLATE_ARGUMENT(FSlateFontInfo, Font)
		SLATE_ATTRIBUTE(FSlateColor, BorderColor)
		SLATE_ARGUMENT(float, Thickness)
		SLATE_ARGUMENT(bool, bShowHeader)
		SLATE_ARGUMENT(float, BlankOffset)
		SLATE_ARGUMENT(float, SpaceWidth)
		SLATE_ARGUMENT(EHeaderPosition, BlankPosition)
		SLATE_ARGUMENT(bool, bDoubleBorder)
		SLATE_ARGUMENT(bool, bAnimate)
		SLATE_ARGUMENT(float, AnimDuration)
		SLATE_ATTRIBUTE(FVector2D, DesiredSizeScale)
		/** Whether or not to show the disabled effect when this border is disabled */
		SLATE_ATTRIBUTE(bool, ShowEffectWhenDisabled)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	
	// Begin of SWidget interface
	virtual int32 OnPaint( const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled ) const override;
	// End of SWidget interface

private:
	void PaintBorder(const FGeometry& AllottedGeometry, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled, const FBorderGeometry& BorderGeometry) const;

	void SetupAnimation();
	
public:
	/** See Padding attribute */
	void SetPadding(const FMargin& InPadding);

	/** See ShowEffectWhenDisabled attribute */
	void SetShowEffectWhenDisabled(const TAttribute<bool>& InShowEffectWhenDisabled);

	/** Set the desired size scale multiplier */
	void SetDesiredSizeScale(const TAttribute<FVector2D>& InDesiredSizeScale);

	/** See HAlign argument */
	void SetHAlign(EHorizontalAlignment HAlign);

	/** See VAlign argument */
	void SetVAlign(EVerticalAlignment VAlign);

	/**
	* Sets the text for this text block
	*
	* @param	InText	The new text to display
	*/
	void SetHeaderText(const FText& InText);

	/**
	* Sets the font used to draw the text
	*
	* @param	InFont	The new font to use
	*/
	void SetFont(const FSlateFontInfo& InFont);

	void SetThickness(float InThickness);
	
	void SetShowHeader(bool bInShowHeader);
	
	void SetBlankOffset(float InBlankOffset);
	
	void SetSpaceWidth(float InSpaceWidth);
	
	void SetBlankPosition(EHeaderPosition InBlankPosition);
	
	void SetDoubleBorder(bool bInDoubleBorder);

	void SetBorderColorAndOpacity(const TAttribute<FSlateColor>& InColorAndOpacity);
	
	void SetAnimate(bool bInAnim);
	
	void SetAnimDuration(float InAnimDuration);

protected:
	// Begin SWidget overrides.
	virtual FVector2D ComputeDesiredSize(float LayoutScaleMultiplier) const override;
	// End SWidget overrides.

	FText GetHeaderText() const;
	
	FSlateColor GetBorderColor() const;
	
	FLinearColor GetHeaderAndContentColor() const;

	FSlateFontInfo GetFont() const;

	EVisibility GetHeaderVisibility() const;
	
private:
	TAttribute<FSlateColor> BorderColor;
	
	/** The text displayed in this text block */
	FText HeaderText;

	/** Default style used by the TextLayout */
	FTextBlockStyle TextStyle;
	
	/** Sets the font used to draw the text */
	FSlateFontInfo Font;
	
	float Thickness;

	bool bShowHeader;

	float BlankOffset;

	float SpaceWidth;

	EHeaderPosition BlankPosition;

	bool bDoubleBorder;

	TAttribute<FVector2D> DesiredSizeScale;

	/** Whether or not to show the disabled effect when this border is disabled */
	TAttribute<bool> ShowDisabledEffect;

	TSharedPtr<STextBlock> Header;

	bool bAnimate;

	float AnimDuration;

	/** Overlay slot which contains header widget. */
	SOverlay::FOverlaySlot* OverlayHeaderSlot;
	
	SOverlay::FOverlaySlot* OverlayContentSlot;
	
	FCurveSequence AnimCurves;
	TArray<FCurveHandle, TInlineAllocator<4>> BorderCurve;

public:
	/**
	* Sets the content for this border
	*
	* @param	InContent	The widget to use as content for the border
	*/
	virtual void SetContent(TSharedRef<SWidget> InContent);
};
