// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ButtonWrapper.generated.h"


DECLARE_MULTICAST_DELEGATE(FOnButtonWrapperClickedEvent);

/**
 * 
 */
UCLASS()
class PROPHUNT_API UButtonWrapper : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UBorder* Body;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* LabelText;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* SecondLabelText;

	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	int32 NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;

public:
	void SynchronizeProperties() override;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Button")
	bool bActive = true;

	UPROPERTY(BlueprintReadWrite, Category = "Button")
	FButtonStyle Style;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Button")
	float HoverSpeed = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Button")
	float InactiveOpacity = 0.2f;

	float CurrentHoveredLength;

	bool bPressed;

	bool bHovered;

	UFUNCTION()
	FEventReply HandleClicked(FGeometry MyGeometry, const FPointerEvent& MouseEvent);

	UFUNCTION(BlueprintImplementableEvent, Category = "Button|Event", meta = (DisplayName = "OnClicked"))
	void K2_OnClicked(bool bKeyDown);

	UFUNCTION(BlueprintImplementableEvent, Category = "Button|Event")
	void K2_OnHovered(bool bNewHover);

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Button")
	FText Text;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Button")
	FText SecondText;

	FOnButtonWrapperClickedEvent OnClicked;

	UFUNCTION(BlueprintCallable, Category = "Button")
	void SetIsActive(bool bInActive);

	UFUNCTION(BlueprintCallable, Category = "Button")
	bool IsActive() const { return bActive; }

	UFUNCTION(BlueprintCallable, Category = "Button")
	void SetText(FText InText);

	UFUNCTION(BlueprintCallable, Category = "Button")
	void SetSecondText(FText InText);
};
