// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/GameModes/GameMode_Hunting.h"
#include "GameMode_Escape.generated.h"


namespace MatchState
{
	extern PROPHUNT_API const FName InProgressEscaping;
}

/**
 * 
 */
UCLASS()
class PROPHUNT_API AGameMode_Escape : public AGameMode_Hunting
{
	GENERATED_BODY()

public:
	//~ Begin GameModeBase Interface
	virtual void InitGameState() override;
	//~ End GameModeBase Interface

protected:
	//~ Begin GameMode Interface
	virtual void HandleMatchIsWaitingToStart() override;
	virtual void OnMatchStateSet() override;
	virtual bool IsMatchInProgress() const override;
	//~ End GameMode Interface

	//~ Begin GameMode_Hunting Interface
	virtual bool IsInHuntingState() const override;
	virtual void HandleHuntingHasStarted() override;
	virtual void DetermineMatchWinner() override;
	//~ End GameMode_Hunting Interface

	virtual void HandleEscapingHasStarted();

	virtual void StartEscaping();

	bool IsInEscapingState() const {
		return GetMatchState() == MatchState::InProgressEscaping;
	}

public:
	void EnergyReceived(AController* InInstigator, float EnergyAmount);

	void EnergyGiven(AController* InInstigator, float EnergyAmount);

	void PortalCharged();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Classes")
	TSubclassOf<AActor> PowerPanelClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Classes")
	TSubclassOf<AActor> PowerPanelSpawnClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Classes")
	TSubclassOf<AActor> PortalClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Classes")
	TSubclassOf<AActor> PortalSpawnClass;

	/** Scale for self instigated damage */
	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	float NeedPortalEnergy;

	/** Maximum number of power panels to spawn */
	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	int32 MaxPowerPanels;
	
	UFUNCTION()
	void LightOnLevelUnloaded();
	
	UFUNCTION()
	void LightOffLevelLoaded();

	void DisablePowerPanels();

	void SpawnPortal();

private:
	FString LightOnLevelName;
	FString LightOffLevelName;
};
