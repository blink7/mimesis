// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameMode_Menu.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API AGameMode_Menu : public AGameModeBase
{
	GENERATED_BODY()
	
	AGameMode_Menu(const FObjectInitializer& ObjectInitializer);
	
public:
	//~ Begin AGameModeBase Interface
	void RestartPlayer(AController* NewPlayer) override;
	//~ End AGameModeBase Interface
};
