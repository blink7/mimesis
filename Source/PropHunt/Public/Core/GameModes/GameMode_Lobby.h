// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PHGameModeBase.h"
#include "GameMode_Lobby.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API AGameMode_Lobby : public APHGameModeBase
{
	GENERATED_UCLASS_BODY()
	
public:
	virtual void InitGameState() override;

	//~ Begin GameMode Interface
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual bool PlayerCanRestart_Implementation(APlayerController* Player) override;
	//~ End GameMode Interface

	void RespawnPlayer(class APlayerController* PlayerController);

	void UpdateReadyStatus();

protected:
	UPROPERTY(Config)
	int32 WarmupTime;

	int32 ReadyPlayers;

	/** Update remaining time */
	virtual void DefaultTimer() override;
};
