// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "PHGameModeBase.generated.h"

class APlayerStart;

/**
 * 
 */
UCLASS()
class PROPHUNT_API APHGameModeBase : public AGameMode
{
	GENERATED_UCLASS_BODY()

	virtual void InitGameState() override;

	//~ Begin GameMode Interface
	virtual void PreInitializeComponents() override;
	virtual bool ShouldSpawnAtStartSpot(AController* Player) override;
	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

protected:
	virtual void SetMatchState(FName NewState) override;
	//~ End GameMode Interface

	/** Number of teams */
	int32 NumTeams;

public:
	/*Finishes the match and bumps everyone to main menu.*/
	/*Only GameInstance should call this function */
	virtual void RequestFinishAndExitToMainMenu();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Classes")
	TSubclassOf<APawn> HunterPawnClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Classes")
	TSubclassOf<APawn> PropPawnClass;

	/** Handle for efficient management of DefaultTimer timer */
	FTimerHandle TimerHandle_DefaultTimer;

	/** Update remaining time */
	virtual void DefaultTimer() {}

	/** Check if player can use spawn point */
	virtual bool IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const;

	/** Check if player should use spawn point */
	bool IsSpawnpointPreferred(APlayerStart* SpawnPoint, AController* Player) const;
};
