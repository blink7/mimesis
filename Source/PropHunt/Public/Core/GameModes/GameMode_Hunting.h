// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PHGameModeBase.h"
#include "GameMode_Hunting.generated.h"


class APlayerStart;

namespace MatchState
{
	extern PROPHUNT_API const FName InProgressHunting;
}

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHuntingStartedDelegate);

/**
 * 
 */
UCLASS()
class PROPHUNT_API AGameMode_Hunting : public APHGameModeBase
{
	GENERATED_BODY()
	
public:
	AGameMode_Hunting();

	//~ Begin GameModeBase Interface
	virtual void InitGameState() override;
	virtual void RestartPlayer(AController* NewPlayer) override;
	//~ End GameModeBase Interface
	
	//~ Begin GameMode Interface
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void HandleSeamlessTravelPlayer(AController*& C) override;
	virtual bool IsMatchInProgress() const override;
	
protected:
	virtual void HandleMatchIsWaitingToStart() override;
	virtual void HandleMatchHasStarted() override;
	//~ End GameMode Interface

	virtual void HandleHuntingHasStarted();

	/** Update remaining time */
	virtual void DefaultTimer() override;

public:
	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnHuntingStartedDelegate OnHuntingStarted;

protected:
	bool bWithSpectator;

	/** Best team */
	int32 WinnerTeam;

	/** Delay between first player login and starting match */
	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	int32 WarmupTime;

	/** Match duration */
	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	int32 RoundTime;

	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	int32 TimeBetweenMatches;

	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	int32 HidingTime;

	/** Number of rounds in a match */
	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	int32 NumRounds;

	/** Scale for self instigated damage */
	UPROPERTY(Config, BlueprintReadOnly, Category = "Config")
	float DamageSelfScale;

	virtual void OnMatchStateSet() override;

	virtual void StartHunting();

	virtual bool IsInHuntingState() const
	{
		return GetMatchState() == MatchState::InProgressHunting;
	}

	virtual bool IsInProgress() const
	{
		return GetMatchState() == MatchState::InProgress || IsInHuntingState();
	}

	UFUNCTION()
	void ProcessTransportation();

	void TeleportHunter(class APlayerController* PlayerController);

	/** Pick team with least players in or random when it's equal */
	int32 ChooseTeam(class APHPlayerState* ForPlayerState) const;

	void SwapTeams();

	/** Check if player can use spawn point */
	virtual bool IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const override;

	/** Check who won */
	virtual void DetermineMatchWinner();

	/** Check if PlayerState is a winner */
	virtual bool IsWinner(APlayerState* PlayerState) const;

	virtual bool IsTeamAlive(int32 TeamNumber) const;

	virtual void SwitchLevel();

	virtual void NotifyMatchFinished();

public:
	/** Finish current match and lock players */
	UFUNCTION(Exec)
	void FinishMatch();

	/*Finishes the match and bumps everyone to main menu.*/
	/*Only GameInstance should call this function */
	virtual void RequestFinishAndExitToMainMenu() override;

	/** Notify about kills */
	virtual void Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, class UGameplayEffectUIData* UIData);
};
