// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PHTypes.h"
#include "PHPlayerController.generated.h"

class UHUDWidget;

namespace EMessageType
{
	extern PROPHUNT_API const FName Server;
	extern PROPHUNT_API const FName All;
	extern PROPHUNT_API const FName Team;
}

/**
 * 
 */
UCLASS()
class PROPHUNT_API APHPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	APHPlayerController(const FObjectInitializer& ObjectInitializer);

	/** Transition to dead state, retries spawning later */
	virtual void FailedToSpawnPawn() override;

	/** Update camera when pawn dies */
	virtual void PawnPendingDestroy(APawn* InPawn) override;

	virtual void InitPlayerState() override;
	virtual void SetPawn(APawn* InPawn) override;
	virtual void InitInputSystem() override;
	virtual void PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel) override;
	virtual void SetCinematicMode(bool bInCinematicMode, bool bHidePlayer, bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning) override;
	virtual bool IsMoveInputIgnored() const override;
	virtual void ResetIgnoreInputFlags() override;
	virtual void NotifyLoadedWorld(FName WorldPackageName, bool bFinalDest) override;
	virtual void AutoManageActiveCameraTarget(AActor* SuggestedTarget) override;

protected:
	//~ Begin APlayerController Interface
	virtual void SetupInputComponent() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void UnFreeze() override;
	//~ End APlayerController Interface

public:
	/* Sets spectator location and rotation */
	UFUNCTION(Client, Reliable)
	void ClientSetSpectatorCamera(FVector CameraLocation, FRotator CameraRotation);
	
	/* Notify player about started match */
	UFUNCTION(Client, Reliable)
	void ClientGameStarted();

	/** Starts the online game using the session name in the PlayerState */
	UFUNCTION(Client, Reliable)
	void ClientStartOnlineGame();

	/** Notify player about finished match */
	virtual void ClientGameEnded_Implementation(class AActor* EndGameFocus, bool bIsWinner) override;

	/** Notifies clients to send the end-of-round event */
	UFUNCTION(Client, Reliable)
	void ClientSendRoundEndEvent(bool bIsWinner, int32 ExpendedTimeInSeconds);

	/** Local function say a string */
	virtual void Say(const FString& Msg, FName Type = EMessageType::All);

	virtual void ClientTeamMessage_Implementation(APlayerState* SenderPlayerState, const FString& S, FName Type, float MsgLifeTime) override;

	/** Get infinite ammo cheat */
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	bool HasInfiniteAmmo() const { return bInfiniteAmmo; }

	void SetInfiniteAmmo(bool bEnable);

	/** Get infinite clip cheat */
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	bool HasInfiniteClip() const { return bInfiniteClip; }

	void SetInfiniteClip(bool bEnable);

	bool HasGodMode() const { return bGodMode; }

	void SetGodMode(bool bEnable);

	/** Ends and/or destroys game session */
	void CleanupSessionOnReturnToMenu();

	/** Informs that player fragged someone */
	void OnKill();

	/** Cleans up any resources necessary to return to main menu. Does not modify GameInstance state. */
	virtual void HandleReturnToMainMenu();

protected:
	UPROPERTY(Transient, Replicated)
	bool bInfiniteAmmo;

	UPROPERTY(Transient, Replicated)
	bool bInfiniteClip;

	UPROPERTY(Transient, Replicated)
	bool bGodMode;

	/** True for the first frame after the game has ended */
	bool bGameEndedFrame;

	/** Stores pawn location at last player death, used where player scores a kill after they died **/
	FVector LastDeathLocation;

	/** Try to find spot for death cam */
	bool FindDeathCameraSpot(FVector& CameraLocation, FRotator& CameraRotation);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerResetPlayer();

	/** RPC for clients to talk to server */
	UFUNCTION(Unreliable, Server, WithValidation)
	void ServerSay(const FString& Msg, FName Type);

public:
	UPROPERTY(Transient, Replicated)
	bool bInLobby;

	UFUNCTION(Client, Reliable)
	void ClientShowTeamChoice(bool bEnable);

	/* Notify local client about deaths */
	void OnDeathMessage(class APHPlayerState* KillerPlayerState, class APHPlayerState* KilledPlayerState, class UGameplayEffectUIData* InUIData);

	void OnToggleInGameMenu();

	void OnToggleTeamChoice();

	void OnShowScoreboard(bool bEnable);

	/* Tell the HUD to toggle the chat window. */
	void OnOpenChatWindow(bool bTeammate = false);

	void OnUpdateStatus();

	void ChooseTeam(int32 TeamNum);

	void ChooseNextMap(int32 MapIndex);

	/* Causes the player to commit suicide */
	UFUNCTION(Exec)
	virtual void Suicide();

	UFUNCTION(Client, Reliable)
	void ClientNotifyWeaponHit(float DamageTaken, const FHitResult& HitInfo);

	UFUNCTION(Client, Reliable)
	void ClientNotifyEnemyHit();

	UFUNCTION(Client, Reliable)
	void ClientShowWinner(int32 TeamNumber);

	UFUNCTION(Client, Reliable)
	void ClientShowInfoMessage(uint8 MessageType, float Duration);

	UFUNCTION(Client, Reliable)
	void ClientUpdateReadyStatus(int32 ReadyPlayers, int32 Total);

	UFUNCTION(Client, Reliable)
	void ClientShowMatchResultWithDelay(float Delay);

	UFUNCTION(Client, Reliable)
	void ClientShowPortalStatus();

	UFUNCTION(Client, Reliable)
	void ClientAddTeammatesToMinimap(const TArray<APawn*>& TeammatePawns);

	void ZoomMinimap();

	UFUNCTION(Client, Reliable)
	void ClientSetPlayersIndicatorVisibility(const TArray<APawn*>& PlayerPawns, bool bVisible);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerUpdateCharactersInfo(const TArray<APawn*>& Pawns);

	void ShowMatchResult();

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void ShowHint(const FHint& Hint);

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void HideHint(EHintType HintToHide);

	UFUNCTION(BlueprintCallable, Category = "HUD|POI")
	void AddPointOfInterest(AActor* TargetActor, EPointOfInterest DisplayType, UTexture2D* MinimapIcon, UTexture2D* ScreenIcon, float Size = 64.f, FLinearColor Color = FLinearColor::White, bool bStatic = true);

	UFUNCTION(BlueprintCallable, Category = "HUD|POI")
	void RemovePointOfInterest(AActor* TargetActor);

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void StartInteractionTimer(float Duration);

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void StopInteractionTimer();

	/** Returns the persistent user record associated with this player, or null if there is't one. */
	class UPHPersistentUser* GetPersistentUser() const;

	void UpdateHUD();

	UFUNCTION(BlueprintCallable, Category = "Input")
	void SetIgnoreGameActions(bool bNewGameActions);

	/** Check if gameplay related actions (movement, weapon usage, etc) are allowed right now */
	UFUNCTION(BlueprintCallable, Category = "Input")
	bool IsGameActionsIgnored() const;

	/** Sets the produce force feedback flag. */
	UFUNCTION(BlueprintCallable, Category = "Input")
	void SetIsVibrationEnabled(bool bEnable);

	/** Should produce force feedback? */
	UFUNCTION(BlueprintCallable, Category = "Input")
	bool IsVibrationEnabled() const;

protected:
	/** Should produce force feedback? */
	bool bVibrationEnabled;

	/** If set, gameplay related actions (movement, weapon usage, etc) are locked */
	bool bIgnoreGameActions;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerChooseTeam(int32 TeamNum);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerChangeTeamPostponement(int32 TeamNumber);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerChooseNextMap(int32 MapIndex);

	/** Notifies the server that the client has suicided */
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSuicide();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerUpdateReadyStatus();

public:
	// For tracking whether or not to send the end event
	bool bHasSentStartEvents;

private:
	/** Handle for efficient management of ClientStartOnlineGame timer */
	FTimerHandle TimerHandle_ClientStartOnlineGame;

public:
	UFUNCTION(Client, Reliable, Category = "Transportation")
	void ClientBeginTransportation();
};
