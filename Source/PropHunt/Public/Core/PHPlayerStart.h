// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"

#include "PHPlayerStart.generated.h"


class UBillboardComponent;

UENUM(BlueprintType)
enum ETeam
{
	TEAM_None		UMETA(DisplayName = "None"),
	TEAM_Hunters	UMETA(DisplayName = "Hunters"),
	TEAM_Props		UMETA(DisplayName = "Props")
};

/**
 * 
 */
UCLASS()
class PROPHUNT_API APHPlayerStart : public APlayerStart
{
	GENERATED_BODY()

	UPROPERTY()
	UBillboardComponent* HunterSprite;

	UPROPERTY()
	UBillboardComponent* PropSprite;
	
public:
	APHPlayerStart(const FObjectInitializer& ObjectInitializer);

	//~ Begin AActor Interface
#if WITH_EDITOR
	virtual void OnConstruction(const FTransform& Transform) override;
#endif
	//~ End AActor Interface

	UPROPERTY(EditInstanceOnly, Category = "Team")
	TEnumAsByte<ETeam> Team;

	UPROPERTY(EditInstanceOnly, Category = "Team")
	bool bHuntingPreparation;
};
