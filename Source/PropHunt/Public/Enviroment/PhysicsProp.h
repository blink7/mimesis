// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "PhysicsProp.generated.h"


USTRUCT()
struct FPropState
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	UStaticMesh* ItemMesh;

	UPROPERTY()
	UMaterialInterface* ItemMaterial;

	UPROPERTY(Transient)
	FVector Location;

	UPROPERTY(Transient)
	float Rotation;

	UPROPERTY()
	float Scale;

	UPROPERTY()
	float MassScale;

	UPROPERTY()
	FString ShapeType;

	FPropState() :
		ItemMesh(nullptr),
		ItemMaterial(nullptr),
		Location(FVector::ZeroVector),
		Rotation(0),
	    Scale(1.f),
	    MassScale(1.f)
	{
	}
};


/**
 * A StaticMeshActor, adjusted to be replicated and randomly spawned by PropSpawn. 
 * Should be also used as a pickable item.
 */
UCLASS()
class PROPHUNT_API APhysicsProp : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	APhysicsProp();

	//Begin AActor Interface
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
#if WITH_EDITOR
	virtual void OnConstruction(const FTransform& Transform) override;
#endif
	//End AActor Interface

private:
	UPROPERTY()
	class UBillboardComponent* PropIcon;

private:
	UPROPERTY(ReplicatedUsing = OnRep_ReplicatedPropState)
	FPropState PropState;

	UFUNCTION()
	void OnRep_ReplicatedPropState();

	void UpdateState();

public:
	void SetState(FPropState PropState);

private:
	// The character who has picked up this item
	UPROPERTY(Replicated)
	AActor* PickedUpBy;

public:
	void PickUp(AActor* Actor);

	bool IsBusy() const;

	UFUNCTION(BlueprintCallable, Category = "Effect")
	void ShowOutline(bool bEnable);

protected:
	UPROPERTY()
	UStaticMesh* OutlineMesh;

	UPROPERTY()
	UMaterialInstance* OutlineMaterial;

	UPROPERTY()
	UStaticMeshComponent* OutlineMeshComponent;
};
