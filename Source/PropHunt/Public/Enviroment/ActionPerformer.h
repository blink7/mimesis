// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActionPerformer.generated.h"


/**
 * ActionPerformer is an actor, which is controlled by a Switch.
 */
UCLASS()
class PROPHUNT_API AActionPerformer : public AActor
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent, Category = "Interact")
	void OnPerformAction(bool bEnable = true);
};
