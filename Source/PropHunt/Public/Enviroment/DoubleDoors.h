// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Enviroment/Door.h"
#include "DoubleDoors.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class PROPHUNT_API ADoubleDoors : public ADoor
{
	GENERATED_BODY()
	
public:
	ADoubleDoors();

	bool IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Door2;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* Door2_Trigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Door2_Handle1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Door2_Handle2;

protected:
	void BeginPlay() override;

public:
	void SetDoorRotation(float AngleFactor) override;

	void SetHandleRotation(float AngleFactor) override;
};
