// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Door.h"
#include "PHPowerPanel.generated.h"


/**
 * 
 */
UCLASS(Abstract)
class PROPHUNT_API APHPowerPanel : public ADoor
{
	GENERATED_BODY()

public:
	APHPowerPanel();

	void HandlePowerPanelEnergyChanged(AController* InInstigator, float DeltaValue) override;

	//~ Begin IInteractable Interface
	void PostInteract_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) override;
	bool IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const override;
	FText GetInteractionHint_Implementation(UPrimitiveComponent* InteractionComponent) const override;
	//~ End IInteractable Interface

	UFUNCTION(BlueprintCallable, Category = "PowerPanel")
	float GetEnergy() const;

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerPanel")
	void OnEnable();

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerPanel")
	void OnDisable();

private:
	UPROPERTY(Category = "PowerPanel", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UShapeComponent* DischargeableArea;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Abilities")
	FGameplayTag DischargeEventTag;
	
	UPROPERTY(EditDefaultsOnly, Category = "Abilities")
	FGameplayTag EnabledTag;

	void ProcessInteractingCharacter(class APropCharacter* PropCharacter);

	UFUNCTION(BlueprintImplementableEvent, Category = "PowerPanel")
	void OnBroke();
};
