// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PerformersController.generated.h"


/*
 * PerformersController is intended for passing actions to all ActionPerformers on the current level.
 */
UCLASS()
class PROPHUNT_API APerformersController : public AActor
{
	GENERATED_BODY()
	
public:
	APerformersController();

public:
	UFUNCTION(BlueprintCallable)
	void OnPerformAction(bool bEnable);

protected:
	UPROPERTY()
	class UBillboardComponent* ControllerIcon;
};
