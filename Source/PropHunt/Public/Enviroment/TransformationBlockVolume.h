// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerVolume.h"
#include "TransformationBlockVolume.generated.h"

/**
 * An invisible volume used to block PropCharacter to transform in a certain spot.
 */
UCLASS()
class PROPHUNT_API ATransformationBlockVolume : public ATriggerVolume
{
	GENERATED_UCLASS_BODY()
	
};
