// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Abilities/PHAbilityActorBase.h"
#include "Character/Interactable.h"
#include "PHPortal.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API APHPortal : public APHAbilityActorBase, public IInteractable
{
	GENERATED_BODY()

public:
	APHPortal();
	
protected:
	void BeginPlay() override;

public:
	void HandlePortalEnergyChanged(AController* InInstigator, float DeltaValue) override;

	//~ Begin IInteractable Interface
	void PostInteract_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) override;
	bool IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const override;
	FText GetInteractionHint_Implementation(UPrimitiveComponent* InteractionComponent) const override;
	//~ End IInteractable Interface

	UFUNCTION(BlueprintCallable, Category = "Portal")
	float GetEnergy() const;

	UFUNCTION(BlueprintCallable, Category = "Portal")
	float GetMaxEnergy() const;

protected:
	FTimerHandle TimerHandle_SetUpPortal;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Portal")
	TSubclassOf<UGameplayEffect> ChargedEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Abilities")
	FGameplayTag ChargeEventTag;

	void SetUp();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Portal")
	void UpdateSize();

	void OnCharged();
};
