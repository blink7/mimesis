// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TextRenderActor.h"
#include "PHTeleportCountdownBoard.generated.h"

/**
 * 
 */
UCLASS()
class PROPHUNT_API APHTeleportCountdownBoard : public ATextRenderActor
{
	GENERATED_BODY()

public:
	APHTeleportCountdownBoard();

	virtual void BeginPlay() override;

protected:
	void SetupCountdown();

	UFUNCTION()
	void StartCountdown();

	UFUNCTION()
	void StopCountdown();
	
	UFUNCTION()
	void UpdateCountdown(int32 Time);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance")
	TArray<FText> EasterEggWords;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Appearance", meta = (ClampMin = "0", ClampMax = "100"))
	int32 EasterEggDisplayChance;

	bool bCountdownStarted;
};
