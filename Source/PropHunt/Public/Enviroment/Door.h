// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/Abilities/PHAbilityActorBase.h"
#include "Character/Interactable.h"
#include "Core/PHTypes.h"

#include "Door.generated.h"


class UBoxComponent;
class UGameplayEffect;

UENUM(BlueprintType)
enum class EDoorState : uint8
{
	DS_Open		UMETA(DisplayName = "Open"),
	DS_Close	UMETA(DisplayName = "Close"),
	DS_Lock		UMETA(DisplayName = "Lock")
};

/**
 * 
 */
UCLASS(Abstract)
class PROPHUNT_API ADoor : public APHAbilityActorBase, public IInteractable
{
	GENERATED_BODY()
	
public:
	ADoor();

	//~ Begin AActor Interface
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

#if WITH_EDITOR
	virtual void OnConstruction(const FTransform& Transform) override;
#endif

	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	//~ End AActor Interface
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Door1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Door1_Handle1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Door1_Handle2;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Door", meta = (AllowPrivateAccess = "true"))
	UBoxComponent* Door1_Trigger;

	/** AkComponent to handle playback */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AkAmbientSound", meta = (ShowOnlyInnerProperties))
	class UAkComponent* AkComponent;

#if WITH_EDITORONLY_DATA
	/** Component shown in the editor only to indicate open door direction */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UArrowComponent* ArrowComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTextRenderComponent* StatusText;
#endif

public:
	//~ Begin IInteractable Interface
	virtual void PostInteract_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) override;
	virtual bool IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const override;
	virtual FText GetInteractionHint_Implementation(UPrimitiveComponent* InteractionComponent) const override;
	//~ End IInteractable Interface

	UFUNCTION(BlueprintCallable)
	virtual void SetDoorRotation(float AngleFactor);

	UFUNCTION(BlueprintCallable)
	virtual void SetHandleRotation(float AngleFactor);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings")
	class UAkSwitchValue* DoorType;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings")
	EDoorState InitialDoorState;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings", meta = (ClampMin = "30.0", ClampMax = "170.0", UIMin = "30.0", UIMax = "170.0"))
	float OpenAngle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings", meta = (ClampMin = "0.0", ClampMax = "90.0", UIMin = "0.0", UIMax = "90.0"))
	float HandleOpenAngle;

	/** The angle of closure at which not to respond to blocking objects. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", AdvancedDisplay)
	float BlockingAngle;

	/** How far to move a blocking object. The smaller value, the smoother movement, but more expensive. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", AdvancedDisplay)
	float PushOffset;

	UFUNCTION()
	void OnDoorBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	UPROPERTY(ReplicatedUsing = "OnRep_DoorAngleFactor")
	float DoorAngleFactor;

	TSoftObjectPtr<class AAkAcousticPortal> AssociatedPortal;

	UFUNCTION()
	void OnRep_DoorAngleFactor();

	void InitializePortal();

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities|State")
	TSubclassOf<UGameplayEffect> OpenEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities|State")
	TSubclassOf<UGameplayEffect> CloseEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Abilities|State")
	TSubclassOf<UGameplayEffect> BlockEffect;

	void AdjustDoorTrigger(UStaticMeshComponent* Door, UBoxComponent* Trigger);

	UFUNCTION(BlueprintCallable, Category = "State")
	bool IsOpen() const;

	UFUNCTION(BlueprintCallable, Category = "State")
	bool IsClose() const;

	UFUNCTION(BlueprintCallable, Category = "State")
	bool IsLock() const;

	UFUNCTION(BlueprintCallable, Category = "State")
	bool IsMoving() const;

	UPROPERTY(BlueprintReadOnly, Category = "Abilities|State")
	FActiveGameplayEffectHandle BlockEffectHandle;
};
