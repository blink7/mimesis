// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Engine/DataTable.h"

#include "PropSpawn.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogPropSpawn, Log, All);

UENUM(BlueprintType)
enum class EPHCollisionShape : uint8
{
	Capsule,
	Box
};

/**
 * FItemProperty is a table row struct containing needed parameters of spwanable items.
 */
USTRUCT(BlueprintType)
struct FItemProperty : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<UStaticMesh> SpawnableItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EPHCollisionShape ShapeType = EPHCollisionShape::Box;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BoundingRadius = 30.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MassScale = 1.f;
};


USTRUCT()
struct FItem
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UStaticMesh> StaticMesh;

	UPROPERTY(EditAnywhere)
	TArray<TSoftObjectPtr<UMaterialInterface>> Materials;

	UPROPERTY(EditAnywhere)
	int32 MinSpawn = 1;

	UPROPERTY(EditAnywhere)
	int32 MaxSpawn = 1;

	UPROPERTY(EditAnywhere)
	float MinScale = 1.f;

	UPROPERTY(EditAnywhere)
	float MaxScale = 1.f;

	UPROPERTY(EditAnywhere)
	float MinRotation = -180;

	UPROPERTY(EditAnywhere)
	float MaxRotation = 180;
};


/**
 * PropSpawn is an object, which serves to randomly spawn Static Meshes to the world at beginning game.
 */
UCLASS()
class PROPHUNT_API APropSpawn : public AActor
{
	GENERATED_BODY()

public:
	APropSpawn();

	void Destroyed() override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY()
	class UBoxComponent* SpawnVolume = nullptr;

	UPROPERTY()
	class UBillboardComponent* SpawnIcon;

	/**
	 * Uses to calculate item bounds, which are used to check whether 
	 * a spawnable actor will intersects with surrounding objects.
	 */
	UPROPERTY(EditAnywhere, Category = "Setup")
	UDataTable* ItemPropertiesTable = nullptr;

	// If true, this spawn will try randomly create actors for each selected Static Mesh.
	UPROPERTY(EditAnywhere, Category = "Setup")
	bool bRandomizeAllItems = true;

	UPROPERTY(EditAnywhere, Category = "Setup")
	TArray<FItem> ItemsToSpawn;

	TArray<FItemProperty*> ItemProperties;

	TQueue<int32> QueueToSpawn;

	FTimerHandle TimerHandle_SpawnQueue;

	void RandomlyPlaceItem(int32 Index);

	bool FindEmptyLocation(FVector& OutLocation, float Radius) const;

	void SpawnProp(const struct FPropState& PropState) const;
};
