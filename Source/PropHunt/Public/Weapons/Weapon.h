// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameplayAbilitySpec.h"
#include "../../2D/Paper2D/Source/Paper2D/Classes/PaperSprite.h"
#include "Weapon.generated.h"

class USoundBase;
class UAnimMontage;
class UNiagaraSystem;
class UAudioComponent;
class AHunterCharacter;
class UNiagaraComponent;
class UForceFeedbackEffect;

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_BODY()

	/** Base weapon spread (degrees) */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Accuracy")
	float WeaponSpread;

	/** Targeting spread modifier */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Accuracy")
	float TargetingSpreadMod;

	/** Continuous firing: spread increment */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Accuracy")
	float FiringSpreadIncrement;

	/** Continuous firing: max increment */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Accuracy")
	float FiringSpreadMax;

	/** Infinite ammo for reloads */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ammo")
	bool bInfiniteAmmo;

	/** Infinite ammo in clip, no reload required */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ammo")
	bool bInfiniteClip;

	FWeaponData()
	{
		WeaponSpread = 5.f;
		TargetingSpreadMod = 0.25f;
		FiringSpreadIncrement = 1.f;
		FiringSpreadMax = 10.f;
		bInfiniteAmmo = false;
		bInfiniteClip = false;
	}
};

USTRUCT(BlueprintType)
struct FWeaponAnim
{
	GENERATED_BODY()

	/** Animation played on pawn (1st person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Animation")
	UAnimMontage* Pawn1P;

	/** Animation played on pawn (3rd person view) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Animation")
	UAnimMontage* Pawn3P;

	FWeaponAnim() :
		Pawn1P(nullptr),
		Pawn3P(nullptr)
	{}
};


UCLASS(Abstract, Blueprintable)
class PROPHUNT_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
	AWeapon();

	//~ Begin AActor Interface
	virtual void PostInitializeComponents() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	//~ End AActor Interface

	//////////////////////////////////////////////////////////////////////////
	// Reading data

	/** Get weapon mesh (needs pawn owner to determine variant) */
	UFUNCTION(BlueprintCallable, Category = "Mesh")
	USkeletalMeshComponent* GetWeaponMesh() const;

	/** Icon displayed on the HUD when weapon is equipped as primary */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	UPaperSprite* PrimaryIcon;

	/** Icon displayed on the HUD when weapon is secondary */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	UPaperSprite* SecondaryIcon;

	/** Bullet icon used to draw current clip (left side) */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	UPaperSprite* PrimaryClipIcon;

	/** Bullet icon used to draw secondary clip (left side) */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	UPaperSprite* SecondaryClipIcon;

	/** Crosshair parts icons (left, top, right, bottom and center) */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	UPaperSprite* Crosshair;

	/** Crosshair parts icons when targeting (left, top, right, bottom and center) */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	UPaperSprite* AimingCrosshair;

	/** Only use red colored center part of aiming crosshair */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	bool bUseLaserDot;

	/** False = default crosshair */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	bool bUseCustomCrosshair;

	/** False = use custom one if set, otherwise default crosshair */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	bool bUseCustomAimingCrosshair;

	/** True - crosshair will not be shown unless aiming with the weapon */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HUD")
	bool bHideCrosshairWhileNotAiming;

	// This tag is primarily used by the first person Animation Blueprint to determine which animations to play
	// (Rifle vs Rocket Launcher)
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Ability")
	FGameplayTag WeaponTag;

	UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = "Ability")
	FGameplayTag FireMode;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Ability")
	FGameplayTag PrimaryAmmoType;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Ability")
	FGameplayTag SecondaryAmmoType;

	FGameplayTag WeaponIsFiringTag;

	void SetOwningPawn(AHunterCharacter* NewOwner);

	float GetCurrentSpread() const;

	/** gets last time when this weapon was switched to */
	float GetEquipStartedTime() const;

	/** gets the duration of equipping weapon*/
	float GetEquipDuration() const;

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	/** Weapon is being equipped by owner pawn */
	virtual void OnEquip(const AWeapon* LastWeapon);

	/** Weapon is holstered by owner pawn */
	virtual void OnUnEquip();

	/** Weapon is now equipped by owner pawn */
	virtual void OnEquipFinished();

	/** [server] Weapon was added to pawn's inventory */
	virtual void OnEnterInventory(AHunterCharacter* NewOwner);

	/** [server] Weapon was removed from pawn's inventory */
	virtual void OnLeaveInventory();

	virtual void AddAbilities();

	virtual void RemoveAbilities();

	// Resets things like fire mode to default
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void ResetWeapon();

	// Getter for LineTraceTargetActor. Spawns it if it doesn't exist yet.
	UFUNCTION(BlueprintCallable, Category = "Targeting")
	class APHGameplayAbilityTargetActor_LineTrace* GetLineTraceTargetActor();

	// Getter for SphereTraceTargetActor. Spawns it if it doesn't exist yet.
	UFUNCTION(BlueprintCallable, Category = "Targeting")
	class APHGameplayAbilityTargetActor_SphereTrace* GetSphereTraceTargetActor();

	void SetAbilities(const TArray<TSubclassOf<class UPHGameplayAbility>>& NewAbilities);

private:
	/** Check if weapon has infinite ammo (include owner's cheats) */
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	bool HasInfiniteAmmo() const;

	/** Check if weapon has infinite clip (include owner's cheats) */
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	bool HasInfiniteClip() const;

protected:
	UPROPERTY(Transient, Replicated)
	AHunterCharacter* MyPawn;

	TWeakObjectPtr<class UPHAbilitySystemComponent> AbilitySystemComponent;

	UPROPERTY(EditAnywhere, Category = "Ability")
	TArray<TSubclassOf<class UPHGameplayAbility>> Abilities;

	UPROPERTY(BlueprintReadOnly, Category = "Ability")
	TArray<FGameplayAbilitySpecHandle> AbilitySpecHandles;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
	FGameplayTag DefaultFireMode;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Config")
	FWeaponData WeaponConfig;

	UPROPERTY()
	class APHGameplayAbilityTargetActor_LineTrace* LineTraceTargetActor;

	UPROPERTY()
	class APHGameplayAbilityTargetActor_SphereTrace* SphereTraceTargetActor;

private:
	/** Weapon mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, Category = "Mesh")
	USkeletalMeshComponent* Mesh1P;

	/** Weapon mesh: 3rd person view */
	UPROPERTY(VisibleDefaultsOnly, Category = "Mesh")
	USkeletalMeshComponent* Mesh3P;

protected:
	/** Name of bone/socket for muzzle in weapon mesh */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Effects")
	FName MuzzleAttachPoint;

	/** FX for muzzle flash */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Effects")
	UNiagaraSystem* MuzzleFX;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Effects")
	UNiagaraSystem* MuzzleSmokeFX;

	/** Camera shake on firing */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	TSubclassOf<class UCameraShakeBase> FireCameraShake;

	/** Force feedback effect to play when the weapon is fired */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	UForceFeedbackEffect* FireForceFeedback;

	/** Single fire sound (bLoopedFireSound not set) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
	USoundBase* FireSound;

	/** Finished burst sound (bLoopedFireSound set) */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Sound")
	USoundBase* FireFinishSound;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Sound")
	USoundBase* OutOfAmmoSound;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Sound")
	USoundBase* ReloadSound;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Animation")
	FWeaponAnim ReloadAnim;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Sound")
	USoundBase* EquipSound;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Animation")
	FWeaponAnim EquipAnim;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Animation")
	FWeaponAnim FireAnim;

	/** Last time when this weapon was switched to */
	float EquipStartedTime;

	/** How much time weapon needs to be equipped */
	float EquipDuration;

	/** Current spread from continuous firing */
	float CurrentFiringSpread;

	/** Handle for efficient management of OnEquipFinished timer */
	FTimerHandle TimerHandle_OnEquipFinished;

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage

	/** [local] Weapon specific fire implementation */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void FireWeapon();

	/** [local] Update spread on firing */
	UFUNCTION(BlueprintCallable, Category = "Weapon")
	virtual void OnBurstFinished();

	//////////////////////////////////////////////////////////////////////////
	// Inventory

	void AttachMeshToPawn();

	void DetachMeshFromPawn();

	//////////////////////////////////////////////////////////////////////////
	// Weapon usage helpers

	UAudioComponent* PlayWeaponSound(USoundBase* Sound);

	float PlayWeaponAnimation(const FWeaponAnim& Animation);

	void StopWeaponAnimation(const FWeaponAnim& Animation);

	/** Get the muzzle location of the weapon */
	FVector GetMuzzleLocation() const;

	/** Get direction of weapon's muzzle */
	FVector GetMuzzleDirection() const;
};
