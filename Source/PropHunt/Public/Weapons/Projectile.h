// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon_Projectile.h"
#include "Core/PHTypes.h"
#include "Core/Abilities/PHGameplayEffectTypes.h"
#include "Projectile.generated.h"


UCLASS(Abstract, Blueprintable)
class PROPHUNT_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	AProjectile();

	/** Initial setup */
	virtual void PostInitializeComponents() override;

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = "Projectile")
	class UProjectileMovementComponent* MovementComp;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = "Projectile")
	class USphereComponent* CollisionComp;

	/** Handle hit */
	UFUNCTION()
	void OnImpact(const FHitResult& HitResult);

	UPROPERTY(BlueprintReadOnly, meta = (ExposeOnSpawn = true), Category = "Projectile")
	FPHGameplayEffectContainerSpec EffectContainerSpec;

	/** Effects for explosion */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Effect", meta = (Categories = "GameplayCue"))
	FGameplayTag ImpactGameplayCueTag;

	TWeakObjectPtr<class UPHAbilitySystemComponent> AbilitySystemComponent;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (ExposeOnSpawn = true), Category = "Projectile")
    FVector InitialVelocity;

	/** Projectile data */
	FProjectileWeaponData WeaponConfig;

	/** Update velocity on client */
	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;
};
