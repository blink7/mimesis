// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/Projectile.h"
#include "Grenade.generated.h"


/**
 * 
 */
UCLASS()
class PROPHUNT_API AGrenade : public AProjectile
{
	GENERATED_BODY()
	
public:
	AGrenade();

	void Tick(float DeltaSeconds) override;

protected:
	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = "Projectile")
	class USkeletalMeshComponent* MeshComp;

	float RotationPitch;
};
