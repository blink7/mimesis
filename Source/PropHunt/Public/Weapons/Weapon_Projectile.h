// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/Weapon.h"

#include "GameFramework/DamageType.h"

#include "Weapon_Projectile.generated.h"


USTRUCT(BlueprintType)
struct FProjectileWeaponData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Projectile")
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Projectile")
	float ProjectileLife;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Projectile")
	float ProjectileRadius;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Projectile")
	float ProjectileSpeed;

	/** Damage at impact point */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "WeaponStat")
	float ExplosionDamage;

	/** Radius of damage */
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "WeaponStat")
	float ExplosionRadius;

	FProjectileWeaponData()
	{
		ProjectileClass = nullptr;
		ProjectileLife = 10.f;
		ProjectileRadius = 7.f;
		ProjectileSpeed = 1000.f;
		ExplosionDamage = 100.f;
		ExplosionRadius = 300.f;
	}
};

/**
 * 
 */
UCLASS(Abstract)
class PROPHUNT_API AWeapon_Projectile : public AWeapon
{
	GENERATED_BODY()
	
public:
	FProjectileWeaponData GetWeaponConfig() { return ProjectileConfig; }

protected:
	/** Weapon config */
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Config")
	FProjectileWeaponData ProjectileConfig;
};
