// Copyright Art-Sun Games. All Rights Reserved.

#include "Weapons/Weapon.h"

#include "PropHunt/PropHunt.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/Abilities/PHGameplayAbility.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/Abilities/PHGameplayAbilityTargetActor_LineTrace.h"
#include "Core/Abilities/PHGameplayAbilityTargetActor_SphereTrace.h"
#include "Character/Hunter/HunterCharacter.h"

#include "Components/AudioComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"

AWeapon::AWeapon() :
	FireMode(FGameplayTag::RequestGameplayTag("Weapon.FireMode.None")),
	WeaponIsFiringTag(FGameplayTag::RequestGameplayTag("Weapon.IsFiring"))
{
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh1P");
	RootComponent = Mesh1P;
	Mesh1P->CastShadow = false;
	Mesh1P->bOnlyOwnerSee = true;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;

	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh3P");
	Mesh3P->SetupAttachment(Mesh1P);
	Mesh3P->CastShadow = true;
	Mesh3P->bOwnerNoSee = true;
	Mesh3P->bReceivesDecals = false;
	Mesh3P->bCastHiddenShadow = true;
	Mesh3P->bRenderInDepthPass = false;
	Mesh3P->bRenderCustomDepth = true;
	Mesh3P->CustomDepthStencilValue = 1;
	Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh3P->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	Mesh3P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;

	PrimaryActorTick.bCanEverTick = false; // Set this actor to never tick
// 	PrimaryActorTick.TickGroup = TG_PrePhysics;
// 	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bNetUseOwnerRelevancy = true;

	NetUpdateFrequency = 100.f; // Set this to a value that's appropriate for your game
}

void AWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	DetachMeshFromPawn();
}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeapon, MyPawn);
}

void AWeapon::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	if (IsValid(LineTraceTargetActor) && GetWorld() && !GetWorld()->bIsTearingDown)
	{
		LineTraceTargetActor->Destroy();
	}

	if (IsValid(SphereTraceTargetActor) && GetWorld() && !GetWorld()->bIsTearingDown)
	{
		SphereTraceTargetActor->Destroy();
	}

	Super::EndPlay(EndPlayReason);
}

USkeletalMeshComponent* AWeapon::GetWeaponMesh() const
{
	return (MyPawn && MyPawn->IsFirstPerson()) ? Mesh1P : Mesh3P;
}

void AWeapon::SetOwningPawn(AHunterCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		MyPawn = NewOwner;

		SetInstigator(MyPawn);
		// net owner for RPC calls
		SetOwner(MyPawn);

		AbilitySystemComponent = MyPawn ? Cast<UPHAbilitySystemComponent>(MyPawn->GetAbilitySystemComponent()) : nullptr;
	}
}

float AWeapon::GetCurrentSpread() const
{
	float FinalSpread = WeaponConfig.WeaponSpread + CurrentFiringSpread;
	if (MyPawn && MyPawn->IsTargeting())
	{
		FinalSpread *= WeaponConfig.TargetingSpreadMod;
	}

	return FinalSpread;
}

float AWeapon::GetEquipStartedTime() const
{
	return EquipStartedTime;
}

float AWeapon::GetEquipDuration() const
{
	return EquipDuration;
}

void AWeapon::OnEquip(const AWeapon* LastWeapon)
{
	AttachMeshToPawn();

	// Only play animation if last weapon is valid
	if (LastWeapon)
	{
		float Duration = PlayWeaponAnimation(EquipAnim);
		if (Duration <= 0.f)
		{
			// failsafe
			Duration = 0.5f;
		}
		EquipStartedTime = GetWorld()->GetTimeSeconds();
		EquipDuration = Duration;

		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipFinished, this, &AWeapon::OnEquipFinished, Duration, false);
	}
	else
	{
		OnEquipFinished();
	}

	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		PlayWeaponSound(EquipSound);
	}
}

void AWeapon::OnUnEquip()
{
	DetachMeshFromPawn();
}

void AWeapon::OnEquipFinished()
{
	AttachMeshToPawn();
}

void AWeapon::OnEnterInventory(AHunterCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
	AddAbilities();
}

void AWeapon::OnLeaveInventory()
{
	if (HasAuthority())
	{
		RemoveAbilities();
		SetOwningPawn(nullptr);
	}

	OnUnEquip();
}

void AWeapon::AddAbilities()
{
	if (!MyPawn || !AbilitySystemComponent.IsValid())
	{
		return;
	}

	// Grant abilities, but only on the server	
	if (HasAuthority())
	{
		for (TSubclassOf<UPHGameplayAbility>& Ability : Abilities)
		{
			AbilitySpecHandles.Add(AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(Ability, 1, static_cast<int32>(Ability.GetDefaultObject()->AbilityInputID), this)));
		}
	}
}

void AWeapon::RemoveAbilities()
{
	if (!MyPawn || !AbilitySystemComponent.IsValid())
	{
		return;
	}

	// Remove abilities, but only on the server	
	if (HasAuthority())
	{
		for (FGameplayAbilitySpecHandle& SpecHandle : AbilitySpecHandles)
		{
			AbilitySystemComponent->ClearAbility(SpecHandle);
		}
	}
}

void AWeapon::ResetWeapon()
{
	FireMode = DefaultFireMode;
}

class APHGameplayAbilityTargetActor_LineTrace* AWeapon::GetLineTraceTargetActor()
{
	if (LineTraceTargetActor)
	{
		return LineTraceTargetActor;
	}

	LineTraceTargetActor = GetWorld()->SpawnActor<APHGameplayAbilityTargetActor_LineTrace>();
	LineTraceTargetActor->SetOwner(this);
	return LineTraceTargetActor;
}

class APHGameplayAbilityTargetActor_SphereTrace* AWeapon::GetSphereTraceTargetActor()
{
	if (SphereTraceTargetActor)
	{
		return SphereTraceTargetActor;
	}

	SphereTraceTargetActor = GetWorld()->SpawnActor<APHGameplayAbilityTargetActor_SphereTrace>();
	SphereTraceTargetActor->SetOwner(this);
	return SphereTraceTargetActor;
}

void AWeapon::SetAbilities(const TArray<TSubclassOf<UPHGameplayAbility>>& NewAbilities)
{
	Abilities = NewAbilities;
}

bool AWeapon::HasInfiniteAmmo() const
{
	const auto MyPC = MyPawn ? Cast<APHPlayerController>(MyPawn->GetController()) : nullptr;
	return WeaponConfig.bInfiniteAmmo || (MyPC && MyPC->HasInfiniteAmmo());
}

bool AWeapon::HasInfiniteClip() const
{
	const auto MyPC = MyPawn ? Cast<APHPlayerController>(MyPawn->GetController()) : nullptr;
	return WeaponConfig.bInfiniteClip || (MyPC && MyPC->HasInfiniteClip());
}

void AWeapon::FireWeapon()
{
	CurrentFiringSpread = FMath::Min(WeaponConfig.FiringSpreadMax, CurrentFiringSpread + WeaponConfig.FiringSpreadIncrement);
}

void AWeapon::OnBurstFinished()
{
	CurrentFiringSpread = 0.f;
}

void AWeapon::AttachMeshToPawn()
{
	if (MyPawn)
	{
		// Remove and hide both first and third person meshes
		DetachMeshFromPawn();

		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		const FName AttachPoint = MyPawn->GetWeaponAttachPoint();
		if (MyPawn->IsLocallyControlled())
		{
			USkeletalMeshComponent* PawnMesh1p = MyPawn->GetSpecificPawnMesh(true);
			USkeletalMeshComponent* PawnMesh3p = MyPawn->GetSpecificPawnMesh(false);
			Mesh1P->SetHiddenInGame(false);
			Mesh3P->SetHiddenInGame(false);
			Mesh1P->AttachToComponent(PawnMesh1p, FAttachmentTransformRules::SnapToTargetIncludingScale, AttachPoint);
			Mesh3P->AttachToComponent(PawnMesh3p, FAttachmentTransformRules::SnapToTargetIncludingScale, AttachPoint);
		}
		else
		{
			USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
			USkeletalMeshComponent* UsePawnMesh = MyPawn->GetPawnMesh();
			UseWeaponMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, AttachPoint);
			UseWeaponMesh->SetHiddenInGame(false);
		}
	}
}

void AWeapon::DetachMeshFromPawn()
{
	Mesh1P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	Mesh1P->SetHiddenInGame(true);

	Mesh3P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	Mesh3P->SetHiddenInGame(true);
}

UAudioComponent* AWeapon::PlayWeaponSound(USoundBase* Sound)
{
	UAudioComponent* AC = nullptr;
	if (Sound && MyPawn)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, MyPawn->GetRootComponent());
	}

	return AC;
}

float AWeapon::PlayWeaponAnimation(const FWeaponAnim& Animation)
{
	float Duration = 0.f;
	if (MyPawn)
	{
		const auto UseAnim = MyPawn->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			Duration = MyPawn->PlayAnimMontage(UseAnim);
		}
	}

	return Duration;
}

void AWeapon::StopWeaponAnimation(const FWeaponAnim& Animation)
{
	if (MyPawn)
	{
		const auto UseAnim = MyPawn->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			MyPawn->StopAnimMontage(UseAnim);
		}
	}
}

FVector AWeapon::GetMuzzleLocation() const
{
	return GetWeaponMesh()->GetSocketLocation(MuzzleAttachPoint);
}

FVector AWeapon::GetMuzzleDirection() const
{
	return GetWeaponMesh()->GetSocketRotation(MuzzleAttachPoint).Vector();
}
