// Copyright Art-Sun Games. All Rights Reserved.

#include "Weapons/Projectile.h"

#include "PropHunt/PropHunt.h"
#include "Weapons/Weapon_Projectile.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/PHBlueprintFunctionLibrary.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Character/Prop/PropCharacter.h"

#include "Kismet/GameplayStatics.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"


AProjectile::AProjectile()
{
	CollisionComp = CreateDefaultSubobject<USphereComponent>("SphereComp");
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->AlwaysLoadOnClient = true;
	CollisionComp->AlwaysLoadOnServer = true;
	CollisionComp->bTraceComplexOnMove = false;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionObjectType(COLLISION_PROJECTILE);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	RootComponent = CollisionComp;

	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileComp");
	MovementComp->UpdatedComponent = CollisionComp;
	MovementComp->bRotationFollowsVelocity = true;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;

	bReplicates = true;
	NetUpdateFrequency = 100.f;
}

void AProjectile::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	auto OwnerWeapon = Cast<AWeapon_Projectile>(GetOwner());
	if (OwnerWeapon)
	{
		WeaponConfig = OwnerWeapon->GetWeaponConfig();
	}

	MovementComp->OnProjectileStop.AddDynamic(this, &AProjectile::OnImpact);
	MovementComp->Velocity = InitialVelocity;

	CollisionComp->MoveIgnoreActors.Add(GetInstigator());
	CollisionComp->SetSphereRadius(WeaponConfig.ProjectileRadius);

	SetLifeSpan(WeaponConfig.ProjectileLife);
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	auto PlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<APHPlayerState>();
	auto ASC = PlayerState ? PlayerState->GetAbilitySystemComponent<UPHAbilitySystemComponent>() : nullptr;
	if (ASC)
	{
		AbilitySystemComponent = ASC;
	}
}

void AProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (AbilitySystemComponent.IsValid())
	{
		FGameplayCueParameters CueParameters;
		CueParameters.RawMagnitude = WeaponConfig.ExplosionRadius;
		CueParameters.Location = GetActorLocation();
		CueParameters.SourceObject = GetOwner();
		CueParameters.Instigator = GetInstigator();

		AbilitySystemComponent->ExecuteGameplayCueLocal(ImpactGameplayCueTag, CueParameters);
	}

	Super::EndPlay(EndPlayReason);
}

void AProjectile::OnImpact(const FHitResult& HitResult)
{
	if (HasAuthority())
	{
		bool bTraceComplex = false;
		FCollisionQueryParams Params(SCENE_QUERY_STAT(AProjectile), bTraceComplex, GetInstigator());

		TArray<FHitResult> HitResults;
		GetWorld()->SweepMultiByObjectType(HitResults, GetActorLocation(), GetActorLocation() + 0.01f, FQuat::Identity, ECollisionChannel::ECC_Pawn, FCollisionShape::MakeSphere(WeaponConfig.ExplosionRadius), Params);

		TArray<FHitResult> ValidHitResults;
		TArray<AActor*> HitTargets;

		for (FHitResult& Hit : HitResults)
		{
			auto HitPropCharacter = Cast<APropCharacter>(Hit.GetActor());
			if (HitPropCharacter && HitPropCharacter->IsAlive() && !HitTargets.Contains(HitPropCharacter))
			{
				ValidHitResults.Add(Hit);
				HitTargets.Add(HitPropCharacter);
			}
		}

		EffectContainerSpec.AddTargets(ValidHitResults, TArray<AActor*>());
		EffectContainerSpec.ApplyExternally();

		Destroy();
	}
}

void AProjectile::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (MovementComp)
	{
		MovementComp->Velocity = NewVelocity;
	}
}
