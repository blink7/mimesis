// Copyright Art-Sun Games. All Rights Reserved.

#include "Weapons/Grenade.h"

#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"


AGrenade::AGrenade()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>("MeshComp");
	MeshComp->SetupAttachment(GetRootComponent());
}

void AGrenade::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	float VelocitySize = MovementComp->Velocity.Size();
	float NewPitch = FMath::Clamp(VelocitySize * DeltaSeconds * -4.f, -10.f, 0.f);
	RotationPitch = (RotationPitch == -360.f) ? 0.f : (RotationPitch + NewPitch);

	FRotator VelocityRotation = MovementComp->Velocity.ToOrientationRotator();
	MeshComp->SetWorldRotation({ RotationPitch, VelocityRotation.Yaw, VelocityRotation.Roll });
}
