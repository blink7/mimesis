// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHGameViewportClient.h"

#include "Core/UI/Menu/MainMenu.h"
#include "Core/UI/Menu/InGameMenu.h"
#include "Core/UI/Widgets/Dialog.h"

#include "UObject/ConstructorHelpers.h"


UPHGameViewportClient::UPHGameViewportClient(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UDialog> DialogBPClass(TEXT("/Game/PropHunt/UI/Widgets/WBP_Dialog"));
	DialogClass = DialogBPClass.Class;

	static ConstructorHelpers::FClassFinder<UPHUserWidget> TeamChoiceBPClass(TEXT("/Game/PropHunt/UI/Menu/WBP_TeamSelect"));
	TeamChoiceClass = TeamChoiceBPClass.Class;

	static ConstructorHelpers::FClassFinder<UPHUserWidget> ScoreboardBPClass(TEXT("/Game/PropHunt/UI/Widgets/WBP_Scoreboard"));
	ScoreboardClass = ScoreboardBPClass.Class;

	static ConstructorHelpers::FClassFinder<UPHUserWidget> MatchResultBPClass(TEXT("/Game/PropHunt/UI/Menu/WBP_MatchResult"));
	MatchResultClass = MatchResultBPClass.Class;

	static ConstructorHelpers::FClassFinder<UInGameMenu> InGameMenuBPClass(TEXT("/Game/PropHunt/UI/Menu/WBP_InGameMenu"));
	InGameMenuClass = InGameMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UPHUserWidget> DataLoadingBPClass(TEXT("/Game/PropHunt/UI/Widgets/WBP_DataLoadingIndicator"));
	DataLoadingClass = DataLoadingBPClass.Class;
}

void UPHGameViewportClient::ShowInGameMenu()
{
	if (!InGameMenu)
	{
		if (InGameWidget)
		{
			InGameWidget->HideWidget();
			HiddenInGameWidget = InGameWidget;
			InGameWidget = nullptr;
		}

		InGameMenu = CreateWidget<UInGameMenu>(GetWorld(), InGameMenuClass);
		InGameMenu->ShowWidget();
	}
}

void UPHGameViewportClient::HideInGameMenu()
{
	if (InGameMenu)
	{
		InGameMenu->HideWidget();
		InGameMenu = nullptr;

		if (HiddenInGameWidget)
		{
			HiddenInGameWidget->ShowWidget();
			InGameWidget = HiddenInGameWidget;
			HiddenInGameWidget = nullptr;
		}
	}
}

void UPHGameViewportClient::ShowScoreboard()
{
	ShowWidget(ScoreboardClass);
}

void UPHGameViewportClient::HideScoreboard()
{
	HideWidget(ScoreboardClass);
}

void UPHGameViewportClient::ShowTeamChoice()
{
	ShowWidget(TeamChoiceClass);
}

void UPHGameViewportClient::HideTeamChoice()
{
	HideWidget(TeamChoiceClass);
}

void UPHGameViewportClient::ToggleTeamChoice()
{
	if (InGameWidget)
	{
		HideTeamChoice();
	}
	else
	{
		ShowTeamChoice();
	}
}

void UPHGameViewportClient::ShowMatchResult()
{
	ShowWidget(MatchResultClass);
}

void UPHGameViewportClient::HideMatchResult()
{
	HideWidget(MatchResultClass);
}

UDialog* UPHGameViewportClient::ShowDialog(const FText& InHeader, const FText& Message, const FText& OKButtonText, const FText& CancelButtonText /*= FText::GetEmpty()*/)
{
	if (MainMenu.IsValid())
	{
		return MainMenu->ShowMessage(InHeader, Message, OKButtonText, CancelButtonText);
	}
	
	if (DialogWidgetStack.Num() > 0)
	{
		DialogWidgetStack.Last()->HideWidget();
	}

	const auto NewDialog = CreateWidget<UDialog>(GetWorld(), DialogClass);
	if (NewDialog)
	{
		NewDialog->ConstructMessageDialog(InHeader, Message, OKButtonText, CancelButtonText);
		NewDialog->ShowWidget();

		DialogWidgetStack.Add(NewDialog);
	}

	return NewDialog;
}

void UPHGameViewportClient::HideDialog()
{
	if (DialogWidgetStack.Num() > 0)
	{
		UDialog* Dialog = DialogWidgetStack.Last();
		Dialog->HideWidget();
		DialogWidgetStack.Remove(Dialog);

		if (DialogWidgetStack.Num() > 0)
		{
			Dialog = DialogWidgetStack.Last();
			Dialog->ShowWidget();
			return;
		}
	}
	
	if (InGameMenu)
	{
		InGameMenu->SetFocus();
	}
	else if (InGameWidget)
	{
		InGameWidget->SetFocus();
	}
	else if (MainMenu)
	{
		MainMenu->SetFocus();
	}
}

void UPHGameViewportClient::ShowDataLoadingIndicator()
{
	if (!DataLoadingWidget)
	{
		DataLoadingWidget = CreateWidget<UPHUserWidget>(GetWorld(), DataLoadingClass);
		DataLoadingWidget->AddToViewport(10);
	}
}

void UPHGameViewportClient::HideDataLoadingIndicator()
{
	if (DataLoadingWidget)
	{
		DataLoadingWidget->RemoveFromViewport();
		DataLoadingWidget = nullptr;
	}
}

bool UPHGameViewportClient::IsShowingInGameWidget()
{
	return InGameWidget || InGameMenu;
}

void UPHGameViewportClient::HideExistingWidgets()
{
	if (InGameWidget)
	{
		InGameWidget->HideWidget();
		InGameWidget = nullptr;
	}

	HiddenInGameWidget = nullptr;

	for (UDialog* Dialog : DialogWidgetStack)
	{
		Dialog->HideWidget();
	}
	DialogWidgetStack.Empty();

	HideInGameMenu();
}

void UPHGameViewportClient::SetMainMenu(const TSoftObjectPtr<UMainMenu>& InMainMenu)
{
	MainMenu = InMainMenu;
}

void UPHGameViewportClient::ShowWidget(TSubclassOf<UPHUserWidget> WidgetClass)
{
	if (!InGameWidget && !HiddenInGameWidget)
	{
		const auto NewWidget = CreateWidget<UPHUserWidget>(GetWorld(), WidgetClass);;
		if (InGameMenu)
		{
			HiddenInGameWidget = NewWidget;
		}
		else
		{
			InGameWidget = NewWidget;
			InGameWidget->ShowWidget();
		}
	}
}

void UPHGameViewportClient::HideWidget(TSubclassOf<UPHUserWidget> WidgetClass)
{
	if (InGameWidget && InGameWidget->IsA(WidgetClass))
	{
		InGameWidget->HideWidget();
		InGameWidget = nullptr;
	}
	else if (HiddenInGameWidget)
	{
		HiddenInGameWidget = nullptr;
	}
}
