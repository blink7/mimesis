// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHGameInstance.h"

#include "PropHunt/PropHunt.h"
#include "Core/PHTypes.h"
#include "Core/PHAssetManager.h"
#include "Core/PHGameUserSettings.h"
#include "Core/PHPlayerController.h"
#include "Core/PHGameViewportClient.h"
#include "Core/PHPlayerController_Menu.h"
#include "Core/UI/Menu/MainMenu.h"
#include "Core/UI/Widgets/Dialog.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/Online/PHGameSession.h"
#include "Core/Online/PHSprayHttpClient.h"
#include "Core/Online/PHPlayerHttpClient.h"
#include "Core/Online/PHPatchHttpClient.h"
#include "Core/Online/PHStatisticsHttpClient.h"
#include "Core/Online/PHCustomizationHttpClient.h"
#include "Core/Online/PHOnlineAsyncTaskManagerSteam.h"
#include "Core/Items/PHItem.h"
#include "Core/Items/PHSkillItem.h"
#include "Core/Items/PHWeaponItem.h"
#include "Core/Items/PHTauntDataAsset.h"
#include "PropHuntLoadingScreen.h"

#include "ChunkDownloader.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Engine/DataTable.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameModeBase.h"

DEFINE_LOG_CATEGORY(LogPatch);

const static FString MAP_DIR("/Game/PropHunt/Maps/");

FAutoConsoleVariable CVarPropHuntTestEncryption(TEXT("PropHunt.TestEncryption"), 0, TEXT("If true, clients will send an encryption token with their request to join the server and attempt to encrypt the connection using a debug key. This is NOT SECURE and for demonstration purposes only."));

namespace PHGameInstanceState
{
	const FName None = FName(TEXT("None"));
	const FName PendingInvite = FName(TEXT("PendingInvite"));
	const FName WelcomeScreen = FName(TEXT("WelcomeScreen"));
	const FName MainMenu = FName(TEXT("MainMenu"));
	const FName MessageMenu = FName(TEXT("MessageMenu"));
	const FName Playing = FName(TEXT("Playing"));
}


UPHGameInstance::UPHGameInstance() :
	CurrentState(PHGameInstanceState::None),
	OnlineMode(EOnlineMode::Online)
{
}

bool UPHGameInstance::Tick(float DeltaSeconds)
{
	// Dedicated server doesn't need to worry about game state
	if (IsRunningDedicatedServer())
	{
		return true;
	}

	MaybeChangeState();

	if (OnlineAsyncTaskThreadRunnable)
	{
		OnlineAsyncTaskThreadRunnable->GameTick();
	}

	return true;
}

APHGameSession* UPHGameInstance::GetGameSession() const
{
	AGameModeBase* Game = GetWorld() ? GetWorld()->GetAuthGameMode() : nullptr;
	if (Game)
	{
		return Cast<APHGameSession>(Game->GameSession);
	}

	return nullptr;
}

void UPHGameInstance::Init()
{
	Super::Init();

	CurrentConnectionStatus = EOnlineServerConnectionStatus::Connected;

	// game requires the ability to ID users.
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	check(OnlineSub);

	const IOnlineIdentityPtr IdentityInterface = OnlineSub->GetIdentityInterface();
	check(IdentityInterface.IsValid());

	const IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();
	check(SessionInterface.IsValid());

	OnNotifyPreClientTravel().AddUObject(this, &UPHGameInstance::OnPreClientTravel);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UPHGameInstance::OnPostLoadMap);

	OnEndSessionCompleteDelegate = FOnEndSessionCompleteDelegate::CreateUObject(this, &UPHGameInstance::OnEndSessionComplete);

	// Register delegate for ticker callback
	TickDelegate = FTickerDelegate::CreateUObject(this, &UPHGameInstance::Tick);
	TickDelegateHandle = FTicker::GetCoreTicker().AddTicker(TickDelegate);

	const auto UserSettings = CastChecked<UPHGameUserSettings>(GEngine->GetGameUserSettings());
	if (UserSettings)
	{
		UserSettings->ApplySoundSettings();
	}

	PatchHttpClient = NewObject<UPHPatchHttpClient>();
	PlayerHttpClient = NewObject<UPHPlayerHttpClient>();
	StatisticsHttpClient = NewObject<UPHStatisticsHttpClient>();
	CustomizationHttpClient = NewObject<UPHCustomizationHttpClient>();
	SprayHttpClient = NewObject<UPHSprayHttpClient>();

	MapListTable->GetAllRows("LoadMapList", MapList);

	FCoreDelegates::OnControllerConnectionChange.AddUObject(this, &UPHGameInstance::OnUserControllerConnectionChange);

	// Create the online async task thread
	OnlineAsyncTaskThreadRunnable = new FPHOnlineAsyncTaskManagerSteam(this);
	check(OnlineAsyncTaskThreadRunnable);
	
	LoadAssets();
}

void UPHGameInstance::Shutdown()
{
	Super::Shutdown();

	// Unregister ticker delegate
	FTicker::GetCoreTicker().RemoveTicker(TickDelegateHandle);

	if (OnlineAsyncTaskThreadRunnable)
	{
		delete OnlineAsyncTaskThreadRunnable;
		OnlineAsyncTaskThreadRunnable = nullptr;
	}

	FChunkDownloader::Shutdown();
}

void UPHGameInstance::StartGameInstance()
{
	GotoInitialState();
}

bool UPHGameInstance::HostGame(ULocalPlayer* InLocalPlayer, const FString& InMapName, const FString& InServerName, int32 InNumPlayers, const FString& InGameType, bool bInRecordDemo, bool bInAllowInternationalTaunts)
{
	MapName = "Lobby";
	TravelURL = MAP_DIR + MapName;

	bool const bIsLanMatch = GetOnlineMode() == EOnlineMode::LAN;

	if (GetOnlineMode() != EOnlineMode::Offline)
	{
		TravelURL += "?listen";
	}
	if (bIsLanMatch)
	{
		TravelURL += "?bIsLanMatch";
	}
	if (bInRecordDemo)
	{
		TravelURL += "?DemoRec";
	}

	if (GetOnlineMode() == EOnlineMode::Offline)
	{
		//
		// Offline game, just go straight to map
		//

		ShowLoadingScreen();
		GotoState(PHGameInstanceState::Playing);

		// Travel to the specified match URL
		GetWorld()->ServerTravel(TravelURL);
		return true;
	}

	//
	// Online game
	//

	const auto GameSession = GetGameSession();
	if (GameSession)
	{
		ServerName = InServerName;
		GameType = InGameType;
		MaxPlayers = InNumPlayers;
		HostMapName = InMapName;
		ServerLang = bInAllowInternationalTaunts ? FString() : UPHGameUserSettings::GetPHGameUserSettings()->GetTauntLang();

		// add callback delegate for completion
		OnCreatePresenceSessionCompleteDelegateHandle = GameSession->OnCreatePresenceSessionComplete().AddUObject(this, &UPHGameInstance::OnCreatePresenceSessionComplete);

		if (GameSession->HostSession(InLocalPlayer->GetPreferredUniqueNetId().GetUniqueNetId(), NAME_GameSession, InGameType, InServerName, MapName, bIsLanMatch, true, InNumPlayers))
		{
			// If any error occurred in the above, pending state would be set
			if ((PendingState == CurrentState) || (PendingState == PHGameInstanceState::None))
			{
				// Go ahead and go into loading state now
				// If we fail, the delegate will handle showing the proper messaging and move to the correct state
				ShowLoadingScreen();
				GotoState(PHGameInstanceState::Playing);
				return true;
			}
		}
	}

	return false;
}

bool UPHGameInstance::JoinSession(ULocalPlayer* LocalPlayer, int32 SessionIndexInSearchResults)
{
	// needs to tear anything down based on current state?

	if (APHGameSession* GameSession = GetGameSession())
	{
		AddNetworkFailureHandlers();
		OnJoinSessionCompleteDelegateHandle = GameSession->OnJoinSessionComplete().AddUObject(this, &UPHGameInstance::OnJoinSessionComplete);

		if (GameSession->GetSearchResults().IsValidIndex(SessionIndexInSearchResults))
		{
			const FOnlineSessionSearchResult& SearchResult = GameSession->GetSearchResults()[SessionIndexInSearchResults];

			if (GameSession->JoinSession(LocalPlayer->GetPreferredUniqueNetId().GetUniqueNetId(), NAME_GameSession, SearchResult))
			{
				// If any error occurred in the above, pending state would be set
				if ((PendingState == CurrentState) || (PendingState == PHGameInstanceState::None))
				{
					// Save session info for LoadingScreen message
					MaxPlayers = SearchResult.Session.NumOpenPublicConnections;
					CurrentPlayers = SearchResult.Session.SessionSettings.NumPublicConnections
						+ SearchResult.Session.SessionSettings.NumPrivateConnections
						- SearchResult.Session.NumOpenPublicConnections
						- SearchResult.Session.NumOpenPrivateConnections;
					SearchResult.Session.SessionSettings.Get(SETTING_MAPNAME, MapName);

					// If we fail, the delegate will handle showing the proper messaging and move to the correct state
					ShowLoadingScreen();
					GotoState(PHGameInstanceState::Playing);
					return true;
				}
			}
		}
	}

	return false;
}

bool UPHGameInstance::UpdateGame(const FString& InMapName)
{
	const auto GameSession = GetGameSession();
	if (GameSession)
	{
		FString TravelURL_Right = TravelURL.RightChop(TravelURL.Find("?"));
		const int32 GameTypeSubStrIndex = TravelURL_Right.Find("?game=");
		if (GameTypeSubStrIndex != INDEX_NONE)
		{
			TravelURL_Right = TravelURL_Right.Left(GameTypeSubStrIndex);
		}
		TravelURL_Right += "?game=" + GameType;
		
		TravelURL = MAP_DIR + InMapName + TravelURL_Right;
		MapName = InMapName;

		OnUpdateSessionCompleteDelegateHandle = GameSession->OnUpdateSessionComplete().AddUObject(this, &UPHGameInstance::OnUpdateSessionComplete);
		if (GameSession->UpdateSession(NAME_GameSession, GameType, InMapName))
		{
			return true;
		}
	}

	return false;
}

bool UPHGameInstance::FindSessions(ULocalPlayer* PlayerOwner, bool bIsDedicatedServer, bool bIsLanMatch)
{
	bool bResult = false;

	check(PlayerOwner);
	if (PlayerOwner)
	{
		if (APHGameSession* GameSession = GetGameSession())
		{
			GameSession->OnFindSessionsComplete().RemoveAll(this);
			OnSearchSessionsCompleteDelegateHandle = GameSession->OnFindSessionsComplete().AddUObject(this, &UPHGameInstance::OnSearchSessionsComplete);

			GameSession->FindSessions(PlayerOwner->GetPreferredUniqueNetId().GetUniqueNetId(), NAME_GameSession, bIsLanMatch, !bIsDedicatedServer);

			bResult = true;
		}
	}

	return bResult;
}

void UPHGameInstance::GotoState(FName NewState)
{
	UE_LOG(LogOnline, Log, TEXT("GotoState: NewState: %s"), *NewState.ToString());

	PendingState = NewState;
}

FName UPHGameInstance::GetInitialState()
{
	// On PC, go directly to the main menu
	return PHGameInstanceState::MainMenu;
}

void UPHGameInstance::GotoInitialState()
{
	GotoState(GetInitialState());
}

void UPHGameInstance::ShowMessageThenGotoState(const FText& Header, const FText& Message, const FText& OKButtonString, const FText& CancelButtonString, const FName& NewState, const bool OverrideExisting /*= true*/, TWeakObjectPtr<ULocalPlayer> PlayerOwner /*= nullptr*/)
{
	UE_LOG(LogOnline, Log, TEXT("ShowMessageThenGotoState: Message: %s, NewState: %s"), *Message.ToString(), *NewState.ToString());

	const bool bAtWelcomeScreen = PendingState == PHGameInstanceState::WelcomeScreen || CurrentState == PHGameInstanceState::WelcomeScreen;

	// Never override the welcome screen
	if (bAtWelcomeScreen)
	{
		UE_LOG(LogOnline, Log, TEXT("ShowMessageThenGotoState: Ignoring due to higher message priority in queue (at welcome screen)."));
		return;
	}

	const bool bAlreadyAtMessageMenu = PendingState == PHGameInstanceState::MessageMenu || CurrentState == PHGameInstanceState::MessageMenu;
	const bool bAlreadyAtDestState = PendingState == NewState || CurrentState == NewState;

	// If we are already going to the message menu, don't override unless asked to
	if (bAlreadyAtMessageMenu && PendingMessage.NextState == NewState && !OverrideExisting)
	{
		UE_LOG(LogOnline, Log, TEXT("ShowMessageThenGotoState: Ignoring due to higher message priority in queue (check 1)."));
		return;
	}

	// If we are already going to the message menu, and the next dest is welcome screen, don't override
	if (bAlreadyAtMessageMenu && PendingMessage.NextState == PHGameInstanceState::WelcomeScreen)
	{
		UE_LOG(LogOnline, Log, TEXT("ShowMessageThenGotoState: Ignoring due to higher message priority in queue (check 2)."));
		return;
	}

	// If we are already at the dest state, don't override unless asked
	if (bAlreadyAtDestState && !OverrideExisting)
	{
		UE_LOG(LogOnline, Log, TEXT("ShowMessageThenGotoState: Ignoring due to higher message priority in queue (check 3)"));
		return;
	}

	PendingMessage.DisplayHeader = Header;
	PendingMessage.DisplayString = Message;
	PendingMessage.OKButtonString = OKButtonString;
	PendingMessage.CancelButtonString = CancelButtonString;
	PendingMessage.NextState = NewState;

	if (CurrentState == PHGameInstanceState::MessageMenu)
	{
		UE_LOG(LogOnline, Log, TEXT("ShowMessageThenGotoState: Forcing new message"));
		EndMessageMenuState();
		BeginMessageMenuState();
	}
	else
	{
		GotoState(PHGameInstanceState::MessageMenu);
	}
}

UDialog* UPHGameInstance::ShowMessage(const FText& Header, const FText& Message, const FText& OKButtonString, const FText& CancelButtonString /*= FText::GetEmpty()*/)
{
	if (auto GameViewportClient = GetPHGameViewportClient())
	{
		return GameViewportClient->ShowDialog(Header, Message, OKButtonString, CancelButtonString);
	}

	return nullptr;
}

void UPHGameInstance::ShowDataLoading(bool bEnable)
{
	if (auto GameViewportClient = GetPHGameViewportClient())
	{
		bEnable ? GameViewportClient->ShowDataLoadingIndicator() : GameViewportClient->HideDataLoadingIndicator();
	}
}

void UPHGameInstance::SetOnlineMode(EOnlineMode InOnlineMode)
{
	OnlineMode = InOnlineMode;
	UpdateUsingMultiplayerFeatures(InOnlineMode == EOnlineMode::Online);
}

void UPHGameInstance::UpdateUsingMultiplayerFeatures(bool bIsUsingMultiplayerFeatures)
{
	if (IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get())
	{
		for (ULocalPlayer* LocalPlayer : LocalPlayers)
		{
			FUniqueNetIdRepl PlayerId = LocalPlayer->GetPreferredUniqueNetId();
			if (PlayerId.IsValid())
			{
				OnlineSub->SetUsingMultiplayerFeatures(*PlayerId, bIsUsingMultiplayerFeatures);
			}
		}
	}
}

void UPHGameInstance::CleanupSessionOnReturnToMenu()
{
	bool bPendingOnlineOp = false;

	// end online game and then destroy it
	const auto OnlineSub = IOnlineSubsystem::Get();
	IOnlineSessionPtr Sessions = OnlineSub ? OnlineSub->GetSessionInterface() : nullptr;

	if (Sessions.IsValid())
	{
		const FName GameSession(NAME_GameSession);
		const EOnlineSessionState::Type SessionState = Sessions->GetSessionState(GameSession);
		UE_LOG(LogOnline, Log, TEXT("Session %s is '%s'"), *GameSession.ToString(), EOnlineSessionState::ToString(SessionState));

		switch (SessionState)
		{
			case EOnlineSessionState::InProgress:
				UE_LOG(LogOnline, Log, TEXT("Ending session %s on return to main menu"), *GameSession.ToString());
				OnEndSessionCompleteDelegateHandle = Sessions->AddOnEndSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
				Sessions->EndSession(GameSession);
				bPendingOnlineOp = true;
				break;
			case EOnlineSessionState::Ending:
				UE_LOG(LogOnline, Log, TEXT("Waiting for session %s to end on return to main menu"), *GameSession.ToString());
				OnEndSessionCompleteDelegateHandle = Sessions->AddOnEndSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
				bPendingOnlineOp = true;
				break;
			case EOnlineSessionState::Ended:
			case EOnlineSessionState::Pending:
				UE_LOG(LogOnline, Log, TEXT("Destroying session %s on return to main menu"), *GameSession.ToString());
				OnDestroySessionCompleteDelegateHandle = Sessions->AddOnDestroySessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
				Sessions->DestroySession(GameSession);
				bPendingOnlineOp = true;
				break;
			case EOnlineSessionState::Starting:
			case EOnlineSessionState::Creating:
				UE_LOG(LogOnline, Log, TEXT("Waiting for session %s to start, and then we will end it to return to main menu"), *GameSession.ToString());
				OnStartSessionCompleteDelegateHandle = Sessions->AddOnStartSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegate);
				bPendingOnlineOp = true;
				break;
		}
	}

	if (!bPendingOnlineOp)
	{
		//GEngine->HandleDisconnect( GetWorld(), GetWorld()->GetNetDriver() );
	}

	ResetRoundCount();
}

void UPHGameInstance::LabelPlayerAsQuitter(ULocalPlayer* LocalPlayer) const
{
	const auto PlayerState = LocalPlayer && LocalPlayer->PlayerController ? Cast<APHPlayerState>(LocalPlayer->PlayerController->PlayerState) : nullptr;
	if (PlayerState)
	{
		PlayerState->SetQuitter(true);
	}
}

void UPHGameInstance::StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& Delegate, EUserPrivileges::Type Privilege, const FUniqueNetId& UserId)
{
// 	WaitMessageWidget = SNew(SShooterWaitDialog).MessageText(NSLOCTEXT("NetworkStatus", "CheckingPrivilegesWithServer", "Checking privileges with server.  Please wait..."));

	if (GEngine && GEngine->GameViewport)
	{
// 		UGameViewportClient* const GVC = GEngine->GameViewport;
// 		GVC->AddViewportWidgetContent(WaitMessageWidget.ToSharedRef());
	}

	auto Identity = Online::GetIdentityInterface();
	if (Identity.IsValid() && UserId.IsValid())
	{
		Identity->GetUserPrivilege(UserId, Privilege, Delegate);
	}
	else
	{
		// Can only get away with faking the UniqueNetId here because the delegates don't use it
		Delegate.ExecuteIfBound(FUniqueNetIdString(), Privilege, static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::NoFailures));
	}
}

void UPHGameInstance::CleanupOnlinePrivilegeTask()
{
	/*if (GEngine && GEngine->GameViewport && WaitMessageWidget.IsValid())
	{
		UGameViewportClient* const GVC = GEngine->GameViewport;
		GVC->RemoveViewportWidgetContent(WaitMessageWidget.ToSharedRef());
	}*/
}

void UPHGameInstance::DisplayOnlinePrivilegeFailureDialogs(const FUniqueNetId& UserId, EUserPrivileges::Type Privilege, uint32 PrivilegeResults)
{
	// Show warning that the user cannot play due to age restrictions
	TWeakObjectPtr<ULocalPlayer> OwningPlayer;
	if (GEngine)
	{
		for (auto It = GEngine->GetLocalPlayerIterator(GetWorld()); It; ++It)
		{
			FUniqueNetIdRepl OtherId = (*It)->GetPreferredUniqueNetId();
			if (OtherId.IsValid())
			{
				if (UserId == (*OtherId))
				{
					OwningPlayer = *It;
				}
			}
		}
	}

	if (OwningPlayer.IsValid())
	{
		if ((PrivilegeResults & static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::AccountTypeFailure)) != 0)
		{
			IOnlineExternalUIPtr ExternalUI = Online::GetExternalUIInterface();
			if (ExternalUI.IsValid())
			{
				ExternalUI->ShowAccountUpgradeUI(UserId);
			}
		}
		else if ((PrivilegeResults & static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::RequiredSystemUpdate)) != 0)
		{
			PendingMessage.DisplayString = NSLOCTEXT("OnlinePrivilegeResult", "RequiredSystemUpdate", "A required system update is available.  Please upgrade to access online features.");
		}
		else if ((PrivilegeResults & static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::RequiredPatchAvailable)) != 0)
		{
			PendingMessage.DisplayString = NSLOCTEXT("OnlinePrivilegeResult", "RequiredPatchAvailable", "A required game patch is available.  Please upgrade to access online features.");
		}
		else if ((PrivilegeResults & static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::AgeRestrictionFailure)) != 0)
		{
			PendingMessage.DisplayString = NSLOCTEXT("OnlinePrivilegeResult", "AgeRestrictionFailure", "Cannot play due to age restrictions!");
		}
		else if ((PrivilegeResults & static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::UserNotFound)) != 0)
		{
			PendingMessage.DisplayString = NSLOCTEXT("OnlinePrivilegeResult", "UserNotFound", "Cannot play due invalid user!");
		}
		else if ((PrivilegeResults & static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::GenericFailure)) != 0)
		{
			PendingMessage.DisplayString = NSLOCTEXT("OnlinePrivilegeResult", "GenericFailure", "Cannot play online.  Check your network connection.");
		}

		PendingMessage.OKButtonString = NSLOCTEXT("DialogButtons", "OKAY", "OK");
		PendingMessage.CancelButtonString = FText::GetEmpty();
		PendingMessage.NextState = PHGameInstanceState::MainMenu;

		if (CurrentState == PHGameInstanceState::MessageMenu)
		{
			UE_LOG(LogOnline, Log, TEXT("ShowMessageThenGotoState: Forcing new message"));
			EndMessageMenuState();
			BeginMessageMenuState();
		}
		else
		{
			GotoState(PHGameInstanceState::MessageMenu);
		}
	}
}

void UPHGameInstance::SetMainMenu(UMainMenu* MenuWidget)
{
	MainMenu = MenuWidget;

	if (UPHGameViewportClient* GameViewportClient = GetPHGameViewportClient())
	{
		GameViewportClient->SetMainMenu(MenuWidget);
	}
}

void UPHGameInstance::OnPreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel)
{
	// Handle only seamless travel
	UE_LOG(LogLoad, Warning, TEXT("OnPreClientTravel: %s"), *CurrentState.ToString());
	if (CurrentState != PHGameInstanceState::MainMenu)
	{
		// Compute the next URL, and pull the map out of it. This handles short->long package name conversion
		const FURL NextURL = FURL(&GetWorldContext()->LastURL, *PendingURL, TRAVEL_Absolute);
		MapName = NextURL.Map.RightChop(MAP_DIR.Len());

		ShowLoadingScreen(bIsSeamlessTravel);
	}
}

void UPHGameInstance::OnPostLoadMap(UWorld* LoadedWorld)
{
	// Make sure we hide the loading screen when the level is done loading
	HideLoadingScreen();
}

void UPHGameInstance::OnUserControllerConnectionChange(bool bConnected, FPlatformUserId UserId, int32 ControllerId)
{
	UE_LOG(LogGameSession, Warning, TEXT("OnUserControllerConnectionChange"));
	bControllerConnected = bConnected;
}

void UPHGameInstance::OnEndSessionComplete(FName SessionName, bool bWasSuccessful)
{
	UE_LOG(LogGameSession, Log, TEXT("UPHGameInstance::OnEndSessionComplete: Session=%s bWasSuccessful=%s"), *SessionName.ToString(), bWasSuccessful ? TEXT("true") : TEXT("false"));

	const auto OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			Sessions->ClearOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegateHandle);
			Sessions->ClearOnEndSessionCompleteDelegate_Handle(OnEndSessionCompleteDelegateHandle);
			Sessions->ClearOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegateHandle);
		}
	}

	// continue
	CleanupSessionOnReturnToMenu();
}

void UPHGameInstance::MaybeChangeState()
{
	if ((PendingState != CurrentState) && (PendingState != PHGameInstanceState::None))
	{
		const FName OldState = CurrentState;

		// end current state
		EndCurrentState(PendingState);

		// begin new state
		BeginNewState(PendingState, OldState);

		// clear pending change
		PendingState = PHGameInstanceState::None;
	}
}

void UPHGameInstance::EndCurrentState(FName NextState)
{
	// per-state custom ending code here
	if (CurrentState == PHGameInstanceState::PendingInvite)
	{
// 		EndPendingInviteState();
	}
	else if (CurrentState == PHGameInstanceState::WelcomeScreen)
	{
// 		EndWelcomeScreenState();
	}
	else if (CurrentState == PHGameInstanceState::MainMenu)
	{
		EndMainMenuState();
	}
	else if (CurrentState == PHGameInstanceState::MessageMenu)
	{
		EndMessageMenuState();
	}
	else if (CurrentState == PHGameInstanceState::Playing)
	{
		EndPlayingState();
	}

	CurrentState = PHGameInstanceState::None;
}

void UPHGameInstance::BeginNewState(FName NewState, FName PrevState)
{
	if (NewState == PHGameInstanceState::PendingInvite)
	{
		// 		BeginPendingInviteState();
	}
	else if (NewState == PHGameInstanceState::WelcomeScreen)
	{
		// 		BeginWelcomeScreenState();
	}
	else if (NewState == PHGameInstanceState::MainMenu)
	{
		BeginMainMenuState();
	}
	else if (NewState == PHGameInstanceState::MessageMenu)
	{
		BeginMessageMenuState();
	}
	else if (NewState == PHGameInstanceState::Playing)
	{
		BeginPlayingState();
	}

	CurrentState = NewState;
}

void UPHGameInstance::BeginWelcomeScreenState()
{
	SetOnlineMode(EOnlineMode::Offline);

	LoadFrontEndMap(WelcomeScreenLevel);

	const auto LocalPlayer = GetFirstGamePlayer();
	LocalPlayer->SetCachedUniqueNetId(nullptr);
	// 	check(!WelcomeMenuUI.IsValid());
	// 	WelcomeMenuUI = CreateWidget<UWelcomeMenu>(GetWorld(), WelcomeMenuClass);
	// 	WelcomeMenuUI->Construct(this, Player);
	// 	WelcomeMenuUI->AddToViewport();

		// Disallow splitscreen (we will allow while in the playing state)
	GetGameViewportClient()->SetForceDisableSplitscreen(true);
}

void UPHGameInstance::BeginMainMenuState()
{
	HideLoadingScreen();

	SetOnlineMode(EOnlineMode::Offline);

	// Set presence to menu state for the owning player
	SetPresenceForLocalPlayers(FString(TEXT("In Menu")), FVariantData(FString(TEXT("OnMenu"))));

	// load startup map
	LoadFrontEndMap(MainMenuLevel);
}

void UPHGameInstance::BeginMessageMenuState()
{
	if (PendingMessage.DisplayString.IsEmpty())
	{
		UE_LOG(LogOnlineGame, Warning, TEXT("UPHGameInstance::BeginMessageMenuState: Display string is empty"));
		GotoInitialState();
		return;
	}

	HideLoadingScreen();

	if (auto GameViewportClient = GetPHGameViewportClient())
	{
		UDialog* MessageDialog = GameViewportClient->ShowDialog(PendingMessage.DisplayHeader, PendingMessage.DisplayString, PendingMessage.OKButtonString, PendingMessage.CancelButtonString);
		if (MessageDialog)
		{
			MessageDialog->OnConfirmClicked.AddUObject(this, &UPHGameInstance::GotoState, PendingMessage.NextState);
		}
	}

	PendingMessage.DisplayString = FText::GetEmpty();
}

void UPHGameInstance::BeginPlayingState()
{
	// Set presence for playing in a map
	SetPresenceForLocalPlayers(FString(TEXT("In Game")), FVariantData(FString(TEXT("InGame"))));
}

void UPHGameInstance::EndMainMenuState()
{
}

void UPHGameInstance::EndMessageMenuState()
{
	if (auto GameViewportClient = GetPHGameViewportClient())
	{
		GameViewportClient->HideDialog();
	}
}

void UPHGameInstance::EndPlayingState()
{
	// Clear the players' presence information
	SetPresenceForLocalPlayers(FString(TEXT("In Menu")), FVariantData(FString(TEXT("OnMenu"))));

	const auto World = GetWorld();
	const auto GameState = World ? World->GetGameState<APHGameState>() : nullptr;
	if (GameState)
	{
		// Send round end events for local players
		for (const auto LocalPlayer : LocalPlayers)
		{
			const auto PC = Cast<APHPlayerController>(LocalPlayer->PlayerController);
			if (PC)
			{
				// Assuming you can't win if you quit early
				PC->ClientSendRoundEndEvent(false, GameState->ElapsedTime);
			}
		}

		// Give the game state a chance to cleanup first
		GameState->RequestFinishAndExitToMainMenu();
	}
	else
	{
		// If there is no game state, make sure the session is in a good state
		CleanupSessionOnReturnToMenu();
	}
}

void UPHGameInstance::ShowLoadingScreen(bool bSeamlessTravel /*= false*/)
{
	if (const auto GameViewportClient = GetPHGameViewportClient())
	{
		// Hide all widgets before loading
		GameViewportClient->HideExistingWidgets();
	}

	const IOnlineSessionPtr Session = IOnlineSubsystem::Get()->GetSessionInterface();
	if (const FOnlineSessionSettings* CurrentSettings = Session->GetSessionSettings(GameSessionName))
	{
		MaxPlayers = CurrentSettings->NumPublicConnections;
	}

	if (CurrentState == PHGameInstanceState::Playing)
	{
		if (const AGameStateBase* GameState = GetWorld()->GetGameState())
		{
			CurrentPlayers = GameState->PlayerArray.Num();
		}
	}

	const FMapRow* const* FoundMapRow = GetMapList().FindByPredicate([this](const FMapRow* TestRow) {
		return TestRow->FileName == MapName;
	});

	FText MapLabel;
	if (FoundMapRow)
	{
		MapLabel = (*FoundMapRow)->Label;
	}
	else
	{
		MapLabel = FText::FromString(MapName);
	}

	const FText LoadingText = FText::Format(NSLOCTEXT("LoadingScreen", "MapPlayersInfo", "Next Location: {0}\r\nPlayers: {1}/{2}"), MapLabel, FText::AsNumber(CurrentPlayers), FText::AsNumber(MaxPlayers));

	IPropHuntLoadingScreenModule& LoadingScreenModule = IPropHuntLoadingScreenModule::Get();
	LoadingScreenModule.StartInGameLoadingScreen(LoadingText, bSeamlessTravel);
}

void UPHGameInstance::HideLoadingScreen()
{
	IPropHuntLoadingScreenModule& LoadingScreenModule = IPropHuntLoadingScreenModule::Get();
	LoadingScreenModule.StopInGameLoadingScreen();
}

void UPHGameInstance::AddNetworkFailureHandlers()
{
	// Add network/travel error handlers (if they are not already there)
	if (!GEngine->OnTravelFailure().IsBoundToObject(this))
	{
		TravelLocalSessionFailureDelegateHandle = GEngine->OnTravelFailure().AddUObject(this, &UPHGameInstance::TravelLocalSessionFailure);
	}
}

void UPHGameInstance::RemoveNetworkFailureHandlers()
{
	// Remove the local session/travel failure bindings if they exist
	if (GEngine->OnTravelFailure().IsBoundToObject(this))
	{
		GEngine->OnTravelFailure().Remove(TravelLocalSessionFailureDelegateHandle);
	}
}

void UPHGameInstance::TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString& ErrorString)
{
	const auto FirstPC = Cast<APHPlayerController_Menu>(World->GetFirstPlayerController());
	if (FirstPC)
	{
		FText ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Join Session failed.");
		if (!ErrorString.IsEmpty())
		{
			ReturnReason = FText::Format(NSLOCTEXT("NetworkErrors", "JoinSessionFailedReasonFmt", "Join Session failed. {0}"), FText::FromString(ErrorString));
		}

		ShowMessageThenGoMain(NSLOCTEXT("DialogHeader", "Error", "Error"), 
			ReturnReason, 
			NSLOCTEXT("DialogButtons", "OKAY", "OK"));
	}
}

void UPHGameInstance::OnJoinSessionComplete(EOnJoinSessionCompleteResult::Type Result)
{
	// unhook the delegate
	const auto GameSession = GetGameSession();
	if (GameSession)
	{
		GameSession->OnJoinSessionComplete().Remove(OnJoinSessionCompleteDelegateHandle);
	}

	FinishJoinSession(Result);
}

void UPHGameInstance::OnCreatePresenceSessionComplete(FName SessionName, bool bWasSuccessful)
{
	const auto GameSession = GetGameSession();
	if (GameSession)
	{
		GameSession->OnCreatePresenceSessionComplete().Remove(OnCreatePresenceSessionCompleteDelegateHandle);

		// We either failed or there is only a single local user
		FinishSessionCreation(bWasSuccessful ? EOnJoinSessionCompleteResult::Success : EOnJoinSessionCompleteResult::UnknownError);
	}
}

void UPHGameInstance::OnUpdateSessionComplete(FName SessionName, bool bWasSuccessful)
{
	const auto GameSession = GetGameSession();
	if (GameSession)
	{
		GameSession->OnUpdateSessionComplete().Remove(OnUpdateSessionCompleteDelegateHandle);

		if (bWasSuccessful)
		{
			GetWorld()->ServerTravel(TravelURL);
		}
		else
		{
			ShowMessageThenGoMain(NSLOCTEXT("DialogHeader", "Error", "Error"), 
				NSLOCTEXT("NetworkErrors", "UpdateSessionFailed", "Failed to update session."), 
				NSLOCTEXT("DialogButtons", "OKAY", "OK"));
		}
	}
}

void UPHGameInstance::FinishSessionCreation(EOnJoinSessionCompleteResult::Type Result)
{
	if (Result == EOnJoinSessionCompleteResult::Success)
	{
		// This will send any Play Together invites if necessary, or do nothing.
// 		SendPlayTogetherInvites();

		// Travel to the specified match URL
		GetWorld()->ServerTravel(TravelURL);
	}
	else
	{
		ShowMessageThenGoMain(NSLOCTEXT("DialogHeader", "Error", "Error"), 
			NSLOCTEXT("NetworkErrors", "CreateSessionFailed", "Failed to create session."),
			NSLOCTEXT("DialogButtons", "OKAY", "OK"));
	}
}

void UPHGameInstance::FinishJoinSession(EOnJoinSessionCompleteResult::Type Result)
{
	if (Result != EOnJoinSessionCompleteResult::Success)
	{
		FText ReturnReason;
		switch (Result)
		{
			case EOnJoinSessionCompleteResult::SessionIsFull:
				ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Game is full.");
				break;
			case EOnJoinSessionCompleteResult::SessionDoesNotExist:
				ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Game no longer exists.");
				break;
			default:
				ReturnReason = NSLOCTEXT("NetworkErrors", "JoinSessionFailed", "Join failed.");
				break;
		}

		RemoveNetworkFailureHandlers();

		ShowMessageThenGoMain(NSLOCTEXT("DialogHeader", "Error", "Error"), 
			ReturnReason, 
			NSLOCTEXT("DialogButtons", "OKAY", "OK"));

		return;
	}

	InternalTravelToSession(NAME_GameSession);
}

void UPHGameInstance::ShowMessageThenGoMain(const FText& Header, const FText& Message, const FText& OKButtonString, const FText& CancelButtonString /*= FText::GetEmpty()*/)
{
	ShowMessageThenGotoState(Header, Message, OKButtonString, CancelButtonString, PHGameInstanceState::MainMenu);
}

void UPHGameInstance::OnSearchSessionsComplete(bool bWasSuccessful)
{
	const auto Session = GetGameSession();
	if (Session)
	{
		Session->OnFindSessionsComplete().Remove(OnSearchSessionsCompleteDelegateHandle);
	}
}

bool UPHGameInstance::LoadFrontEndMap(const TSoftObjectPtr<UWorld>& LevelToLoad)
{
	if (LevelToLoad.IsNull())
	{
		UE_LOG(LogLoad, Warning, TEXT("Pointer to the loading level is NULL"));
		return false;
	}
	
	const FString LevelName = LevelToLoad.GetAssetName();
	
	if (const auto World = GetWorld())
	{
		const FString CurrentLevelName = World->PersistentLevel->GetOutermost()->GetName();
		// if already loaded, do nothing
		if (CurrentLevelName.Equals(LevelName))
		{
			UE_LOG(LogLoad, Warning, TEXT("Level %s is already loaded"), *CurrentLevelName);
			return true;
		}
	}

	UGameplayStatics::OpenLevel(this, FName(LevelName));
	return true;
}

void UPHGameInstance::SetPresenceForLocalPlayers(const FString& StatusStr, const FVariantData& PresenceData)
{
	const auto Presence = Online::GetPresenceInterface();
	if (Presence.IsValid())
	{
		for (const auto LocalPlayer : LocalPlayers)
		{
			const auto UserId = LocalPlayer->GetPreferredUniqueNetId();

			if (UserId.IsValid())
			{
				FOnlineUserPresenceStatus PresenceStatus;
				PresenceStatus.StatusStr = StatusStr;
				PresenceStatus.Properties.Add(DefaultPresenceKey, PresenceData);

				Presence->SetPresence(*UserId, PresenceStatus);
			}
		}
	}
}

void UPHGameInstance::InternalTravelToSession(const FName& SessionName)
{
	const auto PlayerController = GetFirstLocalPlayerController();
	if (!PlayerController)
	{
		RemoveNetworkFailureHandlers();

		ShowMessageThenGoMain(NSLOCTEXT("DialogHeader", "Error", "Error"),
			NSLOCTEXT("NetworkErrors", "InvalidPlayerController", "Invalid Player Controller"),
			NSLOCTEXT("DialogButtons", "OKAY", "OK"));

		return;
	}

	// travel to session
	const auto OnlineSub = IOnlineSubsystem::Get();
	if (!OnlineSub)
	{
		RemoveNetworkFailureHandlers();

		ShowMessageThenGoMain(NSLOCTEXT("DialogHeader", "Error", "Error"), 
			NSLOCTEXT("NetworkErrors", "OSSMissing", "OSS missing"),
			NSLOCTEXT("DialogButtons", "OKAY", "OK"));

		return;
	}

	FString URL;
	auto Sessions = OnlineSub->GetSessionInterface();
	if (!Sessions.IsValid() || !Sessions->GetResolvedConnectString(SessionName, URL))
	{
		ShowMessageThenGoMain(NSLOCTEXT("DialogHeader", "Error", "Error"), 
			NSLOCTEXT("DialogButtons", "OKAY", "OK"),
			NSLOCTEXT("DialogButtons", "OKAY", "OK"));

		UE_LOG(LogOnlineGame, Warning, TEXT("Failed to travel to session upon joining it"));

		return;
	}

	// Add debug encryption token if desired.
	if (CVarPropHuntTestEncryption->GetInt() != 0)
	{
		// This is just a value for testing/debugging, the server will use the same key regardless of the token value.
		// But the token could be a user ID and/or session ID that would be used to generate a unique key per user and/or session, if desired.
		URL += TEXT("?EncryptionToken=1");
	}

	PlayerController->ClientTravel(URL, TRAVEL_Absolute);
}

UPHGameViewportClient* UPHGameInstance::GetPHGameViewportClient()
{
	return GetWorld() ? Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport()) : nullptr;
}

void UPHGameInstance::IncreaseRoundCount()
{
	RoundCount++;
}

void UPHGameInstance::ResetRoundCount()
{
	RoundCount = GetDefault<UPHGameInstance>()->GetRoundCount();
}

void UPHGameInstance::ScoreTeamWins(int32 WinnerTeam)
{
	TeamWins[WinnerTeam]++;
}

void UPHGameInstance::ResetTeamWins()
{
	TeamWins = GetDefault<UPHGameInstance>()->TeamWins;
}

void UPHGameInstance::SwapTeamWins()
{
	TeamWins.SwapMemory(0, 1);
}

void UPHGameInstance::LoadAssets()
{
	if (UAssetManager* Manager = UAssetManager::GetIfValid())
	{
		const FStreamableDelegate LoadCompletedDelegate = FStreamableDelegate::CreateUObject(this, &UPHGameInstance::OnAssetsLoadCompleted);
		SkillItemsLoadHandle = Manager->LoadPrimaryAssetsWithType(UPHAssetManager::SkillItemType, TArray<FName>(), LoadCompletedDelegate);
		WeaponItemsLoadHandle = Manager->LoadPrimaryAssetsWithType(UPHAssetManager::WeaponItemType, TArray<FName>(), LoadCompletedDelegate);
		TauntAssetsLoadHandle = Manager->LoadPrimaryAssetsWithType(UPHTauntDataAsset::StaticClass()->GetFName(), TArray<FName>(), LoadCompletedDelegate);
	}
}

template<class T>
void FillAssets(const TArray<UObject*>& SourceAssets, TArray<T*>& DestinationAssets)
{
	if (SourceAssets.Num() > 0)
	{
		DestinationAssets.Reset();
		for (UObject* SourceAsset : SourceAssets)
		{
			DestinationAssets.Add(static_cast<T*>(SourceAsset));
		}
	}
}

void UPHGameInstance::OnAssetsLoadCompleted()
{
	TArray<UObject*> LoadedAssets;
	if (SkillItemsLoadHandle.IsValid() && SkillItemsLoadHandle->HasLoadCompleted())
	{
		SkillItemsLoadHandle->GetLoadedAssets(LoadedAssets);
		FillAssets(LoadedAssets, SkillItems);
		SkillItemsLoadHandle.Reset();

		OnSkillItemsLoaded.Broadcast();
	}
	else if (WeaponItemsLoadHandle.IsValid() && WeaponItemsLoadHandle->HasLoadCompleted())
	{
		WeaponItemsLoadHandle->GetLoadedAssets(LoadedAssets);
		FillAssets(LoadedAssets, WeaponItems);
		WeaponItemsLoadHandle.Reset();

		OnWeaponItemsLoaded.Broadcast();
	}
	else if (TauntAssetsLoadHandle.IsValid() && TauntAssetsLoadHandle->HasLoadCompleted())
	{
		TauntAssetsLoadHandle->GetLoadedAssets(LoadedAssets);
		FillAssets(LoadedAssets, TauntDataAssets);
		TauntAssetsLoadHandle.Reset();

		OnTauntAssetsLoaded.Broadcast();
	}
}

UPHItem* UPHGameInstance::GetAbilityItem(FPrimaryAssetId ItemId)
{
	if (ItemId.IsValid())
	{
		auto& AssetManager = UPHAssetManager::Get();
		return AssetManager.ForceLoadItem(ItemId);
	}
	else
	{
		UE_LOG(LogPropHunt, Warning, TEXT("GetAbilityItem: Invalid PrimaryAssetId"));
		return nullptr;
	}
}

UPHTauntDataAsset* UPHGameInstance::GetTauntDataAssetByCountry(FString CountryCode) const
{
	const FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	TArray<FAssetData> AssetData;
	TMultiMap<FName, FString> TagsAndValues;
	TagsAndValues.Add(GET_MEMBER_NAME_CHECKED(UPHTauntDataAsset, Language), CountryCode);
	AssetRegistryModule.Get().GetAssetsByTagValues(TagsAndValues, AssetData);

	if (AssetData.Num() > 0)
	{
		return FAssetData::GetFirstAsset<UPHTauntDataAsset>(AssetData);
	}
	
	return nullptr;
}

void UPHGameInstance::InitPatching(const FString& VariantName)
{
	PatchPlatform = VariantName;

	auto BuildIdReceivedCallback = [this](FResponse_ContentBuild ContentBuild)
	{
		OnPatchVersionResponse(ContentBuild);
	};

	PatchHttpClient->GetContentBuild(BuildIdReceivedCallback);
}

void UPHGameInstance::OnPatchVersionResponse(FResponse_ContentBuild ContentBuild)
{
	// Deployment name to use for this patcher configuration
	const FString DeploymentName = "PropHuntLive";

	const FString ContentBuildId = ContentBuild.ContentBuildID;
	ChunkDownloadList = ContentBuild.ChunkDownloadList;

	if (ContentBuildId.IsEmpty() || ChunkDownloadList.Num() == 0)
	{
		UE_LOG(LogPatch, Display, TEXT("Patch Content is empty"));
		
		OnPatchReady.Broadcast(false);
		return;
	}

	UE_LOG(LogPatch, Display, TEXT("Patch Content ID Response: %s"), *ContentBuildId);

	// ---------------------------------------------------------

	UE_LOG(LogPatch, Display, TEXT("Getting Chunk Downloader"));
	
	TSharedRef<FChunkDownloader> Downloader = FChunkDownloader::GetOrCreate();

	if (!bChunkDownloaderInitialized)
	{
		// Initialize the chunk downloader
		Downloader->Initialize(PatchPlatform, 8);
		bChunkDownloaderInitialized = true;
	}
	
	UE_LOG(LogPatch, Display, TEXT("Loading cached Build ID"));

	// Load the cached build ID
	if (Downloader->LoadCachedBuild(DeploymentName))
	{
		UE_LOG(LogPatch, Display, TEXT("Cached Build Succeeded"));
	}
	else
	{
		UE_LOG(LogPatch, Display, TEXT("Cached Build Failed"));
	}
	
	UE_LOG(LogPatch, Display, TEXT("Updating Build Manifest"));

	// Update the build manifest file
	auto ManifestCompleteCallback = [this](bool bSuccess)
	{
		bDownloadManifestUpToDate = bSuccess;

		UE_LOG(LogPatch, Display, TEXT("Manifest Update %s"), bSuccess ? TEXT("Succeeded") : TEXT("Failed"));

		OnPatchReady.Broadcast(bSuccess);
	};
	
	Downloader->UpdateBuild(DeploymentName, ContentBuildId, ManifestCompleteCallback);
}

bool UPHGameInstance::PatchGame()
{
	if (!bDownloadManifestUpToDate)
	{
		UE_LOG(LogPatch, Display, TEXT("Manifest Update Failed. Cant patch the game"));

		return false;
	}
	
	UE_LOG(LogPatch, Display, TEXT("Starting the game patch process"));

	TSharedRef<FChunkDownloader> Downloader = FChunkDownloader::GetChecked();

	bPatchingGame = true;

	bool bNeedToPatch = false;
	for (int32 ChunkID : ChunkDownloadList)
	{
		const FChunkDownloader::EChunkStatus ChunkStatus = Downloader->GetChunkStatus(ChunkID);
		
		UE_LOG(LogPatch, Display, TEXT("Chunk %i status: %i"), ChunkID, static_cast<int32>(ChunkStatus));

		if (ChunkStatus != FChunkDownloader::EChunkStatus::Mounted)
		{
			bNeedToPatch = true;
		}
	}

	if (!bNeedToPatch)
	{
		UE_LOG(LogPatch, Display, TEXT("All Chunks are already mounted. No new updates"));
		return false;
	}

	auto MountCompleteCallback = [this](bool bSuccess)
	{
		bPatchingGame = false;

		UE_LOG(LogPatch, Display, TEXT("Chunk Mount %s"), bSuccess ? TEXT("complete") : TEXT("failed"));
		
		OnPatchComplete.Broadcast(bSuccess);

		if (bSuccess)
		{
			LoadAssets();
		}
	};

	Downloader->MountChunks(ChunkDownloadList, MountCompleteCallback);

	return true;
}

FPatchStats UPHGameInstance::GetPatchStatus()
{
	const TSharedRef<FChunkDownloader> Downloader = FChunkDownloader::GetChecked();
	const FChunkDownloader::FStats LoadingStats = Downloader->GetLoadingStats();

	FPatchStats RetStats;
	RetStats.FilesDownloaded = LoadingStats.TotalFilesToDownload;
	RetStats.MBDownloaded = static_cast<int32>(LoadingStats.BytesDownloaded / (1024 * 1024));
	RetStats.TotalMBToDownload = static_cast<int32>(LoadingStats.TotalBytesToDownload / (1024 * 1024));
	RetStats.DownloadPercent = static_cast<float>(LoadingStats.BytesDownloaded) / static_cast<float>(LoadingStats.TotalBytesToDownload);
	RetStats.LastError = LoadingStats.LastError;
	return RetStats;
}

bool UPHGameInstance::IsChunkLoaded(int32 ChunkID) const
{
	if (!bDownloadManifestUpToDate)
	{
		return false;
	}

	const TSharedRef<FChunkDownloader> Downloader = FChunkDownloader::GetChecked();
	return Downloader->GetChunkStatus(ChunkID) == FChunkDownloader::EChunkStatus::Mounted;
}

bool UPHGameInstance::DownloadSingleChunk(int32 ChunkID)
{
	if (!bDownloadManifestUpToDate)
	{
		UE_LOG(LogPatch, Display, TEXT("Manifest Update Failed. Can't patch the game"));
		return false;
	}

	if (bPatchingGame)
	{
		UE_LOG(LogPatch, Display, TEXT("Main game patching underway. Ignoring single chunk downloads"));
		return false;
	}

	if (SingleChunkDownloadList.Contains(ChunkID))
	{
		UE_LOG(LogPatch, Display, TEXT("Chunk: %i already downloading"), ChunkID);
		return false;
	}

	bDownloadingSingleChunks = true;

	SingleChunkDownloadList.Add(ChunkID);

	UE_LOG(LogPatch, Display, TEXT("Downloading specific Chunk: %i"), ChunkID);

	const TSharedRef<FChunkDownloader> Downloader = FChunkDownloader::GetChecked();

	auto MountCompleteCallback = [this](bool bSuccess)
	{
		UE_LOG(LogPatch, Display, TEXT("Single Chunk Mount %s"), bSuccess ? TEXT("complete") : TEXT("failed"));
		
		OnSingleChunkPatchComplete.Broadcast(bSuccess);
	};

	Downloader->MountChunk(ChunkID, MountCompleteCallback);

	return true;
}
