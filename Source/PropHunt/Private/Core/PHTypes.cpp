// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHTypes.h"


FTakeHitInfo::FTakeHitInfo()
	: ActualDamage(0.f),
	PawnInstigator(nullptr),
	DamageCauser(nullptr),
	bKilled(false),
	EnsureReplicationByte(0)
{}

void FTakeHitInfo::EnsureReplication()
{
	EnsureReplicationByte++;
}
