// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/PHGameEngine.h"

#include "Core/PHGameInstance.h"

#include "Engine/NetDriver.h"


void UPHGameEngine::HandleNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	// Determine if we need to change the King state based on network failures.

	// Only handle failure at this level for game or pending net drivers.
	FName NetDriverName = NetDriver ? NetDriver->NetDriverName : NAME_None;
	if (NetDriverName == NAME_GameNetDriver || NetDriverName == NAME_PendingNetDriver)
	{
		switch (FailureType)
		{
			case ENetworkFailure::FailureReceived:
			{
				const auto GI = Cast<UPHGameInstance>(GameInstance);
				if (GI && NetDriver->GetNetMode() == NM_Client)
				{
					const FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");

					// NOTE - We pass in false here to not override the message if we are already going to the main menu
					// We're going to make the assumption that someone else has a better message than "Lost connection to host" if
					// this is the case
					GI->ShowMessageThenGotoState(NSLOCTEXT("DialogHeader", "Error", "Error"), FText::FromString(ErrorString), OKButton, FText::GetEmpty(), PHGameInstanceState::MainMenu, false);
				}
				break;
			}
			case ENetworkFailure::PendingConnectionFailure:
			{
				const auto GI = Cast<UPHGameInstance>(GameInstance);
				if (GI && NetDriver->GetNetMode() == NM_Client)
				{
					const FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");

					// NOTE - We pass in false here to not override the message if we are already going to the main menu
					// We're going to make the assumption that someone else has a better message than "Lost connection to host" if
					// this is the case
					GI->ShowMessageThenGotoState(NSLOCTEXT("DialogHeader", "Error", "Error"), FText::FromString(ErrorString), OKButton, FText::GetEmpty(), PHGameInstanceState::MainMenu, false);
				}
				break;
			}
			case ENetworkFailure::ConnectionLost:
			case ENetworkFailure::ConnectionTimeout:
			{
				const auto GI = Cast<UPHGameInstance>(GameInstance);
				if (GI && NetDriver->GetNetMode() == NM_Client)
				{
					const FText ReturnReason = NSLOCTEXT("NetworkErrors", "HostDisconnect", "Lost connection to host.");
					const FText OKButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");

					// NOTE - We pass in false here to not override the message if we are already going to the main menu
					// We're going to make the assumption that someone else has a better message than "Lost connection to host" if
					// this is the case
					GI->ShowMessageThenGotoState(NSLOCTEXT("DialogHeader", "Error", "Error"), ReturnReason, OKButton, FText::GetEmpty(), PHGameInstanceState::MainMenu, false);
				}
				break;
			}
			case ENetworkFailure::NetDriverAlreadyExists:
			case ENetworkFailure::NetDriverCreateFailure:
			case ENetworkFailure::OutdatedClient:
			case ENetworkFailure::OutdatedServer:
			default:
				break;
		}
	}

	// standard failure handling.
	Super::HandleNetworkFailure(World, NetDriver, FailureType, ErrorString);
}
