// Copyright Art-Sun Games. All Rights Reserved.


#include "Core/GameModes/PHGameModeBase.h"

#include "Core/PHPlayerStart.h"
#include "Core/PHGameInstance.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/Online/PHGameSession.h"

#include "EngineUtils.h"
#include "TimerManager.h"
#include "Engine/PlayerStartPIE.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/PlayerStart.h"
#include "GameFramework/WorldSettings.h"
#include "GameFramework/SpectatorPawn.h"

APHGameModeBase::APHGameModeBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PlayerControllerClass = APHPlayerController::StaticClass();
	PlayerStateClass = APHPlayerState::StaticClass();
	GameStateClass = APHGameState::StaticClass();
	GameSessionClass = APHGameSession::StaticClass();
	DefaultPawnClass = ASpectatorPawn::StaticClass();

	bUseSeamlessTravel = true;
	bStartPlayersAsSpectators = true;
}

void APHGameModeBase::InitGameState()
{
	Super::InitGameState();

	const auto MyGameInstance = GetGameInstance<UPHGameInstance>();
	const auto MyGameState = GetGameState<APHGameState>();
	if (MyGameState && MyGameInstance)
	{
		MyGameState->ServerName = MyGameInstance->GetServerName();
		MyGameState->MapName = MyGameInstance->GetMapName();
		MyGameState->MaxPlayers = MyGameInstance->GetMaxPlayers();
		MyGameState->ServerLang = MyGameInstance->GetServerLang();
	}
}

void APHGameModeBase::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	GetWorldTimerManager().SetTimer(TimerHandle_DefaultTimer, this, &APHGameModeBase::DefaultTimer, GetWorldSettings()->GetEffectiveTimeDilation(), true);
}

bool APHGameModeBase::ShouldSpawnAtStartSpot(AController* Player)
{
	// Always pick new random spawn
	return false;
}

UClass* APHGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if (const auto PlayerState = InController->GetPlayerState<APHPlayerState>())
	{
		if (PlayerState->IsHunter())
		{
			return HunterPawnClass;
		}
		if (PlayerState->IsProp())
		{
			return PropPawnClass;
		}
	}

	return Super::GetDefaultPawnClassForController_Implementation(InController);
}

AActor* APHGameModeBase::ChoosePlayerStart_Implementation(AController* Player)
{
	TArray<APlayerStart*> PreferredSpawns;
	TArray<APlayerStart*> FallbackSpawns;

	APlayerStart* BestStart = nullptr;
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* const TestStart = *It;
		if (TestStart->IsA<APlayerStartPIE>())
		{
			// Always prefer the first "Play from Here" PlayerStart, if we find one while in PIE mode
			BestStart = TestStart;
			break;
		}
		
		if (IsSpawnpointAllowed(TestStart, Player))
		{
			if (IsSpawnpointPreferred(TestStart, Player))
			{
				PreferredSpawns.Add(TestStart);
			}
			else
			{
				FallbackSpawns.Add(TestStart);
			}
		}
	}

	if (!BestStart)
	{
		if (PreferredSpawns.Num() > 0)
		{
			BestStart = PreferredSpawns[FMath::RandHelper(PreferredSpawns.Num())];
		}
		else if (FallbackSpawns.Num() > 0)
		{
			BestStart = FallbackSpawns[FMath::RandHelper(FallbackSpawns.Num())];
		}
	}

	return BestStart ? BestStart : Super::ChoosePlayerStart_Implementation(Player);
}

void APHGameModeBase::SetMatchState(FName NewState)
{
	Super::SetMatchState(NewState);

	if (const auto MyGameState = GetGameState<APHGameState>())
	{
		MyGameState->OnMatchStateSet();
	}
}

void APHGameModeBase::RequestFinishAndExitToMainMenu()
{
	const FText RemoteReturnReason = NSLOCTEXT("NetworkErrors", "HostHasLeft", "Host has left the game.");

	APHPlayerController* LocalPrimaryController = nullptr;
	for (auto It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		APlayerController* Controller = It->Get();
		if (Controller && Controller->IsPrimaryPlayer())
		{
			if (!Controller->IsLocalController())
			{
				Controller->ClientReturnToMainMenuWithTextReason(RemoteReturnReason);
			}
			else
			{
				LocalPrimaryController = Cast<APHPlayerController>(Controller);
			}
		}
	}

	// GameInstance should be calling this from an EndState.  So call the PC function that performs cleanup, not the one that sets GI state.
	if (LocalPrimaryController)
	{
		LocalPrimaryController->HandleReturnToMainMenu();
	}
}

bool APHGameModeBase::IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const
{
	if (Player)
	{
		const auto PlayerState = Player->GetPlayerState<APHPlayerState>();
		const auto TeamStart = Cast<APHPlayerStart>(SpawnPoint);
		if (PlayerState && TeamStart)
		{
			switch (TeamStart->Team)
			{
				case ETeam::TEAM_Hunters:
					return PlayerState->IsHunter();
				case ETeam::TEAM_Props:
					return PlayerState->IsProp();
				case ETeam::TEAM_None:
				default:
					return PlayerState->GetTeamNumber() == ETeamType::None || PlayerState->GetTeamNumber() == ETeamType::Spectator;
			}
		}
	}

	return false;
}

bool APHGameModeBase::IsSpawnpointPreferred(APlayerStart* SpawnPoint, AController* Player) const
{
	if (const auto MyPawn = HunterPawnClass->GetDefaultObject<ACharacter>())
	{
		const FVector SpawnLocation = SpawnPoint->GetActorLocation();
		for (ACharacter* OtherPawn : TActorRange<ACharacter>(GetWorld()))
		{
			if (OtherPawn != MyPawn)
			{
				const float CombinedHeight = (MyPawn->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + OtherPawn->GetCapsuleComponent()->GetScaledCapsuleHalfHeight()) * 2.f;
				const float CombinedRadius = (MyPawn->GetCapsuleComponent()->GetScaledCapsuleRadius() + OtherPawn->GetCapsuleComponent()->GetScaledCapsuleRadius());
				const FVector OtherLocation = OtherPawn->GetActorLocation();

				// check if player start overlaps this pawn
				if (FMath::Abs(SpawnLocation.Z - OtherLocation.Z) < CombinedHeight && (SpawnLocation - OtherLocation).Size2D() < CombinedRadius)
				{
					return false;
				}
			}
		}
	}
	else
	{
		return false;
	}

	return true;
}
