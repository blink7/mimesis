// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/GameModes/GameMode_Escape.h"

#include "Core/PHGameInstance.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/PHHUD.h"
#include "Enviroment/PHPowerPanel.h"

#include "EngineUtils.h"
#include "Engine/LevelStreaming.h"
#include "Kismet/GameplayStatics.h"

namespace MatchState
{
	const FName InProgressEscaping = FName(TEXT("InProgressEscaping"));
}

void AGameMode_Escape::InitGameState()
{
	Super::InitGameState();

	if (const auto MyGameState = Cast<APHGameState>(GameState))
	{
		MyGameState->NeedPortalEnergy = NeedPortalEnergy;
	}
}

void AGameMode_Escape::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();

	TArray<FTransform> SpawnTransforms;
	for (TActorIterator<AActor> PowerPanelSpawnIterator(GetWorld(), PowerPanelSpawnClass); PowerPanelSpawnIterator; ++PowerPanelSpawnIterator)
	{
		SpawnTransforms.Add(PowerPanelSpawnIterator->GetActorTransform());
	}

	for (int32 i = 0; i < MaxPowerPanels && SpawnTransforms.Num() > 0; i++)
	{
		const int32 RandIndex = FMath::RandHelper(SpawnTransforms.Num());
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		GetWorld()->SpawnActor<AActor>(PowerPanelClass, SpawnTransforms[RandIndex], SpawnInfo);

		SpawnTransforms.RemoveAt(RandIndex);
	}
}

void AGameMode_Escape::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	if (IsInEscapingState())
	{
		HandleEscapingHasStarted();
	}
}

bool AGameMode_Escape::IsMatchInProgress() const
{
	return Super::IsMatchInProgress() || IsInEscapingState();
}

bool AGameMode_Escape::IsInHuntingState() const
{
	return Super::IsInHuntingState() || IsInEscapingState();
}

void AGameMode_Escape::HandleHuntingHasStarted()
{
	Super::HandleHuntingHasStarted();

	for (FConstPlayerControllerIterator PCIterator = GetWorld()->GetPlayerControllerIterator(); PCIterator; ++PCIterator)
	{
		if (const auto PC = Cast<APHPlayerController>(PCIterator->Get()))
		{
			PC->ClientShowPortalStatus();
		}
	}

	for (TActorIterator<APHPowerPanel> PowerPanelIterator(GetWorld(), APHPowerPanel::StaticClass()); PowerPanelIterator; ++PowerPanelIterator)
	{
		PowerPanelIterator->OnEnable();
	}
}

void AGameMode_Escape::DetermineMatchWinner()
{
	if (WinnerTeam == ETeamType::None)
	{
		WinnerTeam = ETeamType::Hunter;
	}

	if (const auto GI = Cast<UPHGameInstance>(GetGameInstance()))
	{
		GI->ScoreTeamWins(WinnerTeam);

		if (const auto MyGameState = GetGameState<APHGameState>())
		{
			MyGameState->SetTeamWins(GI->GetTeamWins());
		}
	}
}

void AGameMode_Escape::HandleEscapingHasStarted()
{
	DisablePowerPanels();

	for (const ULevelStreaming* LevelStreamingObject : GetWorld()->GetStreamingLevels())
	{
		const FString& LevelPackageName = LevelStreamingObject->GetWorldAssetPackageName();
		if (LevelPackageName.Contains("IndoorLights_On"))
		{
			LightOnLevelName = LevelPackageName;
		}
		else if (LevelPackageName.Contains("IndoorLights_Off"))
		{
			LightOffLevelName = LevelPackageName;
		}
	}

	if (!LightOnLevelName.IsEmpty() && !LightOffLevelName.IsEmpty())
	{
		const FLatentActionInfo LatentInfo(0, 0, TEXT("LightOnLevelUnloaded"), this);
		UGameplayStatics::UnloadStreamLevel(this, FName(LightOnLevelName), LatentInfo, false);
	}
}

void AGameMode_Escape::StartEscaping()
{
	if (IsInEscapingState())
	{
		return;
	}

	SetMatchState(MatchState::InProgressEscaping);
}

void AGameMode_Escape::EnergyReceived(AController* InInstigator, float EnergyAmount)
{
	if (const auto MyGameState = GetGameState<APHGameState>())
	{
		const float CurrentEnergy = MyGameState->GetTotalEnergy();
		const float NewEnergy = CurrentEnergy + EnergyAmount;
		MyGameState->SetTotalEnergy(NewEnergy);

		if (FMath::IsNearlyEqual(NewEnergy, NeedPortalEnergy, 0.01f) || NewEnergy > NeedPortalEnergy)
		{
			StartEscaping();
		}
	}
}

void AGameMode_Escape::EnergyGiven(AController* InInstigator, float EnergyAmount)
{
	if (const auto MyGameState = GetGameState<APHGameState>())
	{
		const float CurrentEnergy = MyGameState->GetPortalEnergy();
		const float NewEnergy = CurrentEnergy + EnergyAmount;
		MyGameState->SetPortalEnergy(NewEnergy);
	}
}

void AGameMode_Escape::PortalCharged()
{
	WinnerTeam = ETeamType::Prop;

	FinishMatch();
	NotifyMatchFinished();
}

void AGameMode_Escape::LightOnLevelUnloaded()
{
	const FLatentActionInfo LatentInfo(0, 1, TEXT("LightOffLevelLoaded"), this);
	UGameplayStatics::LoadStreamLevel(this, FName(LightOffLevelName), true, true, LatentInfo);
}

void AGameMode_Escape::LightOffLevelLoaded()
{
	SpawnPortal();

	// notify players
	for (FConstPlayerControllerIterator PCIterator = GetWorld()->GetPlayerControllerIterator(); PCIterator; ++PCIterator)
	{
		if (const auto PC = Cast<APHPlayerController>(*PCIterator))
		{
			const auto PS = PC->GetPlayerState<APHPlayerState>();
			if (PS->IsProp())
			{
				PC->ClientShowInfoMessage(EInfoMessageType::INFO_Portal, 10.f);
			}
		}
	}
}

void AGameMode_Escape::DisablePowerPanels()
{
	for (TActorIterator<APHPowerPanel> PowerPanelIterator(GetWorld(), APHPowerPanel::StaticClass()); PowerPanelIterator; ++PowerPanelIterator)
	{
		PowerPanelIterator->OnDisable();
	}
}

void AGameMode_Escape::SpawnPortal()
{
	TArray<FTransform> SpawnTransforms;
	for (TActorIterator<AActor> PortalSpawnIterator(GetWorld(), PortalSpawnClass); PortalSpawnIterator; ++PortalSpawnIterator)
	{
		SpawnTransforms.Add(PortalSpawnIterator->GetActorTransform());
	}

	if (SpawnTransforms.Num() > 0)
	{
		const int32 RandIndex = FMath::RandHelper(SpawnTransforms.Num());
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		GetWorld()->SpawnActor<AActor>(PortalClass, SpawnTransforms[RandIndex], SpawnInfo);
	}
}
