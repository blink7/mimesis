// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/GameModes/GameMode_Menu.h"

#include "Core/Online/PHPlayerState.h"
#include "Core/Online/PHGameSession.h"
#include "Core/PHPlayerController_Menu.h"


AGameMode_Menu::AGameMode_Menu(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PlayerControllerClass = APHPlayerController_Menu::StaticClass();
	GameSessionClass = APHGameSession::StaticClass();
	PlayerStateClass = APHPlayerState::StaticClass();
}

void AGameMode_Menu::RestartPlayer(AController* NewPlayer)
{
	// skip it, menu doesn't require player start or pawn
}
