// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/GameModes/GameMode_Hunting.h"

#include "Core/PHTypes.h"
#include "Core/PHPlayerStart.h"
#include "Core/PHGameInstance.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/PHHUD.h"

#include "EngineUtils.h"
#include "TimerManager.h"

namespace MatchState
{
	const FName InProgressHunting = FName(TEXT("InProgressHunting"));
}

AGameMode_Hunting::AGameMode_Hunting() : 
	WinnerTeam(ETeamType::None)
{
	bDelayedStart = true;

	NumTeams = 2;
	bWithSpectator = false;

	MinRespawnDelay = 5.f;
}

void AGameMode_Hunting::InitGameState()
{
	Super::InitGameState();

	const auto GI = Cast<UPHGameInstance>(GetGameInstance());
	const auto MyGameState = Cast<APHGameState>(GameState);
	if (MyGameState && GI)
	{
		if (GI->GetRoundCount() == NumRounds)
		{
			GI->ResetRoundCount();
			GI->ResetTeamWins();
		}
		GI->IncreaseRoundCount();

		MyGameState->NumTeams = NumTeams;
		MyGameState->SetRoundCount(GI->GetRoundCount());
		MyGameState->SetTeamWins(GI->GetTeamWins());
	}
}

void AGameMode_Hunting::RestartPlayer(AController* NewPlayer)
{
	// Place player on a team before Super (VoIP team based init, find playerstart, etc)
	const auto NewPlayerState = CastChecked<APHPlayerState>(NewPlayer->PlayerState);
	if (NewPlayerState && NewPlayerState->GetTeamNumber() == ETeamType::None)
	{
		const int32 TeamNum = ChooseTeam(NewPlayerState);
		NewPlayerState->SetTeamNumber(TeamNum);
	}

	Super::RestartPlayer(NewPlayer);
}

void AGameMode_Hunting::PostLogin(APlayerController* NewPlayer)
{
	if (GetMatchState() != MatchState::EnteringMap && GetMatchState() != MatchState::WaitingToStart)
	{
		NewPlayer->PlayerState->SetIsSpectator(true);
	}
	
	Super::PostLogin(NewPlayer);

	if (const auto NewPC = Cast<APHPlayerController>(NewPlayer))
	{
		if (IsMatchInProgress())
		{
			NewPC->ClientGameStarted();
		}

		if (GetMatchState() == MatchState::InProgress)
		{
			if (const auto MyGameState = GetGameState<APHGameState>())
			{
				NewPC->ClientShowInfoMessage(EInfoMessageType::INFO_Hiding, MyGameState->GetRemainingTime());
			}
		}

		NewPC->ClientShowTeamChoice(true);

#if WITH_EDITOR
		if (GetWorld()->IsPlayInEditor() && GetMatchState() == MatchState::InProgress)
		{
			NewPC->ClientShowTeamChoice(false);
			RestartPlayer(NewPlayer);
		}
#endif
	}
}

void AGameMode_Hunting::HandleSeamlessTravelPlayer(AController*& C)
{
	C->PlayerState->SetIsSpectator(false);
	
	Super::HandleSeamlessTravelPlayer(C);

	const auto MyGameState = GetGameState<APHGameState>();
	if (MyGameState && MyGameState->GetRoundCount() == 1)
	{
		// allow players to switch team at begin of each game
		if (const auto PlayerController = Cast<APHPlayerController>(C))
		{
			PlayerController->ClientShowTeamChoice(true);
		}
	}
}

bool AGameMode_Hunting::IsMatchInProgress() const
{
	return Super::IsMatchInProgress() || IsInHuntingState();
}

void AGameMode_Hunting::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchIsWaitingToStart();

	if (bDelayedStart)
	{
		// start warmup if needed
		const auto MyGameState = GetGameState<APHGameState>();
		if (MyGameState && MyGameState->GetRemainingTime() == 0)
		{
			const bool bWantsMatchWarmup = MyGameState->GetRoundCount() == 1 && !GetWorld()->IsPlayInEditor();
			if (bWantsMatchWarmup && WarmupTime > 0)
			{
				MyGameState->SetRemainingTime(WarmupTime);
			}
			else
			{
				MyGameState->SetRemainingTime(1);
			}
		}
	}
}

void AGameMode_Hunting::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	if (const auto MyGameState = GetGameState<APHGameState>())
	{
		MyGameState->SetRemainingTime(HidingTime);

		// notify players
		for (FConstPlayerControllerIterator PCIterator = GetWorld()->GetPlayerControllerIterator(); PCIterator; ++PCIterator)
		{
			if (const auto PC = Cast<APHPlayerController>(*PCIterator))
			{
				const auto PS = PC->GetPlayerState<APHPlayerState>();
				if (PS && !PS->IsHunter() || GetWorld()->IsPlayInEditor())
				{
					PC->ClientGameStarted();
				}

				PC->ClientShowTeamChoice(false);
				PC->ClientShowInfoMessage(EInfoMessageType::INFO_Hiding, MyGameState->GetRemainingTime());
			}
		}
	}
}

void AGameMode_Hunting::HandleHuntingHasStarted()
{
	if (const auto MyGameState = GetGameState<APHGameState>())
	{
		MyGameState->SetRemainingTime(RoundTime);
	}

	for (FConstPlayerControllerIterator PCIterator = GetWorld()->GetPlayerControllerIterator(); PCIterator; ++PCIterator)
	{
		APlayerController* PC = PCIterator->Get();
		const auto MyPC = PC ? Cast<APHPlayerController>(PC) : nullptr;
		const auto MyPS = PC ? PC->GetPlayerState<APHPlayerState>() : nullptr;
		if (MyPC && MyPS && !MyPS->IsSpectator())
		{
			MyPC->ClientBeginTransportation();

			if (MyPS->GetTeamNumber() == ETeamType::Prop)
			{
				MyPS->ApplyHuntingEffect();
			}
		}
	}

	FTimerHandle TimerHandle_Unused;
	GetWorldTimerManager().SetTimer(TimerHandle_Unused, this, &AGameMode_Hunting::ProcessTransportation, 3.f, false);

	OnHuntingStarted.Broadcast();
}

void AGameMode_Hunting::DefaultTimer()
{
#if WITH_EDITOR
	// Don't update timers for Play In Editor mode, it's not real match
	if (GetWorld()->IsPlayInEditor())
	{
		// Start match if necessary.
		if (GetMatchState() == MatchState::WaitingToStart)
		{
			StartMatch();
		}
		return;
	}
#endif

	if (GetMatchState() == MatchState::WaitingToStart)
	{
		if (NumTravellingPlayers > 0)
		{
			// Wait until all players will join
			return;
		}
	}
	
	const auto MyGameState = GetGameState<APHGameState>();
	if (MyGameState && MyGameState->GetRemainingTime() > 0 && !MyGameState->bTimerPaused)
	{
		MyGameState->DecrementRemainingTime();

		if (MyGameState->GetRemainingTime() <= 0)
		{
			if (GetMatchState() == MatchState::WaitingPostMatch)
			{
				SwapTeams();

				if (MyGameState->GetRoundCount() != NumRounds)
				{
					RestartGame();
				}
				else
				{
					SwitchLevel();
				}
			}
			else if (GetMatchState() == MatchState::InProgress)
			{
				StartHunting();
			}
			else if (IsInHuntingState())
			{
				FinishMatch();
				NotifyMatchFinished();
			}
			else if (GetMatchState() == MatchState::WaitingToStart)
			{
				StartMatch();
			}
		}
	}
}

void AGameMode_Hunting::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	if (IsInHuntingState())
	{
		HandleHuntingHasStarted();
	}
}

void AGameMode_Hunting::StartHunting()
{
	if (IsInHuntingState())
	{
		return;
	}

	SetMatchState(MatchState::InProgressHunting);
}

void AGameMode_Hunting::ProcessTransportation()
{
	for (FConstPlayerControllerIterator PCIterator = GetWorld()->GetPlayerControllerIterator(); PCIterator; ++PCIterator)
	{
		const auto MyPC = Cast<APHPlayerController>(PCIterator->Get());
		const auto MyPS = MyPC ? MyPC->GetPlayerState<APHPlayerState>() : nullptr;
		if (MyPC && MyPS && !MyPS->IsSpectator() && MyPS->IsHunter())
		{
			TeleportHunter(MyPC);
			MyPC->ClientGameStarted();
			MyPS->ApplyHuntingEffect();
		}
	}
}

void AGameMode_Hunting::TeleportHunter(class APlayerController* PlayerController)
{
	if (APawn* Pawn = PlayerController->GetPawn())
	{
		const AActor* PlayerStart = ChoosePlayerStart(PlayerController);
		Pawn->TeleportTo(PlayerStart->GetActorLocation(), PlayerStart->GetActorRotation());
	}
}

int32 AGameMode_Hunting::ChooseTeam(APHPlayerState* ForPlayerState) const
{
	TArray<int32> TeamBalance;
	TeamBalance.AddZeroed(bWithSpectator ? (NumTeams - 1) : NumTeams);

	// get current team balance
	for (APlayerState* PlayerState : GameState->PlayerArray)
	{
		const auto* const TestPlayerState = Cast<APHPlayerState>(PlayerState);
		if (TestPlayerState && TestPlayerState != ForPlayerState && TeamBalance.IsValidIndex(TestPlayerState->GetTeamNumber()))
		{
			TeamBalance[TestPlayerState->GetTeamNumber()]++;
		}
	}

	// find least populated one
	int32 BestTeamScore = TeamBalance[0];
	for (int32 i = 1; i < TeamBalance.Num(); i++)
	{
		if (BestTeamScore > TeamBalance[i])
		{
			BestTeamScore = TeamBalance[i];
		}
	}

	// there could be more than one...
	TArray<int32> BestTeams;
	for (int32 i = 0; i < TeamBalance.Num(); i++)
	{
		if (TeamBalance[i] == BestTeamScore)
		{
			BestTeams.Add(i);
		}
	}

	// get random from best list
	const int32 RandomBestTeam = BestTeams[FMath::RandHelper(BestTeams.Num())];
	return RandomBestTeam;
}

void AGameMode_Hunting::SwapTeams()
{
	if (const auto GI = GetGameInstance<UPHGameInstance>())
	{
		GI->SwapTeamWins();
	}

	for (APlayerState* PlayerState : GameState->PlayerArray)
	{
		if (const auto MyPlayerState = Cast<APHPlayerState>(PlayerState))
		{
			MyPlayerState->SwapTeamNumber();
		}
	}
}

bool AGameMode_Hunting::IsSpawnpointAllowed(APlayerStart* SpawnPoint, AController* Player) const
{
	if (const auto TestStart = Cast<APHPlayerStart>(SpawnPoint))
	{
		if (!IsMatchInProgress())
		{
			return TestStart->Team == ETeam::TEAM_None;
		}

		if (const auto PlayerState = Cast<APHPlayerState>(Player->PlayerState))
		{
			switch (TestStart->Team)
			{
				case ETeam::TEAM_Props:
					return PlayerState->IsProp();
				case ETeam::TEAM_Hunters:
					return PlayerState->IsHunter() && (GetMatchState() == MatchState::InProgress && TestStart->bHuntingPreparation || IsInHuntingState() && !TestStart->bHuntingPreparation);
				case ETeam::TEAM_None:
					return PlayerState->GetTeamNumber() == ETeamType::None || PlayerState->GetTeamNumber() == ETeamType::Spectator;
			}
		}
	}

	return false;
}

void AGameMode_Hunting::DetermineMatchWinner()
{
	WinnerTeam = IsTeamAlive(ETeamType::Prop) ? ETeamType::Prop : ETeamType::Hunter;

	if (const auto GI = Cast<UPHGameInstance>(GetGameInstance()))
	{
		GI->ScoreTeamWins(WinnerTeam);

		if (const auto MyGameState = GetGameState<APHGameState>())
		{
			MyGameState->SetTeamWins(GI->GetTeamWins());
		}
	}
}

bool AGameMode_Hunting::IsWinner(APlayerState* PlayerState) const
{
	const auto MyPlayerState = Cast<APHPlayerState>(PlayerState);
	return MyPlayerState && !MyPlayerState->IsQuitter() && MyPlayerState->GetTeamNumber() == WinnerTeam;
}

bool AGameMode_Hunting::IsTeamAlive(int32 TeamNumber) const
{
	if (const auto MyGameState = GetGameState<APHGameState>())
	{
		return MyGameState->GetLivePlayerCount(TeamNumber) > 0;
	}

	return false;
}

void AGameMode_Hunting::SwitchLevel()
{
	for (APlayerState* PlayerState : GameState->PlayerArray)
	{
		if (const auto MyPlayerState = Cast<APHPlayerState>(PlayerState))
		{
			MyPlayerState->ResetStats();
		}
	}

	if (const auto GI = GetGameInstance<UPHGameInstance>())
	{
		GI->UpdateGame("Office_01"); // gag for now until there is only one working map
	}
}

void AGameMode_Hunting::NotifyMatchFinished()
{
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto PlayerController = Cast<APHPlayerController>(*It);
		const auto PlayerState = Cast<APHPlayerState>((*It)->PlayerState);
		const auto MyGameState = GetGameState<APHGameState>();

		if (PlayerController && PlayerState && MyGameState)
		{
			const bool bIsWinner = IsWinner(PlayerState);

			PlayerController->ClientSendRoundEndEvent(bIsWinner, MyGameState->ElapsedTime);
			PlayerState->ScoreWin(bIsWinner);

			if (MyGameState->GetRoundCount() == NumRounds)
			{
				PlayerController->ClientShowMatchResultWithDelay(TimeBetweenMatches / 3.f);
			}
		}
	}
}

void AGameMode_Hunting::FinishMatch()
{
	if (IsMatchInProgress())
	{
		EndMatch();
		DetermineMatchWinner();

		// notify players
		for (FConstPlayerControllerIterator PCIterator = GetWorld()->GetPlayerControllerIterator(); PCIterator; ++PCIterator)
		{
			(*PCIterator)->GameHasEnded(nullptr, IsWinner((*PCIterator)->PlayerState));
		}

		// lock all pawns
		// pawns are not marked as keep for seamless travel, so we will create new pawns on the next match rather than
		// turning these back on.
		for (TActorIterator<APawn> PawnIterator(GetWorld()); PawnIterator; ++PawnIterator)
		{
			(*PawnIterator)->TurnOff();
		}

		if (const auto MyGameState = GetGameState<APHGameState>())
		{
			// set up to restart the match
			MyGameState->SetRemainingTime(TimeBetweenMatches);
		}
	}
}

void AGameMode_Hunting::RequestFinishAndExitToMainMenu()
{
	FinishMatch();

	Super::RequestFinishAndExitToMainMenu();
}

void AGameMode_Hunting::Killed(AController* Killer, AController* KilledPlayer, APawn* KilledPawn, UGameplayEffectUIData* UIData)
{
	const auto KillerPlayerState = Killer ? Cast<APHPlayerState>(Killer->PlayerState) : nullptr;
	const auto VictimPlayerState = KilledPlayer ? Cast<APHPlayerState>(KilledPlayer->PlayerState) : nullptr;

	if (KillerPlayerState && KillerPlayerState != VictimPlayerState)
	{
		KillerPlayerState->IncrementKills();
		KillerPlayerState->NotifyKill(KillerPlayerState, VictimPlayerState, UIData);
	}

	if (VictimPlayerState)
	{
		VictimPlayerState->IncrementDeath();
		VictimPlayerState->BroadcastDeath(KillerPlayerState, VictimPlayerState, UIData);

		if (!IsTeamAlive(VictimPlayerState->GetTeamNumber()))
		{
			if (const auto MyGameState = GetGameState<APHGameState>())
			{
				if (MyGameState->GetRemainingTime() > MinRespawnDelay)
				{
					MyGameState->SetRemainingTime(MinRespawnDelay);
				}
			}
		}
	}
}
