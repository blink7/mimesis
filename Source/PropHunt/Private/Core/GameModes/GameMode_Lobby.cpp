// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/GameModes/GameMode_Lobby.h"

#include "Core/PHTypes.h"
#include "Core/PHGameInstance.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/PHHUD.h"
#include "Character/PHCharacterBase.h"

#include "EngineUtils.h"
#include "Engine/World.h"

AGameMode_Lobby::AGameMode_Lobby(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NumTeams = 2;
}

void AGameMode_Lobby::InitGameState()
{
	Super::InitGameState();

	const auto GI = Cast<UPHGameInstance>(GetGameInstance());
	const auto MyGameState = Cast<APHGameState>(GameState);
	if (MyGameState && GI)
	{
		MyGameState->NumTeams = NumTeams;
		MyGameState->SetRemainingTime(WarmupTime);
	}
}

void AGameMode_Lobby::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	const auto NewPC = Cast<APHPlayerController>(NewPlayer);
	if (NewPC)
	{
		NewPC->bInLobby = true;
		NewPC->ClientGameStarted();
		NewPC->ClientSetSpectatorCamera(NewPC->GetSpawnLocation(), NewPC->GetControlRotation());
		NewPC->ClientShowTeamChoice(true);
	}

	UpdateReadyStatus();
}

bool AGameMode_Lobby::PlayerCanRestart_Implementation(APlayerController* Player)
{
	const auto MyPlayerState = Cast<APHPlayerState>(Player->PlayerState);
	if (MyPlayerState && MyPlayerState->GetTeamNumber() != ETeamType::None)
	{
		return Super::PlayerCanRestart_Implementation(Player);
	}

	return false;
}

void AGameMode_Lobby::RespawnPlayer(class APlayerController* PlayerController)
{
	const auto PC = Cast<APHPlayerController>(PlayerController);
	if (PC && PC->IsInState(NAME_Spectating))
	{
		PC->SetInfiniteAmmo(true);
		PC->ClientShowInfoMessage(EInfoMessageType::INFO_UpdateStatus, 0.f);
	}

	if (const auto Character = Cast<APHCharacterBase>(PlayerController->GetPawn()))
	{
		Character->RemoveAbilities();
	}

	if (const auto Pawn = PlayerController->GetPawn())
	{
		Pawn->SetReplicatingMovement(false);
		Pawn->TearOff();
		Pawn->DetachFromControllerPendingDestroy();
		Pawn->Destroy();
	}

	RestartPlayer(PlayerController);
}

void AGameMode_Lobby::UpdateReadyStatus()
{
	const auto MyGameState = GetGameState<APHGameState>();
	if (MyGameState)
	{
		ReadyPlayers = MyGameState->GetReadyPlayers();
	}

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto PC = Cast<APHPlayerController>(*It);
		if (PC)
		{
			PC->ClientUpdateReadyStatus(ReadyPlayers, GetNumPlayers());
		}
	}
}

void AGameMode_Lobby::DefaultTimer()
{
	if (NumPlayers <= 1) return;

	const auto MyGameState = GetGameState<APHGameState>();
	if (MyGameState && MyGameState->GetRemainingTime() > 0)
	{
		MyGameState->DecrementRemainingTime();

		if (MyGameState->GetRemainingTime() <= 0)
		{
			if (GetMatchState() == MatchState::InProgress)
			{
				EndMatch();

				// notify players
				for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
				{
					(*It)->GameHasEnded(nullptr, false);
				}

				// lock all pawns
				// pawns are not marked as keep for seamless travel, so we will create new pawns on the next match rather than
				// turning these back on.
				for (TActorIterator<APawn> It(GetWorld()); It; ++It)
				{
					(*It)->TurnOff();
				}

				const auto GI = GetGameInstance<UPHGameInstance>();
				if (GI)
				{
					GI->UpdateGame(GI->GetHostMapName());
				}
			}
		}
		else if (MyGameState->GetRemainingTime() > 5 && ReadyPlayers == NumPlayers)
		{
			MyGameState->SetRemainingTime(5);
		}
	}
}
