// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHPlayerController.h"

#include "Core/PHLocalPlayer.h"
#include "Core/PHPlayerStart.h"
#include "Core/PHGameInstance.h"
#include "Core/PHPersistentUser.h"
#include "Core/PHGameViewportClient.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/PHHUD.h"
#include "Core/UI/Widgets/Dialog.h"
#include "Core/GameModes/PHGameModeBase.h"
#include "Core/GameModes/GameMode_Lobby.h"
#include "Character/Prop/PropCharacter.h"
#include "Character/Hunter/HunterCharacter.h"
#include "Character/PHPlayerCameraManager.h"
#include "Character/PlayerIndicatorComponent.h"

#include "EngineUtils.h"
#include "TimerManager.h"
#include "OnlineSubsystem.h"
#include "Camera/CameraActor.h"
#include "Engine/World.h"
#include "Misc/PackageName.h"
#include "Net/UnrealNetwork.h"
#include "Interfaces/OnlineSessionInterface.h"

namespace EMessageType
{
	const FName Server = FName(TEXT("Server"));
	const FName All = FName(TEXT("All"));
	const FName Team = FName(TEXT("Team"));
}

APHPlayerController::APHPlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PlayerCameraManagerClass = APHPlayerCameraManager::StaticClass();

	bGameEndedFrame = false;
	LastDeathLocation = FVector::ZeroVector;

	bHasSentStartEvents = false;
}

void APHPlayerController::FailedToSpawnPawn()
{
	if (IsInState(NAME_Inactive))
	{
		BeginInactiveState();
	}

	Super::FailedToSpawnPawn();
}

void APHPlayerController::PawnPendingDestroy(APawn* InPawn)
{
	if (InPawn->IsA(AHunterCharacter::StaticClass()))
	{
		LastDeathLocation = InPawn->GetActorLocation();
		FVector CameraLocation = LastDeathLocation + FVector(0.f, 0.f, 300.f);
		FRotator CameraRotation(-90.f, 0.f, 0.f);
		FindDeathCameraSpot(CameraLocation, CameraRotation);

		Super::PawnPendingDestroy(InPawn);

		ClientSetSpectatorCamera(CameraLocation, CameraRotation);
	}
	else
	{
		Super::PawnPendingDestroy(InPawn);
	}
}

void APHPlayerController::InitPlayerState()
{
	Super::InitPlayerState();

	bInLobby = false;
	bInfiniteAmmo = false;
	bInfiniteClip = false;
}

void APHPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (const auto CameraManager = Cast<APHPlayerCameraManager>(PlayerCameraManager))
	{
		if (InPawn)
		{
			if (InPawn->IsA(APropCharacter::StaticClass()))
			{
				CameraManager->UpdateCameraSettings(ECameraMode::NAME_ThirdPerson);
			}
			else if (InPawn->IsA(AHunterCharacter::StaticClass()))
			{
				CameraManager->UpdateCameraSettings(ECameraMode::NAME_FirstPerson);
			}
		}
		else
		{
			CameraManager->UpdateCameraSettings(NAME_Default);
		}
	}
}

void APHPlayerController::InitInputSystem()
{
	Super::InitInputSystem();

	if (UPHPersistentUser* PersistentUser = GetPersistentUser())
	{
		PersistentUser->TellInputAboutKeybindings();
	}
}

void APHPlayerController::PreClientTravel(const FString& PendingURL, ETravelType Travel, bool bIsSeamlessTravel)
{
	if (IsLocalController())
	{
		if (const auto HUD = GetHUD<APHHUD>())
		{
			HUD->RemoveHUDWidget();
		}
	}
	
	Super::PreClientTravel(PendingURL, Travel, bIsSeamlessTravel);
}

void APHPlayerController::ClientSetSpectatorCamera_Implementation(FVector CameraLocation, FRotator CameraRotation)
{
	SetInitialLocationAndRotation(CameraLocation, CameraRotation);
	SetViewTarget(this);
}

void APHPlayerController::ClientGameStarted_Implementation()
{
	// Enable controls mode now the game has started
	SetIgnoreGameActions(false);

	// Send round start event
	const IOnlineEventsPtr Events = Online::GetEventsInterface();
	const auto LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer && Events.IsValid())
	{
		const FUniqueNetIdRepl UniqueId = LocalPlayer->GetPreferredUniqueNetId();
		if (UniqueId.IsValid())
		{
			// Generate a new session id
			Events->SetPlayerSessionId(*UniqueId, FGuid::NewGuid());

			const FString LevelName = *FPackageName::GetShortName(GetWorld()->PersistentLevel->GetOutermost()->GetName());

			// Fire session start event for all cases
			FOnlineEventParms Params;
			Params.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ph)
			Params.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused
			Params.Add(TEXT("MapName"), FVariantData(LevelName));

			Events->TriggerEvent(*UniqueId, TEXT("PlayerSessionStart"), Params);

			// Online matches require the MultiplayerRoundStart event as well
			const auto GI = GetWorld() ? GetWorld()->GetGameInstance<UPHGameInstance>() : nullptr;
			if (GI && (GI->GetOnlineMode() == EOnlineMode::Online))
			{
				FOnlineEventParms MultiplayerParams;

				// @todo: fill in with real values
				MultiplayerParams.Add(TEXT("SectionId"), FVariantData((int32)0)); // unused
				MultiplayerParams.Add(TEXT("GameplayModeId"), FVariantData((int32)1)); // @todo determine game mode (ph)
				MultiplayerParams.Add(TEXT("MatchTypeId"), FVariantData((int32)1)); // @todo abstract the specific meaning of this value across platforms
				MultiplayerParams.Add(TEXT("DifficultyLevelId"), FVariantData((int32)0)); // unused

				Events->TriggerEvent(*UniqueId, TEXT("MultiplayerRoundStart"), MultiplayerParams);
			}

			bHasSentStartEvents = true;
		}
	}
}

void APHPlayerController::ClientStartOnlineGame_Implementation()
{
	if (!IsPrimaryPlayer())
	{
		return;
	}

	if (const auto MyPlayerState = GetPlayerState<APHPlayerState>())
	{
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			const FName& SessionName = MyPlayerState->SessionName;

			const IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid() && Sessions->GetNamedSession(SessionName))
			{
				UE_LOG(LogOnline, Log, TEXT("Starting session %s on client"), *SessionName.ToString());
				Sessions->StartSession(SessionName);
			}
		}
	}
	else
	{
		// Keep retrying until player state is replicated
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ClientStartOnlineGame, this, &APHPlayerController::ClientStartOnlineGame_Implementation, 0.2f, false);
	}
}

void APHPlayerController::ClientGameEnded_Implementation(class AActor* EndGameFocus, bool bIsWinner)
{
	Super::ClientGameEnded_Implementation(EndGameFocus, bIsWinner);

	// Disable controls now the game has ended
	SetIgnoreGameActions(true);

	// Make sure that we still have valid view target
	SetViewTarget(GetPawn());

	ClientShowInfoMessage(EInfoMessageType::INFO_RoundEnd, 0.f);

// 	UpdateSaveFileOnGameEnd(bIsWinner);
// 	UpdateAchievementsOnGameEnd();
// 	UpdateLeaderboardsOnGameEnd();

	// Flag that the game has just ended (if it's ended due to host loss we want to wait for ClientReturnToMainMenu_Implementation first, in case we don't want to process)
	bGameEndedFrame = true;
}

void APHPlayerController::ClientSendRoundEndEvent_Implementation(bool bIsWinner, int32 ExpendedTimeInSeconds)
{

}

void APHPlayerController::Say(const FString& Msg, FName Type)
{
	ServerSay(Msg.Left(128), Type);
}

void APHPlayerController::ClientTeamMessage_Implementation(APlayerState* SenderPlayerState, const FString& S, FName Type, float MsgLifeTime)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->AddChatLine(SenderPlayerState, S, Type, MsgLifeTime);
	}
}

void APHPlayerController::SetInfiniteAmmo(bool bEnable)
{
	bInfiniteAmmo = bEnable;
}

void APHPlayerController::SetInfiniteClip(bool bEnable)
{
	bInfiniteClip = bEnable;
}

void APHPlayerController::SetGodMode(bool bEnable)
{
	bGodMode = bEnable;
}

void APHPlayerController::CleanupSessionOnReturnToMenu()
{
	const auto GI = GetWorld() ? Cast<UPHGameInstance>(GetWorld()->GetGameInstance()) : nullptr;
	if (ensure(GI))
	{
		GI->CleanupSessionOnReturnToMenu();
	}
}

void APHPlayerController::OnKill()
{
// 	UpdateAchievementProgress(ACH_FRAG_SOMEONE, 100.0f);
}

void APHPlayerController::HandleReturnToMainMenu()
{
	CleanupSessionOnReturnToMenu();
}

bool APHPlayerController::FindDeathCameraSpot(FVector& CameraLocation, FRotator& CameraRotation)
{
	const FVector PawnLocation = GetPawn()->GetActorLocation();
	FRotator ViewDir = GetControlRotation();
	ViewDir.Pitch = -45.f;

	constexpr float YawOffsets[] = { 0.f, -180.f, 90.f, -90.f, 45.f, -45.f, 135.f, -135.f };
	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(DeathCamera), true, GetPawn());

	FHitResult HitResult;
	for (float YawOffset : YawOffsets)
	{
		constexpr float CameraOffset = 300.f;
		FRotator CameraDir = ViewDir;
		CameraDir.Yaw += YawOffset;
		CameraDir.Normalize();

		const FVector TestLocation = PawnLocation - CameraDir.Vector() * CameraOffset;

		const bool bBlocked = GetWorld()->LineTraceSingleByChannel(HitResult, PawnLocation, TestLocation, ECC_Camera, TraceParams);

		if (!bBlocked)
		{
			CameraLocation = TestLocation;
			CameraRotation = CameraDir;
			return true;
		}
	}

	return false;
}

void APHPlayerController::ServerResetPlayer_Implementation()
{
	ClientReset();
	Reset();
}

bool APHPlayerController::ServerResetPlayer_Validate()
{
	return true;
}

void APHPlayerController::ServerSay_Implementation(const FString& Msg, FName Type)
{
	GetWorld()->GetAuthGameMode<APHGameModeBase>()->Broadcast(this, Msg, Type);
}

bool APHPlayerController::ServerSay_Validate(const FString& Msg, FName Type)
{
	return true;
}

void APHPlayerController::ClientShowTeamChoice_Implementation(bool bEnable)
{
	const auto GameViewportClient = Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport());
	if (GameViewportClient)
	{
		bEnable ? GameViewportClient->ShowTeamChoice() : GameViewportClient->HideTeamChoice();
	}
}

void APHPlayerController::OnDeathMessage(APHPlayerState* KillerPlayerState, APHPlayerState* KilledPlayerState, UGameplayEffectUIData* InUIData)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->ShowDeathMessage(KillerPlayerState, KilledPlayerState, InUIData);
	}
}

void APHPlayerController::OnToggleInGameMenu()
{
	const auto GameViewportClient = Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport());
	if (GameViewportClient)
	{
		GameViewportClient->ShowInGameMenu();
	}
}

void APHPlayerController::OnToggleTeamChoice()
{
	if (IsGameActionsIgnored())
	{
		return;
	}

	const auto PC = GetPlayerState<APHPlayerState>();
	if (!PC || PC->GetTeamNumber() == ETeamType::None)
	{
		return;
	}

	const auto GameViewportClient = Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport());
	if (GameViewportClient)
	{
		GameViewportClient->ToggleTeamChoice();
	}
}

void APHPlayerController::OnShowScoreboard(bool bEnable)
{
	if (bEnable && IsGameActionsIgnored())
	{
		return;
	}

	const auto GameViewportClient = Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport());
	if (GameViewportClient)
	{
		bEnable ? GameViewportClient->ShowScoreboard() : GameViewportClient->HideScoreboard();
	}
}

void APHPlayerController::OnOpenChatWindow(bool bTeammate /*= false*/)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->OpenChat(bTeammate);
	}
}

void APHPlayerController::OnUpdateStatus()
{
	if (bInLobby)
	{
		ServerUpdateReadyStatus();
	}
}

void APHPlayerController::ChooseTeam(int32 TeamNum)
{
	if (IsInState(NAME_Playing) && !bInLobby)
	{
		if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
		{
			UDialog* Dialog = GameInstance->ShowMessage(NSLOCTEXT("DialogHeader", "Info", "Info"),
				NSLOCTEXT("InfoMessage", "ChangeTeamPostponement", "Your team will be changed in the next round"),
				NSLOCTEXT("DialogButtons", "OKAY", "OK"));

			if (Dialog)
			{
				Dialog->OnConfirmClicked.AddUObject(this, &APHPlayerController::ServerChangeTeamPostponement, TeamNum);
				Dialog->OnCancelClicked.AddUObject(this, &APHPlayerController::ServerChangeTeamPostponement, TeamNum);
			}
		}
	}
	else
	{
		if (!HasAuthority())
		{
			ServerChooseTeam(TeamNum);
		}
		ServerChooseTeam_Implementation(TeamNum);

		OnToggleTeamChoice();
	}
}

void APHPlayerController::ChooseNextMap(int32 MapIndex)
{
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerChooseNextMap(MapIndex);
	}

	ServerChooseNextMap_Implementation(MapIndex);
}

void APHPlayerController::Suicide()
{
	if (IsInState(NAME_Playing))
	{
		ServerSuicide();
	}
}

void APHPlayerController::ClientShowInfoMessage_Implementation(uint8 MessageType, float Duration)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->ShowInfoMessage(static_cast<EInfoMessageType>(MessageType), Duration);
	}
}

void APHPlayerController::ClientShowWinner_Implementation(int32 TeamNumber)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->ShowWinner(TeamNumber);
	}
}

void APHPlayerController::ClientNotifyWeaponHit_Implementation(float DamageTaken, const FHitResult& HitInfo)
{
	//TODO
}

void APHPlayerController::ClientNotifyEnemyHit_Implementation()
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->NotifyEnemyHit();
	}
}

void APHPlayerController::ClientUpdateReadyStatus_Implementation(int32 ReadyPlayers, int32 Total)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->UpdateReadyStatus(ReadyPlayers, Total);
	}
}

void APHPlayerController::ClientShowMatchResultWithDelay_Implementation(float Delay)
{
	FTimerHandle TimerHandle_Unused;
	GetWorldTimerManager().SetTimer(TimerHandle_Unused, this, &APHPlayerController::ShowMatchResult, Delay);
}

void APHPlayerController::ClientShowPortalStatus_Implementation()
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->ShowPortalStatus();
	}
}

void APHPlayerController::ClientAddTeammatesToMinimap_Implementation(const TArray<APawn*>& TeammatePawns)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->AddPlayersToMinimap(TeammatePawns);
	}
}

void APHPlayerController::ZoomMinimap()
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->ZoomMinimap();
	}
}

void APHPlayerController::ClientSetPlayersIndicatorVisibility_Implementation(const TArray<APawn*>& PlayerPawns, bool bVisible)
{
	for (APawn* PlayerPawn : PlayerPawns)
	{
		if (const auto CharacterToUpdate = Cast<APHCharacterBase>(PlayerPawn))
		{
			CharacterToUpdate->SetIndicatorVisibility(bVisible);
		}
	}
}

void APHPlayerController::ServerUpdateCharactersInfo_Implementation(const TArray<APawn*>& Pawns)
{
	TArray<APawn*> Teammates;
	TArray<APawn*> OtherPawns;

	for (APawn* PawnToUpdate : Pawns)
	{
		const auto PS = GetPlayerState<APHPlayerState>();
		const auto PawnPS = PawnToUpdate ? PawnToUpdate->GetPlayerState<APHPlayerState>() : nullptr;
		if (PS && PawnPS && PS != PawnPS)
		{
			if (PS->GetTeamNumber() == PawnPS->GetTeamNumber())
			{
				Teammates.Add(PawnToUpdate);
			}
			else
			{
				OtherPawns.Add(PawnToUpdate);
			}
		}
	}

	if (Teammates.Num() > 0)
	{
		ClientAddTeammatesToMinimap(Teammates);
		ClientSetPlayersIndicatorVisibility(Teammates, true);
	}
	if (OtherPawns.Num() > 0)
	{
		ClientSetPlayersIndicatorVisibility(OtherPawns, false);
	}
}

bool APHPlayerController::ServerUpdateCharactersInfo_Validate(const TArray<APawn*>& Pawns)
{
	return true;
}

void APHPlayerController::ShowMatchResult()
{
	const auto GameViewportClient = Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport());
	if (GameViewportClient)
	{
		GameViewportClient->ShowMatchResult();
	}
}

void APHPlayerController::ShowHint(const FHint& Hint)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->ShowHint(Hint);
	}
}

void APHPlayerController::HideHint(EHintType HintToHide)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->ShowHint({ FText::GetEmpty(), HintToHide });
	}
}

void APHPlayerController::AddPointOfInterest(AActor* TargetActor, EPointOfInterest DisplayType, UTexture2D* MinimapIcon, UTexture2D* ScreenIcon, float Size /*= 64.f*/, FLinearColor Color /*= FLinearColor::White*/, bool bStatic /*= true*/)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->AddPointOfInterest(TargetActor, DisplayType, MinimapIcon, ScreenIcon, Size, Color, bStatic);
	}
}

void APHPlayerController::RemovePointOfInterest(AActor* TargetActor)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->RemovePointOfInterest(TargetActor);
	}
}

void APHPlayerController::StartInteractionTimer(float Duration)
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->StartInteractionTimer(Duration);
	}
}

void APHPlayerController::StopInteractionTimer()
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->StopInteractionTimer();
	}
}

UPHPersistentUser* APHPlayerController::GetPersistentUser() const
{
	const auto LocalPlayer = Cast<UPHLocalPlayer>(Player);
	return LocalPlayer ? LocalPlayer->GetPersistentUser() : nullptr;
}

void APHPlayerController::SetCinematicMode(bool bInCinematicMode, bool bHidePlayer, bool bAffectsHUD, bool bAffectsMovement, bool bAffectsTurning)
{
	Super::SetCinematicMode(bInCinematicMode, bHidePlayer, bAffectsHUD, bAffectsMovement, bAffectsTurning);

	if (bAffectsHUD)
	{
		if (const auto HUD = GetHUD<APHHUD>())
		{
			HUD->ShowHUD(!bInCinematicMode);
		}
	}
}

bool APHPlayerController::IsMoveInputIgnored() const
{
	const auto PS = GetPlayerState<APHPlayerState>();
	if (PS && PS->IsInteracting())
	{
		return true;
	}

	return Super::IsMoveInputIgnored();
}

void APHPlayerController::ResetIgnoreInputFlags()
{
	if (!bIgnoreGameActions)
	{
		Super::ResetIgnoreInputFlags();
	}
}

void APHPlayerController::NotifyLoadedWorld(FName MovieSceneBlends, bool bFinalDest)
{
	// Place the camera at the first PlayerStart with None team type
	SetViewTarget(this);

	for(TActorIterator<APHPlayerStart> It(GetWorld()); It; ++It)
	{
		const APHPlayerStart* TestPlayerStart = *It;
		if (TestPlayerStart->Team == ETeam::TEAM_None)
		{
			FRotator SpawnRotation(ForceInit);
			SpawnRotation.Yaw = TestPlayerStart->GetActorRotation().Yaw;
			SetInitialLocationAndRotation(TestPlayerStart->GetActorLocation(), SpawnRotation);
			break;
		}
	}
}

void APHPlayerController::AutoManageActiveCameraTarget(AActor* MovieSceneBlends)
{
	if (MovieSceneBlends && MovieSceneBlends->IsA(AHunterCharacter::StaticClass()))
	{
		for(TActorIterator<ACameraActor> It(GetWorld()); It; ++It)
		{
			ACameraActor* TestCamera = *It;
			if (TestCamera->ActorHasTag("Teleport"))
			{
				SetViewTarget(TestCamera);
				return;
			}
		}
	}
	
	Super::AutoManageActiveCameraTarget(MovieSceneBlends);
}

DECLARE_DELEGATE_OneParam(FActionDelegate, bool);

void APHPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// UI input
	InputComponent->BindAction("InGameMenu", IE_Pressed, this, &APHPlayerController::OnToggleInGameMenu);
	InputComponent->BindAction("TeamChoice", IE_Pressed, this, &APHPlayerController::OnToggleTeamChoice);
	InputComponent->BindAction<FActionDelegate>("Scoreboard", IE_Pressed, this, &APHPlayerController::OnShowScoreboard, true);
	InputComponent->BindAction<FActionDelegate>("Scoreboard", IE_Released, this, &APHPlayerController::OnShowScoreboard, false);

	InputComponent->BindAction<FActionDelegate>("OpenChat", IE_Pressed, this, &APHPlayerController::OnOpenChatWindow, false);
	InputComponent->BindAction<FActionDelegate>("OpenTeamChat", IE_Pressed, this, &APHPlayerController::OnOpenChatWindow, true);
	InputComponent->BindAction("UpdateStatus", IE_Pressed, this, &APHPlayerController::OnUpdateStatus);
}

void APHPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(APHPlayerController, bInfiniteAmmo, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(APHPlayerController, bInfiniteClip, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(APHPlayerController, bInLobby, COND_OwnerOnly);
}

void APHPlayerController::UnFreeze()
{
	if (!bInLobby)
	{
		if (const auto MyPlayerState = GetPlayerState<APHPlayerState>())
		{
			if (MyPlayerState->IsAlive())
			{
				ServerRestartPlayer();
			}
			else
			{
				// spawn as spectator after death
				ServerResetPlayer();
			}
		}
	}
}

void APHPlayerController::UpdateHUD()
{
	if (const auto HUD = GetHUD<APHHUD>())
	{
		HUD->Setup();
	}

	TArray<APawn*> Pawns;
	for (TActorIterator<APawn> PawnIterator(GetWorld()); PawnIterator; ++PawnIterator)
	{
		Pawns.Add(*PawnIterator);
	}

	if (Pawns.Num() > 0)
	{
		ServerUpdateCharactersInfo(Pawns);
	}
}

void APHPlayerController::SetIgnoreGameActions(bool bNewGameActions)
{
	bIgnoreGameActions = bNewGameActions;

	SetIgnoreLookInput(bIgnoreGameActions);
	SetIgnoreMoveInput(bIgnoreGameActions);
}

bool APHPlayerController::IsGameActionsIgnored() const
{
	return bIgnoreGameActions;
}

void APHPlayerController::SetIsVibrationEnabled(bool bEnable)
{
	bVibrationEnabled = bEnable;
}

bool APHPlayerController::IsVibrationEnabled() const
{
	return bVibrationEnabled;
}

void APHPlayerController::ServerChooseTeam_Implementation(int32 TeamNum)
{
	const auto MyPlayerState = GetPlayerState<APHPlayerState>();
	if (MyPlayerState && MyPlayerState->GetTeamNumber() != TeamNum)
	{
		MyPlayerState->SetTeamNumber(TeamNum);
	}

	if (bInLobby)
	{
		const auto GM = GetWorld()->GetAuthGameMode<AGameMode_Lobby>();
		if (GM)
		{
			GM->RespawnPlayer(this);
		}
	}
}

bool APHPlayerController::ServerChooseTeam_Validate(int32 TeamNum)
{
	return true;
}

void APHPlayerController::ServerChangeTeamPostponement_Implementation(int32 TeamNumber)
{
	if (const auto MyPlayerState = GetPlayerState<APHPlayerState>())
	{
		MyPlayerState->SetDesiredTeamNumber(TeamNumber);
	}
}

bool APHPlayerController::ServerChangeTeamPostponement_Validate(int32 TeamNumber)
{
	return true;
}

void APHPlayerController::ServerChooseNextMap_Implementation(int32 MapIndex)
{
	if (const auto MyPlayerState = GetPlayerState<APHPlayerState>())
	{
		if (MyPlayerState->NextMapVote == MapIndex)
		{
			MyPlayerState->NextMapVote = -1;
		}
		else
		{
			MyPlayerState->NextMapVote = MapIndex;
		}
	}
}

bool APHPlayerController::ServerChooseNextMap_Validate(int32 MapIndex)
{
	return true;
}

void APHPlayerController::ServerSuicide_Implementation()
{
	if (GetPawn() && ((GetWorld()->TimeSeconds - GetPawn()->CreationTime > 1) || (GetNetMode() == NM_Standalone)))
	{
		if (const auto MyCharacter = GetPawn<APHCharacterBase>())
		{
			MyCharacter->Suicide();
		}
	}
}

bool APHPlayerController::ServerSuicide_Validate()
{
	return true;
}

void APHPlayerController::ServerUpdateReadyStatus_Implementation()
{
	if (const auto MyPlayerState = GetPlayerState<APHPlayerState>())
	{
		MyPlayerState->bReady = !MyPlayerState->bReady;
	}

	if (bInLobby)
	{
		const auto GM = GetWorld()->GetAuthGameMode<AGameMode_Lobby>();
		if (GM)
		{
			GM->UpdateReadyStatus();
		}
	}
}

bool APHPlayerController::ServerUpdateReadyStatus_Validate()
{
	return true;
}

void APHPlayerController::ClientBeginTransportation_Implementation()
{
	for (TActorIterator<AHunterCharacter> It(GetWorld()); It; ++It)
	{
		It->ShowTransportationEffect(true);
	}
}
