// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/PHPlayerStart.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/BillboardComponent.h"


APHPlayerStart::APHPlayerStart(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	HunterSprite = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("HunterSprite"));
	PropSprite = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("PropSprite"));

#if WITH_EDITORONLY_DATA
	if (!IsRunningCommandlet())
	{
		struct FConstructorStatics
		{
			ConstructorHelpers::FObjectFinderOptional<UTexture2D> HunterTextureObject;
			ConstructorHelpers::FObjectFinderOptional<UTexture2D> PropTextureObject;
			FName ID_Icon;
			FText NAME_Icon;
			FConstructorStatics()
				: HunterTextureObject(TEXT("/Game/PropHunt/UI/Editor/S_PlayerHunter")), 
				PropTextureObject(TEXT("/Game/PropHunt/UI/Editor/S_PlayerProp")),
				ID_Icon(TEXT("PhysicsItem")), 
				NAME_Icon(NSLOCTEXT("SpriteCategory", "PhysicsItem", "PhysicsItem"))
			{}
		};
		static FConstructorStatics ConstructorStatics;

		if (HunterSprite)
		{
			HunterSprite->Sprite = ConstructorStatics.HunterTextureObject.Get();
			HunterSprite->SetRelativeScale3D({ 0.5f, 0.5f, 0.5f });
			HunterSprite->bHiddenInGame = true;
			HunterSprite->SpriteInfo.Category = ConstructorStatics.ID_Icon;
			HunterSprite->SpriteInfo.DisplayName = ConstructorStatics.NAME_Icon;
			HunterSprite->SetupAttachment(GetRootComponent());
			HunterSprite->SetUsingAbsoluteScale(true);
			HunterSprite->bIsScreenSizeScaled = true;
			HunterSprite->SetRelativeLocation({0.f, 0.f, 65.f});
		}

		if (PropSprite)
		{
			PropSprite->Sprite = ConstructorStatics.PropTextureObject.Get();
			PropSprite->SetRelativeScale3D({ 0.5f, 0.5f, 0.5f });
			PropSprite->bHiddenInGame = true;
			PropSprite->SpriteInfo.Category = ConstructorStatics.ID_Icon;
			PropSprite->SpriteInfo.DisplayName = ConstructorStatics.NAME_Icon;
			PropSprite->SetupAttachment(GetRootComponent());
			PropSprite->SetUsingAbsoluteScale(true);
			PropSprite->bIsScreenSizeScaled = true;
			PropSprite->SetRelativeLocation({ 0.f, 0.f, 65.f });
		}
	}
#endif
}

#if WITH_EDITOR
void APHPlayerStart::OnConstruction(const FTransform& Transform)
{
	if (HunterSprite && PropSprite)
	{
		if (Team == ETeam::TEAM_Hunters)
		{
			HunterSprite->SetVisibility(true);
			PropSprite->SetVisibility(false);
		}
		else if (Team == ETeam::TEAM_Props)
		{
			HunterSprite->SetVisibility(false);
			PropSprite->SetVisibility(true);
			bHuntingPreparation = false;
		}
		else
		{
			HunterSprite->SetVisibility(false);
			PropSprite->SetVisibility(false);
			bHuntingPreparation = false;
		}
	}
}
#endif
