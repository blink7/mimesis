// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHAssetManager.h"

#include "../PropHunt.h"
#include "Core/Items/PHItem.h"

#include "AbilitySystemGlobals.h"


const FPrimaryAssetType	UPHAssetManager::SkillItemType = TEXT("Skill");
const FPrimaryAssetType	UPHAssetManager::TokenItemType = TEXT("Token");
const FPrimaryAssetType	UPHAssetManager::WeaponItemType = TEXT("Weapon");

void UPHAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();

	UAbilitySystemGlobals::Get().InitGlobalData();
}

UPHAssetManager& UPHAssetManager::Get()
{
	UPHAssetManager* This = Cast<UPHAssetManager>(GEngine->AssetManager);

	if (This)
	{
		return *This;
	}
	else
	{
		UE_LOG(LogPropHunt, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be PHAssetManager!"));
		return *NewObject<UPHAssetManager>(); // never calls this
	}
}

UPHItem* UPHAssetManager::ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning /*= true*/)
{
	FSoftObjectPath ItemPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	auto LoadedItem = Cast<UPHItem>(ItemPath.TryLoad());
	
	if (bLogWarning && !LoadedItem)
	{
		UE_LOG(LogPropHunt, Warning, TEXT("Failed to load item for identifier %s!"), *PrimaryAssetId.ToString());
	}

	return LoadedItem;
}
