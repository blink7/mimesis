// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/PHLocalPlayer.h"

#include "Core/PHGameInstance.h"
#include "Core/PHPersistentUser.h"

#include "Engine/World.h"


void UPHLocalPlayer::SetControllerId(int32 NewControllerId)
{
	FString SaveGameName = GetNickname();

	ULocalPlayer::SetControllerId(NewControllerId);

	// if we changed controllerId / user, then we need to load the appropriate persistent user.
	if (PersistentUser && (GetControllerId() != PersistentUser->GetUserIndex() || SaveGameName != PersistentUser->GetName()))
	{
		PersistentUser->SaveIfDirty();
		PersistentUser = nullptr;
	}

	if (!PersistentUser)
	{
		LoadPersistentUser();
	}
}

FString UPHLocalPlayer::GetNickname() const
{
	FString UserNickName = Super::GetNickname();

	bool bReplace = (UserNickName.Len() == 0);

	// Check for duplicate nicknames...and prevent reentry
	static bool bReentry = false;
	if (!bReentry)
	{
		bReentry = true;
		const auto GameInstance = GetWorld() ? Cast<UPHGameInstance>(GetWorld()->GetGameInstance()) : nullptr;
		if (GameInstance)
		{
			// Check all the names that occur before ours that are the same
			const auto& LocalPlayers = GameInstance->GetLocalPlayers();
			for (const auto LocalPlayer : LocalPlayers)
			{
				if (this == LocalPlayer)
				{
					break;
				}

				if (UserNickName == LocalPlayer->GetNickname())
				{
					bReplace = true;
					break;
				}
			}
		}
		bReentry = false;
	}

	if (bReplace)
	{
		UserNickName = FString::Printf(TEXT("Player%i"), GetControllerId() + 1);
	}

	return UserNickName;
}

class UPHPersistentUser* UPHLocalPlayer::GetPersistentUser() const
{
	// if persistent data isn't loaded yet, load it
	if (!PersistentUser)
	{
		auto MutableThis = const_cast<UPHLocalPlayer*>(this);
		// casting away constness to enable caching implementation behavior
		MutableThis->LoadPersistentUser();
	}
	return PersistentUser;
}

void UPHLocalPlayer::LoadPersistentUser()
{
	FString SaveGameName = GetNickname();

	// if we changed controllerId / user, then we need to load the appropriate persistent user.
	if (PersistentUser && (GetControllerId() != PersistentUser->GetUserIndex() || SaveGameName != PersistentUser->GetName()))
	{
		PersistentUser->SaveIfDirty();
		PersistentUser = nullptr;
	}

	if (!PersistentUser)
	{
		// Use the platform id here to be resilient in the face of controller swapping and similar situations.
		FPlatformUserId PlatformId = GetControllerId();

		auto Identity = Online::GetIdentityInterface();
		if (Identity.IsValid() && GetPreferredUniqueNetId().IsValid())
		{
			PlatformId = Identity->GetPlatformUserIdFromUniqueNetId(*GetPreferredUniqueNetId());
		}

		PersistentUser = UPHPersistentUser::LoadPersistentUser(SaveGameName, PlatformId);
	}
}
