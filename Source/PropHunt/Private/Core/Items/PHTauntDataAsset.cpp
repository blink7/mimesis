﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Items/PHTauntDataAsset.h"

#include "Engine/AssetManager.h"


UPHTauntDataAsset::UPHTauntDataAsset()
{
	Rules.Priority = 2000;
	Rules.ChunkId = 2001;
	Rules.bApplyRecursively = true;
	Rules.CookRule = EPrimaryAssetCookRule::AlwaysCook;
}

#if WITH_EDITORONLY_DATA
void UPHTauntDataAsset::UpdateAssetBundleData()
{
	Super::UpdateAssetBundleData();

	if (UAssetManager* Manager = UAssetManager::GetIfValid())
	{
		// Update rules
		Manager->SetPrimaryAssetRules(GetPrimaryAssetId(), Rules);
	}
}
#endif
