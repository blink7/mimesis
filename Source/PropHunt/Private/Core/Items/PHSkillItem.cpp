// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Items/PHSkillItem.h"

#include "Core/PHAssetManager.h"


UPHSkillItem::UPHSkillItem()
{
	ItemType = UPHAssetManager::SkillItemType;
}
