// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Items/PHWeaponItem.h"

#include "Core/PHAssetManager.h"


UPHWeaponItem::UPHWeaponItem()
{
	ItemType = UPHAssetManager::WeaponItemType;
	SuitableTeam = EItemSuitableTeam::Hunter;
}
