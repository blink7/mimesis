// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHBlueprintFunctionLibrary.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "Core/Abilities/PHGameplayEffectTypes.h"

#include "GameFramework/PlayerInput.h"
#include "GameFramework/InputSettings.h"


bool UPHBlueprintFunctionLibrary::IsAbilitySpecHandleValid(const FGameplayAbilitySpecHandle& Handle)
{
	return Handle.IsValid();
}

UObject* UPHBlueprintFunctionLibrary::GetSourceObject(const FGameplayCueParameters& Parameters)
{
	return const_cast<UObject*>(Parameters.GetSourceObject());
}

FPHGameplayEffectContainerSpec UPHBlueprintFunctionLibrary::AddTargetsToEffectContainerSpec(const FPHGameplayEffectContainerSpec& ContainerSpec, const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors)
{
	FPHGameplayEffectContainerSpec NewSpec = ContainerSpec;
	NewSpec.AddTargets(HitResults, TargetActors);
	return NewSpec;
}

TArray<FActiveGameplayEffectHandle> UPHBlueprintFunctionLibrary::ApplyExternalEffectContainerSpec(const FPHGameplayEffectContainerSpec& ContainerSpec)
{
	FPHGameplayEffectContainerSpec NewSpec = ContainerSpec;
	return NewSpec.ApplyExternally();
}

void UPHBlueprintFunctionLibrary::GetHitActorFromTargetData(const FGameplayAbilityTargetDataHandle& TargetData, int32 Index, AActor*& HitActor, UPrimitiveComponent*& HitComponent)
{
	const FHitResult& Hit = UAbilitySystemBlueprintLibrary::GetHitResultFromTargetData(TargetData, Index);
	HitActor = Hit.GetActor();
	HitComponent = Hit.GetComponent();
}

FString UPHBlueprintFunctionLibrary::GetGameVersion()
{
	FString ProjectVersion;
	GConfig->GetString(TEXT("/Script/EngineSettings.GeneralProjectSettings"), TEXT("ProjectVersion"), ProjectVersion, GGameIni);
	return ProjectVersion;
}

FText UPHBlueprintFunctionLibrary::GetKeyName(FName InActionName)
{
	TArray<FInputActionKeyMapping> Mappings;
	UInputSettings::GetInputSettings()->GetActionMappingByName(InActionName, Mappings);

	for (const FInputActionKeyMapping& Mapping : Mappings)
	{
		if (!Mapping.Key.IsGamepadKey())
		{
			return Mapping.Key.GetDisplayName();
		}
	}

	if (Mappings.IsValidIndex(0))
	{
		return Mappings[0].Key.GetDisplayName();
	}

	return FText();
}

FString UPHBlueprintFunctionLibrary::GetAssetName(TSoftObjectPtr<> SoftObjectReference)
{
	return SoftObjectReference.ToSoftObjectPath().GetAssetName();
}

TSoftObjectPtr<UAkExternalMediaAsset> UPHBlueprintFunctionLibrary::EffectContextGetExternalMediaAsset(FGameplayEffectContextHandle EffectContextHandle)
{
	const auto MyEffectContext = static_cast<FPHGameplayEffectContext*>(EffectContextHandle.Get());
	if (MyEffectContext)
	{
		return MyEffectContext->GetOptionalSoftObject<UAkExternalMediaAsset>();
	}

	return TSoftObjectPtr<UAkExternalMediaAsset>();
}

FGameplayAbilityTargetDataHandle UPHBlueprintFunctionLibrary::AbilityTargetDataFromExternalMediaAsset(const TSoftObjectPtr<UAkExternalMediaAsset>& ExternalMediaAsset)
{
	// Construct TargetData
	auto NewData = new FPHGameplayAbilityTargetData_SoftObject();
	NewData->SetOptionalSoftObject(ExternalMediaAsset);

	// Give it a handle and return
	FGameplayAbilityTargetDataHandle Handle(NewData);
	return Handle;
}

TSoftObjectPtr<UAkExternalMediaAsset> UPHBlueprintFunctionLibrary::GetExternalMediaAssetFromTargetData(const FGameplayAbilityTargetDataHandle& TargetData, int32 Index)
{
	if (TargetData.Data.IsValidIndex(Index))
	{
		const auto MyData = static_cast<const FPHGameplayAbilityTargetData_SoftObject*>(TargetData.Data[Index].Get());
		if (MyData)
		{
			return MyData->GetOptionalSoftObject<UAkExternalMediaAsset>();
		}
	}

	return TSoftObjectPtr<UAkExternalMediaAsset>();
}
