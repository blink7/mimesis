// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/PHPersistentUser.h"

#include "Core/PHLocalPlayer.h"

#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"

#include "GameFramework/PlayerInput.h"
#include "GameFramework/PlayerController.h"


UPHPersistentUser::UPHPersistentUser(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetToDefaults();
}

UPHPersistentUser* UPHPersistentUser::LoadPersistentUser(FString SlotName, const int32 UserIndex)
{
	UPHPersistentUser* Result = nullptr;

	// first set of player signins can happen before the UWorld exists, which means no OSS, which means no user names, which means no slotnames.
	// Persistent users aren't valid in this state.
	if (SlotName.Len() > 0)
	{
		if (!GIsBuildMachine)
		{
			Result = Cast<UPHPersistentUser>(UGameplayStatics::LoadGameFromSlot(SlotName, UserIndex));
		}

		if (!Result)
		{
			// if failed to load, create a new one
			Result = Cast<UPHPersistentUser>(UGameplayStatics::CreateSaveGameObject(UPHPersistentUser::StaticClass()));
		}
		check(Result != nullptr);

		Result->SlotName = SlotName;
		Result->UserIndex = UserIndex;
	}

	return Result;
}

void UPHPersistentUser::SaveIfDirty()
{
	if (bDirty || IsInvertedYAxisDirty() || IsAimSensitivityDirty())
	{
		SavePersistentUser();
		TellInputAboutKeybindings();
	}
}

void UPHPersistentUser::TellInputAboutKeybindings()
{
	TArray<APlayerController*> PlayerList;
	GEngine->GetAllLocalPlayerControllers(PlayerList);

	for (const auto PC : PlayerList)
	{
		if (!PC || !PC->Player || !PC->PlayerInput)
		{
			continue;
		}

		// Update key bindings for the current user only
		const auto LocalPlayer = Cast<UPHLocalPlayer>(PC->Player);
		if (!LocalPlayer || LocalPlayer->GetPersistentUser() != this)
		{
			continue;
		}

		//set the aim sensitivity
		for (auto& AxisMapping : PC->PlayerInput->AxisMappings)
		{
			if (AxisMapping.AxisName == "Lookup" || AxisMapping.AxisName == "LookupRate" || AxisMapping.AxisName == "Turn" || AxisMapping.AxisName == "TurnRate")
			{
				AxisMapping.Scale = (AxisMapping.Scale < 0.0f) ? -GetAimSensitivity() : +GetAimSensitivity();
			}
		}
		PC->PlayerInput->ForceRebuildingKeyMaps();

		//invert it, and if does not equal our bool, invert it again
		if (PC->PlayerInput->GetInvertAxis("LookupRate") != GetInvertedYAxis())
		{
			PC->PlayerInput->InvertAxis("LookupRate");
		}

		if (PC->PlayerInput->GetInvertAxis("Lookup") != GetInvertedYAxis())
		{
			PC->PlayerInput->InvertAxis("Lookup");
		}
	}
}

void UPHPersistentUser::SetVibration(bool bInVibration)
{
	bDirty |= bVibration != bInVibration;

	bVibration = bVibration;
}

void UPHPersistentUser::SetInvertedYAxis(bool bInvert)
{
	bDirty |= bInvertedYAxis != bInvert;

	bInvertedYAxis = bInvert;
}

void UPHPersistentUser::SetAimSensitivity(float InSensitivity)
{
	bDirty |= AimSensitivity != InSensitivity;

	AimSensitivity = InSensitivity;
}

void UPHPersistentUser::SetIsRecordingDemos(const bool InbIsRecordingDemos)
{
	bDirty |= bRecordingDemos != InbIsRecordingDemos;

	bRecordingDemos = InbIsRecordingDemos;
}

void UPHPersistentUser::SetToDefaults()
{
	bDirty = false;

	bVibration = true;
	bInvertedYAxis = false;
	AimSensitivity = 1.0f;
	bRecordingDemos = false;
}

bool UPHPersistentUser::IsAimSensitivityDirty() const
{
	bool bIsAimSensitivityDirty = false;

	// Fixme: UPHPersistentUser is not setup to work with multiple worlds.
	// For now, user settings are global to all world instances.
	if (GEngine)
	{
		TArray<APlayerController*> PlayerList;
		GEngine->GetAllLocalPlayerControllers(PlayerList);

		for (const auto PC : PlayerList)
		{
			if (!PC || !PC->Player || !PC->PlayerInput)
			{
				continue;
			}

			// Update key bindings for the current user only
			const auto LocalPlayer = Cast<UPHLocalPlayer>(PC->Player);
			if (!LocalPlayer || LocalPlayer->GetPersistentUser() != this)
			{
				continue;
			}

			// check if the aim sensitivity is off anywhere
			for (auto& AxisMapping : PC->PlayerInput->AxisMappings)
			{
				if (AxisMapping.AxisName == "Lookup" || AxisMapping.AxisName == "LookupRate" || AxisMapping.AxisName == "Turn" || AxisMapping.AxisName == "TurnRate")
				{
					if (FMath::Abs(AxisMapping.Scale) != GetAimSensitivity())
					{
						bIsAimSensitivityDirty = true;
						break;
					}
				}
			}
		}
	}

	return bIsAimSensitivityDirty;
}

bool UPHPersistentUser::IsInvertedYAxisDirty() const
{
	bool bIsInvertedYAxisDirty = false;
	if (GEngine)
	{
		TArray<APlayerController*> PlayerList;
		GEngine->GetAllLocalPlayerControllers(PlayerList);

		for (const auto PC : PlayerList)
		{
			if (!PC || !PC->Player || !PC->PlayerInput)
			{
				continue;
			}

			// Update key bindings for the current user only
			const auto LocalPlayer = Cast<UPHLocalPlayer>(PC->Player);
			if (!LocalPlayer || LocalPlayer->GetPersistentUser() != this)
			{
				continue;
			}

			bIsInvertedYAxisDirty |= PC->PlayerInput->GetInvertAxis("Lookup") != GetInvertedYAxis();
			bIsInvertedYAxisDirty |= PC->PlayerInput->GetInvertAxis("LookupRate") != GetInvertedYAxis();
		}
	}

	return bIsInvertedYAxisDirty;
}

void UPHPersistentUser::SavePersistentUser()
{
	UGameplayStatics::SaveGameToSlot(this, SlotName, UserIndex);
	bDirty = false;
}
