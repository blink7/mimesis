// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHAttributeSet.h"

#include "Core/PHPlayerController.h"
#include "Core/UI/Widgets/HUDWidget.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/Abilities/PHGameplayEffectUIData_Damage.h"
#include "Character/Prop/PropCharacter.h"
#include "Character/Hunter/HunterCharacter.h"

#include "Net/UnrealNetwork.h"
#include "GameplayEffectExtension.h"
#include "GameFramework/PlayerController.h"


UPHAttributeSet::UPHAttributeSet()
	: MaxHealth(1.f),
	MaxViolations(1.f),
	Energy(1.f),
	MaxEnergy(1.f),
	RifleAmmo(1.f),
	MaxRifleAmmo(1.f),
	RifleClipAmmo(1.f),
	MaxRifleClipAmmo(1.f),
	GrenadeAmmo(1.f),
	MaxGrenadeAmmo(1.f),
	SkillUseAmount(1.f),
	MaxSkillUseAmount(1.f),
	AttackPower(1.f)
{
	// Cache tags
	EnemyHitTag = FGameplayTag::RequestGameplayTag(FName("Effect.Damage.EnemyHit"));;
}

void UPHAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	// This is called whenever attributes change, so for max energy we want to scale the current totals to match
	Super::PreAttributeChange(Attribute, NewValue);

	if (Attribute == GetMaxEnergyAttribute())
	{
		AdjustAttributeForMaxChange(Energy, MaxEnergy, NewValue, GetEnergyAttribute());
	}
	else if (Attribute == GetMaxRifleAmmoAttribute())
	{
		AdjustAttributeForMaxChange(RifleAmmo, MaxRifleAmmo, NewValue, GetRifleAmmoAttribute());
	}
	else if (Attribute == GetMaxRifleClipAmmoAttribute())
	{
		AdjustAttributeForMaxChange(RifleClipAmmo, MaxRifleClipAmmo, NewValue, GetRifleClipAmmoAttribute());
	}
	else if (Attribute == GetMaxGrenadeAmmoAttribute())
	{
		AdjustAttributeForMaxChange(GrenadeAmmo, MaxGrenadeAmmo, NewValue, GetGrenadeAmmoAttribute());
	}
	else if (Attribute == GetMaxSkillUseAmountAttribute())
	{
		AdjustAttributeForMaxChange(SkillUseAmount, MaxSkillUseAmount, NewValue, GetSkillUseAmountAttribute());
	}
}

void UPHAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// Compute the delta between old and new, if it is available
	float DeltaValue = 0;
	if (Data.EvaluatedData.ModifierOp == EGameplayModOp::Type::Additive)
	{
		// If this was additive, store the raw delta value to be passed along later
		DeltaValue = Data.EvaluatedData.Magnitude;
	}

	// Get the Target actor, which should be our owner
	APHPlayerState* TargetActor = nullptr;
	APHPlayerController* TargetController = nullptr;
	APawn* TargetPawn = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->PlayerController.IsValid())
	{
		TargetActor = Cast<APHPlayerState>(Data.Target.AbilityActorInfo->OwnerActor.Get());
		TargetPawn = Cast<APawn>(Data.Target.AbilityActorInfo->AvatarActor.Get());
		TargetController = Cast<APHPlayerController>(Data.Target.AbilityActorInfo->PlayerController.Get());
	}

	const FGameplayAttribute& ModifiedAttribute = Data.EvaluatedData.Attribute;

	if (ModifiedAttribute == GetDamageAttribute())
	{
		// Get the Source actor
		AActor* SourceActor = nullptr;
		APHPlayerController* SourceController = nullptr;
		APawn* SourcePawn = nullptr;
		if (Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->PlayerController.IsValid())
		{
			SourceActor = Source->AbilityActorInfo->OwnerActor.Get();
			SourcePawn = Cast<APawn>(Source->AbilityActorInfo->AvatarActor.Get());
			SourceController = Cast<APHPlayerController>(Source->AbilityActorInfo->PlayerController.Get());

			if (!SourceController && SourcePawn)
			{
				SourceController = SourcePawn->GetController<APHPlayerController>();
			}

			// Set the causer actor based on context if it's set
			if (Context.GetEffectCauser())
			{
				SourceActor = Context.GetEffectCauser();
			}
		}

		// Store a local copy of the amount of damage done and clear the damage attribute
		const float LocalDamageDone = GetDamage();
		SetDamage(0.f);

		if (LocalDamageDone > 0.f)
		{
			auto TargetPropCharacter = Cast<APropCharacter>(TargetPawn);

			// If character was alive before damage is added, handle damage
			// This prevents damage being added to dead things and replaying death animations
			bool bWasAlive = true;
			if (TargetPropCharacter)
			{
				bWasAlive = TargetPropCharacter->IsAlive();
			}

			// Apply the health change and then clamp it
			const float OldHealth = GetHealth();
			SetHealth(FMath::Clamp(OldHealth - LocalDamageDone, 0.f, GetMaxHealth()));

			if (TargetPropCharacter && bWasAlive)
			{
				// This is proper damage
				TargetPropCharacter->HandleDamage(SourceController, SourceActor);

				auto UIData = Cast<UPHGameplayEffectUIData_Damage>(Data.EffectSpec.Def->UIData);

				// Play effects on hit
				if (TargetController)
				{
					TargetController->ClientNotifyWeaponHit(LocalDamageDone, *Context.GetHitResult());

					// Play the force feedback effect on the client player controller
					if (UIData && TargetController->IsVibrationEnabled())
					{
						FForceFeedbackParameters FFParams;
						FFParams.Tag = "Damage";

						if (TargetPropCharacter->IsAlive() && UIData->HitForceFeedback)
						{
							TargetController->ClientPlayForceFeedback(UIData->HitForceFeedback, FFParams);
						}
						else if (!TargetPropCharacter->IsAlive() && UIData->KilledForceFeedback)
						{
							TargetController->ClientPlayForceFeedback(UIData->KilledForceFeedback, FFParams);
						}
					}
				}

				// Show damage hit for the Source player unless it was self damage
				if (SourceController && SourcePawn != TargetPawn)
				{
					if (Data.EffectSpec.Def->HasMatchingGameplayTag(EnemyHitTag))
					{
						SourceController->ClientNotifyEnemyHit();
					}
				}

				if (!TargetPropCharacter->IsAlive())
				{
					TargetPropCharacter->Die(LocalDamageDone, UIData, SourceController, SourceActor);

					// TargetCharacter was alive before this damage and now is not alive, give XP and Gold bounties to Source.
					// Don't give bounty to self.
					if (SourceController != TargetController)
					{
						// Create a dynamic instant Gameplay Effect to give the bounties
						UGameplayEffect* GEBounty = NewObject<UGameplayEffect>(GetTransientPackage(), FName("Bounty"));
						GEBounty->DurationPolicy = EGameplayEffectDurationType::Instant;

						int32 Idx = GEBounty->Modifiers.Num();
						GEBounty->Modifiers.SetNum(Idx + 1);

						FGameplayModifierInfo& InfoXP = GEBounty->Modifiers[Idx];
						InfoXP.ModifierMagnitude = FScalableFloat(GetXPBounty());
						InfoXP.ModifierOp = EGameplayModOp::Additive;
						InfoXP.Attribute = UPHAttributeSet::GetXPAttribute();

						Source->ApplyGameplayEffectToSelf(GEBounty, 1.0f, Source->MakeEffectContext());
					}
				}
			}
		}
	}
	else if (ModifiedAttribute == GetHealthAttribute())
	{
		// Handle other health changes such as from healing or direct modifiers
		// First clamp it
		SetHealth(FMath::Clamp(GetHealth(), 0.f, GetMaxHealth()));
	}
	else if (ModifiedAttribute == GetPropertyDamageAttribute())
	{
		// Store a local copy of the amount of damage done and clear the damage attribute
		const float PropertyDamageDone = GetPropertyDamage();
		SetPropertyDamage(0.f);

		if (PropertyDamageDone > 0.f)
		{
			auto TargetHunterCharacter = Cast<AHunterCharacter>(TargetPawn);

			bool bWasAdmitted = true;
			if (TargetHunterCharacter)
			{
				bWasAdmitted = TargetHunterCharacter->IsAdmitted();
			}

			// Apply the violations change and then clamp it
			const float OldViolations = GetViolations();
			SetViolations(FMath::Clamp(OldViolations + PropertyDamageDone, 0.f, GetMaxViolations()));

			if (TargetHunterCharacter && bWasAdmitted)
			{
				TargetHunterCharacter->HandleViolation();

				if (!TargetHunterCharacter->IsAdmitted())
				{
					auto UIData = Cast<UPHGameplayEffectUIData_Damage>(Data.EffectSpec.Def->UIData);

					TargetHunterCharacter->Eject(PropertyDamageDone, UIData);
				}
			}
		}
	}
	else if (ModifiedAttribute == GetViolationsAttribute())
	{
		SetViolations(FMath::Clamp(GetViolations(), 0.f, GetMaxViolations()));
	}
	else if (ModifiedAttribute == GetDamageImpulseAttribute())
	{
		// Try to extract a hit result
		FHitResult HitResult;
		if (Context.GetHitResult())
		{
			HitResult = *Context.GetHitResult();
		}

		const float LocalDamageImpulseDone = GetDamageImpulse();
		SetDamageImpulse(0.f);

		auto TargetPropCharacter = Cast<APropCharacter>(TargetPawn);
		if (TargetPropCharacter)
		{
			TargetPropCharacter->ApplyDamageMomentumWithHit(LocalDamageImpulseDone, HitResult);
		}
	}
	else if (ModifiedAttribute == GetMoveSpeedAttribute())
	{
		if (TargetActor)
		{
			// Call for all movespeed changes
			TargetActor->HandleMoveSpeedChanged(DeltaValue, SourceTags);
		}
	}
	else if (ModifiedAttribute == GetEnergyAttribute())
	{
		SetEnergy(FMath::Clamp(GetEnergy(), 0.f, GetMaxEnergy()));

		if (TargetController)
		{
			// Call for all stamina changes
// 			ABILITY_LOG(Warning, TEXT("Energy: %f, MaxEnergy: %f"), GetEnergy(), GetMaxEnergy());
		}
	}
	else if (ModifiedAttribute == GetXPAttribute())
	{
		if (TargetActor)
		{
			TargetActor->HandleXPChanged(DeltaValue, SourceTags);
		}
	}
}

void UPHAttributeSet::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, Violations, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MaxViolations, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, Energy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MaxEnergy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, RifleAmmo, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MaxRifleAmmo, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, RifleClipAmmo, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MaxRifleClipAmmo, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, GrenadeAmmo, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MaxGrenadeAmmo, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, SkillUseAmount, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MaxSkillUseAmount, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, AttackPower, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, DefensePower, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, MoveSpeed, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, XP, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAttributeSet, XPBounty, COND_None, REPNOTIFY_Always);
}

FGameplayAttribute UPHAttributeSet::GetAmmoAttributeFromTag(FGameplayTag& PrimaryAmmoTag)
{
	if (PrimaryAmmoTag == FGameplayTag::RequestGameplayTag(FName("Weapon.Ammo.Rifle")))
	{
		return GetRifleAmmoAttribute();
	}
	else if (PrimaryAmmoTag == FGameplayTag::RequestGameplayTag(FName("Weapon.Ammo.Grenade")))
	{
		return GetGrenadeAmmoAttribute();
	}

	return FGameplayAttribute();
}

FGameplayAttribute UPHAttributeSet::GetMaxAmmoAttributeFromTag(FGameplayTag& PrimaryAmmoTag)
{
	if (PrimaryAmmoTag == FGameplayTag::RequestGameplayTag(FName("Weapon.Ammo.Rifle")))
	{
		return GetMaxRifleAmmoAttribute();
	}
	else if (PrimaryAmmoTag == FGameplayTag::RequestGameplayTag(FName("Weapon.Ammo.Grenade")))
	{
		return GetMaxGrenadeAmmoAttribute();
	}

	return FGameplayAttribute();
}

void UPHAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	if (!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComp)
	{
		// Change current value to maintain the current Val / Max percent
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		const float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}

void UPHAttributeSet::OnRep_Health(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, Health, OldValue);
}

void UPHAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MaxHealth, OldValue);
}

void UPHAttributeSet::OnRep_Violations(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, Violations, OldValue);
}

void UPHAttributeSet::OnRep_MaxViolations(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MaxViolations, OldValue);
}

void UPHAttributeSet::OnRep_Energy(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, Energy, OldValue);
}

void UPHAttributeSet::OnRep_MaxEnergy(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MaxEnergy, OldValue);
}

void UPHAttributeSet::OnRep_RifleAmmo(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, RifleAmmo, OldValue);
}

void UPHAttributeSet::OnRep_MaxRifleAmmo(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MaxRifleAmmo, OldValue);
}

void UPHAttributeSet::OnRep_RifleClipAmmo(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, RifleClipAmmo, OldValue);
}

void UPHAttributeSet::OnRep_MaxRifleClipAmmo(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MaxRifleClipAmmo, OldValue);
}

void UPHAttributeSet::OnRep_GrenadeAmmo(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, GrenadeAmmo, OldValue);
}

void UPHAttributeSet::OnRep_MaxGrenadeAmmo(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MaxGrenadeAmmo, OldValue);
}

void UPHAttributeSet::OnRep_SkillUseAmount(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, SkillUseAmount, OldValue);
}

void UPHAttributeSet::OnRep_MaxSkillUseAmount(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MaxSkillUseAmount, OldValue);
}

void UPHAttributeSet::OnRep_AttackPower(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, AttackPower, OldValue);
}

void UPHAttributeSet::OnRep_DefensePower(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, DefensePower, OldValue);
}

void UPHAttributeSet::OnRep_MoveSpeed(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, MoveSpeed, OldValue);
}

void UPHAttributeSet::OnRep_XP(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, XP, OldValue);
}

void UPHAttributeSet::OnRep_XPBounty(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAttributeSet, XPBounty, OldValue);
}
