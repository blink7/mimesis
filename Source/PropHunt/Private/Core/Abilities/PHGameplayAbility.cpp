// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHGameplayAbility.h"

#include "Core/Online/PHPlayerState.h"
#include "Core/Abilities/PHTargetType.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Character/PHCharacterBase.h"


UPHGameplayAbility::UPHGameplayAbility() 
	: bActivateOnInput(true)
{
	// Default to Instance Per Actor
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	ActivationBlockedTags.AddTag(FGameplayTag::RequestGameplayTag("State.Dead"));
}

void UPHGameplayAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);

	if (bActivateAbilityOnGranted)
	{
		ActorInfo->AbilitySystemComponent->TryActivateAbility(Spec.Handle, false);
	}
}

APHPlayerState* UPHGameplayAbility::GetPlayerStateFromActorInfo() const
{
	if (!ensure(CurrentActorInfo))
	{
		return nullptr;
	}
	return Cast<APHPlayerState>(CurrentActorInfo->OwnerActor.Get());
}

APlayerController* UPHGameplayAbility::GetPlayerControllerFromActorInfo() const
{
	if (!ensure(CurrentActorInfo))
	{
		return nullptr;
	}
	return CurrentActorInfo->PlayerController.Get();
}

APHCharacterBase* UPHGameplayAbility::GetCharacterFromActorInfo() const
{
	if (!ensure(CurrentActorInfo))
	{
		return nullptr;
	}
	return Cast<APHCharacterBase>(CurrentActorInfo->AvatarActor.Get());
}

UPHAbilitySystemComponent* UPHGameplayAbility::GetPHAbilitySystemComponentFromActorInfo() const
{
	return Cast<UPHAbilitySystemComponent>(GetAbilitySystemComponentFromActorInfo());
}

UPHAbilitySystemComponent* UPHGameplayAbility::GetPHAbilitySystemComponentFromActorInfo_Checked() const
{
	auto AbilitySystemComponent = CurrentActorInfo ? Cast<UPHAbilitySystemComponent>(CurrentActorInfo->AbilitySystemComponent.Get()) : nullptr;
	check(AbilitySystemComponent);

	return AbilitySystemComponent;
}

UPHAbilitySystemComponent* UPHGameplayAbility::GetPHAbilitySystemComponentFromActorInfo_Ensured() const
{
	auto AbilitySystemComponent = CurrentActorInfo ? Cast<UPHAbilitySystemComponent>(CurrentActorInfo->AbilitySystemComponent.Get()) : nullptr;
	ensure(AbilitySystemComponent);

	return AbilitySystemComponent;
}

UObject* UPHGameplayAbility::K2_GetSourceObject(FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo) const
{
	return GetSourceObject(Handle, &ActorInfo);
}

bool UPHGameplayAbility::CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags /*= nullptr*/) const
{
	return Super::CheckCost(Handle, ActorInfo, OptionalRelevantTags) && CheckCostCustom(Handle, *ActorInfo);
}

bool UPHGameplayAbility::CheckCostCustom_Implementation(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo& ActorInfo) const
{
	return true;
}

bool UPHGameplayAbility::IsInputPressed() const
{
	FGameplayAbilitySpec* Spec = GetCurrentAbilitySpec();
	if (Spec && Spec->InputPressed)
	{
		return true;
	}

	return false;
}

FPHGameplayEffectContainerSpec UPHGameplayAbility::MakeEffectContainerSpecFromContainer(const FPHGameplayEffectContainer& Container, const FGameplayEventData& EventData, int32 OverrideGameplayLevel /*= -1*/)
{
	// First figure out our actor info
	FPHGameplayEffectContainerSpec ReturnSpec;
	AActor* OwningActor = GetOwningActorFromActorInfo();
	auto OwningPlayerState = Cast<APHPlayerState>(OwningActor);
	UPHAbilitySystemComponent* OwningASC = UPHAbilitySystemComponent::GetAbilitySystemComponentFromActor(OwningActor);

	if (OwningASC)
	{
		// If we have a target type, run the targeting logic. This is optional, targets can be added later
		if (Container.TargetType.Get())
		{
			TArray<FHitResult> HitResults;
			TArray<AActor*> TargetActors;
			const UPHTargetType* TargetTypeCDO = Container.TargetType.GetDefaultObject();
			AActor* AvatarActor = GetAvatarActorFromActorInfo();
			TargetTypeCDO->GetTargets(OwningPlayerState, AvatarActor, EventData, HitResults, TargetActors);
			ReturnSpec.AddTargets(HitResults, TargetActors);
		}

		// If we don't have an override level, use the default on the ability itself
		if (OverrideGameplayLevel == INDEX_NONE)
		{
			OverrideGameplayLevel = OverrideGameplayLevel = this->GetAbilityLevel(); //OwningASC->GetDefaultAbilityLevel();
		}

		// Build GameplayEffectSpecs for each applied effect
		for (const TSubclassOf<UGameplayEffect>& EffectClass : Container.TargetGameplayEffectClasses)
		{
			ReturnSpec.TargetGameplayEffectSpecs.Add(MakeOutgoingGameplayEffectSpec(EffectClass, OverrideGameplayLevel));
		}
	}
	return ReturnSpec;
}

FPHGameplayEffectContainerSpec UPHGameplayAbility::MakeEffectContainerSpec(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel /*= -1*/)
{
	FPHGameplayEffectContainer* FoundContainer = EffectContainerMap.Find(ContainerTag);

	if (FoundContainer)
	{
		return MakeEffectContainerSpecFromContainer(*FoundContainer, EventData, OverrideGameplayLevel);
	}
	return FPHGameplayEffectContainerSpec();
}

TArray<FActiveGameplayEffectHandle> UPHGameplayAbility::ApplyEffectContainerSpec(const FPHGameplayEffectContainerSpec& ContainerSpec)
{
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of effect specs and apply them to their target data
	for (const FGameplayEffectSpecHandle& SpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
	{
		AllEffects.Append(K2_ApplyGameplayEffectSpecToTarget(SpecHandle, ContainerSpec.TargetData));
	}
	return AllEffects;
}

TArray<FActiveGameplayEffectHandle> UPHGameplayAbility::ApplyEffectContainer(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel /*= -1*/)
{
	FPHGameplayEffectContainerSpec Spec = MakeEffectContainerSpec(ContainerTag, EventData, OverrideGameplayLevel);
	return ApplyEffectContainerSpec(Spec);
}

bool UPHGameplayAbility::BatchRPCTryActivateAbility(FGameplayAbilitySpecHandle InAbilityHandle, bool EndAbilityImmediately)
{
	UPHAbilitySystemComponent* AbilitySystemComponent = GetPHAbilitySystemComponentFromActorInfo_Checked();
	return AbilitySystemComponent->BatchRPCTryActivateAbility(InAbilityHandle, EndAbilityImmediately);
}

void UPHGameplayAbility::ExternalEndAbility()
{
	check(CurrentActorInfo);

	bool bReplicateEndAbility = true;
	bool bWasCancelled = false;
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UPHGameplayAbility::K2_ExecuteGameplayCueWithParamsLocal(FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UPHAbilitySystemComponent* AbilitySystemComponent = GetPHAbilitySystemComponentFromActorInfo_Checked();
	AbilitySystemComponent->ExecuteGameplayCueLocal(GameplayCueTag, GameplayCueParameters);
}

void UPHGameplayAbility::K2_AddGameplayCueWithParamsLocal(FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	UPHAbilitySystemComponent* AbilitySystemComponent = GetPHAbilitySystemComponentFromActorInfo_Checked();
	AbilitySystemComponent->AddGameplayCueLocal(GameplayCueTag, GameplayCueParameters);
}

void UPHGameplayAbility::K2_RemoveGameplayCueLocal(FGameplayTag GameplayCueTag)
{
	UPHAbilitySystemComponent* AbilitySystemComponent = GetPHAbilitySystemComponentFromActorInfo_Checked();
	
	FGameplayCueParameters Parameters;
	AbilitySystemComponent->InitDefaultGameplayCueParameters(Parameters);

	AbilitySystemComponent->RemoveGameplayCueLocal(GameplayCueTag, Parameters);
}
