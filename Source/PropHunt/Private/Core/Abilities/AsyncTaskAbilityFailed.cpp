// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/AsyncTaskAbilityFailed.h"


UAsyncTaskAbilityFailed* UAsyncTaskAbilityFailed::ListenForAbilityFailed(UAbilitySystemComponent* AbilitySystemComponent)
{
	UAsyncTaskAbilityFailed* WaitForAbilityFailedTask = NewObject<UAsyncTaskAbilityFailed>();
	WaitForAbilityFailedTask->ASC = AbilitySystemComponent;

	if (!IsValid(AbilitySystemComponent))
	{
		WaitForAbilityFailedTask->RemoveFromRoot();
		return nullptr;
	}

	AbilitySystemComponent->AbilityFailedCallbacks.AddUObject(WaitForAbilityFailedTask, &UAsyncTaskAbilityFailed::AbilityFailed);

	return WaitForAbilityFailedTask;
}

void UAsyncTaskAbilityFailed::EndTask()
{
	if (IsValid(ASC))
	{
		ASC->AbilityFailedCallbacks.RemoveAll(this);
	}

	SetReadyToDestroy();
	MarkPendingKill();
}

void UAsyncTaskAbilityFailed::AbilityFailed(const UGameplayAbility* Ability, const FGameplayTagContainer& FailureReason)
{
	OnAbilityFailed.Broadcast(Ability, FailureReason);
}
