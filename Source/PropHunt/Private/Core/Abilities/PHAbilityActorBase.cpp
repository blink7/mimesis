// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHAbilityActorBase.h"

#include "Core/Abilities/PHGameplayAbility.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/Abilities/PHAbilityActorAttributeSet.h"

#include "GameplayEffect.h"


APHAbilityActorBase::APHAbilityActorBase()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UPHAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Minimal mode means GameplayEffects are not replicated to anyone. Only GameplayTags and Attributes are replicated to clients.
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);

	// Create the attribute set, this replicates by default
	AttributeSet = CreateDefaultSubobject<UPHAbilityActorAttributeSet>("AttributeSet");
}

void APHAbilityActorBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	check(AbilitySystemComponent);
	if (HasAuthority())
	{
		// Grant default effects, but only on the server
		for (const TSubclassOf<UGameplayEffect>& GameplayEffect : PassiveGameplayEffects)
		{
			FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
			EffectContext.AddSourceObject(this);

			FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffect, 1, EffectContext);
			if (NewHandle.IsValid())
			{
				AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
			}
		}

		for (const TSubclassOf<UPHGameplayAbility>& GameplayAbility : Abilities)
		{
			AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(GameplayAbility, 1, INDEX_NONE, this));
		}
	}
}

UAbilitySystemComponent* APHAbilityActorBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void APHAbilityActorBase::BeginPlay()
{
	Super::BeginPlay();

	check(AbilitySystemComponent);
	AbilitySystemComponent->InitAbilityActorInfo(this, this);
}
