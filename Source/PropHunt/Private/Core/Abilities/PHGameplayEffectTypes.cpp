﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHGameplayEffectTypes.h"


bool FPHGameplayEffectContainerSpec::HasValidEffects() const
{
	return TargetGameplayEffectSpecs.Num() > 0;
}

bool FPHGameplayEffectContainerSpec::HasValidTargets() const
{
	return TargetData.Num() > 0;
}

void FPHGameplayEffectContainerSpec::AddTargets(const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors)
{
	for (const FHitResult& HitResult : HitResults)
	{
		const auto NewData = new FGameplayAbilityTargetData_SingleTargetHit(HitResult);
		TargetData.Add(NewData);
	}

	if (TargetActors.Num() > 0)
	{
		auto NewData = new FGameplayAbilityTargetData_ActorArray();
		NewData->TargetActorArray.Append(TargetActors);
		TargetData.Add(NewData);
	}
}

TArray<FActiveGameplayEffectHandle> FPHGameplayEffectContainerSpec::ApplyExternally()
{
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of gameplay effects
	for (const FGameplayEffectSpecHandle& SpecHandle : TargetGameplayEffectSpecs)
	{
		if (SpecHandle.IsValid())
		{
			// If effect is valid, iterate list of targets and apply to all
			for (const TSharedPtr<FGameplayAbilityTargetData>& Data : TargetData.Data)
			{
				AllEffects.Append(Data->ApplyGameplayEffectSpec(*SpecHandle.Data.Get()));
			}
		}
	}
	return AllEffects;
}

void FPHGameplayAbilityTargetData_SoftObject::AddTargetDataToContext(FGameplayEffectContextHandle& Context, bool bIncludeActorArray) const
{
	Super::AddTargetDataToContext(Context, bIncludeActorArray);

	FGameplayEffectContext* ContextData = Context.Get();
	if (ContextData)
	{
		auto MyContextData = static_cast<FPHGameplayEffectContext*>(ContextData);
		if (MyContextData)
		{
			MyContextData->OptionalSoftObject = OptionalSoftObject;
		}
	}
}

bool FPHGameplayAbilityTargetData_SoftObject::NetSerialize(FArchive& Ar, UPackageMap* Map, bool& bOutSuccess)
{
	Ar << OptionalSoftObject;

	bOutSuccess = true;
	return true;
}

bool FPHGameplayEffectContext::NetSerialize(FArchive& Ar, UPackageMap* Map, bool& bOutSuccess)
{
	Super::NetSerialize(Ar, Map, bOutSuccess);
	
	uint8 RepBits = 0;
	if (Ar.IsSaving())
	{
		if (OptionalSoftObject.IsValid())
		{
			RepBits |= 1 << 0;
		}
	}

	Ar.SerializeBits(&RepBits, 1);

	if (RepBits & (1 << 0))
	{
		Ar << OptionalSoftObject;
	}
	
	return true;
}
