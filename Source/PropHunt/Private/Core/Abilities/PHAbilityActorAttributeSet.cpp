// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHAbilityActorAttributeSet.h"

#include "Core/PHPlayerController.h"
#include "Core/Abilities/PHAbilityActorBase.h"

#include "Net/UnrealNetwork.h"
#include "GameplayEffectExtension.h"


UPHAbilityActorAttributeSet::UPHAbilityActorAttributeSet() : 
	PowerPanelEnergy(1.f),
	MaxPowerPanelEnergy(1.f),
	MaxPortalEnergy(1.f)
{
}

void UPHAbilityActorAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	// This is called whenever attributes change, so for max energy we want to scale the current totals to match
	Super::PreAttributeChange(Attribute, NewValue);

	if (Attribute == GetMaxPowerPanelEnergyAttribute())
	{
		AdjustAttributeForMaxChange(PowerPanelEnergy, MaxPowerPanelEnergy, NewValue, GetPowerPanelEnergyAttribute());
	}
}

void UPHAbilityActorAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// Compute the delta between old and new, if it is available
	float DeltaValue = 0;
	if (Data.EvaluatedData.ModifierOp == EGameplayModOp::Type::Additive)
	{
		// If this was additive, store the raw delta value to be passed along later
		DeltaValue = Data.EvaluatedData.Magnitude;
	}

	// Get the Target actor, which should be our owner
	APHAbilityActorBase* TargetActor = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Cast<APHAbilityActorBase>(Data.Target.AbilityActorInfo->OwnerActor.Get());
	}

	// Get the Source actor
	AActor* SourceActor = nullptr;
	APHPlayerController* SourceController = nullptr;
	APawn* SourcePawn = nullptr;
	if (Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->PlayerController.IsValid())
	{
		SourceActor = Source->AbilityActorInfo->OwnerActor.Get();
		SourcePawn = Cast<APawn>(Source->AbilityActorInfo->AvatarActor.Get());
		SourceController = Cast<APHPlayerController>(Source->AbilityActorInfo->PlayerController.Get());

		if (!SourceController && SourcePawn)
		{
			SourceController = SourcePawn->GetController<APHPlayerController>();
		}

		// Set the causer actor based on context if it's set
		if (Context.GetEffectCauser())
		{
			SourceActor = Context.GetEffectCauser();
		}
	}

	const FGameplayAttribute& ModifiedAttribute = Data.EvaluatedData.Attribute;
	if (ModifiedAttribute == GetPowerPanelEnergyAttribute())
	{
		if (TargetActor)
		{
			TargetActor->HandlePowerPanelEnergyChanged(SourceController, DeltaValue);
		}
	}
	else if (ModifiedAttribute == GetPortalEnergyAttribute())
	{
		if (TargetActor)
		{
			TargetActor->HandlePortalEnergyChanged(SourceController, DeltaValue);
		}
	}
}

void UPHAbilityActorAttributeSet::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UPHAbilityActorAttributeSet, PowerPanelEnergy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAbilityActorAttributeSet, MaxPowerPanelEnergy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAbilityActorAttributeSet, PortalEnergy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UPHAbilityActorAttributeSet, MaxPortalEnergy, COND_None, REPNOTIFY_Always);
}

void UPHAbilityActorAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	if (!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComp)
	{
		// Change current value to maintain the current Val / Max percent
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		const float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}

void UPHAbilityActorAttributeSet::OnRep_PowerPanelEnergy(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAbilityActorAttributeSet, PowerPanelEnergy, OldValue);
}

void UPHAbilityActorAttributeSet::OnRep_MaxPowerPanelEnergy(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAbilityActorAttributeSet, MaxPowerPanelEnergy, OldValue);
}

void UPHAbilityActorAttributeSet::OnRep_PortalEnergy(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAbilityActorAttributeSet, PortalEnergy, OldValue);
}

void UPHAbilityActorAttributeSet::OnRep_MaxPortalEnergy(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UPHAbilityActorAttributeSet, MaxPortalEnergy, OldValue);
}
