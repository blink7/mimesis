// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHTargetType.h"

#include "Core/Online/PHPlayerState.h"


void UPHTargetType::GetTargets_Implementation(APHPlayerState* TargetingPlayerState, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
}

void UPHTargetType_UseOwner::GetTargets_Implementation(APHPlayerState* TargetingPlayerState, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	OutActors.Add(TargetingActor);
}

void UPHTargetType_UseEventData::GetTargets_Implementation(APHPlayerState* TargetingPlayerState, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	const FHitResult* FoundHitResult = EventData.ContextHandle.GetHitResult();
	if (FoundHitResult)
	{
		OutHitResults.Add(*FoundHitResult);
	}
	else if (EventData.Target)
	{
		OutActors.Add(const_cast<AActor*>(EventData.Target));
	}
}
