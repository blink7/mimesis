// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHGameplayAbilityTargetActor_ProjectileTrace.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/GameplayStaticsTypes.h"


void APHGameplayAbilityTargetActor_ProjectileTrace::Tick(float DeltaSeconds)
{
	// very temp - do a mostly hardcoded trace from the source actor
	if (SourceActor)
	{
		FHitResult HitResult = PerformTrace(SourceActor);
		FVector EndPoint = HitResult.Location;

#if ENABLE_DRAW_DEBUG
		if (bDebug)
		{
			DrawDebugLine(GetWorld(), SourceActor->GetActorLocation(), EndPoint, FColor::Green, false);
			DrawDebugSphere(GetWorld(), EndPoint, 16, 10, FColor::Green, false);
		}
#endif // ENABLE_DRAW_DEBUG

		SetActorLocationAndRotation(EndPoint, SourceActor->GetActorRotation());

		VisualizeTrace(SourceActor->GetWorld(), HitResult.TraceStart, EndPoint, ProjectileSpeed, ProjectileRadius);
	}
}

FHitResult APHGameplayAbilityTargetActor_ProjectileTrace::PerformTrace(AActor* InSourceActor)
{
	bool bTraceComplex = false;
	TArray<AActor*> ActorsToIgnore;

	ActorsToIgnore.Add(InSourceActor);

	FCollisionQueryParams Params(SCENE_QUERY_STAT(APHGameplayAbilityTargetActor_ProjectileTrace), bTraceComplex);
	Params.bReturnPhysicalMaterial = true;
	Params.AddIgnoredActors(ActorsToIgnore);

	FVector TraceStart;
	FVector TraceEnd;
	if (MasterPC && bTraceFromPlayerViewPoint)
	{
		FRotator ViewRot;
		MasterPC->GetPlayerViewPoint(TraceStart, ViewRot);
	}
	else
	{
		TraceStart = StartLocation.GetTargetingTransform().GetLocation();// InSourceActor->GetActorLocation();
	}

	AimWithPlayerController(InSourceActor, Params, TraceStart, TraceEnd);		//Effective on server and launching client only

	// ------------------------------------------------------

	FPredictProjectilePathParams PredictParams;
	PredictParams.StartLocation = TraceStart;
	PredictParams.LaunchVelocity = (TraceEnd - TraceStart).GetSafeNormal() * ProjectileSpeed;
	PredictParams.ProjectileRadius = ProjectileRadius;
	PredictParams.bTraceWithCollision = true;
	PredictParams.TraceChannel = TraceChannel;
	PredictParams.ActorsToIgnore = ActorsToIgnore;
#if ENABLE_DRAW_DEBUG
	if (bDebug)
	{
		PredictParams.DrawDebugType = EDrawDebugTrace::ForOneFrame;
	}
#endif // ENABLE_DRAW_DEBUG

	FPredictProjectilePathResult PredictResult;
	UGameplayStatics::PredictProjectilePath(InSourceActor->GetWorld(), PredictParams, PredictResult);

	FHitResult ReturnHitResult = PredictResult.HitResult;

	//Default to end of trace line if we don't hit anything.
	if (!ReturnHitResult.bBlockingHit)
	{
		ReturnHitResult.Location = PredictResult.LastTraceDestination.Location;
	}
	if (AGameplayAbilityWorldReticle* LocalReticleActor = ReticleActor.Get())
	{
		const bool bHitActor = ReturnHitResult.bBlockingHit && (ReturnHitResult.Actor != nullptr);
		const FVector ReticleLocation = (bHitActor && LocalReticleActor->bSnapToTargetedActor) ? ReturnHitResult.Actor->GetActorLocation() : ReturnHitResult.Location;

		LocalReticleActor->SetActorLocation(ReticleLocation);
		LocalReticleActor->SetIsTargetAnActor(bHitActor);
	}

	// Start param could be player ViewPoint. We want HitResult to always display the StartLocation.
	ReturnHitResult.TraceStart = StartLocation.GetTargetingTransform().GetLocation();

	return ReturnHitResult;
}

void APHGameplayAbilityTargetActor_ProjectileTrace::VisualizeTrace(const UObject* WorldContextObject, FVector Start, FVector End, float TossSpeed, float CollisionRadius /*= 0.f*/)
{
	const FVector FlightDelta = End - Start;
	const FVector DirXY = FlightDelta.GetSafeNormal2D();
	const float DeltaXY = FlightDelta.Size2D();

	const float DeltaZ = FlightDelta.Z;

	const float TossSpeedSq = FMath::Square(TossSpeed);

	const UWorld* const World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if (!World)
	{
		return;
	}
	const float GravityZ = -World->GetGravityZ();

	// v^4 - g*(g*x^2 + 2*y*v^2)
	const float InsideTheSqrt = FMath::Square(TossSpeedSq) - GravityZ * ((GravityZ * FMath::Square(DeltaXY)) + (2.f * DeltaZ * TossSpeedSq));
	if (InsideTheSqrt < 0.f)
	{
		// sqrt will be imaginary, therefore no solutions
		return;
	}

	// if we got here, there are 2 solutions: one high-angle and one low-angle.

	const float SqrtPart = FMath::Sqrt(InsideTheSqrt);

	// this is the tangent of the firing angle for the first (+) solution
	const float TanSolutionAngleA = (TossSpeedSq + SqrtPart) / (GravityZ * DeltaXY);
	// this is the tangent of the firing angle for the second (-) solution
	const float TanSolutionAngleB = (TossSpeedSq - SqrtPart) / (GravityZ * DeltaXY);

	// mag in the XY dir = sqrt( TossSpeedSq / (TanSolutionAngle^2 + 1) );
	const float MagXYSq_A = TossSpeedSq / (FMath::Square(TanSolutionAngleA) + 1.f);
	const float MagXYSq_B = TossSpeedSq / (FMath::Square(TanSolutionAngleB) + 1.f);

	// choose which arc
	const float FavoredMagXYSq = FMath::Max(MagXYSq_A, MagXYSq_B);
	const float ZSign = (MagXYSq_A > MagXYSq_B) ? FMath::Sign(TanSolutionAngleA) : FMath::Sign(TanSolutionAngleB);

	// finish calculations
	const float MagXY = FMath::Sqrt(FavoredMagXYSq);
	const float MagZ = FMath::Sqrt(TossSpeedSq - FavoredMagXYSq);		// pythagorean

	// final answer!
	FVector TossVelocity = (DirXY * MagXY) + (FVector::UpVector * MagZ * ZSign);

	TArray<FVector> ProjectilePathPositions;

	static const float StepSize = 0.125f;
	FVector TraceStart = Start;
	for (float Step = 0.f; Step < 1.f; Step += StepSize)
	{
		const float TimeInFlight = (Step + StepSize) * DeltaXY / MagXY;

		// d = vt + .5 a t^2
		const FVector TraceEnd = Start + TossVelocity * TimeInFlight + FVector(0.f, 0.f, 0.5f * -GravityZ * FMath::Square(TimeInFlight) - CollisionRadius);

#if ENABLE_DRAW_DEBUG
		if (bDebug)
		{
			DrawDebugLine(World, TraceStart, TraceEnd, FColor::Yellow, true);
		}
#endif // ENABLE_DRAW_DEBUG

		ProjectilePathPositions.Add(TraceStart);

		TraceStart = TraceEnd;
	}

	K2_VisualizeTrace(ProjectilePathPositions);
}
