// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/Tasks/PHAbilityTask_WaitChangeFOV.h"

#include "Camera/PlayerCameraManager.h"


UPHAbilityTask_WaitChangeFOV::UPHAbilityTask_WaitChangeFOV()
{
	bTickingTask = true;
}

void UPHAbilityTask_WaitChangeFOV::TickTask(float DeltaTime)
{
	if (bIsFinished)
	{
		return;
	}

	Super::TickTask(DeltaTime);

	if (CameraManager)
	{
		float CurrentTime = GetWorld()->GetTimeSeconds();

		if (CurrentTime >= TimeChangeWillEnd)
		{
			bIsFinished = true;

			CameraManager->SetFOV(TargetFOV);

			if (ShouldBroadcastAbilityTaskDelegates())
			{
				OnTargetFOVReached.Broadcast();
			}
			EndTask();
		}
		else
		{
			float MoveFraction = (CurrentTime - TimeChangeStarted) / Duration;
			if (LerpCurve)
			{
				MoveFraction = LerpCurve->GetFloatValue(MoveFraction);
			}

			const float NewFOV = FMath::Lerp(StartFOV, TargetFOV, MoveFraction);
			CameraManager->SetFOV(NewFOV);
		}
	}
	else
	{
		bIsFinished = true;
		EndTask();
	}
}

UPHAbilityTask_WaitChangeFOV* UPHAbilityTask_WaitChangeFOV::WaitChangeFOV(UGameplayAbility* OwningAbility, FName TaskInstanceName, APlayerCameraManager* CameraManager, float TargetFOV, float Duration /*= 0.1f*/, UCurveFloat* OptionalInterpolationCurve /*= nullptr*/)
{
	auto MyObj = NewAbilityTask<UPHAbilityTask_WaitChangeFOV>(OwningAbility, TaskInstanceName);

	MyObj->CameraManager = CameraManager;
	if (CameraManager)
	{
		MyObj->StartFOV = MyObj->CameraManager->GetFOVAngle();
	}

	MyObj->TargetFOV = TargetFOV;
	MyObj->Duration = FMath::Max(Duration, 0.001f); // Avoid negative or divide-by-zero cases
	MyObj->TimeChangeStarted = MyObj->GetWorld()->GetTimeSeconds();
	MyObj->TimeChangeWillEnd = MyObj->TimeChangeStarted + MyObj->Duration;
	MyObj->LerpCurve = OptionalInterpolationCurve;

	return MyObj;
}
