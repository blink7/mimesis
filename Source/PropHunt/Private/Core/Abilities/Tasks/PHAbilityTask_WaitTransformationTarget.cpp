// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/Tasks/PHAbilityTask_WaitTransformationTarget.h"

#include "Enviroment/PhysicsProp.h"
#include "Enviroment/TransformationBlockVolume.h"


UPHAbilityTask_WaitTransformationTarget* UPHAbilityTask_WaitTransformationTarget::WaitForTransformationTarget(UGameplayAbility* OwningAbility, FName TaskInstanceName, ECollisionChannel TraceChannel, float MaxRange /*= 200.f*/, float TimerPeriod /*= 0.1f*/, bool bShowDebug /*= false*/)
{
	auto MyObj = NewAbilityTask<UPHAbilityTask_WaitTransformationTarget>(OwningAbility, TaskInstanceName);		//Register for task list here, providing a given FName as a key
	MyObj->TraceChannel = TraceChannel;
	MyObj->MaxRange = MaxRange;
	MyObj->TimerPeriod = TimerPeriod;
	MyObj->bShowDebug = bShowDebug;

	return MyObj;
}

void UPHAbilityTask_WaitTransformationTarget::LineTrace(FHitResult& OutHitResult, const UWorld* World, const FVector& Start, const FVector& End, ECollisionChannel ChannelName, const FCollisionQueryParams Params, bool bLookForInteractableActor) const
{
	check(World);

	TArray<FHitResult> HitResults;
	World->LineTraceMultiByChannel(HitResults, Start, End, ChannelName, Params);

	OutHitResult.TraceStart = Start;
	OutHitResult.TraceEnd = End;

	for (const FHitResult& Hit : HitResults)
	{
		if (!Hit.Actor.IsValid() || Hit.Actor != Ability->GetAvatarActorFromActorInfo())
		{
			// If bLookForInteractableActor is false, we're looking for an endpoint to trace to
			if (bLookForInteractableActor && Hit.Actor.IsValid())
			{
				TSet<AActor*> OverlappingActors;
				Ability->GetAvatarActorFromActorInfo()->GetOverlappingActors(OverlappingActors, ATransformationBlockVolume::StaticClass());
				const bool bAllowedZone = OverlappingActors.Num() == 0;

				if (bAllowedZone && Hit.Component.IsValid() && Hit.Actor->IsA(APhysicsProp::StaticClass()))
				{
					OutHitResult = Hit;
					OutHitResult.bBlockingHit = true; // treat it as a blocking hit
					return;
				}

				OutHitResult.TraceEnd = Hit.Location;
				OutHitResult.bBlockingHit = false; // False means it isn't valid to interact with
				return;
			}

			// This is for the first line trace to get an end point to trace to
			// !Hit.Actor.IsValid() implies we didn't hit anything so return the endpoint as a blocking hit
			// Or if we hit something else
			OutHitResult = Hit;
			OutHitResult.bBlockingHit = true; // treat it as a blocking hit
			return;
		}
	}
}
