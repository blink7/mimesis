// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHGameplayCueNotify_Spray.h"

#include "Core/Online/PHPlayerState.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/Texture2DDynamic.h"
#include "Components/DecalComponent.h"

APHGameplayCueNotify_Spray::APHGameplayCueNotify_Spray()
{
	Decal = CreateDefaultSubobject<UDecalComponent>("Decal");
	Decal->DecalSize = FVector(1.f, 48.f, 48.f);
	RootComponent = Decal;
}

void APHGameplayCueNotify_Spray::HandleGameplayCue(AActor* MyTarget, EGameplayCueEvent::Type EventType, const FGameplayCueParameters& Parameters)
{
	Super::HandleGameplayCue(MyTarget, EventType, Parameters);

	if (EventType == EGameplayCueEvent::Executed)
	{
		SetActorHiddenInGame(true);

		const auto TargetPawn = Cast<APawn>(MyTarget);
		if (!TargetPawn)
		{
			return;
		}
		
		if (TargetPawn->IsLocallyControlled())
		{
			UGameplayStatics::PlaySound2D(this, SpraySound);
		}
		else
		{
			UGameplayStatics::PlaySoundAtLocation(this, SpraySound, GetActorLocation());
		}

		const auto DefaultSprayTexture = Cast<UTexture2D>(const_cast<UObject*>(Parameters.GetSourceObject()));
		if (DefaultSprayTexture)
		{
			SetSprayTexture(DefaultSprayTexture);
		}
		else
		{
			const auto TargetPS = TargetPawn->GetPlayerState<APHPlayerState>();
			if (TargetPS)
			{
				TargetPS->LoadPlayerSpray([this](UTexture2D* NewTexture)
				{
					SetSprayTexture(NewTexture);
				});
			}
		}
	}
}

void APHGameplayCueNotify_Spray::SetSprayTexture(UTexture2D* NewTexture)
{
	if (!NewTexture)
	{
		SetActorHiddenInGame(true);
		return;
	}
	
	if (!DynamicSprayMaterial)
	{
		DynamicSprayMaterial = Decal->CreateDynamicMaterialInstance();
	}
	DynamicSprayMaterial->SetTextureParameterValue("Texture", NewTexture);

	SetActorHiddenInGame(false);
}
