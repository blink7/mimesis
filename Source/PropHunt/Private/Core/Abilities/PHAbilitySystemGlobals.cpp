// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHAbilitySystemGlobals.h"


void UPHAbilitySystemGlobals::InitGlobalTags()
{
	Super::InitGlobalTags();

	if (DeadName != NAME_None)
	{
		DeadTag = FGameplayTag::RequestGameplayTag(DeadName);
	}

	if (InteractingName != NAME_None)
	{
		InteractingTag = FGameplayTag::RequestGameplayTag(InteractingName);
	}

	if (InteractingRemovalName != NAME_None)
	{
		InteractingRemovalTag = FGameplayTag::RequestGameplayTag(InteractingRemovalName);
	}
}
