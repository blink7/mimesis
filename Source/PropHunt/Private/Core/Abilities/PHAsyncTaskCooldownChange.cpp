﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Abilities/PHAsyncTaskCooldownChange.h"

#include "AbilitySystemComponent.h"

UAsyncTaskCooldownChange* UAsyncTaskCooldownChange::ListenForCooldownChange(UAbilitySystemComponent* AbilitySystemComponent, FGameplayTagContainer CooldownTags)
{
	const auto MyObj = NewObject<UAsyncTaskCooldownChange>();

	if (!IsValid(AbilitySystemComponent) || CooldownTags.Num() == 0)
	{
		MyObj->EndTask();
		return nullptr;
	}
	
	MyObj->ASC = AbilitySystemComponent;
	MyObj->Tags = CooldownTags;

	for (auto TagIterator = CooldownTags.CreateConstIterator(); TagIterator; ++TagIterator)
	{
		AbilitySystemComponent->RegisterGameplayTagEvent(*TagIterator, EGameplayTagEventType::NewOrRemoved).AddUObject(MyObj, &UAsyncTaskCooldownChange::TagChanged);
	}

	return MyObj;
}

void UAsyncTaskCooldownChange::EndTask()
{
	if (IsValid(ASC))
	{
		for (auto TagIterator = Tags.CreateConstIterator(); TagIterator; ++TagIterator)
		{
			ASC->RegisterGameplayTagEvent(*TagIterator, EGameplayTagEventType::NewOrRemoved).RemoveAll(this);
		}
	}

	SetReadyToDestroy();
	MarkPendingKill();
}

void UAsyncTaskCooldownChange::TagChanged(const FGameplayTag CooldownTag, int32 NewCount)
{
	float TimeRemaining = 0.f;
	float Duration = 0.f;
	
	if (NewCount > 0)
	{
		FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(FGameplayTagContainer(CooldownTag));
		TArray<TPair<float, float>> DurationAndTimeRemaining = ASC->GetActiveEffectsTimeRemainingAndDuration(Query);
		if (DurationAndTimeRemaining.Num() > 0)
		{
			int32 BestIdx = 0;
			float LongestTime = DurationAndTimeRemaining[0].Key;
			for (int32 Idx = 1; Idx < DurationAndTimeRemaining.Num(); ++Idx)
			{
				if (DurationAndTimeRemaining[Idx].Key > LongestTime)
				{
					LongestTime = DurationAndTimeRemaining[Idx].Key;
					BestIdx = Idx;
				}
			}

			TimeRemaining = DurationAndTimeRemaining[BestIdx].Key;
			Duration = DurationAndTimeRemaining[BestIdx].Value;
		}
		
		OnBegin.Broadcast(CooldownTag, TimeRemaining, Duration);
	}
	else if (NewCount == 0)
	{
		FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(FGameplayTagContainer(CooldownTag));
		TArray<float> CooldownDuration = ASC->GetActiveEffectsDuration(Query);
		if (CooldownDuration.Num() > 0)
		{
			Duration = CooldownDuration[0];
		}
		
		OnEnd.Broadcast(CooldownTag, TimeRemaining, Duration);
	}
}
