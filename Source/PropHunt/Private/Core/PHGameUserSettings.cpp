// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHGameUserSettings.h"

#include "Engine/Engine.h"
#include "HAL/IConsoleManager.h"
#include "Internationalization/Culture.h"

#include "AkAudioDevice.h"


static TAutoConsoleVariable<float> CVarFirstPersonFOV(
	TEXT("r.FirstPersonFOV"),
	90.f,
	TEXT("Set Field of View for Hunter's camera\n")
	TEXT(" 70..130, default: 90"),
	ECVF_Default
);

static TAutoConsoleVariable<float> CVarThirdPersonFOV(
	TEXT("r.ThirdPersonFOV"),
	90.f,
	TEXT("Set Field of View for Prop's camera\n")
	TEXT(" 70..150, default: 90"),
	ECVF_Default
);

UPHGameUserSettings::UPHGameUserSettings(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	UPHGameUserSettings::SetToDefaults();
}

void UPHGameUserSettings::ApplySettings(bool bCheckForCommandLineOverrides)
{
	ApplySoundSettings();

	Super::ApplySettings(bCheckForCommandLineOverrides);
}

void UPHGameUserSettings::ApplyNonResolutionSettings()
{
	Super::ApplyNonResolutionSettings();

	GEngine->DisplayGamma = GetGamma();

	CVarFirstPersonFOV.AsVariable()->Set(FirstPersonFOV, ECVF_SetByGameSetting);
	CVarThirdPersonFOV.AsVariable()->Set(ThirdPersonFOV, ECVF_SetByGameSetting);
}

void UPHGameUserSettings::ApplySoundSettings()
{
	FAkAudioDevice* AudioDevice = FAkAudioDevice::Get();
	if (AudioDevice)
	{
		AudioDevice->SetRTPCValue(TEXT("MasterVolume"), MasterSoundVolume, 0, nullptr);
		AudioDevice->SetRTPCValue(TEXT("AmbientVolume"), AmbientSoundVolume, 0, nullptr);
		AudioDevice->SetRTPCValue(TEXT("EffectsVolume"), EffectsSoundVolume, 0, nullptr);
		AudioDevice->SetRTPCValue(TEXT("MusicVolume"), MusicSoundVolume, 0, nullptr);
		AudioDevice->SetRTPCValue(TEXT("MenuVolume"), MenuSoundVolume, 0, nullptr);
		AudioDevice->SetRTPCValue(TEXT("VoiceVolume"), VoiceSoundVolume, 0, nullptr);
	}
}

void UPHGameUserSettings::SetToDefaults()
{
	SetGraphicsToDefaults();
	SetAudioToDefaults();
	SetGameToDefault();

	Spray = "default";
}

void UPHGameUserSettings::SetGraphicsToDefaults()
{
	Super::SetToDefaults();

	FrameRateLimit = 60.f;

	Gamma = 2.2f;
	FirstPersonFOV = 90.f;
	ThirdPersonFOV = 90.f;
}

void UPHGameUserSettings::SetAudioToDefaults()
{
	bLanMatch = false;
	bDedicatedServer = false;
	MasterSoundVolume = 1.f;
	AmbientSoundVolume = 1.f;
	EffectsSoundVolume = 1.f;
	MusicSoundVolume = 1.f;
	MenuSoundVolume = 1.f;
	VoiceSoundVolume = 1.f;
}

void UPHGameUserSettings::SetGameToDefault()
{
	TauntLang = FInternationalization::Get().GetCurrentLanguage()->GetName();
}

bool UPHGameUserSettings::IsGammaDirty() const
{
	bool bDirty = false;
	if (GEngine && GEngine->GameViewport && GEngine->GameViewport->ViewportFrame)
	{
		bDirty = (Gamma != GEngine->GetDisplayGamma());
	}
	return bDirty;
}

bool UPHGameUserSettings::IsFirstPersonFOVDirty() const
{
	return CVarFirstPersonFOV.AsVariable()->GetFloat() != FirstPersonFOV;
}

bool UPHGameUserSettings::IsThirdPersonFOVDirty() const
{
	return CVarThirdPersonFOV.AsVariable()->GetFloat() != ThirdPersonFOV;
}

bool UPHGameUserSettings::IsDirty() const
{
	return IsGammaDirty() || IsFirstPersonFOVDirty() || IsThirdPersonFOVDirty() || Super::IsDirty();
}

UPHGameUserSettings* UPHGameUserSettings::GetPHGameUserSettings()
{
	return CastChecked<UPHGameUserSettings>(Super::GetGameUserSettings());
}
