// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/HttpClient.h"


DEFINE_LOG_CATEGORY(LogHttpClient);

void UHttpClient::SetAuthorizationHash(FString Hash, HttpRequestRef& Request)
{
	Request->SetHeader(AuthorizationHeader, Hash);
}

HttpRequestRef UHttpClient::RequestWithRoute(FString Subroute)
{
	HttpRequestRef Request = FHttpModule::Get().CreateRequest();
	Request->SetURL(ApiBaseUrl + Subroute);
	SetRequestHeaders(Request);
	return Request;
}

void UHttpClient::SetRequestHeaders(HttpRequestRef& Request)
{
	Request->SetHeader("User-Agent", "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", "application/json");
	Request->SetHeader("Accepts", "application/json");
}

HttpRequestRef UHttpClient::GetRequest(FString Subroute)
{
	HttpRequestRef Request = RequestWithRoute(Subroute);
	Request->SetVerb("GET");
	UE_LOG(LogHttpClient, Verbose, TEXT("Processing %s %s"), *Request->GetVerb(), *Request->GetURL());
	return Request;
}

HttpRequestRef UHttpClient::PostRequest(FString Subroute, FString ContentJsonString)
{
	HttpRequestRef Request = RequestWithRoute(Subroute);
	Request->SetVerb("POST");
	Request->SetContentAsString(ContentJsonString);
	UE_LOG(LogHttpClient, Verbose, TEXT("Processing %s %s \n\r %s"), *Request->GetVerb(), *Request->GetURL(), *ContentJsonString);
	return Request;
}

HttpRequestRef UHttpClient::PutRequest(FString Subroute, FString ContentJsonString)
{
	HttpRequestRef Request = RequestWithRoute(Subroute);
	Request->SetVerb("PUT");
	Request->SetContentAsString(ContentJsonString);
	UE_LOG(LogHttpClient, Verbose, TEXT("Processing %s %s \n\r %s"), *Request->GetVerb(), *Request->GetURL(), *ContentJsonString);
	return Request;
}

HttpRequestRef UHttpClient::PutRequest(FString Subroute, const TArray<uint8>& ContentPayload)
{
	HttpRequestRef Request = RequestWithRoute(Subroute);
	Request->SetVerb("PUT");
	Request->SetContent(ContentPayload);
	UE_LOG(LogHttpClient, Verbose, TEXT("Processing %s %s"), *Request->GetVerb(), *Request->GetURL());
	return Request;
}

void UHttpClient::Send(const HttpRequestRef& Request)
{
	Request->ProcessRequest();
}

bool UHttpClient::ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (!bWasSuccessful || !Response.IsValid()) return false;

	if (EHttpResponseCodes::IsOk(Response->GetResponseCode()))
	{
		return true;
	}
	else
	{
		UE_LOG(LogHttpClient, Warning, TEXT("Http Response returned error code: %d \n\r %s"), Response->GetResponseCode(), *Response->GetContentAsString());
		return false;
	}
}
