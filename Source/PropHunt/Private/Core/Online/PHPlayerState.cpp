// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHPlayerState.h"

#include "PropHunt/PropHunt.h"
#include "Core/PHTypes.h"
#include "Core/PHGameInstance.h"
#include "Core/PHPlayerController.h"
#include "Core/PHGameUserSettings.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHSprayHttpClient.h"
#include "Core/Online/PHPlayerHttpClient.h"
#include "Core/Online/PHStatisticsHttpClient.h"
#include "Core/Abilities/PHAttributeSet.h"
#include "Core/Abilities/PHGameplayAbility.h"
#include "Core/Abilities/PHAbilitySystemGlobals.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/Items/PHItem.h"
#include "Core/Items/PHTauntDataAsset.h"
#include "Character/PHCharacterBase.h"

#include "EngineUtils.h"
#include "GameplayEffect.h"
#include "GameplayEffectUIData.h"
#include "Engine/World.h"
#include "Engine/LocalPlayer.h"
#include "GameFramework/Pawn.h"
#include "Net/UnrealNetwork.h"

APHPlayerState::APHPlayerState() : 
	NextMapVote(-1),
	TeamNumber(ETeamType::None),
	DesiredTeamNumber(ETeamType::None),
	PlayerLevel(1),
	NeededXP(1000),
	CharacterLevel(1)
{
	NetUpdateFrequency = 100.f;

	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UPHAbilitySystemComponent>("AbilitySystemComponent");
	AbilitySystemComponent->SetIsReplicated(true);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	// Create the attribute set, this replicates by default
	AttributeSet = CreateDefaultSubobject<UPHAttributeSet>("AttributeSet");
}

void APHPlayerState::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	TimeSinceTeamChanged = GetStartTime();

	AddCommonStartupGameplayAbilities();

	InteractingTag = UPHAbilitySystemGlobals::PHGet().InteractingTag;
	InteractingRemovalTag = UPHAbilitySystemGlobals::PHGet().InteractingRemovalTag;
	EffectRemoveOnDeathTag = FGameplayTag::RequestGameplayTag("Effect.RemoveOnDeath");
}

void APHPlayerState::UnregisterPlayerWithSession()
{
	if (!IsFromPreviousLevel())
	{
		Super::UnregisterPlayerWithSession();
	}
}

void APHPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APHPlayerState, CharacterLevel);
	DOREPLIFETIME(APHPlayerState, HunterScore);
	DOREPLIFETIME(APHPlayerState, PropScore);
	DOREPLIFETIME(APHPlayerState, Kills);
	DOREPLIFETIME(APHPlayerState, Deaths);
	DOREPLIFETIME(APHPlayerState, Taunts);
	DOREPLIFETIME(APHPlayerState, Morphs);
	DOREPLIFETIME_CONDITION(APHPlayerState, SurvivalTime, COND_OwnerOnly);
	DOREPLIFETIME(APHPlayerState, Wins);
	DOREPLIFETIME_CONDITION(APHPlayerState, Losses, COND_OwnerOnly);
	DOREPLIFETIME(APHPlayerState, PlayerLevel);
	DOREPLIFETIME_CONDITION(APHPlayerState, CurrentXP, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(APHPlayerState, NeededXP, COND_OwnerOnly);

	DOREPLIFETIME(APHPlayerState, TeamNumber);
	DOREPLIFETIME(APHPlayerState, TimeSinceTeamChanged);

	DOREPLIFETIME(APHPlayerState, bReady);
	DOREPLIFETIME(APHPlayerState, NextMapVote);

	DOREPLIFETIME_CONDITION(APHPlayerState, CharacterSkillId, COND_OwnerOnly);
	
	DOREPLIFETIME_CONDITION(APHPlayerState, bSprayDirty, COND_Custom);
}

void APHPlayerState::PreReplication(IRepChangedPropertyTracker& ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);
	
	DOREPLIFETIME_ACTIVE_OVERRIDE(APHPlayerState, bSprayDirty, bSprayDirty == true);
}

void APHPlayerState::OverrideWith(APlayerState* PlayerState)
{
	Super::OverrideWith(PlayerState);

	if (const auto PassedPlayerState = Cast<APHPlayerState>(PlayerState))
	{
		HunterScore = PassedPlayerState->HunterScore;
		PropScore = PassedPlayerState->PropScore;
		Kills = PassedPlayerState->Kills;
		Deaths = PassedPlayerState->Deaths;
		Taunts = PassedPlayerState->Taunts;
		Morphs = PassedPlayerState->Morphs;
		SurvivalTime = PassedPlayerState->SurvivalTime;
		Wins = PassedPlayerState->Wins;
		Losses = PassedPlayerState->Losses;
		CharacterLevel = PassedPlayerState->CharacterLevel;
		PlayerLevel = PassedPlayerState->PlayerLevel;
		CurrentXP = PassedPlayerState->CurrentXP;
		NeededXP = PassedPlayerState->NeededXP;
		HunterSkillId = PassedPlayerState->HunterSkillId;
		PropSkillId = PassedPlayerState->PropSkillId;
		CustomWeaponId = PassedPlayerState->CustomWeaponId;
		bUserDataLoaded = PassedPlayerState->bUserDataLoaded;

		OnLoadPlayerCompleteDelegate.ExecuteIfBound();

		TeamNumber = PassedPlayerState->TeamNumber;
		TimeSinceTeamChanged = PassedPlayerState->TimeSinceTeamChanged;
		DesiredTeamNumber = PassedPlayerState->DesiredTeamNumber;

		OnTeamChanged.ExecuteIfBound();
	}
}

void APHPlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	if (const auto PassedPlayerState = Cast<APHPlayerState>(PlayerState))
	{
		PassedPlayerState->HunterScore = HunterScore;
		PassedPlayerState->PropScore = PropScore;
		PassedPlayerState->Kills = Kills;
		PassedPlayerState->Deaths = Deaths;
		PassedPlayerState->Taunts = Taunts;
		PassedPlayerState->Morphs = Morphs;
		PassedPlayerState->SurvivalTime = SurvivalTime;
		PassedPlayerState->Wins = Wins;
		PassedPlayerState->Losses = Losses;
		PassedPlayerState->CharacterLevel = CharacterLevel;
		PassedPlayerState->PlayerLevel = PlayerLevel;
		PassedPlayerState->CurrentXP = CurrentXP;
		PassedPlayerState->NeededXP = NeededXP;
		PassedPlayerState->HunterSkillId = HunterSkillId;
		PassedPlayerState->PropSkillId = PropSkillId;
		PassedPlayerState->CustomWeaponId = CustomWeaponId;
		PassedPlayerState->bUserDataLoaded = bUserDataLoaded;

		PassedPlayerState->TeamNumber = TeamNumber;
		PassedPlayerState->TimeSinceTeamChanged = TimeSinceTeamChanged;

		PassedPlayerState->DesiredTeamNumber = DesiredTeamNumber;
	}
}

void APHPlayerState::ClientInitialize(AController* C)
{
	Super::ClientInitialize(C);

	if (const auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		GameState->OnPlayerStateChanged.Broadcast(this);
	}
}

bool APHPlayerState::IsHunter() const
{
	return TeamNumber == ETeamType::Hunter;
}

bool APHPlayerState::IsProp() const
{
	return TeamNumber == ETeamType::Prop;
}

bool APHPlayerState::IsInteracting() const
{
	check(AbilitySystemComponent);
	return AbilitySystemComponent->GetTagCount(InteractingTag) > AbilitySystemComponent->GetTagCount(InteractingRemovalTag);
}

FPrimaryAssetId APHPlayerState::GetSkillId(int32 ForTeam) const
{
	switch (ForTeam)
	{
		case ETeamType::Hunter:
			return HunterSkillId;
		case ETeamType::Prop:
			return PropSkillId;
		default:
			return FPrimaryAssetId();
	}
}

void APHPlayerState::SetSkillId(FPrimaryAssetId Id, int32 ForTeam)
{
	bool bDirty = false;

	if (ForTeam == ETeamType::Hunter)
	{
		if (HunterSkillId != Id)
		{
			HunterSkillId = Id;
			bDirty = true;
		}
	}
	else if (ForTeam == ETeamType::Prop)
	{
		if (PropSkillId != Id)
		{
			PropSkillId = Id;
			bDirty = true;
		}
	}

	if (bDirty)
	{
		SaveAbilities();
	}
}

void APHPlayerState::SaveAbilities()
{
	auto GameInstance = GetGameInstance<UPHGameInstance>();
	const TSharedPtr<const FUniqueNetId> UserId = GetUniqueId().GetUniqueNetId();
	if (GameInstance && UserId.IsValid())
	{
		GameInstance->ShowDataLoading(true);
		GameInstance->GetPlayerHttpClient().UpdateAbilities(*UserId, HunterSkillId.ToString(), PropSkillId.ToString(), CustomWeaponId.ToString(), [GameInstance](bool bSuccess) { GameInstance->ShowDataLoading(false); });
	}
}

void APHPlayerState::SetTeamNumber(int32 NewTeamNumber)
{
	TeamNumber = NewTeamNumber;
	TimeSinceTeamChanged = GetWorld()->TimeSeconds;
	DesiredTeamNumber = ETeamType::None;

	OnTeamChanged.ExecuteIfBound();

	if (const auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		GameState->OnPlayerStateChanged.Broadcast(this);
	}
}

void APHPlayerState::OnRep_TeamNumber()
{
	SetTeamNumber(TeamNumber);
}

void APHPlayerState::SetDesiredTeamNumber(int32 NewTeamNumber)
{
	DesiredTeamNumber = NewTeamNumber;
}

void APHPlayerState::SwapTeamNumber()
{
	if (DesiredTeamNumber != ETeamType::None)
	{
		TeamNumber = DesiredTeamNumber;
		DesiredTeamNumber = ETeamType::None;
	}

	if (TeamNumber == ETeamType::Hunter)
	{
		TeamNumber = ETeamType::Prop;
	}
	else if (TeamNumber == ETeamType::Prop)
	{
		TeamNumber = ETeamType::Hunter;
	}
}

FString APHPlayerState::GetShortPlayerName() const
{
	if (GetPlayerName().Len() > MAX_PLAYER_NAME_LENGTH)
	{
		return GetPlayerName().Left(MAX_PLAYER_NAME_LENGTH - 3) + "...";
	}
	return GetPlayerName();
}

void APHPlayerState::NotifyKill_Implementation(APHPlayerState* KillerPlayerState, APHPlayerState* KilledPlayerState, UGameplayEffectUIData* UIData)
{
	if (KillerPlayerState->GetUniqueId().GetUniqueNetId().IsValid())
	{
		//search for the actual killer before calling OnKill()	
		for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
		{
			auto TestPC = Cast<APHPlayerController>(*It);
			if (TestPC && TestPC->IsLocalController())
			{
				// a local player might not have an ID if it was created with CreateDebugPlayer.
				const auto LocalPlayer = Cast<ULocalPlayer>(TestPC->Player);
				FUniqueNetIdRepl LocalID = LocalPlayer->GetCachedUniqueNetId();
				if (LocalID.IsValid() && *LocalPlayer->GetCachedUniqueNetId() == *KillerPlayerState->GetUniqueId())
				{
					TestPC->OnKill();
				}
			}
		}
	}
}

void APHPlayerState::BroadcastDeath_Implementation(APHPlayerState* KillerPlayerState, APHPlayerState* KilledPlayerState, UGameplayEffectUIData* UIData)
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		// all local players get death messages so they can update their huds.
		const auto TestPC = Cast<APHPlayerController>(*It);
		if (TestPC && TestPC->IsLocalController())
		{
			TestPC->OnDeathMessage(KillerPlayerState, this, UIData);
		}
	}
}

void APHPlayerState::AddBulletsFired(int32 NumBullets)
{
	NumBulletsFired += NumBullets;
}

void APHPlayerState::AddRocketsFired(int32 NumRockets)
{
	NumRocketsFired += NumRockets;
}

void APHPlayerState::SetQuitter(bool bInQuitter)
{
	bQuitter = bInQuitter;
}

bool APHPlayerState::CanHearTransformations() const
{
	return IsProp() || bHearTransformations;
}

void APHPlayerState::ScoreWin(bool bWinner)
{
	if (bWinner)
	{
		Wins++;
	}
	else
	{
		Losses++;
	}
}

void APHPlayerState::AddXP()
{
	CurrentXP += GetScore();

	if (CurrentXP >= NeededXP)
	{
		PlayerLevel++;
		CurrentXP -= NeededXP;
		NeededXP *= 2;
	}
}

void APHPlayerState::ResetStats()
{
	if (HasAuthority())
	{
		AddXP();
		SaveStats();

		HunterScore = 0;
		PropScore = 0;
		Kills = 0;
		Deaths = 0;
		Taunts = 0;
		Morphs = 0;
		SurvivalTime = 0;
		Wins = 0;
		Losses = 0;

		TeamNumber = ETeamType::None;
		TimeSinceTeamChanged = 0.f;
	}
}

void APHPlayerState::CreatePlayer()
{
	if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		FRequest_Player CreatePlayerRequest;
		CreatePlayerRequest.uniqueNetId = GetUniqueId().GetUniqueNetId()->ToString();
		CreatePlayerRequest.playerName = GetPlayerName();
		CreatePlayerRequest.onlineSubsystem = IOnlineSubsystem::Get()->GetSubsystemName().ToString();

		auto CreatePlayerCallback = [this](bool bSuccess)
		{
			if (bSuccess)
			{
				bUserDataLoaded = true;
				OnLoadPlayerCompleteDelegate.ExecuteIfBound();
			}
		};
		GameInstance->GetPlayerHttpClient().CreatePlayer(CreatePlayerRequest, CreatePlayerCallback);
	}
}

void APHPlayerState::LoadPlayer()
{
	auto GameInstance = GetGameInstance<UPHGameInstance>();
	const TSharedPtr<const FUniqueNetId> UserId = GetUniqueId().GetUniqueNetId();
	if (GameInstance && UserId.IsValid())
	{
		GameInstance->ShowDataLoading(true);

		auto GetPlayerCallback = [this, GameInstance](FRequest_Player InPlayer)
		{
			if (!InPlayer.uniqueNetId.IsEmpty())
			{
				PlayerLevel = InPlayer.characterLevel;
				CurrentXP = InPlayer.currentXP;
				NeededXP = InPlayer.neededXP;
				HunterSkillId = FPrimaryAssetId::FromString(InPlayer.hunterSkillId);
				PropSkillId = FPrimaryAssetId::FromString(InPlayer.propSkillId);
				CustomWeaponId = FPrimaryAssetId::FromString(InPlayer.customWeaponId);

				bUserDataLoaded = true;
				OnLoadPlayerCompleteDelegate.ExecuteIfBound();
			}
			else
			{
				CreatePlayer();
			}

			GameInstance->ShowDataLoading(false);
		};
		GameInstance->GetPlayerHttpClient().GetPlayer(*UserId, GetPlayerCallback);
	}
}

void APHPlayerState::SaveStats()
{
	auto GameInstance = GetGameInstance<UPHGameInstance>();
	const TSharedPtr<const FUniqueNetId> UserId = GetUniqueId().GetUniqueNetId();
	if (GameInstance && UserId.IsValid())
	{
		GameInstance->ShowDataLoading(true);

		FRequest_PlayerStats PlayerStats;
		PlayerStats.kills = Kills;
		PlayerStats.deaths = Deaths;
		PlayerStats.taunts = Taunts;
		PlayerStats.transforms = Morphs;
		PlayerStats.survivalTime = SurvivalTime;
		PlayerStats.wins = Wins;
		PlayerStats.losses = Losses;
		PlayerStats.propXP = PropScore;
		PlayerStats.hunterXP = HunterScore;

		auto UpdateStatsCallback = [this, GameInstance, UserId](bool bSuccess)
		{
			if (bSuccess)
			{
				FRequest_PlayerExp PlayerExp;
				PlayerExp.lvl = PlayerLevel;
				PlayerExp.currentXP = CurrentXP;
				PlayerExp.neededXP = NeededXP;

				GameInstance->GetPlayerHttpClient().UpdatePlayerExp(*UserId, PlayerExp, [GameInstance](bool bSuccess)
				{
					GameInstance->ShowDataLoading(false);
				});
			}
			else
			{
				GameInstance->ShowDataLoading(false);
			}
		};
		GameInstance->GetStatisticsHttpClient().UpdatePlayerStats(*UserId, PlayerStats, UpdateStatsCallback);
	}
}

void APHPlayerState::LoadStats()
{
	if (bUserDataLoaded)
	{
		auto GameInstance = GetGameInstance<UPHGameInstance>();
		const TSharedPtr<const FUniqueNetId> UserId = GetUniqueId().GetUniqueNetId();
		if (GameInstance && UserId.IsValid())
		{
			GameInstance->ShowDataLoading(true);

			auto GetStatsCallback = [this, GameInstance](FRequest_PlayerStats PlayerStats)
			{
				if (PlayerStats.bValid)
				{
					Kills = PlayerStats.kills;
					Deaths = PlayerStats.deaths;
					Taunts = PlayerStats.taunts;
					Morphs = PlayerStats.transforms;
					SurvivalTime = PlayerStats.survivalTime;
					Wins = PlayerStats.wins;
					Losses = PlayerStats.losses;
				}

				GameInstance->ShowDataLoading(false);
			};
			GameInstance->GetStatisticsHttpClient().GetPlayerStats(*UserId, GetStatsCallback);
		}
	}
	else
	{
		OnLoadPlayerCompleteDelegate.BindUObject(this, &APHPlayerState::LoadStats);
	}
}

UAbilitySystemComponent* APHPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

int32 APHPlayerState::GetCharacterLevel() const
{
	return CharacterLevel;
}

bool APHPlayerState::SetCharacterLevel(int32 NewLevel)
{
	if (GetCharacterLevel() != NewLevel && NewLevel > 0)
	{
		// Our level changed so we need to refresh abilities
		RemoveCommonStartupGameplayAbilities();
		CharacterLevel = NewLevel;
		AddCommonStartupGameplayAbilities();

		return true;
	}
	return false;
}

float APHPlayerState::GetMoveSpeed() const
{
	return AttributeSet->GetMoveSpeed();
}

bool APHPlayerState::IsAlive() const
{
	if (IsSpectator())
	{
		return false;
	}

	switch (TeamNumber)
	{
		case ETeamType::Hunter:
			return AttributeSet->GetViolations() < AttributeSet->GetMaxViolations();
		case ETeamType::Prop:
			return AttributeSet->GetHealth() > 0.f;
		default:
			return false;
	}
}

void APHPlayerState::AddCommonStartupGameplayAbilities()
{
	if (!bAbilitiesInitialized)
	{
		AddGameplayAbilitiesAndEffects(this, GameplayAbilities, PassiveGameplayEffects);
		bAbilitiesInitialized = true;
	}
}

void APHPlayerState::RemoveCommonStartupGameplayAbilities()
{
	if (bAbilitiesInitialized)
	{
		RemoveGameplayAbilitiesAndEffects(this,GameplayAbilities);
		bAbilitiesInitialized = false;
	}
}

void APHPlayerState::HandleMoveSpeedChanged(float DeltaValue, const struct FGameplayTagContainer& EventTags)
{
	if (const auto Character = GetPawn<APHCharacterBase>())
	{
		// Update the character movement's walk speed
		Character->SetMaxValkSpeed(GetMoveSpeed());
	}
}

void APHPlayerState::HandleXPChanged(float DeltaValue, const struct FGameplayTagContainer& EventTags)
{
	SetScore(GetScore() + DeltaValue); // For compatibility with the native Score parameter

	if (TeamNumber == ETeamType::Hunter)
	{
		HunterScore += DeltaValue;
	}
	else if (TeamNumber == ETeamType::Prop)
	{
		PropScore += DeltaValue;
	}
}

void ShuffleArray(TArray<int32>& TargetArray)
{
	const int32 LastIndex = TargetArray.Num() - 1;
	for (int32 i = 0; i < LastIndex; ++i)
	{
		const int32 Index = FMath::RandRange(i, LastIndex);
		if (i != Index)
		{
			TargetArray.Swap(i, Index);
		}
	}
}

bool APHPlayerState::GetRandTauntMediaAsset(TSoftObjectPtr<UAkExternalMediaAsset>& TauntMediaAsset)
{
	const auto GameState = GetWorld()->GetGameState<APHGameState>();
	const auto GameInstance = GetGameInstance<UPHGameInstance>();
	if (GameState && GameInstance)
	{
		FString CurrentLang;
		
		const FString& ServerLang = GameState->ServerLang;
		if (!ServerLang.IsEmpty())
		{
			CurrentLang = ServerLang;
		}
		else
		{
			CurrentLang = UPHGameUserSettings::GetPHGameUserSettings()->GetTauntLang();
		}

		UPHTauntDataAsset* TauntDataAsset = GameInstance->GetTauntDataAssetByCountry(CurrentLang.ToLower());
		if (TauntDataAsset)
		{
			TArray<TSoftObjectPtr<UAkExternalMediaAsset>> TauntAssets = TauntDataAsset->TauntAssets;

			if (TauntPlaylist.Num() == 0)
			{
				for (int32 i = 0; i < TauntAssets.Num(); ++i)
				{
					TauntPlaylist.Add(i);
				}
				
				ShuffleArray(TauntPlaylist);
			}

			if (!TauntPlaylist.IsValidIndex(CurrentTauntIndex))
			{
				ShuffleArray(TauntPlaylist);
				CurrentTauntIndex = 0;
			}

			TauntMediaAsset = TauntAssets[TauntPlaylist[CurrentTauntIndex++]];
			return true;
		}
	}

	return false;
}

void APHPlayerState::ApplyHuntingEffect()
{
	check(AbilitySystemComponent);
	
	if (HasAuthority() && bAbilitiesInitialized)
	{
		AbilitySystemComponent->ApplyGameplayEffectToSelf(Cast<UGameplayEffect>(HuntingEffect->GetDefaultObject()), GetCharacterLevel(), AbilitySystemComponent->MakeEffectContext());
	}
}

void APHPlayerState::AddGameplayAbilitiesAndEffects(AActor* AbilityOwner, const TArray<TSubclassOf<UPHGameplayAbility>>& GameplayAbilitiesToAdd, const TArray<TSubclassOf<UGameplayEffect>>& GameplayEffectsToAdd)
{
	if (HasAuthority())
	{
		// Grant abilities, but only on the server
		for (const TSubclassOf<UPHGameplayAbility>& Ability : GameplayAbilitiesToAdd)
		{
			AddGameplayAbility(AbilityOwner, Ability, GetCharacterLevel());
		}

		// Now apply passives
		for (const TSubclassOf<UGameplayEffect>& GameplayEffect : GameplayEffectsToAdd)
		{
			AddGameplayEffect(AbilityOwner, GameplayEffect, GetCharacterLevel());
		}
	}
}

void APHPlayerState::RemoveGameplayAbilitiesAndEffects(AActor* AbilityOwner, const TArray<TSubclassOf<UPHGameplayAbility>>& GameplayAbilitiesToRemove)
{
	check(AbilitySystemComponent);

	if (HasAuthority())
	{
		// Remove any abilities added from a previous call
		TArray<FGameplayAbilitySpecHandle> AbilitiesToRemove;
		for (const FGameplayAbilitySpec& Spec : AbilitySystemComponent->GetActivatableAbilities())
		{
			if ((Spec.SourceObject == AbilityOwner) && GameplayAbilitiesToRemove.Contains(Spec.Ability->GetClass()))
			{
				AbilitiesToRemove.Add(Spec.Handle);
			}
		}

		// Do in two passes so the removal happens after we have the full list
		for (int32 i = 0; i < AbilitiesToRemove.Num(); i++)
		{
			AbilitySystemComponent->ClearAbility(AbilitiesToRemove[i]);
		}

		// Remove all of the passive gameplay effects that were applied by the owner actor
		FGameplayEffectQuery Query;
		Query.EffectSource = AbilityOwner;
		AbilitySystemComponent->RemoveActiveEffects(Query);
	}
}

void APHPlayerState::RemoveOnDeathEffects()
{
	if (HasAuthority())
	{
		AbilitySystemComponent->CancelAllAbilities();

		FGameplayTagContainer EffectTagsToRemove;
		EffectTagsToRemove.AddTag(EffectRemoveOnDeathTag);
		AbilitySystemComponent->RemoveActiveEffectsWithTags(EffectTagsToRemove);
	}
}

FGameplayAbilitySpecHandle APHPlayerState::AddGameplayAbility(AActor* AbilityOwner, TSubclassOf<UPHGameplayAbility> GameplayAbilityToAdd, int32 Level) const
{
	check(AbilitySystemComponent);
	
	if (AbilityOwner && GameplayAbilityToAdd)
	{
		return AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(GameplayAbilityToAdd, Level, static_cast<int32>(GameplayAbilityToAdd.GetDefaultObject()->AbilityInputID), AbilityOwner));
	}

	return FGameplayAbilitySpecHandle();
}

void APHPlayerState::ClearGameplayAbility(const FGameplayAbilitySpecHandle& Handle) const
{
	check(AbilitySystemComponent);

	if (Handle.IsValid())
	{
		AbilitySystemComponent->ClearAbility(Handle);
	}
}

FActiveGameplayEffectHandle APHPlayerState::AddGameplayEffect(AActor* EffectOwner, TSubclassOf<UGameplayEffect> GameplayEffectToAdd, int32 Level) const
{
	check(AbilitySystemComponent);
	
	if (EffectOwner && GameplayEffectToAdd)
	{
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(EffectOwner);

		FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffectToAdd, Level, EffectContext);
		if (NewHandle.IsValid())
		{
			return AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
		}
	}

	return FActiveGameplayEffectHandle();
}

void APHPlayerState::OnRep_CharacterAbilityId()
{
	SetCharacterSkill(CharacterSkillId);
}

void APHPlayerState::SetCharacterSkill(const FPrimaryAssetId& NewCharacterSkillId)
{
	CharacterSkillId = NewCharacterSkillId;
	OnCharacterSkillSet.ExecuteIfBound();
}

void APHPlayerState::InitCustomSkill(APawn* AbilityOwner)
{
	if (!HasAuthority())
	{
		return;
	}

	if (!IsPlayerLoaded())
	{
		OnLoadPlayerCompleteDelegate.BindUObject(this, &APHPlayerState::InitCustomSkill, AbilityOwner);
		return;
	}

	// Clear out previous skill
	for (const FGameplayAbilitySpecHandle& Handle : CustomAbilitySpecHandles)
	{
		ClearGameplayAbility(Handle);
	}

	if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		SetCharacterSkill(IsHunter() ? HunterSkillId : PropSkillId);

		UPHItem* Item = GameInstance->GetAbilityItem(CharacterSkillId);
		if (Item)
		{
			// Use the character level as default
			int32 AbilityLevel = GetCharacterLevel();
			if (Item->AbilityLevel > 0)
			{
				// Override the ability level to use the data from the slotted item
				AbilityLevel = Item->AbilityLevel;
			}

			for (const TSubclassOf<UPHGameplayAbility>& GrantedAbility : Item->GrantedAbilities)
			{
				FGameplayAbilitySpecHandle Handle = AddGameplayAbility(AbilityOwner, GrantedAbility, AbilityLevel);
				if (Handle.IsValid())
				{
					CustomAbilitySpecHandles.Add(Handle);
				}
			}
		}
	}
}

void APHPlayerState::LoadPlayerSpray(TFunction<void(UTexture2D*)> LoadSprayCallback)
{
	const auto GameInstance = GetGameInstance<UPHGameInstance>();
	const TSharedPtr<const FUniqueNetId> UserId = GetUniqueId().GetUniqueNetId();
	if (GameInstance && UserId.IsValid())
	{
		if (CurrentSprayTexture && !bSprayDirty)
		{
			LoadSprayCallback(CurrentSprayTexture);
			return;
		}
		
		auto SprayDownloadCallback = [this, LoadSprayCallback](UTexture2D* SprayTexture)
		{
			CurrentSprayTexture = SprayTexture;
			bSprayDirty = false;
			LoadSprayCallback(SprayTexture);
		};
		GameInstance->GetSprayHttpClient().DownloadSpray(*UserId, SprayDownloadCallback);
	}
	else
	{
		LoadSprayCallback(nullptr);
	}
}

void APHPlayerState::OnSprayUpdated_Implementation()
{
	bSprayDirty = true;
}

bool APHPlayerState::OnSprayUpdated_Validate()
{
	return true;
}
