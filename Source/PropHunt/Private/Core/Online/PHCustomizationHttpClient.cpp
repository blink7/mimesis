// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHCustomizationHttpClient.h"

UPHCustomizationHttpClient::UPHCustomizationHttpClient() : CustomizationEndpoint("customization")
{
}

void UPHCustomizationHttpClient::GetPropColorScheme(const FUniqueNetId& UserId, ColorSchemeCallback OnGetColorSchemaCallback)
{
	const HttpRequestRef Request = GetRequest(CustomizationEndpoint + "/prop?id=" + UserId.ToString());
	Request->OnProcessRequestComplete().BindUObject(this, &UPHCustomizationHttpClient::OnGetPropColorSchemeResponseReceived, OnGetColorSchemaCallback);
	Send(Request);
}

void UPHCustomizationHttpClient::UpdatePropColorScheme(const FUniqueNetId& UserId, const FPropColorScheme& ColorSchema, ResponseStatusCallback OnUpdateColorSchemaCallback)
{
	FString ContentJsonString;
	GetJsonStringFromStruct<FPropColorScheme>(ColorSchema, ContentJsonString);

	const HttpRequestRef Request = PutRequest(CustomizationEndpoint + "/prop?id=" + UserId.ToString(), ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &UPHCustomizationHttpClient::OnUpdatePropColorSchemeResponseReceived, OnUpdateColorSchemaCallback);
	Send(Request);
}

void UPHCustomizationHttpClient::OnGetPropColorSchemeResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ColorSchemeCallback OnGetColorSchemeCallback)
{
	FPropColorScheme ColorSchemeResponse;

	if (ResponseIsValid(Response, bWasSuccessful))
	{
		GetStructFromJsonString(Response, ColorSchemeResponse);
	}

	OnGetColorSchemeCallback(ColorSchemeResponse);
}

void UPHCustomizationHttpClient::OnUpdatePropColorSchemeResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateColorSchemeCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);
	UE_LOG(LogHttpClient, Verbose, TEXT("Prop's color schema updated with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	OnUpdateColorSchemeCallback(bSuccess);
}
