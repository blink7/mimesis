// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHOnlineAsyncTaskManagerSteam.h"

#include "Core/PHGameInstance.h"
#include "Core/Online/SteamUtils.h"


DEFINE_LOG_CATEGORY(LogSteamOnline);

void FPHOnlineAsyncTaskManagerSteam::OnlineTick()
{
	check(FPlatformTLS::GetCurrentThreadId() == OnlineThreadId);

	if (SteamAPI_IsSteamRunning())
	{
		SteamAPI_RunCallbacks();
	}
}

/**
 * Notification event from Steam that the avatar is loaded
 */
class FOnlineAsyncEventSteamAvatarImageLoaded : public FOnlineAsyncItem
{
private:
	/** Reference to game instance */
	UPHGameInstance* GameInstance;
	/** Avatar load information */
	AvatarImageLoaded_t CallbackResults;

	/** Hidden on purpose */
	FOnlineAsyncEventSteamAvatarImageLoaded() : 
		GameInstance(nullptr)
	{
		FMemory::Memzero(CallbackResults);
	}

public:
	FOnlineAsyncEventSteamAvatarImageLoaded(UPHGameInstance* InSubsystem, const AvatarImageLoaded_t& InResults) : 
		GameInstance(InSubsystem),
		CallbackResults(InResults)
	{
	}

	virtual ~FOnlineAsyncEventSteamAvatarImageLoaded()
	{
	}

	/**
	 *	Get a human readable description of task
	 */
	virtual FString ToString() const override
	{
		return FString::Printf(TEXT("FOnlineAsyncEventSteamAvatarImageLoaded SteamId: %s Image: %d"), *FSteamUtils::CSteamIDToString(CallbackResults.m_steamID), CallbackResults.m_iImage);
	}

	/**
	 * Give the async task a chance to marshal its data back to the game thread
	 * Can only be called on the game thread by the async task manager
	 */
	virtual void Finalize() override
	{
		GameInstance->AvatarImageLoadedEvent.Broadcast(FSteamUtils::CSteamIDToString(CallbackResults.m_steamID), CallbackResults.m_iImage);
	}
};

/**
 * Notification event from Steam that an avatar image has been loaded
 *
 * @param CallbackData information about player avatar
 */
void FPHOnlineAsyncTaskManagerSteam::OnAvatarImageLoaded(AvatarImageLoaded_t* CallbackData)
{
	FOnlineAsyncEventSteamAvatarImageLoaded* NewEvent = new FOnlineAsyncEventSteamAvatarImageLoaded(GameInstance, *CallbackData);
	UE_LOG(LogSteamOnline, Verbose, TEXT("%s"), *NewEvent->ToString());
	AddToOutQueue(NewEvent);
}
