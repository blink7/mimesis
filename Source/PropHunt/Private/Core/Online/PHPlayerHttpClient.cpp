// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHPlayerHttpClient.h"

UPHPlayerHttpClient::UPHPlayerHttpClient() : PlayerEndpoint("player")
{
}

void UPHPlayerHttpClient::CreatePlayer(const FRequest_Player& CreatePlayerRequest, PlayerCreatedCallback OnCreatePlayerCallback)
{
	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_Player>(CreatePlayerRequest, ContentJsonString);

	const HttpRequestRef Request = PostRequest(PlayerEndpoint + "/create", ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &UPHPlayerHttpClient::OnCreatePlayerResponseReceived, OnCreatePlayerCallback);
	Send(Request);
}

void UPHPlayerHttpClient::GetPlayer(const FUniqueNetId& UserId, PlayerCallback OnGetPlayerCallback)
{
	const HttpRequestRef Request = GetRequest(PlayerEndpoint + "?id=" + UserId.ToString());
	Request->OnProcessRequestComplete().BindUObject(this, &UPHPlayerHttpClient::OnGetPlayerResponseReceived, OnGetPlayerCallback);
	Send(Request);
}

void UPHPlayerHttpClient::UpdatePlayerExp(const FUniqueNetId& UserId, const FRequest_PlayerExp& PlayerExp, ResponseStatusCallback OnUpdateExpCallback)
{
	FString ContentJsonString;
	GetJsonStringFromStruct(PlayerExp, ContentJsonString);

	const HttpRequestRef Request = PutRequest(PlayerEndpoint + "/exp?id=" + UserId.ToString(), ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &UPHPlayerHttpClient::OnUpdatePlayerExpResponseReceived, OnUpdateExpCallback);
	Send(Request);
}

void UPHPlayerHttpClient::UpdateAbilities(const FUniqueNetId& UserId, FString HunterSkillId, FString PropSkillId, FString CustomWeaponId, ResponseStatusCallback OnUpdateAbilitiesCallback)
{
	FRequest_Player UpdateAbilitiesRequest;
	UpdateAbilitiesRequest.uniqueNetId = UserId.ToString();
	UpdateAbilitiesRequest.hunterSkillId = HunterSkillId;
	UpdateAbilitiesRequest.propSkillId = PropSkillId;
	UpdateAbilitiesRequest.customWeaponId = CustomWeaponId;

	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_Player>(UpdateAbilitiesRequest, ContentJsonString);

	const HttpRequestRef Request = PutRequest(PlayerEndpoint + "/abilities", ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &UPHPlayerHttpClient::OnUpdateAbilitiesResponseReceived, OnUpdateAbilitiesCallback);
	Send(Request);
}

void UPHPlayerHttpClient::OnCreatePlayerResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, PlayerCreatedCallback OnCreatePlayerCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);

	UE_LOG(LogHttpClient, Warning, TEXT("New Player data created with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	
	OnCreatePlayerCallback(bSuccess);
}

void UPHPlayerHttpClient::OnGetPlayerResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, PlayerCallback OnGetPlayerCallback)
{
	if (ResponseIsValid(Response, bWasSuccessful))
	{
		FRequest_Player GetPlayerResponse;
		GetStructFromJsonString(Response, GetPlayerResponse);
		OnGetPlayerCallback(GetPlayerResponse);
	}
	else if (Response.IsValid() && Response->GetContentAsString().Contains("PlayerNotFoundError"))
	{
		UE_LOG(LogHttpClient, Verbose, TEXT("Requested Player not found!"));
		OnGetPlayerCallback(FRequest_Player());
	}
}

void UPHPlayerHttpClient::OnUpdatePlayerExpResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateExpCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);
	UE_LOG(LogHttpClient, Verbose, TEXT("Player Exp updated with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	OnUpdateExpCallback(bSuccess);
}

void UPHPlayerHttpClient::OnUpdateAbilitiesResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateAbilitiesCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);
	UE_LOG(LogHttpClient, Verbose, TEXT("Player Abilities updated with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	OnUpdateAbilitiesCallback(bSuccess);
}
