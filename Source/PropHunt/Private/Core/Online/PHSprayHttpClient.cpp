// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHSprayHttpClient.h"

#include "ImageUtils.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"

UPHSprayHttpClient::UPHSprayHttpClient() : SprayEndpoint("spray")
{
}

void UPHSprayHttpClient::CheckSprayExists(const FUniqueNetId& UserId, const FString& SprayHash, ResponseStatusCallback OnSprayExistsCallback)
{
	const HttpRequestRef Request = PostRequest(SprayEndpoint + "?id=" + UserId.ToString(), SprayHash);
	Request->SetHeader("Content-Type", "text/plain");
	Request->OnProcessRequestComplete().BindUObject(this, &UPHSprayHttpClient::OnSprayExistsResponseReceived, OnSprayExistsCallback);
	Send(Request);
}

void UPHSprayHttpClient::UploadSpray(const FUniqueNetId& UserId, UTexture2D* SprayTexture, const FString& Name, ResponseStatusCallback OnUploadedCallback)
{
	const uint8* MipData = static_cast<const uint8*>(SprayTexture->PlatformData->Mips[0].BulkData.LockReadOnly());

	TArray<FColor> RawData;
	const uint32 TotalSize = SprayTexture->PlatformData->Mips[0].BulkData.GetBulkDataSize();
	RawData.AddZeroed(TotalSize);

	FMemory::Memcpy(RawData.GetData(), MipData, TotalSize);
	SprayTexture->PlatformData->Mips[0].BulkData.Unlock();

	TArray<uint8> CompressedData;
	FImageUtils::CompressImageArray(SprayTexture->GetSizeX(), SprayTexture->GetSizeY(), RawData, CompressedData);

	const FString CRLF = "\r\n";
	const FString a = CRLF + "--spray" + CRLF;
	const FString b = "Content-Disposition: form-data; name=\"file\";  filename=\"" + Name + ".png\"" + CRLF;
	const FString c = "Content-Type: image/png" + CRLF + CRLF;
	const FString e = CRLF + "--spray--" + CRLF;

	TArray<uint8> MultipartData;
	MultipartData.Append(reinterpret_cast<uint8*>(TCHAR_TO_UTF8(*a)), a.Len());
	MultipartData.Append(reinterpret_cast<uint8*>(TCHAR_TO_UTF8(*b)), b.Len());
	MultipartData.Append(reinterpret_cast<uint8*>(TCHAR_TO_UTF8(*c)), c.Len());
	MultipartData.Append(CompressedData);
	MultipartData.Append(reinterpret_cast<uint8*>(TCHAR_TO_UTF8(*e)), e.Len());

	const HttpRequestRef Request = PutRequest(SprayEndpoint + "?id=" + UserId.ToString(), MultipartData);
	Request->SetHeader("Content-Type", "multipart/form-data; boundary=spray");
	Request->OnProcessRequestComplete().BindUObject(this, &UPHSprayHttpClient::OnUploadPlayerSprayResponseReceived, OnUploadedCallback);
	Send(Request);
}

void UPHSprayHttpClient::DownloadSpray(const FUniqueNetId& UserId, SprayTextureCallback OnSprayDownloadCallback)
{
	const HttpRequestRef Request = GetRequest(SprayEndpoint + "?id=" + UserId.ToString());
	Request->OnProcessRequestComplete().BindUObject(this, &UPHSprayHttpClient::OnDownloadSprayResponseReceived, OnSprayDownloadCallback);
	Send(Request);
}

void UPHSprayHttpClient::OnSprayExistsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnSprayExistsCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);
	const int32 ResponseCode = Response->GetResponseCode();
	UE_LOG(LogHttpClient, Verbose, TEXT("Player spray already exists checked with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	OnSprayExistsCallback(bSuccess && ResponseCode == EHttpResponseCodes::Ok);
}

void UPHSprayHttpClient::OnUploadPlayerSprayResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUploadedCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);
	UE_LOG(LogHttpClient, Verbose, TEXT("Player spray uploaded with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	OnUploadedCallback(bSuccess);
}

void UPHSprayHttpClient::OnDownloadSprayResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, SprayTextureCallback OnSprayLoadedCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);
	UE_LOG(LogHttpClient, Verbose, TEXT("Player spray downloaded with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	if (bSuccess)
	{
		IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
		TSharedPtr<IImageWrapper> ImageWrappers[3] =
		{
			ImageWrapperModule.CreateImageWrapper(EImageFormat::PNG),
			ImageWrapperModule.CreateImageWrapper(EImageFormat::JPEG),
			ImageWrapperModule.CreateImageWrapper(EImageFormat::BMP),
		};

		for (auto ImageWrapper : ImageWrappers)
		{
			if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(Response->GetContent().GetData(), Response->GetContentLength()))
			{
				TArray<uint8> RawData;
				if (ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, RawData))
				{
					UTexture2D* SprayTexture = UTexture2D::CreateTransient(ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), PF_B8G8R8A8);
					if (SprayTexture)
					{
						SprayTexture->bNotOfflineProcessed = true;
						uint8* MipData = static_cast<uint8*>(SprayTexture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE));

						// Bulk data was already allocated for the correct size when we called CreateTransient above
						FMemory::Memcpy(MipData, RawData.GetData(), SprayTexture->PlatformData->Mips[0].BulkData.GetBulkDataSize());

						SprayTexture->PlatformData->Mips[0].BulkData.Unlock();

						SprayTexture->UpdateResource();
					}
					OnSprayLoadedCallback(SprayTexture);
					return;
				}
			}
		}
	}
}
