// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/Online/SteamUtils.h"

#include "Engine/Texture2D.h"
#include "Engine/Engine.h"


CSteamID FSteamUtils::StringToCSteamID(const FString& SteamId)
{
	return CSteamID(static_cast<uint64>(FCString::Atoi64(*SteamId)));
}

FString FSteamUtils::CSteamIDToString(const CSteamID& SteamId)
{
	return FString::Printf(TEXT("%llu"), SteamId.ConvertToUint64());
}

int FSteamUtils::GetSteamAvatar(const FString& PlayerId, EAvatarSize SizeType)
{
	if (SteamAPI_IsSteamRunning() && SteamFriends())
	{
		CSteamID PlayerRawID = StringToCSteamID(PlayerId);

		//Getting the PictureID from the SteamAPI
		switch (SizeType)
		{
			case EAvatarSize::Small:
				return SteamFriends()->GetSmallFriendAvatar(PlayerRawID);
			case EAvatarSize::Medium:
				return SteamFriends()->GetMediumFriendAvatar(PlayerRawID);
			case EAvatarSize::Large:
				return SteamFriends()->GetLargeFriendAvatar(PlayerRawID);
		}
	}

	return 0;
}

class UTexture2D* FSteamUtils::GetSteamImageAsTexture(int Image)
{
	UTexture2D* Texture = nullptr;

	uint32 AvatarWidth, AvatarHeight;
	bool bSuccess = SteamUtils()->GetImageSize(Image, &AvatarWidth, &AvatarHeight);
	if (!bSuccess)
	{
		UE_LOG(LogTemp, Log, TEXT("GetSteamImageAsTexture - failed to get image size for image %d"), Image);
		return Texture;
	}

	const int ImageSizeInBytes = AvatarWidth * AvatarHeight * 4;
	auto AvatarRGBA = new uint8[ImageSizeInBytes];
	bSuccess = SteamUtils()->GetImageRGBA(Image, AvatarRGBA, ImageSizeInBytes);
	if (bSuccess)
	{
		//Swap R and B channels because for some reason the games whack
		for (int i = 0; i < ImageSizeInBytes; i += 4)
		{
			uint8 Temp = AvatarRGBA[i + 0];
			AvatarRGBA[i + 0] = AvatarRGBA[i + 2];
			AvatarRGBA[i + 2] = Temp;
		}

		Texture = UTexture2D::CreateTransient(AvatarWidth, AvatarHeight, PF_B8G8R8A8);

		//MAGIC!
		uint8* MipData = static_cast<uint8*>(Texture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE));
		FMemory::Memcpy(MipData, static_cast<void*>(AvatarRGBA), ImageSizeInBytes);
		Texture->PlatformData->Mips[0].BulkData.Unlock();

		//Setting some Parameters for the Texture and finally returning it
		Texture->PlatformData->SetNumSlices(1);
		Texture->NeverStream = true;
		//Avatar->CompressionSettings = TC_EditorIcon;

		Texture->UpdateResource();

		//todo add the texture to the cache
	}

	delete[] AvatarRGBA;

	return Texture;
}
