// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/Online/PHOnlineGameSettings.h"


FPHOnlineSessionSettings::FPHOnlineSessionSettings(bool bIsLAN /*= false*/, bool bIsPresence /*= false*/, int32 MaxNumPlayers /*= 4*/)
{
	NumPublicConnections = MaxNumPlayers;
	if (NumPublicConnections < 0)
	{
		NumPublicConnections = 0;
	}
	NumPrivateConnections = 0;
	bIsLANMatch = bIsLAN;
	bShouldAdvertise = true;
	bAllowJoinInProgress = true;
	bAllowInvites = true;
	bUsesPresence = bIsPresence;
	bAllowJoinViaPresence = true;
	bAllowJoinViaPresenceFriendsOnly = false;
}

FPHOnlineSearchSettings::FPHOnlineSearchSettings(bool bSearchingLAN /*= false*/, bool bSearchingPresence /*= false*/)
{
	bIsLanQuery = bSearchingLAN;
	MaxSearchResults = 10;
	PingBucketSize = 50;

	if (bSearchingPresence)
	{
		QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
	}
}

FPHInlineSearchSettingsEmptyDedicated::FPHInlineSearchSettingsEmptyDedicated(bool bSearchingLAN /*= false*/, bool bSearchingPresence /*= false*/)
{
	QuerySettings.Set(SEARCH_DEDICATED_ONLY, true, EOnlineComparisonOp::Equals);
	QuerySettings.Set(SEARCH_EMPTY_SERVERS_ONLY, true, EOnlineComparisonOp::Equals);
}
