// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHStatisticsHttpClient.h"

UPHStatisticsHttpClient::UPHStatisticsHttpClient() : StatisticsEndpoint("stats")
{
}

void UPHStatisticsHttpClient::GetTodayRating(const FUniqueNetId& UserId, const FName& RatingType, TodayRatingCallback OnGetTodayRatingCallback)
{
	const HttpRequestRef Request = GetRequest(StatisticsEndpoint + FString::Printf(TEXT("/today-rating?type=%s&id=%s"), *RatingType.ToString(), *UserId.ToString()));
	Request->OnProcessRequestComplete().BindUObject(this, &UPHStatisticsHttpClient::OnGetTodayRatingResponseReceived, OnGetTodayRatingCallback);
	Send(Request);
}

void UPHStatisticsHttpClient::GetPlayerStats(const FUniqueNetId& UserId, StatsCallback OnGetStatsCallback)
{
	const HttpRequestRef Request = GetRequest(StatisticsEndpoint + "?id=" + UserId.ToString());
	Request->OnProcessRequestComplete().BindUObject(this, &UPHStatisticsHttpClient::OnGetPlayerStatsResponseReceived, OnGetStatsCallback);
	Send(Request);
}

void UPHStatisticsHttpClient::UpdatePlayerStats(const FUniqueNetId& UserId, const FRequest_PlayerStats& PlayerStats, ResponseStatusCallback OnUpdateStatsCallback)
{
	FString ContentJsonString;
	GetJsonStringFromStruct<FRequest_PlayerStats>(PlayerStats, ContentJsonString);

	const HttpRequestRef Request = PutRequest(StatisticsEndpoint + "?id=" + UserId.ToString(), ContentJsonString);
	Request->OnProcessRequestComplete().BindUObject(this, &UPHStatisticsHttpClient::OnUpdatePlayerStatsResponseReceived, OnUpdateStatsCallback);
	Send(Request);
}

void UPHStatisticsHttpClient::OnGetTodayRatingResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, TodayRatingCallback OnGetTodayRatingCallback)
{
	TArray<FResponse_TodayRatingItem> TodayRatingResponse;

	if (ResponseIsValid(Response, bWasSuccessful))
	{
		GetStructArrayFromJsonString(Response, TodayRatingResponse);
	}

	OnGetTodayRatingCallback(TodayRatingResponse);
}

void UPHStatisticsHttpClient::OnGetPlayerStatsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, StatsCallback OnGetStatsCallback)
{
	FRequest_PlayerStats PlayerStatsResponse;

	if (ResponseIsValid(Response, bWasSuccessful))
	{
		PlayerStatsResponse.bValid = true;
		GetStructFromJsonString(Response, PlayerStatsResponse);
	}

	OnGetStatsCallback(PlayerStatsResponse);
}

void UPHStatisticsHttpClient::OnUpdatePlayerStatsResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ResponseStatusCallback OnUpdateStatsCallback)
{
	const bool bSuccess = ResponseIsValid(Response, bWasSuccessful);
	UE_LOG(LogHttpClient, Verbose, TEXT("Player Stats updated with status: %s"), bSuccess ? TEXT("SUCCESS") : TEXT("FAIL"));
	OnUpdateStatsCallback(bSuccess);
}
