﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHPatchHttpClient.h"


void UPHPatchHttpClient::GetContentBuild(ContentBuildCallback OnGetContentBuildCallback)
{
	HttpRequestRef Request = GetRequest("PropHuntCDN/ContentBuild.json");
	Request->OnProcessRequestComplete().BindUObject(this, &UPHPatchHttpClient::OnGetContentBuildResponseReceived, OnGetContentBuildCallback);
	Send(Request);
}

void UPHPatchHttpClient::OnGetContentBuildResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, ContentBuildCallback OnGetContentBuildCallback)
{
	FResponse_ContentBuild ContentBuild;
	if (ResponseIsValid(Response, bWasSuccessful))
	{
		GetStructFromJsonString(Response, ContentBuild);
	}
	else
	{
		UE_LOG(LogHttpClient, Display, TEXT("Failde to recieve Content Build"));
	}
	
	OnGetContentBuildCallback(ContentBuild);
}
