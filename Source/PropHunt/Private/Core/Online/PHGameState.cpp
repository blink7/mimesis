// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/Online/PHGameState.h"

#include "Core/PHPlayerController.h"
#include "Core/GameModes/GameMode_Hunting.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/GameModes/PHGameModeBase.h"

#include "Net/UnrealNetwork.h"
#include "Engine/GameInstance.h"

void APHGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APHGameState, NumTeams);
	DOREPLIFETIME(APHGameState, RemainingTime);
	DOREPLIFETIME(APHGameState, bTimerPaused);
	DOREPLIFETIME(APHGameState, ServerName);
	DOREPLIFETIME(APHGameState, MapName);
	DOREPLIFETIME(APHGameState, MaxPlayers);
	DOREPLIFETIME(APHGameState, MatchData);
	DOREPLIFETIME(APHGameState, TotalEnergy);
	DOREPLIFETIME(APHGameState, PortalEnergy);
	DOREPLIFETIME(APHGameState, NeedPortalEnergy);
	DOREPLIFETIME(APHGameState, ServerLang);
}

void APHGameState::OnRep_MatchState()
{
	Super::OnRep_MatchState();

	OnMatchStateSet();
}

void APHGameState::AddPlayerState(APlayerState* PlayerState)
{
	Super::AddPlayerState(PlayerState);

	OnPlayerStateChanged.Broadcast(PlayerState);
}

void APHGameState::RemovePlayerState(APlayerState* PlayerState)
{
	Super::RemovePlayerState(PlayerState);

	OnPlayerStateChanged.Broadcast(PlayerState);
}

void APHGameState::OnMatchStateSet()
{
	if (MatchState == MatchState::WaitingToStart)
	{
		OnMatchIsWaitingToStart.Broadcast();
	}
	else if (MatchState == MatchState::InProgress)
	{
		OnMatchHasStarted.Broadcast();
	}
	else if (MatchState == MatchState::InProgressHunting)
	{
		OnMatchHasStartedHunting.Broadcast();
	}
	else if (MatchState == MatchState::WaitingPostMatch)
	{
		OnMatchHasEnded.Broadcast();
	}
	else if (MatchState == MatchState::LeavingMap)
	{
		OnLeavingMap.Broadcast();
	}
}

void APHGameState::SetRemainingTime(int32 NewValue)
{
	RemainingTime = NewValue;

	if (OnRemainingTimeChanged.IsBound())
	{
		OnRemainingTimeChanged.Broadcast(RemainingTime);
	}
}

int32 APHGameState::DecrementRemainingTime()
{
	SetRemainingTime(GetRemainingTime() - 1);
	return RemainingTime;
}

void APHGameState::SetRoundCount(const uint8& RoundCount)
{
	MatchData.RoundCount = RoundCount;
}

uint8 APHGameState::GetRoundCount() const
{
	return MatchData.RoundCount;
}

void APHGameState::SetTeamWins(const TArray<int32>& TeamWins)
{
	MatchData.TeamWins = TeamWins;
}

const TArray<int32>& APHGameState::GetTeamWins() const
{
	return MatchData.TeamWins;
}

void APHGameState::GetTimedPlayerMap(int32 TeamIndex, TimedPlayerMap& OutTimedMap) const
{
	OutTimedMap.Empty();

	//first, we need to go over all the PlayerStates, grab their time since team changed, and sort them
	TMultiMap<int32, APHPlayerState*> SortedMap;
	for (APlayerState* PlayerState : PlayerArray)
	{
		const auto CurPlayerState = Cast<APHPlayerState>(PlayerState);
		if (CurPlayerState && (CurPlayerState->GetTeamNumber() == TeamIndex))
		{
			SortedMap.Add(FMath::TruncToInt(CurPlayerState->GetTimeSinceTeamChanged()), CurPlayerState);
		}
	}

	//sort by the keys
	SortedMap.KeySort(TLess<int32>());

	//now, add them back to the timed map
	OutTimedMap.Empty();

	int32 Rank = 0;
	for (TMultiMap<int32, APHPlayerState*>::TIterator It(SortedMap); It; ++It)
	{
		OutTimedMap.Add(Rank++, It.Value());
	}
}

void APHGameState::GetRankedMap(int32 TeamIndex, RankedPlayerMap& OutRankedMap) const
{
	OutRankedMap.Empty();

	//first, we need to go over all the PlayerStates, grab their score, and rank them
	TMultiMap<float, APHPlayerState*> SortedMap;
	for (APlayerState* PlayerState : PlayerArray)
	{
		const auto CurPlayerState = Cast<APHPlayerState>(PlayerState);
		if (CurPlayerState && (CurPlayerState->GetTeamNumber() == TeamIndex))
		{
			SortedMap.Add(CurPlayerState->GetScore(), CurPlayerState);
		}
	}

	//sort by the keys
	SortedMap.KeySort(TGreater<float>());

	//now, add them back to the ranked map
	OutRankedMap.Empty();

	int32 Rank = 0;
	for (TMultiMap<float, APHPlayerState*>::TIterator It(SortedMap); It; ++It)
	{
		OutRankedMap.Add(Rank++, It.Value());
	}
}

uint8 APHGameState::GetLivePlayerCount(int32 TeamIndex) const
{
	uint8 Counter = 0;

	for (APlayerState* PlayerState : PlayerArray)
	{
		const auto MyPlayerState = Cast<APHPlayerState>(PlayerState);
		if (MyPlayerState && MyPlayerState->GetTeamNumber() == TeamIndex && MyPlayerState->IsAlive())
		{
			Counter++;
		}
	}
	return Counter;
}

uint8 APHGameState::GetActivePlayersNumber(int32 TeamNumber) const
{
	uint8 Counter = 0;

	for (APlayerState* PlayerState : PlayerArray)
	{
		const auto MyPlayerState = Cast<APHPlayerState>(PlayerState);
		if (MyPlayerState && MyPlayerState->GetTeamNumber() == TeamNumber && !MyPlayerState->IsSpectator())
		{
			Counter++;
		}
	}

	return Counter;
}

uint8 APHGameState::GetVotesForMap(int32 MapIndex) const
{
	uint8 Counter = 0;

	for (APlayerState* PlayerState : PlayerArray)
	{
		const auto MyPlayerState = Cast<APHPlayerState>(PlayerState);
		if (MyPlayerState && MyPlayerState->NextMapVote == MapIndex)
		{
			Counter++;
		}
	}

	return Counter;
}

uint8 APHGameState::GetTeamScore(int32 TeamNumber) const
{
	uint8 Score = 0;

	for (APlayerState* PlayerState : PlayerArray)
	{
		const auto MyPlayerState = Cast<APHPlayerState>(PlayerState);
		if (MyPlayerState && MyPlayerState->GetTeamNumber() == TeamNumber)
		{
			Score += MyPlayerState->GetScore();
		}
	}

	return Score;
}

uint32 APHGameState::GetReadyPlayers() const
{
	int32 Count = 0;

	for (APlayerState* PlayerState : PlayerArray)
	{
		const auto MyPlayerState = Cast<APHPlayerState>(PlayerState);
		if (MyPlayerState && MyPlayerState->bReady)
		{
			Count++;
		}
	}

	return Count;
}

void APHGameState::RequestFinishAndExitToMainMenu()
{
	if (AuthorityGameMode)
	{
		// we are server, tell the gamemode
		if (const auto GameMode = Cast<APHGameModeBase>(AuthorityGameMode))
		{
			GameMode->RequestFinishAndExitToMainMenu();
		}
	}
	else
	{
		const auto PrimaryPC = Cast<APHPlayerController>(GetGameInstance()->GetFirstLocalPlayerController());
		if (PrimaryPC)
		{
			check(PrimaryPC->GetNetMode() == ENetMode::NM_Client);
			PrimaryPC->HandleReturnToMainMenu();
		}
	}
}

void APHGameState::OnRep_RemainingTime()
{
	SetRemainingTime(RemainingTime);
}

void APHGameState::OnRep_TotalEnergy()
{
	SetTotalEnergy(TotalEnergy);
}

void APHGameState::OnRep_PortalEnergy()
{
	SetPortalEnergy(PortalEnergy);
}

void APHGameState::SetTotalEnergy(float NewValue)
{
	TotalEnergy = NewValue;

	if (OnTotalEnergyChanged.IsBound())
	{
		OnTotalEnergyChanged.Broadcast(TotalEnergy);
	}
}

void APHGameState::SetPortalEnergy(float NewValue)
{
	PortalEnergy = NewValue;

	if (OnPortalEnergyChanged.IsBound())
	{
		OnPortalEnergyChanged.Broadcast(PortalEnergy);
	}
}
