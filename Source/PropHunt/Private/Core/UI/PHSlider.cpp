// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/PHSlider.h"

#include "Widgets/Input/SSlider.h"


TSharedRef<SWidget> UPHSlider::RebuildWidget()
{
	const auto& SliderSharedRef = Super::RebuildWidget();

	OnMouseCaptureBegin.AddDynamic(this, &UPHSlider::HandleOnCaptureBegin);
	OnMouseCaptureEnd.AddDynamic(this, &UPHSlider::HandleOnCaptureEnd);
	OnControllerCaptureBegin.AddDynamic(this, &UPHSlider::HandleOnCaptureBegin);
	OnControllerCaptureEnd.AddDynamic(this, &UPHSlider::HandleOnCaptureEnd);

	return SliderSharedRef;
}

void UPHSlider::HandleOnCaptureBegin()
{
	bThumbCaptured = true;
}

void UPHSlider::HandleOnCaptureEnd()
{
	bThumbCaptured = false;
	bAdjusting = true;
}

void UPHSlider::Tick(float DeltaTime)
{
	if (!bThumbCaptured)
	{
		if (bAdjusting)
		{
			const float SnappedValue = FMath::GridSnap(GetValue(), StepSize);
			if (!SetValueCustom(SnappedValue, DeltaTime))
			{
				bAdjusting = false;
			}
		}
		else if (bSwitchAdjusting)
		{
			if (!SetValueCustom(SwitchState, DeltaTime))
			{
				bSwitchAdjusting = false;
			}
		}
	}
}

void UPHSlider::Switch()
{
	SwitchState = SwitchState == 0.f ? 1.f : 0.f;
	bSwitchAdjusting = true;
}

bool UPHSlider::SetValueCustom(float InValue, float DeltaTime)
{
	const bool bEqual = FMath::IsNearlyEqual(GetValue(), InValue, 0.01f);
	if (!bEqual)
	{
		float NewValue = GetValue();
		if (bAnimationScaledByDistance)
		{
			NewValue = FMath::FInterpTo(GetValue(), InValue, DeltaTime, 10.f);
		}
		else
		{
			NewValue = FMath::FInterpConstantTo(GetValue(), InValue, DeltaTime, 10.f);
		}

		SetValue(NewValue);

		return true;
	}

	return false;
}
