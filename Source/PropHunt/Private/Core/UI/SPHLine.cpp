﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/SPHLine.h"

#include "SlateOptMacros.h"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SPHLine::Construct(const FArguments& InArgs)
{
	LineColorAndOpacity = InArgs._LineColorAndOpacity;
	Thickness = InArgs._Thickness;
	bHorizontal = InArgs._bHorizontal;
	ShowDisabledEffect = InArgs._ShowEffectWhenDisabled;
}

int32 SPHLine::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	const bool bShowDisabledEffect = ShowDisabledEffect.Get();
	const ESlateDrawEffect DrawEffects = (bShowDisabledEffect && !bParentEnabled) ? ESlateDrawEffect::DisabledEffect : ESlateDrawEffect::None;
	const FLinearColor BorderTint = InWidgetStyle.GetColorAndOpacityTint() * LineColorAndOpacity.Get().GetColor(InWidgetStyle);
	const float HalfThickness = Thickness / 2.f;
	
	const FVector2D& Size = AllottedGeometry.GetLocalSize();
	TArray<FVector2D> Points;
	
	if (bHorizontal)
	{
		Points.Add({ 0.f, 0.f });
		Points.Add({ Size.X, 0.f });
	}
	else
	{
		Points.Add({ 0.f, 0.f });
		Points.Add({ 0.f, Size.Y });
	}
	
	FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), Points, DrawEffects, BorderTint, true, Thickness);
	
	const bool bEnabled = ShouldBeEnabled(bParentEnabled);
	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bEnabled);
}

void SPHLine::SetThickness(float InThickness)
{
	Thickness = InThickness;
}

void SPHLine::SetLineColorAndOpacity(const TAttribute<FSlateColor>& InColorAndOpacity)
{
	SetAttribute(LineColorAndOpacity, InColorAndOpacity, EInvalidateWidgetReason::Paint);
}

void SPHLine::SetHorizontal(bool bInHorizontal)
{
	bHorizontal = bInHorizontal;
}

void SPHLine::SetShowEffectWhenDisabled(const TAttribute<bool>& InShowEffectWhenDisabled)
{
	SetAttribute(ShowDisabledEffect, InShowEffectWhenDisabled, EInvalidateWidgetReason::Paint);
}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION
