﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/PHDosBorder.h"

#include "Core/UI/PHDosBorderSlot.h"

#include "Engine/Font.h"
#include "ObjectEditorUtils.h"


UPHDosBorder::UPHDosBorder(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	HorizontalAlignment(HAlign_Fill),
	VerticalAlignment(VAlign_Fill),
	bShowEffectWhenDisabled(true),
	ContentColorAndOpacity(FLinearColor::White),
	Padding(FMargin(20.f, 30.f)),
	DesiredSizeScale(FVector2D(1)),
	bShowHeader(true),
	Thickness(4.f),
	BlankOffset(80.f),
	SpaceWidth(6.f),
	BlankPosition(EHeaderPosition::Left),
	bDoubleBorder(true),
	BorderColorAndOpacity(FLinearColor::White),
	AnimationDuration(0.5f)
{
	bIsVariable = false;

	if (!IsRunningDedicatedServer())
	{
		static ConstructorHelpers::FObjectFinder<UFont> RobotoFontObj(*UWidget::GetDefaultFontName());
		Font = FSlateFontInfo(RobotoFontObj.Object, 24, FName("Bold"));

		HeaderText = FText::FromString("Header");
	}
}

void UPHDosBorder::SetContentColorAndOpacity(FLinearColor InContentColorAndOpacity)
{
	ContentColorAndOpacity = InContentColorAndOpacity;
	if (MyBorder.IsValid())
	{
		MyBorder->SetColorAndOpacity(InContentColorAndOpacity);
	}
}

void UPHDosBorder::SetPadding(FMargin InPadding)
{
	Padding = InPadding;
	if (MyBorder.IsValid())
	{
		MyBorder->SetPadding(InPadding);
	}
}

void UPHDosBorder::SetHorizontalAlignment(EHorizontalAlignment InHorizontalAlignment)
{
	HorizontalAlignment = InHorizontalAlignment;
	if (MyBorder.IsValid())
	{
		MyBorder->SetHAlign(InHorizontalAlignment);
	}
}

void UPHDosBorder::SetVerticalAlignment(EVerticalAlignment InVerticalAlignment)
{
	VerticalAlignment = InVerticalAlignment;
	if (MyBorder.IsValid())
	{
		MyBorder->SetVAlign(InVerticalAlignment);
	}
}

void UPHDosBorder::SetDesiredSizeScale(FVector2D InScale)
{
	DesiredSizeScale = InScale;
	if (MyBorder.IsValid())
	{
		MyBorder->SetDesiredSizeScale(InScale);
	}
}

void UPHDosBorder::SetHeaderText(FText InHeaderText)
{
	HeaderText = InHeaderText;
	if (MyBorder.IsValid())
	{
		MyBorder->SetHeaderText(InHeaderText);
	}
}

void UPHDosBorder::SetFont(FSlateFontInfo InFontInfo)
{
	Font = InFontInfo;
	if (MyBorder.IsValid())
	{
		MyBorder->SetFont(Font);
	}
}

void UPHDosBorder::SetThickness(float InThickness)
{
	Thickness = InThickness;
	if (MyBorder.IsValid())
	{
		MyBorder->SetThickness(InThickness);
	}
}

void UPHDosBorder::SetShowHeader(bool bInShowHeader)
{
	bShowHeader = bInShowHeader;
	if (MyBorder.IsValid())
	{
		MyBorder->SetShowHeader(bInShowHeader);
	}
}

void UPHDosBorder::SetBlankOffset(float InBlankOffset)
{
	BlankOffset = InBlankOffset;
	if (MyBorder.IsValid())
	{
		MyBorder->SetBlankOffset(InBlankOffset);
	}
}

void UPHDosBorder::SetSpaceWidth(float InSpaceWidth)
{
	SpaceWidth = InSpaceWidth;
	if (MyBorder.IsValid())
	{
		MyBorder->SetSpaceWidth(InSpaceWidth);
	}
}

void UPHDosBorder::SetBlankPosition(EHeaderPosition InBlankPosition)
{
	BlankPosition = InBlankPosition;
	if (MyBorder.IsValid())
	{
		MyBorder->SetBlankPosition(InBlankPosition);
	}
}

void UPHDosBorder::SetDoubleBorder(bool bInDoubleBorder)
{
	bDoubleBorder = bInDoubleBorder;
	if (MyBorder.IsValid())
	{
		MyBorder->SetDoubleBorder(bInDoubleBorder);
	}
}

void UPHDosBorder::SetBorderColorAndOpacity(FLinearColor InBorderColorAndOpacity)
{
	BorderColorAndOpacity = InBorderColorAndOpacity;
	if (MyBorder.IsValid())
	{
		MyBorder->SetBorderColorAndOpacity(InBorderColorAndOpacity);
	}
}

void UPHDosBorder::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	const TAttribute<FLinearColor> ContentColorAndOpacityBinding = PROPERTY_BINDING(FLinearColor, ContentColorAndOpacity);
	
	MyBorder->SetPadding(Padding);
	MyBorder->SetColorAndOpacity(ContentColorAndOpacityBinding);
	
	MyBorder->SetDesiredSizeScale(DesiredSizeScale);
	MyBorder->SetShowEffectWhenDisabled(bShowEffectWhenDisabled);

	MyBorder->SetHeaderText(HeaderText);
	MyBorder->SetFont(Font);

	MyBorder->SetThickness(Thickness);
	MyBorder->SetShowHeader(bShowHeader);
	MyBorder->SetBlankOffset(BlankOffset);
	MyBorder->SetBlankOffset(BlankOffset);
	MyBorder->SetSpaceWidth(SpaceWidth);
	MyBorder->SetBlankPosition(BlankPosition);
	MyBorder->SetDoubleBorder(bDoubleBorder);
	MyBorder->SetBorderColorAndOpacity(BorderColorAndOpacity);
	MyBorder->SetAnimate(bAnimate);
	MyBorder->SetAnimDuration(AnimationDuration);
}

void UPHDosBorder::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	MyBorder.Reset();
}

void UPHDosBorder::PostLoad()
{
	Super::PostLoad();

	if (GetChildrenCount() > 0)
	{
		//TODO UMG Pre-Release Upgrade, now have slots of their own.  Convert existing slot to new slot.
		if (UPanelSlot* PanelSlot = GetContentSlot())
		{
			auto BorderSlot = Cast<UPHDosBorderSlot>(PanelSlot);
			if (!BorderSlot)
			{
				BorderSlot = NewObject<UPHDosBorderSlot>(this);
				BorderSlot->Content = GetContentSlot()->Content;
				BorderSlot->Content->Slot = BorderSlot;
				Slots[0] = BorderSlot;
			}
		}
	}
}

#if WITH_EDITOR
void UPHDosBorder::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	static bool IsReentrant = false;

	if (!IsReentrant)
	{
		IsReentrant = true;

		if ( PropertyChangedEvent.Property )
		{
			static const FName PaddingName("Padding");
			static const FName HorizontalAlignmentName("HorizontalAlignment");
			static const FName VerticalAlignmentName("VerticalAlignment");

			const FName PropertyName = PropertyChangedEvent.Property->GetFName();

			if (auto BorderSlot = Cast<UPHDosBorderSlot>(GetContentSlot()) )
			{
				if (PropertyName == PaddingName)
				{
					FObjectEditorUtils::MigratePropertyValue(this, PaddingName, BorderSlot, PaddingName);
				}
				else if (PropertyName == HorizontalAlignmentName)
				{
					FObjectEditorUtils::MigratePropertyValue(this, HorizontalAlignmentName, BorderSlot, HorizontalAlignmentName);
				}
				else if (PropertyName == VerticalAlignmentName)
				{
					FObjectEditorUtils::MigratePropertyValue(this, VerticalAlignmentName, BorderSlot, VerticalAlignmentName);
				}
			}
		}

		IsReentrant = false;
	}
}

const FText UPHDosBorder::GetPaletteCategory()
{
	return NSLOCTEXT("UMG", "Common", "Common");
}
#endif

TSharedRef<SWidget> UPHDosBorder::RebuildWidget()
{
	MyBorder = SNew(SPHDosBorder);
	
	if (GetChildrenCount() > 0)
	{
		Cast<UPHDosBorderSlot>(GetContentSlot())->BuildSlot(MyBorder.ToSharedRef());
	}

	return MyBorder.ToSharedRef();
}

UClass* UPHDosBorder::GetSlotClass() const
{
	return UPHDosBorderSlot::StaticClass();
}

void UPHDosBorder::OnSlotAdded(UPanelSlot* InSlot)
{
	// Copy the content properties into the new slot so that it matches what has been setup
	// so far by the user.
	UPHDosBorderSlot* BorderSlot = CastChecked<UPHDosBorderSlot>(InSlot);
	BorderSlot->Padding = Padding;
	BorderSlot->HorizontalAlignment = HorizontalAlignment;
	BorderSlot->VerticalAlignment = VerticalAlignment;

	// Add the child to the live slot if it already exists
	if (MyBorder.IsValid())
	{
		// Construct the underlying slot.
		BorderSlot->BuildSlot(MyBorder.ToSharedRef());
	}
}

void UPHDosBorder::OnSlotRemoved(UPanelSlot* InSlot)
{
	// Remove the widget from the live slot if it exists.
	if (MyBorder.IsValid())
	{
		MyBorder->SetContent(SNullWidget::NullWidget);
	}
}
