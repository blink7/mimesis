// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/ButtonWrapper.h"

#include "Components/Border.h"
#include "Components/TextBlock.h"

#include "Engine/World.h"
#include "Rendering/DrawElements.h"


void UButtonWrapper::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (bActive)
	{
		if (IsHovered())
		{
			const float DesiredHoveredtLength = MyGeometry.GetLocalSize().X;
			if (!FMath::IsNearlyEqual(DesiredHoveredtLength, CurrentHoveredLength, 0.01f))
			{
				CurrentHoveredLength = FMath::FInterpTo(CurrentHoveredLength, DesiredHoveredtLength, InDeltaTime, HoverSpeed);
			}
		}
		else if (CurrentHoveredLength > 0.f)
		{
			CurrentHoveredLength = 0.f;
		}

		if (IsHovered() != bHovered)
		{
			bHovered = IsHovered();
			K2_OnHovered(bHovered);
		}
	}
	else if (!bActive && bHovered)
	{
		bHovered = false;
		K2_OnHovered(bHovered);
	}
}

int32 UButtonWrapper::NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	if (bActive && IsHovered())
	{
		FSlateDrawElement::MakeBox(
			OutDrawElements,
			LayerId - 1,
			AllottedGeometry.ToPaintGeometry(FVector2D::ZeroVector, { CurrentHoveredLength, AllottedGeometry.GetLocalSize().Y }),
			&Style.Hovered,
			ESlateDrawEffect::None,
			Style.Hovered.TintColor.GetSpecifiedColor());
	}

	const int32 MaxLayerID = Super::NativePaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);

	return FMath::Max(MaxLayerID, LayerId);
}

void UButtonWrapper::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	LabelText->SetText(Text);
	SetSecondText(SecondText);

	if (!Body->OnMouseButtonDownEvent.IsBoundToObject(this))
	{
		Body->OnMouseButtonDownEvent.BindDynamic(this, &UButtonWrapper::HandleClicked);
	}
	if (!Body->OnMouseButtonUpEvent.IsBoundToObject(this))
	{
		Body->OnMouseButtonUpEvent.BindDynamic(this, &UButtonWrapper::HandleClicked);
	}

	SetIsActive(bActive);
}

FEventReply UButtonWrapper::HandleClicked(FGeometry MyGeometry, const FPointerEvent& MouseEvent)
{
	if (!bActive)
	{
		return FEventReply();
	}

	if (bPressed)
	{
		OnClicked.Broadcast();
	}

	bPressed = !bPressed;
	K2_OnClicked(bPressed);

	return FEventReply(true);
}

void UButtonWrapper::SetIsActive(bool bInActive)
{
	bActive = bInActive;
	if (bActive)
	{
		LabelText->SetRenderOpacity(1.f);
		SecondLabelText->SetRenderOpacity(1.f);
	}
	else
	{
		LabelText->SetRenderOpacity(InactiveOpacity);
		SecondLabelText->SetRenderOpacity(InactiveOpacity);
	}
}

void UButtonWrapper::SetText(FText InText)
{
	Text = InText;
	LabelText->SetText(Text);
}

void UButtonWrapper::SetSecondText(FText InText)
{
	if (SecondText.IsEmpty() && SecondLabelText->GetVisibility() == ESlateVisibility::Visible)
	{
		SecondLabelText->SetVisibility(ESlateVisibility::Collapsed);
	}
	else if (SecondLabelText->GetVisibility() == ESlateVisibility::Collapsed)
	{
		SecondLabelText->SetVisibility(ESlateVisibility::Visible);
	}

	SecondText = InText;
	SecondLabelText->SetText(SecondText);
}
