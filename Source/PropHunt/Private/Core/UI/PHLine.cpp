﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/PHLine.h"

#include "Core/UI/SPHLine.h"


UPHLine::UPHLine(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	bShowEffectWhenDisabled(true),
	Thickness(3.f),
	LineColorAndOpacity(FLinearColor::White),
	bHorizontal(true)
{
	bIsVariable = false;
}

void UPHLine::SetThickness(float InThickness)
{
	Thickness = InThickness;
	if (MyLine.IsValid())
	{
		MyLine->SetThickness(InThickness);
	}
}

void UPHLine::SetLineColorAndOpacity(FLinearColor InLineColorAndOpacity)
{
	LineColorAndOpacity = InLineColorAndOpacity;
	if (MyLine.IsValid())
	{
		MyLine->SetLineColorAndOpacity(InLineColorAndOpacity);
	}
}

void UPHLine::SetHorizontal(bool bInHorizontal)
{
	bHorizontal = bInHorizontal;
	if (MyLine.IsValid())
	{
		MyLine->SetHorizontal(bInHorizontal);
	}
}

void UPHLine::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	MyLine->SetShowEffectWhenDisabled(bShowEffectWhenDisabled);

	MyLine->SetThickness(Thickness);
	MyLine->SetLineColorAndOpacity(LineColorAndOpacity);
	MyLine->SetHorizontal(bHorizontal);
}

void UPHLine::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	MyLine.Reset();
}

#if WITH_EDITOR
const FText UPHLine::GetPaletteCategory()
{
	return NSLOCTEXT("UMG", "Primitive", "Primitive");
}
#endif

TSharedRef<SWidget> UPHLine::RebuildWidget()
{
	MyLine = SNew(SPHLine);
	return MyLine.ToSharedRef();
}
