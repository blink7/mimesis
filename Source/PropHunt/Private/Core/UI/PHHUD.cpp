﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/PHHUD.h"

#include "Weapons/Weapon.h"
#include "Core/PHGameInstance.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/Widgets/HUDWidget.h"
#include "Core/UI/Widgets/KillLogEntry.h"
#include "Core/Abilities/PHGameplayEffectUIData_Damage.h"
#include "Character/Hunter/HunterCharacter.h"

#include "Engine/Canvas.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/InputSettings.h"

#define LOCTEXT_NAMESPACE "HUD"

void APHHUD::DrawHUD()
{
	Super::DrawHUD();

	DrawCrosshair();
}

void APHHUD::ShowHUD()
{
	ShowHUD(!bShowHUD);
}

void APHHUD::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	HUDWidget = CreateWidget<UHUDWidget>(GetWorld(), HUDWidgetClass);
	if (HUDWidget)
	{
		HUDWidget->AddToViewport();
	}

	Setup();
	BindEvents();
}

void APHHUD::BeginPlay()
{
	Super::BeginPlay();

	const bool bCinematicMode = GetOwningPlayerController()->bCinematicMode;
	if (bCinematicMode)
	{
		ShowHUD(false);
	}
}

void APHHUD::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	RemoveHUDWidget();
	
	Super::EndPlay(EndPlayReason);
}

void APHHUD::Setup()
{
	const auto PlayerState = GetOwningPlayerController()->GetPlayerState<APHPlayerState>();
	if (PlayerState && HUDWidget)
	{
		if (PlayerState->IsProp())
		{
			HUDWidget->OnSetupStatusBox(false);
			HUDWidget->OnSetCrosshairVisibility(true);
			HUDWidget->OnSetHealthBoxVisibility(true);
		}
		else if (PlayerState->IsHunter())
		{
			HUDWidget->OnSetupStatusBox(true);
			HUDWidget->OnSetCrosshairVisibility(false);
			HUDWidget->OnSetHealthBoxVisibility(true);
		}
		else
		{
			HUDWidget->OnSetCrosshairVisibility(false);
			HUDWidget->OnSetHealthBoxVisibility(false);
		}

		HUDWidget->OnUpdateColorScheme(PlayerState->GetTeamNumber());

		SetupSkillBox();
		UpdatePlayerStateMaps();

		if (!PlayerState->OnTeamChanged.IsBoundToObject(this))
		{
			PlayerState->OnTeamChanged.BindUObject(this, &APHHUD::Setup);
		}
	}
	else
	{
		// Keep retrying until Player State is ready
		FTimerHandle TimerHandle_Unused;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Unused, this, &APHHUD::Setup, 0.5f, false);
	}
}

void APHHUD::RemoveHUDWidget()
{
	if (HUDWidget)
	{
		HUDWidget->RemoveFromParent();
		HUDWidget = nullptr;
	}
}

void APHHUD::ShowHUD(bool bShow)
{
	bShowHUD = bShow;
	
	if (HUDWidget)
	{
		HUDWidget->SetVisibility(bShow ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
	}
}

void APHHUD::ShowDeathMessage(APHPlayerState* KillerPlayerState, APHPlayerState* VictimPlayerState, UGameplayEffectUIData* UIData)
{
	const auto MyPlayerState = GetOwningPlayerController()->GetPlayerState<APHPlayerState>();
	if (VictimPlayerState && MyPlayerState && HUDWidget)
	{
		FKillLogItemData NewMessage;
		NewMessage.KillerDesc = KillerPlayerState ? KillerPlayerState->GetShortPlayerName() : "";
		NewMessage.VictimDesc = VictimPlayerState->GetShortPlayerName();
		NewMessage.KillerTeamNum = KillerPlayerState ? KillerPlayerState->GetTeamNumber() : INDEX_NONE;
		NewMessage.VictimTeamNum = VictimPlayerState->GetTeamNumber();
		NewMessage.bKillerIsOwner = MyPlayerState == KillerPlayerState;
		NewMessage.bVictimIsOwner = MyPlayerState == VictimPlayerState;

		const auto MyUIData = Cast<UPHGameplayEffectUIData_Damage>(UIData);
		if (MyUIData)
		{
			NewMessage.KillIcon = MyUIData->KillIcon;
		}

		HUDWidget->AddDeathMessage(NewMessage);

		if (VictimPlayerState == MyPlayerState)
		{
			HUDWidget->OnShowDeadMessage(MyPlayerState->IsProp());
			HUDWidget->OnSetCrosshairVisibility(false);
			HUDWidget->OnSetHealthBoxVisibility(false);
		}
	}
}

void APHHUD::ShowBonus(EBonusType BonusType, int32 Value)
{
	if (HUDWidget)
	{
		switch (BonusType)
			{
			case EBonusType::BONUS_Taunt:
				HUDWidget->AddMessage(FText::Format(LOCTEXT("Bonus1", "Taunt bonus: <Hightlight>+{0}</>"), Value), 4.f);
				break;
			case EBonusType::BONUS_Morphing:
				HUDWidget->AddMessage(FText::Format(LOCTEXT("Bonus2", "Morphing bonus: <Hightlight>+{0}</>"), Value), 4.f);
				break;
			case EBonusType::BONUS_Survival:
				HUDWidget->AddMessage(FText::Format(LOCTEXT("Bonus3", "Survival bonus: <Hightlight>+{0}</>"), Value), 4.f);
				break;
			}
	}
}

void APHHUD::ShowMorphingSizeError()
{
	if (HUDWidget)
	{
		HUDWidget->AddMessage(LOCTEXT("MorphingSizeError", "Not enough <Hightlight>space</>"), 2.f);
	}
}

void APHHUD::ShowInfoMessage(EInfoMessageType MessageType, float Duration)
{
	if (HUDWidget)
	{
		switch (MessageType)
		{
			case EInfoMessageType::INFO_Hiding:
				HUDWidget->AddMessage(LOCTEXT("Message1", "Waiting while Props are hiding"), Duration);
				break;
			case EInfoMessageType::INFO_RoundEnd:
				HUDWidget->AddMessage(LOCTEXT("Message2", "Waiting for the game restart"), Duration);
				break;
			case EInfoMessageType::INFO_UpdateStatus:
				HUDWidget->AddMessage(FText::Format(LOCTEXT("Message3", "Press <Hightlight>[{0}]</> when ready"), GetKeyName(TEXT("UpdateStatus"))), Duration);
				break;
			case EInfoMessageType::INFO_Portal:
				HUDWidget->AddMessage(LOCTEXT("Message4", "Charge the Portal to escape"), Duration);
				break;
		}
	}
}

void APHHUD::ShowWinner(int32 TeamNumber)
{
	const auto MyPlayerState = GetOwningPlayerController()->GetPlayerState<APHPlayerState>();
	if (MyPlayerState && HUDWidget)
	{
		if (TeamNumber == ETeamType::Hunter)
		{
			HUDWidget->AddMessage(LOCTEXT("Winner1", "<Hightlight>Hunters Win!</>"));
		}
		else if (TeamNumber == ETeamType::Prop)
		{
			HUDWidget->AddMessage(LOCTEXT("Winner2", "<Hightlight>Props Win!</>"));
		}
	}
}

void APHHUD::OpenChat(bool bTeammate) const
{
	if (HUDWidget && bShowHUD)
	{
		HUDWidget->OpenChat(bTeammate);
	}
}

void APHHUD::AddChatLine(APlayerState* SenderPlayerState, const FString& Message, FName Type, float MsgLifeTime)
{
	const auto SenderPS = Cast<APHPlayerState>(SenderPlayerState);
	const auto MyPS = GetOwningPlayerController()->GetPlayerState<APHPlayerState>();
	if (HUDWidget && SenderPS && MyPS)
	{
		const bool bTeammate = Type == EMessageType::Team;
		if (bTeammate && SenderPS->GetTeamNumber() != MyPS->GetTeamNumber())
		{
			return;
		}
	
		HUDWidget->AddChatLine(SenderPS->GetShortPlayerName(), Message, SenderPS->GetTeamNumber(), bTeammate, Type == EMessageType::Server);
	}
}

void APHHUD::AddPlayersToMinimap(const TArray<APawn*>& PlayerPawns)
{
	if (HUDWidget)
	{
		for (APawn* PlayerPawn : PlayerPawns)
		{
			HUDWidget->AddPlayerToMinimap(PlayerPawn);
		}
	}
}

void APHHUD::ClearPlayersFromMinimap()
{
	if (HUDWidget)
	{
		HUDWidget->ClearPlayersFromMinimap();
	}
}

void APHHUD::ZoomMinimap() const
{
	if (HUDWidget)
	{
		HUDWidget->ZoomMinimap();
	}
}

void APHHUD::AddPointOfInterest(AActor* TargetActor, EPointOfInterest DisplayType, UTexture2D* MinimapIcon, UTexture2D* ScreenIcon, float Size, FLinearColor Color, bool bStatic) const
{
	if (HUDWidget)
	{
		switch (DisplayType)
		{
			case EPointOfInterest::POI_Minimap:
				HUDWidget->AddTargetOnMinimap(TargetActor, MinimapIcon, Color, bStatic);
				break;
			case EPointOfInterest::POI_Screen:
				HUDWidget->AddTargetOnScreen(TargetActor, ScreenIcon, Color, Size);
				break;
			case EPointOfInterest::POI_Both:
				HUDWidget->AddTargetOnScreen(TargetActor, ScreenIcon, Color, Size);
				HUDWidget->AddTargetOnMinimap(TargetActor, MinimapIcon, Color, bStatic);
		}
	}
}

void APHHUD::RemovePointOfInterest(AActor* TargetActor) const
{
	if (HUDWidget)
	{
		HUDWidget->RemovePointOfInterest(TargetActor);
	}
}

void APHHUD::NotifyEnemyHit() const
{
	if (HUDWidget)
	{
		HUDWidget->NotifyEnemyHit();
	}
}

void APHHUD::UpdateReadyStatus(int32 ReadyPlayers, int32 TotalPlayers)
{
	if (HUDWidget)
	{
		const FText Message = FText::Format(LOCTEXT("ReadyPlayers", "Ready players: <Hightlight>{0}/{1}</>"), ReadyPlayers, TotalPlayers);
		HUDWidget->AddMessage(Message, 0.f, true);
	}
}

void APHHUD::ShowPortalStatus() const
{
	if (HUDWidget)
	{
		HUDWidget->OnShowPortalStatus();
	}
}

void APHHUD::StartInteractionTimer(float Duration)
{
	if (HUDWidget)
	{
		HUDWidget->StartInteractionTimer(Duration);
	}
}

void APHHUD::StopInteractionTimer() const
{
	if (HUDWidget)
	{
		HUDWidget->StopInteractionTimer();
	}
}

void APHHUD::ShowHint(const FHint& Hint)
{
	if (HUDWidget)
	{
		HUDWidget->OnShowHint(Hint);
	}
}

void APHHUD::BindEvents()
{
	if (const auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		GameState->OnRemainingTimeChanged.AddDynamic(this, &APHHUD::OnRemainingTimeChanged);
	}
	else
	{
		// Keep retrying until Game State is ready
		FTimerHandle TimerHandle_Unused;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Unused, this, &APHHUD::BindEvents, 0.5f, false);
	}
}

void APHHUD::OnRemainingTimeChanged(int32 Time)
{
	if (HUDWidget)
	{
		HUDWidget->SetTimer(Time);
	}

	UpdatePlayerStateMaps();
}

void APHHUD::SetupSkillBox()
{
	const auto PlayerState = GetOwningPlayerController()->GetPlayerState<APHPlayerState>();
	if (PlayerState && HUDWidget)
	{
		const FPrimaryAssetId& Skill = PlayerState->GetCharacterSkill();
		if (Skill.IsValid())
		{
			if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
			{
				UPHItem* SelectedAbility = GameInstance->GetAbilityItem(PlayerState->GetCharacterSkill());
				if (SelectedAbility)
				{
					HUDWidget->OnSetupSkillBox(SelectedAbility);
				}
			}
		}
		else if (!PlayerState->OnCharacterSkillSet.IsBoundToObject(this))
		{
			PlayerState->OnCharacterSkillSet.BindUObject(this, &APHHUD::SetupSkillBox);
		}
	}
}

void APHHUD::UpdatePlayerStateMaps()
{
	const auto GameState = GetWorld()->GetGameState<APHGameState>();
	const auto MyPlayerState = GetOwningPlayerController()->GetPlayerState<APHPlayerState>();
	if (GameState && MyPlayerState)
	{
		bool bRequiresWidgetUpdate = false;

		const int32 NumTeams = FMath::Max(GameState->NumTeams, 1);
		if (LastTeamLivePlayerCount.Num() < NumTeams)
		{
			LastTeamLivePlayerCount.AddZeroed(NumTeams);
		}

		for (int32 TeamNumber = 0; TeamNumber < NumTeams; TeamNumber++)
		{
			const uint8 LiveTeamPlayers = GameState->GetLivePlayerCount(TeamNumber);
			if (LastTeamLivePlayerCount[TeamNumber] != LiveTeamPlayers)
			{
				bRequiresWidgetUpdate = true;
				LastTeamLivePlayerCount[TeamNumber] = LiveTeamPlayers;
			}
		}

		if (bRequiresWidgetUpdate)
		{
			UpdateMatchStats(MyPlayerState->GetTeamNumber());
		}
	}
}

void APHHUD::UpdateMatchStats(int32 OwnerTeamNumber)
{
	const auto GameState = GetWorld()->GetGameState<APHGameState>();
	if (GameState && HUDWidget)
	{
		const FText PropTeam(LOCTEXT("Props", "Props:"));
		const FText HunterTeam(LOCTEXT("Hunters", "Hunters:"));
		OwnerTeamNumber = FMath::Clamp(OwnerTeamNumber, 0, 1);
		
		if (OwnerTeamNumber == ETeamType::Prop)
		{
			HUDWidget->SetupMatchStats(PropTeam, HunterTeam, OwnerTeamNumber);
		}
		else
		{
			HUDWidget->SetupMatchStats(HunterTeam, PropTeam, OwnerTeamNumber);
		}
		
		const uint8 LivePlayersTeamA = LastTeamLivePlayerCount[OwnerTeamNumber];
		const uint8 LivePlayersTeamB = LastTeamLivePlayerCount[!OwnerTeamNumber];

		const uint8 TotalPlayersTeamA = GameState->GetActivePlayersNumber(OwnerTeamNumber);
		const uint8 TotalPlayersTeamB = GameState->GetActivePlayersNumber(!OwnerTeamNumber);
			
		HUDWidget->UpdateMatchStats(LivePlayersTeamA, TotalPlayersTeamA, LivePlayersTeamB, TotalPlayersTeamB);
	}
}

FText APHHUD::GetKeyName(const FName& InActionName) const
{
	TArray<FInputActionKeyMapping> Mappings;
	GetDefault<UInputSettings>()->GetActionMappingByName(InActionName, Mappings);

	return Mappings[0].Key.GetDisplayName();
}

void APHHUD::DrawCrosshair()
{
	const auto HunterCharacter = Cast<AHunterCharacter>(GetOwningPawn());
	if (HunterCharacter && HunterCharacter->IsAdmitted())
	{
		AWeapon* MyWeapon = HunterCharacter->GetWeapon();
		if (MyWeapon && !HunterCharacter->IsRunning() && (HunterCharacter->IsTargeting() || (!HunterCharacter->IsTargeting() && !HunterCharacter->GetWeapon()->bHideCrosshairWhileNotAiming)))
		{
			const float CurrentTime = GetWorld()->GetTimeSeconds();

			float AnimOffset = 0.f;
			if (MyWeapon)
			{
				const float EquipStartedTime = MyWeapon->GetEquipStartedTime();
				const float EquipDuration = MyWeapon->GetEquipDuration();
				AnimOffset = 300.f * (1.f - FMath::Min(1.f, (CurrentTime - EquipStartedTime) / EquipDuration));
			}
			float CrossSpread = 2.f + AnimOffset;
			if (MyWeapon)
			{
				constexpr float SpreadMulti = 600.f;
				CrossSpread += SpreadMulti * FMath::Tan(FMath::DegreesToRadians(MyWeapon->GetCurrentSpread()));
			}

			const float CenterX = Canvas->ClipX / 2;
			const float CenterY = Canvas->ClipY / 2;

			FLinearColor CrossCenterColor;

			if (HunterCharacter->IsTargeting() && MyWeapon && MyWeapon->bUseLaserDot)
			{
				CrossCenterColor = FLinearColor::Red;
			}
			else
			{
				CrossCenterColor = CrossColor;

				const float RightX = CenterX + CrossSpread;
				DrawLine(RightX, CenterY, RightX + CrossLength, CenterY, CrossColor, CrossThickness);

				const float LeftX = CenterX - CrossSpread;
				DrawLine(LeftX, CenterY, LeftX - CrossLength, CenterY, CrossColor, CrossThickness);

				const float TopY = CenterY + CrossSpread;
				DrawLine(CenterX, TopY, CenterX, TopY + CrossLength, CrossColor, CrossThickness);

				const float BottomY = CenterY - CrossSpread;
				DrawLine(CenterX, BottomY, CenterX, BottomY - CrossLength, CrossColor, CrossThickness);
			}

			DrawRect(CrossCenterColor, CenterX - CrossSize.X / 2.f, CenterY - CrossSize.Y / 2.f, CrossSize.X, CrossSize.Y);
		}
	}
}

#undef LOCTEXT_NAMESPACE
