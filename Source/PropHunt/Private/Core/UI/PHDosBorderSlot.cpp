﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/PHDosBorderSlot.h"

#include "Core/UI/PHDosBorder.h"
#include "Core/UI/SPHDosBorder.h"

#include "ObjectEditorUtils.h"


UPHDosBorderSlot::UPHDosBorderSlot(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	Padding = FMargin(20.f, 30.f);

	HorizontalAlignment = HAlign_Fill;
	VerticalAlignment = VAlign_Fill;
}

void UPHDosBorderSlot::SetPadding(FMargin InPadding)
{
	CastChecked<UPHDosBorder>(Parent)->SetPadding(InPadding);
}

void UPHDosBorderSlot::SetHorizontalAlignment(EHorizontalAlignment InHorizontalAlignment)
{
	CastChecked<UPHDosBorder>(Parent)->SetHorizontalAlignment(InHorizontalAlignment);
}

void UPHDosBorderSlot::SetVerticalAlignment(EVerticalAlignment InVerticalAlignment)
{
	CastChecked<UPHDosBorder>(Parent)->SetVerticalAlignment(InVerticalAlignment);
}

void UPHDosBorderSlot::SynchronizeProperties()
{
	if (Border.IsValid())
	{
		SetPadding(Padding);
		SetHorizontalAlignment(HorizontalAlignment);
		SetVerticalAlignment(VerticalAlignment);
	}
}

void UPHDosBorderSlot::BuildSlot(TSharedRef<SPHDosBorder> InBorder)
{
	Border = InBorder;

	Border.Pin()->SetPadding(Padding);
	Border.Pin()->SetHAlign(HorizontalAlignment);
	Border.Pin()->SetVAlign(VerticalAlignment);

	Border.Pin()->SetContent(Content ? Content->TakeWidget() : SNullWidget::NullWidget);
}

void UPHDosBorderSlot::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	Border.Reset();
}

#if WITH_EDITOR
void UPHDosBorderSlot::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	static bool IsReentrant = false;

	if (!IsReentrant)
	{
		IsReentrant = true;

		if ( PropertyChangedEvent.Property )
		{
			static const FName PaddingName("Padding");
			static const FName HorizontalAlignmentName("HorizontalAlignment");
			static const FName VerticalAlignmentName("VerticalAlignment");

			const FName PropertyName = PropertyChangedEvent.Property->GetFName();

			if (auto ParentBorder = CastChecked<UPHDosBorder>(Parent))
			{
				if ( PropertyName == PaddingName)
				{
					FObjectEditorUtils::MigratePropertyValue(this, PaddingName, ParentBorder, PaddingName);
				}
				else if ( PropertyName == HorizontalAlignmentName)
				{
					FObjectEditorUtils::MigratePropertyValue(this, HorizontalAlignmentName, ParentBorder, HorizontalAlignmentName);
				}
				else if ( PropertyName == VerticalAlignmentName)
				{
					FObjectEditorUtils::MigratePropertyValue(this, VerticalAlignmentName, ParentBorder, VerticalAlignmentName);
				}
			}
		}

		IsReentrant = false;
	}
}
#endif
