// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/MainMenu.h"

#include "Core/PHTypes.h"
#include "Core/PHGameInstance.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/UI/Widgets/Dialog.h"
#include "Core/UI/Menu/RatingMenu.h"
#include "Core/UI/Menu/SecondaryMenu.h"
#include "Core/Online/PHPlayerState.h"

#include "Components/Border.h"
#include "Components/WidgetSwitcher.h"
#include "Slate/SceneViewport.h"
#include "LevelSequence/Public/LevelSequenceActor.h"
#include "LevelSequence/Public/LevelSequencePlayer.h"

#define LOCTEXT_NAMESPACE "Menu"

UMainMenu::UMainMenu(const FObjectInitializer& ObjectInitializer)
{
	bShowMouseCursor = true;
	bRespondUIOnly = true;
}

void UMainMenu::NativePreConstruct()
{
	Super::NativePreConstruct();

	if (!CreateServerButton->OnClicked.IsBound())
	{
		CreateServerButton->OnClicked.AddUObject(this, &UMainMenu::OnPlaySequenceAndOpenMenu, static_cast<uint8>(EMenuType::CreateServer));
	}
	if (!FindServerButton->OnClicked.IsBound())
	{
		FindServerButton->OnClicked.AddUObject(this, &UMainMenu::OnPlaySequenceAndOpenMenu, static_cast<uint8>(EMenuType::FindServer));
	}
	if (!ProfileButton->OnClicked.IsBound())
	{
		ProfileButton->OnClicked.AddUObject(this, &UMainMenu::OnPlaySequenceAndOpenMenu, static_cast<uint8>(EMenuType::Profile));
	}
	if (!RatingButton->OnClicked.IsBound())
	{
		RatingButton->OnClicked.AddUObject(this, &UMainMenu::OnPlaySequenceAndOpenMenu, static_cast<uint8>(EMenuType::Rating));
	}
	if (!SettingsButton->OnClicked.IsBound())
	{
		SettingsButton->OnClicked.AddUObject(this, &UMainMenu::OnPlaySequenceAndOpenMenu, static_cast<uint8>(EMenuType::Settings));
	}
	if (!QuitButton->OnClicked.IsBound())
	{
		QuitButton->OnClicked.AddUObject(this, &UMainMenu::OnQuit);
	}
}

void UMainMenu::NativeConstruct()
{
	Super::NativeConstruct();

	SetFocus();

	if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		GameInstance->SetMainMenu(this);

		GameInstance->OnPatchReady.AddDynamic(this, &UMainMenu::OnPatchReady);
		GameInstance->OnPatchComplete.AddDynamic(this, &UMainMenu::OnPatchComplete);
		GameInstance->InitPatching("Windows");
	}

	if (SequenceActor)
	{
		SequenceActor->GetSequencePlayer()->OnFinished.AddDynamic(this, &UMainMenu::OnOpenMenu);
	}

	if (const auto PlayerState = GetOwningPlayer()->GetPlayerState<APHPlayerState>())
	{
		if (!PlayerState->IsPlayerLoaded())
		{
			PlayerState->OnLoadPlayerCompleteDelegate.BindUObject(this, &UMainMenu::OnPlayerLoaded);
		}
		else
		{
			OnPlayerLoaded();
		}
	}
}

void UMainMenu::NativeOnFocusLost(const FFocusEvent& InFocusEvent)
{
	Super::NativeOnFocusLost(InFocusEvent);

	UWorld* World = GetWorld();
	if (World && World->GetGameViewport()->GetGameViewport()->HasFocus())
	{
		//Don't allow to lose focus when click outside the monitor
		SetUserFocus(GetOwningPlayer());
	}
}

FReply UMainMenu::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	if (bPlayingSequence || bPatchingGame) //lock input
	{
		return FReply::Handled();
	}

	FReply Result = FReply::Unhandled();

	if (Dialog)
	{
		return Result; //Dialog has priority
	}

	const FKey Key = InKeyEvent.GetKey();

	if (Key == EKeys::Escape || Key == EKeys::Virtual_Back)
	{
		OnQuit();

		Result = FReply::Handled();
	}

	return Result;
}

void UMainMenu::Setup(class USecondaryMenu* InSecondaryMenu, class URatingMenu* InRatingMenu)
{
	SecondaryMenu = InSecondaryMenu;
	RatingMenu = InRatingMenu;

	if (SecondaryMenu)
	{
		SecondaryMenu->OnReturnToMainMenuEvent.AddUObject(this, &UMainMenu::ReturnToMe);
	}

	if (RatingMenu)
	{
		RatingMenu->OnReturnToMainMenuEvent.AddUObject(this, &UMainMenu::ReturnToMe);
		RatingMenu->OnShowMeEvent.AddUObject(this, &UMainMenu::OnPlaySequenceAndOpenMenu, static_cast<uint8>(EMenuType::Rating));
	}
}

void UMainMenu::OnQuit()
{
	Dialog = CreateWidget<UDialog>(GetWorld(), DialogClass);
	if (Dialog)
	{
		Dialog->ConstructMessageDialog(LOCTEXT("Quit", "Quit Game"), 
			LOCTEXT("QuitMessage", "Are you sure you want to exit the game?"), 
			NSLOCTEXT("DialogButtons", "Ok", "Ok"),
			NSLOCTEXT("DialogButtons", "Cancel", "Cancel"));
		Dialog->OnConfirmClicked.AddUObject(this, &UMainMenu::ConfirmQuit);
		Dialog->OnCancelClicked.AddUObject(this, &UMainMenu::HideMessage);

		DialogContainer->AddChild(Dialog);
	}
}

void UMainMenu::ConfirmQuit()
{
	GetOwningPlayer()->ConsoleCommand("quit");
}

void UMainMenu::OnPlaySequenceAndOpenMenu(uint8 InMenuType)
{
	if (bPatchingGame)
	{
		return;
	}
	
	if (!SecondaryMenu)
	{
		return;
	}
	if (!RatingMenu)
	{
		return;
	}
	
	CurrentMenuType = InMenuType;

	if (SequenceActor)
	{
		bPlayingSequence = true;

		SequenceActor->GetSequencePlayer()->Stop();

		if (EMenuType::Rating == CurrentMenuType)
		{
			SequenceActor->SetSequence(RatingMenuSequence);
		}
		else
		{
			SequenceActor->SetSequence(SecondaryMenuSequence);
		}

		SequenceActor->GetSequencePlayer()->Play();
	}
}

void UMainMenu::OnOpenMenu()
{
	if (EMenuType::Rating == CurrentMenuType)
	{
		if (RatingMenu)
		{
			RatingMenu->Setup();
		}
	}
	else if (EMenuType::Main != CurrentMenuType)
	{
		SecondaryMenu->OnSwitchMenu(CurrentMenuType);
	}

	bPlayingSequence = false;
}

void UMainMenu::OnPlayerLoaded()
{
	if (SecondaryMenu)
	{
		SecondaryMenu->OnPlayerLoaded();
	}

	if (RatingMenu)
	{
		RatingMenu->DisplayRating();
	}

	if (ProfileButton)
	{
		ProfileButton->SetIsActive(true);
	}
	if (RatingButton)
	{
		RatingButton->SetIsActive(true);
	}
}

void UMainMenu::ReturnToMe()
{
	if (Dialog)
	{
		Dialog->SetUserFocus(GetOwningPlayer());
	}
	else
	{
		SetUserFocus(GetOwningPlayer());
	}

	CurrentMenuType = Main;

	if (SequenceActor)
	{
		bPlayingSequence = true;
		SequenceActor->GetSequencePlayer()->PlayReverse();
	}
}

UDialog* UMainMenu::ShowMessage(const FText& InHeader, const FText& InMessage, const FText& OkButtonString, const FText& CancelButtonString /*= FText::GetEmpty()*/)
{
	if (Dialog)
	{
		Dialog->OnCancelClicked.Broadcast();
	}

	Dialog = CreateWidget<UDialog>(this, DialogClass);
	if (Dialog)
	{
		Dialog->ConstructMessageDialog(InHeader, InMessage, OkButtonString, CancelButtonString);
		Dialog->OnConfirmClicked.AddUObject(this, &UMainMenu::HideMessage);
		Dialog->OnCancelClicked.AddUObject(this, &UMainMenu::HideMessage);

		if (EMenuType::Main == CurrentMenuType || EMenuType::Rating == CurrentMenuType)
		{
			DialogContainer->AddChild(Dialog);
		}
		else
		{
			SecondaryMenu->ShowDialog(Dialog);
		}
	}

	return Dialog;
}

void UMainMenu::HideMessage()
{
	if (Dialog)
	{
		Dialog->RemoveFromParent();
		Dialog = nullptr;

		if (EMenuType::Main != CurrentMenuType)
		{
			SecondaryMenu->ReturnFocusToSubMenu();
		}
	}
}

void UMainMenu::OnPatchReady(bool bSuccess)
{
	if (bSuccess)
	{
		if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
		{
			if (GameInstance->PatchGame())
			{
				OnBeginPatch();
				bPatchingGame = true;
				return;
			}
		}
	}

	OnBeginMenu();
}

void UMainMenu::OnPatchComplete(bool bSuccess)
{
	OnBeginMenu();
	bPatchingGame = false;
}

#undef LOCTEXT_NAMESPACE
