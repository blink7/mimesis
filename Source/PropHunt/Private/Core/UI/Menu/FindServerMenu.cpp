// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/FindServerMenu.h"

#include "Core/PHTypes.h"
#include "Core/UI/Menu/ServerEntry.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/Online/PHGameSession.h"
#include "Core/PHGameInstance.h"
#include "Core/Online/PHOnlineGameSettings.h"

#include "Components/ListView.h"
#include "Components/TextBlock.h"


#define LOCTEXT_NAMESPACE "Menu.Join"

void UFindServerMenu::NativeConstruct()
{
	Super::NativeConstruct();
	
	if (APHGameSession* GameSession = GetGameSession())
	{
		GameSession->OnFindSessionsComplete().AddUObject(this, &UFindServerMenu::OnFindSessionsComplete);
	}

	if (!ServerList->OnItemIsHoveredChanged().IsBound())
	{
		ServerList->OnItemIsHoveredChanged().AddUObject(this, &UFindServerMenu::OnServerHovered);
	}
	if (!ServerList->OnItemDoubleClicked().IsBound())
	{
		ServerList->OnItemDoubleClicked().AddUObject(this, &UFindServerMenu::OnServerDoubleClicked);
	}
	if (!ServerList->OnItemSelectionChanged().IsBound())
	{
		ServerList->OnItemSelectionChanged().AddUObject(this, &UFindServerMenu::OnServerSelectionChanged);
	}

	if (!RefreshButton->OnClicked.IsBound())
	{
		RefreshButton->OnClicked.AddUObject(this, &UFindServerMenu::OnFindServerSelected);
	}
	if (!ConnectButton->OnClicked.IsBound())
	{
		ConnectButton->OnClicked.AddUObject(this, &UFindServerMenu::ConnectToServer);
	}
	if (!ReturnButton->OnClicked.IsBound())
	{
		ReturnButton->OnClicked.AddUObject(this, &UFindServerMenu::ReturnToMainMenu);
	}
}

FReply UFindServerMenu::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	if (bSearchingForServers || bJoiningServer) //lock input
	{
		return FReply::Handled();
	}

	FReply Result = Super::NativeOnKeyDown(InGeometry, InKeyEvent);
	const FKey Key = InKeyEvent.GetKey();

	if (Key == EKeys::Up || Key == EKeys::Gamepad_DPad_Up || Key == EKeys::Gamepad_LeftStick_Up)
	{
		MoveSelection(-1);
		Result = FReply::Handled();
	}
	else if (Key == EKeys::Down || Key == EKeys::Gamepad_DPad_Down || Key == EKeys::Gamepad_LeftStick_Down)
	{
		MoveSelection(1);
		Result = FReply::Handled();
	}
	else if (Key == EKeys::Enter || Key == EKeys::Virtual_Accept)
	{
		ConnectToServer();
		Result = FReply::Handled();
	}
	//Hit space bar to search for servers again / refresh the list, only when not searching already
	else if (Key == EKeys::SpaceBar || Key == EKeys::Gamepad_FaceButton_Left)
	{
		BeginServerSearch();
	}
	return Result;
}

void UFindServerMenu::ReturnToMainMenu()
{
	if (!bSearchingForServers)
	{
		Super::ReturnToMainMenu();
	}
}

void UFindServerMenu::Setup()
{
	OnJoinServer();
}

APHGameSession* UFindServerMenu::GetGameSession() const
{
	auto GameInstance = GetGameInstance<UPHGameInstance>();
	return GameInstance ? GameInstance->GetGameSession() : nullptr;
}

void UFindServerMenu::StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& Delegate)
{
	if (auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		FUniqueNetIdRepl UserId;
		if (GetOwningLocalPlayer())
		{
			UserId = GetOwningLocalPlayer()->GetPreferredUniqueNetId();
		}
		GameInstance->StartOnlinePrivilegeTask(Delegate, EUserPrivileges::CanPlayOnline, *UserId);
	}
}

void UFindServerMenu::OnUserCanPlayOnlineJoin(const FUniqueNetId& UserId, EUserPrivileges::Type Privilege, uint32 PrivilegeResults)
{
	if (auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		CleanupOnlinePrivilegeTask();

		if (PrivilegeResults == static_cast<uint32>(IOnlineIdentity::EPrivilegeResults::NoFailures))
		{
			//make sure to switch to custom match type so we don't instead use Quick type
			MatchType = EMatchType::Custom;

			GameInstance->SetOnlineMode(bIsLanMatch ? EOnlineMode::LAN : EOnlineMode::Online);

			/*MatchType = EMatchType::Custom;
			// Grab the map filter if there is one
			MapFilterName = MapList::Names[0];
			if (MapName->GetSelectedOption().IsEmpty())
			{
			MapFilterName = MapName->GetSelectedOption();
			}*/

			BeginServerSearch();
		}
		else
		{
			GameInstance->DisplayOnlinePrivilegeFailureDialogs(UserId, Privilege, PrivilegeResults); // TODO Display error inside Event Log
		}
	}
}

void UFindServerMenu::CleanupOnlinePrivilegeTask()
{
	if (auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		GameInstance->CleanupOnlinePrivilegeTask();
	}
}

void UFindServerMenu::BeginServerSearch()
{
	const double CurrentTime = FApp::GetCurrentTime();
	if (!bIsLanMatch && CurrentTime - LastSearchTime < MinTimeBetweenSearches)
	{
		OnServerSearchFinished();
	}
	else
	{
		EventLog->SetText(LOCTEXT("Searching", "Searching..."));

		bSearchingForServers = true;
		ServerList->ClearListItems();
		LastSearchTime = CurrentTime;

		if (auto GameInstance = GetGameInstance<UPHGameInstance>())
		{
			GameInstance->FindSessions(GetOwningLocalPlayer(), bIsDedicatedServer, bIsLanMatch);
		}

		RefreshButton->SetIsActive(false);
		ReturnButton->SetIsActive(false);
	}
}

void UFindServerMenu::OnFindSessionsComplete(bool bWasSuccessful)
{
	if (bWasSuccessful)
	{
		if (APHGameSession* GameSession = GetGameSession())
		{
			ServerList->ClearListItems();
			const TArray<FOnlineSessionSearchResult>& SearchResults = GameSession->GetSearchResults();
			if (SearchResults.Num() == 0)
			{
				EventLog->SetText(LOCTEXT("NoServersFound", "No servers found"));
			}
			else
			{
				EventLog->SetText(LOCTEXT("ServersFound", "Search successfully finished"));
			}

			ServerCount->SetText(FText::AsNumber(SearchResults.Num()));

			for (int32 IdxResult = 0; IdxResult < SearchResults.Num(); ++IdxResult)
			{
				const auto& Result = SearchResults[IdxResult];
				const auto NewServerEntry = NewObject<UServerItemData>();

				FString MapName;

				Result.Session.SessionSettings.Get(SETTING_SERVERNAME, NewServerEntry->ServerName);
				Result.Session.SessionSettings.Get(SETTING_MAPNAME, MapName);

				if (MapName.Equals("Lobby"))
				{
					//We don't have Lobby in the list, so just use its raw name
					//TODO: Use name from the list
					NewServerEntry->MapLabel = FText::FromString(MapName);
				}
				else
				{
					NewServerEntry->MapLabel = GetMapLabel(MapName);
				}

				NewServerEntry->Ping = FString::FromInt(Result.PingInMs);
				NewServerEntry->CurrentPlayers = FString::FromInt(Result.Session.SessionSettings.NumPublicConnections
					+ Result.Session.SessionSettings.NumPrivateConnections
					- Result.Session.NumOpenPublicConnections
					- Result.Session.NumOpenPrivateConnections);
				NewServerEntry->MaxPlayers = FString::FromInt(Result.Session.SessionSettings.NumPublicConnections
					+ Result.Session.SessionSettings.NumPrivateConnections);
				NewServerEntry->SearchResultsIndex = IdxResult;

				ServerList->AddItem(NewServerEntry);
			}
		}
	}
	else
	{
		EventLog->SetText(LOCTEXT("SearchError", "Unexpected error has occurred"));
	}

	OnServerSearchFinished();
}

void UFindServerMenu::OnServerSearchFinished()
{
	bSearchingForServers = false;

	RefreshButton->SetIsActive(true);
	ReturnButton->SetIsActive(true);
}

void UFindServerMenu::OnServerHovered(UObject* InHoveredServer, bool bHovered)
{
	if (auto Server = Cast<UServerItemData>(InHoveredServer))
	{
		Server->OnItemHoverEvent.ExecuteIfBound(bHovered);
	}
}

void UFindServerMenu::OnServerSelectionChanged(UObject* InSelectedServer)
{
	if (bJoiningServer)
	{
		return;
	}
	
	if (auto Server = Cast<UServerItemData>(InSelectedServer))
	{
		SelectedServer = Server;
		ConnectButton->SetIsActive(true);
	}
}

void UFindServerMenu::OnServerDoubleClicked(UObject* InDoubleClickedServer)
{
	if (bJoiningServer)
	{
		return;
	}
	
	OnServerSelectionChanged(InDoubleClickedServer);
	ConnectToServer();
}

void UFindServerMenu::OnFindServerSelected()
{
	if (!bSearchingForServers)
	{
		BeginServerSearch();
	}
}

void UFindServerMenu::OnJoinServer()
{
	StartOnlinePrivilegeTask(IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate::CreateUObject(this, &UFindServerMenu::OnUserCanPlayOnlineJoin));
}

void UFindServerMenu::ConnectToServer()
{
	if (bSearchingForServers)
	{
		// unsafe
		return;
	}

#if WITH_EDITOR
	if (GIsEditor)
	{
		return;
	}
#endif

	if (SelectedServer)
	{
		if (auto GameInstance = GetGameInstance<UPHGameInstance>())
		{
			const int32 ServerToJoin = SelectedServer->SearchResultsIndex;
			GameInstance->JoinSession(GetOwningLocalPlayer(), ServerToJoin);
		}

		TurnJoiningStatus();
	}
}

void UFindServerMenu::MoveSelection(int32 MoveBy)
{
	const int32 SelectedItemIndex = ServerList->GetIndexForItem(SelectedServer);
	if (SelectedItemIndex + MoveBy > -1 && SelectedItemIndex + MoveBy < ServerList->GetNumItems())
	{
		ServerList->SetSelectedIndex(SelectedItemIndex + MoveBy);
	}
}

FText UFindServerMenu::GetMapLabel(const FString& MapName)
{
	if (auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		const auto MapEntry = GameInstance->GetMapList().FindByPredicate([MapName](auto Row) {
			return Row->FileName.Equals(MapName);
		});

		if (MapEntry)
		{
			return (*MapEntry)->Label;
		}
	}

	return FText::GetEmpty();
}

void UFindServerMenu::TurnJoiningStatus()
{
	bJoiningServer = true;

	// Disable all buttons
	RefreshButton->SetIsActive(false);
	ConnectButton->SetIsActive(false);
	ReturnButton->SetIsActive(false);
	FilerButton->SetIsActive(false);

	EventLog->SetText(LOCTEXT("Joining", "Joining server..."));
}

#undef LOCTEXT_NAMESPACE
