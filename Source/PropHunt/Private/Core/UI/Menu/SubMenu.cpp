// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/SubMenu.h"

#include "Core/UI/Menu/SecondaryMenu.h"


USubMenu::USubMenu(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bIsFocusable = true;
}

FReply USubMenu::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	FReply Result = Super::NativeOnKeyDown(InGeometry, InKeyEvent);
	const FKey Key = InKeyEvent.GetKey();

	if (Key == EKeys::Escape || Key == EKeys::Virtual_Back)
	{
		ReturnToMainMenu();
		Result = FReply::Handled();
	}

	return Result;
}

void USubMenu::ReturnToMainMenu()
{
	if (SecondaryMenu)
	{
		SecondaryMenu->ReturnToMainMenu();
	}
}
