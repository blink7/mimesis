// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/SettingsItemCell.h"

void USettingsItemCell::NativeOnMouseEnter(const FGeometry& MovieSceneBlends, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(MovieSceneBlends, InMouseEvent);

	OnHovered.Broadcast(true);
}

void USettingsItemCell::NativeOnMouseLeave(const FPointerEvent& MovieSceneBlends)
{
	Super::NativeOnMouseLeave(MovieSceneBlends);

	OnHovered.Broadcast(false);
}
