// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/AbilityMenu.h"

#include "Core/PHTypes.h"
#include "Core/PHGameInstance.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/UI/Menu/AbilityEntry.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/Items/PHSkillItem.h"

#include "Components/TreeView.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

#define LOCTEXT_NAMESPACE "Menu.Ability"

void UAbilityMenu::NativePreConstruct()
{
	Super::NativePreConstruct();

	if (!AbilityList->OnItemSelectionChanged().IsBound())
	{
		AbilityList->OnItemSelectionChanged().AddUObject(this, &UAbilityMenu::OnAbilitySelected);
	}
	if (!AbilityList->OnItemIsHoveredChanged().IsBound())
	{
		AbilityList->OnItemIsHoveredChanged().AddUObject(this, &UAbilityMenu::OnAbilityHovered);
	}
	if (!AbilityList->OnItemExpansionChanged().IsBound())
	{
		AbilityList->OnItemExpansionChanged().AddUObject(this, &UAbilityMenu::OnAbilityExpansionChanged);
	}

	if (!ApplyButton->OnClicked.IsBound())
	{
		ApplyButton->OnClicked.AddUObject(this, &UAbilityMenu::OnClickApply);
	}
}

void UAbilityMenu::NativeConstruct()
{
	Super::NativeConstruct();

	if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		if (!GameInstance->OnSkillItemsLoaded.IsAlreadyBound(this, &UAbilityMenu::LoadAbilities))
		{
			GameInstance->OnSkillItemsLoaded.AddDynamic(this, &UAbilityMenu::LoadAbilities);
		}
	}
}

void UAbilityMenu::LoadAbilities()
{
	const auto PlayerState = GetOwningPlayer()->GetPlayerState<APHPlayerState>();
	if (!PlayerState || !PlayerState->IsPlayerLoaded())
	{
		return;
	}

	AbilityList->ClearListItems();
	
	FillAbilities(ETeamType::Hunter);
	FillAbilities(ETeamType::Prop);

	AbilityList->ExpandAll();

	if (CurrentHunterAbility)
	{
		AbilityList->SetSelectedItem(CurrentHunterAbility);
	}
	else if (CurrentPropAbility)
	{
		AbilityList->SetSelectedItem(CurrentPropAbility);
	}
	else if (AbilityList->GetNumItems() > 0)
	{
		const auto FirstRootItem = Cast<UAbilityItemData>(AbilityList->GetItemAt(0));
		if (FirstRootItem && FirstRootItem->Children.Num() > 0)
		{
			AbilityList->SetSelectedItem(FirstRootItem->Children[0]);
		}
	}
}

void UAbilityMenu::FillAbilities(int32 ForTeam)
{
	const auto GameInstance = GetGameInstance<UPHGameInstance>();
	if (!GameInstance)
	{
		return;
	}
	TArray<UPHSkillItem*> SkillItems = GameInstance->GetSkillItems();
	if (SkillItems.Num() <= 0)
	{
		return;
	}

	auto FilterByTeam = [ForTeam](const UPHItem* Item)
	{
		if (Item->SuitableTeam == EItemSuitableTeam::Both)
		{
			return true;
		}
		if (ForTeam == ETeamType::Hunter)
		{
			return Item->SuitableTeam == EItemSuitableTeam::Hunter;
		}
		if (ForTeam == ETeamType::Prop)
		{
			return Item->SuitableTeam == EItemSuitableTeam::Prop;
		}
		return false;
	};
	
	SkillItems = SkillItems.FilterByPredicate(FilterByTeam);

	auto SortByRequiredLevel = [](const UPHSkillItem& First, const UPHSkillItem& Second)
	{
		return First.RequiredLevel < Second.RequiredLevel;
	};
	
	SkillItems.Sort(SortByRequiredLevel);

	const auto RootItemWidget = NewObject<UAbilityItemData>();
	RootItemWidget->bRoot = true;
	RootItemWidget->Team = ForTeam;

	int32 PlayerLevel = 0;
	FPrimaryAssetId CurrentAbilityId;

	if (const auto PlayerState = GetPlayerContext().GetPlayerState<APHPlayerState>())
	{
		PlayerLevel = PlayerState->GetPlayerLevel();
		CurrentAbilityId = PlayerState->GetSkillId(ForTeam);
	}

	for (UPHSkillItem* SkillItem : SkillItems)
	{
		auto ChildItemWidget = NewObject<UAbilityItemData>();
		ChildItemWidget->Team = ForTeam;
		ChildItemWidget->AbilityItem = SkillItem;

		if (CurrentAbilityId.IsValid() && SkillItem->GetPrimaryAssetId() == CurrentAbilityId)
		{
			ChildItemWidget->Status = EAbilityStatus::Applied;

			if (ForTeam == ETeamType::Hunter)
			{
				CurrentHunterAbility = ChildItemWidget;
			}
			else if (ForTeam == ETeamType::Prop)
			{
				CurrentPropAbility = ChildItemWidget;
			}
		}
		else if (PlayerLevel < SkillItem->RequiredLevel)
		{
			ChildItemWidget->Status = EAbilityStatus::NotAvailable;
		}
		else
		{
			ChildItemWidget->Status = EAbilityStatus::Available;
		}

		RootItemWidget->Children.Add(ChildItemWidget);
	}

	AbilityList->AddItem(RootItemWidget);
}

void UAbilityMenu::OnAbilityHovered(UObject* InAbilityItem, bool bIsHovered)
{
	if (auto Item = Cast<UAbilityItemData>(InAbilityItem))
	{
		Item->OnAbilityHoverEvent.ExecuteIfBound(bIsHovered);
	}
}

void UAbilityMenu::OnAbilitySelected(UObject* InAbilityItem)
{
	const auto Item = Cast<UAbilityItemData>(InAbilityItem);
	if (Item && !Item->bRoot)
	{
		UpdateDetails(Item);

		SelectedAbility = Item;
	}
}

void UAbilityMenu::OnAbilityExpansionChanged(UObject* InAbilityItem, bool bExpanded)
{
	if (bExpanded)
	{
		const auto Item = Cast<UAbilityItemData>(AbilityList->GetSelectedItem());
		if (Item && Item->bRoot)
		{
			if (SelectedAbility)
			{
				AbilityList->SetSelectedItem(SelectedAbility);
			}
			else if (const auto FirstItem = Cast<UAbilityItemData>(AbilityList->GetItemAt(0)))
			{
				AbilityList->SetSelectedItem(FirstItem->Children[0]);
			}
		}
	}
}

void UAbilityMenu::UpdateDetails(UAbilityItemData* ItemData)
{
	const UPHItem* Item = ItemData->AbilityItem;
	Description->SetText(Item->ItemDescription);
	RequiredLvl->SetText(FText::AsNumber(Item->RequiredLevel));

	switch (Item->UsageType)
	{
		case EItemUsageType::Energy:
			UsesEnergy->SetText(LOCTEXT("Energy", "Energy"));
			break;
		case EItemUsageType::Amount:
			UsesEnergy->SetText(LOCTEXT("Amount", "Amount"));
			break;
		case EItemUsageType::Passive:
			UsesEnergy->SetText(LOCTEXT("Passive", "Passive"));
			break;
	}

	if (ItemData->Status == EAbilityStatus::NotAvailable)
	{
		ApplyButton->SetIsActive(false);
		RequiredLvl->SetColorAndOpacity(NotValidLvlColor);
	}
	else if (ItemData->Status == EAbilityStatus::Applied)
	{
		ApplyButton->SetIsActive(false);
		RequiredLvl->SetColorAndOpacity(ValidLvlColor);
	}
	else if (ItemData->Status == EAbilityStatus::Available)
	{
		ApplyButton->SetIsActive(true);
		RequiredLvl->SetColorAndOpacity(ValidLvlColor);
	}
}

void UAbilityMenu::OnClickApply()
{
	const auto PlayerState = GetPlayerContext().GetPlayerState<APHPlayerState>();
	if (PlayerState && SelectedAbility)
	{
		if (ETeamType::Hunter == SelectedAbility->Team)
		{
			ApplyAbility(&CurrentHunterAbility);
		}
		else if (ETeamType::Prop == SelectedAbility->Team)
		{
			ApplyAbility(&CurrentPropAbility);
		}

		PlayerState->SetSkillId(SelectedAbility->AbilityItem->GetPrimaryAssetId(), SelectedAbility->Team);

		UGameplayStatics::PlaySound2D(this, ApplySound);
	}
}

void UAbilityMenu::ApplyAbility(UAbilityItemData** CurrentAbility)
{
	if ((*CurrentAbility) != nullptr && EAbilityStatus::Applied == (*CurrentAbility)->Status)
	{
		(*CurrentAbility)->Status = EAbilityStatus::Available;
		(*CurrentAbility)->OnUpdateStateDelegate.ExecuteIfBound();

		UpdateDetails(*CurrentAbility);
	}

	if (SelectedAbility)
	{
		SelectedAbility->Status = EAbilityStatus::Applied;
		SelectedAbility->OnUpdateStateDelegate.ExecuteIfBound();

		UpdateDetails(SelectedAbility);

		*CurrentAbility = SelectedAbility;
	}
}

#undef LOCTEXT_NAMESPACE
