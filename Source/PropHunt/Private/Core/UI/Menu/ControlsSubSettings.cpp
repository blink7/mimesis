// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/ControlsSubSettings.h"

#include "Core/PHPersistentUser.h"
#include "Core/UI/Menu/SettingsItem.h"


void UControlsSubSettings::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (!InvertYAxis->OnValueChangedEvent.IsBound())
	{
		InvertYAxis->OnValueChangedEvent.AddUObject(this, &UControlsSubSettings::OnInvertYAxisChanged);
	}
	if (!MouseSensitivity->OnValueChangedEvent.IsBound())
	{
		MouseSensitivity->OnValueChangedEvent.AddUObject(this, &UControlsSubSettings::OnMouseSensitivityChanged);
	}
}

void UControlsSubSettings::NativeConstruct()
{
	Super::NativeConstruct();

	LoadOptions();
	UpdateOptions();
}

void UControlsSubSettings::LoadOptions()
{
	const auto PersistentUser = GetPersistentUser();
	if (PersistentUser)
	{
		bInvertYAxisOpt = PersistentUser->GetInvertedYAxis();
		SensitivityOpt = PersistentUser->GetAimSensitivity();
// 		GammaOpt = PersistentUser->GetGamma();
// 		bVibrationOpt = PersistentUser->GetVibration();
	}
}

void UControlsSubSettings::UpdateOptions()
{
	InvertYAxis->SetSelectedIndex(bInvertYAxisOpt);
	MouseSensitivity->SetCurrentValue(FMath::GetMappedRangeValueUnclamped(FVector2D(MinSensitivity, MaxSensitivity), FVector2D(0.f, 100.f), SensitivityOpt));
}

void UControlsSubSettings::ApplySettings()
{
	const auto PersistentUser = GetPersistentUser();
	if (PersistentUser)
	{
		PersistentUser->SetAimSensitivity(SensitivityOpt);
		PersistentUser->SetInvertedYAxis(bInvertYAxisOpt);
// 		PersistentUser->SetGamma(GammaOpt);
// 		PersistentUser->SetVibration(bVibrationOpt);

		PersistentUser->SaveIfDirty();
	}

	Super::ApplySettings();
}

void UControlsSubSettings::SetToDefaults()
{
	Super::SetToDefaults();

	const auto PersistentUser = GetPersistentUser();
	if (PersistentUser)
	{
		PersistentUser->SetToDefaults();

		LoadOptions();
		UpdateOptions();
	}
}

void UControlsSubSettings::RevertChanges()
{
	LoadOptions();
	UpdateOptions();

	Super::RevertChanges();
}

void UControlsSubSettings::OnInvertYAxisChanged()
{
	const auto PersistentUser = GetPersistentUser();
	if (PersistentUser)
	{
		bInvertYAxisOpt = InvertYAxis->GetValue<bool>();
		OnSettingChanged(InvertYAxis, PersistentUser->GetInvertedYAxis() != bInvertYAxisOpt);
	}
}

void UControlsSubSettings::OnMouseSensitivityChanged()
{
	const auto PersistentUser = GetPersistentUser();
	if (PersistentUser)
	{
		SensitivityOpt = FMath::GetMappedRangeValueUnclamped(FVector2D(0.f, 100.f), FVector2D(MinSensitivity, MaxSensitivity), MouseSensitivity->GetValue<float>());
		OnSettingChanged(MouseSensitivity, PersistentUser->GetAimSensitivity() != SensitivityOpt);
	}
}
