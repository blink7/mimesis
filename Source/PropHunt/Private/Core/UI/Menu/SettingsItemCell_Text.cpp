// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/SettingsItemCell_Text.h"

#include "Components/EditableTextBox.h"


void USettingsItemCell_Text::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	if (!ValueText->OnTextChanged.IsBound())
	{
		ValueText->OnTextChanged.AddDynamic(this, &USettingsItemCell_Text::OnTextChanged);
	}
	if (!ValueText->OnTextCommitted.IsBound())
	{
		ValueText->OnTextCommitted.AddDynamic(this, &USettingsItemCell_Text::OnTextCommitted);
	}
}

void USettingsItemCell_Text::Setup(const FText& Text, const FText& HintText, bool bIsPassword)
{
	ValueText->SetText(Text);
	ValueText->SetHintText(HintText);
	ValueText->SetIsPassword(bIsPassword);
}

FString USettingsItemCell_Text::GetStringValue()
{
	return ValueText->GetText().ToString().TrimStartAndEnd();
}

void USettingsItemCell_Text::OnTextChanged(const FText& Text)
{
	OnValueChangedEvent.Broadcast();
}

void USettingsItemCell_Text::OnTextCommitted(const FText& Text, ETextCommit::Type CommitMethod)
{
	if (CommitMethod == ETextCommit::OnEnter)
	{
		OnValueCommittedEvent.Broadcast();
	}
}
