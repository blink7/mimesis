// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/AudioSubSettings.h"

#include "Core/PHGameUserSettings.h"
#include "Core/UI/Menu/SettingsItem.h"


void UAudioSubSettings::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (!Master->OnValueChangedEvent.IsBound())
	{
		Master->OnValueChangedEvent.AddUObject(this, &UAudioSubSettings::OnMasterVolumeChanged);
	}
	if (!Ambient->OnValueChangedEvent.IsBound())
	{
		Ambient->OnValueChangedEvent.AddUObject(this, &UAudioSubSettings::OnAmbientVolumeChanged);
	}
	if (!Effects->OnValueChangedEvent.IsBound())
	{
		Effects->OnValueChangedEvent.AddUObject(this, &UAudioSubSettings::OnEffectsVolumeChanged);
	}
	if (!Music->OnValueChangedEvent.IsBound())
	{
		Music->OnValueChangedEvent.AddUObject(this, &UAudioSubSettings::OnMusicVolumeChanged);
	}
	if (!Menu->OnValueChangedEvent.IsBound())
	{
		Menu->OnValueChangedEvent.AddUObject(this, &UAudioSubSettings::OnMenuVolumeChanged);
	}
	if (!Voice->OnValueChangedEvent.IsBound())
	{
		Voice->OnValueChangedEvent.AddUObject(this, &UAudioSubSettings::OnVoiceVolumeChanged);
	}
}

void UAudioSubSettings::NativeConstruct()
{
	Super::NativeConstruct();

	LoadOptions();
	UpdateOptions();
}

void UAudioSubSettings::LoadOptions()
{
	if (UserSettings)
	{
		MasterSoundVolumeOpt = UserSettings->GetMasterSoundVolume();
		AmbientSoundVolumeOpt = UserSettings->GetAmbientSoundVolume();
		EffectsSoundVolumeOpt = UserSettings->GetEffectsSoundVolume();
		MusicSoundVolumeOpt = UserSettings->GetMusicSoundVolume();
		MenuSoundVolumeOpt = UserSettings->GetMenuSoundVolume();
		VoiceSoundVolumeOpt = UserSettings->GetVoiceSoundVolume();
	}
}

void UAudioSubSettings::UpdateOptions()
{
	Master->SetCurrentValue(MasterSoundVolumeOpt * 100.f);
	Ambient->SetCurrentValue(AmbientSoundVolumeOpt * 100.f);
	Effects->SetCurrentValue(EffectsSoundVolumeOpt * 100.f);
	Music->SetCurrentValue(MusicSoundVolumeOpt * 100.f);
	Menu->SetCurrentValue(MenuSoundVolumeOpt * 100.f);
	Voice->SetCurrentValue(VoiceSoundVolumeOpt * 100.f);
}

void UAudioSubSettings::ApplySettings()
{
	if (UserSettings)
	{
		UserSettings->SetMasterSoundVolume(MasterSoundVolumeOpt);
		UserSettings->SetAmbientSoundVolume(AmbientSoundVolumeOpt);
		UserSettings->SetEffectsSoundVolume(EffectsSoundVolumeOpt);
		UserSettings->SetMusicSoundVolume(MusicSoundVolumeOpt);
		UserSettings->SetMenuSoundVolume(MenuSoundVolumeOpt);
		UserSettings->SetVoiceSoundVolume(VoiceSoundVolumeOpt);

		UserSettings->ApplySettings(false);
	}

	Super::ApplySettings();
}

void UAudioSubSettings::SetToDefaults()
{
	Super::SetToDefaults();

	if (UserSettings)
	{
		UserSettings->SetAudioToDefaults();

		LoadOptions();
		UpdateOptions();
	}
}

void UAudioSubSettings::RevertChanges()
{
	LoadOptions();
	UpdateOptions();

	Super::RevertChanges();
}

void UAudioSubSettings::OnMasterVolumeChanged()
{
	if (UserSettings)
	{
		MasterSoundVolumeOpt = Master->GetValue<float>() / 100.f;
		OnSettingChanged(Master, UserSettings->GetMasterSoundVolume() != MasterSoundVolumeOpt);
	}
}

void UAudioSubSettings::OnAmbientVolumeChanged()
{
	if (UserSettings)
	{
		AmbientSoundVolumeOpt = Ambient->GetValue<float>() / 100.f;
		OnSettingChanged(Ambient, UserSettings->GetAmbientSoundVolume() != AmbientSoundVolumeOpt);
	}
}

void UAudioSubSettings::OnEffectsVolumeChanged()
{
	if (UserSettings)
	{
		EffectsSoundVolumeOpt = Effects->GetValue<float>() / 100.f;
		OnSettingChanged(Effects, UserSettings->GetEffectsSoundVolume() != EffectsSoundVolumeOpt);
	}
}

void UAudioSubSettings::OnMusicVolumeChanged()
{
	if (UserSettings)
	{
		MusicSoundVolumeOpt = Music->GetValue<float>() / 100.f;
		OnSettingChanged(Music, UserSettings->GetMusicSoundVolume() != MusicSoundVolumeOpt);
	}
}

void UAudioSubSettings::OnMenuVolumeChanged()
{
	if (UserSettings)
	{
		MenuSoundVolumeOpt = Menu->GetValue<float>() / 100.f;
		OnSettingChanged(Menu, UserSettings->GetMenuSoundVolume() != MenuSoundVolumeOpt);
	}
}

void UAudioSubSettings::OnVoiceVolumeChanged()
{
	if (UserSettings)
	{
		VoiceSoundVolumeOpt = Voice->GetValue<float>() / 100.f;
		OnSettingChanged(Voice, UserSettings->GetVoiceSoundVolume() != VoiceSoundVolumeOpt);
	}
}
