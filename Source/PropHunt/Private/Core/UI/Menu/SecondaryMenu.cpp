// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/SecondaryMenu.h"

#include "Core/PHTypes.h"
#include "Core/UI/Menu/SubMenu.h"
#include "Core/UI/Widgets/Dialog.h"

#include "Components/Border.h"
#include "Components/TextBlock.h"
#include "Components/WidgetSwitcher.h"


#define LOCTEXT_NAMESPACE "Menu"

void USecondaryMenu::OnSwitchMenu(uint8 InMenuState)
{
	if (MenuSwitcher && Path)
	{
		switch (InMenuState)
		{
		case EMenuType::CreateServer:
			Path->SetText(LOCTEXT("CreateServer", "CreateServer.gag"));
			MenuSwitcher->SetActiveWidgetIndex(1);
			break;
		case EMenuType::FindServer:
			Path->SetText(LOCTEXT("FindServer", "FindServer.gag"));
			MenuSwitcher->SetActiveWidgetIndex(2);
			break;
		case EMenuType::Profile:
			Path->SetText(LOCTEXT("Profile", "Profile.gag"));
			MenuSwitcher->SetActiveWidgetIndex(3);
			break;
		case EMenuType::Settings:
			Path->SetText(LOCTEXT("Settings", "Settings.gag"));
			MenuSwitcher->SetActiveWidgetIndex(4);
			break;
		}

		const auto CurrentMenu = Cast<USubMenu>(MenuSwitcher->GetActiveWidget());
		if (CurrentMenu)
		{
			CurrentMenu->SecondaryMenu = this;
			CurrentMenu->Setup();

			if (!CurrentMenu->OnShowDialogEvent.IsBoundToObject(this))
			{
				CurrentMenu->OnShowDialogEvent.AddUObject(this, &USecondaryMenu::ShowDialog);
			}

			//Check if we already have focused Dialog
			if (!DialogContainer->GetChildAt(0))
			{
				CurrentMenu->SetUserFocus(GetOwningPlayer());
			}
		}
	}
}

void USecondaryMenu::ReturnToMainMenu()
{
	Path->SetText(LOCTEXT("MainMenu", "MainMenu"));
	MenuSwitcher->SetActiveWidgetIndex(0);

	OnReturnToMainMenuEvent.Broadcast();
}

void USecondaryMenu::ShowDialog(UDialog* DialogToShow)
{
	DialogContainer->AddChild(DialogToShow);
}

void USecondaryMenu::ReturnFocusToSubMenu()
{
	MenuSwitcher->GetActiveWidget()->SetUserFocus(GetOwningPlayer());
}

void USecondaryMenu::OnPlayerLoaded()
{
	for (const auto Child : MenuSwitcher->GetAllChildren())
	{
		const auto SubMenu = Cast<USubMenu>(Child);
		if (SubMenu)
		{
			SubMenu->OnPlayerLoaded();
		}
	}
}

#undef LOCTEXT_NAMESPACE
