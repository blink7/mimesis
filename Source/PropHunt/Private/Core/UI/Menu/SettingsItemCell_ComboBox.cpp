// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/SettingsItemCell_ComboBox.h"

#include "Components/ComboBoxString.h"

void USettingsItemCell_ComboBox::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	if (!ComboBox->OnSelectionChanged.IsBound())
	{
		ComboBox->OnSelectionChanged.AddDynamic(this, &USettingsItemCell_ComboBox::OnSelectionChanged);
	}
}

int32 USettingsItemCell_ComboBox::GetIntegerValue()
{
	return ComboBox->GetSelectedIndex();
}

bool USettingsItemCell_ComboBox::GetBooleanValue()
{
	return ComboBox->GetSelectedIndex() > 0;
}

FString USettingsItemCell_ComboBox::GetStringValue()
{
	return ComboBox->GetSelectedOption();
}

void USettingsItemCell_ComboBox::Setup(const TArray<FString>& InDataSet, const FString& InCurrentValue)
{
	ComboBox->ClearOptions();

	for (const FString& Option : InDataSet)
	{
		ComboBox->AddOption(Option);
	}

	ComboBox->SetSelectedOption(InCurrentValue);
}

void USettingsItemCell_ComboBox::SetSelectedIndex(int32 Index)
{
	ComboBox->SetSelectedIndex(Index);
}

void USettingsItemCell_ComboBox::SetSelectedOption(const FString& Option)
{
	ComboBox->SetSelectedOption(Option);
}

void USettingsItemCell_ComboBox::AddOption(const FString& Option)
{
	ComboBox->AddOption(Option);
}

bool USettingsItemCell_ComboBox::RemoveOption(const FString& Option)
{
	return ComboBox->RemoveOption(Option);
}

void USettingsItemCell_ComboBox::ClearOptions()
{
	ComboBox->ClearOptions();
}

void USettingsItemCell_ComboBox::OnSelectionChanged(FString SelectedItem, ESelectInfo::Type SelectionType)
{
	if (ESelectInfo::OnKeyPress == SelectionType || ESelectInfo::OnMouseClick == SelectionType)
	{
		OnValueChangedEvent.Broadcast();
	}
}
