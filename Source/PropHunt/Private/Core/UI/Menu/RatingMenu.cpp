// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/RatingMenu.h"

#include "PropHunt/PropHunt.h"
#include "Core/PHGameInstance.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/Online/PHStatisticsHttpClient.h"

#include "Slate/SceneViewport.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/PanelWidget.h"
#include "Components/WidgetSwitcher.h"

#define LOCTEXT_NAMESPACE "Menu.Rating"

void URatingMenu::NativeConstruct()
{
	Super::NativeConstruct();

	if (!RefreshButton->OnClicked.IsBound())
	{
		RefreshButton->OnClicked.AddDynamic(this, &URatingMenu::RefreshRating);
	}

	if (!ReturnButton->OnClicked.IsBound())
	{
		ReturnButton->OnClicked.AddDynamic(this, &URatingMenu::ReturnToMainMenu);
	}

	if (!ShowMeButton->OnClicked.IsBound())
	{
		ShowMeButton->OnClicked.AddDynamic(this, &URatingMenu::OnShowMe);
	}
}

void URatingMenu::NativeOnFocusLost(const FFocusEvent& InFocusEvent)
{
	Super::NativeOnFocusLost(InFocusEvent);

	const auto World = GetWorld();
	if (World && World->GetGameViewport()->GetGameViewport()->HasFocus())
	{
		//Don't allow to lose focus when click outside the board
		SetUserFocus(GetOwningPlayer());
	}
}

FReply URatingMenu::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	FReply Result = FReply::Unhandled();
	const FKey Key = InKeyEvent.GetKey();

	if (Key == EKeys::Escape || Key == EKeys::Virtual_Back)
	{
		ReturnToMainMenu();
		Result = FReply::Handled();
	}

	return Result;
}

void URatingMenu::Setup()
{
	SetUserFocus(GetOwningPlayer());

	ShowMeButton->SetVisibility(ESlateVisibility::Hidden);
	ReturnButton->SetIsEnabled(true);
}

void URatingMenu::DisplayRating()
{
	LoadRating();

	Switcher->SetActiveWidgetIndex(1);
	ShowMeButton->SetVisibility(ESlateVisibility::Visible);
}

void URatingMenu::LoadRating()
{
	if (RatingLabels.Num() == 0) return;

	if (Ratings.Num() > 0) return;

	const auto GameInstance = GetGameInstance<UPHGameInstance>();
	const auto PlayerState = GetPlayerContext().GetPlayerState<APHPlayerState>();
	if (GameInstance && PlayerState)
	{
		if (PrevRating >= RatingLabels.Num())
		{
			PrevRating = 0;
		}

		FName RatingType;

		int32 Count = 0;
		for (const auto& RatingLabel : RatingLabels)
		{
			if (PrevRating == Count++)
			{
				RatingType = RatingLabel.Key;
				Title->SetText(RatingLabel.Value);

				++PrevRating;

				break;
			}
		}

		const TSharedPtr<const FUniqueNetId> UserId = PlayerState->GetUniqueId().GetUniqueNetId();
		if (UserId.IsValid())
		{
			auto GetTodayRatingCallback = [this](TArray<FResponse_TodayRatingItem> InTodayRating)
			{
				for (const FResponse_TodayRatingItem& Rating : InTodayRating)
				{
					FRatingItemData RatingData;

					if (Rating.playerName.Len() > MAX_PLAYER_NAME_LENGTH)
					{
						RatingData.PlayerName = FText::FromString(Rating.playerName.Left(MAX_PLAYER_NAME_LENGTH) + "...");
					}
					else
					{
						RatingData.PlayerName = FText::FromString(Rating.playerName);
					}

					uint32 Score = Rating.score;
					if (Score >= 1000000)
					{
						Score /= 1000000;
						RatingData.Score = FText::Format(LOCTEXT("M", "{0}M"), FText::AsNumber(Score));
					}
					else if (Score >= 1000)
					{
						Score /= 1000;
						RatingData.Score = FText::Format(LOCTEXT("K", "{0}K"), FText::AsNumber(Score));
					}
					else
					{
						RatingData.Score = FText::AsNumber(Score);
					}

					RatingData.bOwner = Rating.owner;
					Ratings.Add(RatingData);

					const auto RatingItemWidget = CreateWidget<URatingEntry>(this, RatingItemClass);
					if (RatingItemWidget)
					{
						RatingItemWidget->SetData(RatingData);
					}

					if (ItemList)
					{
						ItemList->AddChild(RatingItemWidget);
					}
				}
			};
			GameInstance->GetStatisticsHttpClient().GetTodayRating(*UserId, RatingType, GetTodayRatingCallback);
		}
	}
}

void URatingMenu::RefreshRating()
{
	Ratings.Empty();
	ItemList->ClearChildren();

	LoadRating();
}

void URatingMenu::ReturnToMainMenu()
{
	ReturnButton->SetIsEnabled(false);
	ShowMeButton->SetVisibility(ESlateVisibility::Visible);

	OnReturnToMainMenuEvent.Broadcast();
}

void URatingMenu::OnShowMe()
{
	OnShowMeEvent.Broadcast();
}

#undef LOCTEXT_NAMESPACE
