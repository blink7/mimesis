// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/SettingsItemCell_Slider.h"

#include "Core/UI/PHEditableTextBox.h"

#include "Components/Button.h"
#include "Components/Slider.h"
#include "Components/ProgressBar.h"


void USettingsItemCell_Slider::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	if (!ValueText->OnTextCommitted.IsBound())
	{
		ValueText->OnTextCommitted.AddDynamic(this, &USettingsItemCell_Slider::ValueTextCommitted);
	}
	if (!DecreaseButton->OnClicked.IsBound())
	{
		DecreaseButton->OnClicked.AddDynamic(this, &USettingsItemCell_Slider::Decrease);
	}
	if (!IncreaseButton->OnClicked.IsBound())
	{
		IncreaseButton->OnClicked.AddDynamic(this, &USettingsItemCell_Slider::Increase);
	}
	if (!SliderValue->OnValueChanged.IsBound())
	{
		SliderValue->OnValueChanged.AddDynamic(this, &USettingsItemCell_Slider::OnSliderChanged);
	}
	if (!SliderValue->OnMouseCaptureEnd.IsBound())
	{
		SliderValue->OnMouseCaptureEnd.AddDynamic(this, &USettingsItemCell_Slider::OnSliderCaptureEnd);
	}
	if (!SliderValue->OnControllerCaptureEnd.IsBound())
	{
		SliderValue->OnControllerCaptureEnd.AddDynamic(this, &USettingsItemCell_Slider::OnSliderCaptureEnd);
	}

	SetCurrentValue(CurrentValue);
}

void USettingsItemCell_Slider::ValueTextCommitted(const FText& Text, ETextCommit::Type CommitMethod)
{
	const float NewValue = FCString::Atof(*Text.ToString());

	SetCurrentValue(NewValue);

	OnValueChangedEvent.Broadcast();

	if (CommitMethod == ETextCommit::OnEnter)
	{
		OnValueCommittedEvent.Broadcast();
	}
}

void USettingsItemCell_Slider::Decrease()
{
	CurrentValue -= Division;
	if (CurrentValue < ValueText->MinValue)
	{
		CurrentValue = ValueText->MinValue;
	}

	UpdateView();

	CheckForMinValue();

	OnValueChangedEvent.Broadcast();
}

void USettingsItemCell_Slider::Increase()
{
	CurrentValue += Division;
	if (CurrentValue > ValueText->MaxValue)
	{
		CurrentValue = ValueText->MaxValue;
	}

	UpdateView();

	CheckForMaxValue();

	OnValueChangedEvent.Broadcast();
}

void USettingsItemCell_Slider::OnSliderChanged(float Value)
{
	SetCurrentValue(Value);

	OnValueChangedEvent.Broadcast();
}

void USettingsItemCell_Slider::OnSliderCaptureEnd()
{
	OnValueCommittedEvent.Broadcast();
}

void USettingsItemCell_Slider::CheckForMinValue()
{

	if (FMath::IsNearlyEqual(CurrentValue, ValueText->MinValue, 1.f) && DecreaseButton && DecreaseButton->GetIsEnabled())
	{
		DecreaseButton->SetIsEnabled(false);
	}

	if (CurrentValue < ValueText->MaxValue && IncreaseButton && !IncreaseButton->GetIsEnabled())
	{
		IncreaseButton->SetIsEnabled(true);
	}
}

void USettingsItemCell_Slider::CheckForMaxValue()
{
	if (FMath::IsNearlyEqual(CurrentValue, ValueText->MaxValue, 1.f) && IncreaseButton && IncreaseButton->GetIsEnabled())
	{
		IncreaseButton->SetIsEnabled(false);
	}

	if (CurrentValue > ValueText->MinValue && DecreaseButton && !DecreaseButton->GetIsEnabled())
	{
		DecreaseButton->SetIsEnabled(true);
	}
}

void USettingsItemCell_Slider::UpdateView()
{
	ValueText->SetValue(CurrentValue);
	SliderValue->SetValue(CurrentValue);
	ProgressValue->SetPercent(FMath::GetMappedRangeValueUnclamped({ MinValue, MaxValue }, { 0.f, 1.f }, CurrentValue));
}

void USettingsItemCell_Slider::SetCurrentValue(float InValue)
{
	CurrentValue = InValue;

	UpdateView();

	CheckForMinValue();
	CheckForMaxValue();
}

void USettingsItemCell_Slider::SetRange(float InMinValue, float InMaxValue)
{
	MinValue = InMinValue;
	MaxValue = InMaxValue;

	ValueText->MinValue = InMinValue;
	ValueText->MaxValue = InMaxValue;

	SliderValue->SetMinValue(InMinValue);
	SliderValue->SetMaxValue(InMaxValue);
}

void USettingsItemCell_Slider::Setup(float InValue, const FVector2D& InRange)
{
	SetRange(InRange.X, InRange.Y);
	SetCurrentValue(InValue);
}
