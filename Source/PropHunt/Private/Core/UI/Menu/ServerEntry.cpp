// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/ServerEntry.h"

#include "Components/TextBlock.h"


void UServerEntry::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	const auto Item = Cast<UServerItemData>(ListItemObject);
	if (Item)
	{
		ServerName->SetText(FText::FromString(Item->ServerName));
		Map->SetText(Item->MapLabel);
		Players->SetText(FText::FromString(FString::Printf(TEXT("%s/%s"), *Item->CurrentPlayers, *Item->MaxPlayers)));
		Ping->SetText(FText::FromString(Item->Ping));

		if (!Item->OnItemHoverEvent.IsBoundToObject(this))
		{
			Item->OnItemHoverEvent.BindUObject(this, &UServerEntry::OnChangeHover);
		}
	}
}

void UServerEntry::NativeOnItemSelectionChanged(bool bIsSelected)
{
	OnChangeSelection(bIsSelected);
}
