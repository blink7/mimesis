// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/GraphicsSubSettings.h"

#include "Core/PHPersistentUser.h"
#include "Core/PHGameUserSettings.h"
#include "Core/UI/Menu/SettingsItem.h"

#include "Framework/Application/SlateApplication.h"


static const int32 CUSTOM_GRAPHICS_QUALITY = 4;

#define LOCTEXT_NAMESPACE "Menu.Settings"

void UGraphicsSubSettings::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (!WindowMode->OnValueChangedEvent.IsBound())
	{
		WindowMode->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnWindowModeChanged);
	}
	if (!Resolution->OnValueChangedEvent.IsBound())
	{
		Resolution->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnResolutionChanged);
	}
	if (!FrameRateLimit->OnValueChangedEvent.IsBound())
	{
		FrameRateLimit->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnFrameRateLimitChanged);
	}
	if (!VerticalSync->OnValueChangedEvent.IsBound())
	{
		VerticalSync->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnVerticalSyncChanged);
	}
	if (!Gamma->OnValueChangedEvent.IsBound())
	{
		Gamma->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnGammaChanged);
	}
	if (!FirstPersonFOV->OnValueChangedEvent.IsBound())
	{
		FirstPersonFOV->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnFirstPersonFOVChanged);
	}
	if (!ThirdPersonFOV->OnValueChangedEvent.IsBound())
	{
		ThirdPersonFOV->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnThirdPersonFOVChanged);
	}
	if (!GraphicsQuality->OnValueChangedEvent.IsBound())
	{
		GraphicsQuality->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnGraphicsQualityChanged);
	}
	if (!ResolutionScale->OnValueChangedEvent.IsBound())
	{
		ResolutionScale->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnResolutionScaleChanged);
	}
	if (!ViewDistance->OnValueChangedEvent.IsBound())
	{
		ViewDistance->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnViewDistanceChanged);
	}
	if (!AntiAliasing->OnValueChangedEvent.IsBound())
	{
		AntiAliasing->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnAntiAliasingChanged);
	}
	if (!PostProcessing->OnValueChangedEvent.IsBound())
	{
		PostProcessing->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnPostProcessingChanged);
	}
	if (!Shadows->OnValueChangedEvent.IsBound())
	{
		Shadows->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnShadowsChanged);
	}
	if (!Textures->OnValueChangedEvent.IsBound())
	{
		Textures->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnTexturesChanged);
	}
	if (!Effects->OnValueChangedEvent.IsBound())
	{
		Effects->OnValueChangedEvent.AddUObject(this, &UGraphicsSubSettings::OnEffectsChanged);
	}
}

void UGraphicsSubSettings::NativeConstruct()
{
	Super::NativeConstruct();

	LoadOptions();
	UpdateOptions();

	LoadQualityOptions();
	UpdateQualityOptions();
}

void UGraphicsSubSettings::LoadOptions()
{
	if (UserSettings)
	{
		WindowModeOpt = UserSettings->GetFullscreenMode();
		ResolutionOpt = UserSettings->GetScreenResolution();
		FrameRateLimitOpt = UserSettings->GetFrameRateLimit();
		bUseVSyncOpt = UserSettings->IsVSyncEnabled();
		GammaOpt = UserSettings->GetGamma();
		FirstPersonFOVOpt = UserSettings->GetFirstPersonFOV();
		ThirdPersonFOVOpt = UserSettings->GetThirdPersonFOV();
	}
}

void UGraphicsSubSettings::UpdateOptions()
{
	//Window Mode
	WindowMode->SetSelectedIndex(WindowModeOpt);

	//Resolution
	FDisplayMetrics DisplayMetrics;
	FSlateApplication::Get().GetInitialDisplayMetrics(DisplayMetrics);

	//Always make sure that the native resolution is available
	Resolution->AddItem(FString::Printf(TEXT("%dx%d"), DisplayMetrics.PrimaryDisplayWidth, DisplayMetrics.PrimaryDisplayHeight));

	// In case a non-standard resolution is set
	Resolution->AddItem(FString::Printf(TEXT("%dx%d"), ResolutionOpt.X, ResolutionOpt.Y));

	Resolution->SetCurrentValue(FText::FromString(FString::Printf(TEXT("%dx%d"), ResolutionOpt.X, ResolutionOpt.Y)));

	//Frame Rate Limit
	if (FrameRateLimitOpt < 666.f)
	{
		FrameRateLimit->SetCurrentValue(FText::AsNumber(FrameRateLimitOpt));
	}
	else
	{
		FrameRateLimit->SetSelectedIndex(5);  //unlimited
	}

	//VSync
	VerticalSync->SetSelectedIndex(bUseVSyncOpt);

	Gamma->SetCurrentValue(FMath::GetMappedRangeValueUnclamped(FVector2D(MinGamma, MaxGamma), FVector2D(0.f, 100.f), GammaOpt));

	FirstPersonFOV->SetCurrentValue(FirstPersonFOVOpt);
	ThirdPersonFOV->SetCurrentValue(ThirdPersonFOVOpt);
}

void UGraphicsSubSettings::LoadQualityOptions()
{
	if (UserSettings)
	{
		GraphicsQualityOpt = UserSettings->GetOverallScalabilityLevel();

		float CurrentScaleNormalized, CurrentScaleValue, MinScaleValue, MaxScaleValue;
		UserSettings->GetResolutionScaleInformationEx(CurrentScaleNormalized, CurrentScaleValue, MinScaleValue, MaxScaleValue);
		ResolutionScaleOpt = CurrentScaleValue;

		ViewDistanceQualityOpt = UserSettings->GetViewDistanceQuality();
		AntiAliasingQualityOpt = UserSettings->GetAntiAliasingQuality();
		PostProcessingQualityOpt = UserSettings->GetPostProcessingQuality();
		ShadowQualityOpt = UserSettings->GetShadowQuality();
		TextureQualityOpt = UserSettings->GetTextureQuality();
		EffectsQualityOpt = UserSettings->GetVisualEffectQuality();
	}
}

void UGraphicsSubSettings::UpdateQualityOptions()
{
	UpdateGraphicsQualityOption();
	ResolutionScale->SetCurrentValue(ResolutionScaleOpt);
	ViewDistance->SetSelectedIndex(ViewDistanceQualityOpt);
	AntiAliasing->SetSelectedIndex(AntiAliasingQualityOpt);
	PostProcessing->SetSelectedIndex(PostProcessingQualityOpt);
	Shadows->SetSelectedIndex(ShadowQualityOpt);
	Textures->SetSelectedIndex(TextureQualityOpt);
	Effects->SetSelectedIndex(EffectsQualityOpt);
}

void UGraphicsSubSettings::ApplySettings()
{
	if (UserSettings)
	{
		UserSettings->SetFullscreenMode(EWindowMode::ConvertIntToWindowMode(WindowModeOpt));
		UserSettings->SetScreenResolution(ResolutionOpt);
		UserSettings->SetFrameRateLimit(FrameRateLimitOpt);
		UserSettings->SetVSyncEnabled(bUseVSyncOpt);
		UserSettings->SetGamma(GammaOpt);
		UserSettings->SetFirstPersonFOV(FirstPersonFOVOpt);
		UserSettings->SetThirdPersonFOV(ThirdPersonFOVOpt);
		if (GraphicsQualityOpt >= 0)
		{
			UserSettings->SetOverallScalabilityLevel(GraphicsQualityOpt);

			float CurrentScaleNormalized, CurrentScaleValue, MinScaleValue, MaxScaleValue;
			UserSettings->GetResolutionScaleInformationEx(CurrentScaleNormalized, CurrentScaleValue, MinScaleValue, MaxScaleValue);
			ResolutionScaleOpt = CurrentScaleValue;
			ResolutionScale->SetCurrentValue(ResolutionScaleOpt);
		}
		else
		{
			UserSettings->SetResolutionScaleValueEx(ResolutionScaleOpt);
			UserSettings->SetViewDistanceQuality(ViewDistanceQualityOpt);
			UserSettings->SetAntiAliasingQuality(AntiAliasingQualityOpt);
			UserSettings->SetPostProcessingQuality(PostProcessingQualityOpt);
			UserSettings->SetShadowQuality(ShadowQualityOpt);
			UserSettings->SetTextureQuality(TextureQualityOpt);
			UserSettings->SetVisualEffectQuality(EffectsQualityOpt);
		}

		UserSettings->ApplySettings(false);
	}

	Super::ApplySettings();
}

void UGraphicsSubSettings::SetToDefaults()
{
	Super::SetToDefaults();

	if (UserSettings)
	{
		UserSettings->SetGraphicsToDefaults();

		LoadOptions();
		UpdateOptions();

		LoadQualityOptions();
		UpdateQualityOptions();
	}
}

void UGraphicsSubSettings::RevertChanges()
{
	LoadOptions();
	UpdateOptions();

	LoadQualityOptions();
	UpdateQualityOptions();

	Super::RevertChanges();
}

void UGraphicsSubSettings::AutoDetect()
{
	if (UserSettings)
	{
		UserSettings->RunHardwareBenchmark();
		UserSettings->ApplyHardwareBenchmarkResults();

		LoadQualityOptions();
		UpdateQualityOptions();
	}
}

void UGraphicsSubSettings::UpdateGraphicsQualityOption()
{
	GraphicsQuality->SetSelectedIndex(GraphicsQualityOpt >= 0 ? GraphicsQualityOpt : CUSTOM_GRAPHICS_QUALITY);
}

void UGraphicsSubSettings::SaveGraphicsQualityOption()
{
	const int32 NewGraphicsQuality = GraphicsQuality->GetValue<int32>();
	GraphicsQualityOpt = NewGraphicsQuality != CUSTOM_GRAPHICS_QUALITY ? NewGraphicsQuality : -1;
}

void UGraphicsSubSettings::CheckGraphicsQuality()
{
	const int32 Target = ViewDistanceQualityOpt;
	if ((Target == AntiAliasingQualityOpt) && (Target == PostProcessingQualityOpt) && (Target == ShadowQualityOpt) && (Target == TextureQualityOpt) && (Target == EffectsQualityOpt))
	{
		GraphicsQualityOpt = Target;
	}
	else
	{
		GraphicsQualityOpt = -1;
	}

	UpdateGraphicsQualityOption();
	OnSettingChanged(GraphicsQuality, UserSettings->GetOverallScalabilityLevel() != GraphicsQualityOpt);
}

void UGraphicsSubSettings::OnWindowModeChanged()
{
	if (UserSettings)
	{
		WindowModeOpt = WindowMode->GetValue<int32>();
		OnSettingChanged(WindowMode, UserSettings->GetFullscreenMode() != WindowModeOpt);
	}
}

void UGraphicsSubSettings::OnResolutionChanged()
{
	if (UserSettings)
	{
		TArray<FString> NewResolution;
		Resolution->GetValue<FString>().ParseIntoArray(NewResolution, TEXT("x"));
		ResolutionOpt = FIntPoint(FCString::Atoi(*NewResolution[0]), FCString::Atoi(*NewResolution[1]));

		OnSettingChanged(Resolution, UserSettings->GetScreenResolution() != ResolutionOpt);
	}
}

void UGraphicsSubSettings::OnFrameRateLimitChanged()
{
	if (UserSettings)
	{
		if (FrameRateLimit->GetValue<int32>() == 5) //unlimited
		{
			FrameRateLimitOpt = 666.f;
		}
		else
		{
			FrameRateLimitOpt = FCString::Atof(*FrameRateLimit->GetValue<FString>());
		}

		OnSettingChanged(FrameRateLimit, UserSettings->GetFrameRateLimit() != FrameRateLimitOpt);
	}
}

void UGraphicsSubSettings::OnVerticalSyncChanged()
{
	if (UserSettings)
	{
		bUseVSyncOpt = VerticalSync->GetValue<bool>();
		OnSettingChanged(VerticalSync, UserSettings->IsVSyncEnabled() != bUseVSyncOpt);
	}
}

void UGraphicsSubSettings::OnGammaChanged()
{
	if (UserSettings)
	{
		GammaOpt = FMath::GetMappedRangeValueUnclamped(FVector2D(0.f, 100.f), FVector2D(MinGamma, MaxGamma), Gamma->GetValue<float>());
		OnSettingChanged(Gamma, UserSettings->GetGamma() != GammaOpt);
	}
}

void UGraphicsSubSettings::OnFirstPersonFOVChanged()
{
	if (UserSettings)
	{
		FirstPersonFOVOpt = FirstPersonFOV->GetValue<float>();
		OnSettingChanged(FirstPersonFOV, UserSettings->GetFirstPersonFOV() != FirstPersonFOVOpt);
	}
}

void UGraphicsSubSettings::OnThirdPersonFOVChanged()
{
	if (UserSettings)
	{
		ThirdPersonFOVOpt = ThirdPersonFOV->GetValue<float>();
		OnSettingChanged(ThirdPersonFOV, UserSettings->GetFirstPersonFOV() != ThirdPersonFOVOpt);
	}
}

void UGraphicsSubSettings::OnGraphicsQualityChanged()
{
	if (UserSettings)
	{
		SaveGraphicsQualityOption();

		if (GraphicsQualityOpt >= 0)
		{
			ViewDistanceQualityOpt = GraphicsQualityOpt;
			AntiAliasingQualityOpt = GraphicsQualityOpt;
			PostProcessingQualityOpt = GraphicsQualityOpt;
			ShadowQualityOpt = GraphicsQualityOpt;
			TextureQualityOpt = GraphicsQualityOpt;
			EffectsQualityOpt = GraphicsQualityOpt;

			UpdateQualityOptions();

			OnSettingChanged(GraphicsQuality, UserSettings->GetOverallScalabilityLevel() != GraphicsQualityOpt);

			OnSettingChanged(ViewDistance, UserSettings->GetViewDistanceQuality() != ViewDistanceQualityOpt);
			OnSettingChanged(AntiAliasing, UserSettings->GetAntiAliasingQuality() != AntiAliasingQualityOpt);
			OnSettingChanged(PostProcessing, UserSettings->GetAntiAliasingQuality() != PostProcessingQualityOpt);
			OnSettingChanged(Shadows, UserSettings->GetShadowQuality() != ShadowQualityOpt);
			OnSettingChanged(Textures, UserSettings->GetTextureQuality() != TextureQualityOpt);
			OnSettingChanged(Effects, UserSettings->GetVisualEffectQuality() != EffectsQualityOpt);
		}
	}
}

void UGraphicsSubSettings::OnResolutionScaleChanged()
{
	if (UserSettings)
	{
		ResolutionScaleOpt = ResolutionScale->GetValue<float>();
		OnSettingChanged(ResolutionScale, UserSettings->GetResolutionScaleNormalized() != ResolutionScaleOpt);

		GraphicsQualityOpt = -1;
		UpdateGraphicsQualityOption();
		OnSettingChanged(GraphicsQuality, UserSettings->GetOverallScalabilityLevel() != GraphicsQualityOpt);
	}
}

void UGraphicsSubSettings::OnViewDistanceChanged()
{
	if (UserSettings)
	{
		ViewDistanceQualityOpt = ViewDistance->GetValue<int32>();
		OnSettingChanged(ViewDistance, UserSettings->GetViewDistanceQuality() != ViewDistanceQualityOpt);

		CheckGraphicsQuality();
	}
}

void UGraphicsSubSettings::OnAntiAliasingChanged()
{
	if (UserSettings)
	{
		AntiAliasingQualityOpt = AntiAliasing->GetValue<int32>();
		OnSettingChanged(AntiAliasing, UserSettings->GetAntiAliasingQuality() != AntiAliasingQualityOpt);

		CheckGraphicsQuality();
	}
}

void UGraphicsSubSettings::OnPostProcessingChanged()
{
	if (UserSettings)
	{
		PostProcessingQualityOpt = PostProcessing->GetValue<int32>();
		OnSettingChanged(PostProcessing, UserSettings->GetPostProcessingQuality() != PostProcessingQualityOpt);

		CheckGraphicsQuality();
	}
}

void UGraphicsSubSettings::OnShadowsChanged()
{
	if (UserSettings)
	{
		ShadowQualityOpt = Shadows->GetValue<int32>();
		OnSettingChanged(Shadows, UserSettings->GetShadowQuality() != ShadowQualityOpt);

		CheckGraphicsQuality();
	}
}

void UGraphicsSubSettings::OnTexturesChanged()
{
	if (UserSettings)
	{
		TextureQualityOpt = Textures->GetValue<int32>();
		OnSettingChanged(Textures, UserSettings->GetTextureQuality() != TextureQualityOpt);

		CheckGraphicsQuality();
	}
}

void UGraphicsSubSettings::OnEffectsChanged()
{
	if (UserSettings)
	{
		EffectsQualityOpt = Effects->GetValue<int32>();
		OnSettingChanged(Effects, UserSettings->GetVisualEffectQuality() != EffectsQualityOpt);

		CheckGraphicsQuality();
	}
}

#undef LOCTEXT_NAMESPACE
