// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/MatchResult.h"

#include "Core/PHTypes.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/Widgets/MapVoteCell.h"
#include "Core/UI/Menu/MatchResultCell.h"

#include "Components/TextBlock.h"
#include "Components/ProgressBar.h"
#include "Components/PanelWidget.h"
#include "Components/WidgetSwitcher.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Core/PHUtilities.h"
#include "UObject/ConstructorHelpers.h"

UMatchResult::UMatchResult(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer), 
	NextMapVote(-1)
{
	bShowMouseCursor = true;
	bRespondUIOnly = true;

	static ConstructorHelpers::FClassFinder<UUserWidget> MapVoteCellBPClass(TEXT("/Game/PropHunt/UI/Widgets/WBP_MapVoteCell"));
	MapVoteCellClass = MapVoteCellBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> MatchResultCellBPClass(TEXT("/Game/PropHunt/UI/Menu/WBP_MatchResultCell"));
	MatchResultCellClass = MatchResultCellBPClass.Class;
}

void UMatchResult::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	UpdateMapVotes();

	if (bRequiresLevelUpUpdate)
	{
		UpdateLevelUp(InDeltaTime);
	}
}

void UMatchResult::NativeConstruct()
{
	UpdateBestTeam();

	FillRewards();

	FillVoteList();

	ShowLevelUp();

	const auto GameState = GetWorld()->GetGameState<APHGameState>();
	if (GameState)
	{
		FTimerHandle TimerHandle_Unused;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Unused, this, &UMatchResult::RunLevelUpUpdate, static_cast<float>(GameState->GetRemainingTime()) / 2.f);
	}

	Super::NativeConstruct();
}

void UMatchResult::UpdateTimer(int32 Time)
{
	if (Time < 0)
	{
		return;
	}

	RemainingTimeText->SetText(FText::FromString(PropHunt::Utilities::TimeToString(Time)));
	RemainingTimeBar->SetPercent(RemainingTimeBar->Percent - 1.f / Time);
}

void UMatchResult::BindMatchTimerChanges()
{
	if (const auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		UpdateTimer(GameState->GetRemainingTime());
		GameState->OnRemainingTimeChanged.AddDynamic(this, &UMatchResult::UpdateTimer);
	}
	else
	{
		// Keep retrying until Game State is ready
		FTimerHandle TimerHandle_Unused;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Unused, this, &UMatchResult::BindMatchTimerChanges, 0.2f, false);
	}
}

void UMatchResult::UpdateBestTeam()
{
	const auto GameState = GetWorld()->GetGameState<APHGameState>();
	if (GameState)
	{
		const auto& TeamWins = GameState->GetTeamWins();

		const uint8 HuntersScore = GameState->GetTeamScore(0);
		const uint8 PropsScore = GameState->GetTeamScore(1);

		if (TeamWins[0] > TeamWins[1])
		{
			SetBestTeam(0, HuntersScore, TeamWins[0]);
		}
		else if (TeamWins[0] < TeamWins[1])
		{
			SetBestTeam(1, PropsScore, TeamWins[1]);
		}
		else if (HuntersScore > PropsScore)
		{
			SetBestTeam(0, HuntersScore, TeamWins[0]);
		}
		else
		{
			SetBestTeam(1, PropsScore, TeamWins[1]);
		}
	}
}

void UMatchResult::SetBestTeam(int32 TeamNumber, uint8 Score, uint8 Wins)
{
	BestTeam->SetText(TeamList::Teams[TeamNumber].ToUpper());
	BestTeamWins->SetText(FText::AsNumber(Wins));
	BestTeamScore->SetText(FText::AsNumber(Score));
}

void UMatchResult::FillRewards()
{
	const auto GameState = GetWorld()->GetGameState<APHGameState>();
	if (GameState)
	{
		TMap<APHPlayerState*, int32> HuntersRang;
		TMap<APHPlayerState*, int32> PropsRang;
		TMap<APHPlayerState*, int32> TauntsRang;
		TMap<APHPlayerState*, int32> TransformsRang;
		TMap<APHPlayerState*, int32> SurvivalRang;

		for (const auto PlayerState : GameState->PlayerArray)
		{
			const auto MyPlayerState = Cast<APHPlayerState>(PlayerState);
			HuntersRang.Add(MyPlayerState, MyPlayerState->GetHunterScore());
			PropsRang.Add(MyPlayerState, MyPlayerState->GetPropScore());
			TauntsRang.Add(MyPlayerState, MyPlayerState->GetTaunts());
			TransformsRang.Add(MyPlayerState, MyPlayerState->GetTransforms());
			SurvivalRang.Add(MyPlayerState, MyPlayerState->GetSurvivalTime());
		}

		HuntersRang.ValueSort(TGreater<int32>());
		PropsRang.ValueSort(TGreater<int32>());
		TauntsRang.ValueSort(TGreater<int32>());
		TransformsRang.ValueSort(TGreater<int32>());
		SurvivalRang.ValueSort(TGreater<int32>());

		const auto BestHunter = HuntersRang.CreateConstIterator();
		CreateRewardCard(BestHunter.Key(), BestHunter.Value(), ERewardType::BestHunter);

		const auto BestProp = PropsRang.CreateConstIterator();
		CreateRewardCard(BestProp.Key(), BestProp.Value(), ERewardType::BestProp);

		const auto MostTaunts = TauntsRang.CreateConstIterator();
		CreateRewardCard(MostTaunts.Key(), MostTaunts.Value(), ERewardType::MostTaunts);

		const auto MostTransforms = TransformsRang.CreateConstIterator();
		CreateRewardCard(MostTransforms.Key(), MostTransforms.Value(), ERewardType::MostTransforms);

		const auto MostInconspicuous = SurvivalRang.CreateConstIterator();
		CreateRewardCard(MostInconspicuous.Key(), MostInconspicuous.Value(), ERewardType::MostInconspicuous);
	}
}

void UMatchResult::CreateRewardCard(class APHPlayerState* PlayerState, int32 Score, ERewardType RewardType)
{
	if (Score > 0)
	{
		const auto RewardCard = CreateWidget<UMatchResultCell>(this, MatchResultCellClass);
		if (RewardCard)
		{
			FPlayerInfo PlayerInfo 
			{ 
				RewardType, 
				PlayerState->GetUniqueId().GetUniqueNetId()->ToString(), 
				PlayerState->GetShortPlayerName(), 
				Score 
			};

			RewardCard->SetUp(PlayerInfo);
			RewardList->AddChild(RewardCard);
		}
	}
}

void UMatchResult::FillVoteList()
{
	for (int32 Index = 0; Index < MapList::Names.Num(); Index++)
	{
		const auto MapCell = CreateWidget<UMapVoteCell>(this, MapVoteCellClass);
		if (MapCell)
		{
			MapCell->SetMapName(MapList::DisplayedNames[Index].ToUpper());
			MapCell->OnMapSelected().AddUObject(this, &UMatchResult::OnNextMapSelected, Index);
			NextMapVoteList->AddChild(MapCell);
		}
	}
}

void UMatchResult::UpdateMapVotes()
{
	const auto GameState = GetWorld()->GetGameState<APHGameState>();
	if (GameState)
	{
		bool bRequiresWidgetUpdate = false;

		const int32 NumMaps = MapList::Names.Num();
		if (MapVotes.Num() < NumMaps)
		{
			MapVotes.AddZeroed(NumMaps);
		}

		for (int32 MapIndex = 0; MapIndex < NumMaps; MapIndex++)
		{
			const uint8 VotesForMap = GameState->GetVotesForMap(MapIndex);

			if (VotesForMap != MapVotes[MapIndex])
			{
				bRequiresWidgetUpdate = true;
				MapVotes[MapIndex] = VotesForMap;
			}
		}

		if (bRequiresWidgetUpdate)
		{
			for (int32 Index = 0; Index < MapList::Names.Num(); ++Index)
			{
				const auto MapCell = Cast<UMapVoteCell>(NextMapVoteList->GetChildAt(Index));
				if (MapCell)
				{
					MapCell->SetVoteCount(MapVotes[Index]);
					MapCell->SetActive(Index != NextMapVote);
				}
			}
		}
	}
}

void UMatchResult::ShowLevelUp()
{
	const auto PlayerController = GetOwningPlayer<APHPlayerController>();
	if (PlayerController)
	{
		const auto PlayerState = PlayerController->GetPlayerState<APHPlayerState>();
		if (PlayerState)
		{
			Level = PlayerState->GetPlayerLevel();
			CurrentLevel->SetText(FText::AsNumber(Level));
			NextLevel->SetText(FText::AsNumber(Level + 1));

			CurrentXP = PlayerState->GetCurrentXP();
			NeededXP = PlayerState->GetNeededXP();
			ProgressXP->SetPercent(static_cast<float>(CurrentXP) / NeededXP);

			PlayerState->AddXP();
		}
	}
}

void UMatchResult::UpdateLevelUp(float InDeltaTime)
{
	const auto PlayerController = GetOwningPlayer<APHPlayerController>();
	if (PlayerController)
	{
		const auto PlayerState = PlayerController->GetPlayerState<APHPlayerState>();
		if (PlayerState)
		{
			float Progress = 0.f;

			if (Level == PlayerState->GetPlayerLevel())
			{
				Progress = FMath::FInterpConstantTo(
					ProgressXP->Percent, 
					static_cast<float>(PlayerState->GetCurrentXP()) / PlayerState->GetNeededXP(), 
					InDeltaTime, 
					0.1f);

				ProgressXP->SetPercent(Progress);
			}
			else if (ProgressXP->Percent < 1.f)
			{
				Progress = FMath::FInterpConstantTo(ProgressXP->Percent, 1.f, InDeltaTime, 0.1f);
				ProgressXP->SetPercent(Progress);
			}
			else
			{
				Level = PlayerState->GetPlayerLevel();
				CurrentLevel->SetText(FText::AsNumber(Level));
				NextLevel->SetText(FText::AsNumber(Level + 1));

				ProgressXP->SetPercent(0.f);
			}
		}
	}
}

void UMatchResult::RunLevelUpUpdate()
{
	CenterWidgetSwitcher->SetActiveWidgetIndex(1);

	bRequiresLevelUpUpdate = true;
}

void UMatchResult::OnNextMapSelected(int32 MapIndex)
{
	NextMapVote = MapIndex;

	const auto PlayerController = GetOwningPlayer<APHPlayerController>();
	if (PlayerController)
	{
		PlayerController->ChooseNextMap(MapIndex);
	}
}
