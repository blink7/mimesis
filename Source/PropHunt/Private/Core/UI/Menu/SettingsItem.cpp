// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/SettingsItem.h"

#include "Core/UI/Menu/SettingsItemCell_Text.h"
#include "Core/UI/Menu/SettingsItemCell_Slider.h"
#include "Core/UI/Menu/SettingsItemCell_ComboBox.h"

#include "Components/Border.h"

void USettingsItem::NativePreConstruct()
{
	Super::NativePreConstruct();

	CurrentValueString = CurrentValue.ToString();

	if (Container)
	{
		if (WidgetCell)
		{
			Container->RemoveChild(WidgetCell);
		}

		switch (SettingsItemType)
		{
			case ESettingsItemType::ComboBox:
			{
				if (const auto ComboBoxCell = CreateWidget<USettingsItemCell_ComboBox>(this, ComboBoxClass))
				{
					ComboBoxCell->Setup(Items, CurrentValueString);
					Container->AddChild(ComboBoxCell);
					WidgetCell = ComboBoxCell;
				}
				break;
			}
			case ESettingsItemType::Text:
			{
				if (const auto TextCell = CreateWidget<USettingsItemCell_Text>(this, TextClass))
				{
					TextCell->Setup(CurrentValue, HintText, bIsPassword);
					Container->AddChild(TextCell);
					WidgetCell = TextCell;
				}
				break;
			}
			case ESettingsItemType::Slider:
			{
				if (const auto SliderCell = CreateWidget<USettingsItemCell_Slider>(this, SliderClass))
				{
					if (CurrentValueString.IsNumeric())
					{
						const float Value = FMath::Clamp(FCString::Atof(*CurrentValueString), MinValue, MaxValue);
						SliderCell->Setup(Value, { MinValue, MaxValue });
					}
					Container->AddChild(SliderCell);
					WidgetCell = SliderCell;
				}
				break;
			}
		}
	}
}

void USettingsItem::NativeConstruct()
{
	Super::NativeConstruct();

	if (WidgetCell)
	{
		WidgetCell->OnValueChangedEvent.AddUObject(this, &USettingsItem::OnValueChanged);
		WidgetCell->OnValueCommittedEvent.AddUObject(this, &USettingsItem::OnValueCommitted);
	}
}

void USettingsItem::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	SetActive(bActive);
}

void USettingsItem::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(InGeometry, InMouseEvent);

	OnChangeHover(true);
	OnHover.Broadcast(true);
}

void USettingsItem::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseLeave(InMouseEvent);

	OnChangeHover(false);
	OnHover.Broadcast(false);
}

bool USettingsItem::SetCurrentValue(const FText& InValue)
{
	const FString InValueString = InValue.ToString();
	return SetCurrentValue(InValueString);
}

bool USettingsItem::SetCurrentValue(const FString& InValue)
{
	if (!Items.Contains(InValue))
	{
		return false;
	}

	if (ESettingsItemType::ComboBox == SettingsItemType)
	{
		if (const auto CurrentWidget = Cast<USettingsItemCell_ComboBox>(WidgetCell))
		{
			CurrentWidget->SetSelectedOption(InValue);
			return true;
		}
	}

	return false;
}

bool USettingsItem::SetCurrentValue(float InValue)
{
	if (ESettingsItemType::Slider == SettingsItemType)
	{
		if (const auto CurrentWidget = Cast<USettingsItemCell_Slider>(WidgetCell))
		{
			CurrentWidget->SetCurrentValue(InValue);
			return true;
		}
	}

	return false;
}

void USettingsItem::SetSelectedIndex(int32 Index)
{
	if (ESettingsItemType::ComboBox == SettingsItemType)
	{
		if (!Items.IsValidIndex(Index))
		{
			return;
		}

		if (const auto CurrentWidget = Cast<USettingsItemCell_ComboBox>(WidgetCell))
		{
			CurrentWidget->SetSelectedIndex(Index);
		}
	}
}

void USettingsItem::AddItem(const FString& InItem)
{
	if (!Items.Contains(InItem))
	{
		Items.Add(InItem);

		if (ESettingsItemType::ComboBox == SettingsItemType)
		{
			if (const auto CurrentWidget = Cast<USettingsItemCell_ComboBox>(WidgetCell))
			{
				CurrentWidget->AddOption(InItem);
			}
		}
	}
}

void USettingsItem::ClearItems()
{
	if (Items.Num() > 0)
	{
		Items.Empty();
		
		if (SettingsItemType == ESettingsItemType::ComboBox)
		{
			if (const auto CurrentWidget = Cast<USettingsItemCell_ComboBox>(WidgetCell))
			{
				CurrentWidget->ClearOptions();
			}
		}
	}
}

int32 USettingsItem::Num() const
{
	return Items.Num();
}

void USettingsItem::SetActive(bool bInActive)
{
	bActive = bInActive;
	if (Container)
	{
		if (bActive)
		{
			Container->SetRenderOpacity(1.f);
			Container->SetIsEnabled(true);
		}
		else
		{
			Container->SetRenderOpacity(InactiveOpacity);
			Container->SetIsEnabled(false);
		}
	}
}

void USettingsItem::OnValueChanged() const
{
	OnValueChangedEvent.Broadcast();
}

void USettingsItem::OnValueCommitted() const
{
	OnValueCommittedEvent.Broadcast();
}
