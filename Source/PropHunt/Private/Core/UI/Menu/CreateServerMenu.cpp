// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/CreateServerMenu.h"

#include "Core/PHTypes.h"
#include "Core/PHGameInstance.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/UI/Menu/SettingsItem.h"
#include "Core/UI/Menu/SelectorItem.h"

#include "Components/Image.h"
#include "Components/ListView.h"

#include "GenericPlatform/GenericPlatformChunkInstall.h"


#define LOCTEXT_NAMESPACE "Menu.Host"

//Use an EMap index, get back the ChunkIndex that map should be part of.
//Instead of this mapping we should really use the AssetRegistry to query for chunk mappings, but maps aren't members of the AssetRegistry yet.
static const int ChunkMapping[] = { 1, 2 };

void UCreateServerMenu::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	if (!CreateButton->OnClicked.IsBound())
	{
		CreateButton->OnClicked.AddUObject(this, &UCreateServerMenu::OnCreateServerSelected);
	}
	if (!ReturnButton->OnClicked.IsBound())
	{
		ReturnButton->OnClicked.AddUObject(this, &UCreateServerMenu::ReturnToMainMenu);
	}
	if (!ServerName->OnValueChangedEvent.IsBound())
	{
		ServerName->OnValueChangedEvent.AddUObject(this, &UCreateServerMenu::OnServerNameChanged);
	}
	if (!ServerName->OnValueCommittedEvent.IsBound())
	{
		ServerName->OnValueCommittedEvent.AddUObject(this, &UCreateServerMenu::OnServerNameCommitted);
	}

	if (!MapList->OnItemIsHoveredChanged().IsBound())
	{
		MapList->OnItemIsHoveredChanged().AddUObject(this, &UCreateServerMenu::OnMapHovered);
	}
	if (!MapList->OnItemSelectionChanged().IsBound())
	{
		MapList->OnItemSelectionChanged().AddUObject(this, &UCreateServerMenu::OnMapSelected);
	}
}

void UCreateServerMenu::NativeConstruct()
{
	Super::NativeConstruct();

	if (auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		for (FMapRow* Map : GameInstance->GetMapList())
		{
			auto NewMapEntry = NewObject<USelectionEntry>();
			NewMapEntry->Label = Map->Label;
			MapList->AddItem(NewMapEntry);
		}

		MapList->SetSelectedIndex(0);
	}
}

FReply UCreateServerMenu::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	FReply Result = Super::NativeOnKeyDown(InGeometry, InKeyEvent);
	
	const FKey Key = InKeyEvent.GetKey();
	if (Key == EKeys::Enter || Key == EKeys::Virtual_Accept)
	{
		OnCreateServerSelected();
		Result = FReply::Handled();
	}
	
	return Result;
}

void UCreateServerMenu::OnCreateServerSelected()
{
#if WITH_EDITOR
	if (GIsEditor)
	{
		return;
	}
#endif

	if (!IsMapReady() || !IsServerNameValid() || bControlsLocked)
	{
		return;
	}

	if (auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		const bool bLanMatchValue = LanMatch->GetValue<bool>();
		GameInstance->SetOnlineMode(bLanMatchValue ? EOnlineMode::LAN : EOnlineMode::Online);
	}

	LockControls(true);
	HostGame();
}

bool UCreateServerMenu::IsServerNameValid() const
{
	const FString& TestName = ServerName->GetValue<FString>();
	return TestName.Len() >= ServerNameMinLength;
}

void UCreateServerMenu::HostGame()
{
	auto GameInstance = GetGameInstance<UPHGameInstance>();
	if (ensure(GameInstance) && GetOwningLocalPlayer())
	{
		FString GameType;
		switch (GameMode->GetValue<int32>())
		{
			case 0:
				GameType = "Hunt";
				break;
			case 1:
				GameType = "Escape";
				break;
			default:
				GameType = "Hunt";
		}

		const FString ServerNameValue = ServerName->GetValue<FString>();
		const int32 NumPlayersValue = FCString::Atoi(*PlayersNumber->GetValue<FString>());
		const bool bRecordDemoValue = RecordingDemo->GetValue<bool>();
		const bool bAllowInternationalTaunts = InternationalTaunts->GetValue<bool>();
		GameInstance->HostGame(GetOwningLocalPlayer(), GetMapName(), ServerNameValue, NumPlayersValue, GameType, bRecordDemoValue, bAllowInternationalTaunts);
	}
}

void UCreateServerMenu::OnServerNameChanged()
{
	CreateButton->SetIsActive(IsServerNameValid());
}

void UCreateServerMenu::OnServerNameCommitted()
{
	OnCreateServerSelected();
}

struct FMapRow* UCreateServerMenu::GetSelectedMapRow(USelectionEntry* InSelectedMap) const
{
	if (auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		FMapRow* const* MapEntry = GameInstance->GetMapList().FindByPredicate([InSelectedMap](auto Row) {
			return Row->Label.EqualTo(InSelectedMap->Label);
		});

		if (MapEntry)
		{
			return *MapEntry;
		}
	}

	return nullptr;
}

void UCreateServerMenu::OnMapHovered(UObject* InHoveredMap, bool bHovered)
{
	if (auto Map = Cast<USelectionEntry>(InHoveredMap))
	{
		Map->OnItemHoverEvent.ExecuteIfBound(bHovered);
	}
}

void UCreateServerMenu::OnMapSelected(UObject* InSelectedMap)
{
	if (bControlsLocked)
	{
		return;
	}

	if (auto SelectedMap = Cast<USelectionEntry>(InSelectedMap))
	{
		FMapRow* MapEntry = GetSelectedMapRow(SelectedMap);
		if (MapEntry && MapIcon)
		{
			MapIcon->SetBrushFromTexture(MapEntry->Icon);
		}
	}
}

bool UCreateServerMenu::IsMapReady() const
{
	bool bReady = true;
	IPlatformChunkInstall* ChunkInstaller = FPlatformMisc::GetPlatformChunkInstall();
	if (ChunkInstaller)
	{
		EMap Map = GetSelectedMap();
		// should use the AssetRegistry as soon as maps are added to the AssetRegistry
		const int32 MapChunk = ChunkMapping[static_cast<int32>(Map)];
		const EChunkLocation::Type ChunkLocation = ChunkInstaller->GetPakchunkLocation(MapChunk);
		if (ChunkLocation == EChunkLocation::NotAvailable)
		{
			bReady = false;
		}
	}
	return bReady;
}

UCreateServerMenu::EMap UCreateServerMenu::GetSelectedMap() const
{
	const int32 MapIndex = MapList->GetIndexForItem(MapList->GetSelectedItem());
	return MapIndex != INDEX_NONE ? static_cast<EMap>(MapIndex) : EMap::EMax;
}

FString UCreateServerMenu::GetMapName() const
{
	if (auto SelectedMap = MapList->GetSelectedItem<USelectionEntry>())
	{
		if (FMapRow* MapEntry = GetSelectedMapRow(SelectedMap))
		{
			return MapEntry->FileName;
		}
	}

	return FString();
}

#undef LOCTEXT_NAMESPACE
