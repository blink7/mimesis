// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/SettingsMenu.h"

#include "Core/PHTypes.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/UI/Widgets/Dialog.h"
#include "Core/UI/Menu/SubSettings.h"
#include "Core/UI/Menu/GraphicsSubSettings.h"

#include "Components/TextBlock.h"
#include "Components/WidgetSwitcher.h"

#define LOCTEXT_NAMESPACE "Menu"

void USettingsMenu::NativeConstruct()
{
	Super::NativeConstruct();

	if (!CommonButton->OnClicked.IsBound())
	{
		CommonButton->OnClicked.AddUObject(this, &USettingsMenu::OnMenuSelected, static_cast<uint8>(EMenuType::CommonSettings));
	}
	if (!GraphicsButton->OnClicked.IsBound())
	{
		GraphicsButton->OnClicked.AddUObject(this, &USettingsMenu::OnMenuSelected, static_cast<uint8>(EMenuType::GraphicsSettings));
	}
	if (!AudioButton->OnClicked.IsBound())
	{
		AudioButton->OnClicked.AddUObject(this, &USettingsMenu::OnMenuSelected, static_cast<uint8>(EMenuType::AudioSettings));
	}
	if (!ControlsButton->OnClicked.IsBound())
	{
		ControlsButton->OnClicked.AddUObject(this, &USettingsMenu::OnMenuSelected, static_cast<uint8>(EMenuType::ControlsSettings));
	}
	if (!MultiplayerButton->OnClicked.IsBound())
	{
		MultiplayerButton->OnClicked.AddUObject(this, &USettingsMenu::OnMenuSelected, static_cast<uint8>(EMenuType::MultiplayerSettings));
	}
	if (!ApplyButton->OnClicked.IsBound())
	{
		ApplyButton->OnClicked.AddUObject(this, &USettingsMenu::OnApplySettings);
	}
	if (!AutoGraphicsButton->OnClicked.IsBound())
	{
		AutoGraphicsButton->OnClicked.AddUObject(this, &USettingsMenu::OnAutoDetectGraphics);
	}
	if (!ResetButton->OnClicked.IsBound())
	{
		ResetButton->OnClicked.AddUObject(this, &USettingsMenu::OnResetSettings);
	}
	if (!ReturnButton->OnClicked.IsBound())
	{
		ReturnButton->OnClicked.AddUObject(this, &USettingsMenu::ReturnToMainMenu);
	}
}

void USettingsMenu::ChangeNavigationState(bool bActive)
{
	switch (CurrentMenuType)
	{
		case EMenuType::CommonSettings:
			CommonButton->SetIsActive(bActive);
			break;
		case EMenuType::GraphicsSettings:
			GraphicsButton->SetIsActive(bActive);
			break;
		case EMenuType::AudioSettings:
			AudioButton->SetIsActive(bActive);
			break;
		case EMenuType::ControlsSettings:
			ControlsButton->SetIsActive(bActive);
			break;
		case EMenuType::MultiplayerSettings:
			MultiplayerButton->SetIsActive(bActive);
			break;
	}
}

void USettingsMenu::ReturnToMainMenu()
{
	if (CurrentSubSettings && CurrentSubSettings->IsDirty())
	{
		Dialog = CreateWidget<UDialog>(this, DialogClass);
		if (Dialog)
		{
			Dialog->ConstructMessageDialog(LOCTEXT("Confirm", "Confirm"),
				LOCTEXT("DirtySettingsMessage", "Do you want to apply changes?"),
				NSLOCTEXT("DialogButtons", "Ok", "Ok"),
				NSLOCTEXT("DialogButtons", "Cancel", "Cancel"));
			Dialog->OnConfirmClicked.AddUObject(this, &USettingsMenu::Return, true);
			Dialog->OnCancelClicked.AddUObject(this, &USettingsMenu::Return, false);

			OnShowDialogEvent.Broadcast(Dialog);
		}
	}
	else
	{
		Super::ReturnToMainMenu();
	}
}

void USettingsMenu::Setup()
{
	SelectMenu(EMenuType::CommonSettings);
}

void USettingsMenu::OnMenuSelected(uint8 InType)
{
	if (CurrentSubSettings && CurrentSubSettings->IsDirty())
	{
		Dialog = CreateWidget<UDialog>(this, DialogClass);
		if (Dialog)
		{
			Dialog->ConstructMessageDialog(LOCTEXT("Confirm", "Confirm"),
				LOCTEXT("DirtySettingsMessage", "Do you want to apply changes?"),
				NSLOCTEXT("DialogButtons", "Ok", "Ok"),
				NSLOCTEXT("DialogButtons", "Cancel", "Cancel"));
			Dialog->OnConfirmClicked.AddUObject(this, &USettingsMenu::SwitchSettings, InType, true);
			Dialog->OnCancelClicked.AddUObject(this, &USettingsMenu::SwitchSettings, InType, false);

			OnShowDialogEvent.Broadcast(Dialog);
		}
	}
	else
	{
		SelectMenu(InType);
	}
}

void USettingsMenu::SelectMenu(uint8 InType)
{
	if (CurrentSubSettings && CurrentSubSettings->OnDirtyEvent.IsBoundToObject(this))
	{
		CurrentSubSettings->OnDirtyEvent.RemoveAll(this);
	}

	//Clear previous flag
	ChangeNavigationState(true);

	CurrentMenuType = InType;

	//Set current button as inactive
	ChangeNavigationState(false);

	if (EMenuType::GraphicsSettings == CurrentMenuType)
	{
		AutoGraphicsButton->SetIsActive(true);
	}
	else
	{
		AutoGraphicsButton->SetIsActive(false);
	}

	switch (CurrentMenuType)
	{
		case EMenuType::CommonSettings:
			SubSettingsSwitcher->SetActiveWidgetIndex(0);
			break;
		case EMenuType::GraphicsSettings:
			SubSettingsSwitcher->SetActiveWidgetIndex(1);
			break;
		case EMenuType::AudioSettings:
			SubSettingsSwitcher->SetActiveWidgetIndex(2);
			break;
		case EMenuType::ControlsSettings:
			SubSettingsSwitcher->SetActiveWidgetIndex(3);
			break;
		case EMenuType::MultiplayerSettings:
			SubSettingsSwitcher->SetActiveWidgetIndex(4);
			break;
	}

	CurrentSubSettings = Cast<USubSettings>(SubSettingsSwitcher->GetActiveWidget());
	if (!CurrentSubSettings->OnDirtyEvent.IsBoundToObject(this))
	{
		CurrentSubSettings->OnDirtyEvent.AddLambda([this](bool bIsDirty) {
			if (ApplyButton->IsActive() != bIsDirty)
			{
				ApplyButton->SetIsActive(bIsDirty);
			}
		});
	}

	SettingDescription->SetText(FText::GetEmpty());

	if (!CurrentSubSettings->OnSettingHoveredEvent.IsBoundToObject(this))
	{
		CurrentSubSettings->OnSettingHoveredEvent.AddLambda([this](FText Label, FText Description) {
			SettingLabel->SetText(Label);
			SettingDescription->SetText(Description);
		});
	}

	CurrentSubSettings->OnOpened();
}

void USettingsMenu::OnApplySettings()
{
	if (CurrentSubSettings)
	{
		CurrentSubSettings->ApplySettings();
	}
}

void USettingsMenu::OnAutoDetectGraphics()
{
	if (EMenuType::GraphicsSettings == CurrentMenuType)
	{
		const auto Graphics = Cast<UGraphicsSubSettings>(GraphicsSettings);
		if (Graphics)
		{
			Graphics->AutoDetect();
		}
	}
}

void USettingsMenu::OnResetSettings()
{
	if (CurrentSubSettings)
	{
		CurrentSubSettings->SetToDefaults();
	}
}

void USettingsMenu::SwitchSettings(uint8 SettingsToOpen, bool bSaveCurrent)
{
	HandleAfterDialog(bSaveCurrent);

	SetUserFocus(GetOwningPlayer());

	SelectMenu(SettingsToOpen);
}

void USettingsMenu::Return(bool bSaveCurrent)
{
	HandleAfterDialog(bSaveCurrent);

	Super::ReturnToMainMenu();
}

void USettingsMenu::HandleAfterDialog(bool bSaveCurrent)
{
	if (CurrentSubSettings)
	{
		if (bSaveCurrent)
		{
			CurrentSubSettings->ApplySettings();
		}
		else
		{
			CurrentSubSettings->RevertChanges();
		}
	}

	if (Dialog)
	{
		Dialog->RemoveFromParent();
		Dialog = nullptr;
	}
}

#undef LOCTEXT_NAMESPACE
