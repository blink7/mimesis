// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/TeamSelect.h"

#include "Core/PHTypes.h"
#include "Core/PHUtilities.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/UI/Menu/TeamSelectEntry.h"

#include "Components/TextBlock.h"
#include "Components/PanelWidget.h"


#define LOCTEXT_NAMESPACE "TeamSelect"

UTeamSelect::UTeamSelect(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bShowMouseCursor = true;
}

void UTeamSelect::NativeConstruct()
{
	Super::NativeConstruct();

	if (!JoinHuntersButton->OnClicked.IsBound())
	{
		JoinHuntersButton->OnClicked.AddUObject(this, &UTeamSelect::OnJoinTeamSelected, static_cast<int32>(ETeamType::Hunter));
	}
	if (!JoinPropsButton->OnClicked.IsBound())
	{
		JoinPropsButton->OnClicked.AddUObject(this, &UTeamSelect::OnJoinTeamSelected, static_cast<int32>(ETeamType::Prop));
	}

	JoinHuntersButton->SetIsActive(false);
	JoinPropsButton->SetIsActive(false);
}

void UTeamSelect::ShowWidget()
{
	Super::ShowWidget();

	HunterList->ClearChildren();
	PropList->ClearChildren();

	BindEvents();
}

void UTeamSelect::HideWidget()
{
	Super::HideWidget();

	if (auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		GameState->OnRemainingTimeChanged.RemoveDynamic(this, &UTeamSelect::UpdateMatchStartTimer);
		GameState->OnPlayerStateChanged.RemoveDynamic(this, &UTeamSelect::OnPlayerStateChanged);
	}
}

void UTeamSelect::BindEvents()
{
	if (auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		GameState->OnRemainingTimeChanged.AddDynamic(this, &UTeamSelect::UpdateMatchStartTimer);
		UpdateMatchStartTimer(GameState->GetRemainingTime());

		GameState->OnPlayerStateChanged.AddDynamic(this, &UTeamSelect::OnPlayerStateChanged);
		UpdatePlayerStateMaps();
	}
	else
	{
		// Keep retrying until Game State is ready
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_BindMatchTimerChanges, this, &UTeamSelect::BindEvents, 0.2f);
	}
}

void UTeamSelect::UpdateMatchStartTimer(int32 Time)
{
	if (Time >= 0)
	{
		TimerText->SetText(FText::Format(LOCTEXT("MatchTimer", ">> Remaining time {0}"), FText::FromString(PropHunt::Utilities::TimeToString(Time))));
	}
	else
	{
		TimerText->SetText(LOCTEXT("EmptyTimer", ">> _"));
	}
}

void UTeamSelect::OnPlayerStateChanged(APlayerState* Owner)
{
	UpdatePlayerStateMaps();
}

void UTeamSelect::OnJoinTeamSelected(int32 TeamNumber)
{
	if (auto PlayerController = GetOwningPlayer<APHPlayerController>())
	{
		InTeam = TeamNumber;
		PlayerController->ChooseTeam(TeamNumber);
	}
}

void UTeamSelect::UpdatePlayerStateMaps()
{
	if (GetOwningPlayer())
	{
		if (auto GameState = GetWorld()->GetGameState<APHGameState>())
		{
			const int32 NumTeams = FMath::Max(GameState->NumTeams, 1);

			PlayerStateMaps.Reset();
			PlayerStateMaps.AddZeroed(NumTeams);

			for (int32 TeamNumber = 0; TeamNumber < NumTeams; TeamNumber++)
			{
				GameState->GetTimedPlayerMap(TeamNumber, PlayerStateMaps[TeamNumber]);
			}

			UpdateTeamTable();
		}
	}
}

void UTeamSelect::UpdateTeamTable()
{
	auto GameState = GetWorld()->GetGameState<APHGameState>();
	auto MyPlayerState = GetOwningPlayer()->GetPlayerState<APHPlayerState>();
	if (GameState && MyPlayerState)
	{
		InTeam = MyPlayerState->GetTeamNumber();
		
		const int32 NumTeams = GameState->NumTeams;
		const int32 MaxPlayers = GameState->MaxPlayers;

		for (int32 TeamNumber = 0; TeamNumber < NumTeams; TeamNumber++)
		{
			for (int32 PlayerNumber = 0; PlayerNumber < ((TeamNumber != ETeamType::Spectator) ? MaxPlayers / 2 : MaxPlayers); PlayerNumber++)
			{
				FPlayerData PlayerData;

				auto FoundPlayerState = PlayerStateMaps[TeamNumber].Find(PlayerNumber);
				if (FoundPlayerState)
				{
					APHPlayerState* PlayerState = FoundPlayerState->Get();
					PlayerData.OnlinePlayerId = PlayerState->GetUniqueId().GetUniqueNetId()->ToString();
					PlayerData.PlayerName = PlayerState->GetShortPlayerName();

					if (PlayerState == MyPlayerState)
					{
						PlayerData.bIsOwnerPlayer = true;
					}

					SetTableItem(PlayerData, NumTeams, TeamNumber, PlayerNumber);
				}
				else if (UPanelWidget* TeamList = GetTeamWidget(TeamNumber))
				{
					TeamList->RemoveChildAt(PlayerNumber);
				}
			}

			if (UButtonWrapper* JoinButton = GetTeamButton(TeamNumber))
			{
				const bool bTeamAvailable = AllowedToJoinByBalance(TeamNumber, MaxPlayers) && MyPlayerState->GetTeamNumber() != TeamNumber;
				if (bTeamAvailable != JoinButton->IsActive())
				{
					JoinButton->SetIsActive(bTeamAvailable);
				}

				JoinButton->SetSecondText(FText::Format(LOCTEXT("TeamCounter", "...{0}/{1}"), PlayerStateMaps[TeamNumber].Num(), MaxPlayers / 2));
			}
		}

		bInitialized = true;
	}
}

void UTeamSelect::SetTableItem(const struct FPlayerData& PlayerData, int32 NumTeams, int32 TeamNumber, int32 Index)
{
	if (UPanelWidget* TeamWidget = GetTeamWidget(TeamNumber))
	{
		auto Item = Cast<UTeamSelectEntry>(TeamWidget->GetChildAt(Index));
		if (!Item)
		{
			Item = CreateWidget<UTeamSelectEntry>(this, TeamSelectEntryClass);
			TeamWidget->AddChild(Item);
		}

		Item->SetPlayerData(PlayerData);
	}
}

bool UTeamSelect::AllowedToJoinByBalance(int32 TeamNumber, int32 MaxPlayers)
{
	for (int32 i = 0; i < PlayerStateMaps.Num(); i++)
	{
		if (i == TeamNumber || i == ETeamType::Spectator)
		{
			continue;
		}

		int32 TestNum = PlayerStateMaps[i].Num();
		if (MaxPlayers > 8)
		{
			// allow difference in teams with 2 players summary
			TestNum += 1;
		}
		if (i == InTeam)
		{
			// don't consider our presence in the team
			TestNum -= 1;
		}

		if (PlayerStateMaps[TeamNumber].Num() > TestNum)
		{
			return false;
		}
	}

	return true;
}

UPanelWidget* UTeamSelect::GetTeamWidget(int32 TeamNumber) const
{
	switch (TeamNumber)
	{
		case ETeamType::Hunter:
			return HunterList;
		case ETeamType::Prop:
			return PropList;
		default:
			return nullptr;
	}
}

UButtonWrapper* UTeamSelect::GetTeamButton(int32 TeamNumber) const
{
	switch (TeamNumber)
	{
		case ETeamType::Hunter:
			return JoinHuntersButton;
		case ETeamType::Prop:
			return JoinPropsButton;
		default:
			return nullptr;
	}
}

#undef LOCTEXT_NAMESPACE
