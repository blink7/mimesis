// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/MultiplayerSubSettings.h"

#include "Core/PHTypes.h"
#include "Core/PHImageLoader.h"
#include "Core/PHGameInstance.h"
#include "Core/PHGameUserSettings.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/UI/Widgets/Dialog.h"
#include "Core/UI/Widgets/OpenFileWidget.h"
#include "Core/UI/Menu/SettingsItem.h"
#include "Core/Online/PHSprayHttpClient.h"

#include "ImageUtils.h"
#include "Components/Image.h"
#include "Core/Online/PHPlayerState.h"
#include "Engine/DataTable.h"
#include "Serialization/BufferArchive.h"
#include "GameFramework/PlayerState.h"

#define LOCTEXT_NAMESPACE "Menu.Settings"

static const FString SPRAY_EXTENSION = ".png";

void UMultiplayerSubSettings::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	DefaultSprayListTable->ForeachRow<FSprayRow>("LoadSprayList", [this](const FName& Key, const FSprayRow& SprayRow)
	{
		FSprayItem SprayItem(SprayRow.Image, Key.ToString());
		SprayList.Add(SprayItem);
	});

	for (const FSprayItem& SprayItem : SprayList)
	{
		SprayImage->AddItem(SprayItem.Label);
	}

	if (!SprayImage->OnValueChangedEvent.IsBound())
	{
		SprayImage->OnValueChangedEvent.AddUObject(this, &UMultiplayerSubSettings::OnSprayImageChanged);
	}

	if (!ImportSprayButton->OnClicked.IsBound())
	{
		ImportSprayButton->OnClicked.AddUObject(this, &UMultiplayerSubSettings::OnImportSpray);
	}

	SprayDirectory = FPaths::ProjectUserDir() + TEXT("Sprays/");
}

void UMultiplayerSubSettings::NativeConstruct()
{
	Super::NativeConstruct();

	LoadOptions();
	UpdateOptions();
}

void UMultiplayerSubSettings::LoadOptions()
{
	if (UserSettings)
	{
		SprayOpt = UserSettings->GetSpray();
	}
}

void UMultiplayerSubSettings::UpdateOptions()
{
	if (SprayImage->SetCurrentValue(SprayOpt))
	{
		OnSprayImageChanged();
	}
}

void UMultiplayerSubSettings::OnOpened()
{
	LoadSpraysFolder();
}

void UMultiplayerSubSettings::ApplySettings()
{
	const auto CurrentSpray = SprayImage->GetValue<FString>();
	FSprayItem* FoundSprayItem = SprayList.FindByKey(CurrentSpray);
	if (FoundSprayItem)
	{
		SprayOpt = FoundSprayItem->Label;
		if (FoundSprayItem->bCustom)
		{
			SaveSprayToDisk(FoundSprayItem->Image, SprayOpt);
			
			auto PS = GetOwningPlayer() ? GetOwningPlayer()->GetPlayerState<APHPlayerState>() : nullptr;
			auto GameInstance = GetGameInstance<UPHGameInstance>();
			if (PS && GameInstance)
			{
				const TSharedPtr<const FUniqueNetId> UserId = PS->GetUniqueId().GetUniqueNetId();
				if (UserId.IsValid())
				{
					const FString SprayPath = SprayDirectory + SprayOpt + SPRAY_EXTENSION;
					const FMD5Hash SprayHash = FMD5Hash::HashFile(*SprayPath);
					const FString SprayHashString = LexToString(SprayHash);

					auto SprayExistsCallback = [this, GameInstance, PS, UserId, FoundSprayItem](bool bExists)
					{
						if (bExists)
						{
							PS->OnSprayUpdated();
						}
						else
						{
							auto SprayUploadedCallback = [PS](bool bSuccess)
							{
								if (bSuccess)
								{
									PS->OnSprayUpdated();
								}
							};
							GameInstance->GetSprayHttpClient().UploadSpray(*UserId, FoundSprayItem->Image, SprayOpt, SprayUploadedCallback);
						}
					};
					GameInstance->GetSprayHttpClient().CheckSprayExists(*UserId, SprayHashString, SprayExistsCallback);
				}
			}
		}
	}

	if (UserSettings)
	{
		UserSettings->SetSpray(SprayOpt);
		UserSettings->ApplySettings(false);
	}

	Super::ApplySettings();
}

void UMultiplayerSubSettings::SetToDefaults()
{
	Super::SetToDefaults();

	SprayOpt = "default";
	UpdateOptions();
	ApplySettings();
}

void UMultiplayerSubSettings::RevertChanges()
{
	LoadOptions();
	UpdateOptions();

	Super::RevertChanges();
}

void UMultiplayerSubSettings::OnSprayImageChanged()
{
	const auto NewSpray = SprayImage->GetValue<FString>();
	FSprayItem* FoundSprayItem = SprayList.FindByKey(NewSpray);
	if (FoundSprayItem)
	{
		SprayPreview->SetBrushFromTexture(FoundSprayItem->Image);

		if (UserSettings)
		{
			SprayOpt = FoundSprayItem->Label;
			OnSettingChanged(SprayImage, !UserSettings->GetSpray().Equals(SprayOpt));
		}
	}
}

void UMultiplayerSubSettings::OnImportSpray()
{
	ImportSprayDialog = CreateWidget<UDialog>(GetWorld(), DialogClass);
	if (ImportSprayDialog)
	{
		ImportSprayDialog->ConstructOpenFileDialog(LOCTEXT("ImportSpray", "Import Spray Image"),
			NSLOCTEXT("DialogButtons", "Import", "Import"),
			NSLOCTEXT("DialogButtons", "Cancel", "Cancel"));
		ImportSprayDialog->OnConfirmClicked.AddUObject(this, &UMultiplayerSubSettings::ConfirmImportSpray);
		ImportSprayDialog->OnCancelClicked.AddUObject(this, &UMultiplayerSubSettings::HideOpenSprayDialog);

		ImportSprayDialog->AddToViewport();
	}
}

void UMultiplayerSubSettings::ConfirmImportSpray()
{
	if (ImportSprayDialog)
	{
		if (const auto OpenFileWidget = ImportSprayDialog->GetWidgetCell<UOpenFileWidget>())
		{
			const FString SprayImagePath = OpenFileWidget->GetCurrentPath();
			if (!SprayImagePath.IsEmpty())
			{
				HandleNewSpray(SprayImagePath);
			}
		}

		ImportSprayDialog->RemoveFromViewport();
	}
}

void UMultiplayerSubSettings::HideOpenSprayDialog()
{
	if (ImportSprayDialog)
	{
		ImportSprayDialog->RemoveFromViewport();
	}
}

void UMultiplayerSubSettings::HandleNewSpray(const FString& SprayPath)
{
	UPHImageLoader* ImageLoaderTask = UPHImageLoader::LoadImageFromDiskAsync(this, SprayPath);
	ImageLoaderTask->OnLoadCompleted().AddDynamic(this, &UMultiplayerSubSettings::OnUserSprayLoaded);
}

void UMultiplayerSubSettings::OnUserSprayLoaded(UTexture2D* SprayTexture, const FString& Path)
{
	SprayOpt = OnSprayLoaded(SprayTexture, Path);
	UpdateOptions();
}

void UMultiplayerSubSettings::OnGameFolderSprayLoaded(UTexture2D* SprayTexture, const FString& Path)
{
	OnSprayLoaded(SprayTexture, Path);

	if (--SprayDirectoryLoadCounter == 0)
	{
		FSprayItem* FoundSprayItem = SprayList.FindByKey(SprayOpt);
		if (FoundSprayItem && FoundSprayItem->bCustom)
		{
			UpdateOptions();
		}
		else if (!FoundSprayItem)
		{
			SetToDefaults();
		}
	}
}

FString UMultiplayerSubSettings::OnSprayLoaded(UTexture2D* SprayTexture, const FString& Path)
{
	FSprayItem UserSpray(SprayTexture, FPaths::GetBaseFilename(Path), true);
	SprayList.Add(UserSpray);

	SprayImage->AddItem(UserSpray.Label);

	return UserSpray.Label;
}

void UMultiplayerSubSettings::SaveSprayToDisk(UTexture2D* Texture, const FString& Name)
{
	const int32 SrcWidth = Texture->GetSizeX();
	const int32 SrcHeight = Texture->GetSizeY();

	FByteBulkData& BulkData = Texture->PlatformData->Mips[0].BulkData;
	void* MipData = BulkData.Lock(LOCK_READ_WRITE);

	TArray<FColor> RawData;
	RawData.AddUninitialized(SrcWidth * SrcHeight);
	FMemory::Memcpy(RawData.GetData(), MipData, BulkData.GetBulkDataSize());

	BulkData.Unlock();

	TArray<uint8> CompressedRawData;
	FImageUtils::CompressImageArray(SrcWidth, SrcHeight, RawData, CompressedRawData);

	const FString DestinationPath = SprayDirectory + Name + SPRAY_EXTENSION;
	FFileHelper::SaveArrayToFile(CompressedRawData, *DestinationPath);
}

void UMultiplayerSubSettings::LoadSpraysFolder()
{
	auto DirVisitor = [this](const TCHAR* FilenameOrDirectory, bool bIsDirectory) {
		if (!bIsDirectory)
		{
			const FString FileExtension = FPaths::GetExtension(FilenameOrDirectory, true);
			if (FileExtension.Equals(SPRAY_EXTENSION, ESearchCase::IgnoreCase))
			{
				UPHImageLoader* ImageLoaderTask = UPHImageLoader::LoadImageFromDiskAsync(this, FilenameOrDirectory);
				ImageLoaderTask->OnLoadCompleted().AddDynamic(this, &UMultiplayerSubSettings::OnGameFolderSprayLoaded);
				SprayDirectoryLoadCounter++;
			}
		}

		return true;
	};

	FPlatformFileManager::Get().GetPlatformFile().IterateDirectory(*SprayDirectory, DirVisitor);
}

#undef LOCTEXT_NAMESPACE
