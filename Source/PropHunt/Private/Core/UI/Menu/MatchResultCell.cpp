// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/MatchResultCell.h"

#include "Core/PHGameInstance.h"
#include "Core/Online/SteamUtils.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"


#define LOCTEXT_NAMESPACE "MatchResult"

void UMatchResultCell::SetUp(FPlayerInfo InPlayerInfo)
{
	if (PlayerInfo != InPlayerInfo)
	{
		PlayerInfo = InPlayerInfo;
		Refresh();
	}
}

void UMatchResultCell::Refresh()
{
	PlayerName->SetText(FText::FromString(PlayerInfo.PlayerName).ToUpper());
	Score->SetText(FText::AsNumber(PlayerInfo.Score));
	LoadAvatar();

	TPair<FText, FText> Reward = GetTextReward(PlayerInfo.RewardType);
	RewardTitle->SetText(Reward.Key.ToUpper());
	Description->SetText(Reward.Value.ToUpper());
}

TPair<FText, FText> UMatchResultCell::GetTextReward(ERewardType RewardType)
{
	switch (RewardType)
	{
		case ERewardType::BestHunter:
			return TPair<FText, FText>(LOCTEXT("Reward1", "Best Hunter"), LOCTEXT("Description1", "Hunter Points"));
		case ERewardType::BestProp:
			return TPair<FText, FText>(LOCTEXT("Reward2", "Best Prop"), LOCTEXT("Description2", "Prop Points"));
		case ERewardType::MostTaunts:
			return TPair<FText, FText>(LOCTEXT("Reward3", "Most Taunts"), LOCTEXT("Description3", "Taunts"));
		case ERewardType::MostTransforms:
			return TPair<FText, FText>(LOCTEXT("Reward4", "Most Transforms"), LOCTEXT("Description4", "Transforms"));
		case ERewardType::MostInconspicuous:
			return TPair<FText, FText>(LOCTEXT("Reward5", "Most Inconspicuous"), LOCTEXT("Description5", "Survival Time"));
		default:
			return TPair<FText, FText>();
	}
}

void UMatchResultCell::LoadAvatar()
{
	const int SteamAvatar = FSteamUtils::GetSteamAvatar(PlayerInfo.OnlinePlayerId, EAvatarSize::Large);
	if (SteamAvatar > 0)
	{
		ApplySteamAvatar(PlayerInfo.OnlinePlayerId, SteamAvatar);
	}
	else if (SteamAvatar == -1)
	{
		if (auto GI = GetGameInstance<UPHGameInstance>())
		{
			GI->AvatarImageLoadedEvent.AddUObject(this, &UMatchResultCell::ApplySteamAvatar);
		}
	}
}

void UMatchResultCell::ApplySteamAvatar(FString SteamId, int Image)
{
	if (Image > 0 && SteamId.Equals(PlayerInfo.OnlinePlayerId))
	{
		UTexture2D* Avatar = FSteamUtils::GetSteamImageAsTexture(Image);
		PlayerAvatar->SetBrushFromTexture(Avatar);
	}
}

#undef LOCTEXT_NAMESPACE
