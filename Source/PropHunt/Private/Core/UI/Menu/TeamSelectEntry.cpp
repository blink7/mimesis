// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/TeamSelectEntry.h"

#include "Core/PHGameInstance.h"
#include "Core/Online/SteamUtils.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"


void UTeamSelectEntry::Refresh()
{
	if (!PlayerData.IsEmpty())
	{
		LoadAvatar();
	}

	PlayerName->SetText(FText::FromString(PlayerData.PlayerName));
}

void UTeamSelectEntry::SetPlayerData(FPlayerData NewPlayerData)
{
	if (PlayerData != NewPlayerData)
	{
		PlayerData = NewPlayerData;
		Refresh();
	}
}

void UTeamSelectEntry::LoadAvatar()
{
	const int SteamAvatar = FSteamUtils::GetSteamAvatar(PlayerData.OnlinePlayerId, EAvatarSize::Medium);
	if (SteamAvatar > 0)
	{
		UTexture2D* Avatar = FSteamUtils::GetSteamImageAsTexture(SteamAvatar);
		PlayerAvatar->SetBrushFromTexture(Avatar);
	}
}
