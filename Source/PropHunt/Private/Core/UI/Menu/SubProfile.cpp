// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/SubProfile.h"

#include "Core/UI/ButtonWrapper.h"


void USubProfile::NativeConstruct()
{
	Super::NativeConstruct();

	if (!ReturnButton->OnClicked.IsBoundToObject(this))
	{
		ReturnButton->OnClicked.AddLambda([this]() {
			OnReturnEvent.Broadcast();
		});
	}
}
