// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/SelectorItem.h"

#include "Components/TextBlock.h"


void USelectorItem::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	const auto Entry = Cast<USelectionEntry>(ListItemObject);
	if (Entry)
	{
		Label->SetText(Entry->Label);

		bActive = Entry->bEnabled;
		Entry->OnItemHoverEvent.BindUObject(this, &USelectorItem::OnChangeHover);

		//Reset widget to default state
		OnChangeHover(false);
		OnChangeSelection(false);
	}
}

void USelectorItem::NativeOnItemSelectionChanged(bool bIsSelected)
{
	OnChangeSelection(bIsSelected);
}
