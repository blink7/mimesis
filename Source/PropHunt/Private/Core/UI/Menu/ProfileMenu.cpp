// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/ProfileMenu.h"

#include "Core/UI/Menu/AbilityMenu.h"


void UProfileMenu::Setup()
{
	AbilityMenu->OnReturnEvent.AddUObject(this, &UProfileMenu::ReturnToMainMenu);
}

void UProfileMenu::OnPlayerLoaded()
{
	AbilityMenu->LoadAbilities();
}
