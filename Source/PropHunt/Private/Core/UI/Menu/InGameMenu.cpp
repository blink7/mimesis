// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/InGameMenu.h"

#include "Core/PHTypes.h"
#include "Core/UI/ButtonWrapper.h"
#include "Core/UI/Widgets/Dialog.h"
#include "Core/UI/Menu/SettingsMenu.h"
#include "Core/UI/Menu/SecondaryMenu.h"
#include "Core/PHGameInstance.h"
#include "Core/PHGameViewportClient.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"


#define LOCTEXT_NAMESPACE "Menu"

UInGameMenu::UInGameMenu(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bShowMouseCursor = true;
	bRespondUIOnly = true;
}

void UInGameMenu::NativeConstruct()
{
	Super::NativeConstruct();

	SecondaryMenu->OnReturnToMainMenuEvent.AddUObject(this, &UInGameMenu::ReturnToMe);

	if (!ContinueButton->OnClicked.IsBound())
	{
		ContinueButton->OnClicked.AddUObject(this, &UInGameMenu::OnHideMenu);
	}
	if (!SettingsButton->OnClicked.IsBound())
	{
		SettingsButton->OnClicked.AddUObject(this, &UInGameMenu::OnOptionsSelected);
	}
	if (!MainMenuButton->OnClicked.IsBound())
	{
		MainMenuButton->OnClicked.AddUObject(this, &UInGameMenu::OnExitToMain);
	}
	if (!QuitButton->OnClicked.IsBound())
	{
		QuitButton->OnClicked.AddUObject(this, &UInGameMenu::OnQuit);
	}

	PlayAnimationForward(NavigationFadeInAnim);
	PlayAnimationForward(BackgroundFadeInAnim);
}

FReply UInGameMenu::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	FReply Result = Super::NativeOnKeyDown(InGeometry, InKeyEvent);
	const FKey Key = InKeyEvent.GetKey();

	// clear settings changes when return on escape
	if (Key == EKeys::Escape)
	{
		OnHideMenu();
		Result = FReply::Handled();
	}

	return Result;
}

void UInGameMenu::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	if (bPendingToHide && Animation == NavigationFadeInAnim && !IsAnimationPlayingForward(Animation))
	{
		const auto GameViewportClient = Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport());
		if (GameViewportClient)
		{
			GameViewportClient->HideInGameMenu();
		}
	}
}

void UInGameMenu::ReturnToMe()
{
	PlayAnimationForward(NavigationFadeInAnim);

	MenuSwitcher->SetActiveWidgetIndex(0);

	SetUserFocus(GetOwningPlayer());
}

void UInGameMenu::OnHideMenu()
{
	bPendingToHide = true;
	PlayAnimationReverse(NavigationFadeInAnim);
	PlayAnimationReverse(BackgroundFadeInAnim);
}

void UInGameMenu::OnOptionsSelected()
{
	MenuSwitcher->SetActiveWidgetIndex(1);
	SecondaryMenu->OnSwitchMenu(EMenuType::Settings);

	PlayAnimationForward(MenuFadeOutAnim);
}

void UInGameMenu::OnExitToMain()
{
	const auto GameInstance = Cast<UPHGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		const auto Dialog = GameInstance->ShowMessage(LOCTEXT("Quit", "Quit Game"),
			LOCTEXT("QuitMessage", "Are you sure you want to return to the main menu?"),
			NSLOCTEXT("DialogButtons", "Ok", "Ok"),
			NSLOCTEXT("DialogButtons", "Cancel", "Cancel"));

		if (Dialog)
		{
			Dialog->OnConfirmClicked.AddUObject(this, &UInGameMenu::OnConfirmExitToMain);
		}
	}
}

void UInGameMenu::OnConfirmExitToMain()
{
	const auto GameInstance = Cast<UPHGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->LabelPlayerAsQuitter(GetOwningLocalPlayer());

		// Tell game instance to go back to main menu state
		GameInstance->GotoState(PHGameInstanceState::MainMenu);
	}
}

void UInGameMenu::OnQuit()
{
	const auto GameInstance = Cast<UPHGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		const auto Dialog = GameInstance->ShowMessage(LOCTEXT("Quit", "Quit Game"),
			LOCTEXT("QuitMessage", "Are you sure you want to exit the game?"),
			NSLOCTEXT("DialogButtons", "Ok", "Ok"),
			NSLOCTEXT("DialogButtons", "Cancel", "Cancel"));
		
		if (Dialog)
		{
			Dialog->OnConfirmClicked.AddUObject(this, &UInGameMenu::OnConfirmQuit);
		}
	}
}

void UInGameMenu::OnConfirmQuit()
{
	const auto GameInstance = Cast<UPHGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->LabelPlayerAsQuitter(GetOwningLocalPlayer());
	}

	GetOwningPlayer()->ConsoleCommand("quit");
}

#undef LOCTEXT_NAMESPACE
