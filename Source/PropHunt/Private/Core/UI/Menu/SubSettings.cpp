// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Menu/SubSettings.h"

#include "Core/PHLocalPlayer.h"
#include "Core/PHGameUserSettings.h"
#include "Core/UI/Menu/SettingsItem.h"

#include "Components/PanelWidget.h"

#include "Engine/Engine.h"


void USubSettings::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (!UserSettings)
	{
		UserSettings = UPHGameUserSettings::GetPHGameUserSettings();
	}
}

void USubSettings::NativeConstruct()
{
	Super::NativeConstruct();

	if (ItemList)
	{
		int32 Index = 0;
		for (const auto Item : ItemList->GetAllChildren())
		{
			const auto SettingItem = Cast<USettingsItem>(Item);
			if (SettingItem && ItemDescriptions.IsValidIndex(Index))
			{
				const FText Label = SettingItem->Label;
				const FText Description = ItemDescriptions[Index++];

				SettingItem->OnHover.AddLambda([this, Label, Description](bool bHovered) {
					if (bHovered)
					{
						OnSettingHoveredEvent.Broadcast(Label, Description);
					}
				});
			}
		}
	}
}

void USubSettings::ApplySettings()
{
	ResetDirtyState();
}

void USubSettings::SetToDefaults()
{
	OnSettingChanged(63, true);
}

void USubSettings::RevertChanges()
{
	ResetDirtyState();
}

void USubSettings::OnSettingChanged(const UWidget* InItemWidget, bool bInDirty)
{
	const int32 Index = ItemList ? ItemList->GetChildIndex(InItemWidget) : INDEX_NONE;
	if (Index != INDEX_NONE)
	{
		OnSettingChanged(Index, bInDirty);
	}
}

const FText& USubSettings::GetDescription(int32 Index) const
{
	if (ItemDescriptions.IsValidIndex(Index))
	{
		return ItemDescriptions[Index];
	}

	return FText::GetEmpty();
}

void USubSettings::OnSettingChanged(int32 InIndex, bool bInDirty)
{
	if (bInDirty)
	{
		bDirty |= 1LLU << InIndex;
	}
	else
	{
		bDirty &= ~(1LLU << InIndex);
	}

	OnDirtyEvent.Broadcast(IsDirty());
}

void USubSettings::ResetDirtyState()
{
	bDirty &= 0LLU;

	OnDirtyEvent.Broadcast(IsDirty());
}

int32 USubSettings::GetItemIndex(const UWidget* InItem)
{
	return ItemList->GetChildIndex(InItem);
}

UPHPersistentUser* USubSettings::GetPersistentUser() const
{
	const auto LP = GetOwningLocalPlayer<UPHLocalPlayer>();
	return LP ? LP->GetPersistentUser() : nullptr;
}

void USubSettings::OnSettingHovered(int32 Index, FText Label)
{

}
