﻿// Copyright Art-Sun Games. All Rights Reserved.


#include "Core/UI/Menu/CommonSubSettings.h"

#include "Core/PHGameInstance.h"
#include "Core/PHGameUserSettings.h"
#include "Core/UI/Menu/SettingsItem.h"

#include "Internationalization/Culture.h"

void UCommonSubSettings::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (!TauntLang->OnValueChangedEvent.IsBound())
	{
		TauntLang->OnValueChangedEvent.AddUObject(this, &UCommonSubSettings::OnTauntLangChanged);
	}
}

void UCommonSubSettings::NativeConstruct()
{
	Super::NativeConstruct();

	LoadOptions();
	UpdateOptions();
}

void UCommonSubSettings::LoadOptions()
{
	if (UserSettings)
	{
		TauntLangOpt = UserSettings->GetTauntLang();
	}
}

void UCommonSubSettings::UpdateOptions()
{
	FInternationalization& Internationalization = FInternationalization::Get();
	const TArray<FString> LocalizedCultureNames = FTextLocalizationManager::Get().GetLocalizedCultureNames(ELocalizationLoadFlags::Game);
	for (const FString& LocalizedCultureName : LocalizedCultureNames)
	{
		const FCulturePtr CulturePtr = Internationalization.GetCulture(LocalizedCultureName);
		if (CulturePtr.IsValid())
		{
			FString CultureName = CulturePtr->GetDisplayName();

			// Capitalize the first letter of a culture name. It's done by default for EN, but not for other languages.
			const FString CapitalizedFirstLetter = FText::FromString(CultureName.Left(1)).ToUpper().ToString();
			const FString RightPart = CultureName.RightChop(1);
			CultureName = CapitalizedFirstLetter + RightPart;
			
			TauntLang->AddItem(CultureName);
			LocalizationMap.Add(LocalizedCultureName, CultureName);
		}
	}

	FString* CurrentCultureName = LocalizationMap.Find(TauntLangOpt);
	if (CurrentCultureName)
	{
		TauntLang->SetCurrentValue(*CurrentCultureName);
	}

	if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
	{
		const bool bInMainMenu = GameInstance->IsInMainMenu();
		TauntLang->SetActive(bInMainMenu);
	}
}

void UCommonSubSettings::ApplySettings()
{
	if (UserSettings)
	{
		const FString* CurrentCulture = LocalizationMap.FindKey(TauntLangOpt);
		if (CurrentCulture)
		{
			UserSettings->SetTauntLang(*CurrentCulture);
		}
	}

	Super::ApplySettings();
}

void UCommonSubSettings::SetToDefaults()
{
	Super::SetToDefaults();

	if (UserSettings)
	{
		UserSettings->SetGameToDefault();

		LoadOptions();
		UpdateOptions();
	}
}

void UCommonSubSettings::RevertChanges()
{
	LoadOptions();
	UpdateOptions();

	Super::RevertChanges();
}

void UCommonSubSettings::OnTauntLangChanged()
{
	if (UserSettings)
	{
		TauntLangOpt = TauntLang->GetValue<FString>();
		const FString* CurrentCulture = LocalizationMap.FindKey(TauntLangOpt);
		if (CurrentCulture)
		{
			OnSettingChanged(TauntLang, UserSettings->GetTauntLang() != *CurrentCulture);
		}
	}
}
