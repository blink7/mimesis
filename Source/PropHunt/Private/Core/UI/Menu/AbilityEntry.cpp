// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Menu/AbilityEntry.h"

#include "Core/PHTypes.h"
#include "Core/Items/PHSkillItem.h"

#include "Components/SizeBox.h"
#include "Components/TextBlock.h"


void UAbilityEntry::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (EApplyAnimation::Stop != ApplyAnimation)
	{
		const float DesiredWidth = StatusText->GetDesiredSize().X;
		const float AnimationSpeed = DesiredWidth / ApplyAnimationDuration;
		float NewWidth = -1.f;

		if (EApplyAnimation::PlayForward == ApplyAnimation)
		{
			NewWidth = FMath::FInterpConstantTo(StatusSize->WidthOverride, DesiredWidth, InDeltaTime, AnimationSpeed);

			if (FMath::IsNearlyEqual(NewWidth, DesiredWidth, 0.1f))
			{
				ApplyAnimation = EApplyAnimation::Stop;
			}
		}
		else if (EApplyAnimation::PlayReverse == ApplyAnimation)
		{
			NewWidth = FMath::FInterpConstantTo(StatusSize->WidthOverride, 0.f, InDeltaTime, AnimationSpeed);

			if (FMath::IsNearlyZero(NewWidth, 0.1f))
			{
				ApplyAnimation = EApplyAnimation::Stop;
			}
		}

		StatusSize->SetWidthOverride(NewWidth);
	}
}

void UAbilityEntry::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	if (auto Item = Cast<UAbilityItemData>(ListItemObject))
	{
		ItemData = Item;

		ItemData->OnUpdateStateDelegate.BindUObject(this, &UAbilityEntry::OnUpdateStatus);

		if (!ItemData->OnAbilityHoverEvent.IsBoundToObject(this))
		{
			ItemData->OnAbilityHoverEvent.BindUObject(this, &UAbilityEntry::OnChangeHover);
		}

		if (ItemData->bRoot)
		{
			if (ItemData->Team == ETeamType::Hunter)
			{
				Setup(NSLOCTEXT("Menu.AbilityEntry", "Hunter", "Hunter"), ItemData->bRoot);
			}
			else if (ItemData->Team == ETeamType::Prop)
			{
				Setup(NSLOCTEXT("Menu.AbilityEntry", "Prop", "Prop"), ItemData->bRoot);
			}
		}
		else
		{
			Setup(ItemData->AbilityItem->ItemName, ItemData->bRoot);
		}

		OnUpdateStatus();
	}
}

EAbilityStatus UAbilityEntry::GetStatus() const
{
	return ItemData ? ItemData->Status : EAbilityStatus::NotAvailable;
}

bool UAbilityEntry::IsRoot() const
{
	return ItemData ? ItemData->bRoot : true;
}

void UAbilityEntry::OnUpdateStatus()
{
	if (!IsRoot())
	{
		if (GetStatus() == EAbilityStatus::Applied)
		{
			ApplyAnimation = EApplyAnimation::PlayForward;
		}
		else
		{
			ApplyAnimation = EApplyAnimation::PlayReverse;
		}
	}
}
