// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/PHThrobber.h"

#include "SPHThrobber.h"


#define LOCTEXT_NAMESPACE "UMG"

static FSlateBrush* DefaultThrobberBrush = nullptr;

UPHThrobber::UPHThrobber(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	Length(10),
	NumberOfPieces(3),
	AnimationFrequency(0.1f)
{
	if (DefaultThrobberBrush == nullptr)
	{
		// HACK: THIS SHOULD NOT COME FROM CORESTYLE AND SHOULD INSTEAD BE DEFINED BY ENGINE TEXTURES/PROJECT SETTINGS
		DefaultThrobberBrush = new FSlateBrush(*FCoreStyle::Get().GetBrush("Throbber.Chunk"));

		// Unlink UMG default colors from the editor settings colors.
		DefaultThrobberBrush->UnlinkColors();
	}

	Image = *DefaultThrobberBrush;
}

void UPHThrobber::SetLength(int32 InLength)
{
	Length = InLength;
	if (MyThrobber.IsValid())
	{
		MyThrobber->SetLength(FMath::Clamp(Length, 2, 100));
	}
}

void UPHThrobber::SetNumberOfPieces(int32 InNumberOfPieces)
{
	NumberOfPieces = InNumberOfPieces;
	if (MyThrobber.IsValid())
	{
		MyThrobber->SetNumPieces(FMath::Clamp(NumberOfPieces, 1, 100));
	}
}

void UPHThrobber::SetAnimationFrequency(float InFrequency)
{
	AnimationFrequency = InFrequency;
	if (MyThrobber.IsValid())
	{
		MyThrobber->SetAnimationFrequency(FMath::Clamp(AnimationFrequency, 0.0001f, 1.f));
	}
}

void UPHThrobber::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	MyThrobber->SetLength(FMath::Clamp(Length, 2, 100));
	MyThrobber->SetNumPieces(FMath::Clamp(NumberOfPieces, 1, 100));
	MyThrobber->SetAnimationFrequency(FMath::Clamp(AnimationFrequency, 0.0001f, 1.f));
}

void UPHThrobber::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	MyThrobber.Reset();
}

#if WITH_EDITOR

const FText UPHThrobber::GetPaletteCategory()
{
	return LOCTEXT("Primitive", "Primitive");
}

#endif

TSharedRef<SWidget> UPHThrobber::RebuildWidget()
{
	MyThrobber = SNew(SPHThrobber)
		.PieceImage(&Image)
		.Length(FMath::Clamp(Length, 2, 100))
		.NumPieces(FMath::Clamp(NumberOfPieces, 1, 100))
		.AnimationFrequency(FMath::Clamp(AnimationFrequency, 0.0001f, 1.f));

	return MyThrobber.ToSharedRef();
}

#undef LOCTEXT_NAMESPACE
