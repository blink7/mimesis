// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/DosBorder.h"

#include "Components/TextBlock.h"
#include "Components/NamedSlot.h"
#include "Components/OverlaySlot.h"
#include "Rendering/DrawElements.h"


void UDosBorder::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	if (Header)
	{
		if (bShowHeader)
		{
			Header->SetVisibility(ESlateVisibility::Visible);
			Header->SetText(HeaderText);
			Header->SetColorAndOpacity(Color);
		}
		else
		{
			Header->SetVisibility(ESlateVisibility::Hidden);
		}
	}

	const auto ContentSlot = Cast<UOverlaySlot>(Content->Slot);
	check(ContentSlot);
	if (ContentSlot)
	{
		FMargin NewPadding((bDoubleBorder ? Thickness * 2.f : Thickness) + SpaceWidth);
		ContentSlot->SetPadding(NewPadding + ContentPadding);
	}
}

int32 UDosBorder::NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	const int32 MaxLayerID = Super::NativePaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);

	FPaintContext Context(AllottedGeometry, MyCullingRect, OutDrawElements, MaxLayerID, InWidgetStyle, bParentEnabled);

	const FVector2D& Size = AllottedGeometry.GetLocalSize();
	const float HalfThickness = Thickness / 2.f;

	float Top = HalfThickness;
	float Bottom = Size.Y - HalfThickness;
	float Left = HalfThickness;
	float Right = Size.X - HalfThickness;

	DrawRectangle(Context, Top, Bottom, Left, Right, Size);

	if (bDoubleBorder)
	{
		const float Width = SpaceWidth + Thickness;

		Top += Width;
		Bottom -= Width;
		Left += Width;
		Right -= Width;

		DrawRectangle(Context, Top, Bottom, Left, Right, Size);
	}

	return FMath::Max(MaxLayerID, Context.MaxLayer);
}

void UDosBorder::DrawRectangle(FPaintContext& InContext, float InTop, float InBottom, float InLeft, float InRight, const FVector2D& OverallSize) const
{
	InContext.MaxLayer++;

	FVector2D Point_LeftTop(InLeft, InTop);
	FVector2D Point_LeftBottom(InLeft, InBottom);
	FVector2D Point_RightBottom(InRight, InBottom);
	FVector2D Point_RightTop(InRight, InTop);

	TArray<FVector2D> Points;

	float NewSpaceWidth = BlankWidth;

	if (bShowHeader)
	{
		const float HeaderWidth = Header->GetCachedGeometry().GetLocalSize().X;
		NewSpaceWidth = HeaderWidth + 30.f;
	}

	if (bEnableBlank)
	{
		FVector2D Point_BlankLeft;

		switch (BlankPosition)
		{
		case EBlankPosition::Left:
			Point_BlankLeft = { Thickness / 2.f + BlankOffset, InTop };
			break;
		case EBlankPosition::Center:
			Point_BlankLeft = { OverallSize.X / 2.f - NewSpaceWidth / 2.f, InTop };
			break;
		case EBlankPosition::Right:
			Point_BlankLeft = { OverallSize.X - (Thickness / 2.f) - BlankOffset - NewSpaceWidth, InTop };
			break;
		}

		Points.Add(Point_BlankLeft);

		if (bShowHeader)
		{
			const auto HeaderSlot = Cast<UOverlaySlot>(Header->Slot);
			check(HeaderSlot);
			if (HeaderSlot)
			{
				const auto& HeaderSize = Header->GetCachedGeometry().GetLocalSize();
				float HeaderY = HeaderY = -HeaderSize.Y / 2.f;

				if (bDoubleBorder)
				{
					HeaderY += Thickness + SpaceWidth / 2.f;
				}

				FMargin NewPadding(Point_BlankLeft.X + 20.f, HeaderY, 0.f, 0.f);
				HeaderSlot->SetPadding(NewPadding);
			}
		}
	}

	Points.Add(Point_LeftTop);
	Points.Add(Point_LeftBottom);
	Points.Add(Point_RightBottom);
	Points.Add(Point_RightTop);

	if (!bEnableBlank)
	{
		Points.Add(Point_LeftTop);
	}
	else
	{
		FVector2D Point_BlankRight;

		switch (BlankPosition)
		{
		case EBlankPosition::Left:
			Point_BlankRight = { Thickness + BlankOffset + NewSpaceWidth, InTop };
			break;
		case EBlankPosition::Center:
			Point_BlankRight = { OverallSize.X / 2.f + NewSpaceWidth / 2.f, InTop };
			break;
		case EBlankPosition::Right:
			Point_BlankRight = { OverallSize.X - (Thickness / 2.f) - BlankOffset, InTop };
			break;
		}

		Points.Add(Point_BlankRight);
	}

	FSlateDrawElement::MakeLines(
		InContext.OutDrawElements,
		InContext.MaxLayer,
		InContext.AllottedGeometry.ToPaintGeometry(),
		Points,
		ESlateDrawEffect::None,
		Color,
		true,
		Thickness);
}

void UDosBorder::SetHeaderText(FText InText)
{
	HeaderText = InText;

	if (Header)
	{
		Header->SetText(HeaderText);
	}
}

void UDosBorder::SetColor(const FLinearColor& InColor)
{
	Color = InColor;

	if (Header)
	{
		Header->SetColorAndOpacity(Color);
	}
}
