// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/PHUserWidget.h"


void UPHUserWidget::ShowWidget()
{
	AddToViewport();

	if (GetOwningPlayer())
	{
		SetFocus();
	}
}

void UPHUserWidget::HideWidget()
{
	RemoveFromViewport();

	if (GetOwningPlayer())
	{
		GetOwningPlayer()->bShowMouseCursor = false;
		GetOwningPlayer()->SetCinematicMode(false, false, true, !bAllowControll, !bAllowControll);

		FInputModeGameOnly InputModeData;
		GetOwningPlayer()->SetInputMode(InputModeData);
	}
}

void UPHUserWidget::SetFocus()
{
	if (GetOwningPlayer())
	{
		GetOwningPlayer()->bShowMouseCursor = bShowMouseCursor;
		GetOwningPlayer()->SetCinematicMode(true, false, true, !bAllowControll, !bAllowControll);

		if (bRespondUIOnly)
		{
			FInputModeUIOnly InputModeData;
			InputModeData.SetWidgetToFocus(TakeWidget());
			InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			GetOwningPlayer()->SetInputMode(InputModeData);
		}
		else
		{
			FInputModeGameAndUI InputModeData;
			InputModeData.SetWidgetToFocus(TakeWidget());
			InputModeData.SetHideCursorDuringCapture(true);
			InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			GetOwningPlayer()->SetInputMode(InputModeData);
		}
	}
}
