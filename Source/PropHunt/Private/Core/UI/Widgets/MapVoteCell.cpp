// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Widgets/MapVoteCell.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"


bool UMapVoteCell::Initialize()
{
	const bool bSuccess = Super::Initialize();
	if (bSuccess)
	{
		SelectButton->OnClicked.AddDynamic(this, &UMapVoteCell::OnMapClicked);
	}

	return bSuccess;
}

void UMapVoteCell::SetMapName(FString InMapName)
{
	MapName->SetText(FText::FromString(InMapName));
}

void UMapVoteCell::SetVoteCount(int32 InVoteCount)
{
	VoteCount->SetText(FText::AsNumber(InVoteCount));
}

void UMapVoteCell::SetActive(bool bEnable)
{
	SelectButton->SetIsEnabled(bEnable);
}

void UMapVoteCell::OnMapClicked()
{
	MapSelectedEvent.Broadcast();
}
