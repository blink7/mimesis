// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Widgets/Dialog.h"

#include "Core/UI/ButtonWrapper.h"
#include "Core/PHGameViewportClient.h"

#include "Slate/SceneViewport.h"


void UDialog::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	ConfirmButton->SetText(ConfirmButtonText);
	if (!ConfirmButton->OnClicked.IsBound())
	{
		ConfirmButton->OnClicked.AddUObject(this, &UDialog::OnClickedConfirm);
	}

	if (CancelButtonText.IsEmpty())
	{
		CancelButton->SetVisibility(ESlateVisibility::Collapsed);
	}
	else
	{
		CancelButton->SetText(CancelButtonText);
		if (!CancelButton->OnClicked.IsBound())
		{
			CancelButton->OnClicked.AddUObject(this, &UDialog::OnClickedCancel);
		}
	}
}

void UDialog::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	// Keep widget alive between levels
}

void UDialog::NativeConstruct()
{
	Super::NativeConstruct();

	PlayAnimationForward(ShowAnimation);

	SetUserFocus(GetOwningPlayer());
}

void UDialog::NativeOnFocusLost(const FFocusEvent& InFocusEvent)
{
	Super::NativeOnFocusLost(InFocusEvent);

	const auto World = GetWorld();
	if (World && World->GetGameViewport()->GetGameViewport()->HasFocus())
	{
		//Don't allow to lose focus when click outside the menu (in case of MainMenu)
		SetUserFocus(GetOwningPlayer());
	}
}

FReply UDialog::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	FReply Result = FReply::Unhandled();
	const FKey Key = InKeyEvent.GetKey();

	if (Key == EKeys::Escape || Key == EKeys::Gamepad_FaceButton_Right || Key == EKeys::Virtual_Back)
	{
		OnClickedCancel();
		Result = FReply::Handled();
	}
	else if (Key == EKeys::Enter || Key == EKeys::Gamepad_FaceButton_Bottom || Key == EKeys::Virtual_Accept)
	{
		OnClickedConfirm();
		Result = FReply::Handled();
	}

	return Result;
}

void UDialog::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	if (Animation == ShowAnimation && !IsAnimationPlayingForward(Animation))
	{
		const auto GameViewportClient = Cast<UPHGameViewportClient>(GetWorld()->GetGameViewport());
		if (GameViewportClient)
		{
			GameViewportClient->HideDialog();
		}

		if (bConfirming)
		{
			OnConfirmClicked.Broadcast();
		}
		else if (bCanceling)
		{
			OnCancelClicked.Broadcast();
		}
	}
}

void UDialog::OnClickedConfirm()
{
	bConfirming = true;

	PlayAnimationReverse(ShowAnimation);
}

void UDialog::OnClickedCancel()
{
	bCanceling = true;

	PlayAnimationReverse(ShowAnimation);
}

void UDialog::EnableConfirmButon(bool bEnable)
{
	ConfirmButton->SetIsActive(bEnable);
}

void UDialog::ConstructMessageDialog(const FText& InHeader, const FText& InMessage, const FText& InConfirmButtonText, const FText& InCancelButtonText /*= FText::GetEmpty()*/)
{
	Type = EDialogType::Message;
	Header = InHeader;
	Message = InMessage;
	ConfirmButtonText = InConfirmButtonText;
	CancelButtonText = InCancelButtonText;

	bRespondUIOnly = true;
	bShowMouseCursor = true;
}

void UDialog::ConstructOpenFileDialog(const FText& InHeader, const FText& InConfirmButtonText, const FText& InCancelButtonText /*= FText::GetEmpty()*/)
{
	Type = EDialogType::OpenFile;
	Header = InHeader;
	ConfirmButtonText = InConfirmButtonText;
	CancelButtonText = InCancelButtonText;

	bRespondUIOnly = true;
	bShowMouseCursor = true;
}
