// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Widgets/PlayerIndicatorWidget.h"

#include "Core/Online/SteamUtils.h"
#include "Core/Online/PHPlayerState.h"

#include "Components/Image.h"
#include "Components/Border.h"
#include "Components/TextBlock.h"


void UPlayerIndicatorWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	const float RenderTimeThreshold = 0.3f;
	if (GetWorld()->TimeSince(LastRenderTime) > RenderTimeThreshold)
	{
		const auto MyPawn = GetOwningPlayerPawn();
		if (OwnerPawn && MyPawn)
		{
			const FVector& OwnerLocation = OwnerPawn->GetActorLocation();
			const FVector& MyLocation = MyPawn->GetActorLocation();
			float Distance = (OwnerLocation - MyLocation).Size2D() / 100.f;

			float NewOpacity = Distance < FullOpacityDistanceThreshold 
				? 1.f 
				: FMath::Max(FullOpacityDistanceThreshold / Distance, 0.2f);
			SetRenderOpacity(NewOpacity);
		}

		LastRenderTime = GetWorld()->GetTimeSeconds();
	}
}

void UPlayerIndicatorWidget::SetUp(APHPlayerState* ForPlayerState)
{
	LoadAvatar(ForPlayerState->GetUniqueId().GetUniqueNetId()->ToString());
	PlayerName->SetText(FText::FromString(ForPlayerState->GetShortPlayerName()));
	
	FLinearColor Color = ForPlayerState->GetTeamNumber() ? FColor::FromHex("#d4454e") : FColor::FromHex("#4576d4");

	auto Font = PlayerName->Font;
	Font.OutlineSettings.OutlineColor = Color;
	PlayerName->SetFont(Font);

	AvatarBorder->SetBrushColor(Color);

	OwnerPawn = ForPlayerState->GetPawn();
}

void UPlayerIndicatorWidget::LoadAvatar(const FString& OnlinePlayerId)
{
	const int SteamAvatar = FSteamUtils::GetSteamAvatar(OnlinePlayerId, EAvatarSize::Small);
	if (SteamAvatar > 0)
	{
		UTexture2D* Avatar = FSteamUtils::GetSteamImageAsTexture(SteamAvatar);
		PlayerAvatar->SetBrushFromTexture(Avatar);
	}
}
