// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/KillLogEntry.h"

#include "Core/PHTypes.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"

#include "TimerManager.h"


void UKillLogEntry::SetMessage(const FKillLogItemData& Message)
{
	KillerName->SetText(FText::FromString(Message.KillerDesc));
	VictimName->SetText(FText::FromString(Message.VictimDesc));
	bKillerIsHunter = Message.KillerTeamNum == ETeamType::Hunter;

	if (Message.KillIcon.IsValid())
	{
		KillIcon->SetBrushFromTexture(Message.KillIcon.Get());
	}
}
