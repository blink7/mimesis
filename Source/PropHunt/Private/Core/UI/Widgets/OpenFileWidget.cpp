// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/OpenFileWidget.h"

#include "Core/UI/Menu/SettingsItem.h"
#include "Core/UI/Menu/SettingsItemCell_ComboBox.h"
#include "Core/UI/Widgets/Dialog.h"
#include "Core/UI/Widgets/OpenFileEntry.h"

#include "Components/ListView.h"
#include "GenericPlatform/GenericPlatformFile.h"

static IPlatformFile& FileManager = FPlatformFileManager::Get().GetPlatformFile();

UOpenFileWidget::UOpenFileWidget(const FObjectInitializer& ObjectInitializer) : 
	Super(ObjectInitializer), 
	ExtensionFilter({ "png", "jpg", "jpeg", "bmp" })
{
}

void UOpenFileWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (!ItemList->OnItemClicked().IsBound())
	{
		ItemList->OnItemClicked().AddUObject(this, &UOpenFileWidget::OnItemClicked);
	}
	if (!ItemList->OnItemDoubleClicked().IsBound())
	{
		ItemList->OnItemDoubleClicked().AddUObject(this, &UOpenFileWidget::OnItemDoubleClicked);
	}
	if (!ItemList->OnItemIsHoveredChanged().IsBound())
	{
		ItemList->OnItemIsHoveredChanged().AddUObject(this, &UOpenFileWidget::OnItemHovered);
	}

	if (!PathSelector->OnValueChangedEvent.IsBound())
	{
		PathSelector->OnValueChangedEvent.AddUObject(this, &UOpenFileWidget::OnDiskSelectorChanged);
	}

	const FString StartDir = FPaths::GetPath(FPaths::ConvertRelativePathToFull(FPaths::ProjectDir()));
	RebuildList(StartDir);
}

void UOpenFileWidget::SetupDiskSelector(const FString& CurrentDir)
{
	PathSelector->ClearOptions();
	const FString Letters = TEXT("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	TArray<FString> Drives;
	for (int32 i = 0; i < Letters.Len(); i++)
	{
		FString Drive(1, &Letters.GetCharArray()[i]);
		Drive.Append(TEXT(":/"));

		const FFileStatData Data = FileManager.GetStatData(*Drive);
		if (Data.bIsDirectory)
		{
			Drives.Add(Drive);

			if (FPaths::IsUnderDirectory(CurrentDir, Drive))
			{
				FString SelectedPath = FPaths::IsDrive(CurrentDir) ? Drive : CurrentDir;
				PathSelector->AddOption(SelectedPath);
				PathSelector->SetSelectedOption(SelectedPath);
			}
			else
			{
				PathSelector->AddOption(Drive);
			}
		}
	}

	if (Drives.Num() == 0)
	{
		PathSelector->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void UOpenFileWidget::RebuildList(const FString& RelativeToDirectory)
{
	SetupDiskSelector(RelativeToDirectory);
	TArray<UOpenFileItemData*> Items;
	auto DirVisitor = [this, &Items](const TCHAR* FilenameOrDirectory, bool bIsDirectory) {
		if (!bIsDirectory)
		{
			const FString FileExtension = FPaths::GetExtension(FilenameOrDirectory);
			if (!ExtensionFilter.Contains(FileExtension))
			{
				return true;
			}
		}

		auto NewItem = NewObject<UOpenFileItemData>();
		NewItem->DirectoryPath = FilenameOrDirectory;
		NewItem->DisplayName = FPaths::GetCleanFilename(FilenameOrDirectory);
		if (bIsDirectory)
		{
			NewItem->Type = EOpenFileItemType::Directory;
			NewItem->DisplayName = "/" + NewItem->DisplayName;
		}
		else
		{
			NewItem->Type = EOpenFileItemType::File;
		}
		Items.Add(NewItem);

		return true;
	};

	if (FileManager.IterateDirectory(*RelativeToDirectory, DirVisitor))
	{
		Algo::Sort(Items, [](UOpenFileItemData* A, UOpenFileItemData* B) {
			if (A->Type == EOpenFileItemType::Directory && B->Type == EOpenFileItemType::File)
			{
				return true;
			}
			else if (A->Type == EOpenFileItemType::Directory && B->Type == EOpenFileItemType::Directory)
			{
				return A->DisplayName < B->DisplayName;
			}
			return false;
		});

		if (!FPaths::IsDrive(RelativeToDirectory))
		{
			auto NewItem = NewObject<UOpenFileItemData>();
			NewItem->DirectoryPath = FPaths::GetPath(RelativeToDirectory);
			NewItem->DisplayName = "/..";
			NewItem->Type = EOpenFileItemType::Directory;
			Items.Insert(NewItem, 0);
		}
		ItemList->SetListItems(Items);

		if (DialogOwner)
		{
			DialogOwner->EnableConfirmButon(false);
		}
	}
}

void UOpenFileWidget::OnItemClicked(UObject* MyListView)
{
	const auto Item = Cast<UOpenFileItemData>(ItemList->GetSelectedItem());
	if (Item && DialogOwner)
	{
		DialogOwner->EnableConfirmButon(Item->Type == EOpenFileItemType::File);
	}
}

void UOpenFileWidget::OnItemDoubleClicked(UObject* MyListView)
{
	if (const auto Item = Cast<UOpenFileItemData>(MyListView))
	{
		if (Item->Type == EOpenFileItemType::Directory)
		{
			RebuildList(Item->DirectoryPath);
		}
		else if (DialogOwner)
		{
			DialogOwner->OnClickedConfirm();
		}
	}
}

void UOpenFileWidget::OnItemHovered(UObject* InHoveredItem, bool bHovered)
{
	if (const auto Item = Cast<UOpenFileItemData>(InHoveredItem))
	{
		Item->OnOpenFileItemHover.ExecuteIfBound(bHovered);
	}
}

void UOpenFileWidget::OnDiskSelectorChanged()
{
	if (PathSelector)
	{
		const FString SelectedDisk = PathSelector->GetStringValue();
		RebuildList(FPaths::GetPath(SelectedDisk));
	}
}

FString UOpenFileWidget::GetCurrentPath()
{
	if (const auto Item = Cast<UOpenFileItemData>(ItemList->GetSelectedItem()))
	{
		const FString CurrentPath = Item->DirectoryPath;
		if (Item->Type == EOpenFileItemType::Directory)
		{
			RebuildList(CurrentPath);
		}
		else
		{
			return CurrentPath;
		}
	}
	return FString();
}
