// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/HUDWidget.h"

#include "Core/PHUtilities.h"
#include "Core/UI/Widgets/Minimap.h"
#include "Core/UI/Widgets/ChatWidget.h"
#include "Core/UI/Widgets/LiveStatus.h"
#include "Core/UI/Widgets/KillLogEntry.h"
#include "Core/UI/Widgets/ScreenIndicator.h"
#include "Core/UI/Widgets/InfoMessageEntry.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/PanelWidget.h"

void UHUDWidget::SetTimer(int32 Time)
{
	if (Time >= 0)
	{
		MatchTimer->SetText(FText::FromString(PropHunt::Utilities::TimeToString(Time)));
	}
}

void UHUDWidget::AddMessage(const FText& Message, float LifeTime, bool bPersistent)
{
	if (bPersistent && PersistentMessageEntry)
	{
		PersistentMessageEntry->SetMessage(Message);
		return;
	}

	const auto MessageEntry = CreateWidget<UInfoMessageEntry>(this, InfoMessageEntryClass);
	if (MessageEntry)
	{
		MessageEntry->Setup(Message, LifeTime);
		InfoMessageList->AddChild(MessageEntry);

		if (bPersistent)
		{
			PersistentMessageEntry = MessageEntry;
		}
	}
}

void UHUDWidget::AddDeathMessage(const FKillLogItemData& Message)
{
	constexpr int32 MaxDeathMessages = 5;

	if (KillLog->GetChildrenCount() >= MaxDeathMessages)
	{
		KillLog->RemoveChildAt(0);
	}

	const auto MessageRow = CreateWidget<UKillLogEntry>(this, KillLogEntryClass);
	if (MessageRow)
	{
		MessageRow->SetMessage(Message);
		KillLog->AddChild(MessageRow);
	}
}

void UHUDWidget::OpenChat(bool bTeammate) const
{
	ChatWidget->OpenChat(bTeammate);
}

void UHUDWidget::AddChatLine(const FString& SenderName, const FString& Message, int32 TeamNumber, bool bTeammate, bool bSystem)
{
	ChatWidget->AddChatLine(SenderName, Message, TeamNumber, bTeammate);
}

void UHUDWidget::AddPlayerToMinimap(APawn* PlayerPawn)
{
	Minimap->AddTargetPawn(PlayerPawn);
}

void UHUDWidget::ClearPlayersFromMinimap()
{
	Minimap->ClearPawns();
}

void UHUDWidget::ZoomMinimap() const
{
	Minimap->ZoomMinimap();
}

void UHUDWidget::AddTargetOnMinimap(AActor* TargetActor, UTexture2D* Icon, FLinearColor Color, bool bStatic)
{
	Minimap->AddTargetActor(TargetActor, Icon, Color, bStatic);
}

void UHUDWidget::AddTargetOnScreen(AActor* TargetActor, UTexture2D* Icon, FLinearColor Color, float Size)
{
	ScreenIndicator->AddTarget(TargetActor, Color, Size, Icon);
}

void UHUDWidget::RemovePointOfInterest(AActor* TargetActor)
{
	ScreenIndicator->RemoveTarget(TargetActor);
	Minimap->RemoveActor(TargetActor);
}

void UHUDWidget::UpdateMatchStats(uint8 AlivePlayersTeamA, uint8 TotalPlayersTeamA, uint8 AlivePlayersTeamB, uint8 TotalPlayersTeamB)
{
	TeamA_LiveStatus->UpdatePlayers(AlivePlayersTeamA, TotalPlayersTeamA);
	TeamB_LiveStatus->UpdatePlayers(AlivePlayersTeamB, TotalPlayersTeamB);
}

void UHUDWidget::SetupMatchStats(const FText& TitleTeamA, const FText& TitleTeamB, int32 PrimaryTeamNumber)
{
	TeamA_Label->SetText(TitleTeamA);
	TeamB_Label->SetText(TitleTeamB);

	TeamA_LiveStatus->Setup(PrimaryTeamNumber);
	TeamB_LiveStatus->Setup(!PrimaryTeamNumber);
}

void UHUDWidget::NotifyEnemyHit()
{
	PlayAnimationForward(HitAnimation);
}
