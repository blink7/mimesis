// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/Scoreboard.h"

#include "Components/PanelWidget.h"
#include "Core/PHTypes.h"
#include "Core/Online/PHGameState.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/UI/Widgets/Scoreboard_TeamEntry.h"
#include "Core/UI/Widgets/Scoreboard_PlayerDetails.h"

#include "Components/TextBlock.h"


UScoreboard::UScoreboard(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bShowMouseCursor = true;
	bAllowControll = true;
}

void UScoreboard::ShowWidget()
{
	Super::ShowWidget();

	BindEvents();
}

void UScoreboard::HideWidget()
{
	Super::HideWidget();

	if (auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		GameState->OnPlayerStateChanged.RemoveDynamic(this, &UScoreboard::OnPlayerStateChanged);
	}
}

void UScoreboard::BindEvents()
{
	if (auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		GameState->OnPlayerStateChanged.AddDynamic(this, &UScoreboard::OnPlayerStateChanged);
		UpdatePlayerStateMaps();
	}
	else
	{
		// Keep retrying until Game State is ready
		FTimerHandle TimerHandle_Unused;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Unused, this, &UScoreboard::BindEvents, 0.2f);
	}
}

void UScoreboard::OnPlayerStateChanged(APlayerState* Owner)
{
	UpdatePlayerStateMaps();
}

void UScoreboard::SetupDetails(APHPlayerState* PlayerState, UPanelWidget* ShowDetailsContainer, UPanelWidget* HideDetailsContainer)
{
	auto ShowDetailsWidget = Cast<UScoreboard_PlayerDetails>(ShowDetailsContainer->GetChildAt(0));
	if (!ShowDetailsWidget)
	{
		ShowDetailsWidget = CreateWidget<UScoreboard_PlayerDetails>(this, ScoreboardPlayerDetailsClass);
		ShowDetailsContainer->AddChild(ShowDetailsWidget);
	}
	ShowDetailsWidget->OnSetup(PlayerState);

	UWidget* HideDetailsWidget = HideDetailsContainer->GetChildAt(0);
	if (HideDetailsWidget)
	{
		HideDetailsContainer->RemoveChild(HideDetailsWidget);
	}
}

void UScoreboard::DisplayDetails(APHPlayerState* PlayerState)
{
	if (PlayerState)
	{
		if (PlayerState->GetTeamNumber() == MainTeam)
		{
			SetupDetails(PlayerState, DetailsContainerTop, DetailsContainerBottom);
		}
		else
		{
			SetupDetails(PlayerState, DetailsContainerBottom, DetailsContainerTop);
		}
	}
	else
	{
		auto DetailsTop = Cast<UScoreboard_PlayerDetails>(DetailsContainerTop->GetChildAt(0));
		if (DetailsTop)
		{
			DetailsTop->OnClear();
		}

		auto DetailsBottom = Cast<UScoreboard_PlayerDetails>(DetailsContainerBottom->GetChildAt(0));
		if (DetailsBottom)
		{
			DetailsBottom->OnClear();
		}
	}
}

void UScoreboard::UpdateScoreboardGrid()
{
	auto GameState = GetWorld()->GetGameState<APHGameState>();
	if (GetOwningPlayer() && GameState)
	{
		ServerName->SetText(FText::FromString(GameState->ServerName));
		MapName->SetText(FText::FromString(GameState->MapName));

		const auto MyPlayerState = Cast<APHPlayerState>(GetOwningPlayer()->PlayerState);
		const int32 MyTeamNumber = MyPlayerState ? MyPlayerState->GetTeamNumber() : ETeamType::Hunter;

		if (MyTeamNumber == ETeamType::Prop)
		{
			FillTeamWidget(TeamCellA, ETeamType::Prop);
			FillTeamWidget(TeamCellB, ETeamType::Hunter);
		}
		else
		{
			FillTeamWidget(TeamCellA, ETeamType::Hunter);
			FillTeamWidget(TeamCellB, ETeamType::Prop);
		}

		MainTeam = MyTeamNumber;
	}
}

void UScoreboard::FillTeamWidget(UScoreboard_TeamEntry* TeamCell, int32 TeamNumber)
{
	if (auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		TeamCell->SetupWidget(this, TeamNumber == ETeamType::Hunter, GameState->GetTeamScore(TeamNumber));

		const int32 MaxPlayers = GameState->MaxPlayers;
		for (int32 i = 0; i < MaxPlayers / 2; i++)
		{
			auto PlayerState = PlayerStateMaps[TeamNumber].Find(i);
			if (PlayerState)
			{
				TeamCell->AddItemAt(i, PlayerState->Get());
			}
			else
			{
				TeamCell->RemoveItemAt(i);
			}
		}
	}
}

void UScoreboard::UpdatePlayerStateMaps()
{
	if (auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		bool bRequiresWidgetUpdate = false;
		const int32 NumTeams = FMath::Max(GameState->NumTeams, 1);

		LastTeamPlayerCount.Reset();
		LastTeamPlayerCount.AddZeroed(NumTeams);
		for (int32 TeamNumber = 0; TeamNumber < PlayerStateMaps.Num(); TeamNumber++)
		{
			LastTeamPlayerCount[TeamNumber] = PlayerStateMaps[TeamNumber].Num();
		}

		PlayerStateMaps.Reset();
		PlayerStateMaps.AddZeroed(NumTeams);

		for (int32 TeamNumber = 0; TeamNumber < NumTeams; TeamNumber++)
		{
			GameState->GetRankedMap(TeamNumber, PlayerStateMaps[TeamNumber]);

			if (PlayerStateMaps[TeamNumber].Num() != LastTeamPlayerCount[TeamNumber])
			{
				bRequiresWidgetUpdate = true;
			}
		}

		if (bRequiresWidgetUpdate)
		{
			UpdateScoreboardGrid();
		}
	}
}
