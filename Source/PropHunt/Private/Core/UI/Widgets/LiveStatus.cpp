// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Widgets/LiveStatus.h"

#include "Core/PHTypes.h"
#include "Core/UI/StyleTypes.h"

#include "Engine/DataTable.h"
#include "Components/PanelWidget.h"


ULiveStatus::ULiveStatus(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	StyleTableRowName("Default")
{
}

void ULiveStatus::Setup(int32 TeamNumber)
{
	const auto ColorScheme = StyleTable->FindRow<FPHHUDStyle>(StyleTableRowName, "Setup");
	if (ColorScheme)
	{
		DeadColor = ColorScheme->PrimaryColor;

		if (ETeamType::Prop == TeamNumber)
		{
			LiveColor = ColorScheme->SecondaryColor;
		}
		else if (ETeamType::Hunter == TeamNumber)
		{
			LiveColor = ColorScheme->TertiaryColor;
		}
	}

	List->ClearChildren();
}

void ULiveStatus::UpdatePlayers(uint8 LivePlayers, uint8 TotalPlayers)
{
	while (List->GetChildrenCount() > TotalPlayers)
	{
		List->RemoveChildAt(List->GetChildrenCount() - 1);
	}

	while (List->GetChildrenCount() < TotalPlayers)
	{
		const auto NewChild = CreateWidget<UUserWidget>(this, EntryTemplate);
		if (NewChild)
		{
			NewChild->SetColorAndOpacity(DeadColor);
			List->AddChild(NewChild);
		}
	}

	for (int32 i = 0; i < List->GetChildrenCount(); ++i)
	{
		const auto Child = Cast<UUserWidget>(List->GetChildAt(i));
		if (Child)
		{
			if (i < LivePlayers)
			{
				Child->SetColorAndOpacity(LiveColor);
			}
			else
			{
				Child->SetColorAndOpacity(DeadColor);
			}
		}
	}
}

bool ULiveStatus::IsEmpty() const
{
	return !List->HasAnyChildren();
}
