// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/ScreenIndicator.h"

#include "Core/UI/Widgets/SScreenIndicatorEntry.h"

#include "Components/NativeWidgetHost.h"


UScreenIndicator::UScreenIndicator(const FObjectInitializer& ObjectInitializer) : 
	Super(ObjectInitializer), 
	Color(FLinearColor::White),
	Size(64.f)
{
}

void UScreenIndicator::NativePreConstruct()
{
	Super::NativePreConstruct();

	Container = SNew(SOverlay);
	ContainerHost->SetContent(Container.ToSharedRef());
}

void UScreenIndicator::AddTarget(AActor* TargetActor)
{
	AddTarget(TargetActor, Color, Size);
}

void UScreenIndicator::AddTarget(AActor* TargetActor, FLinearColor InColor, float InSize, UTexture2D* InOnScreenIcon /*= nullptr*/, UTexture2D* InOffScreenIcon /*= nullptr*/)
{
	if (ActorMap.Contains(TargetActor))
	{
		return;
	}

	APlayerController* PlayerController = GetPlayerContext().GetPlayerController();
	if (PlayerController && TargetActor && PlayerController != TargetActor)
	{
		const int32 Index = Container->AddSlot()
			[
				SNew(SScreenIndicatorEntry)
				.PlayerController(PlayerController)
			.TargetActor(TargetActor)
			.OnScreenTexture(InOnScreenIcon ? InOnScreenIcon : OnScreenIcon)
			.OffScreenTexture(InOffScreenIcon ? InOffScreenIcon : OffScreenIcon)
			.Color(InColor)
			.Size(InSize)
			].ZOrder;

		ActorMap.Add(TargetActor, Index);
	}
}

bool UScreenIndicator::RemoveTarget(AActor* TargetActor)
{
	int32* Index = ActorMap.Find(TargetActor);
	if (Index)
	{
		Container->RemoveSlot(*Index);
		ActorMap.Remove(TargetActor);
		return true;
	}

	return false;
}
