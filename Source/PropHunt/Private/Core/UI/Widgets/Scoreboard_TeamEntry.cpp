// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/Scoreboard_TeamEntry.h"

#include "Core/Online/PHPlayerState.h"

#include "Components/ListView.h"
#include "Core/UI/Widgets/Scoreboard.h"


void UScoreboard_TeamEntry::NativeConstruct()
{
	Super::NativeConstruct();

	if (!PlayersList->OnItemIsHoveredChanged().IsBound())
	{
		PlayersList->OnItemIsHoveredChanged().AddUObject(this, &UScoreboard_TeamEntry::OnItemHovered);
	}
}

void UScoreboard_TeamEntry::AddItemAt(int32 Index, APHPlayerState* PlayerState)
{
	auto ItemData = Cast<UScoreboardItemData>(PlayersList->GetItemAt(Index));
	if (!ItemData)
	{
		ItemData = NewObject<UScoreboardItemData>();
		PlayersList->AddItem(ItemData);
	}
	
	ItemData->PlayerState = PlayerState;
}

void UScoreboard_TeamEntry::RemoveItemAt(int32 Index)
{
	PlayersList->RemoveItem(PlayersList->GetItemAt(Index));
}

void UScoreboard_TeamEntry::SetupWidget(UScoreboard* InOwner, bool bInForHunter, int32 InTeamScore)
{
	K2_SetupWidget(bInForHunter, InTeamScore);

	Owner = InOwner;
}

void UScoreboard_TeamEntry::OnItemHovered(UObject* InHoveredItem, bool bHovered)
{
	if (auto ScoreboardItem = Cast<UScoreboardItemData>(InHoveredItem))
	{
		ScoreboardItem->OnItemHoverEvent.ExecuteIfBound(bHovered);

		if (Owner)
		{
			Owner->DisplayDetails(bHovered ? ScoreboardItem->PlayerState : nullptr);
		}
	}
}
