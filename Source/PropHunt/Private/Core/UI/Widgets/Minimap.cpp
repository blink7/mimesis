// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/Minimap.h"

#include "Core/UI/Widgets/SMinimapEntry.h"

#include "Widgets/SOverlay.h"
#include "Components/NativeWidgetHost.h"


void UMinimap::NativePreConstruct()
{
	Super::NativePreConstruct();

	Container = SNew(SOverlay);
	POIHost->SetContent(Container.ToSharedRef());
}

void UMinimap::AddTargetActor(AActor* TargetActor, class UTexture2D* Texture, FLinearColor Color /*= FLinearColor::White*/, bool bStatic /*= false*/)
{
	if (ActorMap.Contains(TargetActor))
	{
		return;
	}

	APawn* OwnerPawn = GetPlayerContext().GetPawn();
	if (OwnerPawn && TargetActor && OwnerPawn != TargetActor)
	{
		const int32 Index = Container->AddSlot()
			[
				SNew(SMinimapEntry)
				.OwnerActor(OwnerPawn)
				.TargetActor(TargetActor)
				.Texture(Texture)
				.Color(Color)
				.Factor_UObject(this, &UMinimap::GetFactor)
				.Static(bStatic)
			].ZOrder;

		ActorMap.Add(TargetActor, Index);
	}
}

void UMinimap::AddTargetPawn(APawn* PlayerPawn)
{
	AddTargetActor(PlayerPawn, PlayerTexture, PlayerColor);
	PawnList.Add(PlayerPawn);
}

void UMinimap::RemoveActor(AActor* ActorToRemove)
{
	int32* Index = ActorMap.Find(ActorToRemove);
	if (Index)
	{
		Container->RemoveSlot(*Index);
		ActorMap.Remove(ActorToRemove);
	}
}

void UMinimap::ClearPawns()
{
	for (APawn* PawnToRemove : PawnList)
	{
		int32* Index = ActorMap.Find(PawnToRemove);
		if (Index)
		{
			Container->RemoveSlot(*Index);
			ActorMap.Remove(PawnToRemove);
		}
	}

	PawnList.Empty();
}

void UMinimap::Clear()
{
	PawnList.Empty();
	ActorMap.Empty();
	Container->ClearChildren();
}

void UMinimap::ZoomMinimap()
{
	if (CurrentZoomIndex == 0)
	{
		Zoom = DefaultZoom + ZoomDelta;
		CurrentZoomIndex = 1;
	}
	else if (CurrentZoomIndex == 1)
	{
		Zoom = DefaultZoom - ZoomDelta;
		CurrentZoomIndex = 2;
	}
	else if (CurrentZoomIndex == 2)
	{
		Zoom = DefaultZoom;
		CurrentZoomIndex = 0;
	}

	Zoom = FMath::Max(0.1f, Zoom);
	OnZoomChanged(Zoom);
}

void UMinimap::SetDefaultZoom(float Value)
{
	DefaultZoom = Value;
	Zoom = Value;
	OnZoomChanged(Value);
}

float UMinimap::GetFactor() const
{
	return Zoom * Dimensions;
}
