// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Widgets/ChatEntry.h"

#include "Components/RichTextBlock.h"


void UChatEntry::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	const auto Item = Cast<UChatItemData>(ListItemObject);
	if (Item)
	{
		Message->SetText(FText::FromString(Item->Message));
	}
}
