// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/OpenFileEntry.h"


void UOpenFileEntry::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	IUserObjectListEntry::NativeOnListItemObjectSet(ListItemObject);

	if (auto Item = Cast<UOpenFileItemData>(ListItemObject))
	{
		Item->OnOpenFileItemHover.BindUObject(this, &UOpenFileEntry::OnItemHovered);
	}
}
