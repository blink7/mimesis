// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/SMinimapEntry.h"

#include "Engine/Texture2D.h"
#include "Widgets/SOverlay.h"
#include "Widgets/Images/SImage.h"


void SMinimapEntry::Construct(const FArguments& InArgs)
{
	OwnerActor = InArgs._OwnerActor;
	TargetActor = InArgs._TargetActor;
	Factor = InArgs._Factor;
	bStatic = InArgs._Static;
	PointSize = InArgs._Size;
	bVisible = true;
	SetTexture(InArgs._Texture);

	ChildSlot
	[
		SNew(SBox)
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		[
			SNew(SImage)
			.ColorAndOpacity(InArgs._Color)
			.RenderTransformPivot(FVector2D(0.5f))
			.Image(this, &SMinimapEntry::GetImageBrush)
			.Visibility(this, &SMinimapEntry::GetPointVisibility)
			.RenderTransform(this, &SMinimapEntry::GetPointTransform)
		]
	];
}

void SMinimapEntry::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	if (OwnerActor.IsValid() && TargetActor.IsValid())
	{
		const FVector2D& WidgetSize = AllottedGeometry.GetLocalSize();
		const FVector2D WidgetHalfSize = (WidgetSize - PointSize) / 2.f;

		const FVector TargetLocation = TargetActor->GetActorLocation();
		const FVector OwnerLocation = OwnerActor->GetActorLocation();

		float LocationX = (TargetLocation.X - OwnerLocation.X) / (Factor.Get() / WidgetSize.X);
		float LocationY = (TargetLocation.Y - OwnerLocation.Y) / (Factor.Get() / WidgetSize.Y);

		if (bStatic)
		{
			LocationX = FMath::Clamp(LocationX, -WidgetHalfSize.X, WidgetHalfSize.X);
			LocationY = FMath::Clamp(LocationY, -WidgetHalfSize.Y, WidgetHalfSize.Y);
		}
		else
		{
			if (FMath::Abs(LocationX) > WidgetHalfSize.X || FMath::Abs(LocationY) > WidgetHalfSize.Y)
			{
				bVisible = false;
				return;
			}
			else
			{
				bVisible = true;
			}
		}

		Transform = FSlateRenderTransform(FQuat2D(FMath::DegreesToRadians(TargetActor->GetActorRotation().Yaw + 90.f)), FVector2D(LocationX, LocationY));
	}
	else
	{
		bVisible = false;
	}
}

void SMinimapEntry::SetTexture(UTexture2D* Texture)
{
	if (Texture)
	{
		ImageBrush.SetResourceObject(Texture);
		ImageBrush.ImageSize.X = PointSize;
		ImageBrush.ImageSize.Y = PointSize;

		Texture->bForceMiplevelsToBeResident = true;
		Texture->bIgnoreStreamingMipBias = true;
	}
}

const FSlateBrush* SMinimapEntry::GetImageBrush() const
{
	return &ImageBrush;
}

EVisibility SMinimapEntry::GetPointVisibility() const
{
	return bVisible ? EVisibility::HitTestInvisible : EVisibility::Collapsed;
}

TOptional<FSlateRenderTransform> SMinimapEntry::GetPointTransform() const
{
	return Transform;
}
