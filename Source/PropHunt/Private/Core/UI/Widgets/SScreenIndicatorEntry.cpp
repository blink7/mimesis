// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/SScreenIndicatorEntry.h"

#include "Engine/Texture2D.h"
#include "Widgets/Images/SImage.h"
#include "Blueprint/SlateBlueprintLibrary.h"


void SScreenIndicatorEntry::Construct(const FArguments& InArgs)
{
	PlayerController = InArgs._PlayerController;
	TargetActor = InArgs._TargetActor;
	PointSize = InArgs._Size;
	bAnimate = InArgs._Animate;
	SetTexture(OnScreenImageBrush, InArgs._OnScreenTexture);
	SetTexture(OffScreenImageBrush, InArgs._OffScreenTexture);

	ChildSlot
	[
		SNew(SBorder)
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		.RenderTransformPivot(FVector2D(0.5f))
		.BorderImage(FStyleDefaults::GetNoBrush())
		.ContentScale(this, &SScreenIndicatorEntry::GetAnimScale)
		.ColorAndOpacity(this, &SScreenIndicatorEntry::GetAnimColor)
		.RenderTransform(this, &SScreenIndicatorEntry::GetPointTransform)
		[
			SNew(SImage)
			.ColorAndOpacity(InArgs._Color)
			.Image(this, &SScreenIndicatorEntry::GetImageBrush)
		]
	];

	if (bAnimate)
	{
		AnimCurveHandle = AnimCurve.AddCurve(0.f, 0.25f, ECurveEaseFunction::QuadOut);
		AnimCurve.Play(this->AsShared());
	}
}

void SScreenIndicatorEntry::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	if (PlayerController.IsValid() && TargetActor.IsValid())
	{
		FVector2D ScreenPosition;

		const FVector2D WidgetSize = AllottedGeometry.GetLocalSize();
		const FVector2D WidgetHalfSize = WidgetSize / 2.f;

		FVector CameraLoc;
		FRotator CameraRot;
		PlayerController->GetPlayerViewPoint(CameraLoc, CameraRot);

		const FVector TargetLocation = TargetActor->GetActorLocation();

		const FVector CameraToLoc = TargetLocation - CameraLoc;
		const FVector Forward = CameraRot.Vector();
		const FVector Offset = CameraToLoc.GetSafeNormal();
		const bool bLocationIsBehindCamera = FVector::DotProduct(Forward, Offset) < 0.f;

		if (bLocationIsBehindCamera)
		{
			const FVector NewInLocation = CameraLoc - CameraToLoc;
			PlayerController->ProjectWorldLocationToScreen(NewInLocation, ScreenPosition);
			USlateBlueprintLibrary::ScreenToWidgetLocal(PlayerController.Get(), AllottedGeometry, ScreenPosition, ScreenPosition);

			ScreenPosition.X = WidgetSize.X - ScreenPosition.X;
			ScreenPosition.Y = WidgetSize.Y - ScreenPosition.Y;
		}
		else
		{
			PlayerController->ProjectWorldLocationToScreen(TargetLocation, ScreenPosition);
			USlateBlueprintLibrary::ScreenToWidgetLocal(PlayerController.Get(), AllottedGeometry, ScreenPosition, ScreenPosition);
		}

		// Check to see if it's on screen. If it is, ProjectWorldLocationToScreen is all we need, return it.
		if (ScreenPosition.X >= 0.f && ScreenPosition.X <= WidgetSize.X && ScreenPosition.Y >= 0.f && ScreenPosition.Y <= WidgetSize.Y && !bLocationIsBehindCamera)
		{
			Transform = FSlateRenderTransform(ScreenPosition - WidgetHalfSize);
			bOnScreen = true;
			return;
		}

		ScreenPosition -= WidgetHalfSize;

		const float AngleRadians = FMath::Atan2(ScreenPosition.Y, ScreenPosition.X) - FMath::DegreesToRadians(90.f);
		const float Cos = FMath::Cos(AngleRadians);
		const float Sin = -FMath::Sin(AngleRadians);
		const float Cot = Cos / Sin;

		if (Cos > 0)
		{
			ScreenPosition = FVector2D(WidgetHalfSize.Y / Cot, WidgetHalfSize.Y);
		}
		else
		{
			ScreenPosition = FVector2D(-WidgetHalfSize.Y / Cot, -WidgetHalfSize.Y);
		}

		if (ScreenPosition.X > WidgetHalfSize.X)
		{
			ScreenPosition = FVector2D(WidgetHalfSize.X, WidgetHalfSize.X * Cot);
		}
		else if (ScreenPosition.X < -WidgetHalfSize.X)
		{
			ScreenPosition = FVector2D(-WidgetHalfSize.X, -WidgetHalfSize.X * Cot);
		}

		Transform = FSlateRenderTransform(FQuat2D(AngleRadians), ScreenPosition);
		bOnScreen = false;
	}
}

void SScreenIndicatorEntry::SetTexture(FSlateBrush& BrushToApply, UTexture2D* Texture)
{
	if (Texture)
	{
		BrushToApply.SetResourceObject(Texture);
		BrushToApply.ImageSize.X = PointSize;
		BrushToApply.ImageSize.Y = PointSize;

		Texture->bForceMiplevelsToBeResident = true;
		Texture->bIgnoreStreamingMipBias = true;
	}
}

const FSlateBrush* SScreenIndicatorEntry::GetImageBrush() const
{
	return bOnScreen ? &OnScreenImageBrush : &OffScreenImageBrush;
}

TOptional<FSlateRenderTransform> SScreenIndicatorEntry::GetPointTransform() const
{
	return Transform;
}

FVector2D SScreenIndicatorEntry::GetAnimScale() const
{
	if (bAnimate)
	{
		const float Value = AnimCurveHandle.GetLerp();
		return FMath::Lerp(20.f, 1.f, Value);
	}
	else
	{
		return 1.f;
	}
}

FLinearColor SScreenIndicatorEntry::GetAnimColor() const
{
	if (bAnimate)
	{
		const float Value = AnimCurveHandle.GetLerp();
		return FLinearColor(1.f, 1.f, 1.f, Value);
	}
	else
	{
		return FLinearColor::White;
	}
}
