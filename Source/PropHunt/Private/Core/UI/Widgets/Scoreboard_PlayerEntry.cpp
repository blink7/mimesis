// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/Widgets/Scoreboard_PlayerEntry.h"

#include "Core/Online/SteamUtils.h"
#include "Core/Online/PHPlayerState.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"


#define LOCTEXT_NAMESPACE "Scoreboard"

void UScoreboard_PlayerEntry::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	
	if (ItemData)
	{
		if (APHPlayerState* PlayerState = ItemData->PlayerState)
		{
			PlayerScore->SetText(FText::AsNumber(PlayerState->GetScore()));
			PlayerPing->SetText(FText::AsNumber(PlayerState->GetPing()));
		}
	}
}

void UScoreboard_PlayerEntry::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	ItemData = Cast<UScoreboardItemData>(ListItemObject);
	if (ItemData && !ItemData->OnItemHoverEvent.IsBoundToObject(this))
	{
		ItemData->OnItemHoverEvent.BindUObject(this, &UScoreboard_PlayerEntry::OnItemHoverChanged);

		if (APHPlayerState* PlayerState = ItemData->PlayerState)
		{
			LoadAvatar(PlayerState->GetUniqueId().GetUniqueNetId()->ToString());
			PlayerName->SetText(FText::FromString(PlayerState->GetShortPlayerName()));
		}
	}
}

void UScoreboard_PlayerEntry::LoadAvatar(const FString& OnlinePlayerId)
{
	const int SteamAvatar = FSteamUtils::GetSteamAvatar(OnlinePlayerId, EAvatarSize::Small);
	if (SteamAvatar > 0)
	{
		UTexture2D* Avatar = FSteamUtils::GetSteamImageAsTexture(SteamAvatar);
		PlayerAvatar->SetBrushFromTexture(Avatar);
	}
}

#undef LOCTEXT_NAMESPACE
