// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/Widgets/ChatWidget.h"

#include "Core/PHTypes.h"
#include "Core/PHPlayerController.h"
#include "Core/UI/Widgets/ChatEntry.h"

#include "Components/ListView.h"
#include "Components/RichTextBlock.h"
#include "Components/EditableTextBox.h"


#define LOCTEXT_NAMESPACE "Chat"

UChatWidget::UChatWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	StyleTableRowName("Default")
{
}

void UChatWidget::NativeConstruct()
{
	Super::NativeConstruct();

	const auto TextStyleRow = StyleTable->FindRow<FRichTextStyleRow>(StyleTableRowName, "Setup");
	if (TextStyleRow)
	{
		ChatEditBox->WidgetStyle.SetFont(TextStyleRow->TextStyle.Font);
	}

	ChatEditBox->OnTextCommitted.AddDynamic(this, &UChatWidget::OnChatTextCommitted);
}

void UChatWidget::OnChatTextCommitted(const FText& Text, ETextCommit::Type CommitMethod)
{
	if (CommitMethod == ETextCommit::OnEnter)
	{
		const auto PlayerController = GetOwningPlayer<APHPlayerController>();
		if (PlayerController)
		{
			if (!Text.IsEmpty())
			{
				if (bTeammate)
				{
					PlayerController->Say(Text.ToString(), EMessageType::Team);
				}
				else
				{
					// Broadcast chat to other players
					PlayerController->Say(Text.ToString());
				}

				// Clear the text
				ChatEditBox->SetText(FText());
			}

			ShowEditBox(false);
		}
	}
	else if (CommitMethod == ETextCommit::OnCleared || CommitMethod == ETextCommit::Default)
	{
		ShowEditBox(false);
	}
}

void UChatWidget::AddChatLine(const FString& SenderName, const FString& Message, int32 Team, bool bInTeammate, bool bSystem /*= false*/)
{
	FString ChatString;
	if (bInTeammate)
	{
		ChatString = FString::Printf(TEXT("<Teammate>%s:</> %s"), *SenderName, *Message);
	}
	else if (ETeamType::Hunter == Team)
	{
		ChatString = FString::Printf(TEXT("<Hunter>%s:</> %s"), *SenderName, *Message);
	}
	else if (ETeamType::Prop == Team)
	{
		ChatString = FString::Printf(TEXT("<Prop>%s:</> %s"), *SenderName, *Message);
	}
	else if (ETeamType::Spectator == Team)
	{
		ChatString = FString::Printf(TEXT("<Spectator>%s: %s</>"), *SenderName, *Message);
	}
	else
	{
		ChatString = FString::Printf(TEXT("%s: %s"), *SenderName, *Message);
	}

	if (ChatHistoryList->GetNumItems() == 0)
	{
		ChatHistoryOverlay->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}

	const auto NewChatEntry = NewObject<UChatItemData>();
	NewChatEntry->Message = ChatString;
	ChatHistoryList->AddItem(NewChatEntry);
	ChatHistoryList->ScrollToBottom();
}

void UChatWidget::OpenChat(bool bInTeammate)
{
	bTeammate = bInTeammate;

	if (bTeammate)
	{
		ChatStatusText->SetText(LOCTEXT("ChatTeam", "<Teammate>Team:</>"));
	}
	else
	{
		ChatStatusText->SetText(LOCTEXT("ChatAll", "All:"));
	}

	ShowEditBox(true);
}

void UChatWidget::ShowEditBox(bool bEnable)
{
	if (bEnable)
	{
		PlayAnimationForward(ExpandAnimation);

		ChatEditBox->SetUserFocus(GetOwningPlayer());
	}
	else
	{
		PlayAnimationReverse(ExpandAnimation);

		FInputModeGameOnly InputModeData;
		GetOwningPlayer()->SetInputMode(InputModeData);
	}
}

#undef LOCTEXT_NAMESPACE
