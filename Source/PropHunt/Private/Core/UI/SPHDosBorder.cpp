﻿// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/UI/SPHDosBorder.h"

#include "SlateOptMacros.h"


BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SPHDosBorder::Construct(const FArguments& InArgs)
{
	ContentScale = InArgs._ContentScale;
	ColorAndOpacity = InArgs._ColorAndOpacity;
	DesiredSizeScale = InArgs._DesiredSizeScale;

	ShowDisabledEffect = InArgs._ShowEffectWhenDisabled;

	BorderColor = InArgs._BorderColor;

	HeaderText = InArgs._HeaderText;
	TextStyle = *InArgs._TextStyle;
	Font = InArgs._Font;
	Thickness = InArgs._Thickness;
	bShowHeader = InArgs._bShowHeader;
	BlankOffset = InArgs._BlankOffset;
	SpaceWidth = InArgs._SpaceWidth;
	BlankPosition = InArgs._BlankPosition;
	bDoubleBorder = InArgs._bDoubleBorder;
	bAnimate = InArgs._bAnimate;
	AnimDuration = InArgs._AnimDuration;
	
	ChildSlot
	[
		SNew(SBorder)
		.BorderImage(FStyleDefaults::GetNoBrush())
		.Padding(0.f)
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.ColorAndOpacity(this, &SPHDosBorder::GetHeaderAndContentColor)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Top)
			.Expose(OverlayHeaderSlot)
			[
				SAssignNew(Header, STextBlock)
				.Font(this, &SPHDosBorder::GetFont)
				.Text(this, &SPHDosBorder::GetHeaderText)
				.ColorAndOpacity(this, &SPHDosBorder::GetBorderColor)
				.Visibility(this, &SPHDosBorder::GetHeaderVisibility)
			]
			+ SOverlay::Slot()
			.HAlign(InArgs._HAlign)
			.VAlign(InArgs._VAlign)
			.Padding(InArgs._Padding + Thickness)
			.Expose(OverlayContentSlot)
			[
				InArgs._Content.Widget
			]
		]
	];

	SetupAnimation();
}

int32 SPHDosBorder::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	const bool bEnabled = ShouldBeEnabled(bParentEnabled);

	const FVector2D& Size = AllottedGeometry.GetLocalSize();
	const float HalfThickness = Thickness / 2.f;

	float Top = HalfThickness;
	float Bottom = Size.Y - HalfThickness;
	float Left = HalfThickness;
	float Right = Size.X - HalfThickness;

	PaintBorder(AllottedGeometry, OutDrawElements, LayerId, InWidgetStyle, bEnabled, { Top, Bottom, Left, Right, Size });

	if (bDoubleBorder)
	{
		const float Width = SpaceWidth + Thickness;
		Top += Width;
		Bottom -= Width;
		Left += Width;
		Right -= Width;

		PaintBorder(AllottedGeometry, OutDrawElements, LayerId, InWidgetStyle, bEnabled, { Top, Bottom, Left, Right, Size });
	}

	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bEnabled);
}

void SPHDosBorder::PaintBorder(const FGeometry& AllottedGeometry, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled, const FBorderGeometry& BorderGeometry) const
{
	const bool bShowDisabledEffect = ShowDisabledEffect.Get();
	const ESlateDrawEffect DrawEffects = (bShowDisabledEffect && !bParentEnabled) ? ESlateDrawEffect::DisabledEffect : ESlateDrawEffect::None;
	const FLinearColor BorderTint = InWidgetStyle.GetColorAndOpacityTint() * BorderColor.Get().GetColor(InWidgetStyle);
	const bool bAntialias = true;
	uint8 LineIndex = 0;
	
	TArray<FVector2D> Points;

	const float HalfThickness = Thickness / 2.f;
	const FVector2D& HeaderSize = Header->GetPaintSpaceGeometry().GetLocalSize();
	const float HeaderSpaceWidth = HeaderSize.X + 30.f;

	if (bShowHeader)
	{
		FVector2D Point_BlankLeft;

		switch (BlankPosition)
		{
			case EHeaderPosition::Left:
				Point_BlankLeft = { HalfThickness + BlankOffset, BorderGeometry.Top };
				break;
			case EHeaderPosition::Center:
				Point_BlankLeft = { BorderGeometry.OverallSize.X / 2.f - HeaderSpaceWidth / 2.f, BorderGeometry.Top };
				break;
			case EHeaderPosition::Right:
				Point_BlankLeft = { BorderGeometry.OverallSize.X - HalfThickness - BlankOffset - HeaderSpaceWidth, BorderGeometry.Top };
				break;
		}

		if (!bAnimate)
		{
			Points.Add(Point_BlankLeft);
		}
		else
		{
			const float BlankLeftRight = FMath::Lerp(Point_BlankLeft.X, BorderGeometry.Left, BorderCurve[LineIndex++].GetLerp());
			const FVector2D Point_BlankLeftRight(BlankLeftRight, Point_BlankLeft.Y);
			FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), { Point_BlankLeft, Point_BlankLeftRight }, DrawEffects, BorderTint, bAntialias, Thickness);
		}

		float HeaderY = HeaderY = -HeaderSize.Y / 2.f;
		if (bDoubleBorder)
		{
			HeaderY += Thickness;
		}

		OverlayHeaderSlot->Padding(Point_BlankLeft.X + 15.f, HeaderY, 0.f, 0.f);
	}
	
	const FVector2D Point_LeftTop(BorderGeometry.Left, BorderGeometry.Top);
	const FVector2D Point_LeftBottom(BorderGeometry.Left, BorderGeometry.Bottom);
	const FVector2D Point_RightBottom(BorderGeometry.Right, BorderGeometry.Bottom);
	const FVector2D Point_RightTop(BorderGeometry.Right, BorderGeometry.Top);

	if (bAnimate)
	{
		const float TopBottom = FMath::Lerp(BorderGeometry.Top, BorderGeometry.Bottom, BorderCurve[LineIndex++].GetLerp());
		const FVector2D Point_LeftTopBottom(BorderGeometry.Left, TopBottom);
		FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), { Point_LeftTop, Point_LeftTopBottom }, DrawEffects, BorderTint, bAntialias, Thickness);
		
		const float LeftRight = FMath::Lerp(BorderGeometry.Left, BorderGeometry.Right, BorderCurve[LineIndex++].GetLerp());
		const FVector2D Point_LeftRightBottom(LeftRight, BorderGeometry.Bottom);
		FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), { Point_LeftBottom, Point_LeftRightBottom }, DrawEffects, BorderTint, bAntialias, Thickness);
		
		const float BottomTop = FMath::Lerp(BorderGeometry.Bottom, BorderGeometry.Top, BorderCurve[LineIndex++].GetLerp());
		const FVector2D Point_RightBottomTop(BorderGeometry.Right, BottomTop);
		FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), { Point_RightBottom, Point_RightBottomTop }, DrawEffects, BorderTint, bAntialias, Thickness);
	}

	if (!bAnimate)
	{
		Points.Add(Point_LeftTop);
		Points.Add(Point_LeftBottom);
		Points.Add(Point_RightBottom);
		Points.Add(Point_RightTop);
	}

	if (!bShowHeader)
	{
		if (!bAnimate)
		{
			Points.Add(Point_LeftTop);
		}
		else
		{
			const float RightLeft = FMath::Lerp(BorderGeometry.Right, BorderGeometry.Left, BorderCurve[LineIndex++].GetLerp());
			const FVector2D Point_RightLeftTop(RightLeft, BorderGeometry.Top);
			FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), { Point_RightTop, Point_RightLeftTop }, DrawEffects, BorderTint, bAntialias, Thickness);
		}
	}
	else
	{
		FVector2D Point_BlankRight;

		switch (BlankPosition)
		{
			case EHeaderPosition::Left:
				Point_BlankRight = { Thickness + BlankOffset + HeaderSpaceWidth, BorderGeometry.Top };
				break;
			case EHeaderPosition::Center:
				Point_BlankRight = { BorderGeometry.OverallSize.X / 2.f + HeaderSpaceWidth / 2.f, BorderGeometry.Top };
				break;
			case EHeaderPosition::Right:
				Point_BlankRight = { BorderGeometry.OverallSize.X - HalfThickness - BlankOffset, BorderGeometry.Top };
				break;
		}

		if (!bAnimate)
		{
			Points.Add(Point_BlankRight);
		}
		else
		{
			const float BlankRightLeft = FMath::Lerp(Point_RightTop.X, Point_BlankRight.X, BorderCurve[LineIndex++].GetLerp());
			const FVector2D Point_BlankRightLeft(BlankRightLeft, Point_BlankRight.Y);
			FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), { Point_RightTop, Point_BlankRightLeft }, DrawEffects, BorderTint, bAntialias, Thickness);
		}
	}

	if (!bAnimate)
	{
		FSlateDrawElement::MakeLines(OutDrawElements, LayerId, AllottedGeometry.ToPaintGeometry(), Points, DrawEffects, BorderTint, bAntialias, Thickness);
	}
}

void SPHDosBorder::SetupAnimation()
{
	if (bAnimate)
	{
		BorderCurve.Empty();
		AnimCurves = FCurveSequence();

		float AnimationStart = 0.f;
		if (bShowHeader)
		{
			AnimationStart = AnimDuration;
			switch (BlankPosition)
			{
				case EHeaderPosition::Left:
					AnimationStart /= 5.f;
					break;
				case EHeaderPosition::Center:
					AnimationStart /= 2.5f;
					break;
				case EHeaderPosition::Right:
					AnimationStart /= 1.25f;
					break;
			}

			BorderCurve.Add(AnimCurves.AddCurve(0, AnimationStart, ECurveEaseFunction::Linear)); //top-left
		}

		BorderCurve.Add(AnimCurves.AddCurve(AnimationStart, AnimDuration, ECurveEaseFunction::QuadInOut)); //left
		BorderCurve.Add(AnimCurves.AddCurve(AnimationStart, AnimDuration, ECurveEaseFunction::QuadInOut)); //bottom
		BorderCurve.Add(AnimCurves.AddCurve(AnimationStart, AnimDuration, ECurveEaseFunction::QuadInOut)); //right
		BorderCurve.Add(AnimCurves.AddCurve(AnimationStart, AnimDuration - AnimationStart, ECurveEaseFunction::QuadInOut)); //top-right

		BorderCurve.Add(AnimCurves.AddCurve(1 * AnimDuration, AnimDuration * 0.5f, ECurveEaseFunction::CubicOut)); //content

		AnimCurves.Play(this->AsShared(), false);
	}
}

void SPHDosBorder::SetPadding(const FMargin& InPadding)
{
	OverlayContentSlot->Padding(InPadding + Thickness);
	Invalidate(EInvalidateWidget::Layout);
}

void SPHDosBorder::SetShowEffectWhenDisabled(const TAttribute<bool>& InShowEffectWhenDisabled)
{
	SetAttribute(ShowDisabledEffect, InShowEffectWhenDisabled, EInvalidateWidgetReason::Paint);
}

void SPHDosBorder::SetDesiredSizeScale(const TAttribute<FVector2D>& InDesiredSizeScale)
{
	SetAttribute(DesiredSizeScale, InDesiredSizeScale, EInvalidateWidgetReason::Layout);
}

void SPHDosBorder::SetHAlign(EHorizontalAlignment HAlign)
{
	if (OverlayContentSlot->HAlignment != HAlign)
	{
		OverlayContentSlot->HAlign(HAlign);
		Invalidate(EInvalidateWidget::Layout);
	}
}

void SPHDosBorder::SetVAlign(EVerticalAlignment VAlign)
{
	if (OverlayContentSlot->VAlignment != VAlign)
	{
		OverlayContentSlot->VAlign(VAlign);
		Invalidate(EInvalidateWidget::Layout);
	}
}

void SPHDosBorder::SetHeaderText(const FText& InText)
{
	HeaderText = InText;
}

void SPHDosBorder::SetFont(const FSlateFontInfo& InFont)
{
	Font = InFont;
}

void SPHDosBorder::SetThickness(float InThickness)
{
	Thickness = InThickness;
}

void SPHDosBorder::SetShowHeader(bool bInShowHeader)
{
	bShowHeader = bInShowHeader;
}

void SPHDosBorder::SetBlankOffset(float InBlankOffset)
{
	BlankOffset = InBlankOffset;
}

void SPHDosBorder::SetSpaceWidth(float InSpaceWidth)
{
	SpaceWidth = InSpaceWidth;
}

void SPHDosBorder::SetBlankPosition(EHeaderPosition InBlankPosition)
{
	BlankPosition = InBlankPosition;
}

void SPHDosBorder::SetDoubleBorder(bool bInDoubleBorder)
{
	bDoubleBorder = bInDoubleBorder;
}

void SPHDosBorder::SetBorderColorAndOpacity(const TAttribute<FSlateColor>& InColorAndOpacity)
{
	SetAttribute(BorderColor, InColorAndOpacity, EInvalidateWidgetReason::Paint);
}

void SPHDosBorder::SetAnimate(bool bInAnim)
{
	bAnimate = bInAnim;
}

void SPHDosBorder::SetAnimDuration(float InAnimDuration)
{
	AnimDuration = InAnimDuration;

	SetupAnimation();
}

FVector2D SPHDosBorder::ComputeDesiredSize(float LayoutScaleMultiplier) const
{
	return DesiredSizeScale.Get() * SCompoundWidget::ComputeDesiredSize(LayoutScaleMultiplier);
}

FText SPHDosBorder::GetHeaderText() const
{
	return HeaderText;
}

FSlateColor SPHDosBorder::GetBorderColor() const
{
	return BorderColor.Get();
}

FLinearColor SPHDosBorder::GetHeaderAndContentColor() const
{
	if (bAnimate)
	{
		const float Value = BorderCurve[BorderCurve.Num() - 1].GetLerp();
		return FLinearColor(1.f,1.f,1.f, Value);
	}
	return FLinearColor::White;
}

FSlateFontInfo SPHDosBorder::GetFont() const
{
	return Font;
}

EVisibility SPHDosBorder::GetHeaderVisibility() const
{
	return bShowHeader ? EVisibility::HitTestInvisible : EVisibility::Collapsed;
}

void SPHDosBorder::SetContent(TSharedRef<SWidget> InContent)
{
	(*OverlayContentSlot)
	[
		InContent
	];
}

END_SLATE_FUNCTION_BUILD_OPTIMIZATION
