// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/PHEditableTextBox.h"

#include "Widgets/Input/SEditableTextBox.h"


UPHEditableTextBox::UPHEditableTextBox() 
	: MinimumIntegralDigits(1), 
	MaximumIntegralDigits(3), 
	MinimumFractionalDigits(0), 
	MaximumFractionalDigits(0)
{
	NumberFormattingOptions.MinimumIntegralDigits = MinimumIntegralDigits;
	NumberFormattingOptions.MaximumIntegralDigits = MaximumIntegralDigits;
	NumberFormattingOptions.MinimumFractionalDigits = MinimumFractionalDigits;
	NumberFormattingOptions.MaximumFractionalDigits = MaximumFractionalDigits;
	NumberFormattingOptions.UseGrouping = false;
	NumberFormattingOptions.RoundingMode = ERoundingMode::HalfFromZero;
}

void UPHEditableTextBox::SynchronizeProperties()
{
	NumberFormattingOptions.MinimumIntegralDigits = MinimumIntegralDigits;
	NumberFormattingOptions.MaximumIntegralDigits = MaximumIntegralDigits;
	NumberFormattingOptions.MinimumFractionalDigits = MinimumFractionalDigits;
	NumberFormattingOptions.MaximumFractionalDigits = MaximumFractionalDigits;

	Super::SynchronizeProperties();
}

float UPHEditableTextBox::GetValue() const
{
	return FCString::Atof(*GetText().ToString());
}

void UPHEditableTextBox::SetValue(float InValue)
{
	SetText(FText::AsNumber(InValue, &NumberFormattingOptions));
}

void UPHEditableTextBox::HandleOnTextChanged(const FText& InText)
{
	if (bIsNumber)
	{
		// Validate the text on change, and only accept text up until the first invalid character
		const FString& Data = InText.ToString();
		int32 NumValidChars = Data.Len();

		for (int32 Index = 0; Index < Data.Len(); ++Index)
		{
			if (!FCString::IsNumeric(&Data[Index]) || (MaxWidth != 0 && Index >= MaxWidth))
			{
				NumValidChars = Index;
				break;
			}
		}

		if (NumValidChars < Data.Len())
		{
			FText ValidData = NumValidChars > 0 ? FText::FromString(Data.Left(NumValidChars)) : FText::GetEmpty();

			MyEditableTextBlock->SetText(ValidData);
			Super::HandleOnTextChanged(ValidData);

			return;
		}
	}

	Super::HandleOnTextChanged(InText);
}

void UPHEditableTextBox::HandleOnTextCommitted(const FText& InText, ETextCommit::Type CommitMethod)
{
	if (bIsNumber && MinValue != MaxValue)
	{
		float Value = FMath::Clamp(FCString::Atof(*Text.ToString()), MinValue, MaxValue);
		FText NewText = FText::AsNumber(Value, &NumberFormattingOptions);

		MyEditableTextBlock->SetText(NewText);
		Super::HandleOnTextCommitted(NewText, CommitMethod);
	}
	else
	{
		Super::HandleOnTextCommitted(InText, CommitMethod);
	}
}
