// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/UI/TickerWidget.h"

#include "Components/ScrollBox.h"


void UTickerWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	const float ScrollOffsetOfEnd = ScrollContent->GetScrollOffsetOfEnd();
	if (ScrollOffsetOfEnd > 0.f)
	{
		if (!FMath::IsNearlyEqual(SleepTime, Delay, 0.01f))
		{
			SleepTime += InDeltaTime;
			return;
		}

		float Offset = ScrollContent->GetScrollOffset();

		if (ScrollForward)
		{
			if (!FMath::IsNearlyEqual(Offset, ScrollOffsetOfEnd))
			{
				Offset = FMath::FInterpConstantTo(Offset, ScrollOffsetOfEnd, InDeltaTime, Speed);
			}
			else
			{
				ScrollForward = false;
				SleepTime = 0.f;
			}
		}
		else
		{
			if (!FMath::IsNearlyZero(Offset))
			{
				Offset = FMath::FInterpConstantTo(Offset, 0.f, InDeltaTime, Speed);
			}
			else
			{
				ScrollForward = true;
				SleepTime = 0.f;
			}
		}

		ScrollContent->SetScrollOffset(Offset);
	}
	else if (SleepTime > 0.f)
	{
		SleepTime = 0.f;
	}
}
