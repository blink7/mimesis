// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/PHUtilities.h"

#include "Components/SkeletalMeshComponent.h"


class USkeletalMeshComponent* PropHunt::Utilities::CopyMeshComponent(class USkeletalMeshComponent* Template, FString NameSuffix)
{
	if (Template)
	{
		auto NewActorComp = DuplicateObject(Template, Template->GetOwner(), FName(*(Template->GetName() + NameSuffix)));
		if (NewActorComp)
		{
			NewActorComp->CreationMethod = EComponentCreationMethod::UserConstructionScript;
			Template->GetOwner()->BlueprintCreatedComponents.Add(NewActorComp);
			NewActorComp->OnComponentCreated();

			NewActorComp->SetupAttachment(Template);
			NewActorComp->SetRelativeTransform(FTransform());

			NewActorComp->SetMasterPoseComponent(Template);

			if (NewActorComp->bAutoRegister)
			{
				NewActorComp->RegisterComponent();
			}

			return NewActorComp;
		}
	}

	return nullptr;
}

void PropHunt::Utilities::OverrideMeshMaterials(USkeletalMeshComponent* TargetMesh, UMaterialInterface* NewMaterial)
{
	if (NewMaterial)
	{
		for (int32 i = 0; i < TargetMesh->GetNumMaterials(); i++)
		{
			TargetMesh->SetMaterial(i, NewMaterial);
		}
	}
}

FName PropHunt::Utilities::CollisionShapeToName(ECollisionShape::Type ShapeType)
{
	switch (ShapeType)
	{
	case ECollisionShape::Line:		return FName("Line");
	case ECollisionShape::Box:		return FName("Box");
	case ECollisionShape::Sphere:	return FName("Sphere");
	case ECollisionShape::Capsule:	return FName("Capsule");
	default:						return FName("None");
	}
}

FString PropHunt::Utilities::TimeToString(float TimeSeconds)
{
	// only minutes and seconds are relevant
	const int32 TotalSeconds = FMath::Max(0, FMath::TruncToInt(TimeSeconds) % 3600);
	const int32 NumMinutes = TotalSeconds / 60;
	const int32 NumSeconds = TotalSeconds % 60;

	const FString TimeDesc = FString::Printf(TEXT("%02d:%02d"), FMath::Max(0, NumMinutes), FMath::Max(0, NumSeconds));
	return TimeDesc;
}
