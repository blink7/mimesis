// Copyright Art-Sun Games. All Rights Reserved.

#include "Core/PHImageLoader.h"

#include "ImageUtils.h"
#include "IImageWrapperModule.h"
#include "IImageWrapper.h"

UPHImageLoader* UPHImageLoader::LoadImageFromDiskAsync(UObject* Outer, const FString& ImagePath)
{
	// This simply creates a new ImageLoader object and starts an asynchronous load.
	auto Loader = NewObject<UPHImageLoader>();
	Loader->LoadImageAsync(Outer, ImagePath);
	return Loader;
}

TFuture<UTexture2D*> UPHImageLoader::LoadImageFromDiskAsync(UObject* Outer, const FString& ImagePath, TFunction<void()> CompletionCallback)
{
	// Run the image loading function asynchronously through a lambda expression, capturing the ImagePath string by value.
	// Run it on the thread pool, so we can load multiple images simultaneously without interrupting other tasks.
	return Async(EAsyncExecution::ThreadPool, [=]() { return LoadImageFromDisk(Outer, ImagePath); }, CompletionCallback);
}

UTexture2D* UPHImageLoader::LoadImageFromDisk(UObject* Outer, const FString& ImagePath)
{
	TArray<uint8> Buffer;
	if (!FFileHelper::LoadFileToArray(Buffer, *ImagePath))
	{
		return nullptr;
	}

	IImageWrapperModule& ImageWrapperModule = FModuleManager::Get().LoadModuleChecked<IImageWrapperModule>(TEXT("ImageWrapper"));
	const EImageFormat Format = ImageWrapperModule.DetectImageFormat(Buffer.GetData(), Buffer.GetAllocatedSize());
	const TSharedPtr<IImageWrapper> ImageWrapper = ImageWrapperModule.CreateImageWrapper(Format);
	if (!ImageWrapper->SetCompressed(Buffer.GetData(), Buffer.GetAllocatedSize()))
	{
		return nullptr;
	}

	const int32 SrcWidth = ImageWrapper->GetWidth();;
	const int32 SrcHeight = ImageWrapper->GetHeight();

	ERGBFormat RGBFormat = ERGBFormat::Invalid;
	const int32 BitDepth = ImageWrapper->GetBitDepth();
	if (BitDepth == 16)
	{
		RGBFormat = ERGBFormat::BGRA;
	}
	else if (BitDepth == 8)
	{
		RGBFormat = ERGBFormat::BGRA;
	}

	TArray64<uint8> UncompressedData;
	ImageWrapper->GetRaw(RGBFormat, BitDepth, UncompressedData);

	TArray<FColor> RawData;
	RawData.AddUninitialized(SrcWidth * SrcHeight);
	FMemory::Memcpy(RawData.GetData(), UncompressedData.GetData(), UncompressedData.GetAllocatedSize());

	TArray<FColor> ScaledRawData;

	int32 DstWidth = 512;
	int32 DstHeight = 512;
	
	// Check if imported image is too large and scale it if so
	if (SrcWidth > DstWidth || SrcHeight > DstHeight)
	{
		const float SrcAspectRatio = static_cast<float>(SrcWidth) / static_cast<float>(SrcHeight);
		if (SrcAspectRatio > 1)
		{
			DstHeight = FMath::RoundHalfToZero(DstWidth / SrcAspectRatio);
		}
		else
		{
			DstWidth = FMath::RoundHalfToZero(DstHeight * SrcAspectRatio);
		}

		FImageUtils::ImageResize(SrcWidth, SrcHeight, RawData, DstWidth, DstHeight, ScaledRawData, true);
	}
	else
	{
		DstWidth = SrcWidth;
		DstHeight = SrcHeight;

		ScaledRawData = RawData;
	}

	// Compress the scaled image
	TArray<uint8> ScaledPng;
	FImageUtils::CompressImageArray(DstWidth, DstHeight, ScaledRawData, ScaledPng);

	return FImageUtils::ImportBufferAsTexture2D(ScaledPng);
}

void UPHImageLoader::LoadImageAsync(UObject* Outer, const FString& ImagePath)
{
	// The asynchronous loading operation is represented by a Future, which will contain the result value once the operation is done.
	// We store the Future in this object, so we can retrieve the result value in the completion callback below.
	Future = LoadImageFromDiskAsync(Outer, ImagePath, [this, ImagePath]()
	{
		// This is the same Future object that we assigned above, but later in time.
		// At this point, loading is done and the Future contains a value.
		if (Future.IsValid())
		{
			// Notify listeners about the loaded texture on the game thread.
			AsyncTask(ENamedThreads::GameThread, [this, ImagePath]() { LoadCompleted.Broadcast(Future.Get(), ImagePath); });
		}
	});
}
