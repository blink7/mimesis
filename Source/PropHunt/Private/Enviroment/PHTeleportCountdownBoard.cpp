// Copyright Art-Sun Games. All Rights Reserved.


#include "Enviroment/PHTeleportCountdownBoard.h"

#include "Core/PHUtilities.h"
#include "Core/Online/PHGameState.h"

#include "Components/TextRenderComponent.h"

#define LOCTEXT_NAMESPACE "Teleport"

APHTeleportCountdownBoard::APHTeleportCountdownBoard() : EasterEggDisplayChance(1)
{}

void APHTeleportCountdownBoard::BeginPlay()
{
	Super::BeginPlay();

	SetupCountdown();
}

void APHTeleportCountdownBoard::SetupCountdown()
{
	if (const auto GameState = GetWorld()->GetGameState<APHGameState>())
	{
		if (GameState->IsMatchInProgress())
		{
			StartCountdown();
		}
		else
		{
			GameState->OnMatchHasStarted.AddDynamic(this, &APHTeleportCountdownBoard::StartCountdown);
		}

		GameState->OnMatchHasStartedHunting.AddDynamic(this, &APHTeleportCountdownBoard::StopCountdown);
	}
	else
	{
		// Keep retrying until Game State is ready
		FTimerHandle TimerHandle_Unused;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Unused, this, &APHTeleportCountdownBoard::SetupCountdown, 0.2f, false);
	}
}

void APHTeleportCountdownBoard::StartCountdown()
{
	if (!bCountdownStarted)
	{
		if (const auto GameState = GetWorld()->GetGameState<APHGameState>())
		{
			UpdateCountdown(GameState->GetRemainingTime());
			GameState->OnRemainingTimeChanged.AddDynamic(this, &APHTeleportCountdownBoard::UpdateCountdown);
			bCountdownStarted = true;
		}
	}
}

void APHTeleportCountdownBoard::StopCountdown()
{
	if (bCountdownStarted)
	{
		if (const auto GameState = GetWorld()->GetGameState<APHGameState>())
		{
			GameState->OnRemainingTimeChanged.RemoveDynamic(this, &APHTeleportCountdownBoard::UpdateCountdown);
			bCountdownStarted = false;
		}
	}
}

void APHTeleportCountdownBoard::UpdateCountdown(int32 Time)
{
	const int32 TotalSeconds = FMath::Max(0, Time % 3600);
	if (TotalSeconds == 2)
	{
		GetTextRender()->SetText(LOCTEXT("Ready", "READY"));
		return;
	}
	if (TotalSeconds == 1)
	{
		GetTextRender()->SetText(LOCTEXT("Go", "GO!"));
		StopCountdown();
		return;
	}
	
	static uint8 bEasterEggShowed = 0;
	if (!bEasterEggShowed && EasterEggWords.Num() > 0)
	{
		const int32 Rand = FMath::RandHelper(100 - EasterEggDisplayChance);
		if (Rand == 0)
		{
			GetTextRender()->SetText(EasterEggWords[FMath::RandHelper(EasterEggWords.Num())]);
			bEasterEggShowed++;
			return;
		}
	}

	GetTextRender()->SetText(FText::FromString(PropHunt::Utilities::TimeToString(Time)));
}

#undef LOCTEXT_NAMESPACE
