// Copyright Art-Sun Games. All Rights Reserved.

#include "Enviroment/Door.h"

#include "Core/Abilities/PHGameplayAbility.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"

#include "EngineUtils.h"
#include "Engine/StaticMesh.h"
#include "Net/UnrealNetwork.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/StaticMeshComponent.h"

#include "AkComponent.h"
#include "AkAcousticPortal.h"


namespace Color
{
	const FColor Red = FColor(240.f, 40.f, 40.f);
	const FColor Blue = FColor(40.f, 130.f, 240.f);
}

ADoor::ADoor() :
	InitialDoorState(EDoorState::DS_Close),
	OpenAngle(90.f), 
	HandleOpenAngle(45.f), 
	BlockingAngle(30.f),
	PushOffset(5.f)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent0");
	StaticMeshComponent->SetCollisionProfileName(InteractableCollisionProfileName);
	StaticMeshComponent->Mobility = EComponentMobility::Static;
	StaticMeshComponent->SetGenerateOverlapEvents(false);
	StaticMeshComponent->bUseDefaultCollision = true;
	RootComponent = StaticMeshComponent;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> DoorFrameMeshObject(TEXT("/Game/PropHunt/Environment/Doors/SM_DoorFrame_01"));
	if (DoorFrameMeshObject.Succeeded())
	{
		StaticMeshComponent->SetStaticMesh(DoorFrameMeshObject.Object);
	}

	Door1 = CreateDefaultSubobject<UStaticMeshComponent>("Door1");
	Door1->SetupAttachment(GetRootComponent(), FName("Hinge_1"));
	Door1->SetCollisionProfileName(InteractableCollisionProfileName);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> DoorMeshObject(TEXT("/Game/PropHunt/Environment/Doors/SM_Door_01"));
	if (DoorMeshObject.Succeeded())
	{
		Door1->SetStaticMesh(DoorMeshObject.Object);
	}

	Door1_Handle1 = CreateDefaultSubobject<UStaticMeshComponent>("Door1_Handle1");
	Door1_Handle1->SetupAttachment(Door1, FName("Handle_1"));
	Door1_Handle1->SetCollisionProfileName(InteractableCollisionProfileName);

	Door1_Handle2 = CreateDefaultSubobject<UStaticMeshComponent>("Door1_Handle2");
	Door1_Handle2->SetupAttachment(Door1, FName("Handle_2"));
	Door1_Handle2->SetCollisionProfileName(InteractableCollisionProfileName);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> DoorHandleMeshObject(TEXT("/Game/PropHunt/Environment/Doors/SM_DoorHandle_01"));
	if (DoorMeshObject.Succeeded())
	{
		Door1_Handle1->SetStaticMesh(DoorHandleMeshObject.Object);
		Door1_Handle2->SetStaticMesh(DoorHandleMeshObject.Object);
	}

	Door1_Trigger = CreateDefaultSubobject<UBoxComponent>("Door1_Trigger");
	Door1_Trigger->SetupAttachment(Door1);
	Door1_Trigger->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Door1_Trigger->SetCollisionResponseToAllChannels(ECR_Ignore);
	Door1_Trigger->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	if (HasAuthority())
	{
		Door1_Trigger->OnComponentBeginOverlap.AddDynamic(this, &ADoor::OnDoorBeginOverlap);
	}

	AkComponent = CreateDefaultSubobject<UAkComponent>("AkAudioComponent0");
	AkComponent->SetupAttachment(GetRootComponent());
	AkComponent->OcclusionRefreshInterval = 0.f;

#if WITH_EDITORONLY_DATA
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		FName ID_Doors;
		FText NAME_Doors;

		FConstructorStatics() : 
			ID_Doors(TEXT("DoubleDoors")), 
			NAME_Doors(NSLOCTEXT("SpriteCategory", "Doors", "Doors"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	ArrowComponent = CreateEditorOnlyDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	if (ArrowComponent)
	{
		ArrowComponent->SetupAttachment(GetRootComponent());
		ArrowComponent->SetRelativeLocation({ 0.f, 0.f, 100.f });
		ArrowComponent->SetRelativeRotation({ 0.f, 90.f, 0.f });
		ArrowComponent->ArrowColor = Color::Red;
		ArrowComponent->bTreatAsASprite = true;
		ArrowComponent->SpriteInfo.Category = ConstructorStatics.ID_Doors;
		ArrowComponent->SpriteInfo.DisplayName = ConstructorStatics.NAME_Doors;
		ArrowComponent->bIsScreenSizeScaled = true;
	}

	StatusText = CreateEditorOnlyDefaultSubobject<UTextRenderComponent>(TEXT("Status"));
	if (StatusText)
	{
		StatusText->SetupAttachment(GetRootComponent());
		StatusText->SetRelativeLocation({ 0.f, -10.f, 100.f });
		StatusText->SetRelativeRotation({ 0.f, -90.f, 0.f });
		StatusText->SetHorizontalAlignment(EHTA_Center);
		StatusText->SetHiddenInGame(true);
	}
#endif // WITH_EDITORONLY_DATA
}

void ADoor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ADoor, DoorAngleFactor, COND_InitialOnly);
}

#if WITH_EDITOR
void ADoor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (!ArrowComponent || !StatusText)
	{
		return;
	}

	const bool bIsInverted = StaticMeshComponent->GetComponentScale().X < 0;

	if (bIsInverted)
	{
		StatusText->SetRelativeRotation({ 0.f, 90.f, 0.f });
	}
	else
	{
		StatusText->SetRelativeRotation({ 0.f, -90.f, 0.f });
	}

	if (InitialDoorState == EDoorState::DS_Open)
	{
		StatusText->SetText(FText::FromString("Open"));
		StatusText->SetTextRenderColor(Color::Blue);
		ArrowComponent->SetArrowColor(Color::Blue);

		SetDoorRotation(1.f);
	}
	else
	{
		if (InitialDoorState == EDoorState::DS_Close)
		{
			StatusText->SetText(FText::FromString("Close"));
		}
		else if (InitialDoorState == EDoorState::DS_Lock)
		{
			StatusText->SetText(FText::FromString("Lock"));
		}
		
		StatusText->SetTextRenderColor(Color::Red);
		ArrowComponent->SetArrowColor(Color::Red);

		SetDoorRotation(0.f);
	}
}
#endif

void ADoor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (HasAuthority())
	{
		if (InitialDoorState != EDoorState::DS_Lock)
		{
			FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
			EffectContext.AddSourceObject(this);

			FGameplayEffectSpecHandle NewHandle;
			if (InitialDoorState == EDoorState::DS_Open)
			{
				NewHandle = AbilitySystemComponent->MakeOutgoingSpec(OpenEffect, 1, EffectContext);
			}
			else if (InitialDoorState == EDoorState::DS_Close)
			{
				NewHandle = AbilitySystemComponent->MakeOutgoingSpec(CloseEffect, 1, EffectContext);
			}
			
			if (NewHandle.IsValid())
			{
				AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*NewHandle.Data.Get());
			}
		}
	}

	InitializePortal();
}

void ADoor::BeginPlay()
{
	Super::BeginPlay();

	AdjustDoorTrigger(Door1, Door1_Trigger);

	SetDoorRotation(DoorAngleFactor);
}

void ADoor::PostInteract_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent)
{
	if (Abilities.Num() > 0)
	{
		AbilitySystemComponent->TryActivateAbilityByClass(Abilities[0]);
	}
}

bool ADoor::IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const
{
	if (!IsMoving() && (InteractionComponent == Door1 || InteractionComponent == Door1_Handle1 || InteractionComponent == Door1_Handle2))
	{
		return true;
	}
	return false;
}

FText ADoor::GetInteractionHint_Implementation(UPrimitiveComponent* InteractionComponent) const
{
	if (IsOpen())
	{
		return NSLOCTEXT("Door", "Close", "Close <Hightlight>[{0}]</>");
	}
	if (IsClose())
	{
		return NSLOCTEXT("Door", "Open", "Open <Hightlight>[{0}]</>");
	}
	return NSLOCTEXT("Door", "Locked", "Locked");
}

void ADoor::SetDoorRotation(float AngleFactor)
{
	Door1->SetRelativeRotation(FRotator(0.f, OpenAngle * AngleFactor, 0.f));
	DoorAngleFactor = AngleFactor;

	if (AssociatedPortal.IsValid() && FMath::IsNearlyEqual(AngleFactor, 0.2f, 0.1f))
	{
		if (IsOpen() && AssociatedPortal->GetCurrentState() != AkAcousticPortalState::Closed)
		{
			AssociatedPortal->ClosePortal();
		}
		else if (IsClose() && AssociatedPortal->GetCurrentState() != AkAcousticPortalState::Open)
		{
			AssociatedPortal->OpenPortal();
		}
	}
}

void ADoor::SetHandleRotation(float AngleFactor)
{
	Door1_Handle1->SetRelativeRotation(FRotator(0.f, HandleOpenAngle * AngleFactor, 0.f));
	Door1_Handle2->SetRelativeRotation(FRotator(0.f, -HandleOpenAngle * AngleFactor, 0.f));
}

void ADoor::OnDoorBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!IsMoving())
	{
		return;
	}

	const bool bBlocked = AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag("Object.Door.State.Blocked"));
	if (bBlocked)
	{
		return;
	}

	if (IsClose())
	{
		if (OtherActor)
		{
			OtherActor->AddActorWorldOffset(StaticMeshComponent->GetRightVector() * PushOffset);
		}
	}
	else if (IsOpen())
	{
		const float CurrentDoorAngle  = DoorAngleFactor * OpenAngle;
		if (CurrentDoorAngle > BlockingAngle)
		{
			FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
			EffectContext.AddSourceObject(this);

			const FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(BlockEffect, 1, EffectContext);
			if (NewHandle.IsValid())
			{
				BlockEffectHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*NewHandle.Data.Get());
			}
		}
		else if (OtherActor)
		{
			OtherActor->AddActorWorldOffset(StaticMeshComponent->GetRightVector() * -PushOffset);
		}
	}
}

void ADoor::OnRep_DoorAngleFactor()
{
	SetDoorRotation(DoorAngleFactor);
}

void ADoor::InitializePortal()
{
	for (TActorIterator<AAkAcousticPortal> It(GetWorld()); It; ++It)
	{
		const FVector PortalLocation = It->GetActorLocation();
		const float Distance = (PortalLocation - GetActorLocation()).Size();
		if (Distance < 150.f)
		{
			AssociatedPortal = *It;
			break;
		}
	}

	if (AssociatedPortal.IsValid())
	{
		if (InitialDoorState == EDoorState::DS_Open)
		{
			AssociatedPortal->Portal->InitialState = AkAcousticPortalState::Open;
		}
		else
		{
			AssociatedPortal->Portal->InitialState = AkAcousticPortalState::Closed;
		}
	}
}

void ADoor::AdjustDoorTrigger(UStaticMeshComponent* Door, UBoxComponent* Trigger)
{
	const FBox& DoorBox = Door->GetStaticMesh()->GetBoundingBox();
	const FVector DoorExtent = DoorBox.GetExtent();
	const FVector DoorCenter = DoorBox.GetCenter();

	Trigger->SetBoxExtent({ DoorExtent.X, DoorExtent.Y, DoorExtent.Z - 2.f });
	Trigger->SetRelativeLocation(DoorCenter);
}

bool ADoor::IsOpen() const
{
	return AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag("Object.Door.State.Open"));
}

bool ADoor::IsClose() const
{
	return AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag("Object.Door.State.Close"));
}

bool ADoor::IsLock() const
{
	return !IsOpen() && !IsClose();
}

bool ADoor::IsMoving() const
{
	return AbilitySystemComponent->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag("Object.Door.State.Moving"));
}
