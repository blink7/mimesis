// Fill out your copyright notice in the Description page of Project Settings.

#include "Enviroment/TransformationBlockVolume.h"

#include "Components/BrushComponent.h"


ATransformationBlockVolume::ATransformationBlockVolume(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bColored = true;
	BrushColor.R = 255;
	BrushColor.G = 100;
	BrushColor.B = 100;
	BrushColor.A = 255;
}