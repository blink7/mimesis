// Fill out your copyright notice in the Description page of Project Settings.

#include "Enviroment/PropSpawn.h"

#include "../PropHunt.h"
#include "Enviroment/PhysicsProp.h"

#include "TimerManager.h"
#include "Components/BoxComponent.h"
#include "Components/BillboardComponent.h"
#include "Engine/World.h"
#include "Engine/Texture2D.h"
#include "Engine/StaticMesh.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/MaterialInterface.h"


DEFINE_LOG_CATEGORY(LogPropSpawn);

const uint8 MAX_SPAWN_ATTEMPTS = 5;

APropSpawn::APropSpawn()
{
	PrimaryActorTick.bCanEverTick = true;

	SpawnVolume = CreateDefaultSubobject<UBoxComponent>("SpawnVolume");
	SpawnVolume->SetCollisionProfileName("NoCollision");
	SpawnVolume->SetBoxExtent({ 30.f, 30.f, 10.f });
	SpawnVolume->AddLocalOffset({ 0.f, 0.f, 10.f });
	SetRootComponent(SpawnVolume);

	SpawnIcon = CreateEditorOnlyDefaultSubobject<UBillboardComponent>("SpawnIcon");

#if WITH_EDITORONLY_DATA
	if (!IsRunningCommandlet())
	{
		struct FConstructorStatics
		{
			ConstructorHelpers::FObjectFinderOptional<UTexture2D> IconTextureObject;
			FName ID_Icon;
			FText NAME_Icon;
			FConstructorStatics()
				: IconTextureObject(TEXT("/Engine/EditorResources/Spawn_Point"))
				, ID_Icon(TEXT("PropSpawn"))
				, NAME_Icon(NSLOCTEXT("SpriteCategory", "PropSpawn", "PropSpawn"))
			{}
		};
		static FConstructorStatics ConstructorStatics;

		if (SpawnIcon)
		{
			SpawnIcon->Sprite = ConstructorStatics.IconTextureObject.Get();
			SpawnIcon->SetRelativeScale3D({ 0.5f, 0.5f, 0.5f });
			SpawnIcon->bHiddenInGame = true;
			SpawnIcon->SpriteInfo.Category = ConstructorStatics.ID_Icon;
			SpawnIcon->SpriteInfo.DisplayName = ConstructorStatics.NAME_Icon;
			SpawnIcon->SetupAttachment(SpawnVolume);
			SpawnIcon->SetUsingAbsoluteScale(true);
			SpawnIcon->bIsScreenSizeScaled = true;
			SpawnIcon->SetRelativeLocation({ 0.f, 0.f, 10.f });
		}
	}
#endif // WITH_EDITORONLY_DATA
}

void APropSpawn::Destroyed()
{
	Super::Destroyed();

	if (GetWorldTimerManager().IsTimerActive(TimerHandle_SpawnQueue))
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_SpawnQueue);
	}
}

void APropSpawn::BeginPlay()
{
	Super::BeginPlay();

	if (!HasAuthority())
	{
		return;
	}

	// Delete not valid cells
	ItemsToSpawn.RemoveAll([](const FItem& TestItem) {
		return !TestItem.StaticMesh.GetUniqueID().IsValid() || TestItem.MaxSpawn == 0 || FMath::IsNearlyZero(TestItem.MinScale) || FMath::IsNearlyZero(TestItem.MaxScale);
	});

	if (ItemsToSpawn.Num() < 1)
	{
		UE_LOG(LogPropSpawn, Error, TEXT("Specify valid items to spawn for %s"), *GetName());
		return;
	}

	if (!ItemPropertiesTable)
	{
		UE_LOG(LogPropSpawn, Error, TEXT("Specify Item Properties Data for %s"), *GetName());
		return;
	}

	ItemPropertiesTable->GetAllRows("LoadItemProperties", ItemProperties);

	const int32 Range = bRandomizeAllItems ? ItemsToSpawn.Num() : 1;
	for (int32 i = 0; i < Range; ++i)
	{
		const int32 RandIndex = bRandomizeAllItems ? i : FMath::RandHelper(ItemsToSpawn.Num());
		const FItem& ItemToSpawn = ItemsToSpawn[RandIndex];
		const int32 NumberToSpawn = FMath::RandRange(ItemToSpawn.MinSpawn, ItemToSpawn.MaxSpawn);

		if (NumberToSpawn > 0)
		{
			for (int32 j = 0; j < NumberToSpawn; ++j)
			{
				QueueToSpawn.Enqueue(RandIndex);
			}
		}
	}
	
	if (!QueueToSpawn.IsEmpty())
	{
		auto SpawnQueue = [this]() {
			int32 Index;
			QueueToSpawn.Dequeue(Index);

			RandomlyPlaceItem(Index);

			if (QueueToSpawn.IsEmpty())
			{
				GetWorldTimerManager().ClearTimer(TimerHandle_SpawnQueue);
			}
		};
		GetWorldTimerManager().SetTimer(TimerHandle_SpawnQueue, SpawnQueue, 0.1f, true);
	}
}

FString CollisionShapeToString(EPHCollisionShape Type)
{
	switch (Type)
	{
		case EPHCollisionShape::Capsule:	return "Capsule";
		case EPHCollisionShape::Box:		return "Box";
		default:							return "Error";
	}
}

void APropSpawn::RandomlyPlaceItem(int32 Index)
{
	const FItem& ItemToSpawn = ItemsToSpawn[Index];
	FItemProperty** SearchResult = ItemProperties.FindByPredicate([&ItemToSpawn](FItemProperty* Property) {
		return Property->SpawnableItem.GetUniqueID() == ItemToSpawn.StaticMesh.GetUniqueID();
	});

	FItemProperty* ItemProperty = SearchResult ? *SearchResult : nullptr;
	if (!ItemProperty)
	{
		UE_LOG(LogPropSpawn, Warning, TEXT("There are no parameters in the table for %s"), *ItemToSpawn.StaticMesh.GetAssetName());
	}

	FPropState PropState;
	if (FindEmptyLocation(PropState.Location, ItemProperty ? ItemProperty->BoundingRadius : 30))
	{
		PropState.ItemMesh = ItemToSpawn.StaticMesh.LoadSynchronous();

		if (ItemToSpawn.Materials.Num() > 0)
		{
			PropState.ItemMaterial = ItemToSpawn.Materials[FMath::RandHelper(ItemToSpawn.Materials.Num())].LoadSynchronous();
		}

		PropState.Rotation = FMath::RandRange(ItemToSpawn.MinRotation, ItemToSpawn.MaxRotation);
		PropState.Scale = FMath::RandRange(ItemToSpawn.MinScale, ItemToSpawn.MaxScale);

		if (ItemProperty)
		{
			PropState.MassScale = ItemProperty->MassScale;
			PropState.ShapeType = CollisionShapeToString(ItemProperty->ShapeType);
		}

		SpawnProp(PropState);
	}
}

bool APropSpawn::FindEmptyLocation(FVector& OutLocation, float Radius) const
{
	const FVector& BoxExtent = SpawnVolume->GetUnscaledBoxExtent();

	for (uint8 i = 0; i < MAX_SPAWN_ATTEMPTS; ++i)
	{
		const FVector RandPoint = FVector(FMath::RandRange(-BoxExtent.X, BoxExtent.X), FMath::RandRange(-BoxExtent.Y, BoxExtent.Y), 0.f);
		const FVector CandidatePoint = GetTransform().TransformPosition(RandPoint);
		const bool bHasHit = GetWorld()->OverlapBlockingTestByChannel(CandidatePoint, FQuat::Identity, COLLISION_SPAWN, FCollisionShape::MakeSphere(Radius));
		if (!bHasHit)
		{
			OutLocation = CandidatePoint;
			OutLocation.Z = SpawnVolume->GetComponentLocation().Z - SpawnVolume->GetScaledBoxExtent().Z;
			return true;
		}
	}
	return false;
}

void APropSpawn::SpawnProp(const FPropState& PropState) const
{
	FTransform SpawnTransform(FRotator(0.f, PropState.Rotation, 0.f), PropState.Location, FVector(PropState.Scale));
	APhysicsProp* SpawnedActor = GetWorld()->SpawnActor<APhysicsProp>(APhysicsProp::StaticClass(), SpawnTransform);
	if (SpawnedActor)
	{
		SpawnedActor->SetState(PropState);
	}
}
