// Copyright Art-Sun Games. All Rights Reserved.

#include "Enviroment/DoubleDoors.h"

#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

#include "UObject/ConstructorHelpers.h"


ADoubleDoors::ADoubleDoors()
{
	Door2 = CreateDefaultSubobject<UStaticMeshComponent>("Door2");
	Door2->SetupAttachment(GetRootComponent(), FName("Hinge_2"));
	Door2->SetRelativeScale3D({ -1.f, 1.f, 1.f });
	Door2->SetCollisionProfileName(InteractableCollisionProfileName);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> DoorMeshObject(TEXT("/Game/PropHunt/Environment/Doors/SM_Door_01"));
	if (DoorMeshObject.Succeeded())
	{
		Door2->SetStaticMesh(DoorMeshObject.Object);
	}

	Door2_Handle1 = CreateDefaultSubobject<UStaticMeshComponent>("Door2_Handle1");
	Door2_Handle1->SetupAttachment(Door2, FName("Handle_1"));
	Door2_Handle1->SetCollisionProfileName(InteractableCollisionProfileName);

	Door2_Handle2 = CreateDefaultSubobject<UStaticMeshComponent>("Door2_Handle2");
	Door2_Handle2->SetupAttachment(Door2, FName("Handle_2"));
	Door2_Handle2->SetCollisionProfileName(InteractableCollisionProfileName);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> DoorHandleMeshObject(TEXT("/Game/PropHunt/Environment/Doors/SM_DoorHandle_01"));
	if (DoorMeshObject.Succeeded())
	{
		Door2_Handle1->SetStaticMesh(DoorHandleMeshObject.Object);
		Door2_Handle2->SetStaticMesh(DoorHandleMeshObject.Object);
	}

	Door2_Trigger = CreateDefaultSubobject<UBoxComponent>("Door2_Trigger");
	Door2_Trigger->SetupAttachment(Door2);
	Door2_Trigger->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Door2_Trigger->SetCollisionResponseToAllChannels(ECR_Ignore);
	Door2_Trigger->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	if (HasAuthority())
	{
		Door2_Trigger->OnComponentBeginOverlap.AddDynamic(this, &ADoor::OnDoorBeginOverlap);
	}
}

bool ADoubleDoors::IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const
{
	const bool bAvailable = Super::IsAvailableForInteraction_Implementation(InteractingActor,InteractionComponent);
	if (!IsMoving() && (bAvailable || InteractionComponent == Door2 || InteractionComponent == Door2_Handle1 || InteractionComponent == Door2_Handle2))
	{
		return true;
	}
	return false;
}

void ADoubleDoors::BeginPlay()
{
	Super::BeginPlay();

	AdjustDoorTrigger(Door2, Door2_Trigger);
}

void ADoubleDoors::SetDoorRotation(float AngleFactor)
{
	Super::SetDoorRotation(AngleFactor);

	Door2->SetRelativeRotation(FRotator(0.f, -OpenAngle * AngleFactor, 0.f));
}

void ADoubleDoors::SetHandleRotation(float AngleFactor)
{
	Super::SetHandleRotation(AngleFactor);

	Door2_Handle1->SetRelativeRotation(FRotator(0.f, HandleOpenAngle * AngleFactor, 0.f));
	Door2_Handle2->SetRelativeRotation(FRotator(0.f, -HandleOpenAngle * AngleFactor, 0.f));
}
