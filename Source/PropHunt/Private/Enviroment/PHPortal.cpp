// Copyright Art-Sun Games. All Rights Reserved.

#include "Enviroment/PHPortal.h"

#include "Core/Online/PHGameState.h"
#include "Core/GameModes/GameMode_Escape.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/Abilities/PHAbilityActorAttributeSet.h"
#include "Character/Prop/PropCharacter.h"


APHPortal::APHPortal()
{
	bReplicates = true;
}

void APHPortal::BeginPlay()
{
	Super::BeginPlay();

	SetUp();
}

void APHPortal::HandlePortalEnergyChanged(AController* InInstigator, float DeltaValue)
{
	if (HasAuthority())
	{
		if (GetEnergy() > GetMaxEnergy())
		{
			OnCharged();
			return;
		}

		if (auto GM = GetWorld()->GetAuthGameMode<AGameMode_Escape>())
		{
			GM->EnergyGiven(InInstigator, DeltaValue);
		}
	}
}

float APHPortal::GetEnergy() const
{
	return AttributeSet->GetPortalEnergy();
}

float APHPortal::GetMaxEnergy() const
{
	return AttributeSet->GetMaxPortalEnergy();
}

bool APHPortal::IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const
{
	return InteractingActor && InteractingActor->IsA(APropCharacter::StaticClass()) && GetEnergy() < GetMaxEnergy();
}

void APHPortal::PostInteract_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent)
{
	if (auto InteractingPropCharacter = Cast<APropCharacter>(InteractingActor))
	{
		UAbilitySystemComponent* const ASC = InteractingPropCharacter->GetAbilitySystemComponent();
		if (ASC)
		{
			FGameplayEventData EventData;
			EventData.EventTag = ChargeEventTag;
			EventData.Instigator = this;
			EventData.Target = EventData.Instigator;

			FScopedPredictionWindow NewScopedWindow(ASC, true);
			ASC->HandleGameplayEvent(EventData.EventTag, &EventData);
		}
	}
}

FText APHPortal::GetInteractionHint_Implementation(UPrimitiveComponent* InteractionComponent) const
{
	return NSLOCTEXT("Portal", "Charge", "Charge <Hightlight>[{0}]</>");;
}

void APHPortal::SetUp()
{
	if (auto GS = GetWorld()->GetGameState<APHGameState>())
	{
		AttributeSet->SetMaxPortalEnergy(GS->NeedPortalEnergy);
		UpdateSize();
	}
	else
	{
		// Keep retrying until Game State is ready
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_SetUpPortal, this, &APHPortal::SetUp, 0.2f, false);
	}
}

void APHPortal::OnCharged()
{
	check(AbilitySystemComponent);
	AbilitySystemComponent->ApplyGameplayEffectToSelf(Cast<UGameplayEffect>(ChargedEffect->GetDefaultObject()), 1, AbilitySystemComponent->MakeEffectContext());

	if (auto GM = GetWorld()->GetAuthGameMode<AGameMode_Escape>())
	{
		GM->PortalCharged();
	}
}
