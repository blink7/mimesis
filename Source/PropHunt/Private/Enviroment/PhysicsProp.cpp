// Copyright Art-Sun Games. All Rights Reserved.

#include "Enviroment/PhysicsProp.h"

#include "Core/PHTypes.h"
#include "Core/PHUtilities.h"

#include "Net/UnrealNetwork.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "Components/BillboardComponent.h"
#include "Components/StaticMeshComponent.h"


APhysicsProp::APhysicsProp()
{
	bReplicates = true;
	bStaticMeshReplicateMovement = true;

	SetMobility(EComponentMobility::Movable);
	GetStaticMeshComponent()->SetSimulatePhysics(true);
	
	GetStaticMeshComponent()->SetCollisionProfileName("Interactable");
	GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetStaticMeshComponent()->ComponentTags.Add(PropHunt::Utilities::CollisionShapeToName(ECollisionShape::Box));
	GetStaticMeshComponent()->SetUseCCD(true);
	GetStaticMeshComponent()->SetCustomDepthStencilValue(2);

	/*const auto BI = GetStaticMeshComponent()->GetBodyInstance();
	if (BI)
	{
		BI->bStartAwake = false;
	}*/

	static ConstructorHelpers::FObjectFinder<UStaticMesh> OutlineMeshObject(TEXT("/Engine/BasicShapes/Cube"));
	if (OutlineMeshObject.Succeeded())
	{
		OutlineMesh = OutlineMeshObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> OutlineMaterialObject(TEXT("/Game/PropHunt/Effects/MI_LocalOutlines_ItemOutline"));
	if (OutlineMaterialObject.Succeeded())
	{
		OutlineMaterial = OutlineMaterialObject.Object;
	}

	PropIcon = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("PropIcon"));

#if WITH_EDITORONLY_DATA
	if (!IsRunningCommandlet())
	{
		struct FConstructorStatics
		{
			ConstructorHelpers::FObjectFinderOptional<UTexture2D> IconTextureObject;
			FName ID_Icon;
			FText NAME_Icon;
			FConstructorStatics()
				: IconTextureObject(TEXT("/Engine/EditorResources/EmptyActor")), 
				ID_Icon(TEXT("PhysicsItem")), 
				NAME_Icon(NSLOCTEXT("SpriteCategory", "PhysicsItem", "PhysicsItem"))
			{}
		};
		static FConstructorStatics ConstructorStatics;

		if (PropIcon)
		{
			PropIcon->Sprite = ConstructorStatics.IconTextureObject.Get();
			PropIcon->SetRelativeScale3D(FVector(0.5f));
			PropIcon->bHiddenInGame = true;
			PropIcon->SpriteInfo.Category = ConstructorStatics.ID_Icon;
			PropIcon->SpriteInfo.DisplayName = ConstructorStatics.NAME_Icon;
			PropIcon->SetupAttachment(GetRootComponent());
			PropIcon->SetUsingAbsoluteScale(true);
			PropIcon->bIsScreenSizeScaled = true;
		}
	}
#endif // WITH_EDITORONLY_DATA
}

void APhysicsProp::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(APhysicsProp, PropState, COND_InitialOnly);
	DOREPLIFETIME(APhysicsProp, PickedUpBy);
}

#if WITH_EDITOR
void APhysicsProp::OnConstruction(const FTransform& Transform)
{
	if (PropIcon)
	{
		PropIcon->SetVisibility(GetStaticMeshComponent()->GetStaticMesh() ? false : true);
	}
}
#endif

void APhysicsProp::OnRep_ReplicatedPropState()
{
	UpdateState();
}

void APhysicsProp::UpdateState()
{
	UStaticMeshComponent* MeshComp = GetStaticMeshComponent();
	if (PropState.ItemMesh)
	{
		MeshComp->SetStaticMesh(PropState.ItemMesh);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s: Static Mesh is NULL."), *GetName());
		return;
	}

	if (PropState.ItemMaterial)
	{
		MeshComp->SetMaterial(0, PropState.ItemMaterial);
	}

	MeshComp->ComponentTags.Empty(1);
	MeshComp->ComponentTags.Add(FName(*PropState.ShapeType));

	if (!FMath::IsNearlyEqual(PropState.MassScale, 1.f))
	{
		if (FBodyInstance* BodyInst = MeshComp->GetBodyInstance())
		{
			BodyInst->MassScale = PropState.MassScale;
			BodyInst->UpdateMassProperties();
		}
	}
}

void APhysicsProp::SetState(FPropState NewState)
{
	PropState = NewState;
	UpdateState();
}

void APhysicsProp::PickUp(AActor* Actor)
{
	if (HasAuthority())
	{
		PickedUpBy = Actor;
	}
}

bool APhysicsProp::IsBusy() const
{
	return PickedUpBy != nullptr;
}

void APhysicsProp::ShowOutline(bool bEnable)
{
	if (bEnable)
	{
		if (!OutlineMeshComponent)
		{
			OutlineMeshComponent = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass(), "Outline");
			if (OutlineMeshComponent)
			{
				OutlineMeshComponent->CreationMethod = EComponentCreationMethod::UserConstructionScript;
				BlueprintCreatedComponents.Add(OutlineMeshComponent);
				OutlineMeshComponent->OnComponentCreated();
				OutlineMeshComponent->SetStaticMesh(OutlineMesh);
				OutlineMeshComponent->SetMaterial(0, OutlineMaterial);
				OutlineMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				OutlineMeshComponent->SetupAttachment(GetRootComponent());

				if (OutlineMeshComponent->bAutoRegister)
				{
					OutlineMeshComponent->RegisterComponent();
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Failed to create Outline mesh!"));
				return;
			}
		}
		else
		{
			OutlineMeshComponent->SetVisibility(true);
		}

		const FVector BoxExtent = OutlineMesh->GetBoundingBox().GetExtent();
		const FVector PropExtent = GetStaticMeshComponent()->GetStaticMesh()->GetBoundingBox().GetExtent();

		FTransform NewTransform;
		NewTransform.SetLocation({0.f, 0.f, PropExtent.Z });
		NewTransform.SetScale3D((PropExtent + 5.f) / BoxExtent);
		OutlineMeshComponent->SetRelativeTransform(NewTransform);
	}
	else if (OutlineMeshComponent)
	{
		OutlineMeshComponent->SetVisibility(false);
	}

	GetStaticMeshComponent()->SetRenderCustomDepth(bEnable);
}
