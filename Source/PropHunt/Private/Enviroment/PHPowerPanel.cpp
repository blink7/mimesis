// Copyright Art-Sun Games. All Rights Reserved.

#include "Enviroment/PHPowerPanel.h"

#include "Character/Prop/PropCharacter.h"
#include "Core/GameModes/GameMode_Escape.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/Abilities/PHAbilityActorAttributeSet.h"

#include "AbilitySystemComponent.h"
#include "Components/BoxComponent.h"


APHPowerPanel::APHPowerPanel()
{
	DischargeableArea = CreateDefaultSubobject<UBoxComponent>("DischargeableArea");
	DischargeableArea->SetCollisionProfileName(InteractableCollisionProfileName);
	DischargeableArea->SetupAttachment(GetRootComponent());
}

void APHPowerPanel::HandlePowerPanelEnergyChanged(AController* InInstigator, float DeltaValue)
{
	if (HasAuthority())
	{
		if (FMath::IsNearlyZero(GetEnergy(), 0.01f))
		{
			OnBroke();
		}

		if (auto GM = GetWorld()->GetAuthGameMode<AGameMode_Escape>())
		{
			GM->EnergyReceived(InInstigator, -DeltaValue);
		}
	}
}

void APHPowerPanel::PostInteract_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent)
{
	if (InteractionComponent == DischargeableArea)
	{
		if (auto InteractingPropCharacter = Cast<APropCharacter>(InteractingActor))
		{
			ProcessInteractingCharacter(InteractingPropCharacter);
		}
	}
	else
	{
		return Super::PostInteract_Implementation(InteractingActor, InteractionComponent);
	}
}

FText APHPowerPanel::GetInteractionHint_Implementation(UPrimitiveComponent* InteractionComponent) const
{
	if (InteractionComponent == DischargeableArea)
	{
		return NSLOCTEXT("PowerPanel", "Discharge", "Discharge <Hightlight>[{0}]</>");
	}
	
	return Super::GetInteractionHint_Implementation(InteractionComponent);
}

bool APHPowerPanel::IsAvailableForInteraction_Implementation(AActor* InteractingActor, UPrimitiveComponent* InteractionComponent) const
{
	check(AbilitySystemComponent);
	const bool bEnabled = AbilitySystemComponent->HasMatchingGameplayTag(EnabledTag);
	if (!bEnabled)
	{
		return false;
	}

	if (InteractionComponent == DischargeableArea && IsOpen() && !IsMoving())
	{
		if (GetEnergy() > 0.f && InteractingActor && InteractingActor->IsA(APropCharacter::StaticClass()))
		{
			return true;
		}
	}

	return Super::IsAvailableForInteraction_Implementation(InteractingActor, InteractionComponent);
}

float APHPowerPanel::GetEnergy() const
{
	return AttributeSet->GetPowerPanelEnergy();
}

void APHPowerPanel::ProcessInteractingCharacter(APropCharacter* PropCharacter)
{
	UAbilitySystemComponent* const ASC = PropCharacter->GetAbilitySystemComponent();
	if (ASC)
	{
		FGameplayEventData EventData;
		EventData.EventTag = DischargeEventTag;
		EventData.Instigator = this;
		EventData.Target = EventData.Instigator;

		FScopedPredictionWindow NewScopedWindow(ASC, true);
		ASC->HandleGameplayEvent(EventData.EventTag, &EventData);
	}
}
