// Fill out your copyright notice in the Description page of Project Settings.

#include "Enviroment/PerformersController.h"
#include "Enviroment/ActionPerformer.h"
#include "EngineUtils.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/BillboardComponent.h"


APerformersController::APerformersController()
{
	ControllerIcon = CreateEditorOnlyDefaultSubobject<UBillboardComponent>("ControllerIcon");

#if WITH_EDITORONLY_DATA
	if (!IsRunningCommandlet())
	{
		struct FConstructorStatics
		{
			ConstructorHelpers::FObjectFinderOptional<UTexture2D> IconTextureObject;
			FName ID_Icon;
			FText NAME_Icon;
			FConstructorStatics()
				: IconTextureObject(TEXT("/Engine/EditorResources/S_Actor"))
				, ID_Icon(TEXT("PerformersController"))
				, NAME_Icon(NSLOCTEXT("SpriteCategory", "PerformersController", "PerformersController"))
			{}
		};
		static FConstructorStatics ConstructorStatics;

		if (ControllerIcon)
		{
			ControllerIcon->Sprite = ConstructorStatics.IconTextureObject.Get();
			ControllerIcon->bHiddenInGame = true;
			ControllerIcon->SpriteInfo.Category = ConstructorStatics.ID_Icon;
			ControllerIcon->SpriteInfo.DisplayName = ConstructorStatics.NAME_Icon;
			ControllerIcon->SetupAttachment(GetRootComponent());
			ControllerIcon->SetUsingAbsoluteScale(true);
			ControllerIcon->bIsScreenSizeScaled = true;
			ControllerIcon->SetRelativeLocation({ 0.f, 0.f, 10.f });
		}
	}
#endif // WITH_EDITORONLY_DATA
}

void APerformersController::OnPerformAction(bool bEnable)
{
	for (TActorIterator<AActionPerformer> It(GetWorld()); It; ++It)
	{
		(*It)->OnPerformAction(bEnable);
	}
}
