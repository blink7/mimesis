// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/PHPlayerCameraManager.h"


namespace ECameraMode
{
	const FName NAME_ThirdPerson = FName(TEXT("ThirdPerson"));
	const FName NAME_FirstPerson = FName(TEXT("FirstPerson"));
}

APHPlayerCameraManager::APHPlayerCameraManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ViewPitchMin = -87.f;
	ViewPitchMax = 87.f;
	FirstPersonViewPitchMin = -75.f;
	FirstPersonViewPitchMax = 88.f;
	ThirtPersonViewPitchMin = -70.f;
	ThirtPersonViewPitchMax = 45.f;
	bAlwaysApplyModifiers = true;
}

void APHPlayerCameraManager::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	static const auto CVarFirstPersonFOV = IConsoleManager::Get().FindConsoleVariable(TEXT("r.FirstPersonFOV"));
	CVarFirstPersonFOV->SetOnChangedCallback(FConsoleVariableDelegate::CreateUObject(this, &APHPlayerCameraManager::OnChangeFirstPersonFOV));
	SetFirstPersonFOV(CVarFirstPersonFOV->GetFloat());

	static const auto CVarThirdPersonFOV = IConsoleManager::Get().FindConsoleVariable(TEXT("r.ThirdPersonFOV"));
	CVarThirdPersonFOV->SetOnChangedCallback(FConsoleVariableDelegate::CreateUObject(this, &APHPlayerCameraManager::OnChangeThirdPersonFOV));
	SetThirdPersonFOV(CVarThirdPersonFOV->GetFloat());
}

void APHPlayerCameraManager::SetFirstPersonFOV(float InFOV)
{
	DefaultFirstPersonFOV = FMath::Clamp(InFOV, 70.f, 120.f);
	UpdateCameraSettings(CameraMode);
}

void APHPlayerCameraManager::SetThirdPersonFOV(float InFOV)
{
	DefaultThirdPersonFOV = FMath::Clamp(InFOV, 70.f, 130.f);
	UpdateCameraSettings(CameraMode);
}

void APHPlayerCameraManager::UpdateCameraSettings(FName InCameraMode)
{
	CameraMode = InCameraMode;

	if (CameraMode == ECameraMode::NAME_ThirdPerson)
	{
		ViewPitchMin = ThirtPersonViewPitchMin;
		ViewPitchMax = ThirtPersonViewPitchMax;

		SetFOV(DefaultThirdPersonFOV);
	}
	else if (CameraMode == ECameraMode::NAME_FirstPerson)
	{
		ViewPitchMin = FirstPersonViewPitchMin;
		ViewPitchMax = FirstPersonViewPitchMax;

		SetFOV(DefaultFirstPersonFOV);
	}
	else
	{
		auto DefaultCameraManager = GetDefault<APHPlayerCameraManager>();
		ViewPitchMin = DefaultCameraManager->ViewPitchMin;
		ViewPitchMax = DefaultCameraManager->ViewPitchMax;

		UnlockFOV();
	}
}

void APHPlayerCameraManager::OnChangeFirstPersonFOV(IConsoleVariable* Var)
{
	SetFirstPersonFOV(Var->GetFloat());
}

void APHPlayerCameraManager::OnChangeThirdPersonFOV(IConsoleVariable* Var)
{
	SetThirdPersonFOV(Var->GetFloat());
}
