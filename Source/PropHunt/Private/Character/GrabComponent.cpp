// Copyright Art-Sun Games. All Rights Reserved.

#include "Character/GrabComponent.h"

#include "Enviroment/PhysicsProp.h"
#include "Character/PHCharacterBase.h"

#include "Engine/World.h"
#include "Net/UnrealNetwork.h"
#include "Components/CapsuleComponent.h"
#include "Components/PrimitiveComponent.h"


DEFINE_LOG_CATEGORY(LogGrabComponent);

UGrabComponent::UGrabComponent() : 
	MaxMass(40.f),
	ThrowImpulse(1000.f),
	HoldDistance(140.f),
	MinHoldDistance(80.f),
	MaxHoldDistance(180.f),
	MaxDropVelocity(600.f),
	CustomYawRate(4.f),
	DistanceStep(10.f)
{
	PrimaryComponentTick.bCanEverTick = true;

	bInterpolateTarget = false;
	SetLinearDamping(100.f);
	SetLinearStiffness(1400.f);
	SetAngularDamping(200.f);
	SetAngularStiffness(2500.f);

	SetIsReplicatedByDefault(true);
}

void UGrabComponent::BeginPlay()
{
	Super::BeginPlay();

	CharacterOwner = Cast<APHCharacterBase>(GetOwner());
}

void UGrabComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetOwnerRole() > ROLE_SimulatedProxy)
	{
		if (CharacterOwner->IsLocallyControlled())
		{
			CreateMove(DeltaTime);
		}
		else if (GetOwner()->GetRemoteRole() == ROLE_AutonomousProxy)
		{
			// Server with remote Client (Simulation)
			SimulatedTick(DeltaTime);
		}
	}
	else if (GetOwnerRole() == ROLE_SimulatedProxy)
	{
		// Simulation
		SimulatedTick(DeltaTime);
	}
}

void UGrabComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UGrabComponent, Move, COND_SimulatedOnly);
	DOREPLIFETIME_CONDITION(UGrabComponent, GrabbingData, COND_SimulatedOnly);
}

void UGrabComponent::SimulatedTick(float DeltaTime)
{
	// Just connected player should see picked up item
	if (GrabbingData.PickedUpComponent && !GrabbedComponent)
	{
		PerformGrab(GrabbingData.PickedUpComponent);
	}
	// Move picked up item
	else if (GrabbingData.PickedUpComponent && GrabbedComponent && !Move.IsEmpty())
	{
		SetTargetLocationAndRotation(Move.TargetLocation, Move.TargetRotation);
	}
	// Drop item
	else if (!GrabbingData.PickedUpComponent && GrabbedComponent)
	{
		if (GrabbingData.Impulse != FVector_NetQuantize::ZeroVector)
		{
			GrabbedComponent->AddImpulse(GrabbingData.Impulse);
		}

		PerformDrop();

		Move = FGrabbingMove();
	}
}

void UGrabComponent::CreateMove(float DeltaTime)
{
	if (!bHoldItem || !GrabbedComponent)
	{
		return;
	}

	// When we stand on the picked up item it must be dropped to prevent weird behaviors
	if (CharacterOwner->GetStandOnComponent() == GrabbedComponent)
	{
		Drop();
		return;
	}

	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	CharacterOwner->GetPlayerViewPoint(PlayerViewPointLocation, PlayerViewPointRotation);

	const float CurrentDistanceToItem = (PlayerViewPointLocation - GrabbedComponent->GetCenterOfMass()).Size();

	if (CurrentDistanceToItem > (MaxHoldDistance + 30.f)) // plus safety factor
	{
		Drop();
		return;
	}

	PlayerViewPointRotation.Pitch = FMath::ClampAngle(PlayerViewPointRotation.Pitch, -50.f, 90.f);

	const FVector TargetLocation = PlayerViewPointLocation + PlayerViewPointRotation.Vector() * HoldDistance;
	const FRotator TargetRotation = { 0.f, PlayerViewPointRotation.Yaw + 180.f + CustomYawRotation, 0.f };  // add 180 degrees to position the picked up item face to face
	SetTargetLocationAndRotation(TargetLocation, TargetRotation);

	FGrabbingMove NewMove;
	NewMove.TargetLocation = TargetLocation;
	NewMove.TargetRotation = TargetRotation;

	// We are local server
	if (GetOwnerRole() == ROLE_Authority)
	{
		SetMove(NewMove);
	}
	// Local client
	else if (GetOwnerRole() == ROLE_AutonomousProxy && IsNetMode(NM_Client))
	{
		ServerSendMove(NewMove);
	}
}

void UGrabComponent::SetMove(const FGrabbingMove& NewMove)
{
	Move = NewMove;
}

bool UGrabComponent::Pickup(UPrimitiveComponent* ComponentToGrab)
{
	if (!ComponentToGrab || ComponentToGrab->GetMass() > MaxMass)
	{
		return false;
	}

	auto PhysicsProp = Cast<APhysicsProp>(ComponentToGrab->GetOwner());
	if (PhysicsProp && PhysicsProp->IsBusy())
	{
		return false;
	}

	bHoldItem = true;

	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	CharacterOwner->GetPlayerViewPoint(PlayerViewPointLocation, PlayerViewPointRotation);

	DistanceToItem = (PlayerViewPointLocation - ComponentToGrab->GetCenterOfMass()).Size();

	PerformGrab(ComponentToGrab);

	FGrabbingData NewGrabbing;
	NewGrabbing.PickedUpComponent = ComponentToGrab;
	GrabbingData = NewGrabbing;

	return true;
}

void UGrabComponent::Drop(bool bThrow /*= false*/)
{
	if (GrabbedComponent)
	{
		FGrabbingData NewGrabbing;

		if (bThrow)
		{
			FVector PlayerViewPointLocation;
			FRotator PlayerViewPointRotation;
			CharacterOwner->GetPlayerViewPoint(PlayerViewPointLocation, PlayerViewPointRotation);

			const FVector Impulse = PlayerViewPointRotation.Vector() * ThrowImpulse * GrabbedComponent->GetMass();
			GrabbedComponent->AddImpulse(Impulse);
			NewGrabbing.Impulse = Impulse;
		}

		GrabbingData = NewGrabbing;

		PerformDrop();
	}

	Move = FGrabbingMove();
	bHoldItem = false;
	bRotateItem = false;
	CustomYawRotation = 0.f;
	HoldDistance = GetDefault<UGrabComponent>()->HoldDistance;
}

void UGrabComponent::AddYawRotation(float Value)
{
	CustomYawRotation -= Value * CustomYawRate;
}

void UGrabComponent::AdjustHoldDistance(bool bIncrease)
{
	const float NewDistance = bIncrease ? HoldDistance + DistanceStep : HoldDistance - DistanceStep;
	HoldDistance = FMath::Clamp(NewDistance, MinHoldDistance, MaxHoldDistance);
}

void UGrabComponent::PerformGrab(UPrimitiveComponent* ComponentToGrab)
{
	GrabComponentAtLocationWithRotation(
		ComponentToGrab,
		NAME_None,
		ComponentToGrab->GetCenterOfMass(),
		ComponentToGrab->GetComponentRotation());

	if (GetOwnerRole() == ROLE_Authority)
	{
		if (auto PickedUpItem = Cast<APhysicsProp>(ComponentToGrab->GetOwner()))
		{
			PickedUpItem->PickUp(CharacterOwner);
		}
	}
}

void UGrabComponent::PerformDrop()
{
// 	ReleaseComponent();

	if (GetOwnerRole() == ROLE_Authority)
	{
		if (auto PickedUpItem = Cast<APhysicsProp>(GrabbedComponent->GetOwner()))
		{
			PickedUpItem->PickUp(nullptr);

			const FVector& Velocity = GrabbedComponent->GetPhysicsLinearVelocity();
			GrabbedComponent->SetPhysicsLinearVelocity(Velocity.GetClampedToMaxSize(MaxDropVelocity));
		}
	}

	ReleaseComponent();
}

void UGrabComponent::ServerSendMove_Implementation(FGrabbingMove NewMove)
{
	SetMove(NewMove);
}

bool UGrabComponent::ServerSendMove_Validate(FGrabbingMove NewMove)
{
	return true;
}
