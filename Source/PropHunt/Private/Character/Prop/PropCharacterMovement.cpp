// Copyright Art-Sun Games. All Rights Reserved.

#include "Character/Prop/PropCharacterMovement.h"

#include "Character/Prop/ShapeExtent.h"
#include "Character/Prop/PropCharacter.h"

#include "EngineLogs.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Character.h"
#include "AI/Navigation/AvoidanceManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogPropCharacterMovement, Log, All);

/**
 * Character stats
 */
DECLARE_CYCLE_STAT(TEXT("PropChar CallServerMove"), STAT_PropCharacterMovementCallServerMove, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar ReplicateMoveToServer"), STAT_PropCharacterMovementReplicateMoveToServer, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar ClientUpdatePositionAfterServerUpdate"), STAT_PropCharacterMovementClientUpdatePositionAfterServerUpdate, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar CombineNetMove"), STAT_PropCharacterMovementCombineNetMove, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar StepUp"), STAT_PropCharStepUp, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar FindFloor"), STAT_PropCharFindFloor, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar PhysFalling"), STAT_PropCharPhysFalling, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar AI ObstacleAvoidance"), STAT_PropCharObstacleAvoidance, STATGROUP_PropCharacter);
DECLARE_CYCLE_STAT(TEXT("PropChar Avoidance Time"), STAT_AI_PropCharObstacleAvoidance, STATGROUP_AI);

// MAGIC NUMBERS
const float MAX_STEP_SIDE_Z = 0.08f;	// maximum z value for the normal on the vertical side of steps
const float VERTICAL_SLOPE_NORMAL_Z = 0.001f; // Slope is vertical if Abs(Normal.Z) <= this threshold. Accounts for precision problems that sometimes angle normals slightly off horizontal for vertical surface.

/**
 * Helper to change mesh bone updates within a scope.
 * Example usage:
 *	{
 *		FScopedPreventMeshBoneUpdate ScopedNoMeshBoneUpdate(CharacterOwner->GetMesh(), EKinematicBonesUpdateToPhysics::SkipAllBones);
 *		// Do something to move mesh, bones will not update
 *	}
 *	// Movement of mesh at this point will use previous setting.
 */
struct FScopedMeshBoneUpdateOverride
{
	FScopedMeshBoneUpdateOverride(USkeletalMeshComponent* Mesh, EKinematicBonesUpdateToPhysics::Type OverrideSetting) : 
		MeshRef(Mesh)
	{
		if (MeshRef)
		{
			// Save current state.
			SavedUpdateSetting = MeshRef->KinematicBonesUpdateType;
			// Override bone update setting.
			MeshRef->KinematicBonesUpdateType = OverrideSetting;
		}
	}

	~FScopedMeshBoneUpdateOverride()
	{
		if (MeshRef)
		{
			// Restore bone update flag.
			MeshRef->KinematicBonesUpdateType = SavedUpdateSetting;
		}
	}

private:
	USkeletalMeshComponent* MeshRef;
	EKinematicBonesUpdateToPhysics::Type SavedUpdateSetting;
};


UPropCharacterMovement::UPropCharacterMovement() : 
	FaceRotationRate(20.f)
{
}

void UPropCharacterMovement::Serialize(FArchive& Archive)
{
	Super::Serialize(Archive);

	if (Archive.IsLoading() && Archive.UE4Ver() < VER_UE4_ADDED_SWEEP_WHILE_WALKING_FLAG)
	{
		// We need to update the bSweepWhileNavWalking flag to match the previous behavior.
		// Since UpdatedComponent is transient, we'll have to wait until we're registered.
		bNeedsSweepWhileWalkingUpdate = true;
	}
}

void UPropCharacterMovement::PostLoad()
{
	Super::PostLoad();

	PropCharacterOwner = Cast<APropCharacter>(CharacterOwner);
}

void UPropCharacterMovement::UpdateBasedMovement(float DeltaSeconds)
{
	if (!HasValidData())
	{
		return;
	}

	const UPrimitiveComponent* MovementBase = CharacterOwner->GetMovementBase();
	if (!MovementBaseUtility::UseRelativeLocation(MovementBase))
	{
		return;
	}

	if (!IsValid(MovementBase) || !IsValid(MovementBase->GetOwner()))
	{
		SetBase(NULL);
		return;
	}

	// Ignore collision with bases during these movements.
	TGuardValue<EMoveComponentFlags> ScopedFlagRestore(MoveComponentFlags, MoveComponentFlags | MOVECOMP_IgnoreBases);

	FQuat DeltaQuat = FQuat::Identity;
	FVector DeltaPosition = FVector::ZeroVector;

	FQuat NewBaseQuat;
	FVector NewBaseLocation;
	if (!MovementBaseUtility::GetMovementBaseTransform(MovementBase, CharacterOwner->GetBasedMovement().BoneName, NewBaseLocation, NewBaseQuat))
	{
		return;
	}

	// Find change in rotation
	const bool bRotationChanged = !OldBaseQuat.Equals(NewBaseQuat, 1e-8f);
	if (bRotationChanged)
	{
		DeltaQuat = NewBaseQuat * OldBaseQuat.Inverse();
	}

	// only if base moved
	if (bRotationChanged || (OldBaseLocation != NewBaseLocation))
	{
		// Calculate new transform matrix of base actor (ignoring scale).
		const FQuatRotationTranslationMatrix OldLocalToWorld(OldBaseQuat, OldBaseLocation);
		const FQuatRotationTranslationMatrix NewLocalToWorld(NewBaseQuat, NewBaseLocation);

		if (CharacterOwner->IsMatineeControlled())
		{
			FRotationTranslationMatrix HardRelMatrix(CharacterOwner->GetBasedMovement().Rotation, CharacterOwner->GetBasedMovement().Location);
			const FMatrix NewWorldTM = HardRelMatrix * NewLocalToWorld;
			const FQuat NewWorldRot = bIgnoreBaseRotation ? UpdatedComponent->GetComponentQuat() : NewWorldTM.ToQuat();
			MoveUpdatedComponent(NewWorldTM.GetOrigin() - UpdatedComponent->GetComponentLocation(), NewWorldRot, true);
		}
		else
		{
			FQuat FinalQuat = UpdatedComponent->GetComponentQuat();

			if (bRotationChanged && !bIgnoreBaseRotation)
			{
				// Apply change in rotation and pipe through FaceRotation to maintain axis restrictions
				const FQuat PawnOldQuat = UpdatedComponent->GetComponentQuat();
				const FQuat TargetQuat = DeltaQuat * FinalQuat;
				FRotator TargetRotator(TargetQuat);
				CharacterOwner->FaceRotation(TargetRotator, 0.f);
				FinalQuat = UpdatedComponent->GetComponentQuat();

				if (PawnOldQuat.Equals(FinalQuat, 1e-6f))
				{
					// Nothing changed. This means we probably are using another rotation mechanism (bOrientToMovement etc). We should still follow the base object.
					// @todo: This assumes only Yaw is used, currently a valid assumption. This is the only reason FaceRotation() is used above really, aside from being a virtual hook.
					if (bOrientRotationToMovement || (bUseControllerDesiredRotation && CharacterOwner->Controller))
					{
						TargetRotator.Pitch = 0.f;
						TargetRotator.Roll = 0.f;
						MoveUpdatedComponent(FVector::ZeroVector, TargetRotator, false);
						FinalQuat = UpdatedComponent->GetComponentQuat();
					}
				}

				// Pipe through ControlRotation, to affect camera.
				if (CharacterOwner->Controller)
				{
					const FQuat PawnDeltaRotation = FinalQuat * PawnOldQuat.Inverse();
					FRotator FinalRotation = FinalQuat.Rotator();
					UpdateBasedRotation(FinalRotation, PawnDeltaRotation.Rotator());
					FinalQuat = UpdatedComponent->GetComponentQuat();
				}
			}

			// We need to offset the base of the character here, not its origin, so offset by half height
			const float HalfHeight = PropCharacterOwner->GetCollisionSize().GetHalfHeight();

			FVector const BaseOffset(0.0f, 0.0f, HalfHeight);
			FVector const LocalBasePos = OldLocalToWorld.InverseTransformPosition(UpdatedComponent->GetComponentLocation() - BaseOffset);
			FVector const NewWorldPos = ConstrainLocationToPlane(NewLocalToWorld.TransformPosition(LocalBasePos) + BaseOffset);
			DeltaPosition = ConstrainDirectionToPlane(NewWorldPos - UpdatedComponent->GetComponentLocation());

			// move attached actor
			if (bFastAttachedMove)
			{
				// we're trusting no other obstacle can prevent the move here
				UpdatedComponent->SetWorldLocationAndRotation(NewWorldPos, FinalQuat, false);
			}
			else
			{
				// hack - transforms between local and world space introducing slight error FIXMESTEVE - discuss with engine team: just skip the transforms if no rotation?
				FVector BaseMoveDelta = NewBaseLocation - OldBaseLocation;
				if (!bRotationChanged && (BaseMoveDelta.X == 0.f) && (BaseMoveDelta.Y == 0.f))
				{
					DeltaPosition.X = 0.f;
					DeltaPosition.Y = 0.f;
				}

				FHitResult MoveOnBaseHit(1.f);
				const FVector OldLocation = UpdatedComponent->GetComponentLocation();
				MoveUpdatedComponent(DeltaPosition, FinalQuat, true, &MoveOnBaseHit);
				if ((UpdatedComponent->GetComponentLocation() - (OldLocation + DeltaPosition)).IsNearlyZero() == false)
				{
					OnUnableToFollowBaseMove(DeltaPosition, OldLocation, MoveOnBaseHit);
				}
			}
		}

		if (MovementBase->IsSimulatingPhysics() && CharacterOwner->GetMesh())
		{
			CharacterOwner->GetMesh()->ApplyDeltaToAllPhysicsTransforms(DeltaPosition, DeltaQuat);
		}
	}
}

FVector UPropCharacterMovement::GetImpartedMovementBaseVelocity() const
{
	FVector Result = FVector::ZeroVector;
	if (CharacterOwner)
	{
		UPrimitiveComponent* MovementBase = CharacterOwner->GetMovementBase();
		if (MovementBaseUtility::IsDynamicBase(MovementBase))
		{
			FVector BaseVelocity = MovementBaseUtility::GetMovementBaseVelocity(MovementBase, CharacterOwner->GetBasedMovement().BoneName);

			if (bImpartBaseAngularVelocity)
			{
				const FVector CharacterBasePosition = (UpdatedComponent->GetComponentLocation() - FVector(0.f, 0.f, PropCharacterOwner->GetCollisionSize().GetHalfHeight()));
				const FVector BaseTangentialVel = MovementBaseUtility::GetMovementBaseTangentialVelocity(MovementBase, CharacterOwner->GetBasedMovement().BoneName, CharacterBasePosition);
				BaseVelocity += BaseTangentialVel;
			}

			if (bImpartBaseVelocityX)
			{
				Result.X = BaseVelocity.X;
			}
			if (bImpartBaseVelocityY)
			{
				Result.Y = BaseVelocity.Y;
			}
			if (bImpartBaseVelocityZ)
			{
				Result.Z = BaseVelocity.Z;
			}
		}
	}

	return Result;
}

bool UPropCharacterMovement::StepUp(const FVector& GravDir, const FVector& Delta, const FHitResult& InHit, struct UCharacterMovementComponent::FStepDownResult* OutStepDownResult /*= NULL*/)
{
	SCOPE_CYCLE_COUNTER(STAT_PropCharStepUp);

	if (!CanStepUp(InHit) || MaxStepHeight <= 0.f)
	{
		return false;
	}

	const FVector OldLocation = UpdatedComponent->GetComponentLocation();
	const FShapeExtent PawnSize = PropCharacterOwner->GetCollisionSize();

	// Don't bother stepping up if top of capsule is hitting something.
	const float InitialImpactZ = InHit.ImpactPoint.Z;
	if (InitialImpactZ > OldLocation.Z + (PawnSize.GetHalfHeight() - PawnSize.GetRadius()))
	{
		return false;
	}

	if (GravDir.IsZero())
	{
		return false;
	}

	// Gravity should be a normalized direction
	ensure(GravDir.IsNormalized());

	float StepTravelUpHeight = MaxStepHeight;
	float StepTravelDownHeight = StepTravelUpHeight;
	const float StepSideZ = -1.f * FVector::DotProduct(InHit.ImpactNormal, GravDir);
	float PawnInitialFloorBaseZ = OldLocation.Z - PawnSize.GetHalfHeight();
	float PawnFloorPointZ = PawnInitialFloorBaseZ;

	if (IsMovingOnGround() && CurrentFloor.IsWalkableFloor())
	{
		// Since we float a variable amount off the floor, we need to enforce max step height off the actual point of impact with the floor.
		const float FloorDist = FMath::Max(0.f, CurrentFloor.GetDistanceToFloor());
		PawnInitialFloorBaseZ -= FloorDist;
		StepTravelUpHeight = FMath::Max(StepTravelUpHeight - FloorDist, 0.f);
		StepTravelDownHeight = (MaxStepHeight + MAX_FLOOR_DIST * 2.f);

		const bool bHitVerticalFace = !IsWithinEdgeTolerance_PH(InHit.Location, InHit.ImpactPoint, PawnSize);
		if (!CurrentFloor.bLineTrace && !bHitVerticalFace)
		{
			PawnFloorPointZ = CurrentFloor.HitResult.ImpactPoint.Z;
		}
		else
		{
			// Base floor point is the base of the capsule moved down by how far we are hovering over the surface we are hitting.
			PawnFloorPointZ -= CurrentFloor.FloorDist;
		}
	}

	// Don't step up if the impact is below us, accounting for distance from floor.
	if (InitialImpactZ <= PawnInitialFloorBaseZ)
	{
		return false;
	}

	// Scope our movement updates, and do not apply them until all intermediate moves are completed.
	FScopedMovementUpdate ScopedStepUpMovement(UpdatedComponent, EScopedUpdate::DeferredUpdates);

	// step up - treat as vertical wall
	FHitResult SweepUpHit(1.f);
	const FQuat PawnRotation = UpdatedComponent->GetComponentQuat();
	MoveUpdatedComponent(-GravDir * StepTravelUpHeight, PawnRotation, true, &SweepUpHit);

	if (SweepUpHit.bStartPenetrating)
	{
		// Undo movement
		ScopedStepUpMovement.RevertMove();
		return false;
	}

	// step fwd
	FHitResult Hit(1.f);
	MoveUpdatedComponent(Delta, PawnRotation, true, &Hit);

	// Check result of forward movement
	if (Hit.bBlockingHit)
	{
		if (Hit.bStartPenetrating)
		{
			// Undo movement
			ScopedStepUpMovement.RevertMove();
			return false;
		}

		// If we hit something above us and also something ahead of us, we should notify about the upward hit as well.
		// The forward hit will be handled later (in the bSteppedOver case below).
		// In the case of hitting something above but not forward, we are not blocked from moving so we don't need the notification.
		if (SweepUpHit.bBlockingHit && Hit.bBlockingHit)
		{
			HandleImpact(SweepUpHit);
		}

		// pawn ran into a wall
		HandleImpact(Hit);
		if (IsFalling())
		{
			return true;
		}

		// adjust and try again
		const float ForwardHitTime = Hit.Time;
		const float ForwardSlideAmount = SlideAlongSurface(Delta, 1.f - Hit.Time, Hit.Normal, Hit, true);

		if (IsFalling())
		{
			ScopedStepUpMovement.RevertMove();
			return false;
		}

		// If both the forward hit and the deflection got us nowhere, there is no point in this step up.
		if (ForwardHitTime == 0.f && ForwardSlideAmount == 0.f)
		{
			ScopedStepUpMovement.RevertMove();
			return false;
		}
	}

	// Step down
	MoveUpdatedComponent(GravDir * StepTravelDownHeight, UpdatedComponent->GetComponentQuat(), true, &Hit);

	// If step down was initially penetrating abort the step up
	if (Hit.bStartPenetrating)
	{
		ScopedStepUpMovement.RevertMove();
		return false;
	}

	FStepDownResult StepDownResult;
	if (Hit.IsValidBlockingHit())
	{
		// See if this step sequence would have allowed us to travel higher than our max step height allows.
		const float DeltaZ = Hit.ImpactPoint.Z - PawnFloorPointZ;
		if (DeltaZ > MaxStepHeight)
		{
			//UE_LOG(LogCharacterMovement, VeryVerbose, TEXT("- Reject StepUp (too high Height %.3f) up from floor base %f to %f"), DeltaZ, PawnInitialFloorBaseZ, NewLocation.Z);
			ScopedStepUpMovement.RevertMove();
			return false;
		}

		// Reject unwalkable surface normals here.
		if (!IsWalkable(Hit))
		{
			// Reject if normal opposes movement direction
			const bool bNormalTowardsMe = (Delta | Hit.ImpactNormal) < 0.f;
			if (bNormalTowardsMe)
			{
				//UE_LOG(LogCharacterMovement, VeryVerbose, TEXT("- Reject StepUp (unwalkable normal %s opposed to movement)"), *Hit.ImpactNormal.ToString());
				ScopedStepUpMovement.RevertMove();
				return false;
			}

			// Also reject if we would end up being higher than our starting location by stepping down.
			// It's fine to step down onto an unwalkable normal below us, we will just slide off. Rejecting those moves would prevent us from being able to walk off the edge.
			if (Hit.Location.Z > OldLocation.Z)
			{
				//UE_LOG(LogCharacterMovement, VeryVerbose, TEXT("- Reject StepUp (unwalkable normal %s above old position)"), *Hit.ImpactNormal.ToString());
				ScopedStepUpMovement.RevertMove();
				return false;
			}
		}

		// Reject moves where the downward sweep hit something very close to the edge of the capsule. This maintains consistency with FindFloor as well.
		if (!IsWithinEdgeTolerance_PH(Hit.Location, Hit.ImpactPoint, PawnSize))
		{
			//UE_LOG(LogCharacterMovement, VeryVerbose, TEXT("- Reject StepUp (outside edge tolerance)"));
			ScopedStepUpMovement.RevertMove();
			return false;
		}

		// Don't step up onto invalid surfaces if traveling higher.
		if (DeltaZ > 0.f && !CanStepUp(Hit))
		{
			//UE_LOG(LogCharacterMovement, VeryVerbose, TEXT("- Reject StepUp (up onto surface with !CanStepUp())"));
			ScopedStepUpMovement.RevertMove();
			return false;
		}

		// See if we can validate the floor as a result of this step down. In almost all cases this should succeed, and we can avoid computing the floor outside this method.
		if (OutStepDownResult != NULL)
		{
			FindFloor(UpdatedComponent->GetComponentLocation(), StepDownResult.FloorResult, false, &Hit);

			// Reject unwalkable normals if we end up higher than our initial height.
			// It's fine to walk down onto an unwalkable surface, don't reject those moves.
			if (Hit.Location.Z > OldLocation.Z)
			{
				// We should reject the floor result if we are trying to step up an actual step where we are not able to perch (this is rare).
				// In those cases we should instead abort the step up and try to slide along the stair.
				if (!StepDownResult.FloorResult.bBlockingHit && StepSideZ < MAX_STEP_SIDE_Z)
				{
					ScopedStepUpMovement.RevertMove();
					return false;
				}
			}

			StepDownResult.bComputedFloor = true;
		}
	}

	// Copy step down result.
	if (OutStepDownResult != NULL)
	{
		*OutStepDownResult = StepDownResult;
	}

	// Don't recalculate velocity based on this height adjustment, if considering vertical adjustments.
	bJustTeleported |= !bMaintainHorizontalGroundVelocity;

	return true;
}

void UPropCharacterMovement::ApplyRepulsionForce(float DeltaSeconds)
{
	if (UpdatedPrimitive && RepulsionForce > 0.f && CharacterOwner != nullptr)
	{
		const TArray<FOverlapInfo>& Overlaps = UpdatedPrimitive->GetOverlapInfos();
		if (Overlaps.Num() > 0)
		{
			FCollisionQueryParams QueryParams(SCENE_QUERY_STAT(CMC_ApplyRepulsionForce));
			QueryParams.bReturnFaceIndex = false;
			QueryParams.bReturnPhysicalMaterial = false;

			const auto CollisionSize = PropCharacterOwner->GetCollisionSize();
			const float RepulsionForceRadius = CollisionSize.GetRadius() * 1.2f;
			const float StopBodyDistance = 2.5f;
			const FVector MyLocation = UpdatedPrimitive->GetComponentLocation();

			for (int32 i = 0; i < Overlaps.Num(); i++)
			{
				const FOverlapInfo& Overlap = Overlaps[i];

				UPrimitiveComponent* OverlapComp = Overlap.OverlapInfo.Component.Get();
				if (!OverlapComp || OverlapComp->Mobility < EComponentMobility::Movable)
				{
					continue;
				}

				// Use the body instead of the component for cases where we have multi-body overlaps enabled
				FBodyInstance* OverlapBody = nullptr;
				const int32 OverlapBodyIndex = Overlap.GetBodyIndex();
				const USkeletalMeshComponent* SkelMeshForBody = (OverlapBodyIndex != INDEX_NONE) ? Cast<USkeletalMeshComponent>(OverlapComp) : nullptr;
				if (SkelMeshForBody != nullptr)
				{
					OverlapBody = SkelMeshForBody->Bodies.IsValidIndex(OverlapBodyIndex) ? SkelMeshForBody->Bodies[OverlapBodyIndex] : nullptr;
				}
				else
				{
					OverlapBody = OverlapComp->GetBodyInstance();
				}

				if (!OverlapBody)
				{
					UE_LOG(LogPropCharacterMovement, Warning, TEXT("%s could not find overlap body for body index %d"), *GetName(), OverlapBodyIndex);
					continue;
				}

				if (!OverlapBody->IsInstanceSimulatingPhysics())
				{
					continue;
				}

				FTransform BodyTransform = OverlapBody->GetUnrealWorldTransform();

				FVector BodyVelocity = OverlapBody->GetUnrealWorldVelocity();
				FVector BodyLocation = BodyTransform.GetLocation();

				// Trace to get the hit location on the capsule
				FHitResult Hit;
				bool bHasHit = UpdatedPrimitive->LineTraceComponent(Hit, BodyLocation,
					FVector(MyLocation.X, MyLocation.Y, BodyLocation.Z),
					QueryParams);

				FVector HitLoc = Hit.ImpactPoint;
				bool bIsPenetrating = Hit.bStartPenetrating || Hit.PenetrationDepth > StopBodyDistance;

				// If we didn't hit the capsule, we're inside the capsule
				if (!bHasHit)
				{
					HitLoc = BodyLocation;
					bIsPenetrating = true;
				}

				const float DistanceNow = (HitLoc - BodyLocation).SizeSquared2D();
				const float DistanceLater = (HitLoc - (BodyLocation + BodyVelocity * DeltaSeconds)).SizeSquared2D();

				if (bHasHit && DistanceNow < StopBodyDistance && !bIsPenetrating)
				{
					OverlapBody->SetLinearVelocity(FVector(0.f), false);
				}
				else if (DistanceLater <= DistanceNow || bIsPenetrating)
				{
					FVector ForceCenter = MyLocation;

					if (bHasHit)
					{
						ForceCenter.Z = HitLoc.Z;
					}
					else
					{
						ForceCenter.Z = FMath::Clamp(BodyLocation.Z, MyLocation.Z - CollisionSize.GetHalfHeight(), MyLocation.Z + CollisionSize.GetHalfHeight());
					}

					OverlapBody->AddRadialForceToBody(ForceCenter, RepulsionForceRadius, RepulsionForce * Mass, ERadialImpulseFalloff::RIF_Constant);
				}
			}
		}
	}
}

void UPropCharacterMovement::PhysFalling(float deltaTime, int32 Iterations)
{
	SCOPE_CYCLE_COUNTER(STAT_PropCharPhysFalling);
	CSV_SCOPED_TIMING_STAT_EXCLUSIVE(PropCharPhysFalling);

	if (deltaTime < MIN_TICK_TIME)
	{
		return;
	}

	FVector FallAcceleration = GetFallingLateralAcceleration(deltaTime);
	FallAcceleration.Z = 0.f;
	const bool bHasLimitedAirControl = ShouldLimitAirControl(deltaTime, FallAcceleration);

	float remainingTime = deltaTime;
	while ((remainingTime >= MIN_TICK_TIME) && (Iterations < MaxSimulationIterations))
	{
		Iterations++;
		float timeTick = GetSimulationTimeStep(remainingTime, Iterations);
		remainingTime -= timeTick;

		const FVector OldLocation = UpdatedComponent->GetComponentLocation();
		const FQuat PawnRotation = UpdatedComponent->GetComponentQuat();
		bJustTeleported = false;

		RestorePreAdditiveRootMotionVelocity();

		const FVector OldVelocity = Velocity;

		// Apply input
		const float MaxDecel = GetMaxBrakingDeceleration();
		if (!HasAnimRootMotion() && !CurrentRootMotion.HasOverrideVelocity())
		{
			// Compute Velocity
			{
				// Acceleration = FallAcceleration for CalcVelocity(), but we restore it after using it.
				TGuardValue<FVector> RestoreAcceleration(Acceleration, FallAcceleration);
				Velocity.Z = 0.f;
				CalcVelocity(timeTick, FallingLateralFriction, false, MaxDecel);
				Velocity.Z = OldVelocity.Z;
			}
		}

		// Compute current gravity
		const FVector Gravity(0.f, 0.f, GetGravityZ());
		float GravityTime = timeTick;

		// If jump is providing force, gravity may be affected.
		bool bEndingJumpForce = false;
		if (CharacterOwner->JumpForceTimeRemaining > 0.0f)
		{
			// Consume some of the force time. Only the remaining time (if any) is affected by gravity when bApplyGravityWhileJumping=false.
			const float JumpForceTime = FMath::Min(CharacterOwner->JumpForceTimeRemaining, timeTick);
			GravityTime = bApplyGravityWhileJumping ? timeTick : FMath::Max(0.0f, timeTick - JumpForceTime);

			// Update Character state
			CharacterOwner->JumpForceTimeRemaining -= JumpForceTime;
			if (CharacterOwner->JumpForceTimeRemaining <= 0.0f)
			{
				CharacterOwner->ResetJumpState();
				bEndingJumpForce = true;
			}
		}

		// Apply gravity
		Velocity = NewFallVelocity(Velocity, Gravity, GravityTime);

		static const auto ForceJumpPeakSubstepCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("p.ForceJumpPeakSubstep"));
		int32 ForceJumpPeakSubstep = ForceJumpPeakSubstepCVar->GetInt();

		// See if we need to sub-step to exactly reach the apex. This is important for avoiding "cutting off the top" of the trajectory as framerate varies.
		if (ForceJumpPeakSubstep && OldVelocity.Z > 0.f && Velocity.Z <= 0.f && NumJumpApexAttempts < MaxJumpApexAttemptsPerSimulation)
		{
			const FVector DerivedAccel = (Velocity - OldVelocity) / timeTick;
			if (!FMath::IsNearlyZero(DerivedAccel.Z))
			{
				const float TimeToApex = -OldVelocity.Z / DerivedAccel.Z;

				// The time-to-apex calculation should be precise, and we want to avoid adding a substep when we are basically already at the apex from the previous iteration's work.
				const float ApexTimeMinimum = 0.0001f;
				if (TimeToApex >= ApexTimeMinimum && TimeToApex < timeTick)
				{
					const FVector ApexVelocity = OldVelocity + DerivedAccel * TimeToApex;
					Velocity = ApexVelocity;
					Velocity.Z = 0.f; // Should be nearly zero anyway, but this makes apex notifications consistent.

					// We only want to move the amount of time it takes to reach the apex, and refund the unused time for next iteration.
					remainingTime += (timeTick - TimeToApex);
					timeTick = TimeToApex;
					Iterations--;
					NumJumpApexAttempts++;
				}
			}
		}

		//UE_LOG(LogCharacterMovement, Log, TEXT("dt=(%.6f) OldLocation=(%s) OldVelocity=(%s) NewVelocity=(%s)"), timeTick, *(UpdatedComponent->GetComponentLocation()).ToString(), *OldVelocity.ToString(), *Velocity.ToString());
		ApplyRootMotionToVelocity(timeTick);

		if (bNotifyApex && (Velocity.Z < 0.f))
		{
			// Just passed jump apex since now going down
			bNotifyApex = false;
			NotifyJumpApex();
		}

		// Compute change in position (using midpoint integration method).
		FVector Adjusted = 0.5f * (OldVelocity + Velocity) * timeTick;

		// Special handling if ending the jump force where we didn't apply gravity during the jump.
		if (bEndingJumpForce && !bApplyGravityWhileJumping)
		{
			// We had a portion of the time at constant speed then a portion with acceleration due to gravity.
			// Account for that here with a more correct change in position.
			const float NonGravityTime = FMath::Max(0.f, timeTick - GravityTime);
			Adjusted = (OldVelocity * NonGravityTime) + (0.5f * (OldVelocity + Velocity) * GravityTime);
		}

		// Move
		FHitResult Hit(1.f);
		SafeMoveUpdatedComponent(Adjusted, PawnRotation, true, Hit);

		if (!HasValidData())
		{
			return;
		}

		float LastMoveTimeSlice = timeTick;
		float subTimeTickRemaining = timeTick * (1.f - Hit.Time);

		if (IsSwimming()) //just entered water
		{
			remainingTime += subTimeTickRemaining;
			StartSwimming(OldLocation, OldVelocity, timeTick, remainingTime, Iterations);
			return;
		}
		else if (Hit.bBlockingHit)
		{
			if (IsValidLandingSpot(UpdatedComponent->GetComponentLocation(), Hit))
			{
				remainingTime += subTimeTickRemaining;
				ProcessLanded(Hit, remainingTime, Iterations);
				return;
			}
			else
			{
				// Compute impact deflection based on final velocity, not integration step.
				// This allows us to compute a new velocity from the deflected vector, and ensures the full gravity effect is included in the slide result.
				Adjusted = Velocity * timeTick;

				// See if we can convert a normally invalid landing spot (based on the hit result) to a usable one.
				if (!Hit.bStartPenetrating && ShouldCheckForValidLandingSpot(timeTick, Adjusted, Hit))
				{
					const FVector PawnLocation = UpdatedComponent->GetComponentLocation();
					FFindFloorResult FloorResult;
					FindFloor(PawnLocation, FloorResult, false);
					if (FloorResult.IsWalkableFloor() && IsValidLandingSpot(PawnLocation, FloorResult.HitResult))
					{
						remainingTime += subTimeTickRemaining;
						ProcessLanded(FloorResult.HitResult, remainingTime, Iterations);
						return;
					}
				}

				HandleImpact(Hit, LastMoveTimeSlice, Adjusted);

				// If we've changed physics mode, abort.
				if (!HasValidData() || !IsFalling())
				{
					return;
				}

				// Limit air control based on what we hit.
				// We moved to the impact point using air control, but may want to deflect from there based on a limited air control acceleration.
				FVector VelocityNoAirControl = OldVelocity;
				FVector AirControlAccel = Acceleration;
				if (bHasLimitedAirControl)
				{
					// Compute VelocityNoAirControl
					{
						// Find velocity *without* acceleration.
						TGuardValue<FVector> RestoreAcceleration(Acceleration, FVector::ZeroVector);
						TGuardValue<FVector> RestoreVelocity(Velocity, OldVelocity);
						Velocity.Z = 0.f;
						CalcVelocity(timeTick, FallingLateralFriction, false, MaxDecel);
						VelocityNoAirControl = FVector(Velocity.X, Velocity.Y, OldVelocity.Z);
						VelocityNoAirControl = NewFallVelocity(VelocityNoAirControl, Gravity, GravityTime);
					}

					const bool bCheckLandingSpot = false; // we already checked above.
					AirControlAccel = (Velocity - VelocityNoAirControl) / timeTick;
					const FVector AirControlDeltaV = LimitAirControl(LastMoveTimeSlice, AirControlAccel, Hit, bCheckLandingSpot) * LastMoveTimeSlice;
					Adjusted = (VelocityNoAirControl + AirControlDeltaV) * LastMoveTimeSlice;
				}

				const FVector OldHitNormal = Hit.Normal;
				const FVector OldHitImpactNormal = Hit.ImpactNormal;
				FVector Delta = ComputeSlideVector(Adjusted, 1.f - Hit.Time, OldHitNormal, Hit);

				// Compute velocity after deflection (only gravity component for RootMotion)
				if (subTimeTickRemaining > KINDA_SMALL_NUMBER && !bJustTeleported)
				{
					const FVector NewVelocity = (Delta / subTimeTickRemaining);
					Velocity = HasAnimRootMotion() || CurrentRootMotion.HasOverrideVelocityWithIgnoreZAccumulate() ? FVector(Velocity.X, Velocity.Y, NewVelocity.Z) : NewVelocity;
				}

				if (subTimeTickRemaining > KINDA_SMALL_NUMBER && (Delta | Adjusted) > 0.f)
				{
					// Move in deflected direction.
					SafeMoveUpdatedComponent(Delta, PawnRotation, true, Hit);

					if (Hit.bBlockingHit)
					{
						// hit second wall
						LastMoveTimeSlice = subTimeTickRemaining;
						subTimeTickRemaining = subTimeTickRemaining * (1.f - Hit.Time);

						if (IsValidLandingSpot(UpdatedComponent->GetComponentLocation(), Hit))
						{
							remainingTime += subTimeTickRemaining;
							ProcessLanded(Hit, remainingTime, Iterations);
							return;
						}

						HandleImpact(Hit, LastMoveTimeSlice, Delta);

						// If we've changed physics mode, abort.
						if (!HasValidData() || !IsFalling())
						{
							return;
						}

						// Act as if there was no air control on the last move when computing new deflection.
						if (bHasLimitedAirControl && Hit.Normal.Z > VERTICAL_SLOPE_NORMAL_Z)
						{
							const FVector LastMoveNoAirControl = VelocityNoAirControl * LastMoveTimeSlice;
							Delta = ComputeSlideVector(LastMoveNoAirControl, 1.f, OldHitNormal, Hit);
						}

						FVector PreTwoWallDelta = Delta;
						TwoWallAdjust(Delta, Hit, OldHitNormal);

						// Limit air control, but allow a slide along the second wall.
						if (bHasLimitedAirControl)
						{
							const bool bCheckLandingSpot = false; // we already checked above.
							const FVector AirControlDeltaV = LimitAirControl(subTimeTickRemaining, AirControlAccel, Hit, bCheckLandingSpot) * subTimeTickRemaining;

							// Only allow if not back in to first wall
							if (FVector::DotProduct(AirControlDeltaV, OldHitNormal) > 0.f)
							{
								Delta += (AirControlDeltaV * subTimeTickRemaining);
							}
						}

						// Compute velocity after deflection (only gravity component for RootMotion)
						if (subTimeTickRemaining > KINDA_SMALL_NUMBER && !bJustTeleported)
						{
							const FVector NewVelocity = (Delta / subTimeTickRemaining);
							Velocity = HasAnimRootMotion() || CurrentRootMotion.HasOverrideVelocityWithIgnoreZAccumulate() ? FVector(Velocity.X, Velocity.Y, NewVelocity.Z) : NewVelocity;
						}

						// bDitch=true means that pawn is straddling two slopes, neither of which he can stand on
						bool bDitch = ((OldHitImpactNormal.Z > 0.f) && (Hit.ImpactNormal.Z > 0.f) && (FMath::Abs(Delta.Z) <= KINDA_SMALL_NUMBER) && ((Hit.ImpactNormal | OldHitImpactNormal) < 0.f));
						SafeMoveUpdatedComponent(Delta, PawnRotation, true, Hit);
						if (Hit.Time == 0.f)
						{
							// if we are stuck then try to side step
							FVector SideDelta = (OldHitNormal + Hit.ImpactNormal).GetSafeNormal2D();
							if (SideDelta.IsNearlyZero())
							{
								SideDelta = FVector(OldHitNormal.Y, -OldHitNormal.X, 0).GetSafeNormal();
							}
							SafeMoveUpdatedComponent(SideDelta, PawnRotation, true, Hit);
						}

						if (bDitch || IsValidLandingSpot(UpdatedComponent->GetComponentLocation(), Hit) || Hit.Time == 0.f)
						{
							remainingTime = 0.f;
							ProcessLanded(Hit, remainingTime, Iterations);
							return;
						}
						else if ((GetPerchDepthWidthThreshold().X > 0.f || GetPerchDepthWidthThreshold().Y > 0.f) && Hit.Time == 1.f && OldHitImpactNormal.Z >= GetWalkableFloorZ())
						{
							// We might be in a virtual 'ditch' within our perch radius. This is rare.
							const FVector PawnLocation = UpdatedComponent->GetComponentLocation();
							const float ZMovedDist = FMath::Abs(PawnLocation.Z - OldLocation.Z);
							const float MovedDist2DSq = (PawnLocation - OldLocation).SizeSquared2D();
							if (ZMovedDist <= 0.2f * timeTick && MovedDist2DSq <= 4.f * timeTick)
							{
								Velocity.X += 0.25f * GetMaxSpeed() * (RandomStream.FRand() - 0.5f);
								Velocity.Y += 0.25f * GetMaxSpeed() * (RandomStream.FRand() - 0.5f);
								Velocity.Z = FMath::Max<float>(JumpZVelocity * 0.25f, 1.f);
								Delta = Velocity * timeTick;
								SafeMoveUpdatedComponent(Delta, PawnRotation, true, Hit);
							}
						}
					}
				}
			}
		}

		if (Velocity.SizeSquared2D() <= KINDA_SMALL_NUMBER * 10.f)
		{
			Velocity.X = 0.f;
			Velocity.Y = 0.f;
		}
	}
}

bool UPropCharacterMovement::IsWithinEdgeTolerance_PH(const FVector& CollisionLocation, const FVector& TestImpactPoint, const FShapeExtent& CollisionSize) const
{
	if (PropCharacterOwner->GetCollisionType() == ECollisionShape::Capsule)
	{
		const float DistFromCenter = (TestImpactPoint - CollisionLocation).Size2D();
		const float ReducedRadius = FMath::Max(SWEEP_EDGE_REJECT_DISTANCE + KINDA_SMALL_NUMBER, CollisionSize.GetRadius() - SWEEP_EDGE_REJECT_DISTANCE);
		
		return DistFromCenter < ReducedRadius;
	}
	if (PropCharacterOwner->GetCollisionType() == ECollisionShape::Box)
	{
		const float Yaw = UpdatedComponent->GetComponentRotation().Yaw;
		const float cX = CollisionLocation.X;
		const float cY = CollisionLocation.Y;

		const float dX = TestImpactPoint.X - cX;
		const float dY = TestImpactPoint.Y - cY;
		const float nX = cX + (dX * FMath::Cos(Yaw) - dY * FMath::Sin(Yaw));
		const float nY = cY + (dX * FMath::Sin(Yaw) + dY * FMath::Cos(Yaw));

		const float ReducedDepth = FMath::Square(FMath::Max(SWEEP_EDGE_REJECT_DISTANCE + KINDA_SMALL_NUMBER, CollisionSize.GetHalfDepth() - SWEEP_EDGE_REJECT_DISTANCE));
		const float ReducedWidth = FMath::Square(FMath::Max(SWEEP_EDGE_REJECT_DISTANCE + KINDA_SMALL_NUMBER, CollisionSize.GetHalfWidth() - SWEEP_EDGE_REJECT_DISTANCE));

		return nX < (cX + ReducedDepth) && nX >(cX - ReducedDepth) && nY < (cY + ReducedWidth) && nY >(cY - ReducedWidth);
	}

	return false;
}

bool UPropCharacterMovement::MorphInto(const struct FMorphingData& MorphingData, bool bClientSimulation /*= false*/)
{
	if (!HasValidData())
	{
		return false;
	}

	const FVector NewScaledExtent = MorphingData.NewExtent * MorphingData.NewScale;
	const float NewHalfHeight = NewScaledExtent.Z;
	const float OldHalfHeight = MorphingData.CurrentHalfHeight;
	const float HalfHeightAdjust = NewHalfHeight - OldHalfHeight;
	const ECollisionShape::Type NewShapeType = MorphingData.GetNewShapeType();

	if (HalfHeightAdjust > 0.f)
	{
		if (!bClientSimulation)
		{
			const float CurrentHalfHeight = PropCharacterOwner->GetCollisionSize().Size.Z;
			const FVector PawnLocation = UpdatedComponent->GetComponentLocation();

			// Try to stay in place and see if the larger collision fits. We use a slightly taller collision to avoid penetration.
			const UWorld* MyWorld = GetWorld();
			const float SweepInflation = KINDA_SMALL_NUMBER * 10.f;

			FCollisionQueryParams CollisionParams(SCENE_QUERY_STAT(Transform), false, CharacterOwner);
			FCollisionResponseParams ResponseParam;
			InitCollisionParams(CollisionParams, ResponseParam);

			// Compensate for the difference between current collision size and standing size
			FVector ProbeExtent(NewScaledExtent.X, NewScaledExtent.Y, CurrentHalfHeight + SweepInflation + HalfHeightAdjust);
			FCollisionShape StandingCollisionShape;
			if (NewShapeType == ECollisionShape::Box)
			{
				StandingCollisionShape = FCollisionShape::MakeBox(ProbeExtent);
			}
			else
			{
				if (FMath::Max(ProbeExtent.X, ProbeExtent.Y) > ProbeExtent.Z)
				{
					StandingCollisionShape = FCollisionShape::MakeBox(ProbeExtent);
					UE_LOG(LogPropCharacterMovement, Warning, TEXT("Morph: New mesh width > height. Consider to use Box shape instead."), *MorphingData.NewMesh->GetName());
				}
				else
				{
					StandingCollisionShape = FCollisionShape::MakeCapsule(ProbeExtent);
				}
			}

			const ECollisionChannel CollisionChannel = UpdatedComponent->GetCollisionObjectType();
			bool bEncroached = true;

			if (!bCrouchMaintainsBaseLocation)
			{
				// Expand in place
				bEncroached = MyWorld->OverlapBlockingTestByChannel(PawnLocation, UpdatedComponent->GetComponentQuat(), CollisionChannel, StandingCollisionShape, CollisionParams, ResponseParam);

				if (bEncroached)
				{
					// Try adjusting capsule position to see if we can avoid encroachment.
					if (HalfHeightAdjust > 0.f)
					{
						// Shrink to a short capsule, sweep down to base to find where that would hit something, and then try to stand up from there.
						const FShapeExtent PawnSize = PropCharacterOwner->GetCollisionSize();
						const float ShrinkHalfHeight = PawnSize.GetHalfHeight() - FMath::Min(PawnSize.GetHalfDepth(), PawnSize.GetHalfWidth());
						const float TraceDist = PawnSize.GetHalfHeight() - ShrinkHalfHeight;
						const FVector Down = { 0.f, 0.f, -TraceDist };

						ProbeExtent.Z = CurrentHalfHeight - ShrinkHalfHeight;
						const FCollisionShape ShortCollisionShape = (NewShapeType == ECollisionShape::Box) ? FCollisionShape::MakeBox(ProbeExtent) : FCollisionShape::MakeCapsule(ProbeExtent);

						FHitResult Hit(1.f);
						const bool bBlockingHit = MyWorld->SweepSingleByChannel(Hit, PawnLocation, PawnLocation + Down, UpdatedComponent->GetComponentQuat(), CollisionChannel, ShortCollisionShape, CollisionParams);
						if (Hit.bStartPenetrating)
						{
							bEncroached = true;
						}
						else
						{
							// Compute where the base of the sweep ended up, and see if we can stand there
							const float DistanceToBase = (Hit.Time * TraceDist) + ProbeExtent.Z;
							const FVector NewLocation = { PawnLocation.X, PawnLocation.Y, PawnLocation.Z - DistanceToBase + PawnSize.GetHalfHeight() + SweepInflation + MIN_FLOOR_DIST / 2.f };
							bEncroached = MyWorld->OverlapBlockingTestByChannel(NewLocation, UpdatedComponent->GetComponentQuat(), CollisionChannel, StandingCollisionShape, CollisionParams, ResponseParam);
							if (!bEncroached)
							{
								// Intentionally not using MoveUpdatedComponent, where a horizontal plane constraint would prevent the base of the capsule from staying at the same spot.
								UpdatedComponent->MoveComponent(NewLocation - PawnLocation, UpdatedComponent->GetComponentQuat(), false, nullptr, MOVECOMP_NoFlags, ETeleportType::TeleportPhysics);
							}
						}
					}
				}
			}
			else
			{
				// Expand while keeping base location the same.
				FVector StandingLocation = PawnLocation + FVector(0.f, 0.f, ProbeExtent.Z - OldHalfHeight);
				bEncroached = MyWorld->OverlapBlockingTestByChannel(StandingLocation, UpdatedComponent->GetComponentQuat(), CollisionChannel, StandingCollisionShape, CollisionParams, ResponseParam);

				if (bEncroached && IsMovingOnGround())
				{
					// Something might be just barely overhead, try moving down closer to the floor to avoid it.
					if (CurrentFloor.bBlockingHit && CurrentFloor.FloorDist > SweepInflation)
					{
						StandingLocation.Z -= CurrentFloor.FloorDist - SweepInflation;
						bEncroached = MyWorld->OverlapBlockingTestByChannel(StandingLocation, UpdatedComponent->GetComponentQuat(), CollisionChannel, StandingCollisionShape, CollisionParams, ResponseParam);
					}
				}

				if (!bEncroached)
				{
					// Commit the change in location.
					UpdatedComponent->MoveComponent(StandingLocation - PawnLocation, UpdatedComponent->GetComponentQuat(), false, nullptr, MOVECOMP_NoFlags, ETeleportType::TeleportPhysics);
					bForceNextFloorCheck = true;
				}
			}

			// If still encroached then abort.
			if (bEncroached)
			{
				return false;
			}
		}
		else
		{
			bShrinkProxyCapsule = true;
		}
	}
	else
	{
		if (!bClientSimulation)
		{
			if (bCrouchMaintainsBaseLocation)
			{
				// Intentionally not using MoveUpdatedComponent, where a horizontal plane constraint would prevent the base of the capsule from staying at the same spot.
				UpdatedComponent->MoveComponent(FVector(0.f, 0.f, HalfHeightAdjust), UpdatedComponent->GetComponentQuat(), true, nullptr, MOVECOMP_NoFlags, ETeleportType::TeleportPhysics);
			}
		}

		bForceNextFloorCheck = true;
	}

	PropCharacterOwner->SwitchCollisionShape(NewShapeType);
	PropCharacterOwner->SetCollisionSize(NewScaledExtent, true);

	AdjustProxyCapsuleSize();
	PropCharacterOwner->OnEndMorphing(MorphingData.CurrentHeightOffset - HalfHeightAdjust);

	// Don't smooth this change in mesh position
	if ((bClientSimulation && CharacterOwner->GetLocalRole() == ROLE_SimulatedProxy) || (IsNetMode(NM_ListenServer) && CharacterOwner->GetRemoteRole() == ROLE_AutonomousProxy))
	{
		FNetworkPredictionData_Client_Character* ClientData = GetPredictionData_Client_Character();
		if (ClientData)
		{
			ClientData->MeshTranslationOffset += FVector(0.f, 0.f, HalfHeightAdjust);
			ClientData->OriginalMeshTranslationOffset = ClientData->MeshTranslationOffset;
		}
	}

	return true;
}

bool UPropCharacterMovement::CheckLedgeDirection(const FVector& OldLocation, const FVector& SideStep, const FVector& GravDir) const
{
	const FVector SideDest = OldLocation + SideStep;
	FCollisionQueryParams CapsuleParams(SCENE_QUERY_STAT(CheckLedgeDirection), false, CharacterOwner);
	FCollisionResponseParams ResponseParam;
	InitCollisionParams(CapsuleParams, ResponseParam);
	const FCollisionShape CapsuleShape = GetPawnCollisionShape(SHRINK_None);
	const ECollisionChannel CollisionChannel = UpdatedComponent->GetCollisionObjectType();
	FHitResult Result(1.f);
	GetWorld()->SweepSingleByChannel(Result, OldLocation, SideDest, FQuat::Identity, CollisionChannel, CapsuleShape, CapsuleParams, ResponseParam);

	if (!Result.bBlockingHit || IsWalkable(Result))
	{
		if (!Result.bBlockingHit)
		{
			GetWorld()->SweepSingleByChannel(Result, SideDest, SideDest + GravDir * (MaxStepHeight + LedgeCheckThreshold), FQuat::Identity, CollisionChannel, CapsuleShape, CapsuleParams, ResponseParam);
		}
		if ((Result.Time < 1.f) && IsWalkable(Result))
		{
			return true;
		}
	}
	return false;
}

void UPropCharacterMovement::PhysicsRotation(float DeltaTime)
{
	if (!(bOrientRotationToMovement || bUseControllerDesiredRotation))
	{
		return;
	}

	if (!HasValidData() || (!CharacterOwner->Controller && !bRunPhysicsWithNoController))
	{
		return;
	}

	FRotator CurrentRotation = UpdatedComponent->GetComponentRotation(); // Normalized
	CurrentRotation.DiagnosticCheckNaN(TEXT("PropCharacterMovement::PhysicsRotation(): CurrentRotation"));

	FRotator DeltaRot = GetDeltaRotation(DeltaTime);
	DeltaRot.DiagnosticCheckNaN(TEXT("PropCharacterMovement::PhysicsRotation(): GetDeltaRotation"));

	FRotator DesiredRotation = CurrentRotation;
	if (bOrientRotationToMovement)
	{
		DesiredRotation = ComputeOrientToMovementRotation(CurrentRotation, DeltaTime, DeltaRot);
	}
	else if (CharacterOwner->Controller && bUseControllerDesiredRotation)
	{
		DesiredRotation = CharacterOwner->Controller->GetDesiredRotation();
	}
	else
	{
		return;
	}

	if (ShouldRemainVertical())
	{
		DesiredRotation.Pitch = 0.f;
		DesiredRotation.Yaw = FRotator::NormalizeAxis(DesiredRotation.Yaw);
		DesiredRotation.Roll = 0.f;
	}
	else
	{
		DesiredRotation.Normalize();
	}

	// Accumulate a desired new rotation.
	const float AngleTolerance = 1e-3f;

	if (!CurrentRotation.Equals(DesiredRotation, AngleTolerance))
	{
		// PITCH
		if (!FMath::IsNearlyEqual(CurrentRotation.Pitch, DesiredRotation.Pitch, AngleTolerance))
		{
			DesiredRotation.Pitch = FMath::FixedTurn(CurrentRotation.Pitch, DesiredRotation.Pitch, DeltaRot.Pitch);
		}

		// YAW
		if (!FMath::IsNearlyEqual(CurrentRotation.Yaw, DesiredRotation.Yaw, AngleTolerance))
		{
			DesiredRotation.Yaw = FMath::FixedTurn(CurrentRotation.Yaw, DesiredRotation.Yaw, DeltaRot.Yaw);
		}

		// ROLL
		if (!FMath::IsNearlyEqual(CurrentRotation.Roll, DesiredRotation.Roll, AngleTolerance))
		{
			DesiredRotation.Roll = FMath::FixedTurn(CurrentRotation.Roll, DesiredRotation.Roll, DeltaRot.Roll);
		}

		// Set the new rotation.
		DesiredRotation.DiagnosticCheckNaN(TEXT("PropCharacterMovement::PhysicsRotation(): DesiredRotation"));
		RotateUpdatedComponent(DesiredRotation.Quaternion());
	}
}

void UPropCharacterMovement::SetCharacterRotation(FRotator DesiredRotation, float DeltaTime)
{
	if (!HasValidData() || (!PropCharacterOwner->Controller && !bRunPhysicsWithNoController))
	{
		return;
	}

	const FQuat CurrentRotation = UpdatedComponent->GetComponentQuat();
	CurrentRotation.DiagnosticCheckNaN(TEXT("PropCharacterMovement::SetCharacterRotation(): CurrentRotation"));

	// Accumulate a desired new rotation.
	const FQuat NewRotation = FMath::QInterpTo(CurrentRotation, DesiredRotation.Quaternion(), DeltaTime, FaceRotationRate);
	const float AngleTolerance = 1e-3f;
	if (!CurrentRotation.Equals(NewRotation, AngleTolerance))
	{
		// Set the new rotation.
		RotateUpdatedComponent(NewRotation);
	}
}

void UPropCharacterMovement::RotateUpdatedComponent(FQuat DesiredRotation)
{
	DesiredRotation.DiagnosticCheckNaN(TEXT("PropCharacterMovement::SetCharacterRotation(): DesiredRotation"));

	static const float DeltaHeight = 0.2f;
	static const FVector DeltaVector = FVector(0.f, 0.f, DeltaHeight / 2.f);

	FCollisionQueryParams CollisionParams(SCENE_QUERY_STAT(Rotation), false, CharacterOwner);
	FCollisionResponseParams ResponseParam;
	InitCollisionParams(CollisionParams, ResponseParam);
	FCollisionShape CollisionShape = GetPawnCollisionShape(SHRINK_HeightCustom, DeltaHeight);
	const ECollisionChannel CollisionChannel = UpdatedComponent->GetCollisionObjectType();

	if (CollisionShape.IsCapsule())
	{
		// no need to sweep test when capsule
		MoveUpdatedComponent(FVector::ZeroVector, DesiredRotation, false);
		return;
	}

	const FVector StartLocation = UpdatedComponent->GetComponentLocation() - DeltaVector;
	const FVector EndLocation = UpdatedComponent->GetComponentLocation() + DeltaVector;

	FHitResult Hit;
	GetWorld()->SweepSingleByChannel(Hit, StartLocation, EndLocation, DesiredRotation, CollisionChannel, CollisionShape, CollisionParams, ResponseParam);

	// Handle initial penetrations
	if (Hit.bStartPenetrating)
	{
		UE_LOG(LogPropCharacterMovement, Verbose, TEXT("RotateUpdatedComponent: Impact at location %s by %f, normal %s"), *Hit.ImpactPoint.ToString(), Hit.PenetrationDepth, *Hit.Normal.ToString());
		FVector RequestedAdjustment;

		if (FMath::Abs(Hit.Normal.Z) > 0.5f)
		{
			float X = CollisionShape.Box.HalfExtentX;
			float Y = CollisionShape.Box.HalfExtentY;

			FVector Side1 = LineTraceForPenetrationAdjustment(FVector(X, 0.f, 0.f), FVector(X, -Y, 0.f),
				DesiredRotation, CollisionChannel, CollisionParams, ResponseParam);
			FVector Side2 = LineTraceForPenetrationAdjustment(FVector(-X, 0.f, 0.f), FVector(-X, -Y, 0.f),
				DesiredRotation, CollisionChannel, CollisionParams, ResponseParam);

			RequestedAdjustment = Side1.Size2D() > Side2.Size2D() ? Side1 : Side2;

			if (RequestedAdjustment.IsZero())
			{
				Side1 = LineTraceForPenetrationAdjustment(FVector(X, 0.f, 0.f), FVector(X, Y, 0.f),
					DesiredRotation, CollisionChannel, CollisionParams, ResponseParam);
				Side2 = LineTraceForPenetrationAdjustment(FVector(-X, 0.f, 0.f), FVector(-X, Y, 0.f),
					DesiredRotation, CollisionChannel, CollisionParams, ResponseParam);

				RequestedAdjustment = Side1.Size2D() > Side2.Size2D() ? Side1 : Side2;
			}
		}
		else
		{
			RequestedAdjustment = GetPenetrationAdjustment(Hit);
		}
		UE_LOG(LogPropCharacterMovement, Verbose, TEXT("RotateUpdatedComponent: Primary ajustment by %s"), *RequestedAdjustment.ToString());

		// Make sure that we resolved penetration
		bool bEncroached = GetWorld()->SweepSingleByChannel(Hit, StartLocation + RequestedAdjustment, EndLocation + RequestedAdjustment, DesiredRotation, CollisionChannel, CollisionShape, CollisionParams, ResponseParam);

		// Still stuck?
		if (bEncroached)
		{
			// Try to calculate addition adjustment.
			RequestedAdjustment += GetPenetrationAdjustment(Hit);
			UE_LOG(LogPropCharacterMovement, Verbose, TEXT("RotateUpdatedComponent: Secondary ajustment by %s"), *RequestedAdjustment.ToString());

			bEncroached = GetWorld()->SweepTestByChannel(StartLocation + RequestedAdjustment, EndLocation + RequestedAdjustment, DesiredRotation, CollisionChannel, CollisionShape, CollisionParams, ResponseParam);
		}

		if (!bEncroached)
		{
			if (ResolvePenetration(RequestedAdjustment, Hit, DesiredRotation))
			{
				HandleImpact(Hit);
				// Retry original move
				MoveUpdatedComponent(FVector::ZeroVector, DesiredRotation, false);
			}
		}
	}
	else
	{
		MoveUpdatedComponent(FVector::ZeroVector, DesiredRotation, false);
	}
}

FVector UPropCharacterMovement::LineTraceForPenetrationAdjustment(const FVector& StartPoint, const FVector& EndPoint, const FQuat& Rot, ECollisionChannel TraceChannel, const FCollisionQueryParams& Params, const FCollisionResponseParams& ResponseParam)
{
	const FVector CurrentLocation = UpdatedComponent->GetComponentLocation();

	const FVector Start = Rot.RotateVector(StartPoint) + CurrentLocation;
	const FVector End = Rot.RotateVector(EndPoint) + CurrentLocation;

	FHitResult Hit(1.f);
	GetWorld()->LineTraceSingleByChannel(Hit, Start, End, TraceChannel, Params, ResponseParam);

	if (Hit.bBlockingHit)
	{
		const FVector PenetrationDepth = Hit.ImpactPoint - End;
		return ConstrainDirectionToPlane(PenetrationDepth.ProjectOnToNormal(Hit.Normal) + Hit.Normal * 0.125f);
	}
	else
	{
		return FVector::ZeroVector;
	}
}

void UPropCharacterMovement::SetUpdatedComponent(USceneComponent* NewUpdatedComponent)
{
	if (NewUpdatedComponent)
	{
		const auto NewCharacterOwner = Cast<APropCharacter>(NewUpdatedComponent->GetOwner());
		if (NewCharacterOwner == NULL)
		{
			UE_LOG(LogPropCharacterMovement, Error, TEXT("%s owned by %s must update a component owned by a PropCharacter"), *GetName(), *GetNameSafe(NewUpdatedComponent->GetOwner()));
			return;
		}
	}

	if (bMovementInProgress)
	{
		// failsafe to avoid crashes in CharacterMovement. 
		bDeferUpdateMoveComponent = true;
		DeferredUpdatedMoveComponent = NewUpdatedComponent;
		return;
	}
	bDeferUpdateMoveComponent = false;
	DeferredUpdatedMoveComponent = NULL;

	USceneComponent* OldUpdatedComponent = UpdatedComponent;
	UPrimitiveComponent* OldPrimitive = Cast<UPrimitiveComponent>(UpdatedComponent);
	if (IsValid(OldPrimitive) && OldPrimitive->OnComponentBeginOverlap.IsBound())
	{
		OldPrimitive->OnComponentBeginOverlap.RemoveDynamic(this, &UPropCharacterMovement::CapsuleTouched);
	}

	UPawnMovementComponent::SetUpdatedComponent(NewUpdatedComponent);
	CharacterOwner = Cast<ACharacter>(PawnOwner);
	PropCharacterOwner = Cast<APropCharacter>(PawnOwner);

	if (UpdatedComponent != OldUpdatedComponent)
	{
		ClearAccumulatedForces();
	}

	if (UpdatedComponent == NULL)
	{
		StopActiveMovement();
	}

	const bool bValidUpdatedPrimitive = IsValid(UpdatedPrimitive);

	if (bValidUpdatedPrimitive && bEnablePhysicsInteraction)
	{
		UpdatedPrimitive->OnComponentBeginOverlap.AddUniqueDynamic(this, &UPropCharacterMovement::CapsuleTouched);
	}

	if (bNeedsSweepWhileWalkingUpdate)
	{
		bSweepWhileNavWalking = bValidUpdatedPrimitive ? UpdatedPrimitive->GetGenerateOverlapEvents() : false;
		bNeedsSweepWhileWalkingUpdate = false;
	}

	if (bUseRVOAvoidance && IsValid(NewUpdatedComponent))
	{
		UAvoidanceManager* AvoidanceManager = GetWorld()->GetAvoidanceManager();
		if (AvoidanceManager)
		{
			AvoidanceManager->RegisterMovementComponent(this, AvoidanceWeight);
		}
	}
}

FVector2D UPropCharacterMovement::GetPerchDepthWidthThreshold() const
{
	// Don't allow negative values.
	return { FMath::Max(0.f, PerchDepthWidthThreshold.X), FMath::Max(0.f, PerchDepthWidthThreshold.Y) };
}

FVector2D UPropCharacterMovement::GetValidPerchDepthWidth() const
{
	if (PropCharacterOwner)
	{
		const FVector2D DepthWidth = GetPerchDepthWidthThreshold();

		const FShapeExtent CollisionSize = PropCharacterOwner->GetCollisionSize();
		return {
			FMath::Clamp(CollisionSize.GetHalfDepth() - DepthWidth.X, 0.11f, CollisionSize.GetHalfDepth()),
			FMath::Clamp(CollisionSize.GetHalfWidth() - DepthWidth.Y, 0.11f, CollisionSize.GetHalfWidth())
		};
	}

	return FVector2D::ZeroVector;
}

void UPropCharacterMovement::SetNavWalkingPhysics(bool bEnable)
{
	if (UpdatedPrimitive)
	{
		if (bEnable)
		{
			UpdatedPrimitive->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Ignore);
			UpdatedPrimitive->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Ignore);
			CachedProjectedNavMeshHitResult.Reset();

			// Stagger timed updates so many different characters spawned at the same time don't update on the same frame.
			// Initially we want an immediate update though, so set time to a negative randomized range.
			NavMeshProjectionTimer = (NavMeshProjectionInterval > 0.f) ? FMath::FRandRange(-NavMeshProjectionInterval, 0.f) : 0.f;
		}
		else
		{
			UPrimitiveComponent* DefaultCollision = nullptr;
			if (PropCharacterOwner && PropCharacterOwner->GetCollisionComponent() == UpdatedComponent)
			{
				auto DefaultCharacter = PropCharacterOwner->GetClass()->GetDefaultObject<APropCharacter>();
				DefaultCollision = DefaultCharacter ? DefaultCharacter->GetCollisionComponent() : nullptr;
			}

			if (DefaultCollision)
			{
				UpdatedPrimitive->SetCollisionResponseToChannel(ECC_WorldStatic, DefaultCollision->GetCollisionResponseToChannel(ECC_WorldStatic));
				UpdatedPrimitive->SetCollisionResponseToChannel(ECC_WorldDynamic, DefaultCollision->GetCollisionResponseToChannel(ECC_WorldDynamic));
			}
			else
			{
				UE_LOG(LogPropCharacterMovement, Warning, TEXT("Can't revert NavWalking collision settings for %s.%s"),
					*GetNameSafe(CharacterOwner), *GetNameSafe(UpdatedComponent));
			}
		}
	}
}

void UPropCharacterMovement::FindFloor(const FVector& CapsuleLocation, FFindFloorResult& OutFloorResult, bool bCanUseCachedLocation, const FHitResult* DownwardSweepResult /*= NULL*/) const
{
	SCOPE_CYCLE_COUNTER(STAT_PropCharFindFloor);

	// No collision, no floor...
	if (!HasValidData() || !UpdatedComponent->IsQueryCollisionEnabled())
	{
		OutFloorResult.Clear();
		return;
	}

	UE_LOG(LogPropCharacterMovement, VeryVerbose, TEXT("[Role:%d] FindFloor: %s at location %s"), (int32)CharacterOwner->GetLocalRole(), *GetNameSafe(CharacterOwner), *CapsuleLocation.ToString());
	check(PropCharacterOwner->GetCollisionComponent());

	// Increase height check slightly if walking, to prevent floor height adjustment from later invalidating the floor result.
	const float HeightCheckAdjust = (IsMovingOnGround() ? MAX_FLOOR_DIST + KINDA_SMALL_NUMBER : -MAX_FLOOR_DIST);

	float FloorSweepTraceDist = FMath::Max(MAX_FLOOR_DIST, MaxStepHeight + HeightCheckAdjust);
	float FloorLineTraceDist = FloorSweepTraceDist;
	bool bNeedToValidateFloor = true;

	// Sweep floor
	if (FloorLineTraceDist > 0.f || FloorSweepTraceDist > 0.f)
	{
		auto MutableThis = const_cast<UPropCharacterMovement*>(this);

		if (bAlwaysCheckFloor || !bCanUseCachedLocation || bForceNextFloorCheck || bJustTeleported)
		{
			MutableThis->bForceNextFloorCheck = false;
			ComputeFloorDist_PH(CapsuleLocation, FloorLineTraceDist, FloorSweepTraceDist, OutFloorResult, PropCharacterOwner->GetCollisionSize().GetHalf2DSize(), DownwardSweepResult);
		}
		else
		{
			// Force floor check if base has collision disabled or if it does not block us.
			UPrimitiveComponent* MovementBase = CharacterOwner->GetMovementBase();
			const AActor* BaseActor = MovementBase ? MovementBase->GetOwner() : NULL;
			const ECollisionChannel CollisionChannel = UpdatedComponent->GetCollisionObjectType();

			if (MovementBase != NULL)
			{
				MutableThis->bForceNextFloorCheck = !MovementBase->IsQueryCollisionEnabled()
					|| MovementBase->GetCollisionResponseToChannel(CollisionChannel) != ECR_Block
					|| MovementBaseUtility::IsDynamicBase(MovementBase);
			}

			const bool IsActorBasePendingKill = BaseActor && BaseActor->IsPendingKill();

			if (!bForceNextFloorCheck && !IsActorBasePendingKill && MovementBase)
			{
				//UE_LOG(LogCharacterMovement, Log, TEXT("%s SKIP check for floor"), *CharacterOwner->GetName());
				OutFloorResult = CurrentFloor;
				bNeedToValidateFloor = false;
			}
			else
			{
				MutableThis->bForceNextFloorCheck = false;
				ComputeFloorDist_PH(CapsuleLocation, FloorLineTraceDist, FloorSweepTraceDist, OutFloorResult, PropCharacterOwner->GetCollisionSize().GetHalf2DSize(), DownwardSweepResult);
			}
		}
	}

	// OutFloorResult.HitResult is now the result of the vertical floor check.
	// See if we should try to "perch" at this location.
	if (bNeedToValidateFloor && OutFloorResult.bBlockingHit && !OutFloorResult.bLineTrace)
	{
		const bool bCheckRadius = true;
		if (ShouldComputePerchResult(OutFloorResult.HitResult, bCheckRadius))
		{
			float MaxPerchFloorDist = FMath::Max(MAX_FLOOR_DIST, MaxStepHeight + HeightCheckAdjust);
			if (IsMovingOnGround())
			{
				MaxPerchFloorDist += FMath::Max(0.f, PerchAdditionalHeight);
			}

			FFindFloorResult PerchFloorResult;
			if (ComputePerchResult_PH(GetValidPerchDepthWidth(), OutFloorResult.HitResult, MaxPerchFloorDist, PerchFloorResult))
			{
				// Don't allow the floor distance adjustment to push us up too high, or we will move beyond the perch distance and fall next time.
				const float AvgFloorDist = (MIN_FLOOR_DIST + MAX_FLOOR_DIST) * 0.5f;
				const float MoveUpDist = (AvgFloorDist - OutFloorResult.FloorDist);
				if (MoveUpDist + PerchFloorResult.FloorDist >= MaxPerchFloorDist)
				{
					OutFloorResult.FloorDist = AvgFloorDist;
				}

				// If the regular capsule is on an unwalkable surface but the perched one would allow us to stand, override the normal to be one that is walkable.
				if (!OutFloorResult.bWalkableFloor)
				{
					// Floor distances are used as the distance of the regular capsule to the point of collision, to make sure AdjustFloorHeight() behaves correctly.
					OutFloorResult.SetFromLineTrace(PerchFloorResult.HitResult, OutFloorResult.FloorDist, FMath::Max(OutFloorResult.FloorDist, MIN_FLOOR_DIST), true);
				}
			}
			else
			{
				// We had no floor (or an invalid one because it was unwalkable), and couldn't perch here, so invalidate floor (which will cause us to start falling).
				OutFloorResult.bWalkableFloor = false;
			}
		}
	}
}

void UPropCharacterMovement::ComputeFloorDist_PH(const FVector& CollisionLocation, float LineDistance, float SweepDistance, FFindFloorResult& OutFloorResult, const FVector2D& SweepRadius, const FHitResult* DownwardSweepResult /*= NULL*/) const
{
	UE_LOG(LogPropCharacterMovement, VeryVerbose, TEXT("[Role:%d] ComputeFloorDist: %s at location %s"), static_cast<int32>(PropCharacterOwner->GetLocalRole()), *GetNameSafe(PropCharacterOwner), *CollisionLocation.ToString());
	OutFloorResult.Clear();

	const FShapeExtent PawnSize = PropCharacterOwner->GetCollisionSize();

	bool bSkipSweep = false;
	if (DownwardSweepResult && DownwardSweepResult->IsValidBlockingHit())
	{
		// Only if the supplied sweep was vertical and downward.
		if ((DownwardSweepResult->TraceStart.Z > DownwardSweepResult->TraceEnd.Z) &&
			(DownwardSweepResult->TraceStart - DownwardSweepResult->TraceEnd).SizeSquared2D() <= KINDA_SMALL_NUMBER)
		{
			// Reject hits that are barely on the cusp of the radius of the capsule
			if (IsWithinEdgeTolerance_PH(DownwardSweepResult->Location, DownwardSweepResult->ImpactPoint, PawnSize))
			{
				// Don't try a redundant sweep, regardless of whether this sweep is usable.
				bSkipSweep = true;

				const bool bIsWalkable = IsWalkable(*DownwardSweepResult);
				const float FloorDist = (CollisionLocation.Z - DownwardSweepResult->Location.Z);
				OutFloorResult.SetFromSweep(*DownwardSweepResult, FloorDist, bIsWalkable);

				if (bIsWalkable) return; // Use the supplied downward sweep as the floor hit result.
			}
		}
	}

	// We require the sweep distance to be >= the line distance,
	// otherwise the HitResult can't be interpreted as the sweep result.
	if (SweepDistance < LineDistance)
	{
		ensure(SweepDistance >= LineDistance);
		return;
	}

	bool bBlockingHit = false;
	FCollisionQueryParams QueryParams(SCENE_QUERY_STAT(ComputeFloorDist_PH), false, CharacterOwner);
	FCollisionResponseParams ResponseParam;
	InitCollisionParams(QueryParams, ResponseParam);
	const auto CollisionChannel = UpdatedComponent->GetCollisionObjectType();

	// Sweep test
	if (!bSkipSweep && SweepDistance > 0.f && SweepRadius.X > 0.f && SweepRadius.Y > 0.f)
	{
		// Use a shorter height to avoid sweeps giving weird results if we start on a surface.
		// This also allows us to adjust out of penetrations.
		const float ShrinkScale = 0.9f;
		const float ShrinkScaleOverlap = 0.1f;
		float ShrinkHeight = FMath::Max(0.1f, (PawnSize.GetHalfHeight() - PawnSize.GetRadius()) * (1.f - ShrinkScale));
		float TraceDist = SweepDistance + ShrinkHeight;

		FHitResult Hit(1.f);
		auto CollisionRotation = FQuat::Identity;

		FCollisionShape CollisionShape;
		const auto CollisionType = PropCharacterOwner->GetCollisionType();
		if (CollisionType == ECollisionShape::Box)
		{
			CollisionShape = FCollisionShape::MakeBox({ SweepRadius.X, SweepRadius.Y, PawnSize.GetHalfHeight() - ShrinkHeight });
			CollisionRotation = PropCharacterOwner->GetBoxComponent()->GetComponentQuat();
		}
		else if (CollisionType == ECollisionShape::Capsule)
		{
			CollisionShape = FCollisionShape::MakeCapsule(SweepRadius.X, PawnSize.GetHalfHeight() - ShrinkHeight);
		}
		bBlockingHit = FloorSweepTestWithRotation(Hit, CollisionLocation, CollisionLocation + FVector(0.f, 0.f, -TraceDist), CollisionRotation, CollisionChannel, CollisionShape, QueryParams, ResponseParam);

		if (bBlockingHit)
		{
			// Reject hits adjacent to us, we only care about hits on the bottom portion of our capsule.
			// Check 2D distance to impact point, reject if within a tolerance from radius.
			if (Hit.bStartPenetrating || !IsWithinEdgeTolerance_PH(CollisionLocation, Hit.ImpactPoint, PawnSize))
			{
				// Use a capsule with a slightly smaller radius and shorter height to avoid the adjacent object.
				// Capsule must not be nearly zero or the trace will fall back to a line trace from the start point and have the wrong length.
				if (CollisionShape.IsBox())
				{
					CollisionShape.Box.HalfExtentX = FMath::Max(0.f, CollisionShape.Box.HalfExtentX - SWEEP_EDGE_REJECT_DISTANCE - KINDA_SMALL_NUMBER);
					CollisionShape.Box.HalfExtentY = FMath::Max(0.f, CollisionShape.Box.HalfExtentY - SWEEP_EDGE_REJECT_DISTANCE - KINDA_SMALL_NUMBER);
				}
				else if (CollisionShape.IsCapsule())
				{
					CollisionShape.Capsule.Radius = FMath::Max(0.f, CollisionShape.Capsule.Radius - SWEEP_EDGE_REJECT_DISTANCE - KINDA_SMALL_NUMBER);
				}

				if (!CollisionShape.IsNearlyZero())
				{
					ShrinkHeight = FMath::Max(0.1f, (PawnSize.GetHalfHeight() - PawnSize.GetRadius()) * (1.f - ShrinkScaleOverlap));
					TraceDist = SweepDistance + ShrinkHeight;
					if (CollisionShape.IsBox())
					{
						const float ShrinkHeightFromDepth = FMath::Max(0.1f, (PawnSize.GetHalfHeight() - PawnSize.GetHalfDepth()) * (1.f - ShrinkScaleOverlap));
						CollisionShape.Box.HalfExtentX = FMath::Max(PawnSize.GetHalfHeight() - ShrinkHeightFromDepth, CollisionShape.Box.HalfExtentX);

						const float ShrinkHeightFromWidth = FMath::Max(0.1f, (PawnSize.GetHalfHeight() - PawnSize.GetHalfWidth()) * (1.f - ShrinkScaleOverlap));
						CollisionShape.Box.HalfExtentY = FMath::Max(PawnSize.GetHalfHeight() - ShrinkHeightFromWidth, CollisionShape.Box.HalfExtentY);
					}
					else if (CollisionShape.IsCapsule())
					{
						CollisionShape.Capsule.HalfHeight = FMath::Max(PawnSize.GetHalfHeight() - ShrinkHeight, CollisionShape.Capsule.Radius);
					}
					Hit.Reset(1.f, false);

					FloorSweepTestWithRotation(Hit, CollisionLocation, CollisionLocation + FVector(0.f, 0.f, -TraceDist), CollisionRotation, CollisionChannel, CollisionShape, QueryParams, ResponseParam);
				}
			}

			// Reduce hit distance by ShrinkHeight because we shrank the capsule for the trace.
			// We allow negative distances here, because this allows us to pull out of penetrations.
			const float MaxPenetrationAdjust = FMath::Max3(MAX_FLOOR_DIST, PawnSize.GetHalfDepth(), PawnSize.GetHalfWidth()); //TODO Check
			const float SweepResult = FMath::Max(-MaxPenetrationAdjust, Hit.Time * TraceDist - ShrinkHeight);

			OutFloorResult.SetFromSweep(Hit, SweepResult, false);
			if (Hit.IsValidBlockingHit() && IsWalkable(Hit))
			{
				if (SweepResult <= SweepDistance)
				{
					// Hit within test distance.
					OutFloorResult.bWalkableFloor = true;
					return;
				}
			}
		}
	}

	// Since we require a longer sweep than line trace, we don't want to run the line trace if the sweep missed everything.
	// We do however want to try a line trace if the sweep was stuck in penetration.
	if (!OutFloorResult.bBlockingHit && !OutFloorResult.HitResult.bStartPenetrating)
	{
		OutFloorResult.FloorDist = SweepDistance;
		return;
	}

	// Line trace
	if (LineDistance > 0.f)
	{
		const float ShrinkHeight = PawnSize.GetHalfHeight();
		const FVector LineTraceStart = CollisionLocation;
		const float TraceDist = LineDistance + ShrinkHeight;
		const FVector Down = FVector(0.f, 0.f, -TraceDist);
		QueryParams.TraceTag = SCENE_QUERY_STAT_NAME_ONLY(FloorLineTrace);

		FHitResult Hit(1.f);
		bBlockingHit = GetWorld()->LineTraceSingleByChannel(Hit, LineTraceStart, LineTraceStart + Down, CollisionChannel, QueryParams, ResponseParam);

		if (bBlockingHit)
		{
			if (Hit.Time > 0.f)
			{
				// Reduce hit distance by ShrinkHeight because we started the trace higher than the base.
				// We allow negative distances here, because this allows us to pull out of penetrations.
				const float MaxPenetrationAdjust = FMath::Max3(MAX_FLOOR_DIST, PawnSize.GetHalfDepth(), PawnSize.GetHalfWidth()); //TODO Check
				const float LineResult = FMath::Max(-MaxPenetrationAdjust, Hit.Time * TraceDist - ShrinkHeight);

				OutFloorResult.bBlockingHit = true;
				if (LineResult <= LineDistance && IsWalkable(Hit))
				{
					OutFloorResult.SetFromLineTrace(Hit, OutFloorResult.FloorDist, LineResult, true);
					return;
				}
			}
		}
	}

	// No hits were acceptable.
	OutFloorResult.bWalkableFloor = false;
}

void UPropCharacterMovement::K2_ComputeFloorDist_PH(FVector CapsuleLocation, float LineDistance, float SweepDistance, FVector2D SweepRadius, FFindFloorResult& FloorResult) const
{
	if (HasValidData())
	{
		SweepDistance = FMath::Max(SweepDistance, 0.f);
		LineDistance = FMath::Clamp(LineDistance, 0.f, SweepDistance);
		SweepRadius.X = FMath::Max(SweepRadius.X, 0.f);
		SweepRadius.Y = FMath::Max(SweepRadius.Y, 0.f);

		ComputeFloorDist_PH(CapsuleLocation, LineDistance, SweepDistance, FloorResult, SweepRadius);
	}
}

bool UPropCharacterMovement::FloorSweepTestWithRotation(struct FHitResult& OutHit, const FVector& Start, const FVector& End, const FQuat& Rot, ECollisionChannel TraceChannel, const struct FCollisionShape& CollisionShape, const struct FCollisionQueryParams& Params, const struct FCollisionResponseParams& ResponseParam) const
{
	bool bBlockingHit = false;

	if (!bUseFlatBaseForFloorChecks)
	{
		bBlockingHit = GetWorld()->SweepSingleByChannel(OutHit, Start, End, Rot, TraceChannel, CollisionShape, Params, ResponseParam);
	}
	else
	{
		if (CollisionShape.IsBox())
		{
			const auto BoxExtent = CollisionShape.GetExtent();
			const FCollisionShape BoxShape = FCollisionShape::MakeBox({ BoxExtent.X * 0.707f, BoxExtent.Y * 0.707f, BoxExtent.Z });

			bBlockingHit = GetWorld()->SweepSingleByChannel(OutHit, Start, End, Rot, TraceChannel, BoxShape, Params, ResponseParam);
		}
		else if (CollisionShape.IsCapsule())
		{
			// Test with a box that is enclosed by the capsule.
			const float CapsuleRadius = CollisionShape.GetCapsuleRadius();
			const float CapsuleHeight = CollisionShape.GetCapsuleHalfHeight();
			const FCollisionShape BoxShape = FCollisionShape::MakeBox(FVector(CapsuleRadius * 0.707f, CapsuleRadius * 0.707f, CapsuleHeight));

			// First test with the box rotated so the corners are along the major axes (i.e. rotated 45 degrees).
			bBlockingHit = GetWorld()->SweepSingleByChannel(OutHit, Start, End, FQuat({ 0.f, 0.f, -1.f }, PI * 0.25f), TraceChannel, BoxShape, Params, ResponseParam);

			if (!bBlockingHit)
			{
				// Test again with the same box, not rotated.
				OutHit.Reset(1.f, false);
				bBlockingHit = GetWorld()->SweepSingleByChannel(OutHit, Start, End, FQuat::Identity, TraceChannel, BoxShape, Params, ResponseParam);
			}
		}
	}

	return bBlockingHit;
}

bool UPropCharacterMovement::IsValidLandingSpot(const FVector& CapsuleLocation, const FHitResult& Hit) const
{
	if (!Hit.bBlockingHit)
	{
		return false;
	}

	// Skip some checks if penetrating. Penetration will be handled by the FindFloor call (using a smaller capsule)
	if (!Hit.bStartPenetrating)
	{
		// Reject unwalkable floor normals.
		if (!IsWalkable(Hit))
		{
			return false;
		}

		const FShapeExtent ShapeSize = PropCharacterOwner->GetCollisionSize();

		// Reject hits that are above our lower hemisphere (can happen when sliding down a vertical surface).
		const float LowerHemisphereZ = Hit.Location.Z - ShapeSize.GetHalfHeight() + FMath::Min(ShapeSize.GetHalfDepth(), ShapeSize.GetHalfWidth());
		if (Hit.ImpactPoint.Z >= LowerHemisphereZ)
		{
			return false;
		}

		// Reject hits that are barely on the cusp of the radius of the capsule
		if (!IsWithinEdgeTolerance_PH(Hit.Location, Hit.ImpactPoint, ShapeSize))
		{
			return false;
		}
	}
	else
	{
		// Penetrating
		if (Hit.Normal.Z < KINDA_SMALL_NUMBER)
		{
			// Normal is nearly horizontal or downward, that's a penetration adjustment next to a vertical or overhanging wall. Don't pop to the floor.
			return false;
		}
	}

	FFindFloorResult FloorResult;
	FindFloor(CapsuleLocation, FloorResult, false, &Hit);

	if (!FloorResult.IsWalkableFloor())
	{
		return false;
	}

	return true;
}

bool UPropCharacterMovement::ShouldCheckForValidLandingSpot(float DeltaTime, const FVector& Delta, const FHitResult& Hit) const
{
	// See if we hit an edge of a surface on the lower portion of the capsule.
	// In this case the normal will not equal the impact normal, and a downward sweep may find a walkable surface on top of the edge.
	if (Hit.Normal.Z > KINDA_SMALL_NUMBER && !Hit.Normal.Equals(Hit.ImpactNormal))
	{
		const FVector PawnLocation = UpdatedComponent->GetComponentLocation();
		return IsWithinEdgeTolerance_PH(PawnLocation, Hit.ImpactPoint, PropCharacterOwner->GetCollisionSize());
	}

	return false;
}

bool UPropCharacterMovement::ShouldComputePerchResult(const FHitResult& InHit, bool bCheckRadius /*= true*/) const
{
	if (!InHit.IsValidBlockingHit())
	{
		return false;
	}

	// Don't try to perch if the edge radius is very small.
	if (GetPerchDepthWidthThreshold().X <= SWEEP_EDGE_REJECT_DISTANCE)
	{
		return false;
	}

	if (bCheckRadius)
	{
		const FVector2D ValidPerchRadius = GetValidPerchDepthWidth();

		if (PropCharacterOwner->GetCollisionType() == ECollisionShape::Capsule)
		{
			const float DistFromCenter = (InHit.ImpactPoint - InHit.Location).Size2D();
			if (DistFromCenter <= ValidPerchRadius.X)
			{
				// Already within perch radius.
				return false;
			}
		}
		else
		{
			const float Yaw = UpdatedComponent->GetComponentRotation().Yaw;
			const float cX = InHit.Location.X;
			const float cY = InHit.Location.Y;

			const float dX = InHit.ImpactPoint.X - cX;
			const float dY = InHit.ImpactPoint.Y - cY;
			const float nX = cX + (dX * FMath::Cos(Yaw) - dY * FMath::Sin(Yaw));
			const float nY = cY + (dX * FMath::Sin(Yaw) + dY * FMath::Cos(Yaw));

			if (nX <= (cX + ValidPerchRadius.X / 2.f) && nX >= (cX - ValidPerchRadius.X / 2.f) && nY <= (cY + ValidPerchRadius.Y / 2.f) && nY >= (cY - ValidPerchRadius.Y / 2.f))
			{
				return false;
			}
		}
	}

	return true;
}

bool UPropCharacterMovement::ComputePerchResult_PH(const FVector2D& TestRadius, const FHitResult& InHit, const float InMaxFloorDist, FFindFloorResult& OutPerchFloorResult) const
{
	if (InMaxFloorDist <= 0.f)
	{
		return false;
	}

	// Sweep further than actual requested distance, because a reduced capsule radius means we could miss some hits that the normal radius would contact.
	const FShapeExtent PawnSize = PropCharacterOwner->GetCollisionSize();

	const float InHitAboveBase = FMath::Max(0.f, InHit.ImpactPoint.Z - (InHit.Location.Z - PawnSize.GetHalfHeight()));
	const float PerchLineDist = FMath::Max(0.f, InMaxFloorDist - InHitAboveBase);
	const float PerchSweepDist = FMath::Max(0.f, InMaxFloorDist);

	const float ActualSweepDist = PerchSweepDist + PawnSize.GetRadius();
	ComputeFloorDist_PH(InHit.Location, PerchLineDist, ActualSweepDist, OutPerchFloorResult, TestRadius);

	if (!OutPerchFloorResult.IsWalkableFloor()) return false;

	if (InHitAboveBase + OutPerchFloorResult.FloorDist > InMaxFloorDist)
	{
		// Hit something past max distance
		OutPerchFloorResult.bWalkableFloor = false;
		return false;
	}

	return true;
}

FNetworkPredictionData_Client* UPropCharacterMovement::GetPredictionData_Client() const
{
	if (ClientPredictionData == nullptr)
	{
		auto MutableThis = const_cast<UPropCharacterMovement*>(this);
		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_PropCharacter(*this);
	}

	return ClientPredictionData;
}

FVector UPropCharacterMovement::GetPawnCollosionExtent(const EShrinkCapsuleExtent ShrinkMode, const float CustomShrinkAmount /*= 0.f*/) const
{
	check(PropCharacterOwner);

	FVector CollisionExtent(PropCharacterOwner->GetCollisionSize().Size);

	float DepthEpsilon = 0.f;
	float WidthEpsilon = 0.f;
	float HeightEpsilon = 0.f;

	switch (ShrinkMode)
	{
		case SHRINK_None:
			return CollisionExtent;

		case SHRINK_RadiusCustom:
			DepthEpsilon = CustomShrinkAmount;
			WidthEpsilon = CustomShrinkAmount;
			break;

		case SHRINK_HeightCustom:
			HeightEpsilon = CustomShrinkAmount;
			break;

		case SHRINK_AllCustom:
			DepthEpsilon = CustomShrinkAmount;
			WidthEpsilon = CustomShrinkAmount;
			HeightEpsilon = CustomShrinkAmount;
			break;

		default:
			UE_LOG(LogPropCharacterMovement, Warning, TEXT("Unknown EShrinkCollisionExtent in UCharacterMovementComponent::GetCollisionExtent"));
			break;
	}

	// Don't shrink to zero extent.
	const float MinExtent = KINDA_SMALL_NUMBER * 10.f;
	CollisionExtent.X = FMath::Max(CollisionExtent.X - DepthEpsilon, MinExtent);
	CollisionExtent.Y = FMath::Max(CollisionExtent.Y - WidthEpsilon, MinExtent);
	CollisionExtent.Z = FMath::Max(CollisionExtent.Z - HeightEpsilon, MinExtent);

	return CollisionExtent;
}

FCollisionShape UPropCharacterMovement::GetPawnCollisionShape(const EShrinkCapsuleExtent ShrinkMode, const float CustomShrinkAmount /*= 0.f*/) const
{
	const FVector Extent = GetPawnCollosionExtent(ShrinkMode, CustomShrinkAmount);
	if (PropCharacterOwner->GetCollisionType() == ECollisionShape::Box)
	{
		return FCollisionShape::MakeBox(Extent);
	}
	else
	{
		return FCollisionShape::MakeCapsule(Extent);
	}
}

void UPropCharacterMovement::AdjustProxyCapsuleSize()
{
	if (bShrinkProxyCapsule && PropCharacterOwner && PropCharacterOwner->GetLocalRole() == ROLE_SimulatedProxy)
	{
		bShrinkProxyCapsule = false;

		const float ShrinkDepth = FMath::Max(0.f, NetProxyShrinkDepth);
		const float ShrinkWidth = FMath::Max(0.f, NetProxyShrinkWidth);
		const float ShrinkHalfHeight = FMath::Max(0.f, NetProxyShrinkHalfHeight);

		if (ShrinkDepth == 0.f && ShrinkWidth == 0.f && ShrinkHalfHeight == 0.f)
		{
			return;
		}

		const FShapeExtent Size = PropCharacterOwner->GetCollisionSize();
		const float ComponentScale = PropCharacterOwner->GetShapeScale();

		if (ComponentScale <= KINDA_SMALL_NUMBER)
		{
			return;
		}

		const float NewDepth = FMath::Max(0.f, Size.GetHalfDepth() - ShrinkDepth / ComponentScale);
		const float NewWidth = FMath::Max(0.f, Size.GetHalfWidth() - ShrinkWidth / ComponentScale);
		const float NewHalfHeight = FMath::Max(0.f, Size.GetHalfHeight() - ShrinkHalfHeight / ComponentScale);

		if (NewDepth == 0.f || NewWidth == 0.f || NewHalfHeight == 0.f)
		{
			UE_LOG(LogPropCharacterMovement, Warning, TEXT("Invalid attempt to shrink Proxy collision for %s to zero dimension!"), *CharacterOwner->GetName());
			return;
		}

		UE_LOG(LogPropCharacterMovement, Verbose, TEXT("Shrinking collision for %s from (d=%.3f, w=%.3f, h=%.3f) to (d=%.3f, w=%.3f, h=%.3f)"), *CharacterOwner->GetName(),
			Size.GetHalfDepth() * ComponentScale, Size.GetHalfWidth() * ComponentScale, Size.GetHalfHeight() * ComponentScale, NewDepth * ComponentScale, NewWidth * ComponentScale, NewHalfHeight * ComponentScale);

		PropCharacterOwner->SetCollisionSize({ NewDepth, NewWidth, NewHalfHeight }, true);
	}
}

void UPropCharacterMovement::ReplicateMoveToServer(float DeltaTime, const FVector& NewAcceleration)
{
	SCOPE_CYCLE_COUNTER(STAT_PropCharacterMovementReplicateMoveToServer);
	check(CharacterOwner != NULL);

	// Can only start sending moves if our controllers are synced up over the network, otherwise we flood the reliable buffer.
	APlayerController* PC = Cast<APlayerController>(CharacterOwner->GetController());
	if (PC && PC->AcknowledgedPawn != CharacterOwner)
	{
		return;
	}

	// Bail out if our character's controller doesn't have a Player. This may be the case when the local player
	// has switched to another controller, such as a debug camera controller.
	if (PC && PC->Player == nullptr)
	{
		return;
	}

	FNetworkPredictionData_Client_Character* ClientData = GetPredictionData_Client_Character();
	if (!ClientData)
	{
		return;
	}

	// Update our delta time for physics simulation.
	DeltaTime = ClientData->UpdateTimeStampAndDeltaTime(DeltaTime, *CharacterOwner, *this);

	// Find the oldest (unacknowledged) important move (OldMove).
	// Don't include the last move because it may be combined with the next new move.
	// A saved move is interesting if it differs significantly from the last acknowledged move
	FSavedMovePtr OldMove = NULL;
	if (ClientData->LastAckedMove.IsValid())
	{
		const int32 NumSavedMoves = ClientData->SavedMoves.Num();
		for (int32 i = 0; i < NumSavedMoves - 1; i++)
		{
			const FSavedMovePtr& CurrentMove = ClientData->SavedMoves[i];
			if (CurrentMove->IsImportantMove(ClientData->LastAckedMove))
			{
				OldMove = CurrentMove;
				break;
			}
		}
	}

	// Get a SavedMove object to store the movement in.
	FSavedMovePtr NewMovePtr = ClientData->CreateSavedMove();
	FSavedMove_Character* const NewMove = NewMovePtr.Get();
	if (NewMove == nullptr)
	{
		return;
	}

	NewMove->SetMoveFor(CharacterOwner, DeltaTime, NewAcceleration, *ClientData);
	const UWorld* MyWorld = GetWorld();

	// see if the two moves could be combined
	// do not combine moves which have different TimeStamps (before and after reset).
	if (const FSavedMove_Character* PendingMove = ClientData->PendingMove.Get())
	{
		if (PendingMove->CanCombineWith(NewMovePtr, CharacterOwner, ClientData->MaxMoveDeltaTime * CharacterOwner->GetActorTimeDilation(*MyWorld)))
		{
			SCOPE_CYCLE_COUNTER(STAT_PropCharacterMovementCombineNetMove);

			// Only combine and move back to the start location if we don't move back in to a spot that would make us collide with something new.
			const FVector OldStartLocation = PendingMove->GetRevertedLocation();
			const bool bAttachedToObject = (NewMovePtr->StartAttachParent != nullptr);
			if (bAttachedToObject || !OverlapTest(OldStartLocation, PendingMove->StartRotation.Quaternion(), UpdatedComponent->GetCollisionObjectType(), GetPawnCollisionShape(SHRINK_None), CharacterOwner))
			{
				// Avoid updating Mesh bones to physics during the teleport back, since PerformMovement() will update it right away anyway below.
				// Note: this must be before the FScopedMovementUpdate below, since that scope is what actually moves the character and mesh.
				FScopedMeshBoneUpdateOverride ScopedNoMeshBoneUpdate(CharacterOwner->GetMesh(), EKinematicBonesUpdateToPhysics::SkipAllBones);

				// Accumulate multiple transform updates until scope ends.
				FScopedMovementUpdate ScopedMovementUpdate(UpdatedComponent, EScopedUpdate::DeferredUpdates);
				UE_LOG(LogNetPlayerMovement, VeryVerbose, TEXT("CombineMove: add delta %f + %f and revert from %f %f to %f %f"), DeltaTime, PendingMove->DeltaTime, UpdatedComponent->GetComponentLocation().X, UpdatedComponent->GetComponentLocation().Y, OldStartLocation.X, OldStartLocation.Y);

				NewMove->CombineWith(PendingMove, CharacterOwner, PC, OldStartLocation);

				if (PC)
				{
					// We reverted position to that at the start of the pending move (above), however some code paths expect rotation to be set correctly
					// before character movement occurs (via FaceRotation), so try that now. The bOrientRotationToMovement path happens later as part of PerformMovement() and PhysicsRotation().
					CharacterOwner->FaceRotation(PC->GetControlRotation(), NewMove->DeltaTime);
				}

				SaveBaseLocation();
				NewMove->SetInitialPosition(CharacterOwner);

				// Remove pending move from move list. It would have to be the last move on the list.
				if (ClientData->SavedMoves.Num() > 0 && ClientData->SavedMoves.Last() == ClientData->PendingMove)
				{
					const bool bAllowShrinking = false;
					ClientData->SavedMoves.Pop(bAllowShrinking);
				}
				ClientData->FreeMove(ClientData->PendingMove);
				ClientData->PendingMove = nullptr;
				PendingMove = nullptr; // Avoid dangling reference, it's deleted above.
			}
			else
			{
				UE_LOG(LogNetPlayerMovement, Verbose, TEXT("Not combining move [would collide at start location]"));
			}
		}
		else
		{
			UE_LOG(LogNetPlayerMovement, Verbose, TEXT("Not combining move [not allowed by CanCombineWith()]"));
		}
	}

	// Acceleration should match what we send to the server, plus any other restrictions the server also enforces (see MoveAutonomous).
	Acceleration = NewMove->Acceleration.GetClampedToMaxSize(GetMaxAcceleration());
	AnalogInputModifier = ComputeAnalogInputModifier(); // recompute since acceleration may have changed.

	// Perform the move locally
	CharacterOwner->ClientRootMotionParams.Clear();
	CharacterOwner->SavedRootMotion.Clear();
	PerformMovement(NewMove->DeltaTime);

	NewMove->PostUpdate(CharacterOwner, FSavedMove_Character::PostUpdate_Record);

	// Add NewMove to the list
	if (CharacterOwner->IsReplicatingMovement())
	{
		check(NewMove == NewMovePtr.Get());
		ClientData->SavedMoves.Push(NewMovePtr);

		static const auto NetEnableMoveCombiningCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("p.NetEnableMoveCombining"));
		int32 NetEnableMoveCombining = NetEnableMoveCombiningCVar->GetInt();
		const bool bCanDelayMove = (NetEnableMoveCombining != 0) && CanDelaySendingMove(NewMovePtr);

		if (bCanDelayMove && ClientData->PendingMove.IsValid() == false)
		{
			// Decide whether to hold off on move
			const float NetMoveDelta = FMath::Clamp(GetClientNetSendDeltaTime(PC, ClientData, NewMovePtr), 1.f / 120.f, 1.f / 5.f);

			if ((MyWorld->TimeSeconds - ClientData->ClientUpdateTime) * MyWorld->GetWorldSettings()->GetEffectiveTimeDilation() < NetMoveDelta)
			{
				// Delay sending this move.
				ClientData->PendingMove = NewMovePtr;
				return;
			}
		}

		ClientData->ClientUpdateTime = MyWorld->TimeSeconds;

		UE_CLOG(CharacterOwner && UpdatedComponent, LogNetPlayerMovement, VeryVerbose, TEXT("ClientMove Time %f Acceleration %s Velocity %s Position %s Rotation %s DeltaTime %f Mode %s MovementBase %s.%s (Dynamic:%d) DualMove? %d"),
			NewMove->TimeStamp, *NewMove->Acceleration.ToString(), *Velocity.ToString(), *UpdatedComponent->GetComponentLocation().ToString(), *UpdatedComponent->GetComponentRotation().ToCompactString(), NewMove->DeltaTime, *GetMovementName(),
			*GetNameSafe(NewMove->EndBase.Get()), *NewMove->EndBoneName.ToString(), MovementBaseUtility::IsDynamicBase(NewMove->EndBase.Get()) ? 1 : 0, ClientData->PendingMove.IsValid() ? 1 : 0);


		bool bSendServerMove = true;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		static const auto NetForceClientServerMoveLossDurationCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("p.NetForceClientServerMoveLossDuration"));
		int32 NetForceClientServerMoveLossDuration = NetForceClientServerMoveLossDurationCVar->GetInt();

		static const auto NetForceClientServerMoveLossPercentCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("p.NetForceClientServerMoveLossPercent"));
		int32 NetForceClientServerMoveLossPercent = NetForceClientServerMoveLossPercentCVar->GetInt();

		// Testing options: Simulated packet loss to server
		const float TimeSinceLossStart = (MyWorld->RealTimeSeconds - ClientData->DebugForcedPacketLossTimerStart);
		if (ClientData->DebugForcedPacketLossTimerStart > 0.f && (TimeSinceLossStart < NetForceClientServerMoveLossDuration))
		{
			bSendServerMove = false;
			UE_LOG(LogNetPlayerMovement, Log, TEXT("Drop ServerMove, %.2f time remains"), NetForceClientServerMoveLossDuration - TimeSinceLossStart);
		}
		else if (NetForceClientServerMoveLossPercent != 0.f && (RandomStream.FRand() < NetForceClientServerMoveLossPercent))
		{
			bSendServerMove = false;
			ClientData->DebugForcedPacketLossTimerStart = (NetForceClientServerMoveLossDuration > 0) ? MyWorld->RealTimeSeconds : 0.0f;
			UE_LOG(LogNetPlayerMovement, Log, TEXT("Drop ServerMove, %.2f time remains"), NetForceClientServerMoveLossDuration);
		}
		else
		{
			ClientData->DebugForcedPacketLossTimerStart = 0.f;
		}
#endif

		// Send move to server if this character is replicating movement
		if (bSendServerMove)
		{
			SCOPE_CYCLE_COUNTER(STAT_PropCharacterMovementCallServerMove);
			CallServerMovePacked(NewMove, ClientData->PendingMove.Get(), OldMove.Get());
		}
	}

	ClientData->PendingMove = NULL;
}

bool UPropCharacterMovement::ClientUpdatePositionAfterServerUpdate()
{
	SCOPE_CYCLE_COUNTER(STAT_PropCharacterMovementClientUpdatePositionAfterServerUpdate);
	if (!HasValidData())
	{
		return false;
	}

	FNetworkPredictionData_Client_Character* ClientData = GetPredictionData_Client_Character();
	check(ClientData);

	if (!ClientData->bUpdatePosition)
	{
		return false;
	}

	ClientData->bUpdatePosition = false;

	// Don't do any network position updates on things running PHYS_RigidBody
	if (CharacterOwner->GetRootComponent() && CharacterOwner->GetRootComponent()->IsSimulatingPhysics())
	{
		return false;
	}

	if (ClientData->SavedMoves.Num() == 0)
	{
		UE_LOG(LogNetPlayerMovement, Verbose, TEXT("ClientUpdatePositionAfterServerUpdate No saved moves to replay"), ClientData->SavedMoves.Num());

		// With no saved moves to resimulate, the move the server updated us with is the last move we've done, no resimulation needed.
		CharacterOwner->bClientResimulateRootMotion = false;
		if (CharacterOwner->bClientResimulateRootMotionSources)
		{
			// With no resimulation, we just update our current root motion to what the server sent us
			UE_LOG(LogRootMotion, VeryVerbose, TEXT("CurrentRootMotion getting updated to ServerUpdate state: %s"), *CharacterOwner->GetName());
			CurrentRootMotion.UpdateStateFrom(CharacterOwner->SavedRootMotion);
			CharacterOwner->bClientResimulateRootMotionSources = false;
		}

		return false;
	}

	// Save important values that might get affected by the replay.
	const float SavedAnalogInputModifier = AnalogInputModifier;
	const FRootMotionMovementParams BackupRootMotionParams = RootMotionParams; // For animation root motion
	const FRootMotionSourceGroup BackupRootMotion = CurrentRootMotion;
	const bool bRealJump = CharacterOwner->bPressedJump;
	const bool bRealCrouch = bWantsToCrouch;
	const bool bRealForceMaxAccel = bForceMaxAccel;
	CharacterOwner->bClientWasFalling = (MovementMode == MOVE_Falling);
	CharacterOwner->bClientUpdating = true;
	bForceNextFloorCheck = true;

	// Replay moves that have not yet been asked.
	UE_LOG(LogNetPlayerMovement, Verbose, TEXT("ClientUpdatePositionAfterServerUpdate Replaying %d Moves, starting at Timestamp %f"), ClientData->SavedMoves.Num(), ClientData->SavedMoves[0]->TimeStamp);
	for (int32 i = 0; i < ClientData->SavedMoves.Num(); i++)
	{
		FSavedMove_Character* const CurrentMove = ClientData->SavedMoves[i].Get();
		checkSlow(CurrentMove != nullptr);
		CurrentMove->PrepMoveFor(CharacterOwner);

		// Make current move data accessible to MoveAutonomous or any other functions that might need it.
		if (FCharacterNetworkMoveData* NewMove = GetNetworkMoveDataContainer().GetNewMoveData())
		{
			SetCurrentNetworkMoveData(NewMove);
			NewMove->ClientFillNetworkMoveData(*CurrentMove, FCharacterNetworkMoveData::ENetworkMoveType::NewMove);
		}

		MoveAutonomous(CurrentMove->TimeStamp, CurrentMove->DeltaTime, CurrentMove->GetCompressedFlags(), CurrentMove->Acceleration);

		CurrentMove->PostUpdate(CharacterOwner, FSavedMove_Character::PostUpdate_Replay);
		SetCurrentNetworkMoveData(nullptr);
	}

	if (FSavedMove_Character* const PendingMove = ClientData->PendingMove.Get())
	{
		PendingMove->bForceNoCombine = true;
	}

	// Restore saved values.
	AnalogInputModifier = SavedAnalogInputModifier;
	RootMotionParams = BackupRootMotionParams;
	CurrentRootMotion = BackupRootMotion;
	if (CharacterOwner->bClientResimulateRootMotionSources)
	{
		// If we were resimulating root motion sources, it's because we had mismatched state
		// with the server - we just resimulated our SavedMoves and now need to restore
		// CurrentRootMotion with the latest "good state"
		UE_LOG(LogRootMotion, VeryVerbose, TEXT("CurrentRootMotion getting updated after ServerUpdate replays: %s"), *CharacterOwner->GetName());
		CurrentRootMotion.UpdateStateFrom(CharacterOwner->SavedRootMotion);
		CharacterOwner->bClientResimulateRootMotionSources = false;
	}
	CharacterOwner->SavedRootMotion.Clear();
	CharacterOwner->bClientResimulateRootMotion = false;
	CharacterOwner->bClientUpdating = false;
	CharacterOwner->bPressedJump = bRealJump;
	bWantsToCrouch = bRealCrouch;
	bForceMaxAccel = bRealForceMaxAccel;
	bForceNextFloorCheck = true;

	return (ClientData->SavedMoves.Num() > 0);
}

void UPropCharacterMovement::CalcAvoidanceVelocity(float DeltaTime)
{
	SCOPE_CYCLE_COUNTER(STAT_AI_PropCharObstacleAvoidance);

	UAvoidanceManager* AvoidanceManager = GetWorld()->GetAvoidanceManager();
	if (AvoidanceWeight >= 1.0f || AvoidanceManager == NULL || GetCharacterOwner() == NULL)
	{
		return;
	}

	if (GetCharacterOwner()->GetLocalRole() != ROLE_Authority)
	{
		return;
	}

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	const bool bShowDebug = AvoidanceManager->IsDebugEnabled(AvoidanceUID);
#endif

	//Adjust velocity only if we're in "Walking" mode. We should also check if we're dazed, being knocked around, maybe off-navmesh, etc.
	UPrimitiveComponent* OurCollision = GetPropCharacterOwner()->GetCollisionComponent();
	if (!Velocity.IsZero() && IsMovingOnGround() && OurCollision)
	{
		//See if we're doing a locked avoidance move already, and if so, skip the testing and just do the move.
		if (AvoidanceLockTimer > 0.0f)
		{
			Velocity = AvoidanceLockVelocity;
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
			if (bShowDebug)
			{
				DrawDebugLine(GetWorld(), GetActorFeetLocation(), GetActorFeetLocation() + Velocity, FColor::Blue, false, 0.5f, SDPG_MAX);
			}
#endif
		}
		else
		{
			FVector NewVelocity = AvoidanceManager->GetAvoidanceVelocityForComponent(this);
			if (bUseRVOPostProcess)
			{
				PostProcessAvoidanceVelocity(NewVelocity);
			}

			if (!NewVelocity.Equals(Velocity))		//Really want to branch hint that this will probably not pass
			{
				//Had to divert course, lock this avoidance move in for a short time. This will make us a VO, so unlocked others will know to avoid us.
				Velocity = NewVelocity;
				SetAvoidanceVelocityLock(AvoidanceManager, AvoidanceManager->LockTimeAfterAvoid);
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
				if (bShowDebug)
				{
					DrawDebugLine(GetWorld(), GetActorFeetLocation(), GetActorFeetLocation() + Velocity, FColor::Red, false, 0.05f, SDPG_MAX, 10.0f);
				}
#endif
			}
			else
			{
				//Although we didn't divert course, our velocity for this frame is decided. We will not reciprocate anything further, so treat as a VO for the remainder of this frame.
				SetAvoidanceVelocityLock(AvoidanceManager, AvoidanceManager->LockTimeAfterClean);	//10 ms of lock time should be adequate.
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
				if (bShowDebug)
				{
					//DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + Velocity, FColor::Green, false, 0.05f, SDPG_MAX, 10.0f);
				}
#endif
			}
		}
		//RickH - We might do better to do this later in our update
		AvoidanceManager->UpdateRVO(this);

		bWasAvoidanceUpdated = true;
	}
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	else if (bShowDebug)
	{
		DrawDebugLine(GetWorld(), GetActorFeetLocation(), GetActorFeetLocation() + Velocity, FColor::Yellow, false, 0.05f, SDPG_MAX);
	}

	if (bShowDebug)
	{
		FVector UpLine(0, 0, 500);
		DrawDebugLine(GetWorld(), GetActorFeetLocation(), GetActorFeetLocation() + UpLine, (AvoidanceLockTimer > 0.01f) ? FColor::Red : FColor::Blue, false, 0.05f, SDPG_MAX, 5.0f);
	}
#endif
}

float UPropCharacterMovement::GetRVOAvoidanceRadius()
{
	const FShapeExtent CollisionSize = GetPropCharacterOwner()->GetCollisionSize();
	return FMath::Max(CollisionSize.GetHalfDepth(), CollisionSize.GetHalfWidth());
}

float UPropCharacterMovement::GetRVOAvoidanceHeight()
{
	return GetPropCharacterOwner()->GetCollisionSize().GetHalfHeight();
}

void FSavedMove_PropCharacter::SetInitialPosition(ACharacter* Character)
{
	StartLocation = Character->GetActorLocation();
	StartRotation = Character->GetActorRotation();
	StartVelocity = Character->GetCharacterMovement()->Velocity;
	UPrimitiveComponent* const MovementBase = Character->GetMovementBase();
	StartBase = MovementBase;
	StartBaseRotation = FQuat::Identity;
	StartFloor = Character->GetCharacterMovement()->CurrentFloor;
	CustomTimeDilation = Character->CustomTimeDilation;
	StartBoneName = Character->GetBasedMovement().BoneName;
	StartActorOverlapCounter = Character->NumActorOverlapEventsCounter;
	StartComponentOverlapCounter = UPrimitiveComponent::GlobalOverlapEventsCounter;

	if (MovementBaseUtility::UseRelativeLocation(MovementBase))
	{
		StartRelativeLocation = Character->GetBasedMovement().Location;
		FVector StartBaseLocation_Unused;
		MovementBaseUtility::GetMovementBaseTransform(MovementBase, StartBoneName, StartBaseLocation_Unused, StartBaseRotation);
	}

	// Attachment state
	if (const USceneComponent* UpdatedComponent = Character->GetCharacterMovement()->UpdatedComponent)
	{
		StartAttachParent = UpdatedComponent->GetAttachParent();
		StartAttachSocketName = UpdatedComponent->GetAttachSocketName();
		StartAttachRelativeLocation = UpdatedComponent->GetRelativeLocation();
		StartAttachRelativeRotation = UpdatedComponent->GetRelativeRotation();
	}

	StartControlRotation = Character->GetControlRotation().Clamp();

	if (auto PropCharacter = Cast<APropCharacter>(Character))
	{
		StartCapsuleRadius = PropCharacter->GetCollisionSize().GetRadius();
		StartCapsuleHalfHeight = PropCharacter->GetCollisionSize().GetHalfHeight();
	}

	// Jump state
	bPressedJump = Character->bPressedJump;
	bWasJumping = Character->bWasJumping;
	JumpKeyHoldTime = Character->JumpKeyHoldTime;
	JumpForceTimeRemaining = Character->JumpForceTimeRemaining;
	JumpMaxCount = Character->JumpMaxCount;
}

FNetworkPredictionData_Client_PropCharacter::FNetworkPredictionData_Client_PropCharacter(const UPropCharacterMovement& ClientMovement) : 
	FNetworkPredictionData_Client_Character(ClientMovement)
{
}

FSavedMovePtr FNetworkPredictionData_Client_PropCharacter::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_PropCharacter());
}
