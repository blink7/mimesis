﻿#include "Character/Prop/ShapeExtent.h"

const FShapeExtent FShapeExtent::ZeroSize(0.f, 0.f, 0.f);

FShapeExtent::FShapeExtent(float Depth, float Width, float Height)
{
	Size.X = Depth / 2.f;
	Size.Y = Width / 2.f;
	Size.Z = Height / 2.f;
}

FShapeExtent::FShapeExtent(float Radius, float HalfHeight)
{
	Size.X = Radius;
	Size.Y = Radius;
	Size.Z = HalfHeight;
}

FShapeExtent::FShapeExtent(const FVector& Extent)
{
	Size = Extent;
}
