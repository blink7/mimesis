// Copyright Art-Sun Games. All Rights Reserved.

#include "Character/Prop/PropCharacter.h"

#include "PropHunt/PropHunt.h"
#include "Core/PHTypes.h"
#include "Core/PHUtilities.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/GameModes/GameMode_Hunting.h"
#include "Core/Abilities/PHAttributeSet.h"
#include "Core/Abilities/PHGameplayAbility.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/Abilities/PHGameplayEffectUIData_Damage.h"
#include "Character/GrabComponent.h"
#include "Character/PlayerIndicatorComponent.h"
#include "Character/Prop/ShapeExtent.h"
#include "Character/Prop/PropCharacterMovement.h"
#include "Weapons/Weapon.h"

#include "TimerManager.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Curves/CurveLinearColor.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/BoxComponent.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"

#include "AkComponent.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"


APropCharacter::APropCharacter(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer.SetDefaultSubobjectClass<UPropCharacterMovement>(ACharacter::CharacterMovementComponentName)),
	CollisionType(ECollisionShape::Capsule),
	MinCameraArmLength(85.f),
	CameraArmLengthChangeStep(10.f)
{
	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("CharacterStaticMesh0");
	StaticMeshComponent->SetCollisionProfileName("NoCollision");
	StaticMeshComponent->SetupAttachment(GetMesh());
	StaticMeshComponent->AlwaysLoadOnClient = true;
	StaticMeshComponent->AlwaysLoadOnServer = true;
	StaticMeshComponent->bCastDynamicShadow = true;
	StaticMeshComponent->bAffectDynamicIndirectLighting = true;
	StaticMeshComponent->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	StaticMeshComponent->SetGenerateOverlapEvents(false);
	StaticMeshComponent->SetCanEverAffectNavigation(false);

	GetCapsuleComponent()->SetCapsuleSize(10.f, 10.f);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>("CollisionBox");
	BoxComponent->SetupAttachment(GetCapsuleComponent());
	BoxComponent->SetCollisionProfileName("NoCollision");
	BoxComponent->SetCollisionObjectType(ECC_Pawn);
	BoxComponent->CanCharacterStepUpOn = ECB_No;
	BoxComponent->SetShouldUpdatePhysicsVolume(true);
	BoxComponent->SetCanEverAffectNavigation(false);
	BoxComponent->bDynamicObstacle = true;
	BoxComponent->SetBoxExtent(FVector(10.f));
	BoxComponent->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>("CameraBoom");
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->SetRelativeLocation({ 0.f, 0.f, 20.f });
	CameraBoom->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(CameraBoom);

	BodyNiagara = CreateDefaultSubobject<UNiagaraComponent>("BodyNiagara");
	BodyNiagara->SetupAttachment(GetRootComponent());

	MorphMeshComponentHelper = CreateDefaultSubobject<UStaticMeshComponent>("MorphMeshCompHelper");
	MorphMeshComponentHelper->SetupAttachment(GetMesh());
	MorphMeshComponentHelper->SetCollisionProfileName("NoCollision");
	MorphMeshComponentHelper->SetVisibility(false);

	AkComponent = CreateDefaultSubobject<UAkComponent>("AkAudioComponent0");
	AkComponent->SetupAttachment(GetRootComponent());
	AkComponent->OcclusionRefreshInterval = 0.f;

	LowHealthPercentage = 0.1f;

	BodyColorLerpSpeed = 0.25f;

	DefaultCameraArmLength = CameraBoom->TargetArmLength;
	TargetCameraArmLength = DefaultCameraArmLength;
}

DECLARE_DELEGATE_OneParam(FBoolDelegate, bool);

void APropCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APropCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APropCharacter::MoveRight);

	PlayerInputComponent->BindAction<FBoolDelegate>("NextWeapon", IE_Pressed, this, &APropCharacter::OnChangeWeapon, true);
	PlayerInputComponent->BindAction<FBoolDelegate>("PrevWeapon", IE_Pressed, this, &APropCharacter::OnChangeWeapon, false);

	PlayerInputComponent->BindAction("Targeting", IE_Pressed, this, &APropCharacter::OnStartTargeting);
	PlayerInputComponent->BindAction("Targeting", IE_Released, this, &APropCharacter::OnStopTargeting);

	BindAbilityInput(PlayerInputComponent);
}

void APropCharacter::UpdateNavigationRelevance()
{
	Super::UpdateNavigationRelevance();

	if (BoxComponent)
	{
		BoxComponent->SetCanEverAffectNavigation(bCanAffectNavigationGeneration);
	}
}

void APropCharacter::FaceRotation(FRotator NewControlRotation, float DeltaTime /*= 0.f*/)
{
	if (bUseControllerRotationYaw)
	{
		const FRotator CurrentRotation = GetActorRotation();

		NewControlRotation.Pitch = CurrentRotation.Pitch;
		NewControlRotation.Roll = CurrentRotation.Roll;

#if ENABLE_NAN_DIAGNOSTIC
		if (NewControlRotation.ContainsNaN())
		{
			logOrEnsureNanError(TEXT("APawn::FaceRotation about to apply NaN-containing rotation to actor! New:(%s), Current:(%s)"), *NewControlRotation.ToString(), *CurrentRotation.ToString());
		}
#endif

		if (GetPropCharacterMovement())
		{
			GetPropCharacterMovement()->SetCharacterRotation(NewControlRotation, DeltaTime);
		}
	}
}

void APropCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bWantsToRunToggled && !IsRunning())
	{
		SetRunning(false, false);
	}

	if (BodyNiagara)
	{
		if (BodyColorCurve && !bUseCustomBodyColor && !GetVelocity().IsNearlyZero())
		{
			BodyColorLerpFactor += DeltaSeconds * BodyColorLerpSpeed;
			if (BodyColorLerpFactor > 1.f)
			{
				BodyColorLerpFactor = 0.f;
			}
			BodyNiagara->SetNiagaraVariableLinearColor("MainColor", BodyColorCurve->GetLinearColorValue(BodyColorLerpFactor));

			float TailColorLerpFactor = BodyColorLerpFactor - 0.1f;
			if (TailColorLerpFactor < 0.f)
			{
				TailColorLerpFactor = 1.f;
			}
			BodyNiagara->SetNiagaraVariableLinearColor("TailColor", BodyColorCurve->GetLinearColorValue(TailColorLerpFactor));
		}
	}

	if (bUpdateCameraArmLength)
	{
		const float NewLength = FMath::FInterpTo(CameraBoom->TargetArmLength, TargetCameraArmLength, DeltaSeconds, CameraArmLengthChangeStep * 2.f);
		if (FMath::IsNearlyEqual(NewLength, TargetCameraArmLength, 1.f))
		{
			bUpdateCameraArmLength = false;
		}

		CameraBoom->TargetArmLength = NewLength;
	}
}

void APropCharacter::ApplyDamageMomentumWithHit(float DamageImpulse, const FHitResult& HitInfo, bool bScaleMomentumByMass /*= false*/)
{
	if ((DamageImpulse > 3.f) && GetCharacterMovement())
	{
		FVector ImpulseDir = (HitInfo.ImpactPoint - HitInfo.TraceStart).GetSafeNormal();
		FVector Impulse = ImpulseDir * DamageImpulse;

		// limit Z momentum added if already going up faster than jump (to avoid blowing character way up into the sky)
		{
			FVector MassScaledImpulse = Impulse;
			if (bScaleMomentumByMass && GetCharacterMovement()->Mass > SMALL_NUMBER)
			{
				MassScaledImpulse = MassScaledImpulse / GetCharacterMovement()->Mass;
			}

			if ((GetCharacterMovement()->Velocity.Z > GetDefault<UCharacterMovementComponent>(GetCharacterMovement()->GetClass())->JumpZVelocity) && (MassScaledImpulse.Z > 0.f))
			{
				Impulse.Z *= 0.5f;
			}
		}

		GetCharacterMovement()->AddImpulse(Impulse, !bScaleMomentumByMass);
	}
}

void APropCharacter::GetPlayerViewPoint(FVector& OutLocation, FRotator& OutRotation) const
{
	OutLocation = CameraBoom->GetComponentLocation();
	OutRotation = GetViewRotation();
}

class UPropCharacterMovement* APropCharacter::GetPropCharacterMovement() const
{
	return Cast<UPropCharacterMovement>(GetCharacterMovement());
}

UPrimitiveComponent* APropCharacter::GetCollisionComponent() const
{
	if (CollisionType == ECollisionShape::Box)
	{
		return BoxComponent;
	}
	else
	{
		return GetCapsuleComponent();
	}
}

struct FShapeExtent APropCharacter::GetCollisionSize() const
{
	if (GetCollisionType() == ECollisionShape::Box)
	{
		return FShapeExtent(BoxComponent->GetUnscaledBoxExtent());
	}
	else
	{
		float Radius, HalfHeight;
		GetCapsuleComponent()->GetUnscaledCapsuleSize(Radius, HalfHeight);
		return { Radius, HalfHeight };
	}
}

void APropCharacter::SetCollisionSize(FVector InExtent, bool bUpdateOverlaps /*= true*/)
{
	const float Radius = FMath::Min(InExtent.X, InExtent.Z);
	GetCapsuleComponent()->SetCapsuleSize(Radius, InExtent.Z, bUpdateOverlaps);
	BoxComponent->SetBoxExtent(InExtent, bUpdateOverlaps);
}

float APropCharacter::GetShapeScale() const
{
	return GetCollisionComponent()->GetComponentTransform().GetMinimumAxisScale();
}

void APropCharacter::SwitchCollisionShape(ECollisionShape::Type NewCollisionType)
{
	if (CollisionType == NewCollisionType)
	{
		return;
	}

	if (NewCollisionType == ECollisionShape::Box)
	{
		GetMesh()->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
		GetMesh()->AttachToComponent(BoxComponent, FAttachmentTransformRules::KeepRelativeTransform);

		AkComponent->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
		AkComponent->AttachToComponent(BoxComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

		BoxComponent->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		SetRootComponent(BoxComponent);
		BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

		GetCapsuleComponent()->AttachToComponent(BoxComponent, FAttachmentTransformRules::KeepWorldTransform);
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		GetCharacterMovement()->SetUpdatedComponent(BoxComponent);

		CollisionType = ECollisionShape::Box;
	}
	else if (NewCollisionType == ECollisionShape::Capsule)
	{
		GetMesh()->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
		GetMesh()->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);

		AkComponent->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
		AkComponent->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);

		GetCapsuleComponent()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		SetRootComponent(GetCapsuleComponent());
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

		GetCharacterMovement()->SetUpdatedComponent(GetCapsuleComponent());

		BoxComponent->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepWorldTransform);
		BoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		CollisionType = ECollisionShape::Capsule;
	}
}

void APropCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(APropCharacter, MorphingData, COND_SkipOwner);

	DOREPLIFETIME(APropCharacter, ColorScheme);
}

void APropCharacter::MoveForward(float Value)
{
	if (Controller && Value != 0.f)
	{
		const FRotator Rotation = FRotator(0.f, Controller->GetControlRotation().Yaw, 0.f);
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void APropCharacter::MoveRight(float Value)
{
	if (Controller && Value != 0.f)
	{
		const FRotator Rotation = FRotator(0.f, Controller->GetControlRotation().Yaw, 0.f);
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void APropCharacter::OnChangeWeapon(bool bNext)
{
	if (Grabber && Grabber->IsHoldingItem())
	{
		Grabber->AdjustHoldDistance(bNext);
	}
	else
	{
		ChangeCameraArmLength(bNext);

	}
}

void APropCharacter::OnStartTargeting()
{
	FixBodyRotation(true);

	if (!HasAuthority())
	{
		ServerFixBodyRotation(true);
	}
}

void APropCharacter::OnStopTargeting()
{
	FixBodyRotation(false);

	if (!HasAuthority())
	{
		ServerFixBodyRotation(false);
	}
}

float APropCharacter::GetHealth() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetHealth();
	}

	return 0.f;
}

void APropCharacter::SetHealth(float Health)
{
	if (AttributeSetBase.IsValid())
	{
		AttributeSetBase->SetHealth(Health);
	}
}

float APropCharacter::GetMaxHealth() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetMaxHealth();
	}

	return 0.f;
}

void APropCharacter::SetMaxHealth(float MaxHealth)
{
	if (AttributeSetBase.IsValid())
	{
		AttributeSetBase->SetMaxHealth(MaxHealth);
	}
}

bool APropCharacter::IsAlive() const
{
	return GetHealth() > 0.f;
}

float APropCharacter::GetLowHealthPercentage() const
{
	return LowHealthPercentage;
}

void APropCharacter::HandleDamage(AController* EventInstigator, AActor* DamageCauser)
{
	if (EventInstigator && EventInstigator != Controller)
	{
		LastHitBy = EventInstigator;
	}

	MakeNoise(1.f, EventInstigator ? EventInstigator->GetPawn() : this);
}

void APropCharacter::Die(float DamageAmount, UGameplayEffectUIData* UIData, AController* Killer, AActor* DamageCauser)
{
	if (!CanDie())
	{
		return;
	}

	// if this is an environmental death then refer to the previous killer so that they receive credit (knocked into lava pits, etc)
	if ((!Killer || Killer == Controller) && LastHitBy)
	{
		Killer = LastHitBy;
	}

	NetUpdateFrequency = GetDefault<APropCharacter>()->NetUpdateFrequency;
	GetCharacterMovement()->ForceReplicationUpdate();

	RemoveAbilities();

	if (AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent->ApplyGameplayEffectToSelf(Cast<UGameplayEffect>(DeathEffect->GetDefaultObject()), GetCharacterLevel(), AbilitySystemComponent->MakeEffectContext());
	}

	if (auto GM = GetWorld()->GetAuthGameMode<AGameMode_Hunting>())
	{
		AController* KilledPlayer = Controller ? Controller : Cast<AController>(GetOwner());
		GM->Killed(Killer, KilledPlayer, this, UIData);
	}

	OnDeath(DamageAmount, Killer ? Killer->GetPawn() : nullptr, DamageCauser);
}

void APropCharacter::OnDeath(float DamageAmount, APawn* InstigatingPawn, AActor* DamageCauser)
{
	if (bDying)
	{
		return;
	}

	SetReplicatingMovement(false);
	TearOff();
	bDying = true;

	if (HasAuthority())
	{
		ReplicateHit(DamageAmount, InstigatingPawn, DamageCauser, true);
	}

	// cannot use IsLocallyControlled here, because even local client's controller may be NULL here
	/*if (GetNetMode() != NM_DedicatedServer && DeathSound && Mesh1P && Mesh1P->IsVisible())
	{
		UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
	}*/

	DetachFromControllerPendingDestroy();
	SetActorEnableCollision(true);

	// Disable movement and hide actor
	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);
	TurnOff();
	SetActorHiddenInGame(true);
	SetLifeSpan(1.0f);

	// Disable collision
	GetCollisionComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCollisionComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
}

void APropCharacter::KilledBy(class APawn* EventInstigator)
{
	if (HasAuthority() && !bDying)
	{
		AController* Killer = nullptr;
		if (EventInstigator)
		{
			Killer = EventInstigator->Controller;
			LastHitBy = nullptr;
		}

		SetHealth(0.f);

		Die(GetMaxHealth(), nullptr, Killer, nullptr);
	}
}

void APropCharacter::FixBodyRotation(bool bEnable)
{
	if (bEnable)
	{
		bUseControllerRotationYaw = true;
		GetCharacterMovement()->bOrientRotationToMovement = false;
	}
	else
	{
		bUseControllerRotationYaw = false;
		GetCharacterMovement()->bOrientRotationToMovement = true;
	}
}

void APropCharacter::ServerFixBodyRotation_Implementation(bool bEnable)
{
	FixBodyRotation(bEnable);
}

bool APropCharacter::ServerFixBodyRotation_Validate(bool bEnable)
{
	return true;
}

void APropCharacter::OnRep_MorphingData()
{
	
	if (GetPropCharacterMovement())
	{
		GetPropCharacterMovement()->MorphInto(MorphingData, true);
		SetStaticMesh(MorphingData.NewMesh, FVector(MorphingData.NewScale), MorphingData.NewMaterials);
	}
}

void APropCharacter::SetStaticMesh(UStaticMesh* NewMesh, const FVector& Scale, const TArray<UMaterialInterface*>& Materials)
{
	if (BodyNiagara)
	{
		BodyNiagara->DestroyComponent();
		BodyNiagara = nullptr;
	}

	const float HalfHeight = GetCollisionSize().GetHalfHeight();
	PlayerIndicator->SetRelativeLocation({ 0.f, 0.f, HalfHeight + 25.f });

	if (IsLocallyControlled())
	{
		UpdateCameraBoomPosition(HalfHeight + 20.f);
	}

	APlayerController* MyPC = GetWorld()->GetFirstPlayerController();
	APHPlayerState* MyPS = MyPC ? MyPC->GetPlayerState<APHPlayerState>() : nullptr;
	if (MyPS)
	{
		if (MyPS->CanHearTransformations())
		{
			UGameplayStatics::SpawnSoundAttached(MorphingSound, GetRootComponent());
		}

		if (MyPS->GetTeamNumber() == ETeamType::Prop)
		{
			MorphMeshComponentHelper->SetStaticMesh(NewMesh);

			UNiagaraComponent* MorphingEffectComp = UNiagaraFunctionLibrary::SpawnSystemAttached(
				MorphingFX,
				StaticMeshComponent,
				NAME_None,
				FVector::ZeroVector,
				FRotator::ZeroRotator,
				EAttachLocation::KeepRelativeOffset,
				true);

			if (MorphingEffectComp)
			{
				static const float MorphingLifetime = 0.3f;

				MorphingEffectComp->SetFloatParameter("Lifetime", MorphingLifetime);

				const FVector ScaleSource = StaticMeshComponent->GetRelativeScale3D();
				const FVector SizeSource = StaticMeshComponent->GetStaticMesh()->GetBounds().SphereRadius * ScaleSource;
				const FVector SizeTarget = NewMesh->GetBounds().SphereRadius * Scale;

				if (FMath::Max(SizeSource.X, SizeSource.Y) > FMath::Max(SizeTarget.X, SizeTarget.Y))
				{
					MorphingEffectComp->SetVectorParameter("MeshSize", SizeSource);
				}
				else
				{
					MorphingEffectComp->SetVectorParameter("MeshSize", SizeTarget);
				}

				UNiagaraFunctionLibrary::OverrideSystemUserVariableStaticMeshComponent(MorphingEffectComp, "Source", StaticMeshComponent);
				MorphingEffectComp->SetVectorParameter("ScaleSource", ScaleSource);

				UNiagaraFunctionLibrary::OverrideSystemUserVariableStaticMeshComponent(MorphingEffectComp, "Target", MorphMeshComponentHelper);
				MorphingEffectComp->SetVectorParameter("ScaleTarget", Scale);

				FTimerHandle TimerHandle_Unused;
				FTimerDelegate HideSourceMeshDelegate = FTimerDelegate::CreateLambda([this]() {
					StaticMeshComponent->SetVisibility(false);
				});
				GetWorldTimerManager().SetTimer(TimerHandle_Unused, HideSourceMeshDelegate, MorphingLifetime * 0.2f, false);

				FTimerHandle TimerHandle2_Unused;
				FTimerDelegate SetNewMeshDelegate = FTimerDelegate::CreateLambda([this, NewMesh, Scale, Materials]() {
					if (StaticMeshComponent && NewMesh)
					{
						StaticMeshComponent->SetStaticMesh(NewMesh);
						StaticMeshComponent->SetWorldScale3D(Scale);

						for (int32 i = 0; i < Materials.Num(); i++)
						{
							StaticMeshComponent->SetMaterial(i, Materials[i]);
						}
					}
					StaticMeshComponent->SetVisibility(true);
				});
				GetWorldTimerManager().SetTimer(TimerHandle2_Unused, SetNewMeshDelegate, MorphingLifetime * 0.5f, false);

				return;
			}
		}
	}

	// If for some reason we are failed to spawn Niagara effect or we cannot see morphing effect (Hunter team) then we just update the mesh
	if (StaticMeshComponent && NewMesh)
	{
		StaticMeshComponent->SetStaticMesh(NewMesh);
		StaticMeshComponent->SetWorldScale3D(Scale);

		for (int32 i = 0; i < Materials.Num(); i++)
		{
			StaticMeshComponent->SetMaterial(i, Materials[i]);
		}
	}
}

bool APropCharacter::CheckHunterToDischarge(AActor* InHunterToDischarge)
{
	FHitResult HitResult;
	FCollisionQueryParams TraceParameters(SCENE_QUERY_STAT(LineOfSight), false, this);

	static const FVector DischargeLocationAdjust = { 0.f, 0.f, 15.f };
	FVector HunterLocation = InHunterToDischarge->GetActorLocation() + DischargeLocationAdjust;

	GetWorld()->LineTraceSingleByChannel(
		HitResult,
		GetActorLocation(),
		HunterLocation,
		ECC_Visibility,
		TraceParameters
	);

	if (HitResult.GetActor() == InHunterToDischarge)
	{
		FVector MyLocation = GetActorLocation();
		MyLocation.Z = 0.f;
		HunterLocation.Z = 0.f;

		FVector DistanceVector = HunterLocation - MyLocation;
		DistanceVector.Normalize();

		const float Angle = FMath::Acos(InHunterToDischarge->GetActorForwardVector() | DistanceVector);
		if (FMath::RadiansToDegrees(Angle) <= DischargeAngle)
		{
			return true;
		}
	}

	return false;
}

void APropCharacter::OnRep_ColorScheme()
{
	ApplyColorScheme();
}

void APropCharacter::ServerSetColorScheme_Implementation(FPropColorScheme InColorScheme)
{
	ColorScheme = InColorScheme;

	ApplyColorScheme();
}

bool APropCharacter::ServerSetColorScheme_Validate(FPropColorScheme InColorScheme)
{
	return true;
}

void APropCharacter::ApplyColorScheme()
{
	bUseCustomBodyColor = ColorScheme.useCustomColor;

	if (BodyNiagara)
	{
		BodyNiagara->SetNiagaraVariableBool("UseCustomColor", bUseCustomBodyColor);

		if (bUseCustomBodyColor)
		{
			BodyNiagara->SetNiagaraVariableLinearColor("MainColor", ColorScheme.bodyColor);
			BodyNiagara->SetNiagaraVariableLinearColor("TailColor", ColorScheme.tailColor);
		}
	}
}

bool APropCharacter::MorphInto(UPrimitiveComponent* SampleComponent)
{
	auto MeshComponent = Cast<UStaticMeshComponent>(SampleComponent);
	if (MeshComponent)
	{
		UStaticMesh* SampleStaticMesh = MeshComponent->GetStaticMesh();
		const FVector SampleScale = MeshComponent->GetComponentScale();
		TArray<UMaterialInterface*> SampleMaterials = MeshComponent->GetMaterials();

		FMorphingData NewMorphingData;
		NewMorphingData.CurrentHalfHeight = StaticMeshComponent->GetStaticMesh()->GetBoundingBox().GetExtent().Z * StaticMeshComponent->GetComponentScale().Z;
		NewMorphingData.CurrentHeightOffset = BaseTranslationOffset.Z;
		NewMorphingData.NewMesh = SampleStaticMesh;
		NewMorphingData.NewMaterials = SampleMaterials;
		NewMorphingData.NewScale = SampleScale.Z;
		NewMorphingData.NewExtent = SampleStaticMesh->GetBoundingBox().GetExtent();
		NewMorphingData.SetNewShapeType(MeshComponent->ComponentHasTag("Box") ? ECollisionShape::Box : ECollisionShape::Capsule);

		if (GetPropCharacterMovement()->MorphInto(NewMorphingData))
		{
			MorphingData = NewMorphingData;
			SetStaticMesh(SampleStaticMesh, SampleScale, SampleMaterials);
			return true;
		}
	}

	return false;
}

bool APropCharacter::CanMorph(UPrimitiveComponent* SampleComponent)
{
	if (!GetPropCharacterMovement())
	{
		return false;
	}

	auto MeshComponent = Cast<UStaticMeshComponent>(SampleComponent);
	if (MeshComponent)
	{
		UStaticMesh* StaticMesh = MeshComponent->GetStaticMesh();
		if (StaticMesh)
		{
			return StaticMeshComponent->GetStaticMesh() != StaticMesh
				|| StaticMeshComponent->GetMaterial(0) != MeshComponent->GetMaterial(0)
				|| StaticMeshComponent->GetComponentScale().Z != MeshComponent->GetComponentScale().Z;
		}
	}

	return false;
}

void APropCharacter::OnEndMorphing(float NewMeshHeightOffset)
{
	if (GetMesh())
	{
		FVector MeshRelativeLocation = GetMesh()->GetRelativeLocation();
		MeshRelativeLocation.Z = NewMeshHeightOffset;
		GetMesh()->SetRelativeLocation(MeshRelativeLocation);
		BaseTranslationOffset.Z = MeshRelativeLocation.Z;
	}
	else
	{
		const auto DefaultChar = GetDefault<APropCharacter>(GetClass());
		BaseTranslationOffset.Z = DefaultChar->BaseTranslationOffset.Z;
	}
}

void APropCharacter::SetJumpMaxCount(int32 NewValue)
{
	if (JumpMaxCount != NewValue)
	{
		JumpMaxCount = NewValue;
	}
}

void APropCharacter::ChangeCameraArmLength(bool bDecrease)
{
	if (bDecrease)
	{
		if (TargetCameraArmLength > MinCameraArmLength)
		{
			TargetCameraArmLength -= CameraArmLengthChangeStep;
			bUpdateCameraArmLength = true;
		}
	}
	else
	{
		if (TargetCameraArmLength < DefaultCameraArmLength)
		{
			TargetCameraArmLength += CameraArmLengthChangeStep;
			bUpdateCameraArmLength = true;
		}
	}
}
