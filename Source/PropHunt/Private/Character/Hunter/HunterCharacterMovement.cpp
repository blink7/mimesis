// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/Hunter/HunterCharacterMovement.h"

#include "Character/Hunter/HunterCharacter.h"

#include "Components/PrimitiveComponent.h"


float UHunterCharacterMovement::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const auto HunterCharacterOwner = Cast<AHunterCharacter>(PawnOwner);
	if (HunterCharacterOwner)
	{
		if (HunterCharacterOwner->IsTargeting())
		{
			MaxSpeed *= HunterCharacterOwner->GetTargetingSpeedModifier();
		}
		if (HunterCharacterOwner->IsRunning())
		{
			MaxSpeed *= HunterCharacterOwner->GetRunningSpeedModifier();
		}
	}

	return MaxSpeed;
}

void UHunterCharacterMovement::ApplyDownwardForce(float DeltaSeconds)
{
	if (StandingDownwardForceScale != 0.f && CurrentFloor.HitResult.IsValidBlockingHit())
	{
		UPrimitiveComponent* BaseComp = CurrentFloor.HitResult.GetComponent();
		const FVector Gravity = FVector(0.f, 0.f, GetGravityZ());

		if (BaseComp && BaseComp->IsAnySimulatingPhysics() && !Gravity.IsZero())
		{
			const float PhysicsMass = BaseComp->GetMass();
			const float PhysicsMassScale = PhysicsMass <= PhysicsMassDependanceLimit
				? FMath::Pow(FMath::Exp(PhysicsMass - PhysicsMassDependanceLimit), 1 / PhysicsMassDependanceSmoothness)
				: 1.f;
			BaseComp->AddForceAtLocation(
				Gravity * Mass * StandingDownwardForceScale * PhysicsMassScale,
				CurrentFloor.HitResult.ImpactPoint,
				CurrentFloor.HitResult.BoneName
			);
		}
	}
}
