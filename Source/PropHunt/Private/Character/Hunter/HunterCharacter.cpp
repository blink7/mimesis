// Copyright Art-Sun Games. All Rights Reserved.

#include "Character/Hunter/HunterCharacter.h"

#include "PropHunt/PropHunt.h"
#include "Core/PHUtilities.h"
#include "Core/PHGameInstance.h"
#include "Core/PHPlayerController.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/GameModes/GameMode_Hunting.h"
#include "Core/Abilities/PHAttributeSet.h"
#include "Core/Abilities/PHGameplayAbility.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/Items/PHWeaponItem.h"
#include "Character/GrabComponent.h"
#include "Character/PHSpringArmComponent.h"
#include "Character/Prop/PropCharacter.h"
#include "Character/Hunter/PropsPathVisualiser.h"
#include "Character/Hunter/HunterCharacterMovement.h"
#include "Weapons/Weapon.h"

#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/TimelineComponent.h"
#include "Components/SpotLightComponent.h"
#include "Components/PostProcessComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/World.h"
#include "Engine/BlockingVolume.h"
#include "Engine/SkeletalMeshSocket.h"
#include "EngineUtils.h"
#include "TimerManager.h"
#include "SkeletalMeshMerge.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraComponent.h"

#include "AkComponent.h"

using namespace PropHunt;

AHunterCharacter::AHunterCharacter(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UHunterCharacterMovement>(ACharacter::CharacterMovementComponentName)),
	FlashlightAttachPoint("Flashlight")
{
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);

	CameraBoom = CreateDefaultSubobject<UPHSpringArmComponent>("CameraBoom");
	CameraBoom->SetupAttachment(GetCapsuleComponent());
	CameraBoom->SetRelativeLocation({ 0.f, 0.f, BaseEyeHeight });
	CameraBoom->bUsePawnControlRotation = true;

	FirstPersonCamera = CreateDefaultSubobject<UCameraComponent>("Camera");
	FirstPersonCamera->SetupAttachment(CameraBoom);

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>("CharacterMesh1P");
	Mesh1P->SetupAttachment(FirstPersonCamera);
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetReceivesDecals(false);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->SetTickGroup(TG_PrePhysics);
	Mesh1P->SetCollisionObjectType(ECC_Pawn);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;

	GetMesh()->SetOwnerNoSee(true);
	GetMesh()->SetOnlyOwnerSee(false);
	GetMesh()->SetReceivesDecals(false);
	GetMesh()->bCastHiddenShadow = true;
	GetMesh()->SetRenderCustomDepth(true);
	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;

	Legs1P = CreateDefaultSubobject<USkeletalMeshComponent>("Legs1P");
	Legs1P->SetupAttachment(GetMesh());
	Legs1P->SetOnlyOwnerSee(true);
	Legs1P->SetReceivesDecals(false);
	Legs1P->bCastDynamicShadow = false;
	Legs1P->SetTickGroup(TG_PrePhysics);
	Legs1P->SetCollisionObjectType(ECC_Pawn);
	Legs1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Legs1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Legs1P->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered;

	PropsPathPostProcess = CreateDefaultSubobject<UPostProcessComponent>("PropsPathPP");
	PropsPathPostProcess->SetupAttachment(GetRootComponent());
	PropsPathPostProcess->bEnabled = false;

	Flashlight1P = CreateDefaultSubobject<USpotLightComponent>("Flashlight1P");
	Flashlight1P->SetupAttachment(Mesh1P, FlashlightAttachPoint);
	Flashlight1P->SetVisibility(false);
	Flashlight1P->SetAttenuationRadius(1500.f);
	Flashlight1P->SetInnerConeAngle(15.f);
	Flashlight1P->SetOuterConeAngle(30.f);
	Flashlight1P->bUseTemperature = true;
	Flashlight1P->SetTemperature(7000.f);

	Flashlight3P = CreateDefaultSubobject<USpotLightComponent>("Flashlight3P");
	Flashlight3P->SetupAttachment(GetMesh(), FlashlightAttachPoint);
	Flashlight3P->SetVisibility(false);
	Flashlight3P->SetAttenuationRadius(1500.f);
	Flashlight3P->SetInnerConeAngle(15.f);
	Flashlight3P->SetOuterConeAngle(30.f);
	Flashlight3P->bUseTemperature = true;
	Flashlight3P->SetTemperature(7000.f);

	AkComponent = CreateDefaultSubobject<UAkComponent>("AkAudioComponent0");
	AkComponent->SetupAttachment(GetRootComponent());
	AkComponent->OcclusionRefreshInterval = 0.f;

	TargetingSpeedModifier = 0.5f;
	RunningSpeedModifier = 1.5f;
	bWantsToRun = false;
	LowHealthPercentage = 0.5f;

	PathAmount = 10;
	PathOffset = 2;

	NoWeaponTag = FGameplayTag::RequestGameplayTag(FName("Weapon.Equipped.None"));
	WeaponChangingDelayReplicationTag = FGameplayTag::RequestGameplayTag(FName("Ability.Weapon.IsChangingDelayReplication"));
	WeaponAmmoTypeNoneTag = FGameplayTag::RequestGameplayTag(FName("Weapon.Ammo.None"));
	WeaponAbilityTag = FGameplayTag::RequestGameplayTag(FName("Ability.Weapon"));
	CurrentWeaponTag = NoWeaponTag;
}

void AHunterCharacter::BeginPlay()
{
	Super::BeginPlay();

	DefaultBaseEyeHeight = BaseEyeHeight;

	// CurrentWeapon is replicated only to Simulated clients so sync the current weapon manually
	if (GetLocalRole() == ROLE_AutonomousProxy)
	{
		ServerSyncCurrentWeapon();
	}

	if (CloakCurve)
	{
		FOnTimelineFloat TimelineCallback;
		FOnTimelineEvent TimelineFinishedCallback;

		TimelineCallback.BindDynamic(this, &AHunterCharacter::OnUpdateCloakAnimation);
		TimelineFinishedCallback.BindDynamic(this, &AHunterCharacter::OnFinishedCloakAnimation);

		CloakTimeline = NewObject<UTimelineComponent>(this, "CloakAnimation");
		CloakTimeline->SetTimelineLengthMode(TL_LastKeyFrame);
		CloakTimeline->AddInterpFloat(CloakCurve, TimelineCallback);
		CloakTimeline->SetTimelineFinishedFunc(TimelineFinishedCallback);
		CloakTimeline->RegisterComponent();
	}
}

void AHunterCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// Clear CurrentWeaponTag on the ASC. This happens naturally in UnEquipCurrentWeapon() but
		// that is only called on the server from hero death (the OnRep_CurrentWeapon() would have
		// handled it on the client but that is never called due to the hero being marked pending
		// destroy). This makes sure the client has it cleared.
	if (AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent->RemoveLooseGameplayTag(CurrentWeaponTag);
		CurrentWeaponTag = NoWeaponTag;
		AbilitySystemComponent->AddLooseGameplayTag(CurrentWeaponTag);
	}

	Super::EndPlay(EndPlayReason);
}

void AHunterCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (HasAuthority())
	{
		GetWorldTimerManager().SetTimerForNextTick(this, &AHunterCharacter::SpawnDefaultInventory);
	}

	// set initial mesh visibility (3rd person view)
	UpdatePawnMeshes();
}

// Client side
void AHunterCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	if (AbilitySystemComponent.IsValid() && CurrentWeapon)
	{
		// If current weapon repped before PlayerState, set tag on ASC
		AbilitySystemComponent->AddLooseGameplayTag(CurrentWeaponTag);
		// Update owning character and ASC just in case it repped before PlayerState
		CurrentWeapon->SetOwningPawn(this);
	}
}

void AHunterCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();

	// switch mesh to 1st person view
	UpdatePawnMeshes();
}

// Server side
void AHunterCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (AbilitySystemComponent.IsValid())
	{
		FOnGameplayEffectTagCountChanged& OnGameplayEffectTagCountChanged = AbilitySystemComponent->RegisterGameplayTagEvent(WeaponChangingDelayReplicationTag);
		WeaponChangingDelayReplicationTagChangedDelegateHandle = OnGameplayEffectTagCountChanged.AddUObject(this, &AHunterCharacter::WeaponChangingDelayReplicationTagChanged);
	}
}

DECLARE_DELEGATE_OneParam(FBoolDelegate, bool);

void AHunterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHunterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHunterCharacter::MoveRight);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AHunterCharacter::OnStartCrouching);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AHunterCharacter::OnStopCrouching);

	PlayerInputComponent->BindAction("Debug", IE_Pressed, this, &AHunterCharacter::Debug);

	BindAbilityInput(PlayerInputComponent);
}

void AHunterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AHunterCharacter, Inventory);

	// Only replicate CurrentWeapon to simulated clients and manually sync CurrentWeeapon with Owner when we're ready.
	// This allows us to predict weapon changing.
	DOREPLIFETIME_CONDITION(AHunterCharacter, CurrentWeapon, COND_SimulatedOnly);
	DOREPLIFETIME_CONDITION(AHunterCharacter, bFlashlightEnable, COND_SkipOwner);

	DOREPLIFETIME(AHunterCharacter, bInvisible);
}

void AHunterCharacter::RecalculateBaseEyeHeight()
{
	Super::RecalculateBaseEyeHeight();

	CameraBoom->SetRelativeLocation({ 0.f, 0.f, BaseEyeHeight });
}

void AHunterCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bWantsToRunToggled && !IsRunning())
	{
		SetRunning(false, false);
	}

	SimulateFlashlightRotation(DeltaSeconds);
}

void AHunterCharacter::Destroyed()
{
	Super::Destroyed();

	DestroyInventory();

	EmptyPropsPathMap();
}

void AHunterCharacter::GetPlayerViewPoint(FVector& OutLocation, FRotator& OutRotation) const
{
	OutLocation = FirstPersonCamera->GetComponentLocation();
	OutRotation = GetViewRotation();
}

void AHunterCharacter::SimulateFlashlightRotation(float DeltaSeconds)
{
	if (bFlashlightEnable && !IsFirstPerson())
	{
		FRotator NewRotation = GetBaseAimRotation();

		FVector StartLocation = GetActorLocation();
		StartLocation.Z += BaseEyeHeight;
		const FVector EndLocation = StartLocation + NewRotation.Vector() * 999999.f;

		FHitResult HitResult;
		const bool bBlockingHit = GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECollisionChannel::ECC_Camera);
		if (bBlockingHit)
		{
			NewRotation = (HitResult.Location - Flashlight3P->GetComponentLocation()).Rotation();
		}

		Flashlight3P->SetWorldRotation(FMath::RInterpTo(Flashlight3P->GetComponentRotation(), NewRotation, DeltaSeconds, 1.f));
	}
}

bool AHunterCharacter::IsWeaponInInventory(AWeapon* InWeapon)
{
	for (AWeapon* Weapon : Inventory)
	{
		if (Weapon && InWeapon && Weapon->GetClass() == InWeapon->GetClass())
		{
			return true;
		}
	}

	return false;
}

void AHunterCharacter::AddWeaponToInventory(AWeapon* NewWeapon)
{
	/*if (IsWeaponInInventory(NewWeapon))
	{
		USoundCue* PickupSound = NewWeapon->GetPickupSound();

		if (PickupSound && IsLocallyControlled())
		{
			UGameplayStatics::SpawnSoundAttached(PickupSound, GetRootComponent());
		}

		if (GetLocalRole() < ROLE_Authority)
		{
			return false;
		}

		// Create a dynamic instant Gameplay Effect to give the primary and secondary ammo
		UGameplayEffect* GEAmmo = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("Ammo")));
		GEAmmo->DurationPolicy = EGameplayEffectDurationType::Instant;

		if (NewWeapon->PrimaryAmmoType != WeaponAmmoTypeNoneTag)
		{
			int32 Idx = GEAmmo->Modifiers.Num();
			GEAmmo->Modifiers.SetNum(Idx + 1);

			FGameplayModifierInfo& InfoPrimaryAmmo = GEAmmo->Modifiers[Idx];
			InfoPrimaryAmmo.ModifierMagnitude = FScalableFloat(NewWeapon->GetPrimaryClipAmmo());
			InfoPrimaryAmmo.ModifierOp = EGameplayModOp::Additive;
			InfoPrimaryAmmo.Attribute = UGSAmmoAttributeSet::GetReserveAmmoAttributeFromTag(NewWeapon->PrimaryAmmoType);
		}

		if (NewWeapon->SecondaryAmmoType != WeaponAmmoTypeNoneTag)
		{
			int32 Idx = GEAmmo->Modifiers.Num();
			GEAmmo->Modifiers.SetNum(Idx + 1);

			FGameplayModifierInfo& InfoSecondaryAmmo = GEAmmo->Modifiers[Idx];
			InfoSecondaryAmmo.ModifierMagnitude = FScalableFloat(NewWeapon->GetSecondaryClipAmmo());
			InfoSecondaryAmmo.ModifierOp = EGameplayModOp::Additive;
			InfoSecondaryAmmo.Attribute = UGSAmmoAttributeSet::GetReserveAmmoAttributeFromTag(NewWeapon->SecondaryAmmoType);
		}

		if (GEAmmo->Modifiers.Num() > 0)
		{
			AbilitySystemComponent->ApplyGameplayEffectToSelf(GEAmmo, 1.0f, AbilitySystemComponent->MakeEffectContext());
		}

		NewWeapon->Destroy();

		return false;
	}*/

	if (NewWeapon && HasAuthority())
	{
		Inventory.AddUnique(NewWeapon);
	}
}

void AHunterCharacter::RemoveWeaponFromInventory(AWeapon* Weapon)
{
	if (IsWeaponInInventory(Weapon) && HasAuthority())
	{
		if (Weapon == CurrentWeapon)
		{
			UnEquipCurrentWeapon();
		}

		Weapon->ResetWeapon();
		Inventory.RemoveSingle(Weapon);
	}
}

AWeapon* AHunterCharacter::FindWeapon(TSubclassOf<AWeapon> WeaponClass)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] && Inventory[i]->IsA(WeaponClass))
		{
			return Inventory[i];
		}
	}

	return nullptr;
}

void AHunterCharacter::EquipWeapon(AWeapon* NewWeapon)
{
	if (NewWeapon)
	{
		if (HasAuthority())
		{
			SetCurrentWeapon(NewWeapon, CurrentWeapon);
		}
		else
		{
			ServerEquipWeapon(NewWeapon);
			SetCurrentWeapon(NewWeapon, CurrentWeapon);
			bChangedWeaponLocally = true;
		}
	}
}

void AHunterCharacter::UnEquipWeapon(AWeapon* WeaponToUnEquip)
{
	if (WeaponToUnEquip)
	{
		if (AbilitySystemComponent.IsValid())
		{
			AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UPHAttributeSet::GetAmmoAttributeFromTag(WeaponToUnEquip->PrimaryAmmoType)).Remove(PrimaryAmmoChangedDelegateHandle);
			AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UPHAttributeSet::GetAmmoAttributeFromTag(WeaponToUnEquip->SecondaryAmmoType)).Remove(SecondaryAmmoChangedDelegateHandle);
		}

		WeaponToUnEquip->OnLeaveInventory();
	}
}

void AHunterCharacter::UnEquipCurrentWeapon()
{
	if (AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent->RemoveLooseGameplayTag(CurrentWeaponTag);
		CurrentWeaponTag = NoWeaponTag;
		AbilitySystemComponent->AddLooseGameplayTag(CurrentWeaponTag);
	}

	UnEquipWeapon(CurrentWeapon);
	CurrentWeapon = nullptr;

	const auto PC = GetController<APHPlayerController>();
	if (PC && PC->IsLocalController())
	{
// 		PC->SetEquippedWeaponPrimaryIconFromSprite(nullptr);
// 		PC->SetEquippedWeaponStatusText(FText());
// 		PC->SetPrimaryClipAmmo(0);
// 		PC->SetPrimaryReserveAmmo(0);
// 		PC->SetHUDReticle(nullptr);
	}
}

void AHunterCharacter::MoveForward(float Value)
{
	if (Controller && Value != 0.f)
	{
		// Limit pitch when walking or falling
		const bool bLimitRotation = (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling());
		const FRotator Rotation = bLimitRotation ? GetActorRotation() : Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AHunterCharacter::MoveRight(float Value)
{
	if (Value != 0.f)
	{
		const FQuat Rotation = GetActorQuat();
		const FVector Direction = FQuatRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void AHunterCharacter::OnChangeWeapon(bool bNext)
{
	const auto MyPC = GetController<APHPlayerController>();
	if (MyPC && !MyPC->IsGameActionsIgnored())
	{
		if (Grabber && Grabber->IsHoldingItem())
		{
			Grabber->AdjustHoldDistance(bNext);
		}
		else if (Inventory.Num() >= 2)
		{
			const int32 CurrentWeaponIdx = Inventory.IndexOfByKey(CurrentWeapon);
			UnEquipCurrentWeapon();

			if (bNext)
			{
				AWeapon* NextWeapon = Inventory[(CurrentWeaponIdx + 1) % Inventory.Num()];
				EquipWeapon(NextWeapon);
			}
			else
			{
				AWeapon* PrevWeapon = Inventory[(CurrentWeaponIdx - 1 + Inventory.Num()) % Inventory.Num()];
				EquipWeapon(PrevWeapon);
			}
		}
	}
}

void AHunterCharacter::OnStartCrouching()
{
	const auto MyPC = GetController<APHPlayerController>();
	if (MyPC && !MyPC->IsGameActionsIgnored())
	{
		if (IsRunning())
		{
			SetRunning(false, false);
		}
		Crouch();
	}
}

void AHunterCharacter::OnStopCrouching()
{
	UnCrouch();
}

USkeletalMeshComponent* AHunterCharacter::GetPawnMesh() const
{
	return IsFirstPerson() ? Mesh1P : GetMesh();
}

float AHunterCharacter::GetTargetingSpeedModifier() const
{
	return TargetingSpeedModifier;
}

bool AHunterCharacter::IsTargeting() const
{
	if (AbilitySystemComponent.IsValid())
	{
		return AbilitySystemComponent->GetTagCount(FGameplayTag::RequestGameplayTag("State.AimDownSights")) > AbilitySystemComponent->GetTagCount(FGameplayTag::RequestGameplayTag("State.AimDownSightsRemoval"));
	}

	return false;
}

float AHunterCharacter::GetRifleAmmo() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetRifleAmmo();
	}
	return 0.f;
}

float AHunterCharacter::GetRunningSpeedModifier() const
{
	return RunningSpeedModifier;
}

bool AHunterCharacter::IsFirstPerson() const
{
	return IsAdmitted() && GetController() && GetController()->IsLocalPlayerController();
}

float AHunterCharacter::GetViolations() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetViolations();
	}
	return 0.f;
}

float AHunterCharacter::GetMaxViolations() const
{
	if (AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetMaxViolations();
	}
	return 1.f;
}

bool AHunterCharacter::IsAdmitted() const
{
	return GetViolations() < GetMaxViolations();
}

USkeletalMeshComponent* AHunterCharacter::GetSpecificPawnMesh(bool WantFirstPerson) const
{
	return WantFirstPerson == true ? Mesh1P : GetMesh();
}

void AHunterCharacter::HandleViolation()
{
	MakeNoise(1.0f, this);
}

void AHunterCharacter::Eject(float DamageAmount, class UGameplayEffectUIData* UIData)
{
	if (!CanDie())
	{
		return;
	}

	NetUpdateFrequency = GetDefault<AHunterCharacter>()->NetUpdateFrequency;
	GetCharacterMovement()->ForceReplicationUpdate();

	RemoveAbilities();

	if (AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent->ApplyGameplayEffectToSelf(Cast<UGameplayEffect>(DeathEffect->GetDefaultObject()), GetCharacterLevel(), AbilitySystemComponent->MakeEffectContext());
	}

	if (auto GM = GetWorld()->GetAuthGameMode<AGameMode_Hunting>())
	{
		AController* EjectedPlayer = Controller ? Controller : Cast<AController>(GetOwner());
		GM->Killed(nullptr, EjectedPlayer, this, UIData);
	}

	OnDeath(DamageAmount, nullptr, nullptr);
}

void AHunterCharacter::OnDeath(float DamageAmount, class APawn* InstigatingPawn, class AActor* DamageCauser)
{
	if (bDying)
	{
		return;
	}

	SetReplicatingMovement(false);
	TearOff();
	bDying = true;

	if (HasAuthority())
	{
		ReplicateHit(DamageAmount, this, GetPlayerState(), true);
	}

	// remove all weapons
	DestroyInventory();

	if (AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent->RegisterGameplayTagEvent(WeaponChangingDelayReplicationTag).Remove(WeaponChangingDelayReplicationTagChangedDelegateHandle);
	}

	EmptyPropsPathMap();

	DetachFromControllerPendingDestroy();

	if (GetMesh())
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetCollisionProfileName(CollisionProfileName);
	}
	SetActorEnableCollision(true);

	// Ragdoll
	SetRagdollPhysics();

	// disable collisions on capsule
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
}

void AHunterCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!GetMesh() || !GetMesh()->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		// initialize physics/etc
		GetMesh()->SetSimulatePhysics(true);
		GetMesh()->WakeAllRigidBodies();
		GetMesh()->bBlendPhysics = true;

		bInRagdoll = true;
	}

	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);

	if (!bInRagdoll)
	{
		// hide and set short lifespan
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1.0f);
	}
	else
	{
		SetLifeSpan(10.0f);
	}
}

static USkeletalMesh* MergeMeshes(TArray<USkeletalMesh*>& MeshesToMerge, USkeleton* Skeleton)
{
	auto BaseMesh = NewObject<USkeletalMesh>();
	if (Skeleton)
	{
		BaseMesh->SetSkeleton(Skeleton);
	}

	const TArray<FSkelMeshMergeSectionMapping> SectionMappings;
	TArray<FSkelMeshMergeUVTransforms> UvTransforms;
	FSkeletalMeshMerge Merger(BaseMesh, MeshesToMerge, SectionMappings, 0, EMeshBufferAccess::ForceCPUAndGPU, UvTransforms.GetData());
	if (!Merger.DoMerge())
	{
		UE_LOG(LogTemp, Warning, TEXT("HunterCharacter: Meshes merge failed!"));
		return nullptr;
	}

	BaseMesh->RebuildSocketMap();
	return BaseMesh;
}

void AHunterCharacter::UpdatePawnMeshes()
{
	bool const bFirstPerson = IsFirstPerson();

	Mesh1P->VisibilityBasedAnimTickOption = !bFirstPerson ? EVisibilityBasedAnimTickOption::OnlyTickPoseWhenRendered : EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	Mesh1P->SetOwnerNoSee(!bFirstPerson);

	GetMesh()->SetOwnerNoSee(bFirstPerson);

	if (bFirstPerson) // Apply human meshes
	{
		if (Body)
		{
			Legs1P->SetSkeletalMesh(Body);
		}
		if (Arms)
		{
			Mesh1P->SetSkeletalMesh(Arms);
		}
	}

	// Apply 3rd person human meshes
	if (!bFirstPerson && !bMesh3PInitialized)
	{
		TArray<USkeletalMesh*> MeshesToMerge;
		if (Body)
		{
			MeshesToMerge.Add(Body);
		}
		if (bShowHair && Hair)
		{
			MeshesToMerge.Add(Hair);
		}
		if (bShowAccessories && Accessories)
		{
			MeshesToMerge.Add(Accessories);
		}

		if (MeshesToMerge.Num() > 0)
		{
			USkeletalMesh* NewMesh = MergeMeshes(MeshesToMerge, Skeleton);
			if (NewMesh)
			{
				GetMesh()->SetSkeletalMesh(NewMesh);
				bMesh3PInitialized = true;
			}
		}
	}
}

void AHunterCharacter::OnToggleFlashlight()
{
	bFlashlightEnable = !bFlashlightEnable;
	ToggleFlashlight();
}

void AHunterCharacter::ToggleFlashlight()
{
	if (IsFirstPerson())
	{
		Flashlight1P->SetVisibility(bFlashlightEnable);
	}
	else
	{
		Flashlight3P->SetVisibility(bFlashlightEnable);
	}
}

void AHunterCharacter::OnRep_FlashlightEnable()
{
	ToggleFlashlight();
}

void AHunterCharacter::KilledBy(class APawn* EventInstigator)
{
	if (HasAuthority() && !bDying)
	{
		Eject(GetMaxViolations(), nullptr);
	}
}

void AHunterCharacter::SetCurrentWeapon(AWeapon* NewWeapon, AWeapon* LastWeapon /*= nullptr*/)
{
	if (NewWeapon == LastWeapon)
	{
		return;
	}

	// Cancel active weapon abilities
	if (AbilitySystemComponent.IsValid())
	{
		FGameplayTagContainer AbilityTagsToCancel = FGameplayTagContainer(WeaponAbilityTag);
		AbilitySystemComponent->CancelAbilities(&AbilityTagsToCancel);
	}

	UnEquipWeapon(LastWeapon);
	if (NewWeapon)
	{
		if (AbilitySystemComponent.IsValid())
		{
			// Clear out potential NoWeaponTag
			AbilitySystemComponent->RemoveLooseGameplayTag(CurrentWeaponTag);
		}

		// Weapons coming from OnRep_CurrentWeapon won't have the owner set
		CurrentWeapon = NewWeapon;
		CurrentWeapon->OnEnterInventory(this);
		CurrentWeapon->OnEquip(LastWeapon);
		CurrentWeaponTag = CurrentWeapon->WeaponTag;

		if (AbilitySystemComponent.IsValid())
		{
			AbilitySystemComponent->AddLooseGameplayTag(CurrentWeaponTag);
		}

		if (bInvisible)
		{
			LastWeapon->GetWeaponMesh()->SetScalarParameterValueOnMaterials(CloakMaterialParameter, 0.f);

			CreateCloakMesh(NewWeapon->GetWeaponMesh());

			const float CurrentAnimPosition = CloakCurve->GetFloatValue(CloakTimeline->GetPlaybackPosition());
			OnUpdateCloakAnimation(CurrentAnimPosition);
		}
	}
	else
	{
		// This will clear HUD, tags etc
		UnEquipCurrentWeapon();
	}
}

void AHunterCharacter::OnRep_CurrentWeapon(AWeapon* LastWeapon)
{
	bChangedWeaponLocally = false;
	SetCurrentWeapon(CurrentWeapon, LastWeapon);
}

void AHunterCharacter::OnRep_Inventory()
{
	if (GetLocalRole() == ROLE_AutonomousProxy && Inventory.Num() > 0 && !CurrentWeapon)
	{
		// Since we don't replicate the CurrentWeapon to the owning client, this is a way to ask the Server to sync
		// the CurrentWeapon after it's been spawned via replication from the Server.
		// The weapon spawning is replicated but the variable CurrentWeapon is not on the owning client.
		ServerSyncCurrentWeapon();
	}
}

void AHunterCharacter::SpawnDefaultInventory()
{
	if (HasAuthority())
	{
		for (const TSubclassOf<AWeapon>& DefaultInventoryClass : DefaultInventoryClasses)
		{
			SpawnWeaponAndAddToInventory(DefaultInventoryClass);
		}

		// equip first weapon in inventory
		if (Inventory.Num() > 0)
		{
			EquipWeapon(Inventory[0]);
			ClientSyncCurrentWeapon(CurrentWeapon);
		}

		InitCustomWeapon();
	}
}

AWeapon* AHunterCharacter::SpawnWeaponAndAddToInventory(TSubclassOf<AWeapon> WeaponClass)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	auto NewWeapon = GetWorld()->SpawnActor<AWeapon>(WeaponClass, SpawnInfo);
	AddWeaponToInventory(NewWeapon);
	return NewWeapon;
}

void AHunterCharacter::DestroyInventory()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		return;
	}

	UnEquipCurrentWeapon();

	// remove all weapons from inventory and destroy them
	for (int32 i = Inventory.Num() - 1; i >= 0; i--)
	{
		AWeapon* Weapon = Inventory[i];
		if (Weapon)
		{
			RemoveWeaponFromInventory(Weapon);
			Weapon->Destroy();
		}
	}
}

void AHunterCharacter::ServerEquipWeapon_Implementation(AWeapon* NewWeapon)
{
	EquipWeapon(NewWeapon);
}

bool AHunterCharacter::ServerEquipWeapon_Validate(AWeapon* NewWeapon)
{
	return true;
}

void AHunterCharacter::WeaponChangingDelayReplicationTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	if (CallbackTag == WeaponChangingDelayReplicationTag)
	{
		if (NewCount < 1)
		{
			// We only replicate the current weapon to simulated proxies so manually sync it when the weapon changing delay replication
			// tag is removed. We keep the weapon changing tag on for ~1s after the equip montage to allow for activating changing weapon
			// again without the server trying to clobber the next locally predicted weapon.
			ClientSyncCurrentWeapon(CurrentWeapon);
		}
	}
}

void AHunterCharacter::ServerSyncCurrentWeapon_Implementation()
{
	ClientSyncCurrentWeapon(CurrentWeapon);
}

bool AHunterCharacter::ServerSyncCurrentWeapon_Validate()
{
	return true;
}

void AHunterCharacter::ClientSyncCurrentWeapon_Implementation(AWeapon* InWeapon)
{
	AWeapon* LastWeapon = CurrentWeapon;
	CurrentWeapon = InWeapon;
	OnRep_CurrentWeapon(LastWeapon);
}

void AHunterCharacter::TrackPropsPath()
{
	for (TActorIterator<APropCharacter> It(GetWorld()); It; ++It)
	{
		auto PropCharacter = *It;

		bool bRequiresPathUpdate = false;

		TArray<FVector>& Locations = PropsLocationsMap.FindOrAdd(PropCharacter);
		const FVector NewLocation = PropCharacter->GetActorLocation();

		if (Locations.Num() == 0)
		{
			Locations.Add(NewLocation);
		}
		else if (!NewLocation.Equals(Locations.Last(), 100.f)) // don't save locations too close together
		{
			if (Locations.Num() >= PathAmount)
			{
				Locations.RemoveAt(0, 1, false);
			}

			Locations.Add(NewLocation);
			bRequiresPathUpdate = true;
		}

		APropsPathVisualiser** Path = PropsPathMap.Find(PropCharacter);
		if (bVisualizePropsPath && (bRequiresPathUpdate || !Path) && Locations.Num() > PathAmount / 2)
		{
			if (!Path)
			{
				const FTransform SpawnTransform(FRotator::ZeroRotator, Locations[0]);
				APropsPathVisualiser* NewPath = GetWorld()->SpawnActor<APropsPathVisualiser>(PropsPathVisualiserTemplate, SpawnTransform);
				PropsPathMap.Add(PropCharacter, NewPath);
				Path = &NewPath;
			}

			TArray<FVector> FloorLocations;
			FloorLocations.Reserve(PathAmount);

			FHitResult Hit;
			FCollisionQueryParams QueryParams(SCENE_QUERY_STAT(TrackPropsPath), false, PropCharacter);

			const int32 NumToShow = (PathOffset < Locations.Num()) ? (Locations.Num() - PathOffset) : Locations.Num();
			for (int32 i = 0; i < NumToShow; i++)
			{
				FVector& TopPoint = Locations[i];
				FVector BottomPoint = TopPoint - FVector(0.f, 0.f, 500.f);
				GetWorld()->LineTraceSingleByChannel(Hit, TopPoint, BottomPoint, ECC_Pawn, QueryParams);

				FloorLocations.Add(Hit.ImpactPoint);
			}

			(*Path)->SetPoints(FloorLocations);
		}
	}
}

void AHunterCharacter::ShowPropsPath(bool bEnable)
{
	bVisualizePropsPath = bEnable;

	PropsPathPostProcess->bEnabled = bVisualizePropsPath;

	if (!bVisualizePropsPath)
	{
		EmptyPropsPathMap();
	}
}

void AHunterCharacter::EmptyPropsPathMap()
{
	for (const auto& PropPath : PropsPathMap)
	{
		PropPath.Value->Destroy();
	}
	PropsPathMap.Empty();
}

bool AHunterCharacter::IsStandingOnFloor()
{
	AActor* Floor = GetCharacterMovement()->CurrentFloor.HitResult.GetActor();
	return Floor && Floor->IsA(ABlockingVolume::StaticClass());
}

void AHunterCharacter::ActivateCloakMode(bool bEnable)
{
	if (CloakTimeline)
	{
		ServerActivateCloakMode(bEnable);
	}
}

void AHunterCharacter::ServerActivateCloakMode_Implementation(bool bEnable)
{
	SetInvisible(bEnable);
}

bool AHunterCharacter::ServerActivateCloakMode_Validate(bool bEnable)
{
	return true;
}

void AHunterCharacter::SetInvisible(bool bInInvisible)
{
	bInvisible = bInInvisible;

	if (bInvisible)
	{
		CreateCloakMesh(GetPawnMesh());
		CreateCloakMesh(CurrentWeapon->GetWeaponMesh());

		CloakTimeline->Play();
	}
	else if (CloakMeshes.Num() > 0)
	{
		CloakTimeline->Reverse();
	}
}

void AHunterCharacter::OnRep_Invisible()
{
	SetInvisible(bInvisible);
}

USkeletalMeshComponent* AHunterCharacter::CreateCloakMesh(USkeletalMeshComponent* Template)
{
	const FString CopyNameSuffix("_Copy");

	USkeletalMeshComponent** ExistingMesh = CloakMeshes.FindByPredicate([&Template, &CopyNameSuffix](USkeletalMeshComponent* TestMesh) {
		return TestMesh->GetName() == (Template->GetName() + CopyNameSuffix);
	});

	USkeletalMeshComponent* CloakMesh = nullptr;

	if (ExistingMesh)
	{
		CloakMesh = *ExistingMesh;
	}
	else
	{
		CloakMesh = Utilities::CopyMeshComponent(Template, CopyNameSuffix);
		if (CloakMesh)
		{
			Utilities::OverrideMeshMaterials(CloakMesh, CloakMaterial);
			CloakMeshes.Add(CloakMesh);
		}
	}

	return CloakMesh;
}

void AHunterCharacter::OnUpdateCloakAnimation(float Output)
{
	if (CloakMeshes.Num() > 0)
	{
		GetPawnMesh()->SetScalarParameterValueOnMaterials(CloakMaterialParameter, Output);
		CurrentWeapon->GetWeaponMesh()->SetScalarParameterValueOnMaterials(CloakMaterialParameter, Output);

		for (USkeletalMeshComponent* CamouflageMesh : CloakMeshes)
		{
			CamouflageMesh->SetScalarParameterValueOnMaterials(CloakMaterialParameter, Output);
		}
	}
}

void AHunterCharacter::OnFinishedCloakAnimation()
{
	if (!bInvisible && CloakMeshes.Num() > 0)
	{
		for (USkeletalMeshComponent* CamouflageMesh : CloakMeshes)
		{
			CamouflageMesh->DestroyComponent();
		}

		CloakMeshes.Empty();
	}
}

void AHunterCharacter::Debug()
{
}

void AHunterCharacter::InitAbilitySystemComponent()
{
	Super::InitAbilitySystemComponent();

	if (AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent->AbilityFailedCallbacks.AddUObject(this, &AHunterCharacter::OnAbilityActivationFailed);
	}
}

void AHunterCharacter::OnAbilityActivationFailed(const UGameplayAbility* FailedAbility, const FGameplayTagContainer& FailTags)
{
	if (FailedAbility && FailedAbility->AbilityTags.HasTagExact(FGameplayTag::RequestGameplayTag(FName("Ability.Weapon.IsChanging"))))
	{
		if (bChangedWeaponLocally)
		{
			// Ask the Server to resync the CurrentWeapon that we predictively changed
			UE_LOG(LogTemp, Warning, TEXT("%s Weapon Changing ability activation failed. Syncing CurrentWeapon. %s"), TEXT(__FUNCTION__), *FailTags.ToString());

			ServerSyncCurrentWeapon();
		}
	}
}

void AHunterCharacter::InitCustomWeapon()
{
	if (const auto PS = GetPlayerState<APHPlayerState>())
	{
		if (!PS->IsPlayerLoaded())
		{
			PS->OnLoadPlayerCompleteDelegate.BindUObject(this, &AHunterCharacter::InitCustomWeapon);
			return;
		}

		if (const auto GameInstance = GetGameInstance<UPHGameInstance>())
		{
			const auto WeaponItem = GameInstance->GetAbilityItem<UPHWeaponItem>(PS->GetCustomWeaponId());
			if (WeaponItem)
			{
				AWeapon* Weapon = SpawnWeaponAndAddToInventory(WeaponItem->WeaponActorClass);
				Weapon->SetAbilities(WeaponItem->GrantedAbilities);
			}
		}
	}
}
