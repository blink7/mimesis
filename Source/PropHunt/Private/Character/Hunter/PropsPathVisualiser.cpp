// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/Hunter/PropsPathVisualiser.h"

#include "Components/SplineComponent.h"


APropsPathVisualiser::APropsPathVisualiser(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SplinePath = ObjectInitializer.CreateDefaultSubobject<USplineComponent>(this, TEXT("Spline"));
	SetRootComponent(SplinePath);

	PrimaryActorTick.bCanEverTick = true;
}

void APropsPathVisualiser::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APropsPathVisualiser::AddPoint(FVector Location)
{
	SplinePath->AddSplinePoint(Location, ESplineCoordinateSpace::World);
}

void APropsPathVisualiser::SetPoints(TArray<FVector>& Points)
{
	SplinePath->SetSplinePoints(Points, ESplineCoordinateSpace::World);
}

void APropsPathVisualiser::RemoveFirstPoint()
{
	SplinePath->RemoveSplinePoint(0, false);
}

