// Copyright Art-Sun Games. All Rights Reserved.

#include "Character/PHCharacterBase.h"

#include "Core/PHPlayerController.h"
#include "Core/Online/PHPlayerState.h"
#include "Core/Abilities/PHAttributeSet.h"
#include "Core/Abilities/PHAbilitySystemGlobals.h"
#include "Core/Abilities/PHAbilitySystemComponent.h"
#include "Core/GameModes/GameMode_Hunting.h"
#include "Character/GrabComponent.h"
#include "Character/PlayerIndicatorComponent.h"

#include "Net/UnrealNetwork.h"
#include "GameFramework/CharacterMovementComponent.h"


APHCharacterBase::APHCharacterBase(const FObjectInitializer& ObjectInitializer) : 
	Super(ObjectInitializer),
	BaseTurnRate(45.f),
	BaseLookUpRate(45.f)
{
	Grabber = ObjectInitializer.CreateDefaultSubobject<UGrabComponent>(this, "Grabber");

	PlayerIndicator = ObjectInitializer.CreateDefaultSubobject<UPlayerIndicatorComponent>(this, "PlayerIndicator");
	PlayerIndicator->SetupAttachment(GetRootComponent());
	PlayerIndicator->SetRelativeLocation({ 0.f, 0.f, 100.f });
}

void APHCharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(APHCharacterBase, bWantsToRun, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(APHCharacterBase, LastTakeHitInfo, COND_Custom);
}

void APHCharacterBase::PreReplication(IRepChangedPropertyTracker& ChangedPropertyTracker)
{
	Super::PreReplication(ChangedPropertyTracker);

	// Only replicate this property for a short duration after it changes so join in progress players don't get spammed with fx when joining late
	DOREPLIFETIME_ACTIVE_OVERRIDE(APHCharacterBase, LastTakeHitInfo, GetWorld() && GetWorld()->GetTimeSeconds() < LastTakeHitTimeTimeout);
}

DECLARE_DELEGATE_OneParam(FBoolDelegate, bool);
void APHCharacterBase::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &APHCharacterBase::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &APHCharacterBase::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APHCharacterBase::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APHCharacterBase::LookUpAtRate);

	PlayerInputComponent->BindAction<FBoolDelegate>("AlternateFire", IE_Pressed, this, &APHCharacterBase::OnRotateItem, true);
	PlayerInputComponent->BindAction<FBoolDelegate>("AlternateFire", IE_Released, this, &APHCharacterBase::OnRotateItem, false);

	PlayerInputComponent->BindAction("ZoomMinimap", IE_Released, this, &APHCharacterBase::OnZoomMinimap);

	BindAbilityInput(PlayerInputComponent);
}

// Client side
void APHCharacterBase::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	InitAbilitySystemComponent();

	BindAbilityInput(InputComponent);

	if (auto PC = GetController<APHPlayerController>())
	{
		PC->UpdateHUD();
	}

	if (!IsLocallyControlled())
	{
		if (auto MyPC = GetWorld()->GetFirstPlayerController<APHPlayerController>())
		{
			MyPC->ServerUpdateCharactersInfo({ this });
		}
	}
}

// Server side
void APHCharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	InitAbilitySystemComponent();

	if (AbilitySystemComponent.IsValid())
	{
		auto PS = GetPlayerState<APHPlayerState>();
		if (PS && !AbilitySystemComponent->bAbilitiesInitialized)
		{
			PS->AddGameplayAbilitiesAndEffects(this, GameplayAbilities, PassiveGameplayEffects);
			PS->InitCustomSkill(this);
			AbilitySystemComponent->bAbilitiesInitialized = true;
		}

		// Remove Dead tag
		AbilitySystemComponent->RemoveActiveEffectsWithGrantedTags(FGameplayTagContainer(DeadTag));
	}

	if (auto PC = GetController<APHPlayerController>())
	{
		PC->UpdateHUD();
	}

	if (!IsLocallyControlled())
	{
		if (auto MyPC = GetWorld()->GetFirstPlayerController<APHPlayerController>())
		{
			MyPC->ServerUpdateCharactersInfo({ this });
		}
	}
}

UAbilitySystemComponent* APHCharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent.Get();
}

void APHCharacterBase::Suicide()
{
	KilledBy(this);
}

bool APHCharacterBase::CanDie() const
{
	if (bDying											// already dying
		|| IsPendingKill()								// already destroyed
		|| !HasAuthority()								// not authority
		|| !GetWorld()->GetAuthGameMode<AGameMode_Hunting>()
		|| GetWorld()->GetAuthGameMode<AGameMode_Hunting>()->GetMatchState() == MatchState::LeavingMap	// level transition occurring
		|| GetWorld()->GetAuthGameMode<AGameMode_Hunting>()->GetMatchState() != MatchState::InProgressHunting)
	{
		return false;
	}

	return true;
}

void APHCharacterBase::GetPlayerViewPoint(FVector& OutLocation, FRotator& OutRotation) const
{
	GetActorEyesViewPoint(OutLocation, OutRotation);
}

class UPrimitiveComponent* APHCharacterBase::GetStandOnComponent() const
{
	return GetCharacterMovement()->CurrentFloor.HitResult.GetComponent();
}

void APHCharacterBase::SetIndicatorVisibility(bool bNewVisibility)
{
	if (PlayerIndicator)
	{
		PlayerIndicator->SetIndicatorVisibility(bNewVisibility);
	}
}

void APHCharacterBase::OnStartRunning(bool bEnable)
{
	auto MyPC = GetController<APHPlayerController>();
	if (MyPC && !MyPC->IsGameActionsIgnored())
	{
		SetRunning(bEnable, false);
	}
}

void APHCharacterBase::OnStartRunningToggle()
{
	auto MyPC = GetController<APHPlayerController>();
	if (MyPC && !MyPC->IsGameActionsIgnored())
	{
		SetRunning(true, true);
	}
}

bool APHCharacterBase::IsRunning() const
{
	if (GetCharacterMovement())
	{
		return (bWantsToRun || bWantsToRunToggled) && !GetVelocity().IsZero() && (GetVelocity().GetSafeNormal2D() | GetActorForwardVector()) > -0.1;
	}
	return false;
}

void APHCharacterBase::SetMaxValkSpeed(float NewMaxSpeed)
{
	GetCharacterMovement()->MaxWalkSpeed = NewMaxSpeed;
}

void APHCharacterBase::SetRunning(bool bNewRunning, bool bToggle)
{
	bWantsToRun = bNewRunning;
	bWantsToRunToggled = bNewRunning && bToggle;

	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetRunning(bNewRunning, bToggle);
	}
}

void APHCharacterBase::ServerSetRunning_Implementation(bool bNewRunning, bool bToggle)
{
	SetRunning(bNewRunning, bToggle);
}

bool APHCharacterBase::ServerSetRunning_Validate(bool bNewRunning, bool bToggle)
{
	return true;
}

void APHCharacterBase::OnRotateItem(bool bEnable)
{
	if (Grabber && Grabber->IsHoldingItem())
	{
		Grabber->bRotateItem = bEnable;
	}
}

void APHCharacterBase::OnZoomMinimap()
{
	if (auto PC = GetController<APHPlayerController>())
	{
		PC->ZoomMinimap();
	}
}

void APHCharacterBase::InitAbilitySystemComponent()
{
	if (auto PS = GetPlayerState<APHPlayerState>())
	{
		AbilitySystemComponent = Cast<UPHAbilitySystemComponent>(PS->GetAbilitySystemComponent());
		AbilitySystemComponent->SetAvatarActor(this);

		AttributeSetBase = Cast<UPHAttributeSet>(PS->GetAttributeSet());
	}

	// Cache tags
	DeadTag = UPHAbilitySystemGlobals::PHGet().DeadTag;
}

void APHCharacterBase::BindAbilityInput(class UInputComponent* PlayerInputComponent)
{
	if (!bAbilitiesInputBound && AbilitySystemComponent.IsValid() && PlayerInputComponent)
	{
		AbilitySystemComponent->BindAbilityActivationToInputComponent(PlayerInputComponent, FGameplayAbilityInputBinds("ConfirmInput", "CancelInput", "AbilityInput"));

		bAbilitiesInputBound = true;
	}
}

void APHCharacterBase::RemoveAbilities()
{
	if (auto PS = GetPlayerState<APHPlayerState>())
	{
		if (AbilitySystemComponent.IsValid())
		{
			PS->RemoveGameplayAbilitiesAndEffects(this, GameplayAbilities);
			AbilitySystemComponent->bAbilitiesInitialized = false;
		}

		PS->RemoveOnDeathEffects();
	}
}

int32 APHCharacterBase::GetCharacterLevel() const
{
	if (auto PS = GetPlayerState<APHPlayerState>())
	{
		return PS->GetCharacterLevel();
	}
	return 1;
}

void APHCharacterBase::AddControllerPitchInput(float Val)
{
	if (Grabber && Grabber->bRotateItem)
	{
		return;
	}

	Super::AddControllerPitchInput(Val);
}

void APHCharacterBase::AddControllerYawInput(float Val)
{
	if (Grabber && Grabber->bRotateItem)
	{
		Grabber->AddYawRotation(Val);
		return;
	}

	Super::AddControllerYawInput(Val);
}

void APHCharacterBase::TurnAtRate(float Value)
{
	// Calculate delta for this frame from the rate information
	AddControllerYawInput(Value * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APHCharacterBase::LookUpAtRate(float Value)
{
	// Calculate delta for this frame from the rate information
	AddControllerPitchInput(Value * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void APHCharacterBase::ReplicateHit(float DamageAmount, APawn* InstigatingPawn, AActor* DamageCauser, bool bKilled)
{
	const float TimeoutTime = GetWorld()->GetTimeSeconds() + 0.5f;

	if ((InstigatingPawn == LastTakeHitInfo.PawnInstigator.Get()) && (LastTakeHitTimeTimeout == TimeoutTime))
	{
		// same frame damage
		if (bKilled && LastTakeHitInfo.bKilled)
		{
			// Redundant death take hit, just ignore it
			return;
		}
	}

	LastTakeHitInfo.ActualDamage = DamageAmount;
	LastTakeHitInfo.PawnInstigator = InstigatingPawn;
	LastTakeHitInfo.DamageCauser = DamageCauser;
	LastTakeHitInfo.bKilled = bKilled;
	LastTakeHitInfo.EnsureReplication();

	LastTakeHitTimeTimeout = TimeoutTime;
}

void APHCharacterBase::OnRep_LastTakeHitInfo()
{
	if (LastTakeHitInfo.bKilled)
	{
		OnDeath(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
}
