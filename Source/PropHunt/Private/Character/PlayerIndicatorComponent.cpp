// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/PlayerIndicatorComponent.h"

#include "Core/Online/PHPlayerState.h"
#include "Core/UI/Widgets/PlayerIndicatorWidget.h"

#include "UObject/ConstructorHelpers.h"
#include "GameFramework/Pawn.h"


UPlayerIndicatorComponent::UPlayerIndicatorComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget> PlayerIndicatorBPClass(TEXT("/Game/PropHunt/UI/Widgets/WBP_PlayerIndicator"));
	WidgetClass = PlayerIndicatorBPClass.Class;

	SetVisibility(false);
	Space = EWidgetSpace::Screen;
	DrawSize = FIntPoint(100, 100);
}

void UPlayerIndicatorComponent::SetIndicatorVisibility(bool bNewVisibility)
{
	auto PawnOwner = Cast<APawn>(GetOwner());
	auto PlayerState = PawnOwner ? PawnOwner->GetPlayerState<APHPlayerState>() : nullptr;
	if (!PlayerState)
	{
		return;
	}

	if (bNewVisibility)
	{
		// Sometimes widget may be not initialized at this point, so enforce its initialization
		if (!GetUserWidgetObject())
		{
			InitWidget();
		}

		if (auto PlayerIndicator = Cast<UPlayerIndicatorWidget>(GetUserWidgetObject()))
		{
			SetVisibility(true);
			PlayerIndicator->SetUp(PlayerState);
		}
	}
	else
	{
		SetVisibility(false);
	}
}
