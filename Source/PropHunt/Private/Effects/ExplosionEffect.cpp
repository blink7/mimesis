// Fill out your copyright notice in the Description page of Project Settings.

#include "Effects/ExplosionEffect.h"

#include "Components/PointLightComponent.h"

#include "Kismet/GameplayStatics.h"

#include "../../FX/Niagara/Source/Niagara/Public/NiagaraComponent.h"
#include "../../FX/Niagara/Source/Niagara/Public/NiagaraFunctionLibrary.h"


AExplosionEffect::AExplosionEffect(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	ExplosionLightComponentName = TEXT("ExplosionLight");

	PrimaryActorTick.bCanEverTick = true;

	ExplosionLight = ObjectInitializer.CreateDefaultSubobject<UPointLightComponent>(this, ExplosionLightComponentName);
	SetRootComponent(ExplosionLight);
	ExplosionLight->AttenuationRadius = 400.f;
	ExplosionLight->Intensity = 8.f;
	ExplosionLight->bUseInverseSquaredFalloff = false;
	ExplosionLight->LightColor = FColor(255, 185, 35);
	ExplosionLight->CastShadows = false;
	ExplosionLight->SetVisibility(true);

	ExplosionLightFadeOut = 0.2f;
}

void AExplosionEffect::BeginPlay()
{
	Super::BeginPlay();

	if (ExplosionFX)
	{
		const auto Explosion = UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, ExplosionFX, GetActorLocation(), GetActorRotation());
		if (Explosion)
		{
			Explosion->SetNiagaraVariableFloat("User.ExplosionRadius", ExplosionRadius);
		}
	}

	if (ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());
	}
}

void AExplosionEffect::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const float TimeAlive = GetWorld()->GetTimeSeconds() - CreationTime;
	const float TimeRemaining = FMath::Max(0.0f, ExplosionLightFadeOut - TimeAlive);

	if (TimeRemaining > 0)
	{
		const float FadeAlpha = 1.0f - FMath::Square(TimeRemaining / ExplosionLightFadeOut);

		const auto DefLight = Cast<UPointLightComponent>(GetClass()->GetDefaultSubobjectByName(ExplosionLightComponentName));
		ExplosionLight->SetIntensity(DefLight->Intensity * FadeAlpha);
	}
	else
	{
		Destroy();
	}
}

