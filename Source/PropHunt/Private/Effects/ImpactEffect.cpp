// Fill out your copyright notice in the Description page of Project Settings.

#include "Effects/ImpactEffect.h"

#include "Core/PHUtilities.h"

#include "Sound/SoundCue.h"
#include "Engine/DataTable.h"
#include "Engine/EngineTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/DecalComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Particles/ParticleSystemComponent.h"


AImpactEffect::AImpactEffect(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	bRandomizeDecalRoll(true)
{
	RootComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, "DefaultSceneRoot");

	static ConstructorHelpers::FObjectFinder<UDataTable> FXMaterialDataTableObject(TEXT("/Game/PropHunt/Effects/DT_FXMaterialData"));
	if (FXMaterialDataTableObject.Succeeded())
	{
		FXMaterialDataTable = FXMaterialDataTableObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<UParticleSystem> SizzleImpactFXObject(TEXT("/Game/BallisticsVFX/Particles/Impacts/DynamicImpacts/_generic/PS_SizzleImpact_Liquid"));
	if (SizzleImpactFXObject.Succeeded())
	{
		SizzleImpactFX = SizzleImpactFXObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<USoundAttenuation> ParticleSoundAttenuationObject(TEXT("/Game/BallisticsVFX/SFX/Attentuations/ATT_Particle"));
	if (ParticleSoundAttenuationObject.Succeeded())
	{
		ParticleSoundAttenuation = ParticleSoundAttenuationObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<USoundCue> SizzlesDrySoundObject(TEXT("/Game/BallisticsVFX/SFX/Impacts/Bits/Sizzles/OnSolid/A_Sizzles_Solid_Cue"));
	if (SizzlesDrySoundObject.Succeeded())
	{
		SizzlesDrySound = SizzlesDrySoundObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<USoundCue> WetSplashSoundObject(TEXT("/Game/BallisticsVFX/SFX/Impacts/Bits/LiquidSplash/InLiquid/A_Splash_01_Cue"));
	if (WetSplashSoundObject.Succeeded())
	{
		WetSplashSound = WetSplashSoundObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<USoundCue> DrySplashSoundObject(TEXT("/Game/BallisticsVFX/SFX/Impacts/Bits/LiquidSplash/OnSolid/A_Splash_Surface_01_Cue"));
	if (DrySplashSoundObject.Succeeded())
	{
		DrySplashSound = DrySplashSoundObject.Object;
	}

	SetAutoDestroyWhenFinished(true);
}

void AImpactEffect::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	const auto HitPhysMat = SurfaceHit.PhysMaterial.Get();
	const auto ImpactType = UPhysicalMaterial::DetermineSurfaceType(HitPhysMat);
	const auto SurfaceString = PropHunt::Utilities::GetEnumValueAsString("EPhysicalSurface", ImpactType);
	MaterialValues = FXMaterialDataTable->FindRow<FFXMaterialRow>(*SurfaceString, "ImpactEffect");

	if (MaterialValues)
	{
		// Show particles
		const auto ImpactFX = MaterialValues->ImpactParticleFX;
		if (ImpactFX)
		{
			const auto ImpactFXEmitter = UGameplayStatics::SpawnEmitterAtLocation(this, ImpactFX, GetActorLocation(), UKismetMathLibrary::MakeRotFromZ(SurfaceHit.ImpactNormal));
			ImpactFXEmitter->OnParticleCollide.AddDynamic(this, &AImpactEffect::OnImpactParticleCollide);
		}

		// Play sound
		const auto ImpactSound = MaterialValues->ImpactSoundFX;
		if (ImpactSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, GetActorLocation());
		}


		if (!MaterialValues->bLiquidSurface && MaterialValues->ImpactDecal.DecalMaterial)
		{
			const auto ImpactDecal = MaterialValues->ImpactDecal;

			const auto ImpactDecalMaterial = UMaterialInstanceDynamic::Create(ImpactDecal.DecalMaterial, this);
			ImpactDecalMaterial->SetScalarParameterValue("Frame", FMath::RandHelper(4));

			FVector ImpactDecalSize(1.f, ImpactDecal.DecalSize, ImpactDecal.DecalSize);
			switch (FXSize)
			{
			case EProjectileSize::Small:
				ImpactDecalSize *= 0.8f;
				break;
			case EProjectileSize::Medium:
				ImpactDecalSize *= 1.0f;
				break;
			case EProjectileSize::Large:
				ImpactDecalSize *= 1.5f;
				break;
			}
			ImpactDecalSize *= FMath::FRandRange(0.5f, 1.f);

			const auto SpawnedImpactDecal = UGameplayStatics::SpawnDecalAtLocation(
				this,
				ImpactDecalMaterial,
				ImpactDecalSize,
				SurfaceHit.Location,
				(-SurfaceHit.ImpactNormal).Rotation(),
				ImpactDecal.LifeSpan);

			if (SpawnedImpactDecal)
			{
				SpawnedImpactDecal->SetFadeOut(ImpactDecal.LifeSpan, 5.f, false);

				if (bRandomizeDecalRoll)
				{
					SpawnedImpactDecal->GetRelativeRotation_DirectMutable().Roll += 360.f * FMath::FRand();
				}
				if (SurfaceHit.GetComponent() && SurfaceHit.GetComponent()->Mobility == EComponentMobility::Movable)
				{
					SpawnedImpactDecal->AttachToComponent(SurfaceHit.GetComponent(), FAttachmentTransformRules::KeepWorldTransform);
				}
			}
		}
	}
}

void AImpactEffect::OnImpactParticleCollide(FName EventName, float EmitterTime, int32 ParticleTime, FVector Location, FVector Velocity, FVector Direction, FVector Normal, FName BoneName, UPhysicalMaterial* PhysMat)
{
	const auto CollisionSurface = UPhysicalMaterial::DetermineSurfaceType(PhysMat);
	const auto SurfaceString = PropHunt::Utilities::GetEnumValueAsString("EPhysicalSurface", CollisionSurface);
	const auto FXMaterialData = FXMaterialDataTable->FindRow<FFXMaterialRow>(*SurfaceString, "ImpactEffect");

	if (FXMaterialData)
	{
		// Play sound
		if (FXMaterialData->bHotSurface || FXMaterialData->bCaustic || MaterialValues->bHotSurface)
		{
			UGameplayStatics::PlaySoundAtLocation(this, SizzlesDrySound, Location, 1.f, UGameplayStatics::GetGlobalTimeDilation(GetWorld()), 0.f, ParticleSoundAttenuation);
		}
		else if (FXMaterialData->bLiquidSurface)
		{
			UGameplayStatics::PlaySoundAtLocation(this, WetSplashSound, Location, 1.f, UGameplayStatics::GetGlobalTimeDilation(GetWorld()), 0.f, ParticleSoundAttenuation);
		}
		else if (MaterialValues->bLiquidSurface)
		{
			UGameplayStatics::PlaySoundAtLocation(this, DrySplashSound, Location, 1.f, UGameplayStatics::GetGlobalTimeDilation(GetWorld()), 0.f, ParticleSoundAttenuation);
		}
		else
		{
			UGameplayStatics::PlaySoundAtLocation(this, FXMaterialData->ParticleCollisionSounds, Location, 1.f, UGameplayStatics::GetGlobalTimeDilation(GetWorld()), 0.f, ParticleSoundAttenuation);
		}

		if (FXMaterialData->bFlammable && MaterialValues->bHotSurface)
		{
			// Fire propagation
		}

		if (FXMaterialData->bLiquidSurface)
		{
			if (FXMaterialData->bHotSurface && (MaterialValues->bLiquidSurface || MaterialValues->bCaustic) || MaterialValues->bHotSurface)
			{
				UGameplayStatics::SpawnEmitterAtLocation(this, SizzleImpactFX, Location, UKismetMathLibrary::MakeRotFromX(-Location));
			}
			else
			{
				SpawnDebrisFX(Location);
			}
		}
		else
		{
			// Spawn Particle Decals
			if (MaterialValues->ParticleDecal.DecalMaterial)
			{
				const auto ParticleDecal = MaterialValues->ParticleDecal;

				const auto SplasherDecalMaterial = UMaterialInstanceDynamic::Create(ParticleDecal.DecalMaterial, this);
				UDecalComponent* SpawnedParticleDecal = nullptr;

				FVector ParticleDecalSize(0.01f, ParticleDecal.DecalSize, ParticleDecal.DecalSize);
				switch (FXSize)
				{
				case EProjectileSize::Small:
					ParticleDecalSize *= 0.8f;
					break;
				case EProjectileSize::Medium:
					ParticleDecalSize *= 1.0f;
					break;
				case EProjectileSize::Large:
					ParticleDecalSize *= 1.5f;
					break;
				}
				ParticleDecalSize *= FMath::FRandRange(1.5f, 3.5f);

				FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(ParticleCollisionLineCheck), false, this);
				FHitResult Hit;

				GetWorld()->LineTraceSingleByChannel(Hit, Location, Location + Direction, ECC_Visibility, TraceParams);
				
				SpawnedParticleDecal = UGameplayStatics::SpawnDecalAtLocation(
					this,
					SplasherDecalMaterial,
					ParticleDecalSize,
					Location,
					(-Normal).Rotation(),
					ParticleDecal.LifeSpan);

				if (SpawnedParticleDecal)
				{
					SpawnedParticleDecal->SetFadeOut(ParticleDecal.LifeSpan, 5.f, false);

					if (bRandomizeDecalRoll)
					{
						SpawnedParticleDecal->GetRelativeRotation_DirectMutable().Roll += 360.f * FMath::FRand();
					}
					if (Hit.GetComponent() && Hit.GetComponent()->Mobility == EComponentMobility::Movable)
					{
						SpawnedParticleDecal->AttachToComponent(Hit.GetComponent(), FAttachmentTransformRules::KeepWorldTransform);
					}
				}
			}

			SpawnDebrisFX(Location);
		}
	}
}

void AImpactEffect::SpawnDebrisFX(FVector& Location)
{
	const auto DebrisFX = MaterialValues->DebrisParticleFX;
	if (DebrisFX)
	{
		FVector DebrisDecalScale;
		switch (FXSize)
		{
		case EProjectileSize::Small:
			DebrisDecalScale = FVector(0.8f);
			break;
		case EProjectileSize::Medium:
		case EProjectileSize::Large:
			DebrisDecalScale = FVector(1.f);
			break;
		}
		DebrisDecalScale *= FMath::FRandRange(0.5f, 1.f);

		UGameplayStatics::SpawnEmitterAtLocation(this, DebrisFX, Location, UKismetMathLibrary::MakeRotFromX(-Location), DebrisDecalScale);
	}
}
