// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "SlateBasics.h"
#include "SlateExtras.h"


class PROPHUNTLOADINGSCREEN_API SStartupLoadingScreen : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SStartupLoadingScreen)
		{}

		SLATE_ARGUMENT(FSlateFontInfo, Font)
		SLATE_ARGUMENT(FLinearColor, TextColor)
		SLATE_ARGUMENT(FLinearColor, BackgroundColor)
		SLATE_ARGUMENT(FText, LoadingText)
		SLATE_ARGUMENT(TArray<FText>, Tips)
		SLATE_ARGUMENT(FSoftObjectPath, LogoTexture)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

private:
	FSlateFontInfo Font;
	FLinearColor TextColor;
	FText LoadingText;
	TArray<FText> LoadingTextLines;

	double AnimStartTime;
	double AnimNextExecutionTime;

private:
	/** Loading screen image brush */
	TSharedPtr<FSlateDynamicImageBrush> LoadingScreenBrush;

	TSharedPtr<SVerticalBox> VBox;

	TSharedPtr<SWrapBox> WBox;
};

class PROPHUNTLOADINGSCREEN_API SDefaultLoadingScreen : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SDefaultLoadingScreen)
		{}

		SLATE_ARGUMENT(FText, LoadingText)
		SLATE_ARGUMENT(TArray<FText>, Tips)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

private:
	double AnimStartTime;
	double AnimNextExecutionTime;

	FCurveSequence SlideAnimCurves;
	FCurveHandle SlideCurve;

	bool bTransitionSlideVisible;

	int32 FirstSlideIndex;
	int32 SecondSlideIndex;

	const FSlateBrush* GetFirstSlideBrush() const;
	const FSlateBrush* GetSecondSlideBrush() const;

	const FSlateBrush* GetSlideBrush(int32 SlideIndex) const;

	FSlateColor GetSlideColor() const;

	int32 GetRandSlideIndex(int32 ExcludeIndex) const;

private:
	const struct FLoadingScreenStyle* LoadingScreenStyle;
};
