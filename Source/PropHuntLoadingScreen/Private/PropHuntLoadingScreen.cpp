// Copyright Art-Sun Games. All Rights Reserved.

#include "PropHuntLoadingScreen.h"

#include "PropHuntStyle.h"
#include "SLoadingScreen.h"
#include "LoadingScreenSettings.h"

#include "MoviePlayer.h"


class FPropHuntLoadingScreenModule : public IPropHuntLoadingScreenModule
{
public:
	virtual void StartupModule() override
	{
		FPropHuntStyle::Initialize();

		// Force load for cooker reference
		const auto Settings = GetDefault<ULoadingScreenSettings>();
		Settings->LogoImage.TryLoad();

		if (IsMoviePlayerEnabled())
		{
			CreateScreen();
		}
	}

	virtual void ShutdownModule() override
	{
		FPropHuntStyle::Shutdown();
	}
	
	virtual bool IsGameModule() const override
	{
		return true;
	}

	virtual void StartInGameLoadingScreen(const FText& LoadingText, bool bForcePlay) override
	{
		const auto Settings = GetDefault<ULoadingScreenSettings>();
		const FLoadingScreenDescription& ScreenDescription = Settings->DefaultScreen;
		
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = !ScreenDescription.bPlayUntilStopped;
		LoadingScreen.bWaitForManualStop = ScreenDescription.bPlayUntilStopped;
		LoadingScreen.bAllowEngineTick = ScreenDescription.bPlayUntilStopped;
		LoadingScreen.MinimumLoadingScreenDisplayTime = ScreenDescription.PlayTime;
		LoadingScreen.WidgetLoadingScreen = SNew(SDefaultLoadingScreen)
			.LoadingText(LoadingText)
			.Tips(ScreenDescription.Tips);

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);

		if (bForcePlay)
		{
			GetMoviePlayer()->PlayMovie();
		}
	}

	virtual void StopInGameLoadingScreen() override
	{
		GetMoviePlayer()->StopMovie();
	}

	virtual void CreateScreen()
	{
		const auto Settings = GetDefault<ULoadingScreenSettings>();
		const FLoadingScreenDescription& ScreenDescription = Settings->StartupScreen;

		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = !ScreenDescription.bPlayUntilStopped;
		LoadingScreen.bWaitForManualStop = ScreenDescription.bPlayUntilStopped;
		LoadingScreen.bAllowEngineTick = ScreenDescription.bPlayUntilStopped;
		LoadingScreen.MinimumLoadingScreenDisplayTime = ScreenDescription.PlayTime;
		LoadingScreen.WidgetLoadingScreen = SNew(SStartupLoadingScreen)
			.LoadingText(Settings->LoadingText)
			.Tips(ScreenDescription.Tips)
			.Font(Settings->Font)
			.TextColor(Settings->TextColor)
			.BackgroundColor(Settings->BackgroundColor)
			.LogoTexture(Settings->LogoImage);

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}
};

IMPLEMENT_GAME_MODULE(FPropHuntLoadingScreenModule, PropHuntLoadingScreen);
