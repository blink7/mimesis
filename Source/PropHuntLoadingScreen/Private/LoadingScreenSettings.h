// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Fonts/SlateFontInfo.h"
#include "Engine/DeveloperSettings.h"
#include "LoadingScreenSettings.generated.h"


USTRUCT(BlueprintType)
struct FLoadingScreenDescription
{
	GENERATED_BODY()

	/** The minimum time that a loading screen should be opened for, -1 if there is no minimum time. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
	float PlayTime = -1;

	/** If true, movie playback continues until Stop is called. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
	bool bPlayUntilStopped = false;

	/**  Text displayed beside the animated icon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Display")
	TArray<FText> Tips;
};

/**
 * Settings for the simple loading screen plugin.
 */
UCLASS(config=Game, defaultconfig, meta = (DisplayName = "Loading Screen"))
class ULoadingScreenSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	ULoadingScreenSettings();

	/** The startup screen for the project. */
	UPROPERTY(config, EditAnywhere, Category = "Screens")
	FLoadingScreenDescription StartupScreen;

	/** The default load screen between maps. */
	UPROPERTY(config, EditAnywhere, Category = "Screens")
	FLoadingScreenDescription DefaultScreen;

	/**  Text displayed beside the animated icon */
	UPROPERTY(config, EditAnywhere, Category = "Display", meta = (MultiLine = "true"))
	FText LoadingText;

	/** The font to display on loading. */
	UPROPERTY(config, EditAnywhere, Category = "Display")
	FSlateFontInfo Font;

	/** The background color to use */
	UPROPERTY(config, EditAnywhere, Category = "Display")
	FLinearColor TextColor = FLinearColor::White;

	/** The background color to use */
	UPROPERTY(config, EditAnywhere, Category = "Display")
	FLinearColor BackgroundColor = FLinearColor::Black;

	/** The texture display while in the loading screen on top of the movie. */
	UPROPERTY(config, EditAnywhere, Category = "Display", meta = (AllowedClasses = "Texture2D"))
	FSoftObjectPath LogoImage;
};