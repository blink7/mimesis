// Copyright Art-Sun Games. All Rights Reserved.

#include "LoadingScreenSettings.h"

#include "UObject/ConstructorHelpers.h"
#include "Engine/Font.h"

#define LOCTEXT_NAMESPACE "LoadingScreen"

ULoadingScreenSettings::ULoadingScreenSettings()
	: LoadingText(LOCTEXT("Loading", "Loading"))
{
	if (!IsRunningDedicatedServer())
	{
		static ConstructorHelpers::FObjectFinder<UFont> RobotoFontObj(TEXT("/Engine/EngineFonts/Roboto"));
		Font = FSlateFontInfo(RobotoFontObj.Object, 32, FName("Bold"));
	}
}

#undef LOCTEXT_NAMESPACE
