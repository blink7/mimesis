// Copyright Art-Sun Games. All Rights Reserved.

#include "SLoadingScreen.h"

#include "SPHThrobber.h"
#include "PropHuntStyle.h"
#include "LoadingScreenWidgetStyle.h"

#include "MoviePlayer.h"
#include "Widgets/Layout/SScaleBox.h"
#include "Widgets/Layout/SBackgroundBlur.h"


struct FLoadingScreenBrush : public FSlateDynamicImageBrush, public FGCObject
{
	FLoadingScreenBrush(const FName InTextureName, const FVector2D& InImageSize)
		: FSlateDynamicImageBrush(InTextureName, InImageSize)
	{
		SetResourceObject(LoadObject<UObject>(nullptr, *InTextureName.ToString()));
	}

	virtual void AddReferencedObjects(FReferenceCollector& Collector)
	{
		if (UObject* CachedResourceObject = GetResourceObject())
		{
			Collector.AddReferencedObject(CachedResourceObject);
		}
	}
};

void SStartupLoadingScreen::Construct(const FArguments& InArgs)
{
	Font = InArgs._Font;
	TextColor = InArgs._TextColor;
	LoadingText = InArgs._LoadingText;
	LoadingTextLines = InArgs._Tips;

	AnimStartTime = FSlateApplicationBase::Get().GetCurrentTime();
	AnimNextExecutionTime = AnimStartTime + 1.f;

	LoadingScreenBrush = MakeShareable(new FLoadingScreenBrush(InArgs._LogoTexture.GetAssetPathName(), { 256, 256 }));

	VBox = SNew(SVerticalBox);

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SBorder)
			.BorderImage(FStyleDefaults::GetNoBrush())
			.ColorAndOpacity(InArgs._BackgroundColor)
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Top)
		[
			SNew(SImage)
			.Image(LoadingScreenBrush.Get())
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(12.f)
		[
			VBox.ToSharedRef()
		]
	];
}

void SStartupLoadingScreen::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	if (InCurrentTime >= AnimNextExecutionTime)
	{
		while (InCurrentTime >= AnimNextExecutionTime)
		{
			AnimNextExecutionTime += FMath::RandRange(0.1f, 0.6f);
		}

		static int32 Index = 0;

		if (LoadingTextLines.IsValidIndex(Index))
		{
			VBox->AddSlot()
				.HAlign(HAlign_Left)
				.VAlign(VAlign_Top)
				.AutoHeight()
				[
					SNew(STextBlock)
					.Text(LoadingTextLines[Index++])
					.Font(Font)
					.ColorAndOpacity(TextColor)
				];
		}
		else if (!WBox.IsValid())
		{
			WBox = SNew(SWrapBox).UseAllottedSize(true)
				+ SWrapBox::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					SNew(STextBlock)
					.Text(LoadingText)
					.Font(Font)
					.ColorAndOpacity(TextColor)
				];

			VBox->AddSlot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				.AutoHeight()
				[
					WBox.ToSharedRef()
				];
		}
		else
		{
			WBox->AddSlot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					SNew(STextBlock)
					.Text(FText::FromString("."))
					.Font(Font)
					.ColorAndOpacity(TextColor)
				];
		}
	}
}

void SDefaultLoadingScreen::Construct(const FArguments& InArgs)
{
	LoadingScreenStyle = &FPropHuntStyle::Get().GetWidgetStyle<FLoadingScreenStyle>("Style_DefaultLoadingScreen");

	AnimStartTime = FSlateApplicationBase::Get().GetCurrentTime();
	AnimNextExecutionTime = AnimStartTime + LoadingScreenStyle->SlideshowDelay;

	SlideAnimCurves = FCurveSequence();
	SlideCurve = SlideAnimCurves.AddCurve(0.f, LoadingScreenStyle->SlideshowTransitionSpeed);

	FirstSlideIndex = GetRandSlideIndex(0);

	ChildSlot
	[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SScaleBox)
			.Stretch(EStretch::ScaleToFill)
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					SNew(SImage)
					.Image(this, &SDefaultLoadingScreen::GetFirstSlideBrush)
				]
				+ SOverlay::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					SNew(SImage)
					.Image(this, &SDefaultLoadingScreen::GetSecondSlideBrush)
					.ColorAndOpacity(this, &SDefaultLoadingScreen::GetSlideColor)
				]
			]
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SBackgroundBlur)
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Fill)
			.BlurStrength(5.f)
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Bottom)
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Fill)
			[
				SNew(SImage)
				.Image(&LoadingScreenStyle->BackgroundImage)
			]
			+ SOverlay::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Fill)
			.Padding(0.f, 24.f)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				.AutoHeight()
				[
					SNew(STextBlock)
					.Justification(ETextJustify::Center)
					.TextStyle(&LoadingScreenStyle->TextStyle)
					.Text(InArgs._LoadingText)
				]
				+ SVerticalBox::Slot()
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Fill)
				.Padding(0.f, 14.f)
				[
					SNew(SBorder)
					.HAlign(HAlign_Fill)
					.VAlign(VAlign_Fill)
					.Padding(6.f)
					.BorderImage(&LoadingScreenStyle->ThrobberBackgroundImage)
					[
						SNew(SPHThrobber)
						.PieceImage(&LoadingScreenStyle->ThrobberImage)
						.Length(LoadingScreenStyle->ThrobberLength)
						.NumPieces(LoadingScreenStyle->ThrobberPieces)
						.AnimationFrequency(LoadingScreenStyle->ThrobberFrequency)
					]
				]
				+ SVerticalBox::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				.AutoHeight()
				[
					SNew(STextBlock)
					.Justification(ETextJustify::Center)
					.TextStyle(&LoadingScreenStyle->TextStyle)
					.AutoWrapText(true)
					.Text(InArgs._Tips[FMath::RandHelper(InArgs._Tips.Num())])
				]
			]
		]
	];
}

void SDefaultLoadingScreen::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	if (InCurrentTime >= AnimNextExecutionTime)
	{
		while (InCurrentTime >= AnimNextExecutionTime)
		{
			AnimNextExecutionTime += LoadingScreenStyle->SlideshowDelay;
		}

		if (LoadingScreenStyle->SlideImages.Num() > 0)
		{
			if (bTransitionSlideVisible)
			{
				FirstSlideIndex = GetRandSlideIndex(SecondSlideIndex);
				SlideAnimCurves.Reverse();
			}
			else
			{
				SecondSlideIndex = GetRandSlideIndex(FirstSlideIndex);
				SlideAnimCurves.Play(AsShared());
			}

			bTransitionSlideVisible = !bTransitionSlideVisible;
		}
	}
}

const FSlateBrush* SDefaultLoadingScreen::GetFirstSlideBrush() const
{
	return GetSlideBrush(FirstSlideIndex);
}

const FSlateBrush* SDefaultLoadingScreen::GetSecondSlideBrush() const
{
	return GetSlideBrush(SecondSlideIndex);
}

const FSlateBrush* SDefaultLoadingScreen::GetSlideBrush(int32 SlideIndex) const
{
	if (LoadingScreenStyle->SlideImages.IsValidIndex(SlideIndex))
	{
		return &LoadingScreenStyle->SlideImages[SlideIndex];
	}

	return FCoreStyle::Get().GetDefaultBrush();
}

FSlateColor SDefaultLoadingScreen::GetSlideColor() const
{
	return FLinearColor(1.0f, 1.0f, 1.0f, SlideCurve.GetLerp());
}

int32 SDefaultLoadingScreen::GetRandSlideIndex(int32 ExcludeIndex) const
{
	const int32 Index = FMath::RandHelper(LoadingScreenStyle->SlideImages.Num());
	if (Index == ExcludeIndex)
	{
		return GetRandSlideIndex(ExcludeIndex);
	}

	return Index;
}
