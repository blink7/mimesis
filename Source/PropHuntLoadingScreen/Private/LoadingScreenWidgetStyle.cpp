// Copyright Art-Sun Games. All Rights Reserved.

#include "LoadingScreenWidgetStyle.h"


FLoadingScreenStyle::FLoadingScreenStyle()
	: SlideshowDelay(5.f),
	SlideshowTransitionSpeed(1.5f),
	ThrobberLength(10),
	ThrobberPieces(3),
	ThrobberFrequency(0.5f)
{
}

void FLoadingScreenStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	OutBrushes.Add(&ThrobberImage);
	OutBrushes.Add(&ThrobberBackgroundImage);
	OutBrushes.Add(&BackgroundImage);

	for (const FSlateBrush& Slide : SlideImages)
	{
		OutBrushes.Add(&Slide);
	}
}

const FName FLoadingScreenStyle::TypeName(TEXT("FLoadingScreenStyle"));

const FLoadingScreenStyle& FLoadingScreenStyle::GetDefault()
{
	static FLoadingScreenStyle Default;
	return Default;
}
