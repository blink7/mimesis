// Copyright Art-Sun Games. All Rights Reserved.

#include "SPHThrobber.h"

#include "Widgets/Images/SImage.h"


void SPHThrobber::Construct(const FArguments& InArgs)
{
	AnimationFrequency = InArgs._AnimationFrequency;
	Length = InArgs._Length;
	NumPieces = InArgs._NumPieces;
	PieceImage = InArgs._PieceImage;

	AnimStartTime = FSlateApplicationBase::Get().GetCurrentTime();
	AnimNextExecutionTime = AnimStartTime + AnimationFrequency;

	HBox = SNew(SHorizontalBox);

	ChildSlot
	[
		HBox.ToSharedRef()
	];

	ConstructPieces();
}

void SPHThrobber::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	if (InCurrentTime >= AnimNextExecutionTime)
	{
		while (InCurrentTime >= AnimNextExecutionTime)
		{
			AnimNextExecutionTime += AnimationFrequency;
		}

		AnimCounter++;
	}
}

void SPHThrobber::SetPieceImage(const FSlateBrush* InPieceImage)
{
	PieceImage = InPieceImage;
}

void SPHThrobber::SetLength(int32 InLength)
{
	Length = InLength;
	ConstructPieces();
}

void SPHThrobber::SetNumPieces(int32 InNumPieces)
{
	NumPieces = InNumPieces;
	ConstructPieces();
}

void SPHThrobber::SetAnimationFrequency(float InFrequency)
{
	AnimationFrequency = InFrequency;
}

void SPHThrobber::ConstructPieces()
{
	HBox->ClearChildren();
	for (int32 PieceIndex = 0; PieceIndex < Length; ++PieceIndex)
	{
		HBox->AddSlot()
		.AutoWidth()
		[
			SNew(SBorder)
			.BorderImage(FStyleDefaults::GetNoBrush())
			.Visibility(this, &SPHThrobber::GetPieceVisibility, PieceIndex)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(SImage)
				.Image(this, &SPHThrobber::GetPieceBrush)
			]
		];
	}
}

EVisibility SPHThrobber::GetPieceVisibility(int32 PieceIndex) const
{
	int32 AnimIndex = AnimCounter % (Length + NumPieces);
	const bool bVisible = FMath::IsWithin(PieceIndex, AnimIndex - NumPieces, AnimIndex);

	return bVisible ? EVisibility::Visible : EVisibility::Hidden;
}

const FSlateBrush* SPHThrobber::GetPieceBrush() const
{
	return PieceImage;
}
