// Copyright Art-Sun Games. All Rights Reserved.

#include "PropHuntStyle.h"

#include "Slate/SlateGameResources.h"


TSharedPtr<FSlateStyleSet> FPropHuntStyle::PropHuntStyleInstance = NULL;

void FPropHuntStyle::Initialize()
{
	if (!PropHuntStyleInstance.IsValid())
	{
		PropHuntStyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*PropHuntStyleInstance);
	}
}

void FPropHuntStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*PropHuntStyleInstance);
	ensure(PropHuntStyleInstance.IsUnique());
	PropHuntStyleInstance.Reset();
}

void FPropHuntStyle::ReloadTextures()
{
	FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}

const ISlateStyle& FPropHuntStyle::Get()
{
	return *PropHuntStyleInstance;
}

FName FPropHuntStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("PropHuntStyle"));
	return StyleSetName;
}

TSharedRef< class FSlateStyleSet > FPropHuntStyle::Create()
{
	TSharedRef<FSlateStyleSet> StyleRef = FSlateGameResources::New(FPropHuntStyle::GetStyleSetName(), "/Game/PropHunt/Slate/Style", "/Game/PropHunt/Slate/Style");
	FSlateStyleSet& Style = StyleRef.Get();

	return StyleRef;
}
