// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateWidgetStyleContainerBase.h"
#include "LoadingScreenWidgetStyle.generated.h"


/**
 * Represents the appearance of an FShooterMenuItem
 */
USTRUCT()
struct FLoadingScreenStyle : public FSlateWidgetStyle
{
	GENERATED_BODY()

	FLoadingScreenStyle();
	virtual ~FLoadingScreenStyle() {}

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FLoadingScreenStyle& GetDefault();

	UPROPERTY(EditAnywhere, Category = "Appearance")
	FTextBlockStyle TextStyle;
	FLoadingScreenStyle& SetTextStyle(const FTextBlockStyle& InTextStyle) { TextStyle = InTextStyle; return *this; }

	/**
	 * The brush used for the background behind text and throbber
	 */	
	UPROPERTY(EditAnywhere, Category = "Appearance")
	FSlateBrush BackgroundImage;
	FLoadingScreenStyle& SetBackgroundImage(const FSlateBrush& InBackgroundImage) { BackgroundImage = InBackgroundImage; return *this; }

	/**
	 * The brushes used for the background dynamic slide show
	 */
	UPROPERTY(EditAnywhere, Category = "Slide")
	TArray<FSlateBrush> SlideImages;
	FLoadingScreenStyle& SetSlideImages(const TArray<FSlateBrush>& InSlideImages) { SlideImages = InSlideImages; return *this; }

	UPROPERTY(EditAnywhere, Category = "Slide")
	float SlideshowDelay;
	FLoadingScreenStyle& SetSlideshowDelay(float InSlideshowDelay) { SlideshowDelay = InSlideshowDelay; return *this; }

	UPROPERTY(EditAnywhere, Category = "Slide")
	float SlideshowTransitionSpeed;
	FLoadingScreenStyle& SetSlideshowTransitionSpeed(float InSlideshowTransitionSpeed) { SlideshowTransitionSpeed = InSlideshowTransitionSpeed; return *this; }

	/**
	 * The brush used for the throbber pieces
	 */	
	UPROPERTY(EditAnywhere, Category = "Throbber")
	FSlateBrush ThrobberImage;
	FLoadingScreenStyle& SetThrobberImage(const FSlateBrush& InBackgroundBrush) { ThrobberImage = InBackgroundBrush; return *this; }

	/**
	 * The image used for the throbber background
	 */	
	UPROPERTY(EditAnywhere, Category = "Throbber")
	FSlateBrush ThrobberBackgroundImage;
	FLoadingScreenStyle& SetThrobberBackgroundImage(const FSlateBrush& InLeftArrowImage) { ThrobberBackgroundImage = InLeftArrowImage; return *this; }
	
	UPROPERTY(EditAnywhere, Category = "Throbber")
	uint16 ThrobberLength;
	FLoadingScreenStyle& SetThrobberLength(uint16 InThrobberLength) { ThrobberLength = InThrobberLength; return *this; }

	UPROPERTY(EditAnywhere, Category = "Throbber")
	uint16 ThrobberPieces;
	FLoadingScreenStyle& SetThrobberPieces(uint16 InThrobberPieces) { ThrobberPieces = InThrobberPieces; return *this; }

	UPROPERTY(EditAnywhere, Category = "Throbber")
	float ThrobberFrequency;
	FLoadingScreenStyle& SetThrobberFrequency(float InThrobberFrequency) { ThrobberFrequency = InThrobberFrequency; return *this; }
};

/**
 * 
 */
UCLASS()
class PROPHUNTLOADINGSCREEN_API ULoadingScreenWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()
	
public:
	/** The actual data describing the menu's appearance. */
	UPROPERTY(Category = "Appearance", EditAnywhere, meta = (ShowOnlyInnerProperties))
	FLoadingScreenStyle MenuItemStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
		return static_cast<const struct FSlateWidgetStyle*>(&MenuItemStyle);
	}
};
