// Copyright Art-Sun Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


class PROPHUNTLOADINGSCREEN_API SPHThrobber : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SPHThrobber)
		: _AnimationFrequency(0.2f),
		_Length(12),
		_NumPieces(3),
		_PieceImage(FCoreStyle::Get().GetBrush("Throbber.Chunk"))
		{}

		SLATE_ARGUMENT(float, AnimationFrequency)
		SLATE_ARGUMENT(int, Length)
		SLATE_ARGUMENT(int, NumPieces)
		SLATE_ARGUMENT(const FSlateBrush*, PieceImage)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

	void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	/** Sets what each segment of the throbber looks like. */
	void SetPieceImage(const FSlateBrush* InPieceImage);

	/** Sets total length */
	void SetLength(int32 InLength);

	/** Sets how many pieces there are */
	void SetNumPieces(int32 InNumPieces);

	/** Sets how quickly pieces are running (1 is slowest) */
	void SetAnimationFrequency(float InFrequency);

private:
	float AnimationFrequency;

	double AnimStartTime;
	double AnimNextExecutionTime;
	int32 AnimCounter;

	int32 Length;
	int32 NumPieces;

	const FSlateBrush* PieceImage;

private:
	/** The horizontal box which contains the widgets for the throbber pieces. */
	TSharedPtr<SHorizontalBox> HBox;

	/** Constructs the curves and widgets for the pieces which make up the throbber. */
	void ConstructPieces();

	EVisibility GetPieceVisibility(int32 PieceIndex) const;

	/** Gets the brush used to draw each piece of the throbber. */
	const FSlateBrush* GetPieceBrush() const;
};