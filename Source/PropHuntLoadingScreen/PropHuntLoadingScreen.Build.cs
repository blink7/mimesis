// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;


public class PropHuntLoadingScreen : ModuleRules
{
	public PropHuntLoadingScreen(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivatePCHHeaderFile = "Public/PropHuntLoadingScreen.h";

		PCHUsage = PCHUsageMode.UseSharedPCHs;

		PrivateIncludePaths.Add("PropHuntLoadingScreen/Private");

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine" });

		PrivateDependencyModuleNames.AddRange(new string[] { "MoviePlayer", "InputCore", "Slate", "SlateCore", "DeveloperSettings" });
	}
}
