# Mimesis

First/third-person multiplayer Unreal Engine game based on the Hide'n'Seek game mode.

Some gameplay can be found on [YouTube](https://youtu.be/0g9_HjZrA1M?t=114).

## Technical details

The project is divided into 3 parts:
- main game code (C++)
- back-end server with user data (Java, Spring Framework) [Mimesis API](https://gitlab.com/blink7/mimesis-api)
- front-end web application (Typescript, Vue.js) [Mimesis API](https://gitlab.com/blink7/mimesis-api)

This repository doesn't include game assets (can be provided upon personal request).

## License
Not for personal or comercial use!
